import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// cust_type_id : 7
/// code : "FOR"
/// name : "Cá nhân nước ngoài"
/// type : "1"

part 'customer_type_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CustomerTypeModel {
  CustomerTypeModel({
    this.custTypeId,
    this.code,
    this.name,
    this.type,
  });

  final int? custTypeId;
  final String? code;
  final String? name;
  final String? type;

  factory CustomerTypeModel.fromJson(Map<String, dynamic> json) => _$CustomerTypeModelFromJson(json);
}

@JsonSerializable()
class CustomerTypeResponse {
  final List<CustomerTypeModel>? data;
  final MessageData? mess;

  CustomerTypeResponse({this.data, this.mess});

  factory CustomerTypeResponse.fromJson(Map<String, dynamic> json) => _$CustomerTypeResponseFromJson(json);
}
