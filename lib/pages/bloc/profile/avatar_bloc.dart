import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

part 'avatar_event.dart';

part 'avatar_state.dart';

class AvatarBloc extends HydratedBloc<AvatarEvent, AvatarState> {
  final IUserRepository _userRepository;
  final AppBloc _appBloc;

  AvatarBloc({
    required IUserRepository userRepository,
    required AppBloc appBloc,
  })  : _userRepository = userRepository,
        _appBloc = appBloc,
        super(const AvatarState()) {
    on<AvatarDownloaded>(_onAvatarDownloaded);
    on<AvatarUploaded>(_onAvatarUploaded);
  }

  @override
  AvatarState? fromJson(Map<String, dynamic> json) {
    return AvatarState.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(AvatarState state) {
    return state.toJson();
  }

  FutureOr<void> _onAvatarDownloaded(AvatarDownloaded event, emit) async {
    final filePath = state.avatarFilePath;

    // Do nothing if we already cached the avatar
    if (filePath != null && state.contractNo == _appBloc.state.user?.contractNo) {
      final file = File(filePath);

      if (file.existsSync()) {
        emit(state.copyWith(
          avatarFilePath: filePath,
          message: null,
        ));
        return;
      }
    }

    emit(state.copyWith(isLoading: true, message: null));

    final result = await _userRepository.downloadUserAvatar();

    result.when(
      success: (success) => emit(state.copyWith(
        avatarFilePath: success.path,
        message: null,
      )),
      failure: (failure) =>
          emit(state.copyWith(message: failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onAvatarUploaded(
    AvatarUploaded event,
    emit,
  ) async {
    emit(state.copyWith(isLoading: true, message: null));

    final file = File(event.filePath);
    final fileName = path.basename(file.path);
    final bytes = file.readAsBytesSync();
    final base64 = base64Encode(bytes);

    final result = await _userRepository.updateUser(
      fileBase64: base64,
      fileName: fileName,
    );

    await result.when(
      success: (_) async {
        // copy avatar file to dir
        final dir = await getApplicationSupportDirectory();
        final filePath = '${dir.path}/$fileName';
        final newFile = await file.copy(filePath);

        emit(state.copyWith(
          avatarFilePath: newFile.path,
          message: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        message: failure.message ?? 'Có lỗi xảy ra',
      )),
    );
  }
}
