
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'check_passcode_response.g.dart';

@JsonSerializable()
class CheckPassCodeModel {
  String verifyResultCode;
  String verifyDesc;

  CheckPassCodeModel({
    required this.verifyResultCode,
    required this.verifyDesc
  });

  factory CheckPassCodeModel.fromJson(Map<String, dynamic> json) => _$CheckPassCodeModelFromJson(json);
  Map<String, dynamic> toJson() => _$CheckPassCodeModelToJson(this);
}

@JsonSerializable()
class CheckPassCodeResponse {
  CheckPassCodeResponse({
    this.data,
    this.mess,
  });

  final CheckPassCodeModel? data;
  final MessageData? mess;

  factory CheckPassCodeResponse.fromJson(Map<String, dynamic> json) => _$CheckPassCodeResponseFromJson(json);
}