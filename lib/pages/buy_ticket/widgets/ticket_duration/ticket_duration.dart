import 'package:epass/commons/enum/method_charge.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/widgets/textfield/cupertino_picker_field.dart';
import 'package:epass/commons/widgets/textfield/date_picker_field.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/service_plan_type_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/station_stage_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_duration_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';

class TicketDuration extends StatelessWidget {
  const TicketDuration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final monthlyDataSource = _populateMonthlyDataSource();
    final quarterlyDataSource = _populateQuarterlyDataSource();

    return BlocConsumer<StationStageBloc, StationStageState>(
      listener: (context, state) {
        final methodCharge =
            state.stationStage?.methodChargeId ?? MethodChargeInt.unknown;
        final servicePlanType =
            context.read<ServicePlanTypeBloc>().state.servicePlanType;
        _stationStageAndServicePlanTypeChangedHandler(
          context,
          methodCharge,
          servicePlanType,
          monthlyDataSource.first.value,
          quarterlyDataSource.first.value,
        );
      },
      builder: (context, stationStageState) {
        final methodCharge = stationStageState.stationStage?.methodChargeId ??
            MethodChargeInt.unknown;
        return BlocConsumer<ServicePlanTypeBloc, ServicePlanTypeState>(
          listener: (context, servicePlanTypeState) {
            _stationStageAndServicePlanTypeChangedHandler(
              context,
              methodCharge,
              servicePlanTypeState.servicePlanType,
              monthlyDataSource.first.value,
              quarterlyDataSource.first.value,
            );
          },
          builder: (context, servicePlanTypeState) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (methodCharge == MethodChargeInt.normal)
                  Builder(
                    builder: (context) {
                      switch (servicePlanTypeState.servicePlanType) {
                        case ServicePlanType.monthly:
                          return CupertinoPickerField(
                            key: GlobalKey(),
                            defaultItem: monthlyDataSource.first,
                            items: monthlyDataSource,
                            onItemChanged:
                                (PickerItemWithValue<DateTime>? item) {
                              context
                                  .read<TicketDurationBloc>()
                                  .add(MonthChanged(item?.value));
                            },
                          );

                        case ServicePlanType.quarterly:
                          context.read<TicketDurationBloc>().add(
                              QuarterChanged(quarterlyDataSource.first.value));
                          return CupertinoPickerField(
                            key: GlobalKey(),
                            defaultItem: quarterlyDataSource.first,
                            items: quarterlyDataSource,
                            onItemChanged:
                                (PickerItemWithValue<DateTime>? item) {
                              context
                                  .read<TicketDurationBloc>()
                                  .add(QuarterChanged(item?.value));
                            },
                          );
                        case ServicePlanType.daily:
                        case ServicePlanType.other:
                          return const SizedBox.shrink();
                      }
                    },
                  ),
                if (methodCharge == MethodChargeInt.normal)
                  SizedBox(height: 20.h),
                BlocBuilder<TicketDurationBloc, TicketDurationState>(
                  builder: (context, state) {
                    final effectiveDate = state.effectiveDate;
                    final expireDate = state.expireDate;

                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DatePickerField(
                          isMandatory: true,
                          key: GlobalKey(),
                          enabled: methodCharge == MethodChargeInt.block,
                          labelText: 'Thời gian hiệu lực: từ ngày',
                          hintText: 'Chọn ngày bắt đầu hiệu lực',
                          defaultDate: effectiveDate,
                          hasClearButton: false,
                          onChanged: (selectedDate) {
                            final servicePlanType = context
                                .read<ServicePlanTypeBloc>()
                                .state
                                .servicePlanType;
                            context
                                .read<TicketDurationBloc>()
                                .add(EffectiveDateChanged(
                                  effectiveDate: selectedDate,
                                  servicePlanType: servicePlanType,
                                ));
                          },
                        ),
                        SizedBox(height: 16.h),
                        DatePickerField(
                          key: GlobalKey(),
                          enabled: false,
                          labelText: 'Thời gian hiệu lực: đến ngày',
                          hintText: 'Chọn ngày hết hiệu lực',
                          defaultDate: expireDate,
                          hasClearButton: false,
                        ),
                        if (expireDate != null &&
                            Jiffy().diff(expireDate, Units.DAY).abs() <= 5)
                          Padding(
                            padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 0.h),
                            child: Text(
                              'Số ngày hiệu lực còn lại ít hơn 5 ngày, vui lòng'
                              ' chọn lại khoảng thời gian mua vé phù hợp',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2!
                                  .copyWith(
                                    color: ColorName.error,
                                  ),
                            ),
                          ),
                      ],
                    );
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _stationStageAndServicePlanTypeChangedHandler(
    BuildContext context,
    MethodChargeInt methodCharge,
    ServicePlanType servicePlanType,
    DateTime startOfMonth,
    DateTime startOfQuarter,
  ) {
    if (methodCharge == MethodChargeInt.block) {
      final effectiveDate =
          context.read<TicketDurationBloc>().state.effectiveDate;
      context.read<TicketDurationBloc>().add(EffectiveDateChanged(
            servicePlanType: servicePlanType,
            effectiveDate: effectiveDate,
          ));
    } else if (methodCharge == MethodChargeInt.normal) {
      if (servicePlanType == ServicePlanType.monthly) {
        context.read<TicketDurationBloc>().add(MonthChanged(startOfMonth));
      } else if (servicePlanType == ServicePlanType.quarterly) {
        context.read<TicketDurationBloc>().add(QuarterChanged(startOfQuarter));
      }
    }
  }

  List<PickerItemWithValue<DateTime>> _populateMonthlyDataSource() {
    var items = <PickerItemWithValue<DateTime>>[];
    final startOfThisMonth = Jiffy().startOf(Units.MONTH);
    for (var i = 0; i < 12; ++i) {
      final startOfMonth = Jiffy(startOfThisMonth).add(months: i).dateTime;
      final title = 'Tháng ${startOfMonth.month}/${startOfMonth.year}';
      items.add(PickerItemWithValue(title: title, value: startOfMonth));
    }
    return items;
  }

  List<PickerItemWithValue<DateTime>> _populateQuarterlyDataSource() {
    var items = <PickerItemWithValue<DateTime>>[];
    final now = Jiffy();
    final currentQuarter = (now.month / 3).ceil();
    final monthStartOfQuarter = (currentQuarter - 1) * 3 + 1;
    final startOfCurrentQuarter = Jiffy(now)
        .subtract(months: now.month - monthStartOfQuarter)
        .startOf(Units.MONTH)
        .dateTime;

    for (var i = 0; i < 4; ++i) {
      final quarter = currentQuarter + i;
      final startOfMonth =
          Jiffy(startOfCurrentQuarter).add(months: i * 3).dateTime;
      final title = 'Quý $quarter/${startOfMonth.year}';
      items.add(PickerItemWithValue(title: title, value: startOfMonth));
    }
    return items;
  }
}
