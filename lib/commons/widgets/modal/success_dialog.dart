import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';

class SuccessDialog extends StatelessWidget {
  final String title;
  final String content;
  final TextAlign contentTextAlign;
  final String buttonTitle;
  final VoidCallback? onButtonTap;

  const SuccessDialog({
    Key? key,
    this.title = 'Thành công!',
    required this.content,
    this.contentTextAlign = TextAlign.start,
    this.buttonTitle = 'Xong',
    this.onButtonTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.w,
            // vertical: 16.h,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Stack(
            children: [
              Positioned(
                top: -48.h,
                left: 0,
                right: 0,
                child: Center(
                  child: Lottie.asset('assets/lottie/success.json',
                      repeat: false,
                      fit: BoxFit.cover,
                      height: 300.r,
                      width: 300.r),
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 200.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ),
                  SizedBox(height: 12.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Text(
                      content,
                      textAlign: contentTextAlign,
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  SizedBox(height: 28.h),
                  SizedBox(
                    height: 50.h,
                    width: double.infinity,
                    child: SecondaryButton(
                      onTap: onButtonTap ?? Navigator.of(context).pop,
                      title: buttonTitle,
                    ),
                  ),
                  SizedBox(height: 20.h),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
