# Epass

Epass Flutter project.

## How to build project

1. Check dart version: flutter doctor -v. If dart version < 2.17.0 then need update
   1.1 Update dart version, run following command:
- flutter channel stable // master
- flutter upgrade
2. Get dependencies
- flutter clean
- flutter pub get
3. Generate config files
- flutter pub run build_runner build --delete-conflicting-outputs watch
- ```
  class SvgGenImage {
  const SvgGenImage(this._assetName);
  final String _assetName;
  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    Color? color,
    BlendMode colorBlendMode = BlendMode.srcIn,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme? theme,
    Clip clipBehavior = Clip.hardEdge,
    bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      color: color,
      colorBlendMode: colorBlendMode,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }
  String get path => _assetName;
  String get keyName => _assetName;
}
```
4. Run project
- flutter run --flavor stg -t lib/main-stg.dart
5. Build project
Android
- Android/build.gradle: ext.version_name = '1.4.25' && ext.version_code = 101
- pubspec.yaml: '1.4.25+101'
- flutter build apk --flavor prod -t lib/main-prod.dart --release
- flutter build appbundle --flavor prod -t lib/main-prod.dart --release
iOS
- iOS/Runner.xcodeproj/project.pbxproj: FLUTTER_BUILD_NAME = 2.4.8; && FLUTTER_BUILD_NUMBER = 5; || (Chú ý: PRODUCT_BUNDLE_IDENTIFIER)
- flutter build ios --flavor prod -t lib/main-prod.dart --release
- Pods-Runner-frameworks.sh: source="$(readlink -f "${source}")"
