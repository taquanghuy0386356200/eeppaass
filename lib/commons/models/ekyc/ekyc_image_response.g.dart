// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ekyc_image_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EkycImageResponse _$EkycImageResponseFromJson(Map<String, dynamic> json) =>
    EkycImageResponse(
      code: json['code'] as String?,
      message: json['message'] as String?,
      base64: json['base64'] as String?,
      signature: json['signature'] as String?,
      request_time: json['request_time'] as String?,
      response_time: json['response_time'] as String?,
      request_id: json['request_id'] as String?,
    );

Map<String, dynamic> _$EkycImageResponseToJson(EkycImageResponse instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'base64': instance.base64,
      'signature': instance.signature,
      'request_time': instance.request_time,
      'response_time': instance.response_time,
      'request_id': instance.request_id,
    };
