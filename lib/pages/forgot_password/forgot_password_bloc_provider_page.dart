import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_form_bloc.dart';
import 'package:epass/pages/forgot_password/pages/forgot_password_new_password/bloc/forgot_password_new_password_form_bloc.dart';
import 'package:epass/pages/forgot_password/pages/forgot_password_otp/bloc/forgot_password_otp_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ForgotPasswordBlocProviderPage extends StatelessWidget {
  const ForgotPasswordBlocProviderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ForgotPasswordBloc(
            userRepository: getIt<IUserRepository>(),
          ),
        ),
        BlocProvider(create: (context) => ForgotPasswordFormBloc()),
        BlocProvider(create: (context) => ForgotPasswordNewPasswordFormBloc()),
        BlocProvider(
          create: (context) => ForgotPasswordOtpBloc(
            userRepository: getIt<IUserRepository>(),
          ),
        ),
      ],
      child: const AutoRouter(),
    );
  }
}
