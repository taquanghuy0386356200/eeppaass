import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';


part 'vehicle_debit.g.dart';

@JsonSerializable()
class VehicleDebitResponse {
  final VehicleDebitData? data;
  final MessageData? mess;

  const VehicleDebitResponse({
    this.data,
    this.mess,
  });

  factory VehicleDebitResponse.fromJson(Map<String, dynamic> json) =>
      _$VehicleDebitResponseFromJson(json);
}

@JsonSerializable()
class VehicleDebitData {
  final int? statusDebit;
  final  String? debitStatusName;

  const VehicleDebitData({
    this.statusDebit,
    this.debitStatusName,
  });

  factory VehicleDebitData.fromJson(Map<String, dynamic> json) =>
      _$VehicleDebitDataFromJson(json);
}
@JsonSerializable()
class VehicleDebitTransactionResponse {
  final MessageData? mess;

  const VehicleDebitTransactionResponse({
    this.mess,
  });

  factory VehicleDebitTransactionResponse.fromJson(Map<String, dynamic> json) =>
      _$VehicleDebitTransactionResponseFromJson(json);
}
