part of 'change_password_form_bloc.dart';

abstract class ChangePasswordFormEvent extends Equatable {
  const ChangePasswordFormEvent();
}

class ChangePasswordFormSubmitted extends ChangePasswordFormEvent {
  final String oldPassword;
  final String newPassword;

  const ChangePasswordFormSubmitted({
    required this.oldPassword,
    required this.newPassword,
  });

  @override
  List<Object> get props => [oldPassword, newPassword];
}
