import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'alias_form_event.dart';

part 'alias_form_state.dart';

class AliasFormBloc extends Bloc<AliasFormEvent, AliasFormState> {
  final IUserRepository _userRepository;
  final AppBloc _appBloc;

  AliasFormBloc({
    required IUserRepository userRepository,
    required AppBloc appBloc,
  })  : _userRepository = userRepository,
        _appBloc = appBloc,
        super(AliasFormInitial()) {
    on<AliasFormSubmitted>(_onAliasFormSubmitted);
  }

  FutureOr<void> _onAliasFormSubmitted(event, emit) async {
    emit(const AliasFormSubmittedInProgress());

    final result = await _userRepository.updateAlias(alias: event.alias);

    await result.when(
      success: (_) async {
        // Get user details (contract)
        final getUserResult = await _userRepository.getUser();

        getUserResult.when(
          success: (user) {
            // Update [AppBloc] user info
            _appBloc.add(UpdateAppUser(user));

            // Emit authentication success state
            emit(const AliasFormSubmittedSuccess());
          },
          failure: (failure) => emit(
              AliasFormSubmittedFailure(failure.message ?? 'Có lỗi xảy ra')),
        );
      },
      failure: (failure) =>
          emit(AliasFormSubmittedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
