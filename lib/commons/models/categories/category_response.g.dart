// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryModel _$CategoryModelFromJson(Map<String, dynamic> json) =>
    CategoryModel(
      id: json['id'],
      tableName: json['table_name'] as String?,
      name: json['name'] as String?,
      code: json['code'] as String?,
      isDefault: json['is_default'] as String?,
      description: json['description'] as String?,
      isActive: json['is_active'] as String?,
    );

Map<String, dynamic> _$CategoryModelToJson(CategoryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'table_name': instance.tableName,
      'name': instance.name,
      'code': instance.code,
      'is_default': instance.isDefault,
      'description': instance.description,
      'is_active': instance.isActive,
    };

CategoryModelData _$CategoryModelDataFromJson(Map<String, dynamic> json) =>
    CategoryModelData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => CategoryModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$CategoryModelDataToJson(CategoryModelData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

CategoryModelResponse _$CategoryModelResponseFromJson(
        Map<String, dynamic> json) =>
    CategoryModelResponse(
      data: json['data'] == null
          ? null
          : CategoryModelData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CategoryModelResponseToJson(
        CategoryModelResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
