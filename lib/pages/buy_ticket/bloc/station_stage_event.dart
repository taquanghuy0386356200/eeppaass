part of 'station_stage_bloc.dart';

abstract class StationStageEvent extends Equatable {
  const StationStageEvent();
}

class StationStageChanged extends StationStageEvent {
  final StationStage? stationStage;

  const StationStageChanged({this.stationStage});

  @override
  List<Object?> get props => [stationStage];
}
