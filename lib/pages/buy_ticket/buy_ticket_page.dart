import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/footer_container.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/service_plan_type_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/station_stage_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_duration_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_amount.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_button.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_notices.dart';
import 'package:epass/pages/buy_ticket/widgets/service_plan_type_radio/service_plan_type_radio.dart';
import 'package:epass/pages/buy_ticket/widgets/station_stage_dropdown/station_stage_dropdown.dart';
import 'package:epass/pages/buy_ticket/widgets/ticket_duration/ticket_duration.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/vehicle_select_button.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:epass/commons/routes/router.gr.dart';
import 'package:rxdart/rxdart.dart';

class BuyTicketPage extends StatefulWidget {
  const BuyTicketPage({Key? key}) : super(key: key);

  @override
  State<BuyTicketPage> createState() => _BuyTicketPageState();
}

class _BuyTicketPageState extends State<BuyTicketPage> {
  bool _firstRun = true;
  bool canShowPickDate = false;
  final BehaviorSubject<bool> showPickDateBHVSJ = BehaviorSubject.seeded(false);

  @override
  void initState() {
    super.initState();
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));
    context.read<BalanceBloc>().add(BalanceFetched());
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  void dispose() {
    showPickDateBHVSJ.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<TicketVehicleBloc, TicketVehicleState>(
          // listenWhen: (previous, current) => _validateTicketRequest(context),
          listener: (context, state) => _fetchTicketPrice(context),
        ),
        BlocListener<StationStageBloc, StationStageState>(
          // listenWhen: (previous, current) => _validateTicketRequest(context),
          listener: (context, state) => _fetchTicketPrice(context),
        ),
        BlocListener<ServicePlanTypeBloc, ServicePlanTypeState>(
          // listenWhen: (previous, current) => _validateTicketRequest(context),
          listener: (context, state) => _fetchTicketPrice(context),
        ),
        BlocListener<TicketDurationBloc, TicketDurationState>(
          // listenWhen: (previous, current) => _validateTicketRequest(context),
          listener: (context, state) => _fetchTicketPrice(context),
        ),
      ],
      child: BasePage(
        title: 'Mua vé tháng/quý',
        leading: BackButton(
          onPressed: () {
            context
                .read<HomeTabsBloc>()
                .add(const HomeTabBarRequestHidden(isHidden: false));
            context.popRoute();
          },
        ),
        backgroundColor: Colors.white,
        child: Stack(
          children: [
            FadeAnimation(
              delay: 0.5,
              playAnimation: _firstRun,
              child: const GradientHeaderContainer(),
            ),
            SafeArea(
              child: FadeAnimation(
                delay: 1,
                playAnimation: _firstRun,
                direction: FadeDirection.up,
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: RoundedTopContainer(
                    margin: EdgeInsets.only(top: 16.h),
                    padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        VehicleSelectButton(
                          onTap: () =>
                              context.pushRoute(const VehicleSelectTabRoute()),
                        ),
                        SizedBox(height: 16.h),
                        StationStageDropdown(
                          haveChoose: (value) {
                            showPickDateBHVSJ.sink.add(value);
                          },
                        ),
                        SizedBox(height: 20.h),
                        Padding(
                          padding: EdgeInsets.only(left: 4.w),
                          child: Text(
                            'Loại vé',
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                          ),
                        ),
                        SizedBox(height: 8.h),
                        const ServicePlanTypeRadio(),
                        SizedBox(height: 20.h),
                        StreamBuilder<bool>(
                            initialData: showPickDateBHVSJ.valueOrNull,
                            stream: showPickDateBHVSJ.stream,
                            builder: (context, snapshot) {
                              final data = snapshot.data ?? false;
                              return Visibility(
                                visible: data,
                                child: BlocListener<StationStageBloc,
                                    StationStageState>(
                                  listener: (context, state) {
                                    context
                                        .read<TicketDurationBloc>()
                                        .add(const TicketDurationReset());
                                  },
                                  child: const TicketDuration(),
                                ),
                              );
                            }),
                        SizedBox(height: 24.h),
                        const BuyTicketNotices(),
                        SizedBox(height: 100.h),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: FooterContainer(
                height: 100.h,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  // mainAxisAlignment:
                  //     MainAxisAlignment.spaceBetween,
                  children: const [
                    BuyTicketAmount(),
                    Spacer(),
                    Expanded(
                      flex: 3,
                      child: BuyTicketButton(),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _fetchTicketPrice(BuildContext context) {
    final userVehicles = context.read<TicketVehicleBloc>().state.userVehicles;
    final otherVehicles = context.read<TicketVehicleBloc>().state.otherVehicles;
    final vehicles = [...userVehicles, ...otherVehicles];

    final stationStage = context.read<StationStageBloc>().state.stationStage;

    final servicePlanType =
        context.read<ServicePlanTypeBloc>().state.servicePlanType;
    final effectiveDate =
        context.read<TicketDurationBloc>().state.effectiveDate;
    final expireDate = context.read<TicketDurationBloc>().state.expireDate;

    if (stationStage != null &&
        effectiveDate != null &&
        expireDate != null &&
        vehicles.isNotEmpty) {
      context.read<TicketPriceBloc>().add(TicketPriceFetched(
            vehicles: vehicles,
            servicePlanType: servicePlanType,
            stationStage: stationStage,
            effectiveDate: effectiveDate,
            expireDate: expireDate,
          ));
    } else {
      context.read<TicketPriceBloc>().add(TicketPriceInValid());
    }
  }

  // bool _validateTicketRequest(BuildContext context) {
  //   final stationStage = context.read<StationStageBloc>().state.stationStage;
  //   final effectiveDate =
  //       context.read<TicketDurationBloc>().state.effectiveDate;
  //   final expireDate = context.read<TicketDurationBloc>().state.expireDate;
  //   final userVehicles = context.read<TicketVehicleBloc>().state.userVehicles;
  //   final otherVehicles = context.read<TicketVehicleBloc>().state.otherVehicles;
  //
  //   return stationStage != null &&
  //       effectiveDate != null &&
  //       expireDate != null &&
  //       (userVehicles.isNotEmpty || otherVehicles.isNotEmpty);
  // }
}
