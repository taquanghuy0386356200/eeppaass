import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/failures/local_auth_failures.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:simple_result/simple_result.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

enum LocalAuthenticationType { face, fingerprint }

abstract class ILocalAuthService {
  Future<Result<LocalAuthenticationType, Failure>> getAvailableAuthMethod();

  Future<Result<bool, Failure>> authenticate();

  Future<bool> get canCheckBiometric;
}

class LocalAuthService extends ILocalAuthService {
  final LocalAuthentication _localAuth;

  // TODO: i18n
  // final _iosStrings = const IOSAuthMessages(
  //   cancelButton: 'Huỷ',
  //   goToSettingsButton: 'Cài đặt',
  //   goToSettingsDescription: 'Vui lòng cài đặt xác thực sinh trắc học',
  //   lockOut: 'Vui lòng kích hoạt lại xác thực sinh trắc học',
  // );
  //
  // final _androidStrings = const AndroidAuthMessages(
  //   cancelButton: 'Huỷ',
  //   goToSettingsButton: 'Cài đặt',
  //   goToSettingsDescription: 'Vui lòng cài đặt xác thực sinh trắc học',
  //
  // )

  LocalAuthService({
    required LocalAuthentication localAuth,
  }) : _localAuth = localAuth;

  @override
  Future<Result<bool, Failure>> authenticate() async {
    try {
      final checkable = await canCheckBiometric;
      if (checkable) {
        final didAuthenticate = await _localAuth.authenticate(
          localizedReason: 'Vui lòng xác thực để đăng nhập',
        );
        if (didAuthenticate) {
          return const Result.success(true);
        } else {
          return const Result.failure((UndefinedMethodFailure()));
        }
      } else {
        return const Result.failure(BiometricNotAvailableFailure());
      }
    } on PlatformException catch (e) {
      switch (e.code) {
        case auth_error.lockedOut:
          return const Result.failure(BiometricLockedOutFailure());
        case auth_error.permanentlyLockedOut:
          return const Result.failure(BiometricPermanentlyLockedOutFailure());
        case auth_error.notAvailable:
          return const Result.failure(BiometricNotAvailableFailure());
        case auth_error.notEnrolled:
          return const Result.failure(NotEnrolledFailure());
        case auth_error.passcodeNotSet:
          return const Result.failure(PasscodeNotSetFailure());
        case auth_error.otherOperatingSystem:
        default:
          return const Result.failure((UndefinedMethodFailure()));
      }
    }
  }

  @override
  Future<Result<LocalAuthenticationType, Failure>>
      getAvailableAuthMethod() async {
    final checkable = await canCheckBiometric;
    if (checkable) {
      final availableBiometrics = await _localAuth.getAvailableBiometrics();
      if (availableBiometrics.contains(BiometricType.face)) {
        return const Result.success(LocalAuthenticationType.face);
      } else if (availableBiometrics.contains(BiometricType.fingerprint)) {
        return const Result.success(LocalAuthenticationType.fingerprint);
      } else {
        return const Result.failure(UndefinedMethodFailure());
      }
    } else {
      return const Result.failure(BiometricNotAvailableFailure());
    }
  }

  @override
  Future<bool> get canCheckBiometric async {
    final result = await _localAuth.canCheckBiometrics;
    return result;
  }
}
