// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bidv_create_order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BidvCreateOrderRequest _$BidvCreateOrderRequestFromJson(
        Map<String, dynamic> json) =>
    BidvCreateOrderRequest(
      data: json['data'] as String,
    );

Map<String, dynamic> _$BidvCreateOrderRequestToJson(
        BidvCreateOrderRequest instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

BidvCreateOrderResponse _$BidvCreateOrderResponseFromJson(
        Map<String, dynamic> json) =>
    BidvCreateOrderResponse(
      data: BIDVCreateOrderData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BidvCreateOrderResponseToJson(
        BidvCreateOrderResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

BIDVCreateOrderData _$BIDVCreateOrderDataFromJson(Map<String, dynamic> json) =>
    BIDVCreateOrderData(
      transId: json['transId'] as String,
      responseCode: json['responseCode'] as String,
    );

Map<String, dynamic> _$BIDVCreateOrderDataToJson(
        BIDVCreateOrderData instance) =>
    <String, dynamic>{
      'transId': instance.transId,
      'responseCode': instance.responseCode,
    };
