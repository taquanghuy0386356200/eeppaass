import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'forgot_password_new_password_form_event.dart';

part 'forgot_password_new_password_form_state.dart';

class ForgotPasswordNewPasswordFormBloc extends Bloc<
    ForgotPasswordNewPasswordFormEvent, ForgotPasswordNewPasswordFormState> {
  ForgotPasswordNewPasswordFormBloc()
      : super(const ForgotPasswordNewPasswordFormState()) {
    on<ForgotPasswordNewPasswordFormCompleted>(
        _onForgotPasswordNewPasswordFormCompleted);
  }

  FutureOr<void> _onForgotPasswordNewPasswordFormCompleted(
    ForgotPasswordNewPasswordFormCompleted event,
    Emitter<ForgotPasswordNewPasswordFormState> emit,
  ) {
    emit(ForgotPasswordNewPasswordFormState(newPassword: event.newPassword));
  }
}
