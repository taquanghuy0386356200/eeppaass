// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_attachment_file.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportAttachmentFileResponse _$ReportAttachmentFileResponseFromJson(
        Map<String, dynamic> json) =>
    ReportAttachmentFileResponse(
      fileName: json['fileName'] as String?,
      attachmentId: json['attachmentId'] as int?,
    );

Map<String, dynamic> _$ReportAttachmentFileResponseToJson(
        ReportAttachmentFileResponse instance) =>
    <String, dynamic>{
      'fileName': instance.fileName,
      'attachmentId': instance.attachmentId,
    };

ReportAttachmentFileRequest _$ReportAttachmentFileRequestFromJson(
        Map<String, dynamic> json) =>
    ReportAttachmentFileRequest(
      fileName: json['fileName'] as String,
      base64Data: json['base64Data'] as String,
      fileSize: (json['fileSize'] as num).toDouble(),
    );

Map<String, dynamic> _$ReportAttachmentFileRequestToJson(
        ReportAttachmentFileRequest instance) =>
    <String, dynamic>{
      'fileName': instance.fileName,
      'base64Data': instance.base64Data,
      'fileSize': instance.fileSize,
    };
