import 'package:epass/commons/models/station/station_stage.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/place/pages/bot_places/ext/station_stage_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher_string.dart';

class BotPlaceCard extends StatelessWidget {
  final StationStage station;

  const BotPlaceCard({Key? key, required this.station}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final googleMapAppURL = station.getGoogleMapUrl(openApp: true);
    final googleMapWebURL = station.getGoogleMapUrl(openApp: false);

    return Padding(
      padding: EdgeInsets.fromLTRB(36.w, 18.h, 36.w, 16.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            station.name ?? '',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          SizedBox(height: 4.h),
          if (station.address?.isNotEmpty ?? false) SizedBox(height: 6.h),
          if (station.address?.isNotEmpty ?? false)
            Text(
              'Địa chỉ: ${station.address}',
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: ColorName.borderColor),
            ),
          if (station.addressIn?.isNotEmpty ?? false) SizedBox(height: 6.h),
          if (station.addressIn?.isNotEmpty ?? false)
            Text(
              'Trạm vào: ${station.addressIn}',
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: ColorName.borderColor),
            ),
          if (station.addressOut?.isNotEmpty ?? false) SizedBox(height: 6.h),
          if (station.addressOut?.isNotEmpty ?? false)
            Text(
              'Trạm ra: ${station.addressOut}',
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: ColorName.borderColor),
            ),
          if (googleMapAppURL != null || googleMapWebURL != null)
            SizedBox(height: 10.h),
          if (googleMapAppURL != null || googleMapWebURL != null)
            Row(
              children: [
                const Spacer(),
                Material(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(12.0),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(12.0),
                    onTap: () async {
                      if (googleMapAppURL != null &&
                          await canLaunchUrlString(googleMapAppURL)) {
                        await launchUrlString(googleMapAppURL);
                      } else if (googleMapWebURL != null &&
                          await canLaunchUrlString(googleMapWebURL)) {
                        await launchUrlString(googleMapWebURL);
                      }
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 12.w, vertical: 6.h),
                      child: Row(
                        children: [
                          Assets.icons.mapAlt.svg(color: ColorName.linkBlue),
                          SizedBox(width: 8.w),
                          Text(
                            'Xem trên bản đồ',
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      color: ColorName.linkBlue,
                                    ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }
}
