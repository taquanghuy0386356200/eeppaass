import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/extensions/image_extension.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/list_button.dart';
import 'package:epass/commons/widgets/modal/neutral_confirm_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/setting/setting_entrance/bloc/setting_page_bloc.dart';
import 'package:epass/pages/setting/setting_entrance/widgets/biometric_login_setting.dart';
import 'package:epass/pages/setting/setting_entrance/widgets/profile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class SettingList extends StatefulWidget {
  const SettingList({
    Key? key,
  }) : super(key: key);

  @override
  State<SettingList> createState() => _SettingListState();
}

class _SettingListState extends State<SettingList> {
  @override
  Widget build(BuildContext context) {
    final listButtons = [
      const ProfileCard(),
      ListButton(
        leading: Assets.icons.vehicle.svg(width: 20.r, height: 20.r),
        title: 'Danh sách xe',
        onTap: () async {
          await context.pushRoute(const VehicleRouter());
          context
              .read<HomeTabsBloc>()
              .add(const HomeTabBarRequestHidden(isHidden: false));
        },
      ),
      ListButton(
        leading: Assets.icons.tabbar.notiActive.svg(width: 20.r, height: 20.r),
        title: 'Cài đặt thông báo',
        onTap: () => context.pushRoute(const NotificationSettingRoute()),
      ),
      const BiometricLoginSetting(),
      ListButton(
        leading: Assets.icons.alias.svg(width: 20.r, height: 20.r),
        title: 'Bí danh đăng nhập',
        onTap: () => context.pushRoute(const AliasRoute()),
      ),
      // ListButton(
      //   leading: Assets.icons.hdrAuto.svg(width: 20.r, height: 20.r),
      //   title: 'Quản lý nạp tiền tự động',
      //   // body: RichText(
      //   //   text: TextSpan(
      //   //     style: DefaultTextStyle.of(context).style,
      //   //     children: const <TextSpan>[
      //   //       TextSpan(
      //   //           text: 'Quản lý nạp tiền tự động\n',
      //   //           style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
      //   //       TextSpan(
      //   //           text: 'Nạp tự động 100.000đ khi số dư dưới 20.000đ',
      //   //           style: TextStyle(fontSize: 12, color: Colors.black54)),
      //   //     ],
      //   //   ),
      //   // ),
      //   onTap: () => context.pushRoute(const AutoTopupRouter()),
      // ),
      ListButton(
        leading: Assets.icons.lock.svg(width: 20.r, height: 20.r),
        title: 'Đổi mật khẩu',
        onTap: () => context.pushRoute(const ChangePasswordRoute()),
      ),
      ListButton(
        leading: Assets.icons.info.svg(width: 20.r, height: 20.r),
        title: 'Thông tin ứng dụng',
        onTap: () => context.pushRoute(const AppInfoRoute()),
      ),
      ListButton(
        leading: Assets.icons.logout.svg(width: 18.r, height: 18.r),
        trailing: const SizedBox.shrink(),
        title: 'Đăng xuất',
        onTap: _onLogoutTap,
      ),
    ];

    return MultiBlocListener(
      listeners: [
        BlocListener<SettingPageBloc, SettingPageState>(
          listener: (context, state) {
            if (state is SettingPageLoading) {
              context.loaderOverlay.show();
            } else if (state is SettingPageLogoutSuccess ||
                state is SettingPageLogoutFailure) {
              context.loaderOverlay.hide();
              context.read<AppBloc>().add(UserLogout());
              context.replaceRoute(const LoginRoute());
            }
            // else if (state is SettingPageLogoutFailure) {
            //   context.loaderOverlay.hide();
            //   showErrorSnackBBar(context: context, message: state.message);
            // }
          },
        ),
        BlocListener<SettingBloc, SettingState>(
          listenWhen: (previous, current) =>
              previous.isBiometricLoginEnabled == false &&
              current.isBiometricLoginEnabled == true,
          listener: (context, state) {
            showSuccessSnackBBar(
              message: 'Cài đặt chức năng Đăng nhập nhanh thành công',
              context: context,
              duration: const Duration(seconds: 5),
            );
          },
        ),
      ],
      child: Stack(
        fit: StackFit.loose,
        children: [
          Positioned.fill(
            top: 132.h,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(24.0),
                  topRight: Radius.circular(24.0),
                ),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    blurRadius: 10,
                    offset: const Offset(0, -10),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 32.h),
            child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: listButtons.length,
              itemBuilder: (context, index) => listButtons[index],
              separatorBuilder: (BuildContext context, int index) =>
                  SizedBox(height: 20.h),
            ),
          ),
        ],
      ),
    );
  }

  void _onLogoutTap() {
    showDialog(
      context: context,
      builder: (dialogContext) {
        return BlocProvider.value(
          value: context.read<SettingPageBloc>(),
          child: NeutralConfirmDialog(
            title: 'Xác nhận đăng xuất',
            content:
                'Sau khi đăng xuất, bạn sẽ quay trở lại màn hình đăng nhập',
            contentTextAlign: TextAlign.center,
            secondaryButtonTitle: 'Huỷ',
            primaryButtonTitle: 'Đăng xuất',
            onPrimaryButtonTap: () {
              Navigator.of(dialogContext).pop();
              context.read<SettingPageBloc>().add(SettingPageLogout());
            },
          ),
        );
      },
    );
  }
}
