part of 'viettel_money_link_bloc.dart';

abstract class ViettelMoneyLinkState extends Equatable {
  const ViettelMoneyLinkState();
}

class ViettelMoneyLinkInitial extends ViettelMoneyLinkState {
  @override
  List<Object> get props => [];
}

class ViettelMoneyLinkAccountInProgress extends ViettelMoneyLinkState {
  @override
  List<Object> get props => [];
}

class ViettelMoneyLinkAccountFailure extends ViettelMoneyLinkState {
  final String message;

  const ViettelMoneyLinkAccountFailure(this.message);

  @override
  List<Object> get props => [message];
}

class ViettelMoneyLinkAccountSuccess extends ViettelMoneyLinkState {
  @override
  List<Object> get props => [];
}

class ViettelMoneyUnlinkAccountInProgress extends ViettelMoneyLinkState {
  @override
  List<Object> get props => [];
}

class ViettelMoneyUnlinkAccountFailure extends ViettelMoneyLinkState {
  final String message;

  const ViettelMoneyUnlinkAccountFailure(this.message);

  @override
  List<Object> get props => [message];
}

class ViettelMoneyUnlinkAccountSuccess extends ViettelMoneyLinkState {
  @override
  List<Object> get props => [];
}
