import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/setting/notification_setting/bloc/balance_alert_bloc/balance_alert_bloc.dart';
import 'package:flutter/material.dart';
import 'package:epass/commons/extensions/number_ext.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BalanceAlertDialog extends StatefulWidget {
  final int? amount;

  const BalanceAlertDialog({Key? key, this.amount}) : super(key: key);

  @override
  State<BalanceAlertDialog> createState() => _BalanceAlertDialogState();
}

class _BalanceAlertDialogState extends State<BalanceAlertDialog> {
  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: widget.amount?.formatCurrency());
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 16.w),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 32.h),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 12.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Text(
                  'Vui lòng nhập\n hạn mức số dư tối thiểu',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: ColorName.textGray2,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 24.h),
              PrimaryTextField(
                controller: _controller,
                labelText: 'Hạn mức số dư tối thiểu',
                hintText: 'Nhập hạn mức',
                maxLength: 13,
                inputType: TextInputType.number,
                isCurrency: true,
                suffix: const Text('₫'),
              ),
              SizedBox(height: 24.h),
              Row(
                children: [
                  Expanded(
                    child: SizedBox(
                      height: 50.h,
                      child: SecondaryButton(
                        onTap: () => Navigator.of(context).pop(),
                      ),
                    ),
                  ),
                  SizedBox(width: 20.w),
                  Expanded(
                    child: SizedBox(
                      height: 50.h,
                      child: PrimaryButton(
                        onTap: () {
                          final value = int.tryParse(_controller.text.replaceAll('.', ''));
                          context.read<BalanceAlertBloc>().add(BalanceAlertUpdated(
                            isBalanceAlertOn: true,
                            alertAmount: value,
                          ));
                          Navigator.of(context).pop();
                        },
                        title: 'Đồng ý',
                        padding: EdgeInsets.zero,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}