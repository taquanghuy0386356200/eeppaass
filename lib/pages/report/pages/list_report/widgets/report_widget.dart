import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/attachment_file_bloc/attachment_file_bloc.dart';
import 'package:epass/pages/report/pages/list_report/widgets/report_item_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

import 'attachment_files_widget.dart';

class FeedbackItem {
  final FeedbackModel feedback;

  FeedbackItem({
    required this.feedback,
  });
}

class ReportWidget extends StatefulWidget {
  final FeedbackItem feedbackItem;

  const ReportWidget({
    Key? key,
    required this.feedbackItem,
  }) : super(key: key);

  @override
  State<ReportWidget> createState() => _ReportWidgetState();
}

class _ReportWidgetState extends State<ReportWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final feedback = widget.feedbackItem.feedback;
    return Column(
      children: [
        ReportItemWidget(
          title: 'Nhóm phản ánh',
          content: feedback.l1TicketTypeName!,
        ),
        SizedBox(height: 16.h),
        ReportItemWidget(
          title: 'Thể loại',
          content: feedback.l2TicketTypeName!,
        ),
        SizedBox(height: 16.h),
        if (feedback.l3TicketTypeName?.isNotEmpty ?? false)
          ReportItemWidget(
            title: 'Loại phản ánh',
            content: feedback.l3TicketTypeName!,
          ),
        if (feedback.l3TicketTypeName?.isNotEmpty ?? false) SizedBox(height: 16.h),
        ReportItemWidget(
          title: 'Ngày phản ánh',
          content: feedback.createDate ?? '',
        ),
        SizedBox(height: 16.h),
        ReportItemWidget(
          title: 'Nội dung phản ánh',
          content: feedback.contentReceive ?? '',
        ),
        if (feedback.attachmentFiles?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (feedback.attachmentFiles?.isNotEmpty ?? false)
          AttachmentFilesWidget(
            attachmentFiles: feedback.attachmentFiles ?? [],
            onAttachmentTap: (String fileName, int attachmentFileId) {
              context.read<AttachmentFileBloc>().add(AttachmentFileDownloaded(
                fileName: fileName,
                attachmentFileId: attachmentFileId,
              ));
            },
          ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8.h),
          child: const Divider(
            thickness: 0.5,
            color: ColorName.borderColor
          ),
        ),
        ReportItemWidget(
          title: 'Trạng thái xử lý',
          content: feedback.statusName!,
          contentColor: ColorName.success,
        ),
        if (feedback.processTime?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (feedback.processTime?.isNotEmpty ?? false)
          ReportItemWidget(
            title: 'Thời gian xử lý',
            content: feedback.processTime ?? '',
          ),
        if (feedback.processTime?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (feedback.staffName?.isNotEmpty ?? false)
          ReportItemWidget(
            title: 'Người xử lý',
            content: feedback.staffName ?? '',
          ),
        if (feedback.staffName?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (feedback.processContent?.isNotEmpty ?? false)
          ReportItemWidget(
            title: 'Nội dung xử lý',
            content: feedback.processContent ?? '',
          ),
      ],
    );
  }
}
