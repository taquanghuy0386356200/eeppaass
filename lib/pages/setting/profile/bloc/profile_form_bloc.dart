import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'profile_form_event.dart';

part 'profile_form_state.dart';

class ProfileFormBloc extends Bloc<ProfileFormEvent, ProfileFormState> {
  final IUserRepository _userRepository;
  final AppBloc _appBloc;

  ProfileFormBloc({
    required IUserRepository userRepository,
    required AppBloc appBloc,
  })  : _userRepository = userRepository,
        _appBloc = appBloc,
        super(ProfileFormInitial()) {
    on<ProfileFormSubmitted>(_onProfileFormSubmitted);
  }

  FutureOr<void> _onProfileFormSubmitted(event, emit) async {
    emit(const ProfileFormSubmittedInProgress());

    final result = await _userRepository.updateUser(
      email: event.email,
      phone: event.phone,
    );

    await result.when(
      success: (success) async {
        // Get updated user from API
        final getUserResult = await _userRepository.getUser();

        getUserResult.when(
          success: (user) {
            // Update [AppBloc] user info
            _appBloc.add(UpdateAppUser(user));

            // Emit authentication success state
            emit(const ProfileFormSubmittedSuccess());
          },
          failure: (failure) => emit(
              ProfileFormSubmittedFailure(failure.message ?? 'Có lỗi xảy ra')),
        );
      },
      failure: (failure) =>
          emit(ProfileFormSubmittedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
