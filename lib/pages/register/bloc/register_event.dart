part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class RegisterSubmitted extends RegisterEvent {
  final RegisterRequest request;

  const RegisterSubmitted(this.request);

  @override
  List<Object> get props => [request];
}
