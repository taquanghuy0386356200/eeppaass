extension StringExtension on String {
  String get escapeQuotes {
    return replaceAll('"', '\\"');
  }
}
