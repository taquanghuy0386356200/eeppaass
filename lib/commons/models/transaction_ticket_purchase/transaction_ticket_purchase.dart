/// stationId : 960
/// stageId : 100
/// stage : ""
/// station : "Cầu Phú Mỹ"
/// servicePlanTypeId : 4
/// servicePlanTypeName : "Vé tháng"
/// price : 450000
/// status : 2
/// saleTransDate : "2021-12-29T14:17:51+07:00"
/// dateOfUse : "01/01/2022 00:00:00"
/// effDate : "01/01/2022 00:00:00"
/// expDate : "31/01/2022 23:59:59"
/// autoRenew : "0"
/// efficiency : 1
/// plateNumber : "21A12345"
/// chargeMethodName : ""
/// saleTransCode : "TQ1001022.20222912141748"
/// methodCharge : "Cách tính thường"
/// createUser : "V001001022"
/// vehicleGroupName : "Loại 1"
/// vehicleGroupId : 1

import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'transaction_ticket_purchase.g.dart';

@JsonSerializable()
class TransactionTicketPurchase {
  TransactionTicketPurchase({
    this.stationId,
    this.stageId,
    this.stage,
    this.station,
    this.servicePlanTypeId,
    this.servicePlanTypeName,
    this.price,
    this.status,
    this.saleTransDate,
    this.dateOfUse,
    this.effDate,
    this.expDate,
    this.autoRenew,
    this.efficiency,
    this.plateNumber,
    this.chargeMethodName,
    this.saleTransCode,
    this.methodCharge,
    this.createUser,
    this.vehicleGroupName,
    this.vehicleGroupId,
  });

  final int? stationId;
  final int? stageId;
  final String? stage;
  final String? station;

  @JsonKey(unknownEnumValue: ServicePlanType.other)
  final ServicePlanType? servicePlanTypeId;

  final String? servicePlanTypeName;
  final int? price;
  final int? status;
  final String? saleTransDate;
  final String? dateOfUse;
  final String? effDate;
  final String? expDate;
  final String? autoRenew;
  final int? efficiency;
  final String? plateNumber;
  final String? chargeMethodName;
  final String? saleTransCode;
  final String? methodCharge;
  final String? createUser;
  final String? vehicleGroupName;
  final int? vehicleGroupId;

  factory TransactionTicketPurchase.fromJson(Map<String, dynamic> json) =>
      _$TransactionTicketPurchaseFromJson(json);

  @override
  String toString() {
    return 'TransactionTicketPurchase $plateNumber';
  }
}

@JsonSerializable()
class TransactionTicketPurchaseData {
  final List<TransactionTicketPurchase>? listData;
  final int? count;

  TransactionTicketPurchaseData({this.listData, this.count});

  factory TransactionTicketPurchaseData.fromJson(Map<String, dynamic> json) =>
      _$TransactionTicketPurchaseDataFromJson(json);
}

class TransactionTicketPurchaseDataNotNull {
  final List<TransactionTicketPurchase> listData;
  final int count;

  TransactionTicketPurchaseDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class TransactionTicketPurchaseResponse {
  final TransactionTicketPurchaseData? data;
  final MessageData? mess;

  TransactionTicketPurchaseResponse({this.data, this.mess});

  factory TransactionTicketPurchaseResponse.fromJson(
          Map<String, dynamic> json) =>
      _$TransactionTicketPurchaseResponseFromJson(json);
}
