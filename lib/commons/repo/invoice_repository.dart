import 'package:epass/commons/api/clients/billing/billing_client.dart';
import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/invoice/invoice.dart';
import 'package:epass/commons/models/invoice/invoice_details_request.dart';
import 'package:epass/commons/models/invoice_cycle/update_invoice_cycle_request.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/referral_client/referral_client_request.dart';
import 'package:epass/pages/invoice/bloc/invoice_details_bloc.dart';
import 'package:simple_result/simple_result.dart';

abstract class IInvoiceRepository {
  Future<Result<InvoiceDataNotNull, Failure>> getCustInvoices({
    int pageSize = 20,
    int startRecord = 0,
    required String contractId,
    String? plateNumber,
    String? invoiceNo,
    int? adjustmentType,
    int? transactionType,
    int? stationInId,
    int? stationOutId,
    String? from,
    String? to,
  });

  Future<Result<List<int>, Failure>> downloadInvoice({
    required String invoiceId,
    Function()? callback,
  });

  Future<Result<String?, Failure>> updateInvoiceSetting({
    required int billCycle,
    int? billCycleMergeType,
  });

  Future<Result<NoData, Failure>> referralClient({
    required ReferralClientRequest referralClientRequest,
  });
}

class InvoiceRepository implements IInvoiceRepository {
  final BillingClient _billingClient;
  final CrmClient _crmClient;

  InvoiceRepository(
      {required BillingClient billingClient, required CrmClient crmClient})
      : _crmClient = crmClient,
        _billingClient = billingClient;

  @override
  Future<Result<InvoiceDataNotNull, Failure>> getCustInvoices({
    int pageSize = 20,
    int startRecord = 0,
    required String contractId,
    String? plateNumber,
    String? invoiceNo,
    int? adjustmentType,
    int? transactionType,
    int? stationInId,
    int? stationOutId,
    String? from,
    String? to,
  }) async {
    try {
      final response = await _billingClient.getCustInvoices(
        pageSize: pageSize,
        startRecord: startRecord,
        contractId: contractId,
        plateNumber: plateNumber,
        invoiceNo: invoiceNo,
        adjustmentType: adjustmentType,
        transactionType: transactionType,
        stationInId: stationInId,
        stationOutId: stationOutId,
        from: from,
        to: to,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(InvoiceDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  // code cũ xử lý tải hóa đơn
  // @override
  // Future<Result<List<int>, Failure>> downloadInvoice({required String invoiceId}) async {
  //   try {
  //     var response = await _billingClient.downloadInvoice(
  //       invoiceId: invoiceId,
  //       requestBody: InvoiceDetailsRequest(fileType: 'pdf'),
  //     );
  //     if (response.response.statusCode != 200) {
  //       return const Result.failure(UnknownFailure());
  //     }
  //
  //     return Result.success(response.data);
  //   } on UnauthorizedException catch (err) {
  //     return Result.failure(UnauthorizedFailure(message: err.toString()));
  //   } on ResponseException catch (err) {
  //     return Result.failure(ResponseFailure(message: err.toString()));
  //   } on Exception {
  //     return const Result.failure(UnknownFailure());
  //   }
  // }

  @override
  Future<Result<String?, Failure>> updateInvoiceSetting({
    required int billCycle,
    int? billCycleMergeType,
  }) async {
    try {
      final request = UpdateInvoiceCycleRequest(
        billCycle: billCycle,
        billCycleMergeType: billCycleMergeType,
      );

      var response = await _crmClient.updateInvoiceCycle(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return const Result.failure(UnknownFailure());
      }

      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  // code mới xử lý hóa đơn
  @override
  Future<Result<List<int>, Failure>> downloadInvoice(
      {required String invoiceId, Function()? callback}) async {
    try {
      var response = await _billingClient.downloadInvoice(
        invoiceId: invoiceId,
        requestBody: InvoiceDetailsRequest(fileType: 'pdf'),
      );
      if (response.response.statusCode != 200) {
        return const Result.failure(UnknownFailure());
      } else if (response.response.statusCode == 500) {
        var ok = 1;
      }

      return Result.success(response.data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      if (callback != null) {
        callback();
      }
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<NoData, Failure>> referralClient(
      {required ReferralClientRequest referralClientRequest}) async {
    try {
      final response = await _billingClient.referralClient(
        referralRequest: referralClientRequest,
      );

      if (response.mess?.code == REQUEST_SUCCESS_CODE) {
        return Result.success(response);
      } else {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
    } on BadNetworkException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
