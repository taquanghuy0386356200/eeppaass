import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/topup_fee/topup_fee.dart';
import 'package:epass/commons/models/update_fee/topup_fee_update.dart';
import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:simple_result/simple_result.dart';

part 'cash_in_fee_event.dart';

part 'cash_in_fee_state.dart';

class CashInFeeBloc extends Bloc<CashInFeeEvent, CashInFeeState> {
  final ICashInRepository _cashInRepository;

  CashInFeeBloc({required ICashInRepository cashInRepository})
      : _cashInRepository = cashInRepository,
        super(CashInFeeInitial()) {
    on<CashInFeeFetched>((event, emit) async {
      emit(CashInFeeFetchedInProgress());

      final result = await Future.wait([
        _cashInRepository.getTopupFeeUpdate(),
        Future.delayed(const Duration(seconds: 1)),
      ]);

      (result[0] as Result<List<TopupFeeDataUpdate>, Failure>).when(
        success: (fees) => emit(CashInFeeFetchedSuccess(fees)),
        failure: (failure) =>
            emit(CashInFeeFetchedFailure(failure.message ?? '')),
      );
    });
  }
}
