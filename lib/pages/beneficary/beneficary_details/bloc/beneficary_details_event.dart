part of 'beneficary_details_bloc.dart';

abstract class BeneficiaryDetailsEvent extends Equatable {
  const BeneficiaryDetailsEvent();
}

class BeneficiaryUpdated extends BeneficiaryDetailsEvent {
  final int beneficiaryInformationId;
  final String reminiscentName;

  const BeneficiaryUpdated({
    required this.beneficiaryInformationId,
    required this.reminiscentName,
  });

  @override
  List<Object> get props => [reminiscentName];
}

class BeneficiaryDelete extends BeneficiaryDetailsEvent {
  final String beneficiaryId;

  const BeneficiaryDelete({required this.beneficiaryId});

  @override
  List<Object> get props => [beneficiaryId];
}

class BeneficiaryChanged extends BeneficiaryDetailsEvent {
  final String? reminiscentName;

  const BeneficiaryChanged(this.reminiscentName);

  @override
  List<Object?> get props => [reminiscentName];
}
