// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mb_check_sum.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MBCheckSumRequest _$MBCheckSumRequestFromJson(Map<String, dynamic> json) =>
    MBCheckSumRequest(
      checkSumKey: json['checkSumKey'] as String,
      token: json['token'] as String,
    );

Map<String, dynamic> _$MBCheckSumRequestToJson(MBCheckSumRequest instance) =>
    <String, dynamic>{
      'checkSumKey': instance.checkSumKey,
      'token': instance.token,
    };

MBCheckSumResponse _$MBCheckSumResponseFromJson(Map<String, dynamic> json) =>
    MBCheckSumResponse(
      clientMessageId: json['clientMessageId'] as String,
      errorCode: json['errorCode'] as String?,
      data: MBCheckSumData.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MBCheckSumResponseToJson(MBCheckSumResponse instance) =>
    <String, dynamic>{
      'clientMessageId': instance.clientMessageId,
      'errorCode': instance.errorCode,
      'data': instance.data,
    };

MBCheckSumData _$MBCheckSumDataFromJson(Map<String, dynamic> json) =>
    MBCheckSumData(
      transactionID: json['transactionID'] as String,
    );

Map<String, dynamic> _$MBCheckSumDataToJson(MBCheckSumData instance) =>
    <String, dynamic>{
      'transactionID': instance.transactionID,
    };
