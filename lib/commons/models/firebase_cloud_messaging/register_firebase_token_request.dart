/// contractId : "abc"
/// contractNo : ""
/// token : ""
/// deviceType : "ios"
/// deviceInfo : ""

class RegisterFirebaseTokenRequest {
  RegisterFirebaseTokenRequest({
    required this.contractId,
    required this.contractNo,
    required this.token,
    this.deviceType = 'ios',
    required this.deviceInfo,
  });

  final String contractId;
  final String contractNo;
  final String token;
  final String? deviceType;
  final String deviceInfo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['contractId'] = contractId;
    map['contractNo'] = contractNo;
    map['token'] = token;
    map['deviceType'] = deviceType;
    map['deviceInfo'] = deviceInfo;
    return map;
  }
}
