import 'dart:ffi';

import 'package:dio/dio.dart';
import 'package:epass/commons/api/endpoint.dart' as endpoint;
import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/models/alias/update_alias_request.dart';
import 'package:epass/commons/models/app_version/app_version.dart';
import 'package:epass/commons/models/auth_info/auth_info.dart';
import 'package:epass/commons/models/auto_topup/auto-topup.dart';
import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/models/beneficiary/beneficiary_request.dart';
import 'package:epass/commons/models/bidv/bidv_check_otp.dart';
import 'package:epass/commons/models/buy_ticket/buy_ticket_request.dart';
import 'package:epass/commons/models/buy_ticket/buy_ticket_response.dart';
import 'package:epass/commons/models/campaign/campaign.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_verify_otp_request.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_verify_otp_response.dart';
import 'package:epass/commons/models/cash_in_order/cash_in_order.dart';
import 'package:epass/commons/models/change_password/change_password_request.dart';
import 'package:epass/commons/models/confirm_momo_payment/confirm_momo_payment.dart';
import 'package:epass/commons/models/contract_action/contract_action.dart';
import 'package:epass/commons/models/contract_encrypted/insurance_encrypted.dart';
import 'package:epass/commons/models/contract_payment/contract_payment.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:epass/commons/models/contracts_info/contracts_info.dart';
import 'package:epass/commons/models/create_vietmap_transaction/create_vietmap_transaction_request.dart';
import 'package:epass/commons/models/invoice_cycle/update_invoice_cycle_request.dart';
import 'package:epass/commons/models/link_viet_map_request/link_viet_map_request.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/notification_setting/update_balance_alert.dart';
import 'package:epass/commons/models/ocs_info/ocs_info.dart';
import 'package:epass/commons/models/parking/add_service_request.dart';
import 'package:epass/commons/models/parking/add_service_response.dart';
import 'package:epass/commons/models/parking/change_passcode_request.dart';
import 'package:epass/commons/models/parking/change_passcode_response.dart';
import 'package:epass/commons/models/parking/check_passcode_request.dart';
import 'package:epass/commons/models/parking/check_passcode_response.dart';
import 'package:epass/commons/models/parking/condition_service_request.dart';
import 'package:epass/commons/models/parking/condition_service_response.dart';
import 'package:epass/commons/models/parking/config_method_request.dart';
import 'package:epass/commons/models/parking/config_method_response.dart';
import 'package:epass/commons/models/parking/config_service_response.dart';
import 'package:epass/commons/models/parking/verify_config_request.dart';
import 'package:epass/commons/models/parking/verify_config_response.dart';
import 'package:epass/commons/models/register/register_request.dart';
import 'package:epass/commons/models/register/request_otp_register_request.dart';
import 'package:epass/commons/models/reset_password/request_otp_reset_password_request.dart';
import 'package:epass/commons/models/reset_password/reset_password_otp_confirm_request.dart';
import 'package:epass/commons/models/station_vehicle_alert/station_vehicle_alert_request.dart';
import 'package:epass/commons/models/station_vehicle_alert/station_vehicle_alert_response.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_request.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_response.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/models/topup_channel/topup_channel_fee.dart';
import 'package:epass/commons/models/topup_fee/topup_fee.dart';
import 'package:epass/commons/models/transaction_other/transaction_other.dart';
import 'package:epass/commons/models/transaction_packing/transaction_parking.dart';
import 'package:epass/commons/models/transaction_ticket_purchase/transaction_ticket_purchase.dart';
import 'package:epass/commons/models/update_fee/topup_fee_update.dart';
import 'package:epass/commons/models/update_vietmap_transaction/update_vietmap_transaction_request.dart';
import 'package:epass/commons/models/user/update_user_request.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/sms_vehicle_register_request.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/sms_vehicle_update_request.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/user_sms_vehicle_setting.dart';
import 'package:epass/commons/models/utility/unavailable_service_response.dart';
import 'package:epass/commons/models/vehicle/update_vehicle_etag.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/models/vehicle/vehicle_debit.dart';
import 'package:epass/commons/models/viet_map_status/viet_map_status_response.dart';
import 'package:epass/commons/models/viettel_money_balance/viettel_money_balance.dart';
import 'package:epass/commons/models/viettel_pay_insurance/count_insurance_response.dart';
import 'package:epass/commons/models/bidv/bidv_create_order.dart';
import 'package:retrofit/http.dart' as http;
import 'package:epass/commons/models/waiting_order/confirm_order_response.dart';
import 'package:epass/commons/models/waiting_order/waiting_order.dart';
import 'package:epass/commons/models/waiting_order/confirm_order_request.dart';
import 'package:retrofit/retrofit.dart';

import '../../../models/zalo_pay/confirm_contracts_payment.dart';

part 'crm_client.g.dart';

@RestApi()
abstract class CrmClient {
  factory CrmClient(
    Dio dio, {
    String baseUrl,
  }) = _CrmClient;

  /// /api/v1/mobile/get-version-current
  @GET(endpoint.getAppVersion)
  Future<AppVersionResponse> getAppVersion({
    @Query('appCode') String appCode = 'CPT',
    @Query('deviceType') required String deviceType,
  });

  /// /api/v1/login
  @POST(endpoint.loginCRM)
  @FormUrlEncoded()
  Future<AuthInfo?> login(
    @Field() String username,
    @Field() String password,
    @Field("client_id") String clientId,
    @Field("grant_type") String grantType,
  );

  /// /api/v1/logout
  @POST(endpoint.logout)
  @FormUrlEncoded()
  Future<NoData?> logout({
    @Field("client_id") required String clientId,
    @Field("refresh_token") required String refreshToken,
  });

  /// /api/v1/mobile/contracts
  @GET(endpoint.contractDetail)
  Future<UserResponse> getUser();

  /// /api/v1/contract-payments/{contractId}
  @GET(endpoint.contractPayment)
  Future<ContractPaymentResponse> getContractPayment(
      @Path('contractId') String contractId);

  /// /api/v1/campaign
  @GET(endpoint.campaign)
  Future<CampaignResponse> getCampaigns({@Query('isActive') int isActive = 1});

  /// /api/v1/customers/{customerId}/contracts/{contractId}/ocsInfo
  @GET(endpoint.ocsInfo)
  Future<OCSInfoResponse> getOCSInfo(
    @Path('customerId') String customerId,
    @Path('contractId') String contractId,
  );

  /// /api/v1/topup-channel
  @GET(endpoint.topupChannel)
  Future<TopupChannelResponse> getTopupChannel();

  /// /api/v1/contracts-info
  @GET(endpoint.contractsInfo)
  Future<ContractsInfoResponse> getContractsInfo({
    @Query('inputSearch') String? inputSearch,
    @Query('status') int? status,
  });

  /// /api/v1/topup/fee
  @GET(endpoint.topupFee)
  Future<TopupFeesResponse> getTopupFee();

  /// /api/v1/topup/service-fee
  @GET(endpoint.topupFeeUpdate)
  Future<TopupFeeUpdateResponse> getTopupFeeUpdate();

  /// /api/v1/topup-channel/{topupChannelCode}?value=10000
  @GET(endpoint.topupChannelFee)
  Future<TopupChannelFeeResponse> getTopupChannelFee({
    @Path('topupChannelCode') required String topupChannelCode,
    @Query('value') required int amount,
  });

  /// /api/v1/viettelpay/customers/{customerId}/contracts/{contractId}/sale-order
  @POST(endpoint.cashInOrderViettelPay)
  Future<CashInOrderResponse> createCashInOrderViettelPay({
    @Path('customerId') required String customerId,
    @Path('contractId') required String contractId,
    @Body() required CashInOrderRequest request,
  });

  /// /api/v1/mobile-app-chupt/customers/{customerId}/contracts/{contractId}/sale-order
  @POST(endpoint.cashInOrderMomo)
  Future<CashInOrderResponse> createCashInOrderMomo({
    @Path('customerId') required String customerId,
    @Path('contractId') required String contractId,
    @Body() required CashInOrderRequest request,
  });

  /// /api/v1/vnpay/customers/{customerId}/contracts/{contractId}/sale-order
  @POST(endpoint.cashInOrderVNPay)
  Future<CashInOrderResponse> createCashInOrderVNPay({
    @Path('customerId') required String customerId,
    @Path('contractId') required String contractId,
    @Body() required CashInOrderRequest request,
  });

  /// /api/v1/momo/pay/app
  @POST(endpoint.confirmMomoPayment)
  Future<ConfirmMomoPaymentResponse> confirmMomoPayment({
    @Body() required ConfirmMomoPaymentRequest request,
  });

  /// dumv
  @POST(endpoint.confirmContractsPayment)
  Future<ConfirmContractsPaymentResponse> confirmContractsPayment({
    @Path('contractId') required String contractId,
    @Body() required ConfirmContractsPaymentRequest request,
  });

  /// /api/v1/contracts/topup
  @GET(endpoint.contractTopup)
  Future<ContractTopupResponse> getContractTopup({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,

    /// format: dd/MM/yyyy
    @Query('topupDateFrom') String? topupDateFrom,

    /// format: dd/MM/yyyy
    @Query('topupDateTo') String? topupDateTo,
  });

  /// /api/v1/customers/{customerId}/contracts/{contractId}/act-contract-histories
  @GET(endpoint.contractAction)
  Future<ContractActionResponse> getContractAction({
    @Path('customerId') required String customerId,
    @Path('contractId') required String contractId,
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,

    /// format: dd/MM/yyyy
    @Query('startDate') String? startDate,

    /// format: dd/MM/yyyy
    @Query('endDate') String? endDate,
  });

  /// /api/v1/mobile/other-transaction-histories
  @GET(endpoint.transactionOther)
  Future<TransactionOtherResponse> getTransactionOther({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,
    @Query('plateNumber') String? plateNumber,

    /// format: dd/MM/yyyy
    @Query('fromDate') String? fromDate,

    /// format: dd/MM/yyyy
    @Query('toDate') String? toDate,
  });

  /// /api/v1/mobile/ticket-purchase-histories
  @GET(endpoint.transactionTicketPurchase)
  Future<TransactionTicketPurchaseResponse> getTransactionTicketPurchase({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,

    /// active: 1, inactive: 2
    @Query('efficiencyId') int? efficiencyId,
    @Query('plateNumber') String? plateNumber,

    /// format: dd/MM/yyyy
    @Query('saleTransDateFrom') String? saleTransDateFrom,

    /// format: dd/MM/yyyy
    @Query('saleTransDateTo') String? saleTransDateTo,
  });

  /// /api/v1/mobile/ticket-purchase-histories
  @GET(endpoint.transactionParking)
  Future<TransactionPackingResponse> getTransactionParking({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,
    // @Query('serviceCode') String? serviceCode,
    @Query('plateNumber') String? plateNumber,

    /// format: dd/MM/yyyy
    @Query('startDate') String? saleTransDateFrom,

    /// format: dd/MM/yyyy
    @Query('endDate') String? saleTransDateTo,
  });

  /// /api/v1/request-otp/contract/reset-pass
  @POST(endpoint.requestOTPResetPassword)
  Future<NoData> requestOTPResetPassword({
    @Body(nullToAbsent: true) required RequestOtpResetPasswordRequest request,
  });

  /// /api/v1/mobile/reset/user
  @PUT(endpoint.resetPasswordOTPConfirm)
  Future<NoData> resetPasswordOTPConfirm({
    @Body(nullToAbsent: true) required ResetPasswordOTPConfirmRequest request,
  });

  /// /api/v1/alias
  @POST(endpoint.alias)
  Future<NoData> updateAlias({
    @Body(nullToAbsent: true) required UpdateAliasRequest request,
  });

  /// /api/v1/mobile/change/user
  @PUT(endpoint.changePassword)
  Future<NoData> changePassword({
    @Body(nullToAbsent: true) required ChangePasswordRequest request,
  });

  /// /api/v1/mobile/customers/contracts/vehicles
  @GET(endpoint.userVehicles)
  Future<VehicleListDataResponse> getUserVehicles({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,
    @Query('statuses') List<int>? statuses,
    @Query('activeStatuses') List<int>? activeStatuses,
    @Query('plateNumber') String? plateNumber,
    @Query('inContract') bool? inContract,
  });

  /// /api/v1/vehicles/{vehicleId}/rfid/{rfid}
  @PUT(endpoint.vehicleRFID)
  Future<NoData> updateVehicleRFIDStatus({
    @Path('vehicleId') required String vehicleId,
    @Path('rfid') required String rfid,
    @Body() required UpdateVehicleEtag request,
  });

  /// /api/v1/mobile/customers
  @PUT(endpoint.updateUser)
  Future<NoData> updateUser({
    @Body(nullToAbsent: true) required UpdateUserRequest request,
  });

  /// /api/v1/contract/avatar/download
  @POST(endpoint.downloadAvatar)
  @http.Headers({
    'Accept': 'application/json',
  })
  @DioResponseType(ResponseType.bytes)
  Future<HttpResponse<List<int>>> downloadAvatar();

  /// /api/v1/service-plans/fee-boo
  @POST(endpoint.servicePlanFeeBoo)
  Future<TicketPriceResponse> getTicketPrices({
    @Body() required TicketPriceListRequest request,
  });

  /// /api/v1/customers/{customerId}/contracts/{contractId}/charge-ticket-boo
  @POST(endpoint.buyTicket)
  Future<BuyTicketResponse> buyTickets({
    @Path('customerId') required String customerId,
    @Path('contractId') required String contractId,
    @Body() required BuyTicketRequest request,
  });

  /// /api/v1/count/insurance
  @GET(endpoint.countInsurance)
  Future<CountInsuranceResponse> getCountInsurance();

  /// /api/v1/contracts/alert-money
  @PUT(endpoint.alertMoney)
  Future<NoData> updateAlertMoney({
    @Body() required UpdateBalanceAlertRequest request,
  });

  /// /api/v1/sms/notification
  @GET(endpoint.userSMSNotification)
  Future<UserSmsVehicleSettingResponse> getUserSmsVehicleSetting();

  /// /api/v1/sms/register
  @POST(endpoint.smsRegister)
  Future<NoData> registerSMSVehicle({
    @Body() required SmsVehicleRegisterRequest request,
  });

  /// /api/v1/sms/notification
  @PUT(endpoint.smsNotification)
  Future<NoData> updateSMSVehicle({
    @Body() required SmsVehicleUpdateRequest request,
  });

  /// /api/v1/viettelpay/inquiry-balance
  @POST(endpoint.viettelMoneyBalance)
  Future<ViettelMoneyBalanceResponse> getViettelMoneyBalance();

  /// /api/v1/vtt/provinces
  @GET(endpoint.provincesVTT)
  Future<AddressVTTResponse> getProvinces();

  /// /api/v1/vtt/districts
  @GET(endpoint.districtsVTT)
  Future<AddressVTTResponse> getDistricts({
    @Query('province') required String provinceCode,
  });

  /// /api/v1/vtt/wards
  @GET(endpoint.wardsVTT)
  Future<AddressVTTResponse> getWards({
    @Query('province') required String provinceCode,
    @Query('district') required String districtCode,
  });

  /// /api/v1/contracts/service
  @PUT(endpoint.invoiceCycle)
  Future<NoData> updateInvoiceCycle({
    @Body() required UpdateInvoiceCycleRequest request,
  });

  /// /api/v1/request-otp/register
  @POST(endpoint.requestOTPRegister)
  Future<NoData> requestOTPRegister({
    @Body() required RequestOtpRegisterRequest request,
  });

  /// /api/v1/register
  @POST(endpoint.register)
  Future<NoData> register({
    @Body() required RegisterRequest request,
  });

  /// /api/v1/vehicle-tracks
  @POST(endpoint.vehicleTracks)
  Future<StationVehicleAlertResponse> vehicleAlert({
    @Body() required StationVehicleAlertRequest request,
  });

  /// /api/v1/contract/encrypted
  @POST(endpoint.contractEncrypted)
  Future<ContractEncryptedResponse> contractEncrypted();

  @POST(endpoint.saveConfigAutoTopup)
  Future<AutoTopupResponse> saveConfigAutoTopup({
    @Body() required AutoTopupRequest request,
  });

  @GET(endpoint.getConfigAutoTopup)
  Future<AutoTopupResponse> getConfigAutoTopup();

  @PUT(endpoint.updateConfigAutoTopup)
  Future<AutoTopupResponse> updateConfigAutoTopup({
    @Body() required AutoTopupRequest request,
  });

  /// /api/v1/mobile/customers/contracts/vehicles
  @GET(endpoint.getUserBeneficiarys)
  Future<BeneficiaryListDataResponse> getUserBeneficiarys({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,
    @Query('statuses') List<int>? statuses,
    // @Query('activeStatuses') List<int>? activeStatuses,
    @Query('reminiscentName') String? reminiscentName,
    @Query('inContract') bool? inContract,
  });

  @DELETE(endpoint.deleteBeneficiary)
  Future<NoData> deleteBeneficiary(
      {@Query('beneficiaryId') String? beneficiaryId});

  @POST(endpoint.saveBeneficiary)
  Future<NoData> saveBeneficiary({
    @Body() required BeneficiaryRequest request,
  });

  @GET(endpoint.history)
  Future<ContractTopupLastChargeResponse> getLastCharge();

  @POST(endpoint.updateBeneficiary)
  Future<NoData> updateBeneficiary({
    @Body() required BeneficiaryRequestUpdate request,
  });

  @POST(endpoint.topupEwallet)
  Future<ConfirmContractsPaymentResponse> topupEWallet({
    @Body() required ConfirmContractsPaymentRequest request,
  });

  @POST(endpoint.topupBIDV)
  Future<BidvCreateOrderResponse> topupBIDV({
    @Query('wrapperXml') String? wrapperXml,
    @Query('dataEncode') String? dataEncode,
    @Query('callType') int? callType,
  });

  /// /api/v1/get-config-services
  @GET(endpoint.getConfigServices)
  Future<ConfigServiceResponse> getConfigServices();

  /// /api/v1/get-current-verify-config
  @POST(endpoint.getCurrentVerifyConfig)
  Future<VerifyConfigResponse> getCurrentVerifyConfig({
    @Body() required VerifyConfigRequest request,
  });

  /// /api/v1/check-condition-use-service
  @POST(endpoint.checkConditionUseService)
  Future<ConditionServiceResponse> checkConditionUseService({
    @Body() required ConditionServiceRequest request,
  });

  /// /api/v1/add-service
  @POST(endpoint.addService)
  Future<AddServiceResponse> addService({
    @Body() required AddServiceRequest request,
  });

  /// /api/v1/config-method-verify
  @POST(endpoint.configMethodVerify)
  Future<ConfigMethodVerifyResponse> configMethodVerify({
    @Body() required ConfigMethodVerifyRequest request,
  });

  /// /api/v1/check-passcode
  @POST(endpoint.checkPassCode)
  Future<CheckPassCodeResponse> checkPassCode({
    @Body() required CheckPassCodeRequest request,
  });

  /// /api/v1/change-passcode
  @POST(endpoint.changePassCode)
  Future<ChangePassCodeResponse> changePassCode({
    @Body() required ChangePassCodeRequest request,
  });

  /// /api/v1/request-otp
  @GET(endpoint.requestOtp)
  Future<NoData> requestOtp({
    @Query('phone') required String phone,
    @Query('confirmType') required int confirmType,
  });

  /// /api/v1/sale-orders/confirm
  @POST(endpoint.confirmWaitingOrder)
  Future<ConfirmOrderResponse> confirmWaitingOrder({
    @Body() required ConfirmOrderRequest request,
  });

  /// /api/v1/get-contract-allow-access-service
  @GET(endpoint.getUnavailableService)
  Future<UnavailableServiceResponse> getUnavailableService();

  /// /api/v1/lane-parking/list-vehicles-contract
  @GET(endpoint.getListWaitForConfirmation)
  Future<UnavailableServiceResponse> getListWaitForConfirmation();

  /// /api/v1/contract/{contractId}/deposit/{amount}
  @POST(endpoint.checkTypeCust)
  Future<ContractTopupTypeResponse> checkTypeCust({
    @Path('contractId') required String contractId,
    @Path('amount') required int amount,
  });

  @GET(endpoint.checkVehicleInContract)
  Future<VehicleInContractResponse> checkVehicleInContract({
    @Path('contractId') required String contractId,
  });

  /// /api/v1/vietmap/link/check
  @GET(endpoint.checkVietMapStatus)
  Future<VietMapStatusResponse> checkVietMapStatus();

  /// /api/v1/vietmap/link/epass-to-vml
  @POST(endpoint.linkVietMap)
  Future<NoData> linkVietMap({
    @Body() required LinkVietMapRequest request});

  /// /api/v1/vietmap/link/delete
  @POST(endpoint.unLinkVietMap)
  Future<NoData> unLinkVietMap();

  /// /api/v1/vietmap/charge
  @POST(endpoint.createVietMapTransaction)
  Future<NoData> createVietMapTransaction({
    @Body() required CreateVietMapTransactionRequest request});

  /// /api/v1/vietmap/update
  @POST(endpoint.updateVietMapTransaction)
  Future<NoData> updateVietMapTransaction({
    @Body() required UpdateVietMapTransactionRequest request});

  /// /api/v1/debit/get-debit?contractId=&vehicleId=
  @GET(endpoint.getDebit)
  Future<VehicleDebitResponse> getDebit({
    @Query('contractId') required String contractId,
    @Query('vehicleId') required String vehicleId,
  });

  /// /api/v1/debit-transaction/rfid/{vehicleId}
  @POST(endpoint.getDebitTransaction)
  Future<VehicleDebitTransactionResponse> getDebitTransaction({
    @Path('vehicleId') required String vehicleId,
  });
}
