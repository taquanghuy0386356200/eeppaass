import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vietmap_ecommerce_flutter_plugin/src/features/core/application/extensions/iterable_extension.dart';
import 'package:vietmap_ecommerce_flutter_plugin/src/features/core/constants.dart';
import 'package:vietmap_ecommerce_flutter_plugin/src/features/core/domain/entities/utils_entities.dart';
import 'package:vietmap_ecommerce_flutter_plugin/src/features/js_communicate/application/js_communicator.dart';
import 'package:vietmap_ecommerce_flutter_plugin/src/features/payment/presentation/payment_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';

export 'src/exports.dart_exporter.dart';

class VietmapEcommerceFlutterPlugin {
  static Future<dynamic> start(
    BuildContext context, {
    required void Function(JSCommunicator) onCreated,
    required EcommerceMethodCallHander ecommerceMethodCallHander,
    required void Function(OrderResultEntity orderResult) onOrderResult,
    required void Function(OrderResultEntity orderResult)
        onOrderStatusScreenClosed,
    required Function(String newName) onCustomerNameChanged,
    required VoidCallback onLoginRequired,
  }) async {
    // final parentContext = context;
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return Material(
            child: _ProductListScreen(
              onCreated: onCreated,
              ecommerceMethodCallHander: ecommerceMethodCallHander,
              onOrderResult: onOrderResult,
              onCustomerNameChanged: onCustomerNameChanged,
              onOrderStatusScreenClosed: onOrderStatusScreenClosed,
              onLoginRequired: onLoginRequired,
            ),
          );
        },
      ),
    );
    return result;
  }
}

class _ProductListScreen extends StatefulWidget {
  final void Function(JSCommunicator) onCreated;
  final EcommerceMethodCallHander ecommerceMethodCallHander;
  final void Function(OrderResultEntity orderResult) onOrderResult;
  final void Function(OrderResultEntity orderResult) onOrderStatusScreenClosed;
  final Function(String newName) onCustomerNameChanged;
  final VoidCallback onLoginRequired;

  const _ProductListScreen({
    Key? key,
    required this.onCreated,
    required this.ecommerceMethodCallHander,
    required this.onOrderResult,
    required this.onOrderStatusScreenClosed,
    required this.onCustomerNameChanged,
    required this.onLoginRequired,
  }) : super(key: key);

  @override
  State<_ProductListScreen> createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<_ProductListScreen> {
  final navigatorKey = GlobalKey<NavigatorState>();
  String? title;
  String? routeName;
  bool canGoBack = false;
  JSCommunicator? jsCommunicator;

  dynamic currentResult;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Container(
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
      ),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
              // gradient: LinearGradient(
              //   begin: Alignment.topLeft,
              //   end: Alignment.bottomCenter,
              //   colors: [
              //     Color(0xff2787F4),
              //     Color(0xff45A3F5),
              //   ],
              // ),
              color: Colors.white,
            ),
            child: SafeArea(
              bottom: false,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (canGoBack) ...[
                    InkWell(
                      onTap: () async {
                        if (navigatorKey.currentState?.canPop() == true) {
                          navigatorKey.currentState?.pop();
                        } else {
                          bool shouldGoBack = true;
                          if (routeName == 'payment' ||
                              routeName == 'placeOrder') {
                            final yes = await _showYesNoDialog(
                              context,
                              title: 'Quay lại?',
                              content: 'Tất cả các thao tác sẽ bị huỷ.',
                            );

                            shouldGoBack = yes == true;
                          }
                          if (shouldGoBack) {
                            jsCommunicator?.goBack();
                          }
                        }
                      },
                      child: const Icon(
                        Icons.arrow_back_ios_new_sharp,
                        size: 20,
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                  ],
                  Expanded(
                    child: Text(
                      title ?? 'Cửa hàng VIETMAP',
                      style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Container(
                    decoration: ShapeDecoration(
                      shape: StadiumBorder(
                        side: BorderSide(
                          color: Theme.of(context).dividerColor,
                        ),
                      ),
                    ),
                    padding: const EdgeInsets.symmetric(
                      vertical: 2,
                      horizontal: 4,
                    ),
                    child: IntrinsicHeight(
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Image.asset(
                              'lib/assets/more_horizontal.png',
                              package: packageName,
                              width: 20,
                            ),
                          ),
                          const VerticalDivider(
                            width: 16,
                          ),
                          InkWell(
                            onTap: () async {
                              final yes = await _showYesNoDialog(
                                context,
                                title: 'Thoát khỏi cửa hàng VIETMAP?',
                                content:
                                    'Tất cả thao tác hiện tại của bạn sẽ bị huỷ bỏ!',
                                yesText: 'Thoát',
                                noText: 'Huỷ bỏ',
                              );
                              if (yes == true && mounted) {
                                Navigator.pop(context, currentResult);
                              }
                            },
                            child: Image.asset(
                              'lib/assets/circle.png',
                              package: packageName,
                              width: 24,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const Divider(
            height: 0,
            thickness: 1,
          ),
          Expanded(
            child: Navigator(
              key: navigatorKey,
              onGenerateRoute: (settings) {
                return MaterialPageRoute(
                  builder: (context) => _VIETMAPEcommerceScreen(
                    onCreated: (jsCommunicator) {
                      widget.onCreated(jsCommunicator);
                      setState(() {
                        this.jsCommunicator = jsCommunicator;
                      });
                    },
                    ecommerceMethodCallHander: widget.ecommerceMethodCallHander,
                    onOrderResult: (orderId, orderStatus) {
                      final OrderResultEntity orderResult = OrderResultEntity(
                        orderId: orderId,
                        sucess: orderStatus != 'PendingPayment',
                      );
                      widget.onOrderResult.call(orderResult);
                      currentResult = orderResult;
                    },
                    onRouteChanged: (routeName, title, canGoBack) {
                      setState(() {
                        this.title = title;
                        this.routeName = routeName;
                        this.canGoBack = canGoBack;
                      });
                    },
                    onCustomerNameChanged: widget.onCustomerNameChanged,
                    onOrderStatusScreenClosed:
                        (String orderId, String orderStatus) {
                      final OrderResultEntity orderResult = OrderResultEntity(
                        orderId: orderId,
                        sucess: orderStatus != 'PendingPayment',
                      );
                      widget.onOrderStatusScreenClosed.call(orderResult);
                      currentResult = orderResult;
                    },
                    onLoginRequired: widget.onLoginRequired,
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

typedef EcommerceMethodCallHander = Function(VIETMAPJsMethod);

class _VIETMAPEcommerceScreen extends StatefulWidget {
  final void Function(JSCommunicator) onCreated;
  final EcommerceMethodCallHander ecommerceMethodCallHander;
  final void Function(String orderId, String orderStatus) onOrderResult;
  final void Function(String orderId, String orderStatus)
      onOrderStatusScreenClosed;
  final Function(String newName) onCustomerNameChanged;
  final Function(String routeName, String? title, bool canGoBack)
      onRouteChanged;

  final VoidCallback onLoginRequired;
  const _VIETMAPEcommerceScreen({
    Key? key,
    required this.onCreated,
    required this.ecommerceMethodCallHander,
    required this.onOrderResult,
    required this.onOrderStatusScreenClosed,
    required this.onRouteChanged,
    required this.onCustomerNameChanged,
    required this.onLoginRequired,
  }) : super(key: key);

  @override
  State<_VIETMAPEcommerceScreen> createState() =>
      _VIETMAPEcommerceScreenState();
}

class _VIETMAPEcommerceScreenState extends State<_VIETMAPEcommerceScreen> {
  final Completer<WebViewController> _webViewController = Completer();
  final Completer<JSCommunicator> _jsCommunicator = Completer();
  final StreamController<JSCommunicator> _jsCommunicatorStreamController =
      StreamController<JSCommunicator>();

  bool isLoading = false;
  @override
  void initState() {
    super.initState();
    _jsCommunicatorStreamController.stream.listen((jsCommunicator) async {
      widget.onCreated(jsCommunicator);
    });
    try {
      if (Platform.isAndroid) {
        WebView.platform = SurfaceAndroidWebView();
      }
    } catch (e) {
      ///
    }
  }

  @override
  void dispose() {
    _jsCommunicatorStreamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          WebView(
            initialUrl:
                'https://minio.vietmap.vn/khoaln/ecommerce/prod/epass/web/index.html',
            javascriptMode: JavascriptMode.unrestricted,
            gestureNavigationEnabled: false,
            onWebViewCreated: (controller) {
              _webViewController.complete(controller);
            },
            onPageStarted: (_) {
              if (!isLoading && mounted) {
                setState(() {
                  isLoading = true;
                });
              }
            },
            navigationDelegate: (url) {
              if (!url.url.startsWith('http')) {
                launch(url.url);
                return NavigationDecision.prevent;
              }
              return NavigationDecision.navigate;
            },
            onWebResourceError: (error) {
              if (kDebugMode) {
                print(error);
              }
            },
            javascriptChannels: {
              JavascriptChannel(
                name: 'VIETMAPEcommerce',
                onMessageReceived: (JavascriptMessage message) async {
                  final data = message.message;
                  try {
                    final splited = data.split('#');
                    final method = splited.getOrNull(0);
                    final params = splited.getOrNull(1);
                    if (method?.isNotEmpty == true) {
                      switch (method!) {
                        case 'paymentRequest':
                          final paymentUrl = params as String;
                          final result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PaymentScreen(
                                paymentUrl: paymentUrl,
                              ),
                            ),
                          );
                          // final result = await launch(paymentUrl);
                          (await _jsCommunicator.future).invoke(
                              'setPaymentResult', '${result ?? 'cancel'}');
                          break;

                        case 'orderProcessComplete':
                          final json = jsonDecode(params as String)
                              .cast<String, dynamic>() as Map<String, dynamic>;
                          widget.onOrderResult(
                            json['orderId'],
                            json['orderStatus'],
                          );
                          break;

                        case 'orderStatusScreenClosed':
                          final json = jsonDecode(params as String)
                              .cast<String, dynamic>() as Map<String, dynamic>;
                          widget.onOrderStatusScreenClosed(
                            json['orderId'],
                            json['orderStatus'],
                          );
                          break;

                        case 'customerNameChanged':
                          final newName = params;
                          if (newName != null) {
                            widget.onCustomerNameChanged(newName);
                          }
                          break;

                        case 'loginRequired':
                          widget.onLoginRequired();
                          break;

                        case 'routeChanged':
                          final json = jsonDecode(params as String)
                              .cast<String, dynamic>() as Map<String, dynamic>;
                          widget.onRouteChanged(
                            json['routeName'],
                            json['title'],
                            json['canGoBack'] == true,
                          );
                          break;
                        case 'flutterEngine':
                          if (params == 'running') {
                            if (isLoading && mounted) {
                              setState(() {
                                isLoading = false;
                              });
                            }
                            final communicator = JSCommunicator(
                                controller: await _webViewController.future);
                            _jsCommunicator.complete(communicator);
                            _jsCommunicatorStreamController.add(communicator);
                          }
                          break;

                        case 'setClipboardData':
                          if (params != null) {
                            Clipboard.setData(ClipboardData(text: params));
                          }
                          break;

                        default:
                          widget.ecommerceMethodCallHander(
                            VIETMAPJsMethod(
                              name: method,
                              params: params,
                            ),
                          );
                      }
                    }
                  } catch (e) {
                    ///
                  }
                },
              )
            },
          ),
          if (isLoading)
            Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: SafeArea(
                child: Column(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Lottie.asset(
                            'lib/assets/lottie_loading.json',
                            package: packageName,
                            width: 240,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Text(
                            'VIETMAP',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff2787F4),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          style: TextStyle(
                            color:
                                Theme.of(context).textTheme.bodyMedium?.color,
                          ),
                          children: const [
                            TextSpan(
                              text:
                                  'Đây là tiện ích độc lập được phát triển và cung cấp bởi',
                            ),
                            TextSpan(
                                text: ' VIETMAP ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}

Future<bool> _showYesNoDialog(
  BuildContext context, {
  required String title,
  required String content,
  String? yesText,
  String? noText,
}) async {
  final result = await Navigator.push(
      context,
      PageRouteBuilder(
        barrierDismissible: true,
        fullscreenDialog: true,
        pageBuilder: (context, animation1, animation2) => FadeTransition(
          opacity: animation1,
          child: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: Center(
              child: Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        title,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(content),
                      const SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: MaterialButton(
                              elevation: 0,
                              onPressed: () {
                                Navigator.pop(context, true);
                              },
                              height: 40,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  side: BorderSide(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    width: 2,
                                  )),
                              color: Theme.of(context).colorScheme.onPrimary,
                              textColor: Theme.of(context).colorScheme.primary,
                              child: Text(yesText ?? 'Có'),
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: MaterialButton(
                              elevation: 0,
                              height: 40,
                              onPressed: () {
                                Navigator.pop(context, false);
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              color: Theme.of(context).colorScheme.primary,
                              textColor:
                                  Theme.of(context).colorScheme.onPrimary,
                              child: Text(
                                noText ?? 'Không',
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ));
  return result == true;
}
