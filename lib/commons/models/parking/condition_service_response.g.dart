// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'condition_service_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConditionServiceModel _$ConditionServiceModelFromJson(
        Map<String, dynamic> json) =>
    ConditionServiceModel(
      vehicleId: json['vehicleId'] as int?,
      contractId: json['contractId'] as int,
      custId: json['custId'] as int,
      customerName: json['customerName'] as String,
      contactNumber: json['contactNumber'] as String,
      contractNo: json['contractNo'] as String,
      plateTypeId: json['plateTypeId'] as int?,
      epc: json['epc'] as String?,
      isTraffic: json['isTraffic'] as bool,
      moneyCode: json['moneyCode'] as String?,
      isFirstTime: json['isFirstTime'] as bool?,
      description: json['description'] as String?,
      etagged: json['etagged'] as int?,
    );

Map<String, dynamic> _$ConditionServiceModelToJson(
        ConditionServiceModel instance) =>
    <String, dynamic>{
      'vehicleId': instance.vehicleId,
      'contractId': instance.contractId,
      'custId': instance.custId,
      'customerName': instance.customerName,
      'contactNumber': instance.contactNumber,
      'contractNo': instance.contractNo,
      'plateTypeId': instance.plateTypeId,
      'epc': instance.epc,
      'isTraffic': instance.isTraffic,
      'moneyCode': instance.moneyCode,
      'isFirstTime': instance.isFirstTime,
      'description': instance.description,
      'etagged': instance.etagged,
    };

ConditionServiceResponse _$ConditionServiceResponseFromJson(
        Map<String, dynamic> json) =>
    ConditionServiceResponse(
      data: json['data'] == null
          ? null
          : ConditionServiceModel.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConditionServiceResponseToJson(
        ConditionServiceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
