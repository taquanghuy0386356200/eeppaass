import 'dart:async';

import 'package:epass/commons/models/parking/verify_config_response.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/bloc/parking_setting_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/otp_bottom_sheet/widgets/parking_otp_countdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pin_code_fields/pin_code_fields.dart';


class OtpBottomSheet extends StatefulWidget {
  final VerifyConfigModel verifyConfigModel;
  final ParkingSettingBloc parkingBloc;

  const OtpBottomSheet({Key? key, required this.verifyConfigModel,
    required this.parkingBloc}) : super(key: key);

  @override
  State<OtpBottomSheet> createState() => _OtpBottomSheetState();
}

class _OtpBottomSheetState extends State<OtpBottomSheet> {
  final _errorController = StreamController<ErrorAnimationType>();
  final _otpController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void dispose() {
    _errorController.close();
    _otpController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 10.h,
          width: 60.w,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.5),
            borderRadius: const BorderRadius.all(
              Radius.circular(24.0),
            ),
          ),
        ),
        SizedBox(height: 10.h),
        Text('Xác nhận OTP',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 18.sp,
                color: Colors.black,
                fontWeight: FontWeight.bold)),
        SizedBox(height: 10.h),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: 'Nhập mã đã được gửi đến số điện thoại ',
                style: Theme
                    .of(context)
                    .textTheme
                    .bodyText1,
              ),
              TextSpan(
                  text: context.read<AppBloc>().state.user?.noticePhoneNumber ?? '',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontWeight: FontWeight.bold)
              ),
            ],
          ),
        ),
        SizedBox(height: 20.h),
        Material(
          child: PinCodeTextField(
            appContext: context,
            errorAnimationController: _errorController,
            controller: _otpController,
            length: 6,
            obscureText: false,
            animationType: AnimationType.fade,
            pinTheme: PinTheme(
              shape: PinCodeFieldShape.underline,
              fieldHeight: 46.r,
              fieldWidth: 46.r,
              errorBorderColor: ColorName.error,
              borderWidth: 1.0,
              activeColor: ColorName.disabledBorderColor,
              activeFillColor: Colors.white,
              inactiveColor: ColorName.disabledBorderColor,
              inactiveFillColor: Colors.white,
              selectedColor: ColorName.disabledBorderColor,
              selectedFillColor: Colors.white,
            ),
            backgroundColor: Colors.white,
            autoDismissKeyboard: true,
            autoFocus: true,
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
              fontSize: 22.sp,
              fontWeight: FontWeight.w600,
            ),
            onCompleted: _onOTPCompleted,
            onChanged: (value) {},
            beforeTextPaste: (text) {
              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
              //but you can show anything you want here, like your pop up saying wrong paste format or etc
              return false;
            },
          ),
        ),
        SizedBox(height: 8.h),
        ParkingOTPCountdown(
          parkingBloc: widget.parkingBloc,
          otpSubmittedTime: DateTime.now(),
          onResend: (){
            _otpController.text = '';
          },
        )
      ],
    );
  }

  void _onOTPCompleted(String otp) async{
    FocusScope.of(context).unfocus();
    widget.parkingBloc.add(OtpSubmitted(otp: otp));
    // Navigator.of(context).pop();
  }
}
