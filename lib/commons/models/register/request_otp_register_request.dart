import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:epass/flavors.dart';

/// phone : ""
/// confirmType : ""
/// requestTime : ""
/// checkSum : ""

class RequestOtpRegisterRequest {
  RequestOtpRegisterRequest({
    required this.phone,
  }) {
    _confirmType = '0';
    _requestTime = DateTime.now().millisecondsSinceEpoch.toString();
  }

  final String phone;
  late String _confirmType;
  late String _requestTime;


  String get _checksum {



    final checksum = '${F.crmAccessKey}'
        '$phone'
        '${phone.substring(phone.length - 4, phone.length)}'
        '$_requestTime'
        '$_confirmType';
    final key = utf8.encode(F.crmHashKey);
    final data = utf8.encode(checksum);
    final hmacSha1 = Hmac(sha1, key);
    final digest = hmacSha1.convert(data);
    final checkSumBase64 = base64.encode(digest.bytes);

    return checkSumBase64;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['phone'] = phone;
    map['confirmType'] = _confirmType;
    map['requestTime'] = _requestTime;
    map['checkSum'] = _checksum;
    return map;
  }
}
