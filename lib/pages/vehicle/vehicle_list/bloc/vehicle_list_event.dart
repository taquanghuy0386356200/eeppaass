part of 'vehicle_list_bloc.dart';

abstract class VehicleListEvent extends Equatable {}

class VehicleListFetched extends VehicleListEvent {
  @override
  List<Object> get props => [];
}

class VehicleListRefreshed extends VehicleListEvent {
  @override
  List<Object> get props => [];
}

class VehicleListLoadMore extends VehicleListEvent {
  @override
  List<Object> get props => [];
}

class VehicleListSearched extends VehicleListEvent {
  final String? plateNumber;

  VehicleListSearched({
    this.plateNumber,
  });

  @override
  List<Object?> get props => [plateNumber];
}
