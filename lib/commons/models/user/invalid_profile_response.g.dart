// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invalid_profile_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InValidContractProfile _$InValidContractProfileFromJson(
        Map<String, dynamic> json) =>
    InValidContractProfile(
      contractProfileId: json['contractProfileId'] as int?,
      custId: json['custId'] as int?,
      contractId: json['contractId'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      fileSize: json['fileSize'] as String?,
      filePath: json['filePath'] as String?,
      documentTypeName: json['documentTypeName'] as String?,
      status: json['status'] as String?,
      ocrStatus: json['ocrStatus'] as int?,
      ocrResult: json['ocrResult'] as String?,
      ocrComment: json['ocrComment'] as String?,
    );

Map<String, dynamic> _$InValidContractProfileToJson(
        InValidContractProfile instance) =>
    <String, dynamic>{
      'contractProfileId': instance.contractProfileId,
      'custId': instance.custId,
      'contractId': instance.contractId,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'fileSize': instance.fileSize,
      'filePath': instance.filePath,
      'documentTypeName': instance.documentTypeName,
      'status': instance.status,
      'ocrStatus': instance.ocrStatus,
      'ocrResult': instance.ocrResult,
      'ocrComment': instance.ocrComment,
    };

InvalidProfileResponse _$InvalidProfileResponseFromJson(
        Map<String, dynamic> json) =>
    InvalidProfileResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map(
              (e) => InValidContractProfile.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$InvalidProfileResponseToJson(
        InvalidProfileResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
