import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

/// code : "BankPlus"
/// name : "BankPlus"
/// createUser : "admin"
/// createDate : "13/05/2021 00:00:00"
/// status : "1"
/// charge : 0

part 'topup_channel.g.dart';

@JsonEnum(alwaysCreate: true)
enum TopupChannelStatus {
  @JsonValue('0')
  inactive,
  @JsonValue('1')
  active,
  @JsonValue('unknown')
  unknown,
}

@JsonEnum(alwaysCreate: true)
enum TopupChannelCode {
  @JsonValue('BankPlus')
  BankPlus,
  @JsonValue('DomesticATM')
  DomesticATM,
  @JsonValue('InternationalCard')
  InternationalCard,
  @JsonValue('ViettelPay')
  ViettelPay,
  @JsonValue('MoMo')
  MoMo,
  @JsonValue('VNPAY')
  VNPAY,
  @JsonValue('BankTransfer')
  BankTransfer,
  @JsonValue('PayNow')
  PayNow,
  @JsonValue('MobileMoney')
  MobileMoney,
  @JsonValue('MB')
  MB,
  @JsonValue('BIDV')
  BIDV,
  @JsonValue('VPBank')
  VPBank,
  @JsonValue('Agribank')
  Agribank,
  @JsonValue('TPBank')
  TPBank,
  @JsonValue('VIB')
  VIB,
  @JsonValue('OCBBank')
  OCBBank,
  @JsonValue('PVcomBank')
  PVcomBank,
  @JsonValue('MSB')
  MSB,
  @JsonValue('ZaloPay')
  ZaloPay,
  @JsonValue('VnptMoney')
  VnptMoney,
  @JsonValue('unknown')
  unknown,
}

@JsonSerializable()
class TopupChannel extends Equatable {
  @JsonKey(unknownEnumValue: TopupChannelCode.unknown)
  final TopupChannelCode code;
  final String? name;
  final String? createUser;
  final String? createDate;

  @JsonKey(unknownEnumValue: TopupChannelStatus.unknown)
  TopupChannelStatus? status;

  final int? charge;
  late int? groupChannel;
  final String? description;

  TopupChannel({
    required this.code,
    this.name,
    this.createUser,
    this.createDate,
    this.status,
    this.charge,
    this.groupChannel,
    this.description,
  });

  factory TopupChannel.fromJson(Map<String, dynamic> json) =>
      _$TopupChannelFromJson(json);

  @override
  List<Object?> get props => [code, name];

  @override
  bool? get stringify => true;
}

@JsonSerializable()
class TopupChannelData {
  final List<TopupChannel>? listData;
  final int? count;

  TopupChannelData({
    this.listData,
    this.count,
  });

  factory TopupChannelData.fromJson(Map<String, dynamic> json) =>
      _$TopupChannelDataFromJson(json);
}

@JsonSerializable()
class TopupChannelResponse {
  final TopupChannelData? data;
  final MessageData? mess;

  TopupChannelResponse({
    this.data,
    this.mess,
  });

  factory TopupChannelResponse.fromJson(Map<String, dynamic> json) =>
      _$TopupChannelResponseFromJson(json);
}
