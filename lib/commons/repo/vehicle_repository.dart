import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/models/vehicle/update_vehicle_etag.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/models/vehicle/vehicle_debit.dart';
import 'package:simple_result/simple_result.dart';

abstract class IVehicleRepository {
  Future<Result<VehicleListDataNotNull, Failure>> getUserVehicles({
    int pageSize = 20,
    int startRecord = 0,
    List<int>? statuses,
    List<int>? activeStatuses,
    String? plateNumber,
    bool? inContract,
  });

  Future<Result<String?, Failure>> updateVehicleRFIDStatus({
    required String vehicleId,
    required bool isOpen,
    required String custId,
  });
  Future<Result<VehicleDebitData?, Failure>> getVehicleDebit({
    required String vehicleId,
    required String contractId,
  });
  Future<Result<VehicleDebitTransactionResponse?, Failure>>
      getVehicleTransactionDebit({
    required String vehicleId,
  });
}

class VehicleRepository implements IVehicleRepository {
  final CrmClient _crmClient;

  VehicleRepository({required CrmClient crmClient}) : _crmClient = crmClient;

  @override
  Future<Result<VehicleListDataNotNull, Failure>> getUserVehicles({
    int pageSize = 20,
    int startRecord = 0,
    List<int>? statuses,
    List<int>? activeStatuses,
    String? plateNumber,
    bool? inContract,
  }) async {
    try {
      final response = await _crmClient.getUserVehicles(
        pageSize: pageSize,
        startRecord: startRecord,
        plateNumber: plateNumber,
        activeStatuses: activeStatuses,
        statuses: statuses,
        inContract: inContract,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(VehicleListDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> updateVehicleRFIDStatus({
    required String vehicleId,
    required bool isOpen,
    required String custId,
  }) async {
    try {
      final rfid = isOpen ? '4' : '3';
      final reasonId = isOpen ? '42' : '38';
      final request = isOpen
          ? UpdateVehicleEtag(
              reasonId: reasonId,
              custId: custId,
              actTypeId: Constant.openETAG,
            )
          : UpdateVehicleEtag(
              reasonId: reasonId,
              custId: custId,
            );
      final response = await _crmClient.updateVehicleRFIDStatus(
          vehicleId: vehicleId, rfid: rfid, request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<VehicleDebitData, Failure>> getVehicleDebit({
    required String vehicleId,
    required String contractId,
  }) async {
    try {
      final response = await _crmClient.getDebit(
          vehicleId: vehicleId, contractId: contractId);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(VehicleDebitData(
        statusDebit: response.data?.statusDebit,
        debitStatusName: response.data?.debitStatusName,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<VehicleDebitTransactionResponse, Failure>>
      getVehicleTransactionDebit({
    required String vehicleId,
  }) async {
    try {
      final response =
          await _crmClient.getDebitTransaction(vehicleId: vehicleId);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseCodeFailure(
              message: response.mess?.description ?? '',
              code: response.mess?.code),
        );
      }

      return Result.success(
          VehicleDebitTransactionResponse(mess: response.mess));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
