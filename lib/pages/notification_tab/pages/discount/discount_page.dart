import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/notification_tab/pages/event/widget/event_notification_card.dart';
import 'package:epass/pages/notification_tab/pages/event/widget/event_notification_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/gen/assets.gen.dart';

class DiscountPage extends StatefulWidget {
  const DiscountPage({Key? key}) : super(key: key);

  @override
  State<DiscountPage> createState() => _DiscountPageState();
}

class _DiscountPageState extends State<DiscountPage> {
  late RefreshController _refreshController;
  final  _searchController  = TextEditingController();

  @override
  void initState() {
    super.initState();
    context.read<DiscountNotificationBloc>().add(const NotificationFetched(notificationType: 4));
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<DiscountNotificationBloc, NotificationState>(
      listener: (context, state) {
        if (state.error == null) {
          _refreshController.refreshCompleted();
          _refreshController.loadComplete();
        } else if (state.error != null) {
          _refreshController.refreshFailed();
          _refreshController.loadFailed();

          if (state.listData.isNotEmpty) {
            showErrorSnackBBar(
              context: context,
              message: state.error ?? 'Có lỗi xảy ra',
            );
          }
        }
      },
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 24.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: PrimaryTextField(
                controller: _searchController,
                onClear: (){
                  context.read<DiscountNotificationBloc>().add( NotificationSearched(_searchController.text));
                },
                hintText: 'Tìm kiếm',
                onChanged: (value) => context.read<DiscountNotificationBloc>().add(NotificationSearched(value)),
                suffix: Assets.icons.search.svg(
                  width: 24.r,
                  height: 24.r,
                  color: ColorName.borderColor,
                ),
              ),
            ),
            SizedBox(height: 16.h),
            Expanded(
              child: Builder(
                builder: (_) {
                  if (state.isLoading || state.isRefreshing && state.listData.isEmpty) {
                    return const EventNotificationShimmerLoading();
                  } else if (state.error != null && state.listData.isEmpty) {
                    return Center(
                      child: Text(state.error ?? 'Có lỗi xảy ra'),
                    );
                  }
                  final listData = state.listData;
                  if (listData.isEmpty) {
                    return NoDataPage(
                      onTap: () => context.read<DiscountNotificationBloc>().add(const NotificationRefreshed()),
                      title: '',
                      description: 'Chưa có chương trình ưu đãi cho bạn',
                    );
                  }

                  return SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    enablePullUp: !state.isFull,
                    onRefresh: () => context.read<DiscountNotificationBloc>().add(const NotificationRefreshed()),
                    onLoading: () => context.read<DiscountNotificationBloc>().add(const NotificationLoadMore()),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        final notification = listData[index];
                        return EventNotificationCard(
                          notification: notification,
                          onTap: () {
                            final notificationId = notification.notificationMsgId;
                            if (notificationId != null) {
                              context.read<DiscountNotificationBloc>().add(NotificationRead(notificationId));
                            }
                          },
                        );
                      },
                      separatorBuilder: (context, index) => Divider(
                        color: ColorName.disabledBorderColor,
                        height: 0.5,
                        thickness: 0.5,
                        indent: 60.w,
                        endIndent: 24.w,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
