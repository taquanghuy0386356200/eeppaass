import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PrimaryButton extends StatelessWidget {
  final void Function()? onTap;
  final String title;
  final EdgeInsets? padding;
  final double borderRadius;
  final double? paddingButton;
  final bool enabled;
  final double? widthCustom;
  final double? heightCustom;

  const PrimaryButton({
    Key? key,
    this.onTap,
    required this.title,
    this.padding,
    this.borderRadius = 12.0,
    this.paddingButton,
    this.widthCustom,
    this.heightCustom,
    this.enabled = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthCustom,
      height: heightCustom,
      padding: (paddingButton != null)
          ? EdgeInsets.symmetric(
              horizontal: paddingButton!.w,
            )
          : null,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(borderRadius),
        color: enabled ? null : Colors.grey.shade300,
        gradient: enabled
            ? const LinearGradient(
                colors: <Color>[
                  ColorName.primaryGradientStart,
                  ColorName.primaryGradientEnd,
                ],
              )
            : null,
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: padding ??
              EdgeInsets.symmetric(
                horizontal: 16.w,
                vertical: 12.h,
              ),
          visualDensity: VisualDensity.standard,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
          primary: enabled ? Colors.white : Colors.grey.shade600,
        ),
        onPressed: onTap,
        child: Text(
          title,
          style: Theme.of(context).textTheme.button!.copyWith(
                fontSize: 16.sp,
                fontWeight: FontWeight.w700,
                color: enabled ? Colors.white : Colors.grey.shade600,
              ),
        ),
      ),
    );
  }
}
