// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'topup_channel_fee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopupChannelFee _$TopupChannelFeeFromJson(Map<String, dynamic> json) =>
    TopupChannelFee(
      fee: json['fee'] as int?,
      value: json['value'] as int?,
    );

Map<String, dynamic> _$TopupChannelFeeToJson(TopupChannelFee instance) =>
    <String, dynamic>{
      'fee': instance.fee,
      'value': instance.value,
    };

TopupChannelFeeResponse _$TopupChannelFeeResponseFromJson(
        Map<String, dynamic> json) =>
    TopupChannelFeeResponse(
      data: json['data'] == null
          ? null
          : TopupChannelFee.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TopupChannelFeeResponseToJson(
        TopupChannelFeeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
