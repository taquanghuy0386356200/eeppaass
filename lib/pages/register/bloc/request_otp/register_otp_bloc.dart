import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/register_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'register_otp_event.dart';

part 'register_otp_state.dart';

class RegisterOtpBloc extends Bloc<RegisterOtpEvent, RegisterOtpState> {
  final IRegisterRepository _registerRepository;

  RegisterOtpBloc({required IRegisterRepository registerRepository})
      : _registerRepository = registerRepository,
        super(RegisterOtpInitial()) {
    on<RegisterOtpRequested>(_onRegisterOtpRequested);
  }

  FutureOr<void> _onRegisterOtpRequested(
    RegisterOtpRequested event,
    Emitter<RegisterOtpState> emit,
  ) async {
    emit(RegisterOtpRequestedInProgress());

    final result = await _registerRepository.requestOTPRegister(phoneNumber: event.phoneNumber);

    result.when(
      success: (success) => emit(RegisterOtpRequestedSuccess(
        reRequestedEvent: event,
        submittedSuccessTime: DateTime.now(),
      )),
      failure: (failure) => emit(RegisterOtpRequestedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
