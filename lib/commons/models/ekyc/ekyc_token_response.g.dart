// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ekyc_token_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EkycTokenResponse _$EkycTokenResponseFromJson(Map<String, dynamic> json) =>
    EkycTokenResponse(
      code: json['code'] as int?,
      message: json['message'] as String?,
      token: json['token'] as String?,
      request_id: json['request_id'] as String?,
    );

Map<String, dynamic> _$EkycTokenResponseToJson(EkycTokenResponse instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'token': instance.token,
      'request_id': instance.request_id,
    };
