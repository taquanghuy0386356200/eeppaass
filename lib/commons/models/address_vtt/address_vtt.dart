import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

/// areaCode : "A076"
/// areaType : "1"
/// province : "A076"
/// center : "3"
/// name : "An Giang"
/// status : "1"
/// vtMapCode : "89"
/// vnCode : "89"
/// vnName : "An Giang"
/// population : 2177448
/// square : 3406000
/// households : 506540

part 'address_vtt.g.dart';

@JsonSerializable()
class AddressVTT extends DropdownItem with EquatableMixin {
  AddressVTT({
    this.areaCode,
    this.areaType,
    this.province,
    this.district,
    this.precinct,
    this.center,
    this.name,
    this.status,
    this.vtMapCode,
    this.vnCode,
    this.vnName,
    this.population,
    this.square,
    this.households,
  }) : super(title: name, value: areaCode);

  final String? areaCode;
  final String? areaType;
  final String? province;
  final String? district;
  final String? precinct;
  final String? center;
  final String? name;
  final String? status;
  final String? vtMapCode;
  final String? vnCode;
  final String? vnName;
  final int? population;
  final int? square;
  final int? households;

  factory AddressVTT.fromJson(Map<String, dynamic> json) => _$AddressVTTFromJson(json);

  @override
  List<Object?> get props => [areaCode];

  @override
  String toString() {
    return name ?? '';
  }
}

@JsonSerializable()
class AddressVTTResponse {
  final List<AddressVTT>? data;
  final MessageData? mess;

  AddressVTTResponse({this.data, this.mess});

  factory AddressVTTResponse.fromJson(Map<String, dynamic> json) => _$AddressVTTResponseFromJson(json);
}
