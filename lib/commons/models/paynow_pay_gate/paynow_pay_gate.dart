import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:epass/flavors.dart';

class PayNowPayGateRequest {
  /// Mã hoá đơn. Trường hợp đối tác không có nhu cầu truyền mã hoá đơn thì
  /// đặt giá trị bằng với order_id. Lưu ý không dùng tiếng Việt.
  final String billCode;

  /// Mã lệnh. Giá trị cố định là: PAYMENT
  final String command;

  /// Nội dung giao dịch/ đơn hàng
  final String desc;

  /// User ID của đối tác. Mặc định là số điện thoại của khách hàng
  final String merchantMsisdn;

  /// Mã đối tác mà Viettel đã cung cấp
  final String merchantCode = F.viettelPayGateMerchantCode;

  /// Mã giao dịch duy nhất bên phía đối tác. Lưu ý không dùng tiếng Việt
  final String orderId;

  /// Địa chỉ để chuyển sau khi KH thanh toán. Url này sử dụng trong trường
  /// hợp đối tác không truyền trong quá trình thanh toán.
  final String returnUrl = '/success';

  /* Hình thức thanh toán khách hàng lựa chọn từ App/Web của đơn vị chấp nhận
  thanh toán:
    ViettelPay: Thanh toán qua hình thức ViettelPay
    BankPlus: Thanh toán qua hình thức Bankplus
    InternationalCard: Thanh toán qua hình thức Thẻ quốc tế
    DomesticATM: Thanh toán qua hình thức Thẻ nội địa
    Trong 1 giao dịch thanh toán đối tác chỉ được truyền sang 1 hình thức
     thanh toán duy nhất.
    Không được truyền payment_method không có giá trị. Trường hợp đối tác
    không truyền payment_method hoặc payment_method không có giá trị thì
    khách hàng sẽ được lựa chọn các hình thức thanh toán mà Cổng thanh
    toán ViettelPay cung cấp theo hợp đồng kết nối giữa 2 bên
 */
  final String paymentMethod;

  /// Địa chỉ để chuyển sau nếu KH hủy giao dịch
  final String cancelUrl = '/cancel';

  final int transAmount;

  /// Phiên bản kết nối. Giá trị cố định là: 2.0
  final double version = 2.0;

  /// Chuỗi mã hóa tạo ra dựa trên các trường dữ liệu truyền sang và được
  /// encode UTF-8: bao gồm ( access_code + billcode + command + merchant_code
  /// + order_id + trans_amount + version)
  // final String checkSum;

  PayNowPayGateRequest({
    required this.billCode,
    required this.command,
    required this.desc,
    required this.merchantMsisdn,
    required this.orderId,
    required this.paymentMethod,
    required this.transAmount,
  });

  String get paynowGateUrl {
    final checkSumStr =
        '${F.viettelPayGateAccessCode}$billCode$command$merchantCode$orderId'
        '$transAmount$version';
    final key = utf8.encode(F.viettelPayGateHashCode);
    final data = utf8.encode(checkSumStr);

    final hmacSha1 = Hmac(sha1, key);
    final digest = hmacSha1.convert(data);
    final checkSumBase64 = base64.encode(digest.bytes);

    final params = 'billcode=$billCode'
        '&command=$command'
        '${desc.isNotEmpty ? '&desc=$desc' : ''}'
        '&merchant_code=$merchantCode'
        '&order_id=$orderId'
        '&return_url=$returnUrl'
        '&payment_method=$paymentMethod'
        '&cancel_url=$cancelUrl'
        '&trans_amount=$transAmount'
        '&version=$version'
        '&check_sum=$checkSumBase64';

    return '${F.viettelPayGateUrl}?${Uri.encodeFull(params)}';
  }
}

class PayNowPayGateRequestResponse {
  final String orderId;
  final int paymentStatus;
  final String? errorCode;
  final int transAmount;

  PayNowPayGateRequestResponse({
    required this.orderId,
    required this.paymentStatus,
    this.errorCode,
    required this.transAmount,
  });
}
