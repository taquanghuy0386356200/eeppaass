import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_topup/bloc/transaction_topup_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionTopupSearchForm extends StatefulWidget {
  final DateTime? topupDateFrom;
  final DateTime? topupDateTo;

  const TransactionTopupSearchForm({
    Key? key,
    this.topupDateFrom,
    this.topupDateTo,
  }) : super(key: key);

  @override
  State<TransactionTopupSearchForm> createState() =>
      _TransactionTopupSearchFormState();
}

class _TransactionTopupSearchFormState
    extends State<TransactionTopupSearchForm> {
  final _topupDateFromController = TextEditingController();
  final _topupDateToController = TextEditingController();

  DateTime? _topupDateFrom;
  DateTime? _topupDateTo;

  @override
  void initState() {
    super.initState();
    context.read<TransactionTopupBloc>().isSearching = false;
    _topupDateFrom = widget.topupDateFrom;
    _topupDateTo = widget.topupDateTo;

    if (_topupDateFrom != null) {
      _topupDateFromController.text =
          Jiffy(_topupDateFrom).format('dd/MM/yyyy');
    }

    if (_topupDateTo != null) {
      _topupDateToController.text = Jiffy(_topupDateTo).format('dd/MM/yyyy');
    }
  }

  @override
  void dispose() {
    _topupDateFromController.dispose();
    _topupDateToController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Positioned(
            top: 12.h,
            right: 12.w,
            child: SplashIconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: ColorName.borderColor,
              ),
              onTap: () => Navigator.of(context).pop(),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'TÌM KIẾM',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(height: 24.h),
                PrimaryTextField(
                  controller: _topupDateFromController,
                  labelText: 'Ngày giao dịch: từ ngày',
                  hintText: 'Từ ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showTopupDateRangePicker,
                  onClear: () => _topupDateFrom = null,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _topupDateToController,
                  labelText: 'Ngày giao dịch: đến ngày',
                  hintText: 'Đến ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showTopupDateRangePicker,
                  onClear: () => _topupDateTo = null,
                ),
                SizedBox(height: 34.h),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: SecondaryButton(
                          onTap: () {
                            context
                                .read<TransactionTopupBloc>()
                                .add(const TransactionTopupFetched());
                            context.read<TransactionTopupBloc>().isSearching =
                                false;
                            Navigator.of(context).pop();
                          },
                          title: 'Đặt lại',
                        ),
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: PrimaryButton(
                          title: 'Tìm kiếm',
                          onTap: () {
                            context
                                .read<TransactionTopupBloc>()
                                .add(TransactionTopupSearched(
                                  topupDateFrom: _topupDateFrom,
                                  topupDateTo: _topupDateTo,
                                ));
                            context.read<TransactionTopupBloc>().isSearching =
                                true;
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // SizedBox(height: 16.h),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _showTopupDateRangePicker() async {
    DateTimeRange? initialDateRange;
    if (_topupDateFrom != null) {
      initialDateRange = DateTimeRange(
        start: _topupDateFrom!,
        end: _topupDateTo ?? Jiffy().dateTime,
      );
    }

    final dateRange = await showDateRangePicker(
          context: context,
          locale: const Locale('vi'),
          initialDateRange: initialDateRange,
          firstDate: Jiffy().subtract(years: 2).startOf(Units.DAY).dateTime,
          lastDate: Jiffy().dateTime,
          initialEntryMode: DatePickerEntryMode.calendarOnly,
        ) ??
        initialDateRange;

    _topupDateFrom = dateRange?.start;
    _topupDateTo = dateRange?.end;

    if (_topupDateFrom != null) {
      _topupDateFromController.text =
          Jiffy(_topupDateFrom).format('dd/MM/yyyy');
    }

    if (_topupDateTo != null) {
      _topupDateToController.text = Jiffy(_topupDateTo).format('dd/MM/yyyy');
    }
  }
}
