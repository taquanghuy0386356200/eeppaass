import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/parking_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'passcode_bottom_sheet_event.dart';
part 'passcode_bottom_sheet_state.dart';

class PasscodeBottomSheetBloc extends Bloc<PasscodeBottomSheetEvent, PasscodeBottomSheetState> {
  final IParkingRepository _parkingRepository;
  final AppBloc _appBloc;

  PasscodeBottomSheetBloc({
    required IParkingRepository parkingRepository,
    required AppBloc appBloc
  }) : _parkingRepository = parkingRepository,
        _appBloc = appBloc,
        super(const PasscodeBottomSheetState()) {
    on<PasscodeBottomSheetEvent>((event, emit) {
      on<ChangePasscodeSubmitted>(_onChangePasscodeSubmitted);
      on<FormValidated>(_onFormValidated);
    });
  }

  FutureOr<void> _onChangePasscodeSubmitted(event, emit) async {
    emit(const ChangePasscodeFormSubmittedInProgress());


  }

  FutureOr<void> _onFormValidated(event, emit) async {
    emit(const ChangePasscodeFormValidated());
  }


}
