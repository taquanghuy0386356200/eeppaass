// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buy_ticket_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BuyTicketResponseItem _$BuyTicketResponseItemFromJson(
        Map<String, dynamic> json) =>
    BuyTicketResponseItem(
      servicePlanTypeId: $enumDecodeNullable(
          _$ServicePlanTypeEnumMap, json['servicePlanTypeId'],
          unknownValue: ServicePlanType.other),
      vehicleGroupId: json['vehicleGroupId'] as int?,
      stationType: json['stationType'] as int?,
      stationId: json['stationId'] as int?,
      chargeMethodId: $enumDecodeNullable(
          _$MethodChargeIntEnumMap, json['chargeMethodId'],
          unknownValue: MethodChargeInt.unknown),
      autoRenew: json['autoRenew'] as int?,
      createDateFrom: json['createDateFrom'] as String?,
      createDateTo: json['createDateTo'] as String?,
      effDate: json['effDate'] as String?,
      plateNumber: json['plateNumber'] as String?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      netWeight: (json['netWeight'] as num?)?.toDouble(),
      seatNumber: json['seatNumber'] as int?,
      epc: json['epc'] as String?,
      booCode: json['booCode'] as String?,
      expDate: json['expDate'] as String?,
      vehicleId: json['vehicleId'] as int?,
      reasons: json['reasons'] as String?,
      isChargeTicket: json['isChargeTicket'] as bool?,
      message: json['message'] as String?,
      dateOfUse: json['dateOfUse'] as String?,
    );

Map<String, dynamic> _$BuyTicketResponseItemToJson(
        BuyTicketResponseItem instance) =>
    <String, dynamic>{
      'servicePlanTypeId': _$ServicePlanTypeEnumMap[instance.servicePlanTypeId],
      'vehicleGroupId': instance.vehicleGroupId,
      'stationType': instance.stationType,
      'stationId': instance.stationId,
      'chargeMethodId': _$MethodChargeIntEnumMap[instance.chargeMethodId],
      'autoRenew': instance.autoRenew,
      'createDateFrom': instance.createDateFrom,
      'createDateTo': instance.createDateTo,
      'effDate': instance.effDate,
      'plateNumber': instance.plateNumber,
      'vehicleTypeId': instance.vehicleTypeId,
      'netWeight': instance.netWeight,
      'seatNumber': instance.seatNumber,
      'epc': instance.epc,
      'booCode': instance.booCode,
      'expDate': instance.expDate,
      'vehicleId': instance.vehicleId,
      'reasons': instance.reasons,
      'isChargeTicket': instance.isChargeTicket,
      'message': instance.message,
      'dateOfUse': instance.dateOfUse,
    };

const _$ServicePlanTypeEnumMap = {
  ServicePlanType.daily: 1,
  ServicePlanType.monthly: 4,
  ServicePlanType.quarterly: 5,
  ServicePlanType.other: 'other',
};

const _$MethodChargeIntEnumMap = {
  MethodChargeInt.normal: 1,
  MethodChargeInt.block: 2,
  MethodChargeInt.unknown: 'unknown',
};

BuyTicketResponseData _$BuyTicketResponseDataFromJson(
        Map<String, dynamic> json) =>
    BuyTicketResponseData(
      listSuccess: (json['listSuccess'] as List<dynamic>?)
          ?.map(
              (e) => BuyTicketResponseItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      listFail: (json['listFail'] as List<dynamic>?)
          ?.map(
              (e) => BuyTicketResponseItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BuyTicketResponseDataToJson(
        BuyTicketResponseData instance) =>
    <String, dynamic>{
      'listSuccess': instance.listSuccess,
      'listFail': instance.listFail,
    };

BuyTicketResponse _$BuyTicketResponseFromJson(Map<String, dynamic> json) =>
    BuyTicketResponse(
      data: json['data'] == null
          ? null
          : BuyTicketResponseData.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BuyTicketResponseToJson(BuyTicketResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
