import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/transaction_vehicle/transaction_vehicle.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'transaction_vehicle_event.dart';

part 'transaction_vehicle_state.dart';

class TransactionVehicleBloc
    extends Bloc<TransactionVehicleEvent, TransactionVehicleState> {
  final ITransactionHistoryRepository _transRepository;

  final int _pageSize;
  int _startRecord = 0;
  bool isSearching = false;

  TransactionVehicleBloc({
    int pageSize = 10,
    required ITransactionHistoryRepository transactionHistoryRepository,
  })  : _pageSize = pageSize,
        _transRepository = transactionHistoryRepository,
        super(TransactionVehicleState(
          listData: const [],
          timestampInFrom: Jiffy().subtract(weeks: 1).dateTime,
          timestampInTo: Jiffy().dateTime,
          error: null,
        )) {
    on<TransactionVehicleFetched>(_onTransactionVehicleFetched);
    on<TransactionVehicleRefreshed>(_onTransactionVehicleRefreshed);
    on<TransactionVehicleLoadMore>(_onTransactionVehicleLoadMore);
    on<TransactionVehicleSearched>(_onTransactionVehicleSearched);
  }

  FutureOr<void> _onTransactionVehicleFetched(
    TransactionVehicleFetched event,
    Emitter<TransactionVehicleState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      plateNumber: null,
      timestampInFrom: Jiffy().subtract(weeks: 1).dateTime,
      timestampInTo: Jiffy().dateTime,
      error: null,
    ));

    final result = await _getTransactionVehicle();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            plateNumber: state.plateNumber,
            isFull: data.listData.length == data.count,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: state.plateNumber,
      )),
    );
  }

  FutureOr<void> _onTransactionVehicleRefreshed(
      TransactionVehicleRefreshed event,
      Emitter<TransactionVehicleState> emit) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      plateNumber: state.plateNumber,
      error: null,
    ));

    Result<TransactionVehicleDataNotNull, Failure> result =
        await _getTransactionVehicle();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
        plateNumber: state.plateNumber,
      )),
    );
  }

  FutureOr<void> _onTransactionVehicleLoadMore(
    TransactionVehicleLoadMore event,
    Emitter<TransactionVehicleState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      plateNumber: state.plateNumber,
      error: null,
    ));

    final result = await _getTransactionVehicle();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(state.copyWith(
          isLoadingMore: false,
          listData: listData,
          isFull: listData.length == data.count,
          plateNumber: state.plateNumber,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        error: failure.message,
        plateNumber: state.plateNumber,
      )),
    );
  }

  FutureOr<void> _onTransactionVehicleSearched(
    TransactionVehicleSearched event,
    Emitter<TransactionVehicleState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
      plateNumber: event.plateNumber,
      timestampInFrom: event.timestampInFrom,
      timestampInTo: event.timestampInTo,
      error: null,
    ));

    _startRecord = 0;

    final result = await _getTransactionVehicle();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: state.plateNumber,
      )),
    );
  }

  Future<Result<TransactionVehicleDataNotNull, Failure>>
      _getTransactionVehicle() async {
    final result = await _transRepository.getTransactionVehicle(
      pageSize: _pageSize,
      startRecord: _startRecord,
      plateNumber: state.plateNumber,
      timestampInFrom: Jiffy(state.timestampInFrom).format('dd/MM/yyyy'),
      timestampInTo: Jiffy(state.timestampInTo).format('dd/MM/yyyy'),
    );
    return result;
  }
}
