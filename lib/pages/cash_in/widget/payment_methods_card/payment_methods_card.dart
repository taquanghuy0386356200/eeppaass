import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/widgets/animations/shimmer_widget.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/modal/alert_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/bloc/payment_methods_card_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/payment_methods_icon.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/payment_methods_group_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'dart:io' show Platform;

class Item {
  Item({
    required this.headerValue,
    required this.key,
  });

  String headerValue;
  int key;
}

TopupChannel selectedMethod = TopupChannel(code: TopupChannelCode.unknown);

final List<Item> data = [
  Item(headerValue: 'Ví điện tử', key: 0),
  Item(headerValue: 'Mobile Banking', key: 1),
  Item(headerValue: 'Thẻ ATM nội địa', key: 2),
  Item(headerValue: 'Thẻ tín dụng/ ghi nợ', key: 3),
];
final List<Item> dataNoBank = [
  Item(headerValue: 'Ví điện tử', key: 0),
  Item(headerValue: 'Thẻ ATM nội địa', key: 2),
  Item(headerValue: 'Thẻ tín dụng/ ghi nợ', key: 3),
];

class PaymentMethodsCard extends StatefulWidget {
  final ValueChanged<TopupChannel>? onValueChanged;

  const PaymentMethodsCard({
    Key? key,
    this.onValueChanged,
  }) : super(key: key);

  @override
  State<PaymentMethodsCard> createState() => _PaymentMethodsCardState();
}

class _PaymentMethodsCardState extends State<PaymentMethodsCard> {
  final TextEditingController _searchQuery = TextEditingController();
  List<TopupChannel> _foundMethod = [];
  bool groupChannelOne = false;
  String updateByPlatform = '';

  @override
  void initState() {
    context.read<CashInAmountBloc>().add(CashInTargetLastRecharge());
    updateByPlatform = Platform.isAndroid
        ? "Tính năng này chỉ hỗ trợ cho phiên bản ứng dụng ePass từ 1.4.24. Quý khách vui lòng cập nhật trước khi sử dụng."
        : "Tính năng này chỉ hỗ trợ cho phiên bản ứng dụng ePass từ 2.4.1. Quý khách vui lòng cập nhật trước khi sử dụng.";
    super.initState();
  }

  @override
  void didUpdateWidget(PaymentMethodsCard oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<PaymentMethodsCardBloc>();
    final selected = context.select((CashInAmountBloc bloc) {
      final state = bloc.state;
      return state is CashInAmountChangedSuccess ? state.topupChannel : null;
    });
    return BlocConsumer<PaymentMethodsCardBloc, PaymentMethodsCardState>(
      listener: (context, state) async {
        if (state is PaymentMethodsFetchedFailure) {
          showErrorSnackBBar(context: context, message: state.message);
        }
        if (state is PaymentMethodsFetchedSuccess) {
          _foundMethod = await bloc.listTopupChannelsCustomize(
              defaultTopupChannels: state.paymentMethods);
          // Nhóm theo groupChannel và status là 0
          groupChannelOne = _foundMethod
                  .where((method) =>
                      method.groupChannel == 1 &&
                      method.status == TopupChannelStatus.active)
                  .toList()
                  .isNotEmpty
              ? false
              : true;

          if (selected != null) {
            setState(() {
              final listSelected = state.paymentMethods
                  .where((element) => element.code.name == selected)
                  .toList();
              if (listSelected.isNotEmpty) {
                selectedMethod = listSelected.first;
              }
              // context
              //   .read<PaymentMethodsCardBloc>()
              //   .add(PaymentMethodChanged(selectedMethod));
            });
          }
        }
      },
      builder: (context, state) {
        if (state is PaymentMethodsFetchedFailure) {
          return const SizedBox.shrink();
        }
        return ShadowCard(
          shadowOpacity: 0.2,
          child: Padding(
            padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 6.w),
                  child: Text(
                    'Phương thức thanh toán',
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          color: ColorName.textGray1,
                        ),
                  ),
                ),
                SizedBox(height: 16.h),
                if (state is PaymentMethodsCardLoading)
                  ShimmerColor(
                    child: ListView.separated(
                      shrinkWrap: true,
                      itemCount: 3,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return ShimmerWidget(
                          width: double.infinity,
                          height: 54.h,
                        );
                      },
                      separatorBuilder: (_, __) => SizedBox(height: 20.h),
                    ),
                  ),
                if (state is PaymentMethodsFetchedSuccess)
                  ShadowCard(
                    shadowOpacity: 0.2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          cardColor: Colors.white,
                        ),
                        child: groupChannelOne
                            ? ExpansionPanelList.radio(
                                elevation: 4,
                                animationDuration:
                                    const Duration(milliseconds: 500),
                                initialOpenPanelValue:
                                    selectedMethod.groupChannel,
                                expandedHeaderPadding:
                                    const EdgeInsets.symmetric(vertical: 0),
                                children: dataNoBank.map<ExpansionPanel>(
                                  (Item item) {
                                    return ExpansionPanelRadio(
                                      canTapOnHeader: true,
                                      headerBuilder: (BuildContext context,
                                          bool isExpanded) {
                                        return Column(
                                          children: [
                                            Container(
                                              width: ScreenUtil().screenWidth,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 16.w,
                                                vertical: 12.h,
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  PaymentMethodsGroupIcon(
                                                    group: item.key,
                                                    height: 30.h,
                                                    width: 30.w,
                                                  ),
                                                  SizedBox(width: 12.w),
                                                  Text(
                                                    item.headerValue,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle1!
                                                        .copyWith(
                                                            color: ColorName
                                                                .textGray1,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                      body: Column(
                                        children: [
                                          ListView.separated(
                                            shrinkWrap: true,
                                            itemCount: 1,
                                            physics:
                                                const NeverScrollableScrollPhysics(),
                                            itemBuilder: (context, index) {
                                              index = item.key;
                                              return Column(
                                                children: [
                                                  index == 2 || index == 3
                                                      ? SizedBox(
                                                          height: 10.h,
                                                        )
                                                      : Padding(
                                                          padding: EdgeInsets
                                                              .fromLTRB(16.w, 0,
                                                                  16.w, 8.h),
                                                          child: TextFormField(
                                                            controller:
                                                                _searchQuery,
                                                            keyboardType:
                                                                TextInputType
                                                                    .name,
                                                            onChanged: (value) {
                                                              setState(
                                                                () {
                                                                  value = _searchQuery
                                                                      .text
                                                                      .toString()
                                                                      .toUpperCase();
                                                                  _foundMethod = state
                                                                      .paymentMethods
                                                                      .where((element) => element
                                                                          .name
                                                                          .toString()
                                                                          .toUpperCase()
                                                                          .contains(
                                                                              value))
                                                                      .toList();
                                                                },
                                                              );
                                                            },
                                                            decoration:
                                                                InputDecoration(
                                                              hintText:
                                                                  'Tìm kiếm',
                                                              suffixIcon:
                                                                  IconButton(
                                                                icon: const Icon(
                                                                    Icons
                                                                        .clear),
                                                                onPressed: () {
                                                                  setState(() {
                                                                    _searchQuery
                                                                        .clear();
                                                                    _foundMethod =
                                                                        state
                                                                            .paymentMethods;
                                                                  });
                                                                },
                                                              ),
                                                              focusedBorder: const UnderlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              ColorName.primaryColor)),
                                                              enabledBorder: const UnderlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              ColorName.primaryColor)),
                                                            ),
                                                          ),
                                                        ),
                                                  PaymentMethodList(
                                                    paymentMethods: _foundMethod
                                                        .where(
                                                          (element) =>
                                                              element
                                                                  .groupChannel ==
                                                              item.key,
                                                        )
                                                        .toList(),
                                                    onTap: (TopupChannel
                                                        topupChannel) async {
                                                      bloc.add(
                                                        PaymentMethodChanged(
                                                            topupChannel),
                                                      );
                                                    },
                                                  ),
                                                  SizedBox(height: 16.h),
                                                ],
                                              );
                                            },
                                            separatorBuilder: (_, __) =>
                                                SizedBox(height: 20.h),
                                          ),
                                          // ),
                                        ],
                                      ),
                                      value: item.key,
                                    );
                                  },
                                ).toList(),
                              )
                            : ExpansionPanelList.radio(
                                elevation: 4,
                                animationDuration:
                                    const Duration(milliseconds: 500),
                                initialOpenPanelValue:
                                    selectedMethod.groupChannel,
                                expandedHeaderPadding:
                                    const EdgeInsets.symmetric(vertical: 0),
                                children: data.map<ExpansionPanel>(
                                  (Item item) {
                                    return ExpansionPanelRadio(
                                      canTapOnHeader: true,
                                      headerBuilder: (BuildContext context,
                                          bool isExpanded) {
                                        return Column(
                                          children: [
                                            Container(
                                              width: ScreenUtil().screenWidth,
                                              padding: EdgeInsets.symmetric(
                                                horizontal: 16.w,
                                                vertical: 12.h,
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  PaymentMethodsGroupIcon(
                                                    group: item.key,
                                                    height: 30.h,
                                                    width: 30.w,
                                                  ),
                                                  SizedBox(width: 12.w),
                                                  Text(
                                                    item.headerValue,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .subtitle1!
                                                        .copyWith(
                                                            color: ColorName
                                                                .textGray1,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                      body: Column(
                                        children: [
                                          ListView.separated(
                                            shrinkWrap: true,
                                            itemCount: 1,
                                            physics:
                                                const NeverScrollableScrollPhysics(),
                                            itemBuilder: (context, index) {
                                              index = item.key;
                                              return Column(
                                                children: [
                                                  index == 2 || index == 3
                                                      ? SizedBox(
                                                          height: 10.h,
                                                        )
                                                      : Padding(
                                                          padding: EdgeInsets
                                                              .fromLTRB(16.w, 0,
                                                                  16.w, 8.h),
                                                          child: TextFormField(
                                                            controller:
                                                                _searchQuery,
                                                            keyboardType:
                                                                TextInputType
                                                                    .name,
                                                            onChanged: (value) {
                                                              setState(
                                                                () {
                                                                  value = _searchQuery
                                                                      .text
                                                                      .toString()
                                                                      .toUpperCase();
                                                                  _foundMethod = state
                                                                      .paymentMethods
                                                                      .where((element) => element
                                                                          .name
                                                                          .toString()
                                                                          .toUpperCase()
                                                                          .contains(
                                                                              value))
                                                                      .toList();
                                                                },
                                                              );
                                                            },
                                                            decoration:
                                                                InputDecoration(
                                                              hintText:
                                                                  'Tìm kiếm',
                                                              suffixIcon:
                                                                  IconButton(
                                                                icon:
                                                                    const Icon(
                                                                  Icons.clear,
                                                                ),
                                                                onPressed: () {
                                                                  setState(() {
                                                                    _searchQuery
                                                                        .clear();
                                                                    _foundMethod =
                                                                        state
                                                                            .paymentMethods;
                                                                  });
                                                                },
                                                              ),
                                                              focusedBorder: const UnderlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              ColorName.primaryColor)),
                                                              enabledBorder: const UnderlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              ColorName.primaryColor)),
                                                            ),
                                                          ),
                                                        ),
                                                  PaymentMethodList(
                                                    paymentMethods: _foundMethod
                                                        .where(
                                                          (element) =>
                                                              element
                                                                  .groupChannel ==
                                                              item.key,
                                                        )
                                                        .toList(),
                                                    onTap: (TopupChannel
                                                        topupChannel) async {
                                                      bloc.add(
                                                        PaymentMethodChanged(
                                                            topupChannel),
                                                      );
                                                    },
                                                  ),
                                                  SizedBox(height: 16.h),
                                                ],
                                              );
                                            },
                                            separatorBuilder: (_, __) =>
                                                SizedBox(height: 20.h),
                                          ),
                                          // ),
                                        ],
                                      ),
                                      value: item.key,
                                    );
                                  },
                                ).toList(),
                              ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class PaymentMethodList extends StatefulWidget {
  final List<TopupChannel> paymentMethods;
  final void Function(TopupChannel topupChannel)? onTap;

  const PaymentMethodList({
    Key? key,
    required this.paymentMethods,
    this.onTap,
  }) : super(key: key);

  @override
  State<PaymentMethodList> createState() => _PaymentMethodListState();
}

class _PaymentMethodListState extends State<PaymentMethodList> {
  TopupChannel? selectedTopupChannel;

  @override
  void initState() {
    setState(() {
      selectedTopupChannel = selectedMethod;
    });
    context
        .read<PaymentMethodsCardBloc>()
        .add(PaymentMethodChanged(selectedMethod));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final amount = context.select((CashInAmountBloc bloc) {
      final state = bloc.state;
      return state is CashInAmountChangedSuccess ? state.amount : null;
    });
    final selected = context.select((PaymentMethodsCardBloc bloc) {
      final state = bloc.state;
      return state is PaymentMethodsFetchedSuccess
          ? state.selectedPaymentMethod
          : null;
    });
    final contractNo = context.select(
      (CashInTargetBloc bloc) => bloc.state.contractNo,
    );
    final contractId = context.select(
      (CashInTargetBloc bloc) => bloc.state.contractId,
    );
    setState(() {
      selectedTopupChannel = selected;
    });
    final cashInEnabled = amount != null && contractNo != null;
    return SizedBox(
      width: double.infinity,
      child: Wrap(
        spacing: 2.w,
        runSpacing: 10.h,
        children: widget.paymentMethods.map((paymentMethod) {
          if (paymentMethod.status != TopupChannelStatus.active) {
            return const SizedBox.shrink();
          }
          return PaymentMethodButton(
            paymentMethod: paymentMethod,
            isSelected: paymentMethod == selectedTopupChannel,
            onTap: (paymentMethod) {
              setState(() {
                selectedTopupChannel = paymentMethod;
              });
              widget.onTap?.call(paymentMethod);
            },
            onDoubleTap: (topupChannel) {
              setState(() {
                selectedTopupChannel = paymentMethod;
              });
              widget.onTap?.call(paymentMethod);
              if (cashInEnabled == false) {
                showErrorSnackBBar(
                    context: context,
                    message: amount == null
                        ? 'Vui lòng nhập số tiền!'
                        : 'Vui lòng nhập số hợp đồng/biển số xe!');
              } else {
                context.read<CashInBloc>().add(
                      CheckCustomerType(
                        contractId: contractId!,
                        amount: amount!,
                      ),
                    );
              }
            },
          );
        }).toList(),
      ),
    );
  }
}

class PaymentMethodButton extends StatelessWidget {
  final TopupChannel paymentMethod;
  final bool isSelected;
  final void Function(TopupChannel topupChannel)? onTap;
  final void Function(TopupChannel topupChannel)? onDoubleTap;

  const PaymentMethodButton({
    Key? key,
    required this.paymentMethod,
    this.onTap,
    this.onDoubleTap,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: ScreenUtil().screenWidth,
      child: Column(
        children: [
          Material(
            color: isSelected ? ColorName.primaryColor : Colors.white,
            borderRadius: BorderRadius.circular(8.0),
            child: InkWell(
              onTap: () => onTap?.call(paymentMethod),
              onDoubleTap: () => onDoubleTap?.call(paymentMethod),
              child: Stack(
                children: [
                  SizedBox(
                    width: ScreenUtil().screenWidth,
                    child: Row(
                      children: [
                        SizedBox(width: 10.w),
                        Container(
                          decoration: BoxDecoration(
                              color: isSelected
                                  ? ColorName.primaryColor
                                  : ColorName.backgroundAvatar,
                              boxShadow: [
                                isSelected
                                    ? const BoxShadow(
                                        color: ColorName.primaryColor,
                                        spreadRadius: 0,
                                        blurRadius: 0,
                                        offset: Offset(0, 0),
                                      )
                                    : BoxShadow(
                                        color: ColorName.borderColor
                                            .withOpacity(0.3),
                                        spreadRadius: 3,
                                        blurRadius: 4,
                                        offset: const Offset(0, 2),
                                      ),
                              ],
                              // border: Border.all(color: ColorName.primaryColor),
                              borderRadius: BorderRadius.circular(8.0)),
                          padding: const EdgeInsets.all(6.0),
                          child: SizedBox(
                            height: 28.h,
                            width: 28.w,
                            child: PaymentMethodsIcon(
                              topupChannelCode: paymentMethod.code,
                            ),
                          ),
                        ),
                        SizedBox(width: 10.w),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '${paymentMethod.name}' ?? '',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      color: ColorName.textGray1,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.sp,
                                    ),
                              ),
                              paymentMethod.description != null
                                  ? Text(
                                      paymentMethod.description ?? '',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1!
                                          .copyWith(
                                            color: ColorName.textGray1,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12.sp,
                                          ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  : const SizedBox.shrink(),
                            ],
                          ),
                        ),
                        SizedBox(height: 14.h),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
