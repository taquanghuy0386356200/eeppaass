part of 'mb_pay_gate_bloc.dart';

abstract class MBPayGateState extends Equatable {
  const MBPayGateState();
}

class MBPayGateInitial extends MBPayGateState {
  @override
  List<Object> get props => [];
}

class MBPayGateInProgress extends MBPayGateState {
  @override
  List<Object> get props => [];
}

class MBGetTokenCompletedSuccess extends MBPayGateState {
  final String accessToken;
  final String tokenType;
  final int expiresIn;
  final String issuedAt;

  const MBGetTokenCompletedSuccess({
    required this.accessToken,
    required this.tokenType,
    required this.expiresIn,
    required this.issuedAt,
  });

  @override
  List<Object> get props => [accessToken, tokenType, expiresIn, issuedAt];
}

class MBGetTokenCompletedFailure extends MBPayGateState {
  final String message;

  const MBGetTokenCompletedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class MBCreateOrderCompletedSuccess extends MBPayGateState {
  final String clientMessageId;
  final String errorCode;
  final MBOrderData data;

  const MBCreateOrderCompletedSuccess({
    required this.clientMessageId,
    required this.errorCode,
    required this.data,
  });

  @override
  List<Object> get props => [clientMessageId];
}

class MBCreateOrderCompletedFailure extends MBPayGateState {
  final String message;

  const MBCreateOrderCompletedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class MBCheckSumCompletedSuccess extends MBPayGateState {
  final String clientMessageId;
  final String transactionID;
  final String errorCode;

  const MBCheckSumCompletedSuccess({
    required this.clientMessageId,
    required this.transactionID,
    required this.errorCode,
  });

  @override
  List<Object> get props => [clientMessageId, transactionID, errorCode];
}

class MBCheckSumCompletedFailure extends MBPayGateState {
  final String message;

  const MBCheckSumCompletedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class MBCheckPaymentResultCompletedSuccess extends MBPayGateState {
  final String clientMessageId;
  final String status;
  final String errorCode;

  const MBCheckPaymentResultCompletedSuccess({
    required this.clientMessageId,
    required this.status,
    required this.errorCode,
  });

  @override
  List<Object> get props => [clientMessageId, status, errorCode];
}

class MBCheckPaymentResultCompletedFailure extends MBPayGateState {
  final String message;

  const MBCheckPaymentResultCompletedFailure(this.message);

  @override
  List<Object> get props => [message];
}
