import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/parking/config_service_response.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home/bloc/home_bloc.dart';
import 'package:epass/pages/home/bloc/viettel_money_link_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/bloc/parking_setting_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/not_eligible_dialog.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/otp_bottom_sheet/otp_bottom_sheet.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/parking_shimmer_loading.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/passcode_bottom_sheet/bloc/passcode_bottom_sheet_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/passcode_bottom_sheet/passcode_bottom_sheet.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/policy_bottom_sheet/bloc/policy_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/policy_bottom_sheet/policy_bottom_sheet.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/verify_card/verify_config_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ParkingRoadSidePage extends StatelessWidget {
  const ParkingRoadSidePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      backgroundColor: Colors.white,
      leading: BackButton(onPressed: () => context.popRoute()),
      title: 'Dịch vụ đỗ xe lòng đường',
      resizeToAvoidBottomInset: true,
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                    margin: EdgeInsets.only(top: 16.h),
                    padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                    child: const ParkingSettingForm()),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ParkingSettingForm extends StatefulWidget {
  const ParkingSettingForm({
    Key? key,
  }) : super(key: key);

  @override
  State<ParkingSettingForm> createState() => _ParkingSettingFormState();
}

class _ParkingSettingFormState extends State<ParkingSettingForm> {
  ServiceModel? parkingService;

  @override
  void initState() {
    super.initState();
    context.read<ParkingSettingBloc>().add(const ServiceRoadSideFetched());
  }

  @override
  Widget build(BuildContext context) {
    final policyBloc = BlocProvider.of<PolicyBloc>(context);
    final parkingBloc = BlocProvider.of<ParkingSettingBloc>(context);
    final passCodeBloc = BlocProvider.of<PasscodeBottomSheetBloc>(context);
    final viettelMoneyLinkBloc = BlocProvider.of<ViettelMoneyLinkBloc>(context);
    return BlocListener<ViettelMoneyLinkBloc, ViettelMoneyLinkState>(
      listener: (context, state) {
        if (state is ViettelMoneyLinkAccountInProgress) {
          context.loaderOverlay.show();
        } else {
          context.loaderOverlay.hide();
          if (state is ViettelMoneyLinkAccountSuccess) {}
        }
      },
      child: BlocConsumer<ParkingSettingBloc, ParkingSettingState>(
          listener: (context, state) async {},
          builder: (context, state) {
            if (state.isDoing) {
              return const Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (state.isLoading) {
              return const ParkingShimmerLoading();
            } else if (state.error != null &&
                state.serviceVerifyConfig.isEmpty) {
              return CommonErrorPage(
                message: state.error,
                onTap: () => context
                    .read<ParkingSettingBloc>()
                    .add(const ServiceFetched()),
              );
            } else {
              for (var element in state.services) {
                if (element.code == 'PARKING_SERVICE') {
                  parkingService = element;
                  break;
                }
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Khi Quý khách gửi xe tại các điểm dịch vụ có liên kết với VDTC, '
                    'quý khách có thể sử dụng tiền trong tài khoản ePass để thanh '
                    'toán phí gửi xe tự động, không cần tiền mặt.',
                    textAlign: TextAlign.justify,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontSize: 12.sp,
                        color: ColorName.textGray2,
                        height: 1.5),
                  ),
                  SizedBox(height: 10.h),
                  Align(
                    alignment: Alignment.centerRight,
                    child: CupertinoSwitch(
                      value: state.isOnRoadSide,
                      activeColor: ColorName.primaryColor,
                      onChanged: (isOnRoadSide) async {
                        if (!isOnRoadSide) {
                          await showDialog(
                            context: context,
                            builder: (dialogContext) {
                              return ConfirmDialog(
                                title:
                                    'Bạn có chắc chắn muốn tắt dịch vụ gửi xe không?',
                                contentTextAlign: TextAlign.center,
                                secondaryButtonTitle: 'Đóng',
                                primaryButtonTitle: 'Xác nhận',
                                onPrimaryButtonTap: () {
                                  Navigator.of(dialogContext).pop();
                                  context.read<ParkingSettingBloc>().add(
                                      ParkingRoadSideSettingAlerted(
                                          isOnRoadSide: isOnRoadSide,
                                          serviceId: 256));
                                },
                              );
                            },
                          );
                        } else {
                          context.read<ParkingSettingBloc>().add(
                              ParkingRoadSideSettingAlerted(
                                  isOnRoadSide: isOnRoadSide, serviceId: 256));
                        }
                      },
                    ),
                  ),
                ],
              );
            }
          }),
    );
  }
}
