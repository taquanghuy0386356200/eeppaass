/// trackDate : ""
/// stationId : 1
/// warLat1 : 10.02
/// warLong1 : 13.2

class StationVehicleAlertRequest {
  StationVehicleAlertRequest({
    this.trackDate,
    this.stationId,
    this.warLat1,
    this.warLong1,
  });

  final String? trackDate;
  final int? stationId;
  final double? warLat1;
  final double? warLong1;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['trackDate'] = trackDate;
    map['stationId'] = stationId;
    map['warLat1'] = warLat1;
    map['warLong1'] = warLong1;
    return map;
  }
}
