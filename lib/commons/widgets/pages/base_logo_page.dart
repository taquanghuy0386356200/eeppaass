import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseLogoPage extends StatelessWidget {
  final Key? scaffoldKey;
  final Color backgroundColor;
  final Widget? leading;
  final List<Widget>? trailing;
  final String? title;
  final Widget child;
  final Widget? bottomNavigationBar;
  final bool resizeToAvoidBottomInset;

  const BaseLogoPage({
    Key? key,
    this.scaffoldKey,
    this.leading,
    this.trailing,
    this.title,
    required this.child,
    this.bottomNavigationBar,
    this.backgroundColor = ColorName.backgroundColor,
    this.resizeToAvoidBottomInset = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: backgroundColor,
      bottomNavigationBar: bottomNavigationBar,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      appBar: AppBar(
        elevation: 0.0,
        toolbarHeight: 72.h,
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
        centerTitle: true,
        title: FadeAnimation(
          delay: 0.5,
          child: Assets.icons.logoWhite.svg(),
        ),
        leading: leading,
        actions: trailing,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                ColorName.primaryGradientStart,
                ColorName.primaryGradientEnd,
              ],
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      extendBodyBehindAppBar: true,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          color: Colors.transparent,
          child: child,
        ),
      ),
    );
  }
}
