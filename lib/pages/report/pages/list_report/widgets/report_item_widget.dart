import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';

class ReportItemWidget extends StatelessWidget {
  const ReportItemWidget({
    Key? key,
    this.title = 'Thời gian xử lý',
    this.content = '22/02/2022 19:00:00',
    this.contentColor = ColorName.textGray2,
  }) : super(key: key);

  final String title;
  final String content;
  final Color contentColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            title,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Text(
            content,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: contentColor,
            ),
          ),
        )
      ],
    );
  }
}
