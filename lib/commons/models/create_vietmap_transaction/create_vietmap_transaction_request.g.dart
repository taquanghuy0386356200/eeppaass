// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_vietmap_transaction_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateVietMapTransactionRequest _$CreateVietMapTransactionRequestFromJson(
        Map<String, dynamic> json) =>
    CreateVietMapTransactionRequest(
      refTransactionId: json['refTransactionId'] as String?,
      expireDate: json['expireDate'] as String?,
      price: json['price'] as int?,
    );

Map<String, dynamic> _$CreateVietMapTransactionRequestToJson(
        CreateVietMapTransactionRequest instance) =>
    <String, dynamic>{
      'refTransactionId': instance.refTransactionId,
      'expireDate': instance.expireDate,
      'price': instance.price,
    };
