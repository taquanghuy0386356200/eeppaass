import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/pages/vehicle/ext/vehicle_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_shadow/simple_shadow.dart';

class VehicleListCard extends StatelessWidget {
  final Vehicle vehicle;

  const VehicleListCard({
    Key? key,
    required this.vehicle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleShadow(
      offset: const Offset(0, 1),
      opacity: 0.1,
      sigma: 10,
      child: Material(
        borderRadius: BorderRadius.circular(16.0),
        color: Colors.white,
        child: InkWell(
          borderRadius: BorderRadius.circular(16.0),
          onTap: () => context.pushRoute(VehicleDetailsRoute(
            vehicleId: vehicle.vehicleId.toString(),
            contractId: vehicle.contractId.toString(),
            fee: vehicle.debit?.fee
          )),
          child: Container(
            padding: EdgeInsets.fromLTRB(16.w, 16.h, 25.w, 16.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Row(
              children: [
                VehicleGroupCircle(
                  vehicleGroupId: vehicle.vehicleGroupId,
                  vehicleGroupName: vehicle.vehicleGroupName,
                ),
                SizedBox(width: 16.w),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        vehicle.plateNumber ?? '',
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      SizedBox(height: 6.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          VehicleGroupActiveStatusTag(
                            vehicle: vehicle,
                          ),
                          if(vehicle.debit?.statusDebit == VehicleDebitStatus.active)
                            VehicleDebitStatusTag(
                              vehicle: vehicle,
                            ),
                        ],
                      ),

                    ],
                  ),
                ),
                SizedBox(width: 12.w),

                Assets.icons.chevronRight.svg(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class VehicleGroupCircle extends StatelessWidget {
  final int? vehicleGroupId;
  final String? vehicleGroupName;

  const VehicleGroupCircle({
    Key? key,
    this.vehicleGroupId,
    this.vehicleGroupName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color color;
    switch (vehicleGroupId) {
      case 1:
        color = const Color(0xFFF2994A);
        break;
      case 2:
        color = const Color(0xFF2F80ED);
        break;
      case 3:
        color = const Color(0xFF9B51E0);
        break;
      case 4:
        color = const Color(0xFFEB5757);
        break;
      case 5:
        color = const Color(0xFF27AE60);
        break;
      default:
        color = Colors.grey;
        break;
    }

    return Container(
      height: 48.r,
      width: 48.r,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
      child: Center(
        child: Text(
          'XeL$vehicleGroupId',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
        ),
      ),
    );
  }
}

class VehicleGroupActiveStatusTag extends StatelessWidget {
  final Vehicle vehicle;

  const VehicleGroupActiveStatusTag({
    Key? key,
    required this.vehicle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.h, horizontal: 8.w),
      decoration: BoxDecoration(
        color: vehicle.backgroundStatusColor,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Text(
        vehicle.activeStatus?.title ?? '',
        style: Theme.of(context).textTheme.subtitle2!.copyWith(
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
              color: vehicle.textStatusColor,
            ),
      ),
    );
  }
}

class VehicleDebitStatusTag extends StatelessWidget {
  final Vehicle vehicle;

  const VehicleDebitStatusTag({
    Key? key,
    required this.vehicle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.h, horizontal: 8.w),
      decoration: BoxDecoration(
        color: const Color(0xFFFFE5E5),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Text(
        vehicle.debit?.statusDebit?.statusDebit ?? '',
        style: Theme.of(context).textTheme.subtitle2!.copyWith(
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
              color: const Color(0xFFEE0033),
            ),
      ),
    );
  }
}
