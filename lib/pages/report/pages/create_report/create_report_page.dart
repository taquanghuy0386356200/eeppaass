import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_type_picker/feedback_type_bloc.dart';
import 'package:epass/pages/report/pages/create_report/bloc/create_report_otp_bloc.dart';
import 'package:epass/pages/report/pages/create_report/widgets/report_form/bloc/report_form_bloc.dart';
import 'package:epass/pages/report/pages/create_report/widgets/report_form/report_form.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/injections.dart';

class CreateReportPage extends StatelessWidget {
  const CreateReportPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider(
        create: (context) => FilePickerBloc(
          imagePicker: getIt<ImagePicker>(),
          filePicker: getIt<FilePicker>(),
        ),
      ),
      BlocProvider<VehicleListBloc>(
        create: (context) => VehicleListBloc(
          vehicleRepository: getIt<IVehicleRepository>(),
          pageSize: 100,
        ),
        child: const AutoRouter(),
      ),
      BlocProvider<TicketVehicleBloc>(
        create: (context) => TicketVehicleBloc(),
      ),
      BlocProvider<FeedbackTypeBloc>(
        create: (context) => FeedbackTypeBloc(feedbackRepository: getIt<IFeedbackRepository>()),
      ),
      BlocProvider(
        create: (context) => ReportFormBloc(appBloc: getIt<AppBloc>()),
      ),
      BlocProvider(
        create: (context) => CreateReportOtpBloc(
          feedbackRepository: getIt<IFeedbackRepository>(),
        ),
      ),
    ], child: const ReportForm()
        // child: const AutoRouter()
        );
  }
}
