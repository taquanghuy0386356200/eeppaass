import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/widgets/textfield/cupertino_picker_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/category/category_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VehicleTypePicker extends StatelessWidget {
  final CategoryModel? selectedVehicleType;
  final bool isMandatory;
  final ValueChanged<PickerItemWithValue<CategoryModel>?>? onItemChanged;

  const VehicleTypePicker({
    Key? key,
    this.selectedVehicleType,
    this.onItemChanged,
    this.isMandatory = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CategoryBloc(categoryRepository: getIt<ICategoryRepository>()),
      child: _VehicleTypePicker(
        isMandatory: isMandatory,
        selectedVehicleType: selectedVehicleType,
        onItemChanged: onItemChanged,
      ),
    );
  }
}

class _VehicleTypePicker extends StatefulWidget {
  final CategoryModel? selectedVehicleType;
  final ValueChanged<PickerItemWithValue<CategoryModel>?>? onItemChanged;
  final bool isMandatory;

  const _VehicleTypePicker({
    Key? key,
    this.selectedVehicleType,
    this.onItemChanged,
    this.isMandatory = false,
  }) : super(key: key);

  @override
  State<_VehicleTypePicker> createState() => _VehicleTypePickerState();
}

class _VehicleTypePickerState extends State<_VehicleTypePicker> {
  PickerItemWithValue<CategoryModel>? _selectedVehicleType;

  @override
  void initState() {
    context.read<CategoryBloc>().add(const VehicleTypeFetched());

    if (widget.selectedVehicleType != null) {
      _selectedVehicleType = PickerItemWithValue(
        title: widget.selectedVehicleType!.name ?? '',
        value: widget.selectedVehicleType!,
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryState>(
      builder: (context, state) {
        List<PickerItemWithValue<CategoryModel>> items = [];
        if (state is VehicleTypeFetchedSuccess) {
          items = state.vehicleTypes.map((e) => PickerItemWithValue(title: e.name ?? '', value: e)).toList();
        }
        return CupertinoPickerField(
          isMandatory: widget.isMandatory,
          items: items,
          defaultItem: _selectedVehicleType,
          isLoading: state is VehicleTypeFetchedInProgress,
          validator: (_) {
            if (_selectedVehicleType == null) {
              return 'Vui lòng chọn Loại phương tiện';
            }
            return null;
          },
          hintText: 'Loại phương tiện',
          enabled: state is! VehicleTypeFetchedInProgress,
          label: 'Loại phương tiện',
          errorText: state is VehicleTypeFetchedFailure ? state.message : null,
          onItemChanged: (PickerItemWithValue<CategoryModel>? selectedItem) {
            setState(() => _selectedVehicleType = selectedItem);
            widget.onItemChanged?.call(selectedItem);
          },
        );
      },
    );
  }
}
