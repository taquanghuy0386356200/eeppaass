// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportRequest _$ReportRequestFromJson(Map<String, dynamic> json) =>
    ReportRequest(
      ticketChannel: json['ticketChannel'] as String? ?? '1',
      custId: json['custId'] as String,
      contractNo: json['contractNo'] as String,
      contractId: json['contractId'] as String,
      custTypeId: json['custTypeId'] as String,
      custName: json['custName'] as String,
      email: json['email'] as String?,
      plateNumber: json['plateNumber'] as String?,
      plateTypeCode: json['plateTypeCode'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      location: json['location'] as String?,
      priorityId: json['priorityId'] as String? ?? '1',
      l1TicketTypeId: json['l1TicketTypeId'] as String?,
      l2TicketTypeId: json['l2TicketTypeId'] as String,
      l3TicketTypeId: json['l3TicketTypeId'] as String?,
      provinceName: json['provinceName'] as String,
      districtName: json['districtName'] as String?,
      communeName: json['communeName'] as String?,
      areaCode: json['areaCode'] as String,
      stationId: json['stationId'] as String?,
      stationName: json['stationName'] as String?,
      stageId: json['stageId'] as String?,
      stageName: json['stageName'] as String?,
      contentReceive: json['contentReceive'] as String,
      ticketKind: json['ticketKind'] as String? ?? '1',
      requestDate: json['requestDate'] as String,
      supportInfo: json['supportInfo'] as String?,
      phoneContact: json['phoneContact'] as String,
      attachmentFiles: (json['attachmentFiles'] as List<dynamic>?)
          ?.map((e) =>
              ReportAttachmentFileRequest.fromJson(e as Map<String, dynamic>))
          .toList(),
      otp: json['otp'] as String?,
    );

Map<String, dynamic> _$ReportRequestToJson(ReportRequest instance) =>
    <String, dynamic>{
      'ticketChannel': instance.ticketChannel,
      'custId': instance.custId,
      'contractNo': instance.contractNo,
      'contractId': instance.contractId,
      'custTypeId': instance.custTypeId,
      'custName': instance.custName,
      'email': instance.email,
      'plateNumber': instance.plateNumber,
      'plateTypeCode': instance.plateTypeCode,
      'phoneNumber': instance.phoneNumber,
      'location': instance.location,
      'priorityId': instance.priorityId,
      'l1TicketTypeId': instance.l1TicketTypeId,
      'l2TicketTypeId': instance.l2TicketTypeId,
      'l3TicketTypeId': instance.l3TicketTypeId,
      'provinceName': instance.provinceName,
      'districtName': instance.districtName,
      'communeName': instance.communeName,
      'areaCode': instance.areaCode,
      'stationId': instance.stationId,
      'stationName': instance.stationName,
      'stageId': instance.stageId,
      'stageName': instance.stageName,
      'contentReceive': instance.contentReceive,
      'ticketKind': instance.ticketKind,
      'requestDate': instance.requestDate,
      'supportInfo': instance.supportInfo,
      'phoneContact': instance.phoneContact,
      'attachmentFiles':
          instance.attachmentFiles?.map((e) => e.toJson()).toList(),
      'otp': instance.otp,
    };
