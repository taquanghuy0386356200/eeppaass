// ignore_for_file: file_names

import 'pages/app.dart';
import 'flavors.dart';

void main() {
  F.appFlavor = Flavor.dev;
  startApp();
}
