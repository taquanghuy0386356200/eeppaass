import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/extensions/image_extension.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/repo/insurance_repository.dart';
import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:epass/commons/validators/phone_number_validator.dart';
import 'package:epass/commons/validators/plate_number_validator.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/setting/referrals_epass/bloc/referrals_client_bloc.dart';
import 'package:epass/pages/setting/referrals_epass/ui/count_down_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ReferralsClientPage extends StatefulWidget with AutoRouteWrapper {
  const ReferralsClientPage({Key? key}) : super(key: key);

  @override
  State<ReferralsClientPage> createState() => _ReferralsClientPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (context) => ReferralsClientBloc(
        referralRepository: getIt<IInvoiceRepository>(),
        accessKeyRepository: getIt<IInsuranceRepository>(),
        configTimeRepository: getIt<ICategoryRepository>(),
      ),
      child: this,
    );
  }
}

class _ReferralsClientPageState extends State<ReferralsClientPage>
    with PhoneNumberValidator, PlateNumberValidator {
  bool _firstRun = true;
  late TextEditingController _soDienThoai;
  late TextEditingController _bienSoXe;
  late ReferralsClientBloc bloc;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _soDienThoai = TextEditingController();
    _bienSoXe = TextEditingController();
    bloc = context.read<ReferralsClientBloc>();
    bloc.add(const GetConfigTimeDisableBtn());
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ReferralsClientBloc, ReferralsClientState>(
      listener: (context, state) {
        if (state is SendingInfoReferralsClientState) {
          context.loaderOverlay.show();
        } else {
          context.loaderOverlay.hide();
          if (state is SuccessReferralsClientState) {
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (dialogContext) {
                return SuccessDialog(
                  content: "Cám ơn bạn đã tin tưởng và đồng hành cùng ePass!",
                  buttonTitle: 'Đóng',
                  contentTextAlign: TextAlign.center,
                  onButtonTap: () async {
                    Navigator.of(dialogContext).pop();
                    context.read<ReferralsClientBloc>().add(
                          CloseSuccessReferralsClientEvent(),
                        );
                  },
                );
              },
            );
          } else if (state is FailReferralsClientState) {
            showDialog(
              context: context,
              builder: (dialogContext) {
                return ConfirmDialog(
                  title: 'Thông báo',
                  image: ImageAssests.svgAssets(
                    ImageAssests.alertIcon,
                    height: 102.h,
                    width: 102.w,
                  ),
                  content: state.messageError,
                  contentTextAlign: TextAlign.center,
                  primaryButtonTitle: 'Đóng',
                  hasSecondaryButton: false,
                  onPrimaryButtonTap: () {
                    Navigator.of(dialogContext).pop();
                  },
                );
              },
            );
          } else if (state is DisableSendReferralsClientState) {
            bloc.firstValidate = false;
            _soDienThoai.clear();
            _bienSoXe.clear();
          }
        }
      },
      child: BasePage(
        backgroundColor: Colors.white,
        title: 'Giới thiệu ePass',
        child: Stack(
          children: [
            FadeAnimation(
              delay: 0.5,
              playAnimation: _firstRun,
              child: const GradientHeaderContainer(),
            ),
            SafeArea(
              child: FadeAnimation(
                delay: 1,
                direction: FadeDirection.up,
                playAnimation: _firstRun,
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: RoundedTopContainer(
                    margin: EdgeInsets.only(top: 16.h),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 17.h,
                            left: 16.w,
                            right: 16.w,
                          ),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10.h,
                                ),
                                Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Giới thiệu bạn mới nhận ngay quà từ ePass',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                  ),
                                ),
                                SizedBox(
                                  height: 24.h,
                                ),
                                PrimaryTextField(
                                  fontSize: 16,
                                  controller: _soDienThoai,
                                  labelText: 'Số điện thoại',
                                  hintText: 'Nhập số điện thoại',
                                  validator: (phoneNumber) {
                                    final expression = RegExp(r'^(0|84)\d{9}$');
                                    if (bloc.firstValidate &&
                                        (phoneNumber == null ||
                                            phoneNumber.isEmpty)) {
                                      return 'Vui lòng nhập Số điện thoại';
                                    } else if (phoneNumber != null &&
                                        phoneNumber.isNotEmpty &&
                                        !expression.hasMatch(phoneNumber)) {
                                      return 'Số điện thoại không hợp lệ';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    bloc.firstValidate = true;
                                  },
                                ),
                                SizedBox(
                                  height: 20.h,
                                ),
                                PrimaryTextField(
                                  fontSize: 16,
                                  controller: _bienSoXe,
                                  labelText: 'Biển số xe',
                                  hintText: 'VD 30H12345',
                                  validator: (plateNumber) {
                                    final pattern = RegExp(
                                        r'^(?=.*\d)(?=.*[a-zA-Z])([a-zA-Z\d]+){2,10}$');
                                    if (bloc.firstValidate &&
                                        (plateNumber == null ||
                                            plateNumber.isEmpty)) {
                                      return 'Vui lòng nhập Biển số xe';
                                    } else if (plateNumber != null &&
                                        plateNumber.isNotEmpty &&
                                        !pattern.hasMatch(plateNumber)) {
                                      return 'Biển số xe không hợp lệ';
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    bloc.firstValidate = true;

                                  },
                                ),
                                SizedBox(
                                  height: 48.h,
                                ),
                                BlocBuilder<ReferralsClientBloc,
                                    ReferralsClientState>(
                                  builder: (context, state) {
                                    return CountDownButton(
                                      key: UniqueKey(),
                                      countDown: bloc.timer,
                                      title: 'Gửi thông tin',
                                      widthCustom: double.infinity,
                                      enabled: (state
                                              is DisableSendReferralsClientState)
                                          ? false
                                          : true,
                                      onTap: () {
                                        if (_formKey.currentState?.validate() ??
                                            false) {
                                          context
                                              .read<ReferralsClientBloc>()
                                              .add(
                                                SendInfoReferralsClientEvent(
                                                  plateNumber: _bienSoXe.text,
                                                  phoneNumber:
                                                      _soDienThoai.text,
                                                ),
                                              );
                                        } else {
                                          //nothing
                                        }
                                      },
                                    );
                                  },
                                ),
                                SizedBox(height: 28.h,),
                                Center(
                                  child: Image.asset(
                                    ImageAssests.referralsLogo,
                                    height: 220.h,
                                    width: 334.r,
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
