import 'package:epass/commons/enum/method_charge.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// servicePlanTypeId : 4
/// vehicleGroupId : 2
/// stationType : 1
/// stationId : 5032
/// chargeMethodId : 1
/// autoRenew : 0
/// createDateFrom : "01/04/2022"
/// createDateTo : "30/04/2022"
/// effDate : "01/04/2022"
/// plateNumber : "02G11111"
/// vehicleTypeId : 11
/// netWeight : 1.0
/// seatNumber : 5
/// epc : "3416214B9400F274G0001000"
/// booCode : "BOO2"
/// expDate : "30/04/2022 23:59:59"
/// vehicleId : 1000976
/// reasons : "Phương tiện 02G11111 không lấy được giá vé do đã tồn tại vé"
/// isChargeTicket : true
/// message : "Phương tiện 02G11111 không lấy được giá vé do đã tồn tại vé"
/// dateOfUse : "01/04/2022 00:00:00"

part 'buy_ticket_response.g.dart';

@JsonSerializable()
class BuyTicketResponseItem {
  @JsonKey(unknownEnumValue: ServicePlanType.other)
  final ServicePlanType? servicePlanTypeId;

  final int? vehicleGroupId;
  final int? stationType;
  final int? stationId;

  @JsonKey(unknownEnumValue: MethodChargeInt.unknown)
  final MethodChargeInt? chargeMethodId;

  final int? autoRenew;
  final String? createDateFrom;
  final String? createDateTo;
  final String? effDate;
  final String? plateNumber;
  final int? vehicleTypeId;
  final double? netWeight;
  final int? seatNumber;
  final String? epc;
  final String? booCode;
  final String? expDate;
  final int? vehicleId;
  final String? reasons;
  final bool? isChargeTicket;
  final String? message;
  final String? dateOfUse;

  factory BuyTicketResponseItem.fromJson(Map<String, dynamic> json) =>
      _$BuyTicketResponseItemFromJson(json);

  BuyTicketResponseItem({
    this.servicePlanTypeId,
    this.vehicleGroupId,
    this.stationType,
    this.stationId,
    this.chargeMethodId,
    this.autoRenew,
    this.createDateFrom,
    this.createDateTo,
    this.effDate,
    this.plateNumber,
    this.vehicleTypeId,
    this.netWeight,
    this.seatNumber,
    this.epc,
    this.booCode,
    this.expDate,
    this.vehicleId,
    this.reasons,
    this.isChargeTicket,
    this.message,
    this.dateOfUse,
  });
}

@JsonSerializable()
class BuyTicketResponseData {
  final List<BuyTicketResponseItem>? listSuccess;
  final List<BuyTicketResponseItem>? listFail;

  const BuyTicketResponseData({
    this.listSuccess,
    this.listFail,
  });

  factory BuyTicketResponseData.fromJson(Map<String, dynamic> json) =>
      _$BuyTicketResponseDataFromJson(json);
}

class BuyTicketResponseDataNotNull {
  final List<BuyTicketResponseItem> listSuccess;
  final List<BuyTicketResponseItem> listFail;

  const BuyTicketResponseDataNotNull({
    required this.listSuccess,
    required this.listFail,
  });
}

@JsonSerializable()
class BuyTicketResponse {
  final BuyTicketResponseData? data;
  final MessageData? mess;

  const BuyTicketResponse({this.data, this.mess});

  factory BuyTicketResponse.fromJson(Map<String, dynamic> json) =>
      _$BuyTicketResponseFromJson(json);
}
