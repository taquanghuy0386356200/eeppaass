// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_feat_save_ticket_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResultCreateSaveTicketTotalResponse
    _$ResultCreateSaveTicketTotalResponseFromJson(Map<String, dynamic> json) =>
        ResultCreateSaveTicketTotalResponse(
          data: json['data'] == null
              ? null
              : ResultCreateSaveTicket.fromJson(
                  json['data'] as Map<String, dynamic>),
          mess: json['mess'] == null
              ? null
              : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
        );

Map<String, dynamic> _$ResultCreateSaveTicketTotalResponseToJson(
        ResultCreateSaveTicketTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ResultCreateSaveTicket _$ResultCreateSaveTicketFromJson(
        Map<String, dynamic> json) =>
    ResultCreateSaveTicket(
      ticketId: json['ticketId'] as int?,
      slaDate: json['slaDate'] as String?,
    );

Map<String, dynamic> _$ResultCreateSaveTicketToJson(
        ResultCreateSaveTicket instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'slaDate': instance.slaDate,
    };
