import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cash_in_order.g.dart';

@JsonSerializable()
class CashInOrder {
  final int? billingCode;

  CashInOrder({this.billingCode});

  factory CashInOrder.fromJson(Map<String, dynamic> json) =>
      _$CashInOrderFromJson(json);

  Map<String, dynamic> toJson() => _$CashInOrderToJson(this);
}

@JsonSerializable()
class CashInOrderResponse {
  @JsonKey(name: 'data')
  final CashInOrder? order;
  final MessageData? mess;

  CashInOrderResponse({this.order, this.mess});

  factory CashInOrderResponse.fromJson(Map<String, dynamic> json) =>
      _$CashInOrderResponseFromJson(json);
}

@JsonSerializable()
class CashInOrderRequest {
  final String contractNo;
  final int amount;
  @JsonKey(name: 'desContractId')
  final String receiverContractId;
  final int quantity;
  final String? paymentMethodName;

  CashInOrderRequest({
    required this.contractNo,
    required this.amount,
    required this.receiverContractId,
    required this.quantity,
    this.paymentMethodName,
  });

  factory CashInOrderRequest.fromJson(Map<String, dynamic> json) =>
      _$CashInOrderRequestFromJson(json);

  Map<String, dynamic> toJson() => _$CashInOrderRequestToJson(this);
}
