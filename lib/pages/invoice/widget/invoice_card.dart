import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/invoice/invoice.dart';
import 'package:epass/pages/invoice/bloc/invoice_details_bloc.dart';
import 'package:epass/pages/invoice/widget/invoice_clipper.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/gen/assets.gen.dart';

class InvoiceCard extends StatelessWidget {
  final Invoice item;

  const InvoiceCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleShadow(
      offset: const Offset(0, 1),
      opacity: 0.1,
      sigma: 10,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipPath(
            clipper: InvoiceClipper1(),
            child: Container(
              padding: EdgeInsets.fromLTRB(14.w, 20.h, 16.w, 24.h),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(16.0),
                  topLeft: Radius.circular(16.0),
                ),
                color: Colors.white,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: GradientText(
                      item.adjustmentTypeName ?? '',
                      gradient: const LinearGradient(colors: [
                        ColorName.primaryGradientStart,
                        ColorName.primaryGradientEnd,
                      ]),
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            fontWeight: FontWeight.w700,
                          ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Text(
                    (item.totAmount ?? 0).round().formatCurrency(symbol: 'đ'),
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          fontWeight: FontWeight.w700,
                        ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 0.5.h),
          ClipPath(
            clipper: InvoiceClipper2(),
            child: Container(
              padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 16.h),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        'Số hoá đơn',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.borderColor,
                            ),
                      ),
                      SizedBox(width: 12.w),
                      const Spacer(),
                      Text(
                        item.invoiceNo ?? '',
                        maxLines: 2,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                    ],
                  ),
                  SizedBox(height: 12.h),
                  Row(
                    children: [
                      Text(
                        'Thời gian',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.borderColor,
                            ),
                      ),
                      SizedBox(width: 12.w),
                      const Spacer(),
                      Text(
                        item.strInvoiceIssuedDate ?? '',
                        maxLines: 2,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                    ],
                  ),
                  SizedBox(height: 12.h),
                  Row(
                    children: [
                      Text(
                        'Trạng thái',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.borderColor,
                            ),
                      ),
                      SizedBox(width: 12.w),
                      const Spacer(),
                      Text(
                        item.invoiceStatusName ?? '',
                        maxLines: 2,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                    ],
                  ),
                  SizedBox(height: 16.h),
                  const Divider(color: ColorName.borderColor),
                  SizedBox(height: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      InvoiceCardButton(
                        icon: Assets.icons.eye.svg(color: ColorName.textGray1),
                        title: 'Xem hoá đơn',
                        onTap: () => context.read<InvoiceDetailsBloc>().add(
                            InvoiceViewed(
                                item.custInvoiceId?.toString() ?? '')),
                      ),
                      SizedBox(width: 8.w),
                      InvoiceCardButton(
                        icon: Assets.icons.download
                            .svg(color: ColorName.textGray1),
                        title: 'Tải hoá đơn',
                        onTap: () => context.read<InvoiceDetailsBloc>().add(
                            InvoiceDownloaded(
                                item.custInvoiceId?.toString() ?? '')),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class InvoiceCardButton extends StatelessWidget {
  final Widget icon;
  final String title;
  final VoidCallback onTap;

  const InvoiceCardButton({
    Key? key,
    required this.icon,
    required this.title,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(16.0),
      color: Colors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.circular(16.0),
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.fromLTRB(10.w, 12.h, 14.w, 12.h),
          child: Row(
            children: [
              icon,
              SizedBox(width: 10.w),
              Text(
                title,
                style: Theme.of(context).textTheme.subtitle2!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
