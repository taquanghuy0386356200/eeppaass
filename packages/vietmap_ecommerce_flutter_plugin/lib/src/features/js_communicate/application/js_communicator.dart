import 'dart:convert';

import 'package:dart_exporter_annotation/dart_exporter_annotation.dart';
import 'package:flutter/material.dart';
import 'package:vietmap_ecommerce_flutter_plugin/src/features/core/application/extensions/string_extension.dart';
import 'package:webview_flutter/webview_flutter.dart';

@Export()
class JSCommunicator {
  late final WebViewController _controller;
  JSCommunicator({required WebViewController controller}) {
    _controller = controller;
  }
  Future invoke(String methodName, String params) {
    return _controller.evaluateJavascript(
        'vietmapEcommerceHandle("$methodName", "${params.escapeQuotes}");');
  }

  @visibleForTesting
  Future forceCallHandle(String methodName, String params) {
    return _controller.evaluateJavascript(
        'vietmapEcommerceInvoke("$methodName", "$params");');
  }
}

@Export()
extension JSCommunicatorDefaultMethods on JSCommunicator {
  Future setApiBaseUrl(String baseUrl) {
    return invoke('setApiBaseUrl', baseUrl);
  }

  Future openOrdersList() {
    return invoke('openOrdersList', '');
  }

  Future openProductDetailScreen({required String productId}) {
    return invoke('openProductDetail', productId);
  }

  Future<dynamic> goBack() {
    return invoke('goBack', '');
  }

  Future updateAuthInfo({
    required String accessToken,
    required String phoneNumber,
    required String? customerName,
  }) {
    return invoke(
        'updateAuthInfo',
        jsonEncode({
          'accessToken': accessToken,
          'phoneNumber': phoneNumber,
          'customerName': customerName,
        }));
  }
}

@Export()
class VIETMAPJsMethod {
  final String name;
  final String? params;
  VIETMAPJsMethod({
    required this.name,
    this.params,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is VIETMAPJsMethod &&
        other.name == name &&
        other.params == params;
  }

  @override
  int get hashCode => name.hashCode ^ params.hashCode;

  @override
  String toString() {
    return '\n\n\n===\nVIETMAPJsMethod:\n-name: $name\n-params: $params\n===\n\n\n';
  }
}
