import 'package:dio/dio.dart';
import 'package:epass/commons/api/endpoint.dart' as endpoint;
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/models/customer_type/customer_type_response.dart';
import 'package:epass/commons/models/station/station.dart';
import 'package:epass/commons/models/station/station_detail.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:retrofit/retrofit.dart';

import '../../../models/car_doctor/car_doctor_config_response.dart';

part 'category_client.g.dart';

@RestApi()
abstract class CategoryClient {
  factory CategoryClient(
    Dio dio, {
    String baseUrl,
  }) = _CategoryClient;

  /// /api/v1/categories/plate-type
  @GET(endpoint.plateTypes)
  Future<CategoryModelResponse> getPlateTypes();

  /// /api/v1/stations
  @GET(endpoint.stations)
  Future<StationResponse> getStations({
    String? province,
  });

  /// /api/v1/mobile/stage/station
  @GET(endpoint.stationStage)
  Future<StationStageResponse> getStationStages({
    @Query('booCode') String? booCode,
    @Query('stationType') int? stationType,
  });

  /// /api/v1/categories
  @GET(endpoint.categories)
  Future<CategoryModelResponse> getCategories({
    @Query('table_name') String? tableName,
    @Query('code') String? code,
  });

  /// /api/v1/vehicle-types
  @GET(endpoint.vehicleType)
  Future<CategoryModelResponse> getVehicleTypes();

  /// /api/v1/customer-types
  @GET(endpoint.customerType)
  Future<CustomerTypeResponse> getCustomerType();

  /// /api/v1/stations/details
  @GET(endpoint.stationDetails)
  Future<StationDetailResponse> getStationDetails();

  /// /api/v1/categories/config
  @GET(endpoint.categoriesConfig)
  Future<CategoryModelResponse> getCategoriesConfig();

  /// /api/v1/categories/service_config
  @GET(endpoint.getServiceConfig)
  Future<CarDoctorConfigResponse> getCarDoctorConfig();
}
