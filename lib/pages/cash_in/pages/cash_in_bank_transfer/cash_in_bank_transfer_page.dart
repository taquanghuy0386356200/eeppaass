import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/showcase/custom_showcase.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:showcaseview/showcaseview.dart';

class CashInBankTransferPage extends StatefulWidget {
  const CashInBankTransferPage({Key? key}) : super(key: key);

  @override
  State<CashInBankTransferPage> createState() => _CashInBankTransferPageState();
}

class _CashInBankTransferPageState extends State<CashInBankTransferPage> {

  final _bankShowCaseKey = GlobalKey();
  final _accountNoShowCaseKey = GlobalKey();
  final _accountNameShowCaseKey = GlobalKey();

  late List<GlobalKey> _showCaseList;

  bool lastStepVisible = false;

  late BuildContext _showCaseContext;

  @override
  void initState() {
    super.initState();

    super.initState();
    _showCaseList = [
      _bankShowCaseKey,
      _accountNoShowCaseKey,
      _accountNameShowCaseKey,
    ];
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final isCashInBankTransferFirstRun = getIt<SettingBloc>().state.isCashInBankTransferFirstRun;
      if (isCashInBankTransferFirstRun) {
        Future.delayed(
          const Duration(milliseconds: 1000),
          () {
            context.read<HomeTabsBloc>().add(const HomeTabBarRequestHidden(isHidden: true));
            ShowCaseWidget.of(_showCaseContext).startShowCase(_showCaseList);
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ShowCaseWidget(
      onFinish: () {
        setState(() => lastStepVisible = true);
      },
      builder: Builder(builder: (context) {
        _showCaseContext = context;

        return BasePage(
          backgroundColor: Colors.white,
          title: 'Thông tin chuyển khoản',
          child: Stack(
            children: [
              const FadeAnimation(
                delay: 0.5,
                child: GradientHeaderContainer(),
              ),
              SafeArea(
                child: FadeAnimation(
                  delay: 1,
                  direction: FadeDirection.up,
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: RoundedTopContainer(
                      margin: EdgeInsets.only(top: 16.h),
                      padding: EdgeInsets.fromLTRB(10.w, 12.h, 10.w, 32.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                            onPressed: () => ShowCaseWidget.of(_showCaseContext).startShowCase(_showCaseList),
                            child: Text(
                              'Xem hướng dẫn',
                              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.linkBlue,
                                    decoration: TextDecoration.underline,
                                  ),
                            ),
                          ),
                          CustomShowcase(
                            showCaseKey: _bankShowCaseKey,
                            overlayPadding: EdgeInsets.zero,
                            description: 'Chuyển khoản từ BẤT KỲ NGÂN HÀNG NÀO bạn đang sử dụng tới BIDV.',
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 12.h),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 6.w),
                                  child: Text(
                                    'Để nạp tiền, bạn hãy dùng bất kỳ ngân hàng nào'
                                    ' để chuyển khoản đến tài khoản sau',
                                    style: Theme.of(context).textTheme.bodyText1,
                                  ),
                                ),
                                SizedBox(height: 20.h),
                                // BANK
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 26.w),
                                  child: Text(
                                    'Ngân hàng nhận',
                                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                          color: ColorName.borderColor,
                                        ),
                                  ),
                                ),
                                SizedBox(height: 6.h),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 26.w),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'Ngân hàng TMCP Đầu Tư & Phát Triển Việt Nam'
                                          ' (BIDV)',
                                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                fontWeight: FontWeight.bold,
                                              ),
                                        ),
                                      ),
                                      SizedBox(width: 20.w),
                                      Assets.icons.bidvIcon.image(
                                        height: 60.h,
                                        width: 60.w,
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 24.h),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 26.w),
                            child: Row(
                              children: [
                                Text(
                                  'Số tiền cần chuyển',
                                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                        color: ColorName.borderColor,
                                      ),
                                ),
                                const Spacer(),
                                BlocSelector<CashInAmountBloc, CashInAmountState, int?>(
                                  selector: (state) {
                                    return state is CashInAmountChangedSuccess ? state.amount : null;
                                  },
                                  builder: (context, amount) {
                                    return Text(
                                      (amount ?? 0).formatCurrency(symbol: 'đ'),
                                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.sp,
                                          ),
                                      overflow: TextOverflow.ellipsis,
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                          CustomShowcase(
                            showCaseKey: _accountNoShowCaseKey,
                            overlayPadding: EdgeInsets.zero,
                            description:
                                'Ấn COPY Số tài khoản nhận và dán vào màn hình chuyển khoản trong app ngân hàng của bạn.',
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 26.w),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 24.h),

                                  // BIDV ACCOUNT
                                  Text(
                                    'Số tài khoản nhận (bắt buộc)',
                                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                          color: ColorName.borderColor,
                                        ),
                                  ),
                                  BlocSelector<CashInTargetBloc, CashInTargetState, String>(
                                    selector: (state) => state.contractNo ?? '',
                                    builder: (context, contractNo) {
                                      return Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              'V5EPASS$contractNo',
                                              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                            ),
                                          ),
                                          SizedBox(width: 16.w),
                                          SplashIconButton(
                                            onTap: () async {
                                              await Clipboard.setData(ClipboardData(text: 'V5EPASS$contractNo'));
                                              showSuccessSnackBBar(
                                                context: context,
                                                message: 'Đã sao chép Số tài khoản nhận',
                                              );
                                            },
                                            icon: Assets.icons.copy.svg(),
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                  SizedBox(height: 16.h),
                                  const Divider(
                                    color: ColorName.disabledBorderColor,
                                    height: 1.0,
                                    thickness: 1.0,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          CustomShowcase(
                            showCaseKey: _accountNameShowCaseKey,
                            overlayPadding: EdgeInsets.zero,
                            description: 'Ấn COPY Tên người thụ hưởng và làm tương tự.',
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 26.w),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // BIDV ACCOUNT NAME
                                  SizedBox(height: 20.h),
                                  Text(
                                    'Tên người thụ hưởng (bắt buộc)',
                                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                          color: ColorName.borderColor,
                                        ),
                                  ),
                                  BlocSelector<CashInTargetBloc, CashInTargetState, String>(
                                    selector: (state) => state.userName ?? '',
                                    builder: (context, username) {
                                      return Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              'EPASS ${username.toUpperCase()}',
                                              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                            ),
                                          ),
                                          SizedBox(width: 16.w),
                                          SplashIconButton(
                                            onTap: () async {
                                              await Clipboard.setData(
                                                  ClipboardData(text: 'EPASS ${username.toUpperCase()}'));
                                              showSuccessSnackBBar(
                                                context: context,
                                                message: 'Đã sao chép Tên người thụ hưởng',
                                              );
                                            },
                                            icon: Assets.icons.copy.svg(),
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                  SizedBox(height: 16.h),
                                  const Divider(
                                    color: ColorName.disabledBorderColor,
                                    height: 1.0,
                                    thickness: 1.0,
                                  ),
                                  SizedBox(height: 24.h),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(28.w, 0.h, 32.w, 0.h),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Assets.icons.infoBlue.svg(
                                  width: 24.r,
                                  height: 24.r,
                                ),
                                SizedBox(width: 12.w),
                                Expanded(
                                  child: RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: 'Nếu ',
                                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                                  color: ColorName.linkBlue,
                                                )),
                                        TextSpan(
                                            text: 'nội dung chuyển khoản, tên'
                                                ' người thụ hưởng',
                                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                                  color: ColorName.linkBlue,
                                                  fontWeight: FontWeight.bold,
                                                )),
                                        TextSpan(
                                            text: ' thiếu hoặc không chính xác,'
                                                ' số tiền sẽ không được chuyển tới'
                                                ' tài khoản giao thông ePass.',
                                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                                  color: ColorName.linkBlue,
                                                )),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: lastStepVisible,
                child: Container(
                  height: ScreenUtil().screenHeight,
                  width: ScreenUtil().screenWidth,
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  color: Colors.black87.withOpacity(0.8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'HÃY COPY ĐẦY ĐỦ & CHÍNH XÁC\n'
                        'ePass cần thông tin đầy đủ, chính xác để đưa số tiền của bạn'
                        ' đến ví ePass một cách nhanh nhất',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              height: 1.5,
                            ),
                      ),
                      SizedBox(height: 32.h),
                      Row(
                        children: [
                          Expanded(
                            child: SizedBox(
                              height: 50.h,
                              child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 16.w,
                                    vertical: 12.h,
                                  ),
                                  visualDensity: VisualDensity.standard,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16.0),
                                  ),
                                  side: const BorderSide(
                                    color: Colors.white,
                                    width: 1.0,
                                  ),
                                  primary: Colors.white,
                                ),
                                onPressed: () {
                                  setState(() => lastStepVisible = false);
                                  ShowCaseWidget.of(_showCaseContext).startShowCase(_showCaseList);
                                },
                                child: Text(
                                  'Xem lại HD',
                                  style: Theme.of(context).textTheme.button!.copyWith(
                                        fontSize: 18.sp,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.white,
                                      ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 16.w),
                          Expanded(
                            child: SizedBox(
                              height: 50.h,
                              child: PrimaryButton(
                                title: 'Đã hiểu',
                                onTap: () {
                                  setState(() => lastStepVisible = false);
                                  context.read<SettingBloc>().add(CashInBankTransferFirstRun());
                                  context.read<HomeTabsBloc>().add(const HomeTabBarRequestHidden(isHidden: false));
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
