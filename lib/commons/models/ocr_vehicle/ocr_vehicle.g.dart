// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ocr_vehicle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OCRVehicle _$OCRVehicleFromJson(Map<String, dynamic> json) => OCRVehicle(
      responseCode: json['responseCode'] as int?,
      plateType: json['plateType'] as String?,
      plateColor: json['plateColor'] as String?,
      number: json['number'] as String?,
    );

Map<String, dynamic> _$OCRVehicleToJson(OCRVehicle instance) =>
    <String, dynamic>{
      'responseCode': instance.responseCode,
      'plateType': instance.plateType,
      'plateColor': instance.plateColor,
      'number': instance.number,
    };

OCRVehicleResponse _$OCRVehicleResponseFromJson(Map<String, dynamic> json) =>
    OCRVehicleResponse(
      data: json['data'] == null
          ? null
          : OCRVehicle.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OCRVehicleResponseToJson(OCRVehicleResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
