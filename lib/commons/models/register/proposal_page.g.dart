// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proposal_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProposalPage _$ProposalPageFromJson(Map<String, dynamic> json) => ProposalPage(
      fileName: json['fileName'] as String?,
      base64Data: json['base64Data'] as String?,
    );

Map<String, dynamic> _$ProposalPageToJson(ProposalPage instance) =>
    <String, dynamic>{
      'fileName': instance.fileName,
      'base64Data': instance.base64Data,
    };

ProposalPageResponse _$ProposalPageResponseFromJson(
        Map<String, dynamic> json) =>
    ProposalPageResponse(
      data: json['data'] == null
          ? null
          : ProposalPage.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProposalPageResponseToJson(
        ProposalPageResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
