import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'confirm_momo_payment.g.dart';

@JsonSerializable()
class ConfirmMomoPaymentRequest {
  final String partnerCode;
  final String partnerRefId;
  final int amount;
  final String customerNumber;
  final String appData;

  ConfirmMomoPaymentRequest({
    required this.partnerCode,
    required this.partnerRefId,
    required this.amount,
    required this.customerNumber,
    required this.appData,
  });

  factory ConfirmMomoPaymentRequest.fromJson(Map<String, dynamic> json) =>
      _$ConfirmMomoPaymentRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ConfirmMomoPaymentRequestToJson(this);
}

@JsonSerializable()
class ConfirmMomoPaymentData {
  final int? amount;
  final String? momoTransId;
  final String? partnerRefId;
  final String? partnerCode;

  ConfirmMomoPaymentData({
    this.amount,
    this.momoTransId,
    this.partnerRefId,
    this.partnerCode,
  });

  factory ConfirmMomoPaymentData.fromJson(Map<String, dynamic> json) =>
      _$ConfirmMomoPaymentDataFromJson(json);
}

@JsonSerializable()
class ConfirmMomoPaymentResponse {
  final ConfirmMomoPaymentData? data;
  final MessageData? mess;

  ConfirmMomoPaymentResponse({this.data, this.mess});

  factory ConfirmMomoPaymentResponse.fromJson(Map<String, dynamic> json) =>
      _$ConfirmMomoPaymentResponseFromJson(json);
}
