// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_method_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfigMethodVerifyRequest _$ConfigMethodVerifyRequestFromJson(
        Map<String, dynamic> json) =>
    ConfigMethodVerifyRequest(
      serviceId: json['serviceId'] as int,
      verifyType: json['verifyType'] as int,
      actionType: json['actionType'] as int,
      passCode: json['passCode'] as String,
      otp: json['otp'] as String,
    );

Map<String, dynamic> _$ConfigMethodVerifyRequestToJson(
        ConfigMethodVerifyRequest instance) =>
    <String, dynamic>{
      'serviceId': instance.serviceId,
      'verifyType': instance.verifyType,
      'actionType': instance.actionType,
      'passCode': instance.passCode,
      'otp': instance.otp,
    };
