import 'dart:typed_data';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/services/local_file_service/local_file_service.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/modal/full_screen_bottom_sheet.dart';
import 'package:epass/commons/widgets/modal/pdf_view_modal.dart';
import 'package:epass/commons/widgets/modal/photo_view_modal.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/attachment_file_bloc/attachment_file_bloc.dart';
import 'package:epass/pages/feedback/pages/list_feedback/bloc/feedback_bloc.dart';
import 'package:epass/commons/widgets/support_center_widgets/feedback_shimmer_loading.dart';
import 'package:epass/commons/widgets/support_center_widgets/feedback_widget.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:photo_view/photo_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:path/path.dart' as path;

class ListFeedbackPage extends StatefulWidget with AutoRouteWrapper {
  const ListFeedbackPage({Key? key}) : super(key: key);

  @override
  State<ListFeedbackPage> createState() => _ListFeedbackPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ListFeedbackBloc(
            feedbackRepository: getIt<IFeedbackRepository>(),
            firebaseMessaging: getIt<FirebaseMessaging>(),
          ),
        ),
        BlocProvider(
          create: (context) => AttachmentFileBloc(
            feedbackRepository: getIt<IFeedbackRepository>(),
          ),
        ),
      ],
      child: this,
    );
  }
}

class _ListFeedbackPageState extends State<ListFeedbackPage> {
  late RefreshController _refreshController;

  var _feedbackItems = <FeedbackItem>[];

  @override
  void initState() {
    super.initState();
    context.read<ListFeedbackBloc>().add(const FeedbackFetched(
          feedbackChannel: FeedbackChannel.suggestion,
        ));
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AttachmentFileBloc, AttachmentFileState>(
      listener: (_, state) async {
        if (state is AttachmentFileDownloadedInProgress) {
          context.loaderOverlay.show();
        } else if (state is AttachmentFileDownloadedFailure) {
          context.loaderOverlay.hide();
          await showErrorSnackBBar(context: context, message: state.message);
        } else if (state is AttachmentFileDownloadedSuccess) {
          context.loaderOverlay.hide();
          final extension = path.extension(state.fileName);

          switch (extension.toLowerCase()) {
            case '.png':
            case '.jpeg':
            case '.jpg':
              await showFullScreenBottomSheet(
                context: context,
                builder: (context) {
                  return PhotoViewModal(
                    child: PhotoView(
                      imageProvider: MemoryImage(Uint8List.fromList(state.attachmentFileData)),
                    ),
                  );
                },
              );
              break;
            case '.pdf':
              await showFullScreenBottomSheet(
                context: context,
                builder: (context) {
                  return PDFViewModal(
                    title: 'Chi tiết hoá đơn',
                    child: PDFView(
                      pdfData: Uint8List.fromList(state.attachmentFileData),
                      fitEachPage: true,
                    ),
                  );
                },
              );
              break;
            default:
              final saveFileResult = await getIt<ILocalFileService>().saveFile(
                data: state.attachmentFileData,
                fileName: state.fileName,
              );

              await saveFileResult.when(
                success: (file) async {
                  await showSuccessSnackBBar(
                    context: context,
                    message: 'Tệp tin tải về thành công tại đường dẫn: ${file.path}',
                  );
                },
                failure: (failure) async {
                  await showErrorSnackBBar(
                    context: context,
                    message: failure.message ?? 'Có lỗi xảy ra',
                  );
                },
              );

              break;
          }
        }
      },
      child: BlocConsumer<ListFeedbackBloc, FeedbackState>(
        listener: (context, state) {
          if (state.error == null) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          } else if (state.error != null) {
            _refreshController.refreshFailed();
            _refreshController.loadFailed();

            if (state.listFeedback.isNotEmpty) {
              showErrorSnackBBar(
                context: context,
                message: state.error ?? 'Có lỗi xảy ra',
              );
            }
          }
        },
        builder: (context, state) {
          final error = state.error;

          if (state.isLoading || state.isRefreshing && state.listFeedback.isEmpty) {
            return const FeedbackShimmerLoading();
          } else if (error != null && state.listFeedback.isEmpty) {
            return CommonErrorPage(
              message: error,
              onTap: () => context.read<ListFeedbackBloc>().add(const FeedbackFetched()),
            );
          }
          _feedbackItems = state.listFeedback.map((e) => FeedbackItem(feedback: e)).toList();

          if (_feedbackItems.isEmpty) {
            return NoDataPage(
              title: 'Bạn chưa thực hiện gửi góp ý',
              description: 'Vui lòng thực hiện gửi góp ý',
              isImageError: false,
              onTap: () => context.read<ListFeedbackBloc>().add(const FeedbackRefreshed()),
            );
          }
          return SmartRefresher(
            physics: const BouncingScrollPhysics(),
            enablePullDown: true,
            onRefresh: () => context.read<ListFeedbackBloc>().add(const FeedbackRefreshed()),
            onLoading: () => context.read<ListFeedbackBloc>().add(const FeedbackFetched()),
            controller: _refreshController,
            child: ListView.builder(
              itemCount: _feedbackItems.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.fromLTRB(
                    16.w,
                    index == 0 ? 32.h : 16.h,
                    16.w,
                    index == _feedbackItems.length - 1 ? 32.h : 0,
                  ),
                  child: ShadowCard(
                    padding: EdgeInsets.fromLTRB(16.w, 12.h, 0.w, 16.h),
                    shadowOpacity: 0.2,
                    child: FeedbackWidget(
                      key: UniqueKey(),
                      feedbackItem: _feedbackItems[index],
                      onExpandToggle: (isExpanded) {
                        _feedbackItems[index].isExpanded = isExpanded;
                      },
                    ),
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
