// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ocs_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OcsInfo _$OcsInfoFromJson(Map<String, dynamic> json) => OcsInfo(
      contractId: json['contractId'] as int?,
      custId: json['custId'] as int?,
      contractNo: json['contractNo'] as String?,
      signDate: json['signDate'] as String?,
      effDate: json['effDate'] as String?,
      status: json['status'] as String?,
      emailNotification: json['emailNotification'] as String?,
      smsNotification: json['smsNotification'] as String?,
      pushNotification: json['pushNotification'] as String?,
      billCycle: json['billCycle'] as String?,
      payCharge: json['payCharge'] as String?,
      accountUser: json['accountUser'] as String?,
      noticeName: json['noticeName'] as String?,
      noticeAreaName: json['noticeAreaName'] as String?,
      noticeStreet: json['noticeStreet'] as String?,
      noticeAreaCode: json['noticeAreaCode'] as String?,
      noticeEmail: json['noticeEmail'] as String?,
      noticePhoneNumber: json['noticePhoneNumber'] as String?,
      profileStatus: json['profileStatus'] as String?,
      approvedUser: json['approvedUser'] as String?,
      approvedDate: json['approvedDate'] as int?,
      signName: json['signName'] as String?,
      smsRenew: json['smsRenew'] as String?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
      balance: json['balance'] as int?,
      billCycleMergeType: json['billCycleMergeType'] as String?,
      isAlertMoney: json['isAlertMoney'] as int?,
      alertMoney: json['alertMoney'] as int?,
      isSpecial: json['isSpecial'] as int?,
    );

Map<String, dynamic> _$OcsInfoToJson(OcsInfo instance) => <String, dynamic>{
      'contractId': instance.contractId,
      'custId': instance.custId,
      'contractNo': instance.contractNo,
      'signDate': instance.signDate,
      'effDate': instance.effDate,
      'status': instance.status,
      'emailNotification': instance.emailNotification,
      'smsNotification': instance.smsNotification,
      'pushNotification': instance.pushNotification,
      'billCycle': instance.billCycle,
      'payCharge': instance.payCharge,
      'accountUser': instance.accountUser,
      'noticeName': instance.noticeName,
      'noticeAreaName': instance.noticeAreaName,
      'noticeStreet': instance.noticeStreet,
      'noticeAreaCode': instance.noticeAreaCode,
      'noticeEmail': instance.noticeEmail,
      'noticePhoneNumber': instance.noticePhoneNumber,
      'profileStatus': instance.profileStatus,
      'approvedUser': instance.approvedUser,
      'approvedDate': instance.approvedDate,
      'signName': instance.signName,
      'smsRenew': instance.smsRenew,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'balance': instance.balance,
      'billCycleMergeType': instance.billCycleMergeType,
      'isAlertMoney': instance.isAlertMoney,
      'alertMoney': instance.alertMoney,
      'isSpecial': instance.isSpecial,
    };

OCSInfoResponse _$OCSInfoResponseFromJson(Map<String, dynamic> json) =>
    OCSInfoResponse(
      ocsInfo: json['data'] == null
          ? null
          : OcsInfo.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OCSInfoResponseToJson(OCSInfoResponse instance) =>
    <String, dynamic>{
      'data': instance.ocsInfo,
      'mess': instance.mess,
    };
