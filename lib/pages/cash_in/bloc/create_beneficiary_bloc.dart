import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:equatable/equatable.dart';

part 'create_beneficiary_event.dart';
part 'create_beneficiary_state.dart';

class CreateBeneficiaryBloc
    extends Bloc<CreateBeneficiaryEvent, CreateBeneficiaryState> {
  final IBeneficiaryRepository _beneficiaryRepository;
  CreateBeneficiaryBloc({
    required IBeneficiaryRepository beneficiaryRepository,
  })  : _beneficiaryRepository = beneficiaryRepository,
        super(CreateBeneficiaryInitial()) {
    on<CreateBeneficiaryFormSubmitted>(_onCreateBeneficiaryFormSubmitted);
  }

  FutureOr<void> _onCreateBeneficiaryFormSubmitted(
    CreateBeneficiaryFormSubmitted event,
    Emitter<CreateBeneficiaryState> emit,
  ) async {
    emit(const CreateBeneficiaryFormSubmittedInProgress());

    final contractNo = event.contractNo;
    final customerName = event.customerName;
    final reminiscentName = event.reminiscentName;
    final result = await _beneficiaryRepository.createBeneficiary(
      contractNo: contractNo,
      customerName: customerName,
      reminiscentName: reminiscentName,
    );

    result.when(
      success: (success) => emit(const CreateBeneficiaryFormSubmittedSuccess()),
      failure: (failure) => emit(CreateBeneficiaryFormSubmittedFailure(
          failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
