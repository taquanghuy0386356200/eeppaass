import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/pages/app.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:flutter/material.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:auto_route/auto_route.dart';

class OnboardingContent {
  final String title;
  final String subtitle;
  final Widget image;

  OnboardingContent({
    required this.title,
    required this.subtitle,
    required this.image,
  });
}

class OnboardingPage extends StatefulWidget {
  const OnboardingPage({Key? key}) : super(key: key);

  @override
  State<OnboardingPage> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  int _currentPage = 0;

  final _contents = [
    OnboardingContent(
      title: 'Liên kết \nViettel Money - ePass',
      subtitle: 'dễ dàng, tiện lợi',
      image: Assets.images.onboarding.first.image(),
    ),
    OnboardingContent(
      title: 'Nạp tiền nhanh chóng',
      subtitle: 'đa dạng hình thức thanh toán',
      image: Assets.images.onboarding.second.image(),
    ),
    OnboardingContent(
      title: 'Qua trạm 3K',
      subtitle: 'Không dừng, Không tiền mặt,\n Không tiếp xúc',
      image: Assets.images.onboarding.third.image(),
    ),
  ];

  final _pageController = PageController();

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SettingBloc, SettingState>(
      listenWhen: (previous, current) =>
          previous.isFirstRun != current.isFirstRun,
      listener: (context, state) {
        if (!state.isFirstRun) {
          context.replaceRoute(const LoginRoute());
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            children: [
              const Spacer(flex: 1),
              Expanded(
                flex: 5,
                child: PageView.builder(
                  controller: _pageController,
                  physics: const BouncingScrollPhysics(),
                  onPageChanged: (value) {
                    setState(() {
                      _currentPage = value;
                    });
                  },
                  itemCount: _contents.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        _contents[index].image,
                        SizedBox(height: 44.h),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32.w),
                          child: FadeAnimation(
                            delay: 0.5,
                            direction: FadeDirection.down,
                            child: Text(
                              _contents[index].title,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5!
                                  .copyWith(
                                    color: ColorName.textGray1,
                                    fontWeight: FontWeight.w700,
                                    height: 1.3,
                                  ),
                            ),
                          ),
                        ),
                        SizedBox(height: 14.h),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 32.w),
                          child: FadeAnimation(
                            delay: 1,
                            direction: FadeDirection.down,
                            child: Text(
                              _contents[index].subtitle,
                              textAlign: TextAlign.center,
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    color: ColorName.textGray2,
                                  ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 32.w, right: 16.w),
                child: Row(
                  children: [
                    Visibility(
                      visible: _currentPage != _contents.length - 1,
                      maintainSize: true,
                      maintainAnimation: true,
                      maintainState: true,
                      child: TextButton(
                        onPressed: () {
                          // canNotShowPopupUpdateApp = true;
                          context.read<SettingBloc>().add(FirstRun());
                        },
                        child: Text(
                          'Bỏ qua',
                          style:
                              Theme.of(context).textTheme.headline6!.copyWith(
                                    color: ColorName.borderColor,
                                    fontSize: 18.sp,
                                  ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(
                          _contents.length,
                          (index) => buildDot(index: index),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: _currentPage != _contents.length - 1
                          ? () => _pageController.nextPage(
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.ease,
                              )
                          : () {
                              // canNotShowPopupUpdateApp = true;
                              context.read<SettingBloc>().add(FirstRun());
                            },
                      child: Text(
                        _currentPage != _contents.length - 1
                            ? 'Tiếp theo'
                            : 'Đăng nhập',
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: ColorName.primaryColor,
                              fontSize: 18.sp,
                            ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      margin: EdgeInsets.only(right: 4.w),
      height: 6.r,
      width: _currentPage == index ? 20.r : 6.r,
      decoration: BoxDecoration(
        color: _currentPage == index
            ? ColorName.primaryColor
            : ColorName.textGray4,
        borderRadius: BorderRadius.circular(8.0),
        gradient: _currentPage == index
            ? const LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                colors: [
                  ColorName.primaryGradientStart,
                  ColorName.primaryGradientEnd,
                ],
              )
            : null,
      ),
    );
  }
}
