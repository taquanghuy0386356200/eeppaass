import 'package:json_annotation/json_annotation.dart';

part 'check_passcode_request.g.dart';

@JsonSerializable()
class CheckPassCodeRequest {
  int contractId;
  int serviceId;
  int passCode;

  CheckPassCodeRequest({
    required this.contractId,
    required this.serviceId,
    required this.passCode
  });

  Map<String, dynamic> toJson() => _$CheckPassCodeRequestToJson(this);
}