import 'package:json_annotation/json_annotation.dart';

@JsonEnum(alwaysCreate: true)
enum BooCode {
  @JsonValue('BOO1')
  boo1,
  @JsonValue('BOO2')
  boo2,
  unknown,
}

extension BooCodeExt on BooCode {
  String get value {
    switch (this) {
      case BooCode.boo1:
        return 'BOO1';
      case BooCode.boo2:
        return 'BOO2';
      case BooCode.unknown:
        return 'unknown';
    }
  }
}
