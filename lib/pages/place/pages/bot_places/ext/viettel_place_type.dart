enum ViettelPlaceType {
  viettelStore,
  // viettelPost,
  // viettelTelecom,
}

extension ViettelPlaceTypeExt on ViettelPlaceType {
  String get name {
    switch (this) {
      case ViettelPlaceType.viettelStore:
        return 'Cửa hàng / Siêu thị Viettel';
      // case ViettelPlaceType.viettelPost:
      //   return 'Viettel Post';
      // case ViettelPlaceType.viettelTelecom:
      //   return 'Viettel Telecom';
    }
  }

  String get desc {
    switch (this) {
      case ViettelPlaceType.viettelStore:
      // case ViettelPlaceType.viettelPost:
      // case ViettelPlaceType.viettelTelecom:
        return 'Tìm kiếm điểm dán thẻ / nạp tiền gần bạn nhất';
    }
  }

  Map<String, String> _googleMapURLParams({
    double? longitude,
    double? latitude,
  }) {
    var map = {
      'q': name,
    };
    if (latitude != null && longitude != null) {
      map['ll'] = '$latitude, $longitude';
    }
    map['zoom'] = '15';
    return map;
  }

  String? getGoogleMapUrl({
    double? longitude,
    double? latitude,
    required bool openApp,
  }) {
    final params = _googleMapURLParams(
      latitude: latitude,
      longitude: longitude,
    );

    final googleMapURLEncodedParams =
        params.entries.map((e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}').join('&');

    if (openApp) {
      return Uri(
        scheme: 'comgooglemapsurl',
        host: 'maps.google.com',
        query: googleMapURLEncodedParams,
      ).toString();
    } else {
      return Uri(
        scheme: 'https',
        host: 'maps.google.com',
        query: googleMapURLEncodedParams,
      ).toString();
    }
  }
}
