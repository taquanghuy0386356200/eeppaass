import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/station_repository.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/place/pages/bot_places/bloc/bot_place_bloc.dart';
import 'package:epass/pages/place/pages/bot_places/widget/bot_place_card.dart';
import 'package:epass/pages/place/pages/bot_places/widget/bot_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/gen/assets.gen.dart';

class BotPlacesPage extends StatefulWidget with AutoRouteWrapper {
  const BotPlacesPage({Key? key}) : super(key: key);

  @override
  State<BotPlacesPage> createState() => _BotPlacesPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (context) => BotPlaceBloc(
        stationRepository: getIt<IStationRepository>(),
      ),
      child: this,
    );
  }
}

class _BotPlacesPageState extends State<BotPlacesPage> {
  late RefreshController _refreshController;
  late TextEditingController _searchController;

  @override
  void initState() {
    super.initState();
    context.read<BotPlaceBloc>().add(const BotPlaceFetched());
    _searchController = TextEditingController();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BotPlaceBloc, BotPlaceState>(
      listener: (context, state) {
        if (state.error == null) {
          _refreshController.refreshCompleted();
          _refreshController.loadComplete();
        } else if (state.error != null) {
          _refreshController.refreshFailed();
          _refreshController.loadFailed();

          if (state.listData.isNotEmpty) {
            showErrorSnackBBar(
              context: context,
              message: state.error ?? 'Có lỗi xảy ra',
            );
          }
        }
      },
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 24.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: PrimaryTextField(
                controller: _searchController,
                hintText: 'Tìm kiếm',
                hasClearButton: true,
                onClear: () => context
                    .read<BotPlaceBloc>()
                    .add(BotPlaceSearched(_searchController.text)),
                onChanged: (value) =>
                    context.read<BotPlaceBloc>().add(BotPlaceSearched(value)),
                suffix: Assets.icons.search.svg(
                  width: 24.r,
                  height: 24.r,
                  color: ColorName.borderColor,
                ),
              ),
            ),
            SizedBox(height: 16.h),
            Expanded(
              child: Builder(
                builder: (_) {
                  if (state.isLoading ||
                      state.isRefreshing && state.filteredListData.isEmpty) {
                    return const BotPlaceShimmerLoading();
                  } else if (state.error != null &&
                      state.filteredListData.isEmpty) {
                    return CommonErrorPage(
                      message: state.error,
                      onTap: () => context
                          .read<BotPlaceBloc>()
                          .add(const BotPlaceFetched()),
                    );
                  }
                  final listData = state.filteredListData;
                  if (listData.isEmpty) {
                    return NoDataPage(
                      onTap: () => context
                          .read<BotPlaceBloc>()
                          .add(const BotPlaceFetched()),
                    );
                  }

                  return SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    enablePullUp: false,
                    onRefresh: () => context
                        .read<BotPlaceBloc>()
                        .add(const BotPlaceRefreshed()),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        return BotPlaceCard(station: listData[index]);
                      },
                      separatorBuilder: (context, index) => Divider(
                        color: ColorName.disabledBorderColor,
                        height: 1,
                        thickness: 1,
                        indent: 36.w,
                        endIndent: 36.w,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
