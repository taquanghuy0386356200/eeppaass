package io.flutter.plugins;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.vnpay.authentication.VNP_AuthenticationActivity;
import com.vnpay.authentication.VNP_SdkCompletedCallback;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;

import io.flutter.plugin.common.MethodChannel;

public class VnPayUtils {
    private Context context;

    public void openSdk(@NotNull Activity activity, @NonNull MethodChannel.Result myResult, @Nullable HashMap<String, Object> arguments) {
        Intent intent = new Intent(activity, VNP_AuthenticationActivity.class);
        intent.putExtra("url", arguments.get("paymentUrl").toString());
        intent.putExtra("tmn_code", arguments.get("tmnCode").toString());
        intent.putExtra("scheme", "resultactivity");
        intent.putExtra("is_sandbox", Boolean.parseBoolean(arguments.get("isSandbox").toString()));
        VNP_AuthenticationActivity.setSdkCompletedCallback(new VNP_SdkCompletedCallback() {
            @Override
            public void sdkAction(String action) {
                Log.e("HCLog", "action: " + action);
                switch (action) {
                    case "AppBackAction":
                        //Người dùng nhấn back từ sdk để quay lại
                        break;
                    case "CallMobileBankingApp":
                        //Người dùng nhấn chọn thanh toán qua app thanh toán (Mobile Banking, Ví...)
                        //lúc này app tích hợp sẽ cần lưu lại mã giao dịch thanh toán (vnp_TxnRef). Khi người dùng mở lại app tích hợp với cheme thì sẽ gọi kiểm tra trạng thái thanh toán của mã TxnRef đó kiểm tra xem đã thanh toán hay chưa để thực hiện nghiệp vụ kết thúc thanh toán / thông báo kết quả cho khách hàng..

                        break;
                    case "WebBackAction":
                        //Tạo nút sự kiện cho user click từ return url của merchant chuyển hướng về URL: http://cancel.sdk.merchantbackapp
                        // vnp_ResponseCode == 24 / Khách hàng hủy thanh toán.
                        myResult.success("24");
                        break;
                    case "FaildBackAction":
                        //Tạo nút sự kiện cho user click từ return url của merchant chuyển hướng về URL: http://fail.sdk.merchantbackapp
                        // vnp_ResponseCode != 00 / Giao dịch thanh toán không thành công
                        myResult.success("-1");
                        break;
                    case "SuccessBackAction":
                        //Tạo nút sự kiện cho user click từ return url của merchant chuyển hướng về URL: http://success.sdk.merchantbackapp
                        //vnp_ResponseCode == 00) / Giao dịch thành công
                        myResult.success("00");
                        break;
                }
            }
        });
        activity.startActivity(intent);
    }
}
