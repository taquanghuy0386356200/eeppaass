/// value : "value"
/// newValue : "newValue"

class ChangePasswordRequest {
  ChangePasswordRequest({
    required this.value,
    required this.newValue,
  });

  final String value;
  final String newValue;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['value'] = value;
    map['newValue'] = newValue;
    return map;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}
