part of 'feedback_type_bloc.dart';

@immutable
abstract class FeedbackTypeEvent extends Equatable {
  const FeedbackTypeEvent();
}

class FeedbackTypeFetched extends FeedbackTypeEvent {
  final int? parentId;
  final FeedbackChannel feedbackChannel;

  const FeedbackTypeFetched({
    this.parentId,
    required this.feedbackChannel,
  });

  @override
  List<Object?> get props => [parentId, feedbackChannel];
}
