import 'package:epass/commons/enum/boo_code.dart';
import 'package:epass/commons/enum/method_charge.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

/// vehicleGroupId : 5
/// stationType : 1
/// stationId : 5033
/// chargeMethodId : 2
/// autoRenew : 0
/// fee : 3535000
/// status : 2
/// effDate : "11/03/2022"
/// plateNumber : "18L20239"
/// vehicleTypeId : 21
/// cargoWeight : 100000.0
/// seatNumber : 3
/// booCode : "BOO2"
/// expDate : "09/04/2022 23:59:59"
/// vehicleId : 827582
/// dateOfUse : "11/03/2022 17:30:14"

part 'ticket_price_response.g.dart';

@JsonSerializable()
class TicketPrice extends Equatable {
  const TicketPrice({
    this.vehicleGroupId,
    this.stationType,
    this.stationId,
    this.chargeMethodId,
    this.fee,
    this.status,
    this.effDate,
    this.plateNumber,
    this.vehicleTypeId,
    this.netWeight,
    this.cargoWeight,
    this.seatNumber,
    this.epc,
    this.booCode,
    this.expDate,
    this.vehicleId,
    this.dateOfUse,
    this.message,
  });

  final int? vehicleGroupId;
  final int? stationType;
  final int? stationId;

  @JsonKey(unknownEnumValue: MethodChargeInt.unknown)
  final MethodChargeInt? chargeMethodId;

  final int? fee;
  final int? status;
  final String? effDate;
  final String? plateNumber;
  final int? vehicleTypeId;
  final double? netWeight;
  final double? cargoWeight;
  final int? seatNumber;
  final String? epc;

  @JsonKey(unknownEnumValue: BooCode.unknown)
  final BooCode? booCode;

  final String? expDate;
  final int? vehicleId;

  final String? dateOfUse;

  final String? message;

  factory TicketPrice.fromJson(Map<String, dynamic> json) =>
      _$TicketPriceFromJson(json);

  @override
  List<Object?> get props => [
        vehicleGroupId,
        stationType,
        stationId,
        chargeMethodId,
        fee,
        status,
        effDate,
        plateNumber,
        vehicleTypeId,
        cargoWeight,
        seatNumber,
        booCode,
        expDate,
        vehicleId,
        dateOfUse,
        message,
      ];
}

@JsonSerializable()
class TicketPriceData {
  final List<TicketPrice>? listServicePlan;

  const TicketPriceData({
    this.listServicePlan,
  });

  factory TicketPriceData.fromJson(Map<String, dynamic> json) =>
      _$TicketPriceDataFromJson(json);
}

class TicketPriceDataNotNull {
  final List<TicketPrice> listServicePlan;

  TicketPriceDataNotNull({
    required this.listServicePlan,
  });
}

/// data : {"listServicePlan":[{"vehicleGroupId":5,"stationType":1,"stationId":5033,"chargeMethodId":2,"autoRenew":0,"fee":3535000,"status":2,"effDate":"11/03/2022","plateNumber":"18L20239","vehicleTypeId":21,"cargoWeight":100000.0,"seatNumber":3,"booCode":"BOO2","expDate":"09/04/2022 23:59:59","vehicleId":827582,"dateOfUse":"11/03/2022 17:30:14"}]}
/// mess : {"code":1,"description":"Thành công"}

@JsonSerializable()
class TicketPriceResponse {
  const TicketPriceResponse({
    this.data,
    this.mess,
  });

  final TicketPriceData? data;
  final MessageData? mess;

  factory TicketPriceResponse.fromJson(Map<String, dynamic> json) =>
      _$TicketPriceResponseFromJson(json);
}
