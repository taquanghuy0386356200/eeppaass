import 'package:epass/commons/widgets/animations/shimmer_widget.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ParkingShimmerLoading extends StatelessWidget {
  const ParkingShimmerLoading({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShimmerColor(
      child: Container(
        padding: EdgeInsets.fromLTRB(16.w, 32.h, 16.w, 32.h),
        child: Column(
          children: [
            ShimmerWidget(width: double.infinity, height: 80.h),
            SizedBox(height: 10.h),
            Align(
              alignment: Alignment.centerRight,
              child: ShimmerWidget(width: 40.w, height: 20.h),
            ),
            SizedBox(height: 10.h),
            Divider(
              color: ColorName.disabledBorderColor,
              height: 1,
              thickness: 1,
              indent: 36.w,
              endIndent: 36.w,
            ),
            SizedBox(height: 10.h),
            ShimmerWidget(width: double.infinity, height: 100.h),
          ],
        )
      )
    );
  }
}
