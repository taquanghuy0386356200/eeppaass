import 'package:json_annotation/json_annotation.dart';

part 'update_vietmap_transaction_request.g.dart';

@JsonSerializable()
class UpdateVietMapTransactionRequest {
  UpdateVietMapTransactionRequest({
    this.saleOrderCode,
  });

  final String? saleOrderCode;

  factory UpdateVietMapTransactionRequest.fromJson(Map<String, dynamic> json) => _$UpdateVietMapTransactionRequestFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateVietMapTransactionRequestToJson(this);
}
