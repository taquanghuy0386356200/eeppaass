
import 'package:json_annotation/json_annotation.dart';

part 'add_service_request.g.dart';

@JsonSerializable()
class ActionModel {
  int serviceId;
  int actionType;

  ActionModel({
    required this.serviceId,
    required this.actionType,
  });

  factory ActionModel.fromJson(Map<String, dynamic> json) => _$ActionModelFromJson(json);
  Map<String, dynamic> toJson() => _$ActionModelToJson(this);

}

@JsonSerializable(explicitToJson: true)
class AddServiceRequest {

  List<ActionModel> actions;

  AddServiceRequest({
    required this.actions
  });

  Map<String, dynamic> toJson() => _$AddServiceRequestToJson(this);
}