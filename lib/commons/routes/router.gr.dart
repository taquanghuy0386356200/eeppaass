// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i88;
import 'package:epass/commons/models/beneficiary/beneficiary.dart' as _i92;
import 'package:epass/commons/models/vehicle/vehicle.dart' as _i93;
import 'package:epass/commons/models/viettel_pay_gate/viettel_pay_gate.dart'
    as _i94;
import 'package:epass/commons/routes/guards/first_run_guard.dart' as _i90;
import 'package:epass/commons/routes/router.dart' as _i17;
import 'package:epass/commons/services/bidv/bidv_check_link_request.dart'
    as _i95;
import 'package:epass/commons/services/vnpt/vnpay_payment_request.dart' as _i96;
import 'package:epass/pages/beneficary/beneficary_bloc_provider_page.dart'
    as _i27;
import 'package:epass/pages/beneficary/beneficary_details/beneficary_details_page.dart'
    as _i54;
import 'package:epass/pages/beneficary/beneficary_list/beneficary_list_page.dart'
    as _i53;
import 'package:epass/pages/buy_ticket/buy_ticket_bloc_provider_page.dart'
    as _i23;
import 'package:epass/pages/buy_ticket/buy_ticket_page.dart' as _i38;
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_confirm/buy_ticket_confirm_page.dart'
    as _i40;
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_result/buy_ticket_result_details_page.dart'
    as _i44;
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_result/buy_ticket_result_page.dart'
    as _i43;
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/vehicle_other_page.dart'
    as _i42;
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/vehicle_select_tab_page.dart'
    as _i39;
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/vehicle_user_page.dart'
    as _i41;
import 'package:epass/pages/car_service/car_service_page.dart' as _i20;
import 'package:epass/pages/cash_in/bloc/vnpt_gate_bloc.dart' as _i97;
import 'package:epass/pages/cash_in/cash_in_BIDV_pay_gate.dart' as _i36;
import 'package:epass/pages/cash_in/cash_in_bloc_provider_page.dart' as _i22;
import 'package:epass/pages/cash_in/cash_in_page.dart' as _i29;
import 'package:epass/pages/cash_in/cash_in_viettel_pay_gate.dart' as _i35;
import 'package:epass/pages/cash_in/cash_in_vnpt_pay_gate.dart' as _i37;
import 'package:epass/pages/cash_in/pages/cash_in_bank_transfer/cash_in_bank_transfer_page.dart'
    as _i31;
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_confirm_otp_page.dart'
    as _i33;
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_confirm_page.dart'
    as _i32;
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_result_page.dart'
    as _i34;
import 'package:epass/pages/cash_in/pages/cash_in_fee/cash_in_fee_page.dart'
    as _i30;
import 'package:epass/pages/feedback/feedback_tab_page.dart' as _i60;
import 'package:epass/pages/feedback/pages/create_feedback/create_feedback_page.dart'
    as _i62;
import 'package:epass/pages/feedback/pages/list_feedback/list_feedback_page.dart'
    as _i63;
import 'package:epass/pages/forgot_password/forgot_password_bloc_provider_page.dart'
    as _i3;
import 'package:epass/pages/forgot_password/forgot_password_new_password_page.dart'
    as _i8;
import 'package:epass/pages/forgot_password/forgot_password_otp_page.dart'
    as _i9;
import 'package:epass/pages/forgot_password/forgot_password_page.dart' as _i7;
import 'package:epass/pages/home/home_page.dart' as _i19;
import 'package:epass/pages/home_tab/home_tabs_page.dart' as _i5;
import 'package:epass/pages/insurance/anbinh/insurance_an_binh_page.dart'
    as _i57;
import 'package:epass/pages/insurance/gc/insurance_gc_page.dart' as _i58;
import 'package:epass/pages/insurance/insurance_page.dart' as _i55;
import 'package:epass/pages/insurance/viettelpay/insurance_viettelpay_page.dart'
    as _i56;
import 'package:epass/pages/invoice/invoice_page.dart' as _i24;
import 'package:epass/pages/invoice_setting/invoice_setting_page.dart' as _i68;
import 'package:epass/pages/login/login_page.dart' as _i2;
import 'package:epass/pages/notification_tab/notification_tab_page.dart'
    as _i18;
import 'package:epass/pages/notification_tab/pages/balance_change/balance_change_page.dart'
    as _i80;
import 'package:epass/pages/notification_tab/pages/discount/discount_page.dart'
    as _i82;
import 'package:epass/pages/notification_tab/pages/event/event_page.dart'
    as _i78;
import 'package:epass/pages/notification_tab/pages/news/news_page.dart' as _i81;
import 'package:epass/pages/notification_tab/pages/wait_for_confirmation/wait_for_confirmation_page.dart'
    as _i79;
import 'package:epass/pages/onboarding/onboarding_page.dart' as _i1;
import 'package:epass/pages/parking/pages/parking_setting/parking_bloc_provider_page.dart'
    as _i70;
import 'package:epass/pages/parking/pages/parking_setting/parking_bloc_provider_roadSide_page.dart'
    as _i71;
import 'package:epass/pages/parking/pages/parking_setting/parking_roadSide_page.dart'
    as _i77;
import 'package:epass/pages/parking/pages/parking_setting/parking_setting_page.dart'
    as _i76;
import 'package:epass/pages/place/pages/bot_places/bot_places_page.dart'
    as _i74;
import 'package:epass/pages/place/pages/viettel_places/viettel_places_page.dart'
    as _i75;
import 'package:epass/pages/place/place_tab_page.dart' as _i67;
import 'package:epass/pages/register/pages/register_address/register_address_page.dart'
    as _i13;
import 'package:epass/pages/register/pages/register_confirm/register_confirm_page.dart'
    as _i14;
import 'package:epass/pages/register/pages/register_customer/register_paper_scan_page.dart'
    as _i12;
import 'package:epass/pages/register/pages/register_customer/register_paper_type_page.dart'
    as _i11;
import 'package:epass/pages/register/pages/register_otp_confirm/regsiter_otp_page.dart'
    as _i16;
import 'package:epass/pages/register/pages/register_service/register_service_page.dart'
    as _i15;
import 'package:epass/pages/register/pages/register_vehicles/register_vehicles_page.dart'
    as _i10;
import 'package:epass/pages/register/register_bloc_provider_page.dart' as _i4;
import 'package:epass/pages/report/pages/create_report/create_report_page.dart'
    as _i64;
import 'package:epass/pages/report/pages/list_report/list_report_page.dart'
    as _i65;
import 'package:epass/pages/report/report_tab_page.dart' as _i61;
import 'package:epass/pages/setting/alias/alias_page.dart' as _i28;
import 'package:epass/pages/setting/app_info/app_info_page.dart' as _i87;
import 'package:epass/pages/setting/change_password/change_password_page.dart'
    as _i86;
import 'package:epass/pages/setting/notification_setting/notification_setting_page.dart'
    as _i85;
import 'package:epass/pages/setting/profile/profile_page.dart' as _i84;
import 'package:epass/pages/setting/referrals_epass/ui/referrals_client.dart'
    as _i73;
import 'package:epass/pages/setting/setting_entrance/setting_page.dart' as _i83;
import 'package:epass/pages/support/support_page.dart' as _i59;
import 'package:epass/pages/transaction_history_tab/pages/contract_action/contract_action_page.dart'
    as _i49;
import 'package:epass/pages/transaction_history_tab/pages/transaction_other/transaction_other_page.dart'
    as _i50;
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/transaction_parking_page.dart'
    as _i47;
import 'package:epass/pages/transaction_history_tab/pages/transaction_ticket_purchase/transaction_ticket_purchase_page.dart'
    as _i45;
import 'package:epass/pages/transaction_history_tab/pages/transaction_topup/transaction_topup_page.dart'
    as _i48;
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/transaction_vehicle_page.dart'
    as _i46;
import 'package:epass/pages/transaction_history_tab/transaction_history_tab_page.dart'
    as _i25;
import 'package:epass/pages/utility/cool_penalty/cool_penalty_page.dart'
    as _i69;
import 'package:epass/pages/utility/sms_register/sms_register_page.dart' as _i6;
import 'package:epass/pages/utility/utility_page.dart' as _i66;
import 'package:epass/pages/utility/vietmap/viet_map_page.dart' as _i72;
import 'package:epass/pages/vehicle/vehicle_bloc_provider_page.dart' as _i26;
import 'package:epass/pages/vehicle/vehicle_details/vehicle_details_page.dart'
    as _i52;
import 'package:epass/pages/vehicle/vehicle_list/vehicle_list_page.dart'
    as _i51;
import 'package:epass/pages/web/webview_page.dart' as _i21;
import 'package:flutter/foundation.dart' as _i91;
import 'package:flutter/material.dart' as _i89;

class AppRouter extends _i88.RootStackRouter {
  AppRouter({
    _i89.GlobalKey<_i89.NavigatorState>? navigatorKey,
    required this.firstRunGuard,
  }) : super(navigatorKey);

  final _i90.FirstRunGuard firstRunGuard;

  @override
  final Map<String, _i88.PageFactory> pagesMap = {
    OnboardingRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i1.OnboardingPage(),
      );
    },
    LoginRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i2.LoginPage()),
      );
    },
    ForgotPasswordRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i3.ForgotPasswordBlocProviderPage(),
      );
    },
    RegisterRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i4.RegisterBlocProviderPage(),
      );
    },
    HomeTabsRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i5.HomeTabsPage(),
      );
    },
    SmsRegisterRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i6.SmsRegisterPage(),
      );
    },
    ForgotPasswordRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i7.ForgotPasswordPage(),
      );
    },
    ForgotPasswordNewPasswordRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i8.ForgotPasswordNewPasswordPage(),
      );
    },
    ForgotPasswordOTPRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i9.ForgotPasswordOTPPage(),
      );
    },
    RegisterVehiclesRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i10.RegisterVehiclesPage(),
      );
    },
    RegisterPaperTypeRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i11.RegisterPaperTypePage(),
      );
    },
    RegisterPaperScanRoute.name: (routeData) {
      final args = routeData.argsAs<RegisterPaperScanRouteArgs>();
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i12.RegisterPaperScanPage(
          key: args.key,
          isFront: args.isFront,
        ),
      );
    },
    RegisterAddressRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i13.RegisterAddressPage(),
      );
    },
    RegisterConfirmRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i14.RegisterConfirmPage(),
      );
    },
    RegisterServiceRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i15.RegisterServicePage(),
      );
    },
    RegisterOTPRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i16.RegisterOTPPage(),
      );
    },
    HomeRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i17.EmptyRouterPage(),
      );
    },
    UtilityRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i17.EmptyRouterPage(),
      );
    },
    NotificationRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i18.NotificationTabPage(),
      );
    },
    SettingRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i17.EmptyRouterPage(),
      );
    },
    HomeRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i19.HomePage()),
      );
    },
    CarServiceRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i20.CarServicePage(),
      );
    },
    WebViewRoute.name: (routeData) {
      final args = routeData.argsAs<WebViewRouteArgs>();
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i21.WebViewPage(
          key: args.key,
          title: args.title,
          initUrl: args.initUrl,
        ),
      );
    },
    CashInRouter.name: (routeData) {
      final args = routeData.argsAs<CashInRouterArgs>(
          orElse: () => const CashInRouterArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i22.CashInBlocProviderPage(
          key: args.key,
          beneficary: args.beneficary,
        ),
      );
    },
    BuyTicketRouter.name: (routeData) {
      final args = routeData.argsAs<BuyTicketRouterArgs>(
          orElse: () => const BuyTicketRouterArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i23.BuyTicketBlocProviderPage(
          key: args.key,
          userVehicle: args.userVehicle,
        ),
      );
    },
    InvoiceRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i24.InvoicePage(),
      );
    },
    TransactionHistoryRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i25.TransactionHistoryTabPage(),
      );
    },
    VehicleRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i26.VehicleBlocProviderPage(),
      );
    },
    BenficaryRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i27.BeneficiaryBlocProviderPage(),
      );
    },
    InsuranceRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i17.EmptyRouterPage(),
      );
    },
    AliasDialogRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i28.AliasPage(),
      );
    },
    SupportRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i17.EmptyRouterPage(),
      );
    },
    CashInRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i29.CashInPage(),
      );
    },
    CashInFeeRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i30.CashInFeePage(),
      );
    },
    CashInBankTransferRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i31.CashInBankTransferPage(),
      );
    },
    CashInConfirmRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i32.CashInConfirmPage(),
      );
    },
    CashInConfirmOTPRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i33.CashInConfirmOTPPage(),
      );
    },
    CashInResultRoute.name: (routeData) {
      final args = routeData.argsAs<CashInResultRouteArgs>(
          orElse: () => const CashInResultRouteArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i34.CashInResultPage(
          key: args.key,
          isSuccess: args.isSuccess,
          message: args.message,
        ),
      );
    },
    CashInViettelPayGateRoute.name: (routeData) {
      final args = routeData.argsAs<CashInViettelPayGateRouteArgs>();
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i35.CashInViettelPayGatePage(
          key: args.key,
          title: args.title,
          request: args.request,
        ),
      );
    },
    CashInBIDVPayGateRoute.name: (routeData) {
      final args = routeData.argsAs<CashInBIDVPayGateRouteArgs>();
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i36.CashInBIDVPayGatePage(
          key: args.key,
          title: args.title,
          request: args.request,
        ),
      );
    },
    CashInVnptPayGateRoute.name: (routeData) {
      final args = routeData.argsAs<CashInVnptPayGateRouteArgs>();
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i37.CashInVnptPayGatePage(
          key: args.key,
          title: args.title,
          request: args.request,
          state: args.state,
          callback: args.callback,
        ),
      );
    },
    BuyTicketRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i38.BuyTicketPage(),
      );
    },
    VehicleSelectTabRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i39.VehicleSelectTabPage(),
      );
    },
    BuyTicketConfirmRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i40.BuyTicketConfirmPage(),
      );
    },
    BuyTicketResultRouter.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i17.EmptyRouterPage(),
      );
    },
    VehicleUserRoute.name: (routeData) {
      final args = routeData.argsAs<VehicleUserRouteArgs>(
          orElse: () => const VehicleUserRouteArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i41.VehicleUserPage(
          key: args.key,
          onChanged: args.onChanged,
        ),
      );
    },
    VehicleOtherRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i42.VehicleOtherPage()),
      );
    },
    BuyTicketResultRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i43.BuyTicketResultPage(),
      );
    },
    BuyTicketResultDetailRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i44.BuyTicketResultDetailPage(),
      );
    },
    TransactionTicketPurchaseRoute.name: (routeData) {
      final args = routeData.argsAs<TransactionTicketPurchaseRouteArgs>(
          orElse: () => const TransactionTicketPurchaseRouteArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i45.TransactionTicketPurchasePage(
          key: args.key,
          plateNumber: args.plateNumber,
        ),
      );
    },
    TransactionVehicleRoute.name: (routeData) {
      final args = routeData.argsAs<TransactionVehicleRouteArgs>(
          orElse: () => const TransactionVehicleRouteArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(
            child: _i46.TransactionVehiclePage(
          key: args.key,
          plateNumber: args.plateNumber,
        )),
      );
    },
    TransactionParkingRoute.name: (routeData) {
      final args = routeData.argsAs<TransactionParkingRouteArgs>(
          orElse: () => const TransactionParkingRouteArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i47.TransactionParkingPage(
          key: args.key,
          plateNumber: args.plateNumber,
        ),
      );
    },
    TransactionTopupRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i48.TransactionTopupPage(),
      );
    },
    ContractActionRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i49.ContractActionPage(),
      );
    },
    TransactionOtherRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i50.TransactionOtherPage(),
      );
    },
    VehicleListRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i51.VehicleListPage(),
      );
    },
    VehicleDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<VehicleDetailsRouteArgs>();
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i52.VehicleDetailsPage(
          key: args.key,
          vehicleId: args.vehicleId,
          contractId: args.contractId,
          fee: args.fee,
        ),
      );
    },
    BeneficiaryListRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i53.BeneficiaryListPage(),
      );
    },
    BeneficiaryDetailsRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<BeneficiaryDetailsRouteArgs>(
          orElse: () => BeneficiaryDetailsRouteArgs(
              beneficiaryId: pathParams.getString('id')));
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i54.BeneficiaryDetailsPage(
          key: args.key,
          beneficiaryId: args.beneficiaryId,
        ),
      );
    },
    InsuranceRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i55.InsurancePage()),
      );
    },
    InsuranceViettelPayRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i56.InsuranceViettelPayPage(),
      );
    },
    InsuranceAnBinhRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i57.InsuranceAnBinhPage(),
      );
    },
    InsuranceGCRoute.name: (routeData) {
      final args = routeData.argsAs<InsuranceGCRouteArgs>(
          orElse: () => const InsuranceGCRouteArgs());
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i58.InsuranceGCPage(
          key: args.key,
          contractNoEncrypted: args.contractNoEncrypted,
        ),
      );
    },
    SupportRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i59.SupportPage(),
      );
    },
    FeedbackTabRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i60.FeedbackTabPage(),
      );
    },
    ReportTabRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i61.ReportTabPage(),
      );
    },
    CreateFeedbackRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i62.CreateFeedbackPage(),
      );
    },
    ListFeedbackRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i63.ListFeedbackPage()),
      );
    },
    CreateReportRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i64.CreateReportPage(),
      );
    },
    ListReportRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i65.ListReportPage()),
      );
    },
    UtilityRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i66.UtilityPage(),
      );
    },
    PlaceTabRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i67.PlaceTabPage(),
      );
    },
    InvoiceSettingRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i68.InvoiceSettingPage(),
      );
    },
    CoolPenaltyRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i69.CoolPenaltyPage(),
      );
    },
    ParkingBlocProviderRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i70.ParkingBlocProviderPage(),
      );
    },
    ParkingBlocProviderRoadSideRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i71.ParkingBlocProviderRoadSidePage(),
      );
    },
    VietMapRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i72.VietMapPage(),
      );
    },
    ReferralsClientRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i73.ReferralsClientPage()),
      );
    },
    BotPlacesRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i74.BotPlacesPage()),
      );
    },
    ViettelPlacesRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i75.ViettelPlacesPage(),
      );
    },
    ParkingSettingRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i76.ParkingSettingPage(),
      );
    },
    ParkingRoadSideRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i77.ParkingRoadSidePage(),
      );
    },
    EventRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i78.EventPage(),
      );
    },
    WaitForConfirmationRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i79.WaitForConfirmationPage()),
      );
    },
    BalanceChangeRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i80.BalanceChangePage(),
      );
    },
    NewsRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i81.NewsPage(),
      );
    },
    DiscountRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i82.DiscountPage(),
      );
    },
    SettingRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i88.WrappedRoute(child: const _i83.SettingPage()),
      );
    },
    ProfileRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i84.ProfilePage(),
      );
    },
    NotificationSettingRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i85.NotificationSettingPage(),
      );
    },
    AliasRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i28.AliasPage(),
      );
    },
    ChangePasswordRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i86.ChangePasswordPage(),
      );
    },
    AppInfoRoute.name: (routeData) {
      return _i88.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i87.AppInfoPage(),
      );
    },
  };

  @override
  List<_i88.RouteConfig> get routes => [
        _i88.RouteConfig(
          '/#redirect',
          path: '/',
          redirectTo: '/login',
          fullMatch: true,
        ),
        _i88.RouteConfig(
          OnboardingRoute.name,
          path: '/onboarding',
        ),
        _i88.RouteConfig(
          LoginRoute.name,
          path: '/login',
          guards: [firstRunGuard],
        ),
        _i88.RouteConfig(
          ForgotPasswordRouter.name,
          path: '/forgot-password',
          children: [
            _i88.RouteConfig(
              ForgotPasswordRoute.name,
              path: '',
              parent: ForgotPasswordRouter.name,
            ),
            _i88.RouteConfig(
              ForgotPasswordNewPasswordRoute.name,
              path: 'new-password',
              parent: ForgotPasswordRouter.name,
            ),
            _i88.RouteConfig(
              ForgotPasswordOTPRoute.name,
              path: 'otp',
              parent: ForgotPasswordRouter.name,
            ),
          ],
        ),
        _i88.RouteConfig(
          RegisterRouter.name,
          path: '/register',
          children: [
            _i88.RouteConfig(
              RegisterVehiclesRoute.name,
              path: '',
              parent: RegisterRouter.name,
            ),
            _i88.RouteConfig(
              RegisterPaperTypeRoute.name,
              path: 'verify',
              parent: RegisterRouter.name,
            ),
            _i88.RouteConfig(
              RegisterPaperScanRoute.name,
              path: 'scan',
              parent: RegisterRouter.name,
            ),
            _i88.RouteConfig(
              RegisterAddressRoute.name,
              path: 'address',
              parent: RegisterRouter.name,
            ),
            _i88.RouteConfig(
              RegisterConfirmRoute.name,
              path: 'confirm',
              parent: RegisterRouter.name,
            ),
            _i88.RouteConfig(
              RegisterServiceRoute.name,
              path: 'service',
              parent: RegisterRouter.name,
            ),
            _i88.RouteConfig(
              RegisterOTPRoute.name,
              path: 'otp',
              parent: RegisterRouter.name,
            ),
          ],
        ),
        _i88.RouteConfig(
          HomeTabsRoute.name,
          path: '/home',
          children: [
            _i88.RouteConfig(
              HomeRouter.name,
              path: '',
              parent: HomeTabsRoute.name,
              children: [
                _i88.RouteConfig(
                  HomeRoute.name,
                  path: '',
                  parent: HomeRouter.name,
                ),
                _i88.RouteConfig(
                  CarServiceRoute.name,
                  path: 'carServices',
                  parent: HomeRouter.name,
                ),
                _i88.RouteConfig(
                  WebViewRoute.name,
                  path: 'webview',
                  parent: HomeRouter.name,
                ),
                _i88.RouteConfig(
                  CashInRouter.name,
                  path: 'cash-in',
                  parent: HomeRouter.name,
                  children: [
                    _i88.RouteConfig(
                      CashInRoute.name,
                      path: '',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInFeeRoute.name,
                      path: 'fee',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInBankTransferRoute.name,
                      path: 'bank-transfer',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInConfirmRoute.name,
                      path: 'confirm',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInConfirmOTPRoute.name,
                      path: 'check-otp-bidv',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInResultRoute.name,
                      path: 'result',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInViettelPayGateRoute.name,
                      path: 'viettel-pay-gate',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInBIDVPayGateRoute.name,
                      path: 'bidv-pay-gate',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInVnptPayGateRoute.name,
                      path: 'vnpt-pay-gate',
                      parent: CashInRouter.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  BuyTicketRouter.name,
                  path: 'buy-ticket',
                  parent: HomeRouter.name,
                  children: [
                    _i88.RouteConfig(
                      BuyTicketRoute.name,
                      path: '',
                      parent: BuyTicketRouter.name,
                    ),
                    _i88.RouteConfig(
                      VehicleSelectTabRoute.name,
                      path: 'vehicle',
                      parent: BuyTicketRouter.name,
                      children: [
                        _i88.RouteConfig(
                          VehicleUserRoute.name,
                          path: 'user',
                          parent: VehicleSelectTabRoute.name,
                        ),
                        _i88.RouteConfig(
                          VehicleOtherRoute.name,
                          path: 'other',
                          parent: VehicleSelectTabRoute.name,
                        ),
                      ],
                    ),
                    _i88.RouteConfig(
                      BuyTicketConfirmRoute.name,
                      path: 'confirm',
                      parent: BuyTicketRouter.name,
                    ),
                    _i88.RouteConfig(
                      BuyTicketResultRouter.name,
                      path: 'result',
                      parent: BuyTicketRouter.name,
                      children: [
                        _i88.RouteConfig(
                          BuyTicketResultRoute.name,
                          path: '',
                          parent: BuyTicketResultRouter.name,
                        ),
                        _i88.RouteConfig(
                          BuyTicketResultDetailRoute.name,
                          path: 'details',
                          parent: BuyTicketResultRouter.name,
                        ),
                      ],
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  InvoiceRoute.name,
                  path: 'invoice',
                  parent: HomeRouter.name,
                ),
                _i88.RouteConfig(
                  TransactionHistoryRouter.name,
                  path: 'transaction-history',
                  parent: HomeRouter.name,
                  children: [
                    _i88.RouteConfig(
                      TransactionTicketPurchaseRoute.name,
                      path: 'transaction-ticket-purchase',
                      parent: TransactionHistoryRouter.name,
                    ),
                    _i88.RouteConfig(
                      TransactionVehicleRoute.name,
                      path: 'transaction-vehicle',
                      parent: TransactionHistoryRouter.name,
                    ),
                    _i88.RouteConfig(
                      TransactionParkingRoute.name,
                      path: 'transaction-parking',
                      parent: TransactionHistoryRouter.name,
                    ),
                    _i88.RouteConfig(
                      TransactionTopupRoute.name,
                      path: 'transaction-topup',
                      parent: TransactionHistoryRouter.name,
                    ),
                    _i88.RouteConfig(
                      ContractActionRoute.name,
                      path: 'contract-action',
                      parent: TransactionHistoryRouter.name,
                    ),
                    _i88.RouteConfig(
                      TransactionOtherRoute.name,
                      path: 'transaction-other',
                      parent: TransactionHistoryRouter.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  VehicleRouter.name,
                  path: 'vehicles',
                  parent: HomeRouter.name,
                  children: [
                    _i88.RouteConfig(
                      VehicleListRoute.name,
                      path: '',
                      parent: VehicleRouter.name,
                    ),
                    _i88.RouteConfig(
                      VehicleDetailsRoute.name,
                      path: ':id',
                      parent: VehicleRouter.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  BenficaryRouter.name,
                  path: 'beneficary',
                  parent: HomeRouter.name,
                  children: [
                    _i88.RouteConfig(
                      BeneficiaryListRoute.name,
                      path: '',
                      parent: BenficaryRouter.name,
                    ),
                    _i88.RouteConfig(
                      BeneficiaryDetailsRoute.name,
                      path: ':id',
                      parent: BenficaryRouter.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  InsuranceRouter.name,
                  path: 'insurance',
                  parent: HomeRouter.name,
                  children: [
                    _i88.RouteConfig(
                      InsuranceRoute.name,
                      path: '',
                      parent: InsuranceRouter.name,
                    ),
                    _i88.RouteConfig(
                      InsuranceViettelPayRoute.name,
                      path: 'vds',
                      parent: InsuranceRouter.name,
                    ),
                    _i88.RouteConfig(
                      InsuranceAnBinhRoute.name,
                      path: 'aba',
                      parent: InsuranceRouter.name,
                    ),
                    _i88.RouteConfig(
                      InsuranceGCRoute.name,
                      path: 'gc',
                      parent: InsuranceRouter.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  AliasDialogRoute.name,
                  path: 'alias-dialog',
                  parent: HomeRouter.name,
                ),
                _i88.RouteConfig(
                  SupportRouter.name,
                  path: 'support',
                  parent: HomeRouter.name,
                  children: [
                    _i88.RouteConfig(
                      SupportRoute.name,
                      path: '',
                      parent: SupportRouter.name,
                    ),
                    _i88.RouteConfig(
                      FeedbackTabRoute.name,
                      path: 'feedback',
                      parent: SupportRouter.name,
                      children: [
                        _i88.RouteConfig(
                          CreateFeedbackRoute.name,
                          path: 'create',
                          parent: FeedbackTabRoute.name,
                        ),
                        _i88.RouteConfig(
                          ListFeedbackRoute.name,
                          path: 'list',
                          parent: FeedbackTabRoute.name,
                        ),
                      ],
                    ),
                    _i88.RouteConfig(
                      ReportTabRoute.name,
                      path: 'report',
                      parent: SupportRouter.name,
                      children: [
                        _i88.RouteConfig(
                          CreateReportRoute.name,
                          path: 'create',
                          parent: ReportTabRoute.name,
                        ),
                        _i88.RouteConfig(
                          ListReportRoute.name,
                          path: 'list',
                          parent: ReportTabRoute.name,
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            _i88.RouteConfig(
              UtilityRouter.name,
              path: 'utility',
              parent: HomeTabsRoute.name,
              children: [
                _i88.RouteConfig(
                  UtilityRoute.name,
                  path: '',
                  parent: UtilityRouter.name,
                ),
                _i88.RouteConfig(
                  PlaceTabRoute.name,
                  path: 'place',
                  parent: UtilityRouter.name,
                  children: [
                    _i88.RouteConfig(
                      BotPlacesRoute.name,
                      path: 'bot',
                      parent: PlaceTabRoute.name,
                    ),
                    _i88.RouteConfig(
                      ViettelPlacesRoute.name,
                      path: 'viettel',
                      parent: PlaceTabRoute.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  SmsRegisterRoute.name,
                  path: 'sms-register',
                  parent: UtilityRouter.name,
                ),
                _i88.RouteConfig(
                  InvoiceSettingRoute.name,
                  path: 'invoice-setting',
                  parent: UtilityRouter.name,
                ),
                _i88.RouteConfig(
                  CoolPenaltyRoute.name,
                  path: 'cool-penalty',
                  parent: UtilityRouter.name,
                ),
                _i88.RouteConfig(
                  ParkingBlocProviderRoute.name,
                  path: 'parking',
                  parent: UtilityRouter.name,
                  children: [
                    _i88.RouteConfig(
                      ParkingSettingRoute.name,
                      path: '',
                      parent: ParkingBlocProviderRoute.name,
                    )
                  ],
                ),
                _i88.RouteConfig(
                  ParkingBlocProviderRoadSideRoute.name,
                  path: 'parking-roadSide',
                  parent: UtilityRouter.name,
                  children: [
                    _i88.RouteConfig(
                      ParkingRoadSideRoute.name,
                      path: '',
                      parent: ParkingBlocProviderRoadSideRoute.name,
                    )
                  ],
                ),
                _i88.RouteConfig(
                  VietMapRoute.name,
                  path: 'vietmap',
                  parent: UtilityRouter.name,
                ),
                _i88.RouteConfig(
                  ReferralsClientRoute.name,
                  path: 'referrals-client',
                  parent: UtilityRouter.name,
                ),
              ],
            ),
            _i88.RouteConfig(
              NotificationRouter.name,
              path: 'notification',
              parent: HomeTabsRoute.name,
              children: [
                _i88.RouteConfig(
                  EventRoute.name,
                  path: 'event',
                  parent: NotificationRouter.name,
                ),
                _i88.RouteConfig(
                  WaitForConfirmationRoute.name,
                  path: 'wait-for-confirmation',
                  parent: NotificationRouter.name,
                ),
                _i88.RouteConfig(
                  BalanceChangeRoute.name,
                  path: 'balance-change',
                  parent: NotificationRouter.name,
                ),
                _i88.RouteConfig(
                  NewsRoute.name,
                  path: 'news',
                  parent: NotificationRouter.name,
                ),
                _i88.RouteConfig(
                  DiscountRoute.name,
                  path: 'discount',
                  parent: NotificationRouter.name,
                ),
              ],
            ),
            _i88.RouteConfig(
              SettingRouter.name,
              path: 'setting',
              parent: HomeTabsRoute.name,
              children: [
                _i88.RouteConfig(
                  VehicleRouter.name,
                  path: 'vehicles',
                  parent: SettingRouter.name,
                  children: [
                    _i88.RouteConfig(
                      VehicleListRoute.name,
                      path: '',
                      parent: VehicleRouter.name,
                    ),
                    _i88.RouteConfig(
                      VehicleDetailsRoute.name,
                      path: ':id',
                      parent: VehicleRouter.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  CashInRouter.name,
                  path: 'cash-in',
                  parent: SettingRouter.name,
                  children: [
                    _i88.RouteConfig(
                      CashInRoute.name,
                      path: '',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInFeeRoute.name,
                      path: 'fee',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInBankTransferRoute.name,
                      path: 'bank-transfer',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInConfirmRoute.name,
                      path: 'confirm',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInConfirmOTPRoute.name,
                      path: 'check-otp-bidv',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInResultRoute.name,
                      path: 'result',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInViettelPayGateRoute.name,
                      path: 'viettel-pay-gate',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInBIDVPayGateRoute.name,
                      path: 'bidv-pay-gate',
                      parent: CashInRouter.name,
                    ),
                    _i88.RouteConfig(
                      CashInVnptPayGateRoute.name,
                      path: 'vnpt-pay-gate',
                      parent: CashInRouter.name,
                    ),
                  ],
                ),
                _i88.RouteConfig(
                  SettingRoute.name,
                  path: '',
                  parent: SettingRouter.name,
                ),
                _i88.RouteConfig(
                  ProfileRoute.name,
                  path: 'profile',
                  parent: SettingRouter.name,
                ),
                _i88.RouteConfig(
                  NotificationSettingRoute.name,
                  path: 'notification-setting',
                  parent: SettingRouter.name,
                ),
                _i88.RouteConfig(
                  AliasRoute.name,
                  path: 'alias',
                  parent: SettingRouter.name,
                ),
                _i88.RouteConfig(
                  ChangePasswordRoute.name,
                  path: 'change-password',
                  parent: SettingRouter.name,
                ),
                _i88.RouteConfig(
                  AppInfoRoute.name,
                  path: 'app-info',
                  parent: SettingRouter.name,
                ),
              ],
            ),
          ],
        ),
        _i88.RouteConfig(
          SmsRegisterRoute.name,
          path: 'sms-register',
        ),
      ];
}

/// generated route for
/// [_i1.OnboardingPage]
class OnboardingRoute extends _i88.PageRouteInfo<void> {
  const OnboardingRoute()
      : super(
          OnboardingRoute.name,
          path: '/onboarding',
        );

  static const String name = 'OnboardingRoute';
}

/// generated route for
/// [_i2.LoginPage]
class LoginRoute extends _i88.PageRouteInfo<void> {
  const LoginRoute()
      : super(
          LoginRoute.name,
          path: '/login',
        );

  static const String name = 'LoginRoute';
}

/// generated route for
/// [_i3.ForgotPasswordBlocProviderPage]
class ForgotPasswordRouter extends _i88.PageRouteInfo<void> {
  const ForgotPasswordRouter({List<_i88.PageRouteInfo>? children})
      : super(
          ForgotPasswordRouter.name,
          path: '/forgot-password',
          initialChildren: children,
        );

  static const String name = 'ForgotPasswordRouter';
}

/// generated route for
/// [_i4.RegisterBlocProviderPage]
class RegisterRouter extends _i88.PageRouteInfo<void> {
  const RegisterRouter({List<_i88.PageRouteInfo>? children})
      : super(
          RegisterRouter.name,
          path: '/register',
          initialChildren: children,
        );

  static const String name = 'RegisterRouter';
}

/// generated route for
/// [_i5.HomeTabsPage]
class HomeTabsRoute extends _i88.PageRouteInfo<void> {
  const HomeTabsRoute({List<_i88.PageRouteInfo>? children})
      : super(
          HomeTabsRoute.name,
          path: '/home',
          initialChildren: children,
        );

  static const String name = 'HomeTabsRoute';
}

/// generated route for
/// [_i6.SmsRegisterPage]
class SmsRegisterRoute extends _i88.PageRouteInfo<void> {
  const SmsRegisterRoute()
      : super(
          SmsRegisterRoute.name,
          path: 'sms-register',
        );

  static const String name = 'SmsRegisterRoute';
}

/// generated route for
/// [_i7.ForgotPasswordPage]
class ForgotPasswordRoute extends _i88.PageRouteInfo<void> {
  const ForgotPasswordRoute()
      : super(
          ForgotPasswordRoute.name,
          path: '',
        );

  static const String name = 'ForgotPasswordRoute';
}

/// generated route for
/// [_i8.ForgotPasswordNewPasswordPage]
class ForgotPasswordNewPasswordRoute extends _i88.PageRouteInfo<void> {
  const ForgotPasswordNewPasswordRoute()
      : super(
          ForgotPasswordNewPasswordRoute.name,
          path: 'new-password',
        );

  static const String name = 'ForgotPasswordNewPasswordRoute';
}

/// generated route for
/// [_i9.ForgotPasswordOTPPage]
class ForgotPasswordOTPRoute extends _i88.PageRouteInfo<void> {
  const ForgotPasswordOTPRoute()
      : super(
          ForgotPasswordOTPRoute.name,
          path: 'otp',
        );

  static const String name = 'ForgotPasswordOTPRoute';
}

/// generated route for
/// [_i10.RegisterVehiclesPage]
class RegisterVehiclesRoute extends _i88.PageRouteInfo<void> {
  const RegisterVehiclesRoute()
      : super(
          RegisterVehiclesRoute.name,
          path: '',
        );

  static const String name = 'RegisterVehiclesRoute';
}

/// generated route for
/// [_i11.RegisterPaperTypePage]
class RegisterPaperTypeRoute extends _i88.PageRouteInfo<void> {
  const RegisterPaperTypeRoute()
      : super(
          RegisterPaperTypeRoute.name,
          path: 'verify',
        );

  static const String name = 'RegisterPaperTypeRoute';
}

/// generated route for
/// [_i12.RegisterPaperScanPage]
class RegisterPaperScanRoute
    extends _i88.PageRouteInfo<RegisterPaperScanRouteArgs> {
  RegisterPaperScanRoute({
    _i91.Key? key,
    required bool isFront,
  }) : super(
          RegisterPaperScanRoute.name,
          path: 'scan',
          args: RegisterPaperScanRouteArgs(
            key: key,
            isFront: isFront,
          ),
        );

  static const String name = 'RegisterPaperScanRoute';
}

class RegisterPaperScanRouteArgs {
  const RegisterPaperScanRouteArgs({
    this.key,
    required this.isFront,
  });

  final _i91.Key? key;

  final bool isFront;

  @override
  String toString() {
    return 'RegisterPaperScanRouteArgs{key: $key, isFront: $isFront}';
  }
}

/// generated route for
/// [_i13.RegisterAddressPage]
class RegisterAddressRoute extends _i88.PageRouteInfo<void> {
  const RegisterAddressRoute()
      : super(
          RegisterAddressRoute.name,
          path: 'address',
        );

  static const String name = 'RegisterAddressRoute';
}

/// generated route for
/// [_i14.RegisterConfirmPage]
class RegisterConfirmRoute extends _i88.PageRouteInfo<void> {
  const RegisterConfirmRoute()
      : super(
          RegisterConfirmRoute.name,
          path: 'confirm',
        );

  static const String name = 'RegisterConfirmRoute';
}

/// generated route for
/// [_i15.RegisterServicePage]
class RegisterServiceRoute extends _i88.PageRouteInfo<void> {
  const RegisterServiceRoute()
      : super(
          RegisterServiceRoute.name,
          path: 'service',
        );

  static const String name = 'RegisterServiceRoute';
}

/// generated route for
/// [_i16.RegisterOTPPage]
class RegisterOTPRoute extends _i88.PageRouteInfo<void> {
  const RegisterOTPRoute()
      : super(
          RegisterOTPRoute.name,
          path: 'otp',
        );

  static const String name = 'RegisterOTPRoute';
}

/// generated route for
/// [_i17.EmptyRouterPage]
class HomeRouter extends _i88.PageRouteInfo<void> {
  const HomeRouter({List<_i88.PageRouteInfo>? children})
      : super(
          HomeRouter.name,
          path: '',
          initialChildren: children,
        );

  static const String name = 'HomeRouter';
}

/// generated route for
/// [_i17.EmptyRouterPage]
class UtilityRouter extends _i88.PageRouteInfo<void> {
  const UtilityRouter({List<_i88.PageRouteInfo>? children})
      : super(
          UtilityRouter.name,
          path: 'utility',
          initialChildren: children,
        );

  static const String name = 'UtilityRouter';
}

/// generated route for
/// [_i18.NotificationTabPage]
class NotificationRouter extends _i88.PageRouteInfo<void> {
  const NotificationRouter({List<_i88.PageRouteInfo>? children})
      : super(
          NotificationRouter.name,
          path: 'notification',
          initialChildren: children,
        );

  static const String name = 'NotificationRouter';
}

/// generated route for
/// [_i17.EmptyRouterPage]
class SettingRouter extends _i88.PageRouteInfo<void> {
  const SettingRouter({List<_i88.PageRouteInfo>? children})
      : super(
          SettingRouter.name,
          path: 'setting',
          initialChildren: children,
        );

  static const String name = 'SettingRouter';
}

/// generated route for
/// [_i19.HomePage]
class HomeRoute extends _i88.PageRouteInfo<void> {
  const HomeRoute()
      : super(
          HomeRoute.name,
          path: '',
        );

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i20.CarServicePage]
class CarServiceRoute extends _i88.PageRouteInfo<void> {
  const CarServiceRoute()
      : super(
          CarServiceRoute.name,
          path: 'carServices',
        );

  static const String name = 'CarServiceRoute';
}

/// generated route for
/// [_i21.WebViewPage]
class WebViewRoute extends _i88.PageRouteInfo<WebViewRouteArgs> {
  WebViewRoute({
    _i91.Key? key,
    required String title,
    required String initUrl,
  }) : super(
          WebViewRoute.name,
          path: 'webview',
          args: WebViewRouteArgs(
            key: key,
            title: title,
            initUrl: initUrl,
          ),
        );

  static const String name = 'WebViewRoute';
}

class WebViewRouteArgs {
  const WebViewRouteArgs({
    this.key,
    required this.title,
    required this.initUrl,
  });

  final _i91.Key? key;

  final String title;

  final String initUrl;

  @override
  String toString() {
    return 'WebViewRouteArgs{key: $key, title: $title, initUrl: $initUrl}';
  }
}

/// generated route for
/// [_i22.CashInBlocProviderPage]
class CashInRouter extends _i88.PageRouteInfo<CashInRouterArgs> {
  CashInRouter({
    _i91.Key? key,
    _i92.Beneficiary? beneficary,
    List<_i88.PageRouteInfo>? children,
  }) : super(
          CashInRouter.name,
          path: 'cash-in',
          args: CashInRouterArgs(
            key: key,
            beneficary: beneficary,
          ),
          initialChildren: children,
        );

  static const String name = 'CashInRouter';
}

class CashInRouterArgs {
  const CashInRouterArgs({
    this.key,
    this.beneficary,
  });

  final _i91.Key? key;

  final _i92.Beneficiary? beneficary;

  @override
  String toString() {
    return 'CashInRouterArgs{key: $key, beneficary: $beneficary}';
  }
}

/// generated route for
/// [_i23.BuyTicketBlocProviderPage]
class BuyTicketRouter extends _i88.PageRouteInfo<BuyTicketRouterArgs> {
  BuyTicketRouter({
    _i91.Key? key,
    List<_i93.Vehicle>? userVehicle,
    List<_i88.PageRouteInfo>? children,
  }) : super(
          BuyTicketRouter.name,
          path: 'buy-ticket',
          args: BuyTicketRouterArgs(
            key: key,
            userVehicle: userVehicle,
          ),
          initialChildren: children,
        );

  static const String name = 'BuyTicketRouter';
}

class BuyTicketRouterArgs {
  const BuyTicketRouterArgs({
    this.key,
    this.userVehicle,
  });

  final _i91.Key? key;

  final List<_i93.Vehicle>? userVehicle;

  @override
  String toString() {
    return 'BuyTicketRouterArgs{key: $key, userVehicle: $userVehicle}';
  }
}

/// generated route for
/// [_i24.InvoicePage]
class InvoiceRoute extends _i88.PageRouteInfo<void> {
  const InvoiceRoute()
      : super(
          InvoiceRoute.name,
          path: 'invoice',
        );

  static const String name = 'InvoiceRoute';
}

/// generated route for
/// [_i25.TransactionHistoryTabPage]
class TransactionHistoryRouter extends _i88.PageRouteInfo<void> {
  const TransactionHistoryRouter({List<_i88.PageRouteInfo>? children})
      : super(
          TransactionHistoryRouter.name,
          path: 'transaction-history',
          initialChildren: children,
        );

  static const String name = 'TransactionHistoryRouter';
}

/// generated route for
/// [_i26.VehicleBlocProviderPage]
class VehicleRouter extends _i88.PageRouteInfo<void> {
  const VehicleRouter({List<_i88.PageRouteInfo>? children})
      : super(
          VehicleRouter.name,
          path: 'vehicles',
          initialChildren: children,
        );

  static const String name = 'VehicleRouter';
}

/// generated route for
/// [_i27.BeneficiaryBlocProviderPage]
class BenficaryRouter extends _i88.PageRouteInfo<void> {
  const BenficaryRouter({List<_i88.PageRouteInfo>? children})
      : super(
          BenficaryRouter.name,
          path: 'beneficary',
          initialChildren: children,
        );

  static const String name = 'BenficaryRouter';
}

/// generated route for
/// [_i17.EmptyRouterPage]
class InsuranceRouter extends _i88.PageRouteInfo<void> {
  const InsuranceRouter({List<_i88.PageRouteInfo>? children})
      : super(
          InsuranceRouter.name,
          path: 'insurance',
          initialChildren: children,
        );

  static const String name = 'InsuranceRouter';
}

/// generated route for
/// [_i28.AliasPage]
class AliasDialogRoute extends _i88.PageRouteInfo<void> {
  const AliasDialogRoute()
      : super(
          AliasDialogRoute.name,
          path: 'alias-dialog',
        );

  static const String name = 'AliasDialogRoute';
}

/// generated route for
/// [_i17.EmptyRouterPage]
class SupportRouter extends _i88.PageRouteInfo<void> {
  const SupportRouter({List<_i88.PageRouteInfo>? children})
      : super(
          SupportRouter.name,
          path: 'support',
          initialChildren: children,
        );

  static const String name = 'SupportRouter';
}

/// generated route for
/// [_i29.CashInPage]
class CashInRoute extends _i88.PageRouteInfo<void> {
  const CashInRoute()
      : super(
          CashInRoute.name,
          path: '',
        );

  static const String name = 'CashInRoute';
}

/// generated route for
/// [_i30.CashInFeePage]
class CashInFeeRoute extends _i88.PageRouteInfo<void> {
  const CashInFeeRoute()
      : super(
          CashInFeeRoute.name,
          path: 'fee',
        );

  static const String name = 'CashInFeeRoute';
}

/// generated route for
/// [_i31.CashInBankTransferPage]
class CashInBankTransferRoute extends _i88.PageRouteInfo<void> {
  const CashInBankTransferRoute()
      : super(
          CashInBankTransferRoute.name,
          path: 'bank-transfer',
        );

  static const String name = 'CashInBankTransferRoute';
}

/// generated route for
/// [_i32.CashInConfirmPage]
class CashInConfirmRoute extends _i88.PageRouteInfo<void> {
  const CashInConfirmRoute()
      : super(
          CashInConfirmRoute.name,
          path: 'confirm',
        );

  static const String name = 'CashInConfirmRoute';
}

/// generated route for
/// [_i33.CashInConfirmOTPPage]
class CashInConfirmOTPRoute extends _i88.PageRouteInfo<void> {
  const CashInConfirmOTPRoute()
      : super(
          CashInConfirmOTPRoute.name,
          path: 'check-otp-bidv',
        );

  static const String name = 'CashInConfirmOTPRoute';
}

/// generated route for
/// [_i34.CashInResultPage]
class CashInResultRoute extends _i88.PageRouteInfo<CashInResultRouteArgs> {
  CashInResultRoute({
    _i91.Key? key,
    bool isSuccess = true,
    String? message,
  }) : super(
          CashInResultRoute.name,
          path: 'result',
          args: CashInResultRouteArgs(
            key: key,
            isSuccess: isSuccess,
            message: message,
          ),
        );

  static const String name = 'CashInResultRoute';
}

class CashInResultRouteArgs {
  const CashInResultRouteArgs({
    this.key,
    this.isSuccess = true,
    this.message,
  });

  final _i91.Key? key;

  final bool isSuccess;

  final String? message;

  @override
  String toString() {
    return 'CashInResultRouteArgs{key: $key, isSuccess: $isSuccess, message: $message}';
  }
}

/// generated route for
/// [_i35.CashInViettelPayGatePage]
class CashInViettelPayGateRoute
    extends _i88.PageRouteInfo<CashInViettelPayGateRouteArgs> {
  CashInViettelPayGateRoute({
    _i91.Key? key,
    required String title,
    required _i94.ViettelPayGateRequest request,
  }) : super(
          CashInViettelPayGateRoute.name,
          path: 'viettel-pay-gate',
          args: CashInViettelPayGateRouteArgs(
            key: key,
            title: title,
            request: request,
          ),
        );

  static const String name = 'CashInViettelPayGateRoute';
}

class CashInViettelPayGateRouteArgs {
  const CashInViettelPayGateRouteArgs({
    this.key,
    required this.title,
    required this.request,
  });

  final _i91.Key? key;

  final String title;

  final _i94.ViettelPayGateRequest request;

  @override
  String toString() {
    return 'CashInViettelPayGateRouteArgs{key: $key, title: $title, request: $request}';
  }
}

/// generated route for
/// [_i36.CashInBIDVPayGatePage]
class CashInBIDVPayGateRoute
    extends _i88.PageRouteInfo<CashInBIDVPayGateRouteArgs> {
  CashInBIDVPayGateRoute({
    _i91.Key? key,
    required String title,
    required _i95.CheckLinkBIDVPayGateRequest request,
  }) : super(
          CashInBIDVPayGateRoute.name,
          path: 'bidv-pay-gate',
          args: CashInBIDVPayGateRouteArgs(
            key: key,
            title: title,
            request: request,
          ),
        );

  static const String name = 'CashInBIDVPayGateRoute';
}

class CashInBIDVPayGateRouteArgs {
  const CashInBIDVPayGateRouteArgs({
    this.key,
    required this.title,
    required this.request,
  });

  final _i91.Key? key;

  final String title;

  final _i95.CheckLinkBIDVPayGateRequest request;

  @override
  String toString() {
    return 'CashInBIDVPayGateRouteArgs{key: $key, title: $title, request: $request}';
  }
}

/// generated route for
/// [_i37.CashInVnptPayGatePage]
class CashInVnptPayGateRoute
    extends _i88.PageRouteInfo<CashInVnptPayGateRouteArgs> {
  CashInVnptPayGateRoute({
    _i91.Key? key,
    required String title,
    required _i96.VNPTRequest request,
    required _i97.VnptPaymentSuccess state,
    required Function callback,
  }) : super(
          CashInVnptPayGateRoute.name,
          path: 'vnpt-pay-gate',
          args: CashInVnptPayGateRouteArgs(
            key: key,
            title: title,
            request: request,
            state: state,
            callback: callback,
          ),
        );

  static const String name = 'CashInVnptPayGateRoute';
}

class CashInVnptPayGateRouteArgs {
  const CashInVnptPayGateRouteArgs({
    this.key,
    required this.title,
    required this.request,
    required this.state,
    required this.callback,
  });

  final _i91.Key? key;

  final String title;

  final _i96.VNPTRequest request;

  final _i97.VnptPaymentSuccess state;

  final Function callback;

  @override
  String toString() {
    return 'CashInVnptPayGateRouteArgs{key: $key, title: $title, request: $request, state: $state, callback: $callback}';
  }
}

/// generated route for
/// [_i38.BuyTicketPage]
class BuyTicketRoute extends _i88.PageRouteInfo<void> {
  const BuyTicketRoute()
      : super(
          BuyTicketRoute.name,
          path: '',
        );

  static const String name = 'BuyTicketRoute';
}

/// generated route for
/// [_i39.VehicleSelectTabPage]
class VehicleSelectTabRoute extends _i88.PageRouteInfo<void> {
  const VehicleSelectTabRoute({List<_i88.PageRouteInfo>? children})
      : super(
          VehicleSelectTabRoute.name,
          path: 'vehicle',
          initialChildren: children,
        );

  static const String name = 'VehicleSelectTabRoute';
}

/// generated route for
/// [_i40.BuyTicketConfirmPage]
class BuyTicketConfirmRoute extends _i88.PageRouteInfo<void> {
  const BuyTicketConfirmRoute()
      : super(
          BuyTicketConfirmRoute.name,
          path: 'confirm',
        );

  static const String name = 'BuyTicketConfirmRoute';
}

/// generated route for
/// [_i17.EmptyRouterPage]
class BuyTicketResultRouter extends _i88.PageRouteInfo<void> {
  const BuyTicketResultRouter({List<_i88.PageRouteInfo>? children})
      : super(
          BuyTicketResultRouter.name,
          path: 'result',
          initialChildren: children,
        );

  static const String name = 'BuyTicketResultRouter';
}

/// generated route for
/// [_i41.VehicleUserPage]
class VehicleUserRoute extends _i88.PageRouteInfo<VehicleUserRouteArgs> {
  VehicleUserRoute({
    _i91.Key? key,
    void Function()? onChanged,
  }) : super(
          VehicleUserRoute.name,
          path: 'user',
          args: VehicleUserRouteArgs(
            key: key,
            onChanged: onChanged,
          ),
        );

  static const String name = 'VehicleUserRoute';
}

class VehicleUserRouteArgs {
  const VehicleUserRouteArgs({
    this.key,
    this.onChanged,
  });

  final _i91.Key? key;

  final void Function()? onChanged;

  @override
  String toString() {
    return 'VehicleUserRouteArgs{key: $key, onChanged: $onChanged}';
  }
}

/// generated route for
/// [_i42.VehicleOtherPage]
class VehicleOtherRoute extends _i88.PageRouteInfo<void> {
  const VehicleOtherRoute()
      : super(
          VehicleOtherRoute.name,
          path: 'other',
        );

  static const String name = 'VehicleOtherRoute';
}

/// generated route for
/// [_i43.BuyTicketResultPage]
class BuyTicketResultRoute extends _i88.PageRouteInfo<void> {
  const BuyTicketResultRoute()
      : super(
          BuyTicketResultRoute.name,
          path: '',
        );

  static const String name = 'BuyTicketResultRoute';
}

/// generated route for
/// [_i44.BuyTicketResultDetailPage]
class BuyTicketResultDetailRoute extends _i88.PageRouteInfo<void> {
  const BuyTicketResultDetailRoute()
      : super(
          BuyTicketResultDetailRoute.name,
          path: 'details',
        );

  static const String name = 'BuyTicketResultDetailRoute';
}

/// generated route for
/// [_i45.TransactionTicketPurchasePage]
class TransactionTicketPurchaseRoute
    extends _i88.PageRouteInfo<TransactionTicketPurchaseRouteArgs> {
  TransactionTicketPurchaseRoute({
    _i91.Key? key,
    String? plateNumber,
  }) : super(
          TransactionTicketPurchaseRoute.name,
          path: 'transaction-ticket-purchase',
          args: TransactionTicketPurchaseRouteArgs(
            key: key,
            plateNumber: plateNumber,
          ),
        );

  static const String name = 'TransactionTicketPurchaseRoute';
}

class TransactionTicketPurchaseRouteArgs {
  const TransactionTicketPurchaseRouteArgs({
    this.key,
    this.plateNumber,
  });

  final _i91.Key? key;

  final String? plateNumber;

  @override
  String toString() {
    return 'TransactionTicketPurchaseRouteArgs{key: $key, plateNumber: $plateNumber}';
  }
}

/// generated route for
/// [_i46.TransactionVehiclePage]
class TransactionVehicleRoute
    extends _i88.PageRouteInfo<TransactionVehicleRouteArgs> {
  TransactionVehicleRoute({
    _i91.Key? key,
    String? plateNumber,
  }) : super(
          TransactionVehicleRoute.name,
          path: 'transaction-vehicle',
          args: TransactionVehicleRouteArgs(
            key: key,
            plateNumber: plateNumber,
          ),
        );

  static const String name = 'TransactionVehicleRoute';
}

class TransactionVehicleRouteArgs {
  const TransactionVehicleRouteArgs({
    this.key,
    this.plateNumber,
  });

  final _i91.Key? key;

  final String? plateNumber;

  @override
  String toString() {
    return 'TransactionVehicleRouteArgs{key: $key, plateNumber: $plateNumber}';
  }
}

/// generated route for
/// [_i47.TransactionParkingPage]
class TransactionParkingRoute
    extends _i88.PageRouteInfo<TransactionParkingRouteArgs> {
  TransactionParkingRoute({
    _i91.Key? key,
    String? plateNumber,
  }) : super(
          TransactionParkingRoute.name,
          path: 'transaction-parking',
          args: TransactionParkingRouteArgs(
            key: key,
            plateNumber: plateNumber,
          ),
        );

  static const String name = 'TransactionParkingRoute';
}

class TransactionParkingRouteArgs {
  const TransactionParkingRouteArgs({
    this.key,
    this.plateNumber,
  });

  final _i91.Key? key;

  final String? plateNumber;

  @override
  String toString() {
    return 'TransactionParkingRouteArgs{key: $key, plateNumber: $plateNumber}';
  }
}

/// generated route for
/// [_i48.TransactionTopupPage]
class TransactionTopupRoute extends _i88.PageRouteInfo<void> {
  const TransactionTopupRoute()
      : super(
          TransactionTopupRoute.name,
          path: 'transaction-topup',
        );

  static const String name = 'TransactionTopupRoute';
}

/// generated route for
/// [_i49.ContractActionPage]
class ContractActionRoute extends _i88.PageRouteInfo<void> {
  const ContractActionRoute()
      : super(
          ContractActionRoute.name,
          path: 'contract-action',
        );

  static const String name = 'ContractActionRoute';
}

/// generated route for
/// [_i50.TransactionOtherPage]
class TransactionOtherRoute extends _i88.PageRouteInfo<void> {
  const TransactionOtherRoute()
      : super(
          TransactionOtherRoute.name,
          path: 'transaction-other',
        );

  static const String name = 'TransactionOtherRoute';
}

/// generated route for
/// [_i51.VehicleListPage]
class VehicleListRoute extends _i88.PageRouteInfo<void> {
  const VehicleListRoute()
      : super(
          VehicleListRoute.name,
          path: '',
        );

  static const String name = 'VehicleListRoute';
}

/// generated route for
/// [_i52.VehicleDetailsPage]
class VehicleDetailsRoute extends _i88.PageRouteInfo<VehicleDetailsRouteArgs> {
  VehicleDetailsRoute({
    _i91.Key? key,
    required String vehicleId,
    required String contractId,
    int? fee,
  }) : super(
          VehicleDetailsRoute.name,
          path: ':id',
          args: VehicleDetailsRouteArgs(
            key: key,
            vehicleId: vehicleId,
            contractId: contractId,
            fee: fee,
          ),
          rawPathParams: {'id': vehicleId},
        );

  static const String name = 'VehicleDetailsRoute';
}

class VehicleDetailsRouteArgs {
  const VehicleDetailsRouteArgs({
    this.key,
    required this.vehicleId,
    required this.contractId,
    this.fee,
  });

  final _i91.Key? key;

  final String vehicleId;

  final String contractId;

  final int? fee;

  @override
  String toString() {
    return 'VehicleDetailsRouteArgs{key: $key, vehicleId: $vehicleId, contractId: $contractId, fee: $fee}';
  }
}

/// generated route for
/// [_i53.BeneficiaryListPage]
class BeneficiaryListRoute extends _i88.PageRouteInfo<void> {
  const BeneficiaryListRoute()
      : super(
          BeneficiaryListRoute.name,
          path: '',
        );

  static const String name = 'BeneficiaryListRoute';
}

/// generated route for
/// [_i54.BeneficiaryDetailsPage]
class BeneficiaryDetailsRoute
    extends _i88.PageRouteInfo<BeneficiaryDetailsRouteArgs> {
  BeneficiaryDetailsRoute({
    _i91.Key? key,
    required String beneficiaryId,
  }) : super(
          BeneficiaryDetailsRoute.name,
          path: ':id',
          args: BeneficiaryDetailsRouteArgs(
            key: key,
            beneficiaryId: beneficiaryId,
          ),
          rawPathParams: {'id': beneficiaryId},
        );

  static const String name = 'BeneficiaryDetailsRoute';
}

class BeneficiaryDetailsRouteArgs {
  const BeneficiaryDetailsRouteArgs({
    this.key,
    required this.beneficiaryId,
  });

  final _i91.Key? key;

  final String beneficiaryId;

  @override
  String toString() {
    return 'BeneficiaryDetailsRouteArgs{key: $key, beneficiaryId: $beneficiaryId}';
  }
}

/// generated route for
/// [_i55.InsurancePage]
class InsuranceRoute extends _i88.PageRouteInfo<void> {
  const InsuranceRoute()
      : super(
          InsuranceRoute.name,
          path: '',
        );

  static const String name = 'InsuranceRoute';
}

/// generated route for
/// [_i56.InsuranceViettelPayPage]
class InsuranceViettelPayRoute extends _i88.PageRouteInfo<void> {
  const InsuranceViettelPayRoute()
      : super(
          InsuranceViettelPayRoute.name,
          path: 'vds',
        );

  static const String name = 'InsuranceViettelPayRoute';
}

/// generated route for
/// [_i57.InsuranceAnBinhPage]
class InsuranceAnBinhRoute extends _i88.PageRouteInfo<void> {
  const InsuranceAnBinhRoute()
      : super(
          InsuranceAnBinhRoute.name,
          path: 'aba',
        );

  static const String name = 'InsuranceAnBinhRoute';
}

/// generated route for
/// [_i58.InsuranceGCPage]
class InsuranceGCRoute extends _i88.PageRouteInfo<InsuranceGCRouteArgs> {
  InsuranceGCRoute({
    _i91.Key? key,
    String? contractNoEncrypted,
  }) : super(
          InsuranceGCRoute.name,
          path: 'gc',
          args: InsuranceGCRouteArgs(
            key: key,
            contractNoEncrypted: contractNoEncrypted,
          ),
        );

  static const String name = 'InsuranceGCRoute';
}

class InsuranceGCRouteArgs {
  const InsuranceGCRouteArgs({
    this.key,
    this.contractNoEncrypted,
  });

  final _i91.Key? key;

  final String? contractNoEncrypted;

  @override
  String toString() {
    return 'InsuranceGCRouteArgs{key: $key, contractNoEncrypted: $contractNoEncrypted}';
  }
}

/// generated route for
/// [_i59.SupportPage]
class SupportRoute extends _i88.PageRouteInfo<void> {
  const SupportRoute()
      : super(
          SupportRoute.name,
          path: '',
        );

  static const String name = 'SupportRoute';
}

/// generated route for
/// [_i60.FeedbackTabPage]
class FeedbackTabRoute extends _i88.PageRouteInfo<void> {
  const FeedbackTabRoute({List<_i88.PageRouteInfo>? children})
      : super(
          FeedbackTabRoute.name,
          path: 'feedback',
          initialChildren: children,
        );

  static const String name = 'FeedbackTabRoute';
}

/// generated route for
/// [_i61.ReportTabPage]
class ReportTabRoute extends _i88.PageRouteInfo<void> {
  const ReportTabRoute({List<_i88.PageRouteInfo>? children})
      : super(
          ReportTabRoute.name,
          path: 'report',
          initialChildren: children,
        );

  static const String name = 'ReportTabRoute';
}

/// generated route for
/// [_i62.CreateFeedbackPage]
class CreateFeedbackRoute extends _i88.PageRouteInfo<void> {
  const CreateFeedbackRoute()
      : super(
          CreateFeedbackRoute.name,
          path: 'create',
        );

  static const String name = 'CreateFeedbackRoute';
}

/// generated route for
/// [_i63.ListFeedbackPage]
class ListFeedbackRoute extends _i88.PageRouteInfo<void> {
  const ListFeedbackRoute()
      : super(
          ListFeedbackRoute.name,
          path: 'list',
        );

  static const String name = 'ListFeedbackRoute';
}

/// generated route for
/// [_i64.CreateReportPage]
class CreateReportRoute extends _i88.PageRouteInfo<void> {
  const CreateReportRoute()
      : super(
          CreateReportRoute.name,
          path: 'create',
        );

  static const String name = 'CreateReportRoute';
}

/// generated route for
/// [_i65.ListReportPage]
class ListReportRoute extends _i88.PageRouteInfo<void> {
  const ListReportRoute()
      : super(
          ListReportRoute.name,
          path: 'list',
        );

  static const String name = 'ListReportRoute';
}

/// generated route for
/// [_i66.UtilityPage]
class UtilityRoute extends _i88.PageRouteInfo<void> {
  const UtilityRoute()
      : super(
          UtilityRoute.name,
          path: '',
        );

  static const String name = 'UtilityRoute';
}

/// generated route for
/// [_i67.PlaceTabPage]
class PlaceTabRoute extends _i88.PageRouteInfo<void> {
  const PlaceTabRoute({List<_i88.PageRouteInfo>? children})
      : super(
          PlaceTabRoute.name,
          path: 'place',
          initialChildren: children,
        );

  static const String name = 'PlaceTabRoute';
}

/// generated route for
/// [_i68.InvoiceSettingPage]
class InvoiceSettingRoute extends _i88.PageRouteInfo<void> {
  const InvoiceSettingRoute()
      : super(
          InvoiceSettingRoute.name,
          path: 'invoice-setting',
        );

  static const String name = 'InvoiceSettingRoute';
}

/// generated route for
/// [_i69.CoolPenaltyPage]
class CoolPenaltyRoute extends _i88.PageRouteInfo<void> {
  const CoolPenaltyRoute()
      : super(
          CoolPenaltyRoute.name,
          path: 'cool-penalty',
        );

  static const String name = 'CoolPenaltyRoute';
}

/// generated route for
/// [_i70.ParkingBlocProviderPage]
class ParkingBlocProviderRoute extends _i88.PageRouteInfo<void> {
  const ParkingBlocProviderRoute({List<_i88.PageRouteInfo>? children})
      : super(
          ParkingBlocProviderRoute.name,
          path: 'parking',
          initialChildren: children,
        );

  static const String name = 'ParkingBlocProviderRoute';
}

/// generated route for
/// [_i71.ParkingBlocProviderRoadSidePage]
class ParkingBlocProviderRoadSideRoute extends _i88.PageRouteInfo<void> {
  const ParkingBlocProviderRoadSideRoute({List<_i88.PageRouteInfo>? children})
      : super(
          ParkingBlocProviderRoadSideRoute.name,
          path: 'parking-roadSide',
          initialChildren: children,
        );

  static const String name = 'ParkingBlocProviderRoadSideRoute';
}

/// generated route for
/// [_i72.VietMapPage]
class VietMapRoute extends _i88.PageRouteInfo<void> {
  const VietMapRoute()
      : super(
          VietMapRoute.name,
          path: 'vietmap',
        );

  static const String name = 'VietMapRoute';
}

/// generated route for
/// [_i73.ReferralsClientPage]
class ReferralsClientRoute extends _i88.PageRouteInfo<void> {
  const ReferralsClientRoute()
      : super(
          ReferralsClientRoute.name,
          path: 'referrals-client',
        );

  static const String name = 'ReferralsClientRoute';
}

/// generated route for
/// [_i74.BotPlacesPage]
class BotPlacesRoute extends _i88.PageRouteInfo<void> {
  const BotPlacesRoute()
      : super(
          BotPlacesRoute.name,
          path: 'bot',
        );

  static const String name = 'BotPlacesRoute';
}

/// generated route for
/// [_i75.ViettelPlacesPage]
class ViettelPlacesRoute extends _i88.PageRouteInfo<void> {
  const ViettelPlacesRoute()
      : super(
          ViettelPlacesRoute.name,
          path: 'viettel',
        );

  static const String name = 'ViettelPlacesRoute';
}

/// generated route for
/// [_i76.ParkingSettingPage]
class ParkingSettingRoute extends _i88.PageRouteInfo<void> {
  const ParkingSettingRoute()
      : super(
          ParkingSettingRoute.name,
          path: '',
        );

  static const String name = 'ParkingSettingRoute';
}

/// generated route for
/// [_i77.ParkingRoadSidePage]
class ParkingRoadSideRoute extends _i88.PageRouteInfo<void> {
  const ParkingRoadSideRoute()
      : super(
          ParkingRoadSideRoute.name,
          path: '',
        );

  static const String name = 'ParkingRoadSideRoute';
}

/// generated route for
/// [_i78.EventPage]
class EventRoute extends _i88.PageRouteInfo<void> {
  const EventRoute()
      : super(
          EventRoute.name,
          path: 'event',
        );

  static const String name = 'EventRoute';
}

/// generated route for
/// [_i79.WaitForConfirmationPage]
class WaitForConfirmationRoute extends _i88.PageRouteInfo<void> {
  const WaitForConfirmationRoute()
      : super(
          WaitForConfirmationRoute.name,
          path: 'wait-for-confirmation',
        );

  static const String name = 'WaitForConfirmationRoute';
}

/// generated route for
/// [_i80.BalanceChangePage]
class BalanceChangeRoute extends _i88.PageRouteInfo<void> {
  const BalanceChangeRoute()
      : super(
          BalanceChangeRoute.name,
          path: 'balance-change',
        );

  static const String name = 'BalanceChangeRoute';
}

/// generated route for
/// [_i81.NewsPage]
class NewsRoute extends _i88.PageRouteInfo<void> {
  const NewsRoute()
      : super(
          NewsRoute.name,
          path: 'news',
        );

  static const String name = 'NewsRoute';
}

/// generated route for
/// [_i82.DiscountPage]
class DiscountRoute extends _i88.PageRouteInfo<void> {
  const DiscountRoute()
      : super(
          DiscountRoute.name,
          path: 'discount',
        );

  static const String name = 'DiscountRoute';
}

/// generated route for
/// [_i83.SettingPage]
class SettingRoute extends _i88.PageRouteInfo<void> {
  const SettingRoute()
      : super(
          SettingRoute.name,
          path: '',
        );

  static const String name = 'SettingRoute';
}

/// generated route for
/// [_i84.ProfilePage]
class ProfileRoute extends _i88.PageRouteInfo<void> {
  const ProfileRoute()
      : super(
          ProfileRoute.name,
          path: 'profile',
        );

  static const String name = 'ProfileRoute';
}

/// generated route for
/// [_i85.NotificationSettingPage]
class NotificationSettingRoute extends _i88.PageRouteInfo<void> {
  const NotificationSettingRoute()
      : super(
          NotificationSettingRoute.name,
          path: 'notification-setting',
        );

  static const String name = 'NotificationSettingRoute';
}

/// generated route for
/// [_i28.AliasPage]
class AliasRoute extends _i88.PageRouteInfo<void> {
  const AliasRoute()
      : super(
          AliasRoute.name,
          path: 'alias',
        );

  static const String name = 'AliasRoute';
}

/// generated route for
/// [_i86.ChangePasswordPage]
class ChangePasswordRoute extends _i88.PageRouteInfo<void> {
  const ChangePasswordRoute()
      : super(
          ChangePasswordRoute.name,
          path: 'change-password',
        );

  static const String name = 'ChangePasswordRoute';
}

/// generated route for
/// [_i87.AppInfoPage]
class AppInfoRoute extends _i88.PageRouteInfo<void> {
  const AppInfoRoute()
      : super(
          AppInfoRoute.name,
          path: 'app-info',
        );

  static const String name = 'AppInfoRoute';
}
