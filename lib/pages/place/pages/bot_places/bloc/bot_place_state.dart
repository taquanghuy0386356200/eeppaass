part of 'bot_place_bloc.dart';

class BotPlaceState extends Equatable {
  final List<StationStage> listData;
  final List<StationStage> filteredListData;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;

  const BotPlaceState({
    required this.listData,
    required this.filteredListData,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
  });

  @override
  List<Object?> get props =>
      [listData, filteredListData, error, isLoading, isRefreshing];

  BotPlaceState copyWith({
    List<StationStage>? listData,
    List<StationStage>? filteredListData,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
  }) {
    return BotPlaceState(
      listData: listData ?? this.listData,
      filteredListData: filteredListData ?? this.filteredListData,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
    );
  }
}
