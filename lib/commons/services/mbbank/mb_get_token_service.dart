import 'dart:convert';
import 'package:epass/commons/models/mb_bank/mb_get_token.dart';
import 'package:epass/flavors.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class MBGetTokenService {
  Future<MBGetTokenResponse?> getToken(MBGetTokenRequest request) async {
    try {
      /// url auth token
      String url = F.mbGetTokenUrl;

      var username = F.mbUsername;

      var password = F.mbPassword;

      var headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':
            'Basic ${base64Encode(utf8.encode('$username:$password'))}',
      };

      var body = {
        'grant_type': '${request.grant_type}',
      };

      var response =
          await http.post(Uri.parse(url), headers: headers, body: body);

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        return MBGetTokenResponse.fromJson(data);
      } else {
        return null;
      }
    } on Exception {
      return null;
    }
  }
}
