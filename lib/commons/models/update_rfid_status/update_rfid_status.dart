/// reasonId : "38"
/// custId : "custId"
/// actTypeId : 20

class UpdateRfidStatus {
  const UpdateRfidStatus({
    this.reasonId,
    this.custId,
    this.actTypeId,
  });

  final String? reasonId;
  final String? custId;
  final int? actTypeId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['reasonId'] = reasonId;
    map['custId'] = custId;
    map['actTypeId'] = actTypeId;
    return map;
  }
}
