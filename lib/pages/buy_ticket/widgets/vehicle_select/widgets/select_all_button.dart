import 'package:epass/commons/widgets/primary_checkbox.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SelectAllButton extends StatelessWidget {
  final bool isCheck;
  final VoidCallback? onTap;

  const SelectAllButton({
    Key? key,
    this.isCheck = false,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Row(
        children: [
          const Spacer(),
          Padding(
            padding: EdgeInsets.only(right: 6.w),
            child: Material(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.0),
              child: InkWell(
                borderRadius: BorderRadius.circular(8.0),
                onTap: onTap,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(width: 12.w),
                    Text(
                      'Chọn tất cả',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    AbsorbPointer(
                      child: PrimaryCheckbox(
                        isCheck: isCheck,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
