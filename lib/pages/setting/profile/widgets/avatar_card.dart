import 'package:epass/commons/widgets/avatar_widget.dart';
import 'package:epass/commons/widgets/modal/image_picker/image_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/image_picker/image_picker_modal.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/profile/avatar_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loader_overlay/loader_overlay.dart';

class AvatarCard extends StatelessWidget {
  const AvatarCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final avatarSize = 150.r;

    return BlocProvider(
      create: (context) => ImagePickerBloc(imagePicker: getIt<ImagePicker>()),
      child: MultiBlocListener(
        listeners: [
          BlocListener<ImagePickerBloc, ImagePickerState>(
            listener: (context, state) {
              if (state is ImagePickedInProgress) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
                if (state is ImagePickedSuccess) {
                  context
                      .read<AvatarBloc>()
                      .add(AvatarUploaded(filePath: state.filePath));
                } else if (state is ImagePickedFailure) {
                  context.loaderOverlay.hide();
                  showErrorSnackBBar(context: context, message: state.message);
                }
              }
            },
          ),
          BlocListener<AvatarBloc, AvatarState>(
            listener: (context, state) {
              if (state.isLoading) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
              }
            },
          ),
        ],
        child: Builder(
          builder: (context) {
            return ClipRRect(
              borderRadius: BorderRadius.circular(avatarSize / 2),
              child: Stack(
                children: [
                  AvatarWidget(
                    height: avatarSize,
                    width: avatarSize,
                  ),
                  Positioned(
                    bottom: 0.0,
                    child: Container(
                      height: 36.h,
                      width: avatarSize,
                      color: Colors.black54,
                      child: Center(
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                          size: 20.r,
                        ),
                      ),
                    ),
                  ),
                  Positioned.fill(
                    child: Material(
                      borderRadius: BorderRadius.circular(avatarSize / 2),
                      color: Colors.transparent,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(avatarSize / 2),
                        onTap: () async => await showImagePickerModal(context),
                        child: const SizedBox(),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
