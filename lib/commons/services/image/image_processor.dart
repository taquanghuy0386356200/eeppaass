import 'dart:io';

import 'package:epass/commons/api/failures/failures.dart';
import 'package:image/image.dart' as img;
import 'package:simple_result/simple_result.dart';

class ImageProcessor {
  // Work for portrait mode only
  static Future<Result<String, Failure>> cropImage({
    required String srcPath,
    required String destPath,
    required double screenWidth,
    required double screenHeight,
    double ratio = 1,
    double offsetX = 0,
    double offsetY = 0,
  }) async {
    final bytes = await File(srcPath).readAsBytes();
    final src = img.decodeImage(bytes);
    if (src == null) {
      return const Result.failure(UnknownFailure());
    }

    final imageWidth = src.width;

    final screenRatio = screenHeight / screenWidth;
    final imageHeight = screenRatio * imageWidth;

    final screenImageWidthRatio = screenWidth / imageWidth;
    final screenImageHeightRatio = screenHeight / imageHeight;
    final imageOffsetX = offsetX / screenImageWidthRatio;
    final imageOffsetY = offsetY / screenImageHeightRatio;

    final croppedWidth = imageWidth * .9;
    final croppedHeight = croppedWidth / ratio;
    // final trueOffsetY = offsetY * heightRatio;
    // final trueOffsetX = offsetX * widthRatio;

    final centerX = imageWidth / 2;
    final x = (centerX - croppedWidth / 2 + imageOffsetX).round();
    final centerY = imageHeight / 2;
    final y = (centerY - croppedHeight / 2 + imageOffsetY).round();

    final dest = img.copyCrop(src, x, y, croppedWidth.floor(), croppedHeight.floor());
    final jpg = img.encodeJpg(dest);
    final file =  await File(destPath).writeAsBytes(jpg);
    return Result.success(file.path);
  }
}
