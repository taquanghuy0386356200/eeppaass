import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/bidv/bidv_check_otp.dart';
import 'package:epass/commons/models/cash_in_order/cash_in_order.dart';
import 'package:epass/commons/models/confirm_momo_payment/confirm_momo_payment.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:epass/commons/models/contracts_info/contracts_info.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/models/topup_channel/topup_channel_fee.dart';
import 'package:epass/commons/models/topup_fee/topup_fee.dart';
import 'package:epass/commons/models/bidv/bidv_create_order.dart';
import 'package:epass/commons/services/bidv/bidv_check_otp_request.dart';
import 'package:epass/commons/services/bidv/bidv_top_up_request.dart';
import 'package:epass/commons/models/update_fee/topup_fee_update.dart';
import 'package:simple_result/simple_result.dart';

import '../models/zalo_pay/confirm_contracts_payment.dart';

abstract class ICashInRepository {
  Future<Result<List<TopupChannel>, Failure>> getTopupChannel();

  Future<Result<List<ContractsInfo>, Failure>> getContractsInfo({
    String? inputSearch,
    int status = 2,
  });

  Future<Result<List<TopupFee>, Failure>> getTopupFee();

  Future<Result<List<TopupFeeDataUpdate>, Failure>> getTopupFeeUpdate();

  Future<Result<TopupChannelFee, Failure>> getTopupChannelFee({
    required String topupChannelCode,
    required int amount,
  });

  Future<Result<String, Failure>> createCashInMomoOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  });

  Future<Result<ConfirmMomoPaymentData, Failure>> confirmMomoPayment({
    required String partnerCode,
    required String orderId,
    required int amount,
    required String phoneNumber,
    required String token,
  });

  Future<Result<String, Failure>> createCashInViettelPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
    required String paymentMethodName,
  });

  Future<Result<String, Failure>> createCashInVNPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  });

  Future<Result<ContractTopupLastChargeResponse, Failure>> getLastCharge();

  Future<Result<ConfirmContractsPaymentResponse, Failure>> topupEWallet({
    required String destContractId,
    required String requestId,
    required int amount,
    required String topupChannel,
  });

  Future<Result<BidvCreateOrderResponse, Failure>> bidvCreateOrder(
      {required String payerAdd,
      required String payerId,
      required String payerIdentity,
      required String payerMobile,
      required String payerName,
      required int transAmount});

  Future<Result<BidvCreateOrderResponse, Failure>> bidvCheckOTP(
      {required String payerAdd,
      required String transId,
      required String payerId,
      required String payerIdentity,
      required String payerMobile,
      required String payerName,
      required int transAmount,
      required String otpNumber});

  Future<Result<ContractTopupTypeResponse, Failure>> checkTypeCust({
    required String contractId,
    required int amount,
  });

  Future<Result<VehicleInContractResponse, Failure>> checkVehicleInContract({
    required String contractId,
  });
}

class CashInRepository extends ICashInRepository {
  final CrmClient _crmClient;

  CashInRepository({required CrmClient crmClient}) : _crmClient = crmClient;

  @override
  Future<Result<List<TopupChannel>, Failure>> getTopupChannel() async {
    try {
      final response = await _crmClient.getTopupChannel();
      final topupChannels = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          topupChannels == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(topupChannels);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<ContractsInfo>, Failure>> getContractsInfo({
    String? inputSearch,
    int status = 2,
  }) async {
    try {
      final response = await _crmClient.getContractsInfo(
        inputSearch: inputSearch,
        status: status,
      );
      final contractsInfo = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          contractsInfo == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(contractsInfo);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String, Failure>> createCashInMomoOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  }) async {
    try {
      final request = CashInOrderRequest(
        amount: amount,
        contractNo: contractNo,
        receiverContractId: receiverContractId,
        quantity: quantity,
      );

      final response = await _crmClient.createCashInOrderMomo(
        contractId: contractId,
        customerId: customerId,
        request: request,
      );
      final orderId = response.order?.billingCode;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || orderId == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(orderId.toString());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<TopupFee>, Failure>> getTopupFee() async {
    try {
      final response = await _crmClient.getTopupFee();
      final topupFees = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || topupFees == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(topupFees);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<TopupFeeDataUpdate>, Failure>> getTopupFeeUpdate() async {
    try {
      final response = await _crmClient.getTopupFeeUpdate();
      final topupFees = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || topupFees == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(topupFees);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }


  @override
  Future<Result<TopupChannelFee, Failure>> getTopupChannelFee({
    required String topupChannelCode,
    required int amount,
  }) async {
    try {
      final response = await _crmClient.getTopupChannelFee(
        topupChannelCode: topupChannelCode,
        amount: amount,
      );
      final topupFee = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || topupFee == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(topupFee);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ConfirmMomoPaymentData, Failure>> confirmMomoPayment({
    required String partnerCode,
    required String orderId,
    required int amount,
    required String phoneNumber,
    required String token,
  }) async {
    try {
      final request = ConfirmMomoPaymentRequest(
        partnerCode: partnerCode,
        partnerRefId: orderId,
        amount: amount,
        customerNumber: phoneNumber,
        appData: token,
      );

      final response = await _crmClient.confirmMomoPayment(
        request: request,
      );
      final confirmMomoPaymentData = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          confirmMomoPaymentData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(confirmMomoPaymentData);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String, Failure>> createCashInViettelPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
    required String paymentMethodName,
  }) async {
    try {
      final request = CashInOrderRequest(
        amount: amount,
        contractNo: contractNo,
        receiverContractId: receiverContractId,
        quantity: quantity,
        paymentMethodName: paymentMethodName,
      );

      final response = await _crmClient.createCashInOrderViettelPay(
        contractId: contractId,
        customerId: customerId,
        request: request,
      );
      final orderId = response.order?.billingCode;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || orderId == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(orderId.toString());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String, Failure>> createCashInVNPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  }) async {
    try {
      final request = CashInOrderRequest(
        amount: amount,
        contractNo: contractNo,
        receiverContractId: receiverContractId,
        quantity: quantity,
      );

      final response = await _crmClient.createCashInOrderVNPay(
        contractId: contractId,
        customerId: customerId,
        request: request,
      );
      final orderId = response.order?.billingCode;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || orderId == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(orderId.toString());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ContractTopupLastChargeResponse, Failure>>
      getLastCharge() async {
    try {
      final response = await _crmClient.getLastCharge();
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ConfirmContractsPaymentResponse, Failure>> topupEWallet({
    required String destContractId,
    required String requestId,
    required int amount,
    required String topupChannel,
  }) async {
    try {
      final request = ConfirmContractsPaymentRequest(
          amount: amount,
          destContractId: destContractId,
          requestId: requestId,
          topupChannel: topupChannel);

      final response = await _crmClient.topupEWallet(
        request: request,
      );
      if (response.mess!.code != 1) {
        return const Result.failure(
            ResponseFailure(message: "Có lỗi xảy ra! Vui lòng thử lại."));
      } else {
        return Result.success(response);
      }
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ContractTopupTypeResponse, Failure>> checkTypeCust(
      {required String contractId, required int amount}) async {
    try {
      final reponse = await _crmClient.checkTypeCust(
          amount: amount, contractId: contractId);
      return Result.success(reponse);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  //dumv
  @override
  Future<Result<BidvCreateOrderResponse, Failure>> bidvCreateOrder(
      {required String payerAdd,
      required String payerId,
      required String payerIdentity,
      required String payerMobile,
      required String payerName,
      required int transAmount}) async {
    try {
      final request = BIDVTopUpPayGateServiceRequest(
        payerAdd: payerAdd,
        payerId: payerId,
        payerIdentity: payerIdentity,
        payerMobile: payerMobile,
        payerName: payerName,
        transAmount: transAmount,
      );
      request.getBidvPayInfo();
      final response = await _crmClient.topupBIDV(
        wrapperXml: request.wrapperXml,
        dataEncode: request.dataEncode,
        callType: 1,
      );
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<BidvCreateOrderResponse, Failure>> bidvCheckOTP(
      {required String payerAdd,
      required String transId,
      required String payerId,
      required String payerIdentity,
      required String payerMobile,
      required String payerName,
      required int transAmount,
      required String otpNumber}) async {
    try {
      final request = BidvCheckOTPRequest(
        payerAdd: payerAdd,
        transId: transId,
        payerId: payerId,
        payerIdentity: payerIdentity,
        payerMobile: payerMobile,
        payerName: payerName,
        transAmount: transAmount,
        otpNumber: otpNumber,
      );

      request.getBidvOtpInfo();
      final response = await _crmClient.topupBIDV(
        wrapperXml: request.wrapperXml,
        dataEncode: request.dataEncode,
        callType: 0,
      );
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<VehicleInContractResponse, Failure>> checkVehicleInContract(
      {required String contractId}) async {
    try {
      final response = await _crmClient.checkVehicleInContract(
        contractId: contractId,
      );
      final data = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || data == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
