import 'package:epass/commons/models/user/user.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GenderAvatar extends StatelessWidget {
  final Gender gender;
  final double? height;
  final double? width;

  const GenderAvatar({
    Key? key,
    required this.gender,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white70,
        shape: BoxShape.circle,
      ),
      child: Builder(
        builder: (context) {
          switch (gender) {
            case Gender.male:
              return Assets.images.home.avatarMale.svg(
                height: height ?? 48.r,
                width: width ?? 48.r,
                color: ColorName.primaryColor,
              );
            case Gender.female:
              return Assets.images.home.avatarFemale.svg(
                height: height ?? 48.r,
                width: width ?? 48.r,
                color: ColorName.primaryColor,
              );
            case Gender.unknown:
              return Assets.images.home.avatarNinja.svg(
                height: height ?? 48.r,
                width: width ?? 48.r,
                color: ColorName.primaryColor,
              );
          }
        },
      ),
    );
  }
}
