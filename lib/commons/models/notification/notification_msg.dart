import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// notificationMsgId : 7508
/// notificationCode : "DK_TK"
/// notificationName : "Thông báo mở tài khoản Epass"
/// toContractId : 1002809
/// toContractNo : "V001002809"
/// toPlateTypeCode : "1"
/// toPlateNumber : "09A09876"
/// toUserId : "V001002809"
/// message : "Quy khach da dang ky thanh cong tai khoan ePass so V001002809, mat khau R1!319v1. Truy cap https://epass-vdtc.com.vn/tai-ung-dung-cpt de cai dat ung dung va doi mat khau. Chi tiet LH 19009080. Tran trong!"
/// orgPushApp : "crm"
/// pushDate : "30/12/2021 17:41:16"
/// viewStatus : 0

part 'notification_msg.g.dart';

@JsonEnum(alwaysCreate: true)
enum ViewStatus {
  @JsonValue(1)
  read,
  @JsonValue(0)
  unread,
  unknown,
}

@JsonSerializable()
class NotificationMsg {
  NotificationMsg({
    this.notificationMsgId,
    this.notificationCode,
    this.notificationName,
    this.toContractId,
    this.toContractNo,
    this.toPlateTypeCode,
    this.toPlateNumber,
    this.toUserId,
    this.message,
    this.orgPushApp,
    this.pushDate,
    this.viewStatus,
    this.uri,
  });

  final int? notificationMsgId;
  final String? notificationCode;
  final String? notificationName;
  final int? toContractId;
  final String? toContractNo;
  final String? toPlateTypeCode;
  final String? toPlateNumber;
  final String? toUserId;
  final String? message;
  final String? orgPushApp;
  final String? pushDate;
  final String? uri;

  @JsonKey(unknownEnumValue: ViewStatus.unknown)
  final ViewStatus? viewStatus;

  factory NotificationMsg.fromJson(Map<String, dynamic> json) =>
      _$NotificationMsgFromJson(json);

  @override
  String toString() {
    return 'Notification $notificationMsgId';
  }

  NotificationMsg copyWith({
    int? notificationMsgId,
    String? notificationCode,
    String? notificationName,
    int? toContractId,
    String? toContractNo,
    String? toPlateTypeCode,
    String? toPlateNumber,
    String? toUserId,
    String? message,
    String? orgPushApp,
    String? pushDate,
    ViewStatus? viewStatus,
    String? uri,
  }) {
    return NotificationMsg(
      notificationMsgId: notificationMsgId ?? this.notificationMsgId,
      notificationCode: notificationCode ?? this.notificationCode,
      notificationName: notificationName ?? this.notificationName,
      toContractId: toContractId ?? this.toContractId,
      toContractNo: toContractNo ?? this.toContractNo,
      toPlateTypeCode: toPlateTypeCode ?? this.toPlateTypeCode,
      toPlateNumber: toPlateNumber ?? this.toPlateNumber,
      toUserId: toUserId ?? this.toUserId,
      message: message ?? this.message,
      orgPushApp: orgPushApp ?? this.orgPushApp,
      pushDate: pushDate ?? this.pushDate,
      viewStatus: viewStatus ?? this.viewStatus,
      uri: uri ?? this.uri,
    );
  }
}

@JsonSerializable()
class NotificationMsgData {
  final List<NotificationMsg>? listData;
  final int? count;

  const NotificationMsgData({
    this.listData,
    this.count,
  });

  factory NotificationMsgData.fromJson(Map<String, dynamic> json) =>
      _$NotificationMsgDataFromJson(json);
}

class NotificationMsgDataNotNull {
  final List<NotificationMsg> listData;
  final int count;
  final int totalUnread;

  const NotificationMsgDataNotNull({
    required this.listData,
    required this.count,
    required this.totalUnread,
  });
}

@JsonSerializable()
class NotificationMsgResponse {
  final NotificationMsgData? data;
  final MessageData? mess;
  final int? totalUnread;

  NotificationMsgResponse({
    this.data,
    this.mess,
    this.totalUnread,
  });

  factory NotificationMsgResponse.fromJson(Map<String, dynamic> json) =>
      _$NotificationMsgResponseFromJson(json);
}

@JsonSerializable()
class UpdateNotificationViewStatusRequest {
  final int viewStatus;

  UpdateNotificationViewStatusRequest({
    this.viewStatus = 1,
  });

  Map<String, dynamic> toJson() =>
      _$UpdateNotificationViewStatusRequestToJson(this);
}
