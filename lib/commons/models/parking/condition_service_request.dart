
import 'package:json_annotation/json_annotation.dart';

part 'condition_service_request.g.dart';

@JsonSerializable(explicitToJson: true)
class ConditionServiceRequest {
  bool isRegisterService;
  int serviceId;

  ConditionServiceRequest({
    this.isRegisterService = true,
    required this.serviceId
  });

  Map<String, dynamic> toJson() => _$ConditionServiceRequestToJson(this);

}