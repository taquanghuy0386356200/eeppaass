import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/api/endpoint.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/services/bidv/bidv_link_request.dart';
import 'package:epass/commons/services/vnpt/vnpay_payment_request.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/bloc/vnpt_gate_bloc.dart';
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_result_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CashInVnptPayGatePage extends StatefulWidget {
  final String title;
  final VNPTRequest request;
  final VnptPaymentSuccess state;
  final Function callback;

  const CashInVnptPayGatePage({
    Key? key,
    required this.title,
    required this.request,
    required this.state,
    required this.callback,
  }) : super(key: key);

  @override
  State<CashInVnptPayGatePage> createState() => _CashInVnptPayGatePageState();
}

class _CashInVnptPayGatePageState extends State<CashInVnptPayGatePage> {
  int _progress = 0;
  String? surl = '';
  String contractId = '';
  int? amount = 0;
  late WebViewController _controller;

  @override
  void initState() {
    surl = widget.state.redirectUrl;
    contractId = widget.request.contractId;
    amount = widget.request.amount;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: _progress == 100
            ? BackButton(onPressed: context.popRoute)
            : const SizedBox.shrink(),
      ),
      body: MultiBlocListener(
        listeners: [
          BlocListener<CashInBloc, CashInState>(
              listener: (context, state) async {
            if (state is CashInContractsPaymentSuccess) {
              context.loaderOverlay.hide();
              context.read<BalanceBloc>().add(BalanceFetched());
              context.replaceRoute(CashInResultRoute(
                isSuccess: true,
              ));
            } else if (state is CashInContractsPaymentFailure) {
              context.loaderOverlay.hide();
              context.replaceRoute(CashInResultRoute(
                isSuccess: false,
                message: "Thanh toán thất bại",
              ));
            }
          }),
          BlocListener<VnptGateBloc, VnptGateState>(listener: (context, state) {
            if (state is VnptPaymentSuccessOk) {
              //dumv xu ly sau
              context.loaderOverlay.show();
              context.read<CashInBloc>().add(CashInContractsPayment(
                    topupChannel: 'VnptMoney',
                    destContractId: contractId,
                    requestId: state.transactionId,
                    amount: amount!,
                  ));
            } else if (state is VnptPaymentFailure) {
              context.loaderOverlay.hide();
              context.replaceRoute(CashInResultRoute(
                isSuccess: false,
                message: state.message,
              ));
            }
          }),
        ],
        child: WebView(
          initialUrl: surl,
          javascriptMode: JavascriptMode.unrestricted,
          navigationDelegate: (navigation) {
            final uri = Uri.parse(navigation.url);

            if (navigation.url.indexOf("wallet_return") > 0) {
              final responseCode = uri.queryParameters['responseCode'];
              final transactionId = uri.queryParameters['transactionId'];
              final secureCode = uri.queryParameters['secureCode'];
              if (responseCode != null &&
                  transactionId != null &&
                  secureCode != null) {
                context.read<VnptGateBloc>().add(VnptPaymentsSuccess(
                    transactionId: transactionId,
                    responseCode: responseCode,
                    secureCode: secureCode));
              }
            }
            return NavigationDecision.navigate;
          },
          onWebViewCreated: (WebViewController webViewController) {
            _controller = webViewController;
          },
          onProgress: (progress) {
            setState(() {
              _progress = progress;
            });
          },
        ),
      ),
    );
  }
}
