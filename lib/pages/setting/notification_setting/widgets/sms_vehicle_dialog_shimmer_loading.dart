import 'package:epass/commons/widgets/animations/shimmer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SMSVehicleDialogShimmerLoading extends StatelessWidget {
  const SMSVehicleDialogShimmerLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShimmerColor(
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) => ShimmerWidget(
          height: 28.h,
          width: double.infinity,
        ),
        separatorBuilder: (context, index) => SizedBox(height: 10.h),
        itemCount: 3,
      ),
    );
  }
}
