import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/list_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher_string.dart';

class SupportPage extends StatelessWidget {
  const SupportPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final listButtons = [
      // ListButton(
      //   onTap: () {},
      //   leading: Assets.icons.help.svg(),
      //   title: 'Hướng dẫn sử dụng'
      // ),
      ListButton(
          onTap: () => context.pushRoute(const FeedbackTabRoute()),
          leading: Assets.icons.feedback.svg(),
          title: 'Góp ý'
      ),
      ListButton(
        onTap: () async {
          await launchUrlString('tel:${Constant.hotline}');
        },
        leading: Assets.icons.phone.svg(),
        title: 'Hotline',
      ),
      ListButton(
        onTap: () async {
          final canLaunchPage = await canLaunchUrlString('https://facebook.com/${Constant.facebookPageName}');

          if (canLaunchPage) {
            final url = 'https://facebook.com/${Constant.facebookPageName}';
            await launchUrlString(url,
                mode: LaunchMode.externalApplication);
          }
        },
        leading: Assets.icons.facebookRounded.svg(),
        title: 'Fanpage',
      ),
      ListButton(
        onTap: () async {
          await launchUrlString(
            'https://m.me/${Constant.facebookPageName}?ref=.f.606bdb5d5b047800122a9e5f',
            mode: LaunchMode.externalApplication,
          );
        },
        leading: Assets.icons.messenger.svg(),
        title: 'Messenger',
      ),
      ListButton(
        onTap: () => context.pushRoute(const ReportTabRoute()),
        leading: Assets.icons.report.svg(),
        title: 'Báo lỗi',
      )
    ];

    return BasePage(
      title: 'Trung tâm trợ giúp',
      leading: BackButton(onPressed: context.popRoute),
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: listButtons.length,
                    itemBuilder: (context, index) => listButtons[index],
                    separatorBuilder: (BuildContext context, int index) => SizedBox(height: 20.h),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
