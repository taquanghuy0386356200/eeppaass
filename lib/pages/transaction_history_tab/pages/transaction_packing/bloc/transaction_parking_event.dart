part of 'transaction_parking_bloc.dart';

@immutable
abstract class TransactionParkingEvent extends Equatable {
  const TransactionParkingEvent();
}

class TransactionParkingFetched extends TransactionParkingEvent {
  const TransactionParkingFetched();

  @override
  List<Object?> get props => [];
}

class TransactionParkingRefreshed
    extends TransactionParkingEvent {
  const TransactionParkingRefreshed();

  @override
  List<Object?> get props => [];
}

class TransactionParkingLoadMore extends TransactionParkingEvent {
  const TransactionParkingLoadMore();

  @override
  List<Object?> get props => [];
}

class TransactionParkingSearched extends TransactionParkingEvent {
  final String? plateNumber;
  final DateTime? startDate;
  final DateTime? endDate;

  const TransactionParkingSearched({
    this.plateNumber,
    this.startDate,
    this.endDate,
  });

  @override
  List<Object?> get props => [
        plateNumber,
        startDate,
        endDate,
      ];
}

class TransactionParkingDateRangeChanged
    extends TransactionParkingEvent {
  final DateTimeRange saleTransDate;

  const TransactionParkingDateRangeChanged(this.saleTransDate);

  @override
  List<Object> get props => [saleTransDate];
}

class TransactionParkingPlateNumberChanged
    extends TransactionParkingEvent {
  final String plateNumber;

  const TransactionParkingPlateNumberChanged(this.plateNumber);

  @override
  List<Object> get props => [plateNumber];
}
