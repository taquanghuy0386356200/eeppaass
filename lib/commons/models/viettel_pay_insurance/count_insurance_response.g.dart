// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'count_insurance_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountInsuranceResponse _$CountInsuranceResponseFromJson(
        Map<String, dynamic> json) =>
    CountInsuranceResponse(
      data: json['data'] as int?,
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CountInsuranceResponseToJson(
        CountInsuranceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
