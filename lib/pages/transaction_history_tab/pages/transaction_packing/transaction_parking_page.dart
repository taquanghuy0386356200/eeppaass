import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/commons/widgets/buttons/floating_primary_button.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/bloc/transaction_parking_bloc.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/widget/transaction_packing_shimmer_loading.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/widget/transaction_parking_card.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/widget/transaction_parking_search_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionParkingPage extends StatefulWidget {
  final String? plateNumber;

  const TransactionParkingPage({
    Key? key,
    this.plateNumber,
  }) : super(key: key);

  @override
  State<TransactionParkingPage> createState() => _TransactionParkingPageState();
}

class _TransactionParkingPageState extends State<TransactionParkingPage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TransactionParkingBloc(
        transRepository: getIt<ITransactionHistoryRepository>(),
        appBloc: getIt<AppBloc>(),
      )..add(widget.plateNumber == null
          ? const TransactionParkingFetched()
          : TransactionParkingSearched(
              plateNumber: widget.plateNumber,
            )),
      child: BlocConsumer<TransactionParkingBloc, TransactionParkingState>(
        listener: (context, state) {
          final error = state.error;

          if (error == null) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          } else {
            _refreshController.refreshFailed();
            _refreshController.loadFailed();
          }
        },
        builder: (context, state) {
          final error = state.error;

          if (state.isLoading || state.isRefreshing && state.listData.isEmpty) {
            return const TransactionParkingShimmerLoading();
          } else if (error != null && state.listData.isEmpty) {
            return CommonErrorPage(
              message: state.error,
              onTap: () => context
                  .read<TransactionParkingBloc>()
                  .add(const TransactionParkingFetched()),
            );
          }
          final listData = state.listData;
          return Scaffold(
            backgroundColor: Colors.transparent,
            floatingActionButton: GestureDetector(
              onTap: () => _onActionButtonTapped(
                context,
                plateNumber: state.plateNumber,
                startDate: state.startDate,
                endDate: state.endDate,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "Tìm kiếm",
                    style: Theme.of(context).textTheme.caption!.copyWith(
                        color: Colors.orange,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  const FloatingPrimaryButton(),
                ],
              ),
            ),
            body: state.listData.isEmpty
                ? NoDataPage(
                    title: context.read<TransactionParkingBloc>().isSearching
                        ? 'Bạn không có giao dịch nào phát sinh trong thời gian tìm kiếm'
                        : 'Bạn chưa có giao dịch nào phát sinh',
                    description: '',
                  )
                : SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    enablePullUp: !state.isFull,
                    onRefresh: () => context
                        .read<TransactionParkingBloc>()
                        .add(const TransactionParkingRefreshed()),
                    onLoading: () => context
                        .read<TransactionParkingBloc>()
                        .add(const TransactionParkingLoadMore()),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        final item = listData[index];
                        return Padding(
                          padding: EdgeInsets.fromLTRB(
                            16.w,
                            index == 0 ? 24.h : 0.h,
                            16.w,
                            index == listData.length - 1 ? 80.h : 0.h,
                          ),
                          child: TransactionParkingCard(item: item),
                        );
                      },
                      separatorBuilder: (context, index) =>
                          SizedBox(height: 24.h),
                    ),
                  ),
          );
        },
      ),
    );
  }

  void _onActionButtonTapped(
    BuildContext context, {
    String? plateNumber,
    DateTime? startDate,
    DateTime? endDate,
  }) async {
    await showBarModalBottomSheet(
      context: context,
      expand: false,
      useRootNavigator: true,
      builder: (modalContext) => BlocProvider.value(
        value: context.read<TransactionParkingBloc>(),
        child: TransactionParkingSearchForm(
          plateNumber: plateNumber,
          startDate: startDate,
          endDate: endDate,
        ),
      ),
    );
  }
}
