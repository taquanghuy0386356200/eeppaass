import 'package:epass/commons/models/update_fee/topup_fee_update.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'item_widget.dart';

class CashInFeeCardUpdate extends StatelessWidget {
  final TopupFeeDataUpdate? topupFee;
  final String? otherFee;

  const CashInFeeCardUpdate({Key? key, this.topupFee, this.otherFee})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(12.w, 20.h, 0.w, 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            decoration: const BoxDecoration(
                color: Colors.orange, //new Color.fromRGBO(255, 0, 0, 0.0),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                gradient: LinearGradient(
                  colors: [
                    ColorName.primaryGradientStart,
                    ColorName.primaryGradientEnd,
                  ],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  topupFee?.groupImage?.split('.').last == 'svg'
                      ? SvgPicture.network(topupFee?.groupImage ?? '',
                          width: 28.w, height: 28.h)
                      : Image.network(topupFee?.groupImage ?? '',
                          width: 28.w,
                          height: 28.h, errorBuilder: (BuildContext context,
                              Object exception, StackTrace? stackTrace) {
                          return Assets.icons.feeArrow.svg(
                            height: 28.h,
                            width: 28.w,
                          );
                        }),
                  SizedBox(
                    width: 4.w,
                  ),
                  Text(
                    topupFee?.groupName ?? '',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.bold , color: Colors.white, fontSize: 13.sp
                    ),
                  )
                ],
              ),
            ),
          ),
          topupFee?.channels != null &&
                  topupFee!.channels!.length > 1 &&
                  topupFee?.containButton == true
              ? Padding(
                  padding: EdgeInsets.fromLTRB(12.w, 24.h, 0.w, 0.h),
                  child: GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 250.w,
                          childAspectRatio: 3.5,
                          crossAxisSpacing: 2.w,
                          mainAxisSpacing: 2.h),
                      itemCount: topupFee?.channels?.length ?? 0,
                      itemBuilder: (BuildContext ctx, index) {
                        return ItemWidget(
                          icon: topupFee?.channels?[index].image,
                          serviceFeeValAdd:
                              topupFee?.channels?[index].serviceFeeValAdd,
                          serviceFeeVal:
                              topupFee?.channels?[index].serviceFeeVal,
                          containButton:
                              topupFee?.channels?[index].containButton??false,
                          url: topupFee?.channels?[index].url,
                          topupChannelName:
                              topupFee?.channels?[index].topupChannelName,
                        );
                      }),
                )
              : Padding(
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 0.w, 0.h),
                  child: ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: topupFee?.channels?.length ?? 0,
                      separatorBuilder: (context, index) => Container(
                            height: 12.h,
                          ),
                      itemBuilder: (BuildContext ctx, index) {
                        return ItemWidget(
                          icon: topupFee?.channels?[index].image,
                          serviceFeeValAdd:
                              topupFee?.channels?[index].serviceFeeValAdd,
                          serviceFeeVal:
                              topupFee?.channels?[index].serviceFeeVal,
                          containButton:
                              topupFee?.channels?[index].containButton??false,
                          topupChannelName:
                              topupFee?.channels?[index].topupChannelName,
                          url: topupFee?.channels?[index].url,
                        );
                      }),
                ),
        ],
      ),
    );
  }
}
