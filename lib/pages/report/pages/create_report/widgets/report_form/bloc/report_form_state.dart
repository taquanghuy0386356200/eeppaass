part of 'report_form_bloc.dart';

class GroupReportState extends Equatable {
  final FeedbackType? l1TicketType;
  final FeedbackType? l2TicketType;
  final FeedbackType? l3TicketType;
  int? currentLevel = 1;

  GroupReportState({
    this.l1TicketType,
    this.l2TicketType,
    this.l3TicketType,
    this.currentLevel,
  });

  @override
  List<Object?> get props =>
      [l1TicketType, l2TicketType, l3TicketType, currentLevel];

  GroupReportState copyWith({
    FeedbackType? l1TicketType,
    FeedbackType? l2TicketType,
    FeedbackType? l3TicketType,
    int? currentLevel,
  }) {
    return GroupReportState(
        l1TicketType: l1TicketType,
        l2TicketType: l2TicketType,
        l3TicketType: l3TicketType,
        currentLevel: currentLevel);
  }
}

class LocationInfo extends Equatable {
  final AddressVTT? province;
  final AddressVTT? district;
  final AddressVTT? ward;

  const LocationInfo({this.province, this.district, this.ward});

  @override
  List<Object?> get props => [province, district, ward];

  LocationInfo copyWith({
    AddressVTT? province,
    AddressVTT? district,
    AddressVTT? ward,
  }) {
    return LocationInfo(province: province, district: district, ward: ward);
  }
}

@immutable
class ReportFormState extends Equatable {
  final String reporterName;
  final String? plateNumber;
  final String? plateTypeCode;
  final String? email;
  final String? location;
  final LocationInfo locationInfo;
  final GroupReportState groupReportState;
  final String? stationId;
  final String? stageId;
  final String? stationStageName;
  final String contentReceive;
  final String? supportInfo;
  final String? ticketKind;
  final String phoneContact;
  final List<String> attachments;

  const ReportFormState({
    // Mặc định là thông tin user
    this.reporterName = 'reporterName',
    this.plateNumber,
    this.plateTypeCode,
    this.email,
    this.location,
    required this.locationInfo,
    required this.groupReportState,
    this.stationId,
    this.stageId,
    this.stationStageName,
    this.contentReceive = 'contentReceive',
    this.supportInfo,
    this.ticketKind,
    this.phoneContact = 'phone',
    this.attachments = const [],
  });

  @override
  List<Object?> get props => [
        reporterName,
        plateNumber,
        plateTypeCode,
        email,
        location,
        locationInfo,
        groupReportState,
        stationId,
        stageId,
        stationStageName,
        contentReceive,
        supportInfo,
        ticketKind,
        phoneContact,
        attachments
      ];

  ReportFormState copyWith({
    String? reporterName,
    String? plateNumber,
    String? plateTypeCode,
    String? email,
    String? location,
    LocationInfo? locationInfo,
    GroupReportState? groupReportState,
    String? stationId,
    String? stageId,
    String? stationStageName,
    String? communeName,
    String? provinceName,
    String? district,
    String? contentReceive,
    String? supportInfo,
    String? ticketKind,
    String? phoneContact,
    List<String>? attachments,
  }) {
    return ReportFormState(
      reporterName: reporterName ?? this.reporterName,
      plateNumber: plateNumber ?? this.plateNumber,
      plateTypeCode: plateTypeCode ?? this.plateTypeCode,
      email: email ?? this.email,
      location: location ?? this.location,
      locationInfo: locationInfo ?? this.locationInfo,
      groupReportState: groupReportState ?? this.groupReportState,
      stationId: stationId ?? this.stationId,
      stageId: stageId ?? this.stageId,
      stationStageName: stationStageName ?? this.stationStageName,
      contentReceive: contentReceive ?? this.contentReceive,
      supportInfo: supportInfo ?? this.supportInfo,
      ticketKind: ticketKind ?? this.ticketKind,
      phoneContact: phoneContact ?? this.phoneContact,
      attachments: attachments ?? this.attachments,
    );
  }
}
