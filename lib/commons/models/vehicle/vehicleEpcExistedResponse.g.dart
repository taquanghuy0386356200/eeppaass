// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicleEpcExistedResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleEpcExistedResponse _$VehicleEpcExistedResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleEpcExistedResponse(
      data: json['data'] as bool?,
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleEpcExistedResponseToJson(
        VehicleEpcExistedResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
