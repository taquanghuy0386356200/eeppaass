// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'viettel_money_balance.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ViettelMoneyBalance _$ViettelMoneyBalanceFromJson(Map<String, dynamic> json) =>
    ViettelMoneyBalance(
      orderId: json['orderId'] as String?,
      contractId: json['contractId'] as String?,
      msisdn: json['msisdn'] as String?,
      bankCode: json['bankCode'] as String?,
      balanceAmount: json['balanceAmount'] as int?,
    );

Map<String, dynamic> _$ViettelMoneyBalanceToJson(
        ViettelMoneyBalance instance) =>
    <String, dynamic>{
      'orderId': instance.orderId,
      'contractId': instance.contractId,
      'msisdn': instance.msisdn,
      'bankCode': instance.bankCode,
      'balanceAmount': instance.balanceAmount,
    };

ViettelMoneyBalanceResponse _$ViettelMoneyBalanceResponseFromJson(
        Map<String, dynamic> json) =>
    ViettelMoneyBalanceResponse(
      data: json['data'] == null
          ? null
          : ViettelMoneyBalance.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ViettelMoneyBalanceResponseToJson(
        ViettelMoneyBalanceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
