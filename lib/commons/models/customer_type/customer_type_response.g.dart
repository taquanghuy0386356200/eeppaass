// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_type_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerTypeModel _$CustomerTypeModelFromJson(Map<String, dynamic> json) =>
    CustomerTypeModel(
      custTypeId: json['cust_type_id'] as int?,
      code: json['code'] as String?,
      name: json['name'] as String?,
      type: json['type'] as String?,
    );

Map<String, dynamic> _$CustomerTypeModelToJson(CustomerTypeModel instance) =>
    <String, dynamic>{
      'cust_type_id': instance.custTypeId,
      'code': instance.code,
      'name': instance.name,
      'type': instance.type,
    };

CustomerTypeResponse _$CustomerTypeResponseFromJson(
        Map<String, dynamic> json) =>
    CustomerTypeResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => CustomerTypeModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CustomerTypeResponseToJson(
        CustomerTypeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
