import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/models/topup_fee/topup_fee.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/cash_in/pages/cash_in_fee/widget/cash_in_fee_corner.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/payment_methods_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TopupFeeCard extends StatelessWidget {
  final TopupFee topupFee;

  const TopupFeeCard({
    Key? key,
    required this.topupFee,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShadowCard(
      shadowOpacity: 0.3,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16.0),
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(16.w, 20.h, 16.w, 12.h),
              color: Colors.white,
              height: topupFee.valueMin != null && topupFee.valueMin != 0 ? 160.h : 100.h,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    topupFee.topupChannelName ?? '',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(color: ColorName.textGray2),
                  ),
                  SizedBox(height: 10.h),
                  topupFee.serviceFeeValAdd != null && topupFee.serviceFeeValAdd != 0
                      ? Text(
                          '${topupFee.serviceFeeValAdd ?? ''}'
                          ' + '
                          '${((topupFee.serviceFeeVal ?? 0.0) * 100).toStringAsFixed(2)}%',
                          style: Theme.of(context).textTheme.headline5!.copyWith(
                                color: ColorName.textGray1,
                                fontWeight: FontWeight.bold,
                              ),
                        )
                      : topupFee.topupChannelCode == TopupChannelCode.BankTransfer
                          ? Text(
                              'Biểu phí ngân hàng',
                              style: Theme.of(context).textTheme.headline5!.copyWith(
                                    color: ColorName.textGray1,
                                    fontWeight: FontWeight.bold,
                                  ),
                            )
                          : Text(
                              'Miễn phí',
                              style: Theme.of(context).textTheme.headline5!.copyWith(
                                    color: ColorName.textGray1,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                  const Expanded(
                    child: SizedBox.shrink(),
                  ),
                  topupFee.valueMin != null && topupFee.valueMin != 0
                      ? Text(
                          'Số tiền nạp từ',
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(color: ColorName.borderColor),
                        )
                      : const SizedBox.shrink(),
                ],
              ),
            ),
            topupFee.valueMin != null && topupFee.valueMin != 0
                ? Positioned(
                    bottom: 0,
                    right: 0,
                    child: CashInFeeCorner(
                      topupChannelCode: topupFee.topupChannelCode,
                      height: 85.h,
                    ),
                  )
                : const SizedBox.shrink(),
            Positioned(
              top: 16.h,
              right: 16.w,
              child: PaymentMethodsIcon(
                topupChannelCode: topupFee.topupChannelCode,
                height: 28.h,
              ),
            ),
            Positioned(
              bottom: 16.h,
              right: 24.w,
              child: topupFee.valueMin != null && topupFee.valueMin != 0
                  ? Text(
                      '${topupFee.valueMin}đ',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                    )
                  : const SizedBox.shrink(),
            ),
          ],
        ),
      ),
    );
  }
}
