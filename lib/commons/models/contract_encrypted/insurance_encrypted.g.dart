// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'insurance_encrypted.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractEncrypted _$ContractEncryptedFromJson(Map<String, dynamic> json) =>
    ContractEncrypted(
      contractNoEncrypted: json['contractNoEncrypted'] as String?,
      secretKey: json['secretKey'] as String?,
    );

Map<String, dynamic> _$ContractEncryptedToJson(ContractEncrypted instance) =>
    <String, dynamic>{
      'contractNoEncrypted': instance.contractNoEncrypted,
      'secretKey': instance.secretKey,
    };

ContractEncryptedResponse _$ContractEncryptedResponseFromJson(
        Map<String, dynamic> json) =>
    ContractEncryptedResponse(
      data: json['data'] == null
          ? null
          : ContractEncrypted.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContractEncryptedResponseToJson(
        ContractEncryptedResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
