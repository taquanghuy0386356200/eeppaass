// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_method_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfigMethodVerifyModel _$ConfigMethodVerifyModelFromJson(
        Map<String, dynamic> json) =>
    ConfigMethodVerifyModel(
      configId: json['configId'] as int?,
      contractId: json['contractId'] as int?,
      serviceId: json['serviceId'] as int?,
      serviceName: json['serviceName'] as String?,
      verifyType: json['verifyType'] as int?,
      status: json['status'] as int?,
      isCreatedPassCode: json['isCreatedPassCode'] as bool?,
    );

Map<String, dynamic> _$ConfigMethodVerifyModelToJson(
        ConfigMethodVerifyModel instance) =>
    <String, dynamic>{
      'configId': instance.configId,
      'contractId': instance.contractId,
      'serviceId': instance.serviceId,
      'serviceName': instance.serviceName,
      'verifyType': instance.verifyType,
      'status': instance.status,
      'isCreatedPassCode': instance.isCreatedPassCode,
    };

ConfigMethodVerifyResponse _$ConfigMethodVerifyResponseFromJson(
        Map<String, dynamic> json) =>
    ConfigMethodVerifyResponse(
      data: json['data'] == null
          ? null
          : ConfigMethodVerifyModel.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfigMethodVerifyResponseToJson(
        ConfigMethodVerifyResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
