import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// contractNoEncrypted : "+9v/dRkqJ04eALurbvJYV37t18VEZ82wOOIzcznb9kIpa567+YM="

part 'insurance_encrypted.g.dart';

@JsonSerializable()
class ContractEncrypted {
  const ContractEncrypted({
    this.contractNoEncrypted,
    this.secretKey,
  });

  final String? contractNoEncrypted;
  final String? secretKey;

  factory ContractEncrypted.fromJson(Map<String, dynamic> json) =>
      _$ContractEncryptedFromJson(json);
}

@JsonSerializable()
class ContractEncryptedResponse {
  final ContractEncrypted? data;
  final MessageData? mess;

  ContractEncryptedResponse({this.data, this.mess});

  factory ContractEncryptedResponse.fromJson(Map<String, dynamic> json) =>
      _$ContractEncryptedResponseFromJson(json);
}
