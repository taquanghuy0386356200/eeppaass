import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/models/customer_type/customer_type_response.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/widgets/textfield/cupertino_picker_field.dart';
import 'package:epass/commons/widgets/textfield/customer_type_picker.dart';
import 'package:epass/commons/widgets/textfield/date_picker_field.dart';
import 'package:epass/commons/widgets/textfield/phone_number_text_field.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/register/bloc/register_customer/register_customer_bloc.dart';
import 'package:epass/pages/register/pages/register_address/widgets/address_dropdown/address_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';

class ConfirmCustomerInfo extends StatefulWidget {
  const ConfirmCustomerInfo({Key? key}) : super(key: key);

  @override
  State<ConfirmCustomerInfo> createState() => _ConfirmCustomerInfoState();
}

class _ConfirmCustomerInfoState extends State<ConfirmCustomerInfo> {
  final _nameController = TextEditingController();
  final _phoneNumber = TextEditingController();
  final _documentNumberController = TextEditingController();
  final _placeOfIssuedController = TextEditingController();
  final _addressController = TextEditingController();

  @override
  void initState() {
    _nameController.text = context.read<RegisterCustomerBloc>().state.name ?? '';
    _phoneNumber.text = context.read<RegisterCustomerBloc>().state.phoneNumber ?? '';
    _documentNumberController.text = context.read<RegisterCustomerBloc>().state.documentNumber ?? '';
    _placeOfIssuedController.text = context.read<RegisterCustomerBloc>().state.placeOfIssue ?? '';
    _addressController.text = context.read<RegisterCustomerBloc>().state.address ?? '';
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _phoneNumber.dispose();
    _documentNumberController.dispose();
    _placeOfIssuedController.dispose();
    _addressController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Thông tin khách hàng',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: ColorName.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 18.sp,
              ),
        ),
        SizedBox(height: 20.h),

        // CUSTOMER NAME
        PrimaryTextField(
          inputAction: TextInputAction.next,
          controller: _nameController,
          labelText: 'Tên khách hàng *',
          hintText: 'Nhập Tên khách hàng',
          validator: (value) {
            if (value?.isEmpty ?? true) {
              return 'Vui lòng nhập Tên khách hàng';
            }
            return null;
          },
          onChanged: (value) {
            context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(name: value ?? ''));
          },
        ),
        SizedBox(height: 20.h),

        // CUSTOMER PHONE
        PhoneNumberTextField(
          controller: _phoneNumber,
          labelText: 'Số điện thoại *',
          onChanged: (value) {
            context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(phoneNumber: value ?? ''));
          },
        ),
        SizedBox(height: 20.h),

        // CUSTOMER TYPE
        BlocSelector<RegisterCustomerBloc, RegisterCustomerState, CustomerTypeModel?>(
          selector: (state) => state.customerType,
          builder: (context, customerType) {
            return CustomerTypePicker(
              selectedCustomerType: customerType,
              onItemChanged: (value) {
                context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(customerType: value?.value));
              },
            );
          },
        ),
        SizedBox(height: 20.h),

        // DATE OF BIRTH AND GENDER
        BlocSelector<RegisterCustomerBloc, RegisterCustomerState, DateTime?>(
          selector: (state) => state.dob,
          builder: (context, dob) {
            return DatePickerField(
              labelText: 'Ngày sinh *',
              hintText: 'Chọn Ngày sinh',
              hasClearButton: false,
              defaultDate: dob ?? Jiffy().subtract(years: 18).startOf(Units.DAY).dateTime,
              firstDate: Jiffy([1900, 1, 1]).dateTime,
              lastDate: Jiffy().subtract(years: 18).endOf(Units.DAY).dateTime,
              onChanged: (value) {
                context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(dob: value));
              },
              validator: (value) {
                if (value?.isEmpty ?? true) {
                  return 'Vui lòng chọn Ngày sinh';
                }
                return null;
              },
            );
          },
        ),
        SizedBox(height: 20.h),

        BlocSelector<RegisterCustomerBloc, RegisterCustomerState, Gender?>(
          selector: (state) => state.gender,
          builder: (context, gender) {
            return CupertinoPickerField(
              label: 'Giới tính',
              // hintText: 'Chọn giới tính',
              defaultItem: gender != null ? PickerItemWithValue(title: gender.title, value: gender) : null,
              items: Gender.values.map((e) => PickerItemWithValue(title: e.title, value: e)).toList(),
              onItemChanged: (PickerItemWithValue<Gender>? selectedItem) {
                context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(gender: selectedItem?.value));
              },
            );
          },
        ),
        SizedBox(height: 20.h),

        // ID NUMBER
        PrimaryTextField(
          controller: _documentNumberController,
          inputAction: TextInputAction.next,
          labelText: 'Số giấy tờ *',
          hintText: 'Nhập Số giấy tờ',
          validator: (value) {
            if (value?.isEmpty ?? true) {
              return 'Vui lòng nhập Số giấy tờ';
            }
            return null;
          },
          onChanged: (value) {
            context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(documentNumber: value));
          },
        ),
        SizedBox(height: 20.h),

        // PLACE OF ISSUED
        PrimaryTextField(
          controller: _placeOfIssuedController,
          labelText: 'Nơi cấp *',
          inputAction: TextInputAction.next,
          hintText: 'Nhập Nơi cấp',
          maxLines: 2,
          validator: (value) {
            if (value?.isEmpty ?? true) {
              return 'Vui lòng nhập Nơi cấp';
            }
            return null;
          },
          onChanged: (value) {
            context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(placeOfIssue: value));
          },
        ),
        SizedBox(height: 20.h),

        // DATE OF ISSUED
        BlocSelector<RegisterCustomerBloc, RegisterCustomerState, DateTime?>(
          selector: (state) => state.dateOfIssue,
          builder: (context, dateOfIssue) {
            return DatePickerField(
              labelText: 'Ngày cấp *',
              hintText: 'Chọn Ngày cấp',
              hasClearButton: false,
              firstDate: Jiffy([1900, 1, 1]).dateTime,
              lastDate: Jiffy().endOf(Units.DAY).dateTime,
              defaultDate: dateOfIssue,
              onChanged: (value) {
                context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(dateOfIssue: value));
              },
              validator: (value) {
                if (value?.isEmpty ?? true) {
                  return 'Vui lòng chọn Ngày cấp';
                }
                return null;
              },
            );
          },
        ),
        SizedBox(height: 20.h),

        // ADDRESS
        PrimaryTextField(
          controller: _addressController,
          labelText: 'Địa chỉ *',
          inputAction: TextInputAction.next,
          hintText: 'Nhập Địa chỉ',
          maxLines: 2,
          validator: (value) {
            if (value?.isEmpty ?? true) {
              return 'Vui lòng nhập địa chỉ';
            }
            return null;
          },
          onChanged: (value) {
            context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(address: value));
          },
        ),
        SizedBox(height: 20.h),

        //
        BlocSelector<RegisterCustomerBloc, RegisterCustomerState, AddressVTT?>(
          selector: (state) => state.province,
          builder: (context, province) {
            return AddressDropdown(
              dropdownType: AddressDropdownType.province,
              selectedItem: province,
              onChanged: (province) {
                context.read<RegisterCustomerBloc>().add(RegisterCustomerProvinceUpdated(province));
              },
            );
          },
        ),
        SizedBox(height: 20.h),
        BlocBuilder<RegisterCustomerBloc, RegisterCustomerState>(
          builder: (context, state) {
            return AddressDropdown(
              key: UniqueKey(),
              dropdownType: AddressDropdownType.district,
              provinceCode: state.province?.province,
              selectedItem: state.district,
              onChanged: (district) =>
                  context.read<RegisterCustomerBloc>().add(RegisterCustomerDistrictUpdated(district)),
            );
          },
        ),
        SizedBox(height: 20.h),
        BlocBuilder<RegisterCustomerBloc, RegisterCustomerState>(
          builder: (context, state) {
            return AddressDropdown(
              key: UniqueKey(),
              dropdownType: AddressDropdownType.ward,
              provinceCode: state.province?.province,
              districtCode: state.district?.district,
              selectedItem: state.ward,
              onChanged: (ward) => context.read<RegisterCustomerBloc>().add(RegisterCustomerUpdated(ward: ward)),
            );
          },
        ),
      ],
    );
  }
}
