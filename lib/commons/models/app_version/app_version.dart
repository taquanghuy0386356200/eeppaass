import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// autoUpdateVerId : 2
/// appCode : "CPT"
/// versionName : "1.1.3"
/// fromVersion : null
/// toVersion : null
/// createDate : null
/// isActived : 1
/// url : null
/// urlUpdate : null
/// deviceType : "IOS"
/// description : null
/// version : 1
/// versionType : null
/// roleIsUsed : null
/// serialNumber : null
/// moreInfo : null
/// numberDownload : 0
/// publishBy : null
/// invalid : 1

part 'app_version.g.dart';

@JsonSerializable()
class AppVersion {
  const AppVersion({
    this.appCode,
    this.versionName,
    this.deviceType,
    this.version,
    this.invalid,
  });

  final String? appCode;
  final String? versionName;
  final String? deviceType;
  final int? version;

  /// Invalid: 1 - update required, 0 - update optional
  final int? invalid;

  factory AppVersion.fromJson(Map<String, dynamic> json) =>
      _$AppVersionFromJson(json);
}

@JsonSerializable()
class AppVersionResponse {
  final AppVersion? data;
  final MessageData? mess;

  const AppVersionResponse({this.data, this.mess});

  factory AppVersionResponse.fromJson(Map<String, dynamic> json) =>
      _$AppVersionResponseFromJson(json);
}
