part of 'vnpt_gate_bloc.dart';

abstract class VnptGateState extends CashInState {
  const VnptGateState();
}

class VnptInitial extends VnptGateState {
  @override
  List<Object> get props => [];
}

class VnptPaymentInProgress extends VnptGateState {
  @override
  List<Object> get props => [];
}

class VnptPaymentSuccess extends VnptGateState {
  final String? responseCode;
  final String? description;
  final String? redirectUrl;
  final String? secureCode;

  const VnptPaymentSuccess({
    required this.responseCode,
    required this.description,
    required this.redirectUrl,
    required this.secureCode,
  });

  @override
  List<Object?> get props =>
      [responseCode, description, redirectUrl, secureCode];
}

class VnptPaymentFailure extends VnptGateState {
  final String message;

  const VnptPaymentFailure(this.message);

  @override
  List<Object> get props => [message];
}

class VnptPaymentSuccessOk extends VnptGateState {
  final String? responseCode;
  final String transactionId;
  final String? secureCode;

  const VnptPaymentSuccessOk({
    required this.responseCode,
    required this.transactionId,
    required this.secureCode,
  });

  @override
  List<Object?> get props => [responseCode, transactionId, secureCode];
}
