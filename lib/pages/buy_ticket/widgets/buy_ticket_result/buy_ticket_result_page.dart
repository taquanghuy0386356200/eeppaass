import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/buy_ticket/buy_ticket_response.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/balance_alt_widget.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/container/footer_container.dart';
import 'package:epass/commons/widgets/pages/base_result_page.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/buy_ticket_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BuyTicketResultPage extends StatelessWidget {
  const BuyTicketResultPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BuyTicketBloc, BuyTicketState>(
      builder: (context, state) {
        var isAllSuccess = false;
        var isSomeSuccess = false;
        var totalTickets = <BuyTicketResponseItem>[];
        var listSuccess = <BuyTicketResponseItem>[];
        var totalAmount = 0;

        if (state is BuyTicketConfirmedAllSuccess) {
          totalTickets = state.listSuccess;
          listSuccess = state.listSuccess;
          isAllSuccess = true;
        } else if (state is BuyTicketConfirmedSomeSuccess) {
          listSuccess = state.listSuccess;
          final listFailure = state.listFailure;
          totalTickets = [...listSuccess, ...listFailure];
          isSomeSuccess = true;
        } else if (state is BuyTicketConfirmedNoSuccess) {
          totalTickets = state.listFailure;
        }

        final ticketPriceState = context.read<TicketPriceBloc>().state;
        if (ticketPriceState is TicketPriceFetchedSuccess) {
          totalAmount = ticketPriceState.ticketPrices
              .map((e) => e.fee ?? 0)
              .reduce((value, e) => value += e);
        }

        return BaseResultPage(
          title: isAllSuccess || isSomeSuccess
              ? 'Mua vé thành công'
              : 'Mua vé thất bại',
          leading: BackButton(onPressed: context.popRoute),
          heading: isAllSuccess || isSomeSuccess
              ? 'Mua vé thành công!'
              : 'Mua vé thất bại!',
          image: isAllSuccess || isSomeSuccess
              ? Assets.images.buyTicket.buyTicketSuccess.image(
                  height: 200.r,
                  width: 200.r,
                )
              : Assets.images.buyTicket.buyTicketFailure.image(
                  height: 200.r,
                  width: 200.r,
                ),
          content: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Số tiền',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.borderColor,
                          ),
                    ),
                    const Spacer(),
                    Text(
                      totalAmount.formatCurrency(symbol: '₫'),
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ],
                ),
                SizedBox(height: 8.h),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Số vé',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.borderColor,
                          ),
                    ),
                    const Spacer(),
                    Text(
                      '${listSuccess.length}/${totalTickets.length} Vé',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: isAllSuccess
                                ? ColorName.success
                                : ColorName.error,
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ],
                ),
                SizedBox(height: 8.h),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Phương thức thanh toán',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.borderColor,
                          ),
                    ),
                    const Spacer(),
                    Text(
                      'Ví ePass',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ],
                ),
                SizedBox(height: 8.h),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Số dư hiện tại',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.borderColor,
                          ),
                    ),
                    const Spacer(),
                    BlocProvider(
                      create: (context) => BalanceBloc(
                        appBloc: getIt<AppBloc>(),
                        userRepository: getIt<IUserRepository>(),
                      )..add(BalanceFetched()),
                      child: BalanceAltWidget(
                        mainTextSize: 16.sp,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          footer: FooterContainer(
            height: 180.h,
            hasShadow: false,
            child: Column(
              children: [
                SizedBox(
                  height: 56.h,
                  width: double.infinity,
                  child: SecondaryButton(
                    title: 'Chi tiết giao dịch',
                    onTap: () =>
                        context.pushRoute(const BuyTicketResultDetailRoute()),
                  ),
                ),
                SizedBox(height: 16.h),
                SizedBox(
                  height: 56.h,
                  width: double.infinity,
                  child: PrimaryButton(
                    title: 'Về trang chủ',
                    onTap: () {
                      context
                          .read<HomeTabsBloc>()
                          .add(const HomeTabBarRequestHidden(isHidden: false));
                      context.navigateTo(const HomeRoute());
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
