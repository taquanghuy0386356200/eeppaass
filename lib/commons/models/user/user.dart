import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonEnum(alwaysCreate: true)
enum Gender {
  @JsonValue('1')
  male,
  @JsonValue('2')
  female,
  @JsonValue('unknown')
  unknown,
}

extension GenderExt on Gender {
  String get title {
    switch (this) {
      case Gender.male:
        return 'Nam';
      case Gender.female:
        return 'Nữ';
      case Gender.unknown:
        return 'Khác';
    }
  }

  String get value {
    switch (this) {
      case Gender.male:
        return '0';
      case Gender.female:
        return '1';
      case Gender.unknown:
        return '2';
    }
  }
}

@JsonSerializable()
@immutable
class User extends Equatable {
  final int? cusTypeId;
  final String? userName;
  final String? userId;
  final String? birth;
  @JsonKey(unknownEnumValue: Gender.unknown)
  final Gender? gender;
  final String? address;
  final String? identifier;
  final String? dateOfIssue;
  final String? placeOfIssue;
  final String? contractNo;
  final String? signDate;
  final String? effDate;
  final String? phone;
  final String? email;
  final String? customerId;
  final String? contractId;
  final int? documentType;
  final int? isAdditional;
  final int? isAlertMoney;
  final int? alertMoney;
  final String? smsNotification;
  final String? accountAlias;
  final String? billCycle;
  final String? billCycleMergeType;
  final String? noticeAreaName;
  final String? noticeStreet;
  final String? noticeAreaCode;
  final String? noticeEmail;
  final String? noticePhoneNumber;

  const User({
    this.cusTypeId,
    this.userName,
    this.userId,
    this.birth,
    this.gender,
    this.address,
    this.identifier,
    this.dateOfIssue,
    this.placeOfIssue,
    this.contractNo,
    this.signDate,
    this.effDate,
    this.phone,
    this.email,
    this.customerId,
    this.contractId,
    this.documentType,
    this.isAdditional,
    this.isAlertMoney,
    this.alertMoney,
    this.smsNotification,
    this.accountAlias,
    this.billCycle,
    this.billCycleMergeType,
    this.noticeAreaName,
    this.noticeStreet,
    this.noticeAreaCode,
    this.noticeEmail,
    this.noticePhoneNumber,
  });

  @override
  List<Object?> get props => [
        userId,
        customerId,
        contractId,
        contractNo,
        userName,
        phone,
        email,
        accountAlias,
        isAlertMoney,
        alertMoney,
      ];

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

@JsonSerializable()
class UserResponse {
  @JsonKey(name: 'data')
  final User? user;
  final MessageData? mess;

  UserResponse({this.user, this.mess});

  factory UserResponse.fromJson(Map<String, dynamic> json) => _$UserResponseFromJson(json);
}
