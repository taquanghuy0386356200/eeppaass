import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/transaction_ticket_purchase/transaction_ticket_purchase.dart';
import 'package:epass/commons/widgets/container/ticket_bottom_jag_clipper.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/commons/enum/service_plan_type.dart';

class TransactionTicketPurchaseCard extends StatelessWidget {
  final TransactionTicketPurchase item;

  const TransactionTicketPurchaseCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleShadow(
      offset: const Offset(0, 1),
      opacity: 0.1,
      sigma: 10,
      child: ClipPath(
        clipper: TicketBottomJagClipper(),
        child: Container(
          padding: EdgeInsets.fromLTRB(14.w, 16.h, 16.w, 16.h),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16.0),
              topRight: Radius.circular(16.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // TICKET TYPE AND VEHICLE TYPE
              Row(
                children: [
                  Icon(
                    Icons.circle,
                    color: item.servicePlanTypeId?.color,
                    size: 10.r,
                  ),
                  SizedBox(width: 8.w),
                  RichText(
                    text: TextSpan(
                      text: 'Loại vé: ',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.textGray2,
                          ),
                      children: [
                        TextSpan(
                          text: item.servicePlanTypeName,
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.w700,
                                  ),
                        )
                      ],
                    ),
                  ),
                  const Spacer(),
                  RichText(
                    text: TextSpan(
                      text: 'Loại xe: ',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.textGray2,
                          ),
                      children: [
                        TextSpan(
                          text: item.vehicleGroupName,
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.w700,
                                  ),
                        )
                      ],
                    ),
                  ),
                ],
              ),

              // PLATE NUMBER AND PRICE
              SizedBox(height: 20.h),
              Row(
                children: [
                  Expanded(
                    child: GradientText(
                      'Xe ${item.plateNumber ?? ''}',
                      gradient: const LinearGradient(colors: [
                        ColorName.primaryGradientStart,
                        ColorName.primaryGradientEnd,
                      ]),
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            fontWeight: FontWeight.w700,
                          ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Text(
                    (item.price ?? 0).formatCurrency(symbol: '₫'),
                    style: Theme.of(context).textTheme.headline5!.copyWith(
                          fontWeight: FontWeight.w700,
                        ),
                  ),
                ],
              ),
              SizedBox(height: 12.h),
              Divider(color: Colors.grey[300]),
              SizedBox(height: 16.h),

              Row(
                children: [
                  Text(
                    'Ngày mua',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorName.borderColor,
                        ),
                  ),
                  SizedBox(width: 12.w),
                  const Spacer(),
                  Text(
                    Jiffy(item.saleTransDate, "yyyy-MM-dd'T'HH:mm:ss+HH:mm")
                        .format('dd/MM/yyyy'),
                    maxLines: 2,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ],
              ),
              SizedBox(height: 12.h),

              Row(
                children: [
                  Text(
                    item.station != null ? 'Trạm' : 'Đoạn',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorName.borderColor,
                        ),
                  ),
                  SizedBox(width: 12.w),
                  const Spacer(),
                  Text(
                    item.station ?? item.stage ?? '',
                    maxLines: 2,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ],
              ),
              SizedBox(height: 12.h),

              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Hiệu lực',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorName.borderColor,
                        ),
                  ),
                  SizedBox(width: 12.w),
                  Expanded(
                    child: Text(
                      '  Từ ${item.effDate}\nđến ${item.expDate}',
                      maxLines: 2,
                      textAlign: TextAlign.end,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 12.h),
            ],
          ),
        ),
      ),
    );
  }
}
