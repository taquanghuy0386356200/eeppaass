// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_vehicle_etag.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateVehicleEtag _$UpdateVehicleEtagFromJson(Map<String, dynamic> json) =>
    UpdateVehicleEtag(
      custId: json['custId'] as String?,
      reasonId: json['reasonId'] as String?,
      actTypeId: json['actTypeId'] as int? ?? 20,
    );

Map<String, dynamic> _$UpdateVehicleEtagToJson(UpdateVehicleEtag instance) =>
    <String, dynamic>{
      'custId': instance.custId,
      'reasonId': instance.reasonId,
      'actTypeId': instance.actTypeId,
    };
