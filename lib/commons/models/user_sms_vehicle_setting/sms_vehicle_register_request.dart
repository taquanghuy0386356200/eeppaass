/// code : "123"
/// autoRenew : "1"
/// nextMonth : false

class SmsVehicleRegisterRequest {
  SmsVehicleRegisterRequest({
    required this.code,
    required this.autoRenew,
    this.nextMonth = false,
  });

  final String code;
  final String autoRenew;
  final bool? nextMonth;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = code;
    map['autoRenew'] = autoRenew;
    map['nextMonth'] = nextMonth;
    return map;
  }
}
