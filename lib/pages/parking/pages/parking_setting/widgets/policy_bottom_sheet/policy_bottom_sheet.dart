import 'package:epass/commons/models/parking/config_service_response.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/parking/pages/parking_setting/bloc/parking_setting_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/policy_bottom_sheet/bloc/policy_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PolicyBottomSheet extends StatefulWidget {
  final ServiceModel? parkingService;
  final ParkingSettingBloc parkingBloc;

  const PolicyBottomSheet({
    Key? key,
    required this.parkingService,
    required this.parkingBloc
  }) : super(key: key);

  @override
  State<PolicyBottomSheet> createState() => _PolicyBottomSheetState();
}

class _PolicyBottomSheetState extends State<PolicyBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PolicyBloc, PolicyState>(
      builder: (context, state) {
        return Column(
          children: [
            Container(
              height: 10.h,
              width: 60.w,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.5),
                borderRadius: const BorderRadius.all(
                  Radius.circular(24.0),
                ),
              ),
            ),
            SizedBox(height: 10.h),
            Text('Điều khoản & Điều kiện',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(
                    fontSize: 18.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 6.h),
            Text(
                'Vui lòng đọc và đồng ý với các điều khoản '
                    'và điều kiện này để sử dụng dịch vụ',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(
                    fontSize: 12.sp,
                    color: ColorName.textGray2)),
            SizedBox(height: 6.h),
            Expanded(
              flex: 4,
              child: ListView.separated(
                padding: EdgeInsets.symmetric(vertical: 10.w),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Text(
                      widget.parkingService!
                          .serviceRules![index].ruleContent,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(
                          fontSize: 12.sp,
                          color: ColorName.textGray2));
                },
                separatorBuilder: (context, index) =>
                    SizedBox(height: 6.h),
                itemCount: widget.parkingService!.serviceRules!.length,
              ),
            ),
            SizedBox(height: 10.h),
            Material(
                color: Colors.white,
                child: CheckboxListTile(
                  title: Text(
                      "Tôi đã đọc và đồng ý với các điều khoản & điều kiện",
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(
                          fontSize: 10.sp,
                          color: ColorName.textGray2)),
                  value: state.isPolicyChecked,
                  onChanged: (newValue) {
                    BlocProvider.of<PolicyBloc>(context).add(PolicyChecked(isPolicyChecked: newValue));
                  },
                  controlAffinity: ListTileControlAffinity.leading,
                )
            ),
            SizedBox(height: 10.h),
            Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                width: 200.w,
                height: 50.h,
                child: PrimaryButton(
                  onTap: () async {
                    if (state.isPolicyChecked){
                      widget.parkingBloc.add(ServiceEnabled(serviceId: widget.parkingService!.serviceId));
                      Navigator.pop(context);
                    }
                  },
                  title: "Tiếp theo",
                  padding: EdgeInsets.zero,
                  enabled: state.isPolicyChecked,
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
