library endpoint;

/// /api/v1/mobile/get-version-current
const getAppVersion = '/api/v1/mobile/get-version-current';

/// /auth/realms/etc-internal/protocol/openid-connect/token
const login = '/auth/realms/etc-internal/protocol/openid-connect/token';

/// /api/v1/login
const loginCRM = '/api/v1/login';

/// /api/v1/logout
const logout = '/api/v1/logout';

/// /api/v1/mobile/contracts
const contractDetail = '/api/v1/mobile/contracts';

/// /api/v1/customers/{customerId}/contracts/{contractId}/ocsInfo
const ocsInfo = '/api/v1/customers/{customerId}/contracts/{contractId}/ocsInfo';

/// /api/v1/contract-payments/{contractId}
const contractPayment = '/api/v1/contract-payments/{contractId}';

/// /api/v1/campaign
const campaign = '/api/v1/campaign';

/// /api/v1/topup-channel
const topupChannel = '/api/v1/topup-channel';

/// /api/v1/topup-channel/{topupChannelCode}?value=10000
const topupChannelFee = '/api/v1/topup-channel/{topupChannelCode}';

/// /api/v1/topup/fee
const topupFee = '/api/v1/topup/fee';

/// /api/v1/topup/service-fee
const topupFeeUpdate = '/api/v1/topup/service-fee';

/// /api/v1/contracts-info
const contractsInfo = '/api/v1/contracts-info';

/// /api/v1/viettelpay/customers/{customerId}/contracts/{contractId}/sale-order
const cashInOrderViettelPay =
    '/api/v1/viettelpay/customers/{customerId}/contracts/{contractId}/sale-order';

/// /api/v1/mobile-app-chupt/customers/{customerId}/contracts/{contractId}/sale-order
const cashInOrderMomo =
    '/api/v1/mobile-app-chupt/customers/{customerId}/contracts/{contractId}/sale-order';

/// /api/v1/vnpay/customers/{customerId}/contracts/{contractId}/sale-order
const cashInOrderVNPay =
    '/api/v1/vnpay/customers/{customerId}/contracts/{contractId}/sale-order';

/// /api/v1/momo/pay/app
const confirmMomoPayment = '/api/v1/momo/pay/app';

/// /contracts/{contractId}/topup
const confirmContractsPayment = '/contracts/{contractId}/topup';

/// /api/v1/transactions-vehicles
const transactionVehicles = '/api/v1/transactions-vehicles';

/// /api/v1/contracts/topup
const contractTopup = '/api/v1/contracts/topup';

/// /api/v1/customers/{customerId}/contracts/{contractId}/act-contract-histories
const contractAction =
    '/api/v1/customers/{customerId}/contracts/{contractId}/act-contract-histories';

/// /api/v1/mobile/other-transaction-histories
const transactionOther = '/api/v1/mobile/other-transaction-histories';

/// /api/v1/notification-msg
const notifications = '/api/v1/notification-msg';

/// /api/v1/notification-msg/{notificationId}
const updateNotification = '/api/v1/notification-msg/{notificationId}';

/// /api/v1/mobile/ticket-purchase-histories
const transactionTicketPurchase = '/api/v1/mobile/ticket-purchase-histories';

/// /api/v1/customers/act-histories-use-service-other
const transactionParking = '/api/v1/customers/act-histories-use-service-other';

/// /api/v1/cust-invoices
const custInvoices = '/api/v1/cust-invoices';

/// /api/v1/cust-invoice-file/{invoiceId}
const downloadInvoice = '/api/v1/cust-invoice-file/{invoiceId}';

/// /api/v1/categories/plate-type
const plateTypes = '/api/v1/categories/plate-type';

/// /api/v1/request-otp/contract/reset-pass
const requestOTPResetPassword = '/api/v1/request-otp/contract/reset-pass';

/// /api/v1/mobile/reset/user
const resetPasswordOTPConfirm = '/api/v1/mobile/reset/user';

/// /api/v1/alias
const alias = '/api/v1/alias';

/// /api/v1/mobile/change/user
const changePassword = '/api/v1/mobile/change/user';

/// /api/v1/mobile/customers/contracts/vehicles
const userVehicles = '/api/v1/mobile/customers/contracts/vehicles';

/// /api/v1/customers/contracts/{contractId}/vehicles/assigned-rfid
const vehicles =
    '/api/v1/customers/contracts/{contractId}/vehicles/assigned-rfid';

/// /api/v1/vehicles/{vehicleId}/rfid/{rfid}
const vehicleRFID = '/api/v1/vehicles/{vehicleId}/rfid/{rfid}';

/// /api/v1/mobile/customers
const updateUser = '/api/v1/mobile/customers';

/// /api/v1/contract/avatar/download
const downloadAvatar = '/api/v1/contract/avatar/download';

/// /api/v1/stations
const stations = '/api/v1/stations';

/// /api/v1/mobile/stage/station
const stationStage = '/api/v1/mobile/stage/station';

/// /api/v1/service-plans/fee-boo
const servicePlanFeeBoo = '/api/v1/service-plans/fee-boo';

/// /api/v1/customers/{customerId}/contracts/{contractId}/charge-ticket-boo
const buyTicket =
    '/api/v1/customers/{customerId}/contracts/{contractId}/charge-ticket-boo';

/// /api/v1/count/insurance
const countInsurance = '/api/v1/count/insurance';

/// /api/v1/contracts/alert-money
const alertMoney = '/api/v1/contracts/alert-money';

/// /api/v1/categories
const categories = '/api/v1/categories';

/// /api/v1/sms/notification
const userSMSNotification = '/api/v1/sms/notification';

/// /api/v1/sms/register
const smsRegister = '/api/v1/sms/register';

/// /api/v1/sms/notification
const smsNotification = '/api/v1/sms/notification';

/// /api/v1/viettelpay/inquiry-balance
const viettelMoneyBalance = '/api/v1/viettelpay/inquiry-balance';

/// /api/v1/contracts/service
const invoiceCycle = '/api/v1/contracts/service';

/// /api/v2/ocr/idcard
const scanIdCard = '/api/v2/ocr/idcard';

/// /api/v1/vtt/provinces
const provincesVTT = '/api/v1/vtt/provinces';

/// /api/v1/vtt/districts
const districtsVTT = '/api/v1/vtt/districts';

/// /api/v1/vtt/wards
const wardsVTT = '/api/v1/vtt/wards';

/// /api/v1/vehicle-types
const vehicleType = '/api/v1/vehicle-types';

/// /api/v1/request-otp/register
const requestOTPRegister = '/api/v1/request-otp/register';

/// /api/v1/register
const register = '/api/v1/register';

/// /api/v1/customer-types
const customerType = '/api/v1/customer-types';

/// /api/v1/vehicle-tracks
const vehicleTracks = '/api/v1/vehicle-tracks';

/// /api/v1/stations/details
const stationDetails = '/api/v1/stations/details';

/// /api/v1/token-firebases
const tokenFirebase = '/api/v1/token-firebases';

/// /api/v1/contract/encrypted
const contractEncrypted = '/api/v1/contract/encrypted';

/// /api/v1/mobile/tickets
const feedback = '/api/v1/mobile/tickets';

/// /api/v1/ticket-types
const feedbackType = '/api/v1/ticket-types';

/// /api/v1/contracts/request-otp
const feedbackRequestOtp = '/api/v1/contracts/request-otp';

/// /api/v1/ticket-attachment/{attachmentId}/download
const downloadAttachment = '/api/v1/ticket-attachment/{attachmentId}/download';

/// /api/v1/categories/config
const categoriesConfig = '/api/v1/categories/config';

/// /api/v1/auto-config-topup/save
const saveConfigAutoTopup = '/api/v1/auto-config-topup/save';

/// /api/v1/auto-config-topup/update
const updateConfigAutoTopup = '/api/v1/auto-config-topup/update';

/// /api/v1/auto-config-topup/get
const getConfigAutoTopup = '/api/v1/auto-config-topup/get';

/// /auto-config-topup/getInfoBank
const getInfoBank = '/auto-config-topup/getInfoBank';

/// /api/v1/beneficiary-information/get
const getUserBeneficiarys = '/api/v1/beneficiary-information/get';

/// /api/v1/beneficiary-information/delete
const deleteBeneficiary = '/api/v1/beneficiary-information/delete';

const saveBeneficiary = '/api/v1/beneficiary-information/save';

const updateBeneficiary = '/api/v1/beneficiary-information/update';

const history = '/api/v1/topup/history';

const createOrderUrl = "https://sb-openapi.zalopay.vn/v2/create";

/// /api/v1/getCurrentVerifyConfig
const getCurrentVerifyConfig = '/api/v1/get-current-verify-config';

/// /api/v1/checkPassCode
const checkPassCode = '/api/v1/checkPassCode';

/// /api/v1/getTicketType
const getTicketType = '/api/v1/getTicketType';

const appStore = '/app_store.txt';

/// /api/v1/changePassCode
const changePassCode = '/api/v1/change-passcode';

/// /api/v1/categories/SERVICE_CONFIG
const getServiceConfig = '/api/v1/categories?table_name=SERVICE_CONFIG';

/// /api/v1/get-config-services
const getConfigServices = '/api/v1/get-config-services';

/// /api/v1/check-condition-use-service
const checkConditionUseService = '/api/v1/check-condition-use-service';

/// /api/v1/add-service
const addService = '/api/v1/add-service';

/// /api/v1/config-method-verify
const configMethodVerify = '/api/v1/config-method-verify';

/// /api/v1/request-otp
const requestOtp = '/api/v1/request-otp';

/// /api/v1/mobile/sale-orders/waiting
const getWaitingOrder = '/api/v1/sale-order/waiting';

/// /api/v1/mobile/sale-orders/waiting
const getWaitingMobileOrder = '/api/v1/mobile/sale-orders/waiting';

/// /api/v1/sale-orders/confirm
const confirmWaitingOrder = '/api/v1/sale-orders/confirm';

/// /api/v1/get-contract-allow-access-service
const getUnavailableService = '/api/v1/get-contract-allow-access-service';

///api/v1/lane-parking/list-vehicles-contract
const getListWaitForConfirmation =
    '/api/v1/lane-parking/list-vehicles-contract';

////api/v1/lane-parking/cancel
const waitForConfirmationCancel = '/api/v1/lane-parking/cancel';

////api/v1/lane-parking/confirm-payment
const waitForConfirmation = '/api/v1/lane-parking/confirm-payment';

/// /api/v1/auto-topup/topup-account
const topupEwallet = '/api/v1/auto-topup/topup-account';

/// /api/v1/contract/1003985/deposit/50000000
const checkTypeCust = '/api/v1/contract/{contractId}/deposit/{amount}';

/// /api/v1/bidv/epass-topup
const topupBIDV = '/api/v1/bidv/epass-topup';

/// /contract/{contractId}/vehicle
const checkVehicleInContract = '/api/v1/contract/{contractId}/vehicle';

/// /api/v1/vietmap/link/check
const checkVietMapStatus = '/api/v1/vietmap/link/check';

/// /api/v1/vietmap/link/epass-to-vml
const linkVietMap = '/api/v1/vietmap/link/epass-to-vml';

/// /api/v1/vietmap/link/delete
const unLinkVietMap = '/api/v1/vietmap/link/delete';

/// /api/v1/vietmap/charge
const createVietMapTransaction = '/api/v1/vietmap/charge';

/// /api/v1/vietmap/update
const updateVietMapTransaction = '/api/v1/vietmap/update';

/// /api/v1/debit/get-debit?contractId=&vehicleId=
const getDebit = '/api/v1/debit/get-debit';

/// /api/v1/debit-transaction/rfid/{vehicleId}
const getDebitTransaction = '/api/v1/debit-transaction/rfid/{vehicleId}';

const referralClient = 'api/v1/contract-recommend/create';
