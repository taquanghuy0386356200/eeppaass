import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/models/topup_channel/topup_channel_fee.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum CashInConfirmResult {
  confirmed,
  canceled,
}

class CashInConfirmModal extends StatelessWidget {
  final TopupChannel paymentMethod;
  final TopupChannelFee fee;
  final int amount;

  const CashInConfirmModal({
    Key? key,
    required this.paymentMethod,
    required this.fee,
    required this.amount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(16.w, 8.h, 4.w, 0.h),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Text(
                    'Xác nhận thanh toán',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
                SplashIconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.grey,
                    size: 24.r,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
          SizedBox(height: 8.h),
          const Divider(
            color: ColorName.borderColor,
            height: 0.5,
            thickness: 0.5,
          ),
          SizedBox(height: 24.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Phương thức',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.borderColor,
                            fontWeight: FontWeight.w600,
                          ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: Text(
                        paymentMethod.name ?? '',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.textGray1,
                              fontWeight: FontWeight.w700,
                            ),
                        textAlign: TextAlign.end,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 16.h),
                Row(
                  children: [
                    Text(
                      'Số tiền',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.borderColor,
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: Text(
                        amount.formatCurrency(symbol: '₫'),
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.textGray1,
                              fontWeight: FontWeight.w700,
                            ),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 16.h),
                (fee.fee ?? 0) != 0
                    ? Row(
                        children: [
                          Text(
                            'Phí giao dịch',
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      color: ColorName.borderColor,
                                      fontWeight: FontWeight.w600,
                                    ),
                          ),
                          SizedBox(width: 16.w),
                          Expanded(
                            child: Text(
                              (fee.fee ?? 0).formatCurrency(symbol: 'đ'),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    color: ColorName.textGray1,
                                    fontWeight: FontWeight.w700,
                                  ),
                              textAlign: TextAlign.end,
                            ),
                          )
                        ],
                      )
                    : const SizedBox.shrink(),
                SizedBox(height: 32.h),
                const Divider(
                  color: ColorName.borderColor,
                  height: 0.5,
                  thickness: 0.5,
                ),
                SizedBox(height: 24.h),
                Row(
                  children: [
                    Text(
                      'Tổng tiền',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.textGray1,
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: Text(
                        (amount + (fee.fee ?? 0)).formatCurrency(symbol: '₫'),
                        style: Theme.of(context).textTheme.headline5!.copyWith(
                              color: ColorName.textGray1,
                              fontWeight: FontWeight.bold,
                            ),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 42.h),
          Container(
            height: 56.h,
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 16.w),
            child: PrimaryButton(
              title: 'Thanh toán',
              onTap: () => Navigator.pop(
                context,
                CashInConfirmResult.confirmed,
              ),
            ),
          ),
          SizedBox(height: 20.h),
        ],
      ),
    );
  }
}
