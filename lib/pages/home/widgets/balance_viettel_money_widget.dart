import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/home/widgets/home_currency_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/commons/extensions/number_ext.dart';

class BalanceViettelMoneyWidget extends StatelessWidget {
  const BalanceViettelMoneyWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BalanceBloc(
        appBloc: context.read<AppBloc>(),
        userRepository: getIt<IUserRepository>(),
      )..add(BalanceViettelMoneyToggled()),
      child: BlocConsumer<BalanceBloc, BalanceState>(
        listenWhen: (previous, current) => current.error != null,
        listener: (context, state) {
          if (state.error != null) {
            showErrorSnackBBar(context: context, message: state.error!);
          }
        },
        builder: (context, state) {
          return Material(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(20.0),
            child: InkWell(
              borderRadius: BorderRadius.circular(20.0),
              onTap: state.isLoading
                  ? null
                  : () => context
                      .read<BalanceBloc>()
                      .add(BalanceViettelMoneyToggled()),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 10.w,
                  vertical: 6.h,
                ),
                child: Row(
                  children: [
                    state.isLoading
                        ? Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '— — — —',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.w700,
                                    ),
                              ),
                              Text(
                                ' đ',
                                style: Theme.of(context).textTheme.bodyText1,
                              )
                            ],
                          )
                        : state.isHidden
                            ? Text(
                                '＊＊＊＊＊＊',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.w500,
                                    ),
                              )
                            : HomeCurrencyText(
                                text: (state.balance ?? 0).formatCurrency(),
                              ),
                    SizedBox(width: 8.w),
                    state.isHidden
                        ? const Icon(
                            CupertinoIcons.eye_slash_fill,
                            color: Colors.grey,
                            size: 24.0,
                          )
                        : const Icon(
                            CupertinoIcons.eye_fill,
                            color: Colors.grey,
                            size: 24.0,
                          ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
