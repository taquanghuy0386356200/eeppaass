part of 'report_form_bloc.dart';

abstract class ReportFormEvent extends Equatable {
  const ReportFormEvent();
}

class ReporterNameChanged extends ReportFormEvent {
  final String? reporterName;

  const ReporterNameChanged(this.reporterName);

  @override
  List<Object?> get props => [reporterName];
}

class VehiclesChanged extends ReportFormEvent {
  final String? plateNumber;
  final String? plateTypeCode;

  const VehiclesChanged({
    this.plateNumber,
    this.plateTypeCode,
  });

  @override
  List<Object?> get props => [plateNumber, plateTypeCode];
}

class EmailChanged extends ReportFormEvent {
  final String? email;

  const EmailChanged(this.email);

  @override
  List<Object?> get props => [email];
}
class LocationChanged extends ReportFormEvent {
  final String? location;

  const LocationChanged(this.location);

  @override
  List<Object?> get props => [location];
}
class ProvinceChanged extends ReportFormEvent {
  final AddressVTT? province;

  const ProvinceChanged(this.province);

  @override
  List<Object?> get props => [province];
}
class DistrictChanged extends ReportFormEvent {
  final AddressVTT? district;

  const DistrictChanged(this.district);

  @override
  List<Object?> get props => [district];
}
class CommuneChanged extends ReportFormEvent {
  final AddressVTT? ward;

  const CommuneChanged(this.ward);

  @override
  List<Object?> get props => [ward];
}
class L1TicketTypeChanged extends ReportFormEvent {
  final FeedbackType? l1TicketType;

  const L1TicketTypeChanged(this.l1TicketType);

  @override
  List<Object?> get props => [l1TicketType];
}
class L2TicketTypeChanged extends ReportFormEvent {
  final FeedbackType? l2TicketType;

  const L2TicketTypeChanged(this.l2TicketType);

  @override
  List<Object?> get props => [l2TicketType];
}
class L3TicketTypeChanged extends ReportFormEvent {
  final FeedbackType? l3TicketType;

  const L3TicketTypeChanged(this.l3TicketType);

  @override
  List<Object?> get props => [l3TicketType];
}
class StationIdChanged extends ReportFormEvent {
  final String? stationId;

  const StationIdChanged(this.stationId);

  @override
  List<Object?> get props => [stationId];
}
class StageIdChanged extends ReportFormEvent {
  final String? stageId;

  const StageIdChanged(this.stageId);

  @override
  List<Object?> get props => [stageId];
}
class StationStageNameChanged extends ReportFormEvent {
  final String? stationStageName;

  const StationStageNameChanged(this.stationStageName);

  @override
  List<Object?> get props => [stationStageName];
}
class ReceiveContentChanged extends ReportFormEvent {
  final String? content;

  const ReceiveContentChanged(this.content);

  @override
  List<Object?> get props => [content];
}
class TicketKindChanged extends ReportFormEvent {
  final String? ticketKind;

  const TicketKindChanged(this.ticketKind);

  @override
  List<Object?> get props => [ticketKind];
}
class SupportInfoChanged extends ReportFormEvent {
  final String? supportInfo;

  const SupportInfoChanged(this.supportInfo);

  @override
  List<Object?> get props => [supportInfo];
}
class PhoneContactChanged extends ReportFormEvent {
  final String? phoneContact;

  const PhoneContactChanged(this.phoneContact);

  @override
  List<Object?> get props => [phoneContact];
}

class ReportAttachmentFilesChanged extends ReportFormEvent {
  final List<String>? attachments;

  const ReportAttachmentFilesChanged(this.attachments);

  @override
  List<Object?> get props => [attachments];
}


