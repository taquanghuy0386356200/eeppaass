import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/widgets/select_all_button.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/widgets/vehicle_user_list_card.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_list/widgets/vehicle_list_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class VehicleUserPage extends StatefulWidget {
  const VehicleUserPage({
    Key? key,
    this.onChanged
  }) : super(key: key);

  final VoidCallback? onChanged;

  @override
  State<VehicleUserPage> createState() => _VehicleUserPageState();
}

class _VehicleUserPageState extends State<VehicleUserPage> {
  late RefreshController _refreshController;

  var _selectedVehicles = <Vehicle>[];
  var _searchTerm = '';

  @override
  void initState() {
    _refreshController = RefreshController(initialRefresh: false);
    _selectedVehicles = context.read<TicketVehicleBloc>().state.userVehicles;
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: BlocConsumer<VehicleListBloc, VehicleListState>(
        listener: (context, state) {
          if (state.error == null) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          } else if (state.error != null) {
            _refreshController.refreshFailed();
            _refreshController.loadFailed();

            if (state.listData.isNotEmpty) {
              showErrorSnackBBar(
                context: context,
                message: state.error ?? 'Có lỗi xảy ra',
              );
            }
          }
        },
        builder: (context, state) {
          return Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 24.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: PrimaryTextField(
                      labelText: "Nhập biển số xe",
                      hintText: '30H12345',
                      isPlateNumber: true,
                      onChanged: (value) =>
                          setState(() => _searchTerm = (value?.trim() ?? '')),
                      suffix: Assets.icons.search.svg(
                        width: 24.r,
                        height: 24.r,
                        color: ColorName.borderColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 8.h),
                  Expanded(
                    child: Builder(
                      builder: (context) {
                        if (state.isLoading ||
                            state.isRefreshing && state.listData.isEmpty) {
                          return const VehicleListShimmerLoading();
                        } else if (state.error != null &&
                            state.listData.isEmpty) {
                          return Center(
                            child: Text(state.error ?? 'Có lỗi xảy ra'),
                          );
                        }
                        final listData = state.listData
                            .where((element) =>
                                element.plateNumber
                                    ?.toLowerCase()
                                    .contains(_searchTerm.toLowerCase()) ??
                                false)
                            .toList();
                        if (listData.isEmpty) {
                          return const NoDataPage();
                        }

                        return SmartRefresher(
                          physics: const BouncingScrollPhysics(),
                          enablePullDown: true,
                          enablePullUp: !state.isFull,
                          onRefresh: () => context
                              .read<VehicleListBloc>()
                              .add(VehicleListRefreshed()),
                          onLoading: () => context
                              .read<VehicleListBloc>()
                              .add(VehicleListLoadMore()),
                          controller: _refreshController,
                          child: ListView.builder(
                            physics: const BouncingScrollPhysics(),
                            itemCount: listData.length + 1,
                            itemBuilder: (context, i) {
                              if (i == 0) {
                                return Visibility(
                                  visible:
                                      listData.length == state.listData.length,
                                  child: SelectAllButton(
                                    isCheck: _selectedVehicles.length ==
                                        state.listData.length,
                                    onTap: _onSelectAllTap,
                                  ),
                                );
                              }
                              final index = i - 1;
                              final vehicle = listData[index];
                              return Padding(
                                padding: EdgeInsets.symmetric(horizontal: 16.w),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    top: index == 0 ? 8.h : 20.h,
                                    bottom: index == listData.length - 1
                                        ? 120.h
                                        : 0,
                                  ),
                                  child: VehicleUserListCard(
                                    vehicle: vehicle,
                                    isSelected:
                                        _selectedVehicles.contains(vehicle),
                                    onTap: () => _onVehicleTapped(vehicle),
                                  ),
                                ),
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
              state.listData.isNotEmpty
                  ? Align(
                      alignment: Alignment.bottomCenter,
                      child: AnimatedContainer(
                        duration: const Duration(milliseconds: 500),
                        height: 100.h,
                        color: Colors.white,
                        child: Wrap(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 16.w,
                                vertical: 20.h,
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 2,
                                    blurRadius: 10,
                                    offset: const Offset(
                                        0, -6), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: SizedBox(
                                height: 56.h,
                                width: double.infinity,
                                child: PrimaryButton(
                                  title:
                                      'Chọn xong (${_selectedVehicles.length})',
                                  onTap: _onDoneTap,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : const SizedBox.shrink(),
            ],
          );
        },
      ),
    );
  }

  void _onSelectAllTap() {
    final userVehicles = context.read<VehicleListBloc>().state.listData;
    if (_selectedVehicles.length == userVehicles.length) {
      setState(() => _selectedVehicles = []);
    } else {
      setState(() => _selectedVehicles = List.from(userVehicles));
    }
  }

  void _onVehicleTapped(Vehicle vehicle) {
    if (_selectedVehicles.contains(vehicle)) {
      setState(() =>
          _selectedVehicles = List.from(_selectedVehicles)..remove(vehicle));
    } else {
      setState(
          () => _selectedVehicles = List.from(_selectedVehicles)..add(vehicle));
    }
  }

  void _onDoneTap() {
    context
        .read<TicketVehicleBloc>()
        .add(TicketVehicleUserChanged(_selectedVehicles));
    context.popRoute().then((_) => widget.onChanged?.call());
  }
}
