part of 'sms_vehicle_bloc.dart';

abstract class SMSVehicleEvent extends Equatable {
  const SMSVehicleEvent();
}

class UserSMSVehicleFetched extends SMSVehicleEvent {
  @override
  List<Object> get props => [];
}

class SMSVehicleRegistered extends SMSVehicleEvent {
  final String smsRegisCode;
  final bool autoRenew;

  const SMSVehicleRegistered({
    required this.smsRegisCode,
    required this.autoRenew,
  });

  @override
  List<Object> get props => [smsRegisCode, autoRenew];
}

class SMSVehicleUpdated extends SMSVehicleEvent {
  final int status;
  final bool autoRenew;

  const SMSVehicleUpdated({
    required this.status,
    required this.autoRenew,
  });

  @override
  List<Object?> get props => [status, autoRenew];
}
