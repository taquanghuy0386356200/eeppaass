import 'package:epass/commons/widgets/modal/alert_dialog.dart';
import 'package:epass/commons/widgets/modal/neutral_confirm_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/station_vehicle_alert/station_vehicle_alert_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:permission_handler/permission_handler.dart';

class StationVehicleAlertSetting extends StatelessWidget {
  const StationVehicleAlertSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<StationVehicleAlertBloc, StationVehicleAlertState>(
      listener: (context, state) {
        if (state.isLoading) {
          context.loaderOverlay.show();
        } else {
          context.loaderOverlay.hide();
        }
        if (state.isOpenLocationPermission) {
          showAlertDialog(
            context,
            title: 'Mở quyền truy cập',
            content: const Text(
              "Quý khách vui lòng vào mục cài đặt trên điện thoại bật quyền truy cập vị trí để sử dụng tính năng này!",
              textAlign: TextAlign.center,
            ),
            primaryButtonTitle: 'Mở quyền',
            secondaryButtonTitle: 'Đóng',
            onPrimaryButtonTap: (context) {
              openAppSettings().then((_) => Navigator.of(context).pop());
            },
            onSecondaryButtonTap: (context) {
              Navigator.of(context).pop();
            }
          );
        }
        if ((state.error ?? '').isNotEmpty) {
          showErrorSnackBBar(context: context, message: state.error!);
        }

      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(16.w, 20.h, 17.w, 20.h),
        child: BlocSelector<StationVehicleAlertBloc, StationVehicleAlertState,
            bool>(
          selector: (state) => state.isOn ?? false,
          builder: (context, isOn) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Cảnh báo xe qua trạm',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    const Spacer(),
                    CupertinoSwitch(
                      value: isOn,
                      activeColor: ColorName.primaryColor,
                      onChanged: (isOn) async {
                        context
                            .read<StationVehicleAlertBloc>()
                            .add(StationVehicleAlerted(isOn: isOn));
                      },
                    ),
                  ],
                ),
                SizedBox(height: 12.h),
                Text(
                  'Khi bật chức năng này sẽ cho phép ứng dụng ePass thu thập dữ liệu'
                  ' vị trí của bạn ngay cả khi ứng dụng'
                  ' chạy nền để có thể cảnh báo nếu số dư'
                  ' tài khoản giao thông của bạn không đủ tiền khi đến gần trạm thu phí.',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
