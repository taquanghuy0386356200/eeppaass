import 'dart:io';

import 'package:epass/commons/models/user/user.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/profile/avatar_bloc.dart';
import 'package:epass/pages/home/widgets/gender_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AvatarWidget extends StatelessWidget {
  final double? height;
  final double? width;

  const AvatarWidget({
    Key? key,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<AvatarBloc, AvatarState, String?>(
      selector: (state) => state.avatarFilePath,
      builder: (context, filePath) {
        if (filePath != null) {
          final file = File(filePath);

          if (file.existsSync()) {
            return ClipRRect(
              borderRadius: BorderRadius.circular((height ?? 48.r) / 2),
              child: Image.file(
                file,
                width: width ?? 48.r,
                height: height ?? 48.r,
                fit: BoxFit.cover,
              ),
            );
          }
        }

        return BlocSelector<AppBloc, AppState, Gender>(
          selector: (state) {
            return state.user?.gender ?? Gender.unknown;
          },
          builder: (context, gender) {
            return GenderAvatar(
              gender: gender,
              height: height ?? 48.r,
              width: 48.r,
            );
          },
        );
      },
    );
  }
}
