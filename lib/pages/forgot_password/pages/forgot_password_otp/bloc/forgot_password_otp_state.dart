part of 'forgot_password_otp_bloc.dart';

abstract class ForgotPasswordOtpState extends Equatable {
  const ForgotPasswordOtpState();
}

class ForgotPasswordOtpInitial extends ForgotPasswordOtpState {
  @override
  List<Object> get props => [];
}

class ForgotPasswordOTPConfirmedInProgress extends ForgotPasswordOtpState {
  @override
  List<Object> get props => [];
}

class ForgotPasswordOTPConfirmedSuccess extends ForgotPasswordOtpState {
  @override
  List<Object> get props => [];
}

class ForgotPasswordOTPConfirmedFailure extends ForgotPasswordOtpState {
  final String message;

  const ForgotPasswordOTPConfirmedFailure(this.message);

  @override
  List<Object> get props => [message];
}
