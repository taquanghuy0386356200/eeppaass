// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle_register_card_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleRegisterCardRequest _$VehicleRegisterCardRequestFromJson(
        Map<String, dynamic> json) =>
    VehicleRegisterCardRequest(
      phoneNumber: json['phoneNumber'] as String?,
      email: json['email'] as String?,
      street: json['street'] as String?,
      areaCode: json['areaCode'] as String?,
      areaName: json['areaName'] as String?,
    );

Map<String, dynamic> _$VehicleRegisterCardRequestToJson(
        VehicleRegisterCardRequest instance) =>
    <String, dynamic>{
      'phoneNumber': instance.phoneNumber,
      'email': instance.email,
      'street': instance.street,
      'areaCode': instance.areaCode,
      'areaName': instance.areaName,
    };
