// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contracts_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractsInfo _$ContractsInfoFromJson(Map<String, dynamic> json) =>
    ContractsInfo(
      contractId: json['contractId'] as int?,
      contractNo: json['contractNo'] as String?,
      plateNumber: json['plateNumber'] as String?,
      custName: json['custName'] as String?,
    );

Map<String, dynamic> _$ContractsInfoToJson(ContractsInfo instance) =>
    <String, dynamic>{
      'contractId': instance.contractId,
      'contractNo': instance.contractNo,
      'plateNumber': instance.plateNumber,
      'custName': instance.custName,
    };

ContractsInfoData _$ContractsInfoDataFromJson(Map<String, dynamic> json) =>
    ContractsInfoData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => ContractsInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$ContractsInfoDataToJson(ContractsInfoData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

ContractsInfoResponse _$ContractsInfoResponseFromJson(
        Map<String, dynamic> json) =>
    ContractsInfoResponse(
      data: json['data'] == null
          ? null
          : ContractsInfoData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContractsInfoResponseToJson(
        ContractsInfoResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
