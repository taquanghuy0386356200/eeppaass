// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'info_vehicle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InfoVehicleResponse _$InfoVehicleResponseFromJson(Map<String, dynamic> json) =>
    InfoVehicleResponse(
      data: json['data'] == null
          ? null
          : ListInfoVehicleResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$InfoVehicleResponseToJson(
        InfoVehicleResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ListInfoVehicleResponse _$ListInfoVehicleResponseFromJson(
        Map<String, dynamic> json) =>
    ListInfoVehicleResponse(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) =>
              DataInfoVehicleResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
      activeOto: json['activeOto'] as int?,
      activeMotobike: json['activeMotobike'] as int?,
    );

Map<String, dynamic> _$ListInfoVehicleResponseToJson(
        ListInfoVehicleResponse instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
      'activeOto': instance.activeOto,
      'activeMotobike': instance.activeMotobike,
    };

DataInfoVehicleResponse _$DataInfoVehicleResponseFromJson(
        Map<String, dynamic> json) =>
    DataInfoVehicleResponse(
      json['vehicleId'] as int?,
      json['contractId'] as int?,
      json['plateNumber'] as String?,
      json['vehicleTypeId'] as int?,
      json['vehicleGroupId'] as int?,
      json['vehicleGroupCode'] as String?,
      json['vehicleGroupName'] as String?,
      json['vehicleGroupDescription'] as String?,
      json['seatNumber'] as int?,
      (json['netWeight'] as num?)?.toDouble(),
      json['status'] as String?,
      json['activeStatus'] as String?,
      json['epc'] as String?,
      json['rfidSerial'] as String?,
      json['deptId'] as int?,
      json['profileStatus'] as String?,
      json['vehicleTypeCode'] as String?,
      json['plateTypeCode'] as String?,
    );

Map<String, dynamic> _$DataInfoVehicleResponseToJson(
        DataInfoVehicleResponse instance) =>
    <String, dynamic>{
      'vehicleId': instance.vehicleId,
      'contractId': instance.contractId,
      'plateNumber': instance.plateNumber,
      'vehicleTypeId': instance.vehicleTypeId,
      'vehicleGroupId': instance.vehicleGroupId,
      'vehicleGroupCode': instance.vehicleGroupCode,
      'vehicleGroupName': instance.vehicleGroupName,
      'vehicleGroupDescription': instance.vehicleGroupDescription,
      'seatNumber': instance.seatNumber,
      'netWeight': instance.netWeight,
      'status': instance.status,
      'activeStatus': instance.activeStatus,
      'epc': instance.epc,
      'rfidSerial': instance.rfidSerial,
      'deptId': instance.deptId,
      'profileStatus': instance.profileStatus,
      'vehicleTypeCode': instance.vehicleTypeCode,
      'plateTypeCode': instance.plateTypeCode,
    };
