import 'package:bloc/bloc.dart';
import 'package:epass/pages/invoice_setting/invoice_cycle.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'register_service_event.dart';

part 'register_service_state.dart';

class RegisterServiceBloc extends Bloc<RegisterServiceEvent, RegisterServiceState> {
  RegisterServiceBloc() : super(const RegisterServiceState()) {
    on<RegisterServiceUpdated>((event, emit) {
      emit(state.copyWith(
        invoiceCycle: event.invoiceCycle,
        invoiceCycleGroupType: event.invoiceCycle == InvoiceCycle.monthly
            ? event.invoiceCycleGroupType ?? InvoiceCycleGroupType.byContract
            : null,
      ));
    });
  }
}
