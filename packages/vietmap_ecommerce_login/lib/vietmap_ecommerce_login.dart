library vietmap_ecommerce_login;

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';

Future<VIETMAPEcommerceAuth?> openVIETMAPEcommerceLoginScreen(
  BuildContext context, {
  required Function(BuildContext context, VIETMAPEcommerceAuth) onLoggedIn,
  required Function(BuildContext context) onContextChanged,
}) {
  return Navigator.of(context).push(
    MaterialPageRoute(
      builder: (context) => _LoginScreenWebView(
        onLoggedIn: onLoggedIn,
        onContextChanged: onContextChanged,
      ),
    ),
  );
}

class _LoginScreenWebView extends StatefulWidget {
  final Function(BuildContext context, VIETMAPEcommerceAuth) onLoggedIn;
  final Function(BuildContext context) onContextChanged;

  const _LoginScreenWebView({
    Key? key,
    required this.onLoggedIn,
    required this.onContextChanged,
  }) : super(key: key);
  @override
  State<_LoginScreenWebView> createState() => __LoginScreenWebViewState();
}

class __LoginScreenWebViewState extends State<_LoginScreenWebView> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      widget.onContextChanged(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
            child: WebView(
              initialUrl:
                  'https://minio.vietmap.vn/khoaln/ecommerce/prod/epass/web/index.html?login=true',
              javascriptMode: JavascriptMode.unrestricted,
              javascriptChannels: {
                JavascriptChannel(
                  name: 'VIETMAPEcommerce',
                  onMessageReceived: (JavascriptMessage message) async {
                    final data = message.message;
                    try {
                      final splited = data.split('#');
                      final method = splited.getOrNull(0);
                      final params = splited.getOrNull(1);
                      if (method?.isNotEmpty == true) {
                        switch (method!) {
                          case 'authInfoUpdated':
                            final json =
                                jsonDecode(params!).cast<String, dynamic>()
                                    as Map<String, dynamic>;
                            if (mounted) {
                              widget.onLoggedIn(
                                  context, VIETMAPEcommerceAuth.fromMap(json));
                            }
                            break;
                          case 'flutterEngine':
                            if (params == 'running') {
                              if (isLoading && mounted) {
                                setState(() {
                                  isLoading = false;
                                });
                              }
                            }
                            break;
                          case 'setClipboardData':
                            if (params != null) {
                              Clipboard.setData(ClipboardData(text: params));
                            }
                            break;
                          case 'loginScreenClosed':
                            Navigator.pop(context);
                            break;
                        }
                      }
                    } catch (e) {
                      ///
                    }
                  },
                )
              },
            ),
          ),
          if (isLoading)
            Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            )
        ],
      ),
    );
  }
}

class VIETMAPEcommerceAuth {
  final String accessToken;
  final String phoneNumber;
  final String name;

  VIETMAPEcommerceAuth({
    required this.accessToken,
    required this.phoneNumber,
    required this.name,
  });
  Map<String, dynamic> toMap() {
    return {
      'accessToken': accessToken,
      'phoneNumber': phoneNumber,
      'customerName': name,
    };
  }

  factory VIETMAPEcommerceAuth.fromMap(Map<String, dynamic> map) {
    return VIETMAPEcommerceAuth(
      accessToken: map['accessToken'] ?? '',
      phoneNumber: map['phoneNumber'] ?? '',
      name: map['customerName'] ?? '',
    );
  }
  String toJson() => json.encode(toMap());
  factory VIETMAPEcommerceAuth.fromJson(String source) =>
      VIETMAPEcommerceAuth.fromMap(json.decode(source));
  @override
  String toString() =>
      'VIETMAPEcommerceAuth(accessToken: $accessToken, phoneNumber: $phoneNumber, name: $name)';
}

extension _SafeAccess<T> on Iterable<T> {
  T? getOrNull(int index) {
    if (isEmpty) {
      return null;
    }
    if (index >= length) {
      return null;
    }
    return elementAt(index);
  }
}
