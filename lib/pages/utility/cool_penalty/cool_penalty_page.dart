import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CoolPenaltyPage extends StatefulWidget {
  const CoolPenaltyPage({
    Key? key,
  }) : super(key: key);

  @override
  State<CoolPenaltyPage> createState() => _CoolPenaltyPageState();
}

class _CoolPenaltyPageState extends State<CoolPenaltyPage> {
  int _progress = 0;
  WebViewController? _controller;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: BasePage(
        backgroundColor: Colors.white,
        title: 'Tra cứu phạt nguội',
        leading: BackButton(
          color: Colors.white,
          onPressed: () async {
            final router = context.router;

            if (_controller == null) {
              router.pop();
              return;
            }
            final canGoBack = await _controller?.canGoBack() ?? false;
            if (canGoBack) {
              _controller?.goBack();
            } else {
              router.pop();
            }
          },
        ),
        trailing: [
          SplashIconButton(
            icon: const Icon(CupertinoIcons.xmark, color: Colors.white),
            onTap: context.popRoute,
          )
        ],
        child: Stack(
          children: [
            const FadeAnimation(
              delay: 0.5,
              child: GradientHeaderContainer(),
            ),
            SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _progress != 100
                      ? LinearProgressIndicator(
                          value: _progress.toDouble() / 100.0,
                          backgroundColor: ColorName.blue.withOpacity(0.3),
                          valueColor: const AlwaysStoppedAnimation(ColorName.blue),
                          minHeight: 2.h,
                        )
                      : SizedBox(height: 2.h),
                  Flexible(
                    fit: FlexFit.tight,
                    child: WebView(
                      initialUrl: Constant.coolPenaltyUrl,
                      javascriptMode: JavascriptMode.unrestricted,
                      onWebViewCreated: (WebViewController webViewController) {
                        _controller = webViewController;
                      },
                      onProgress: (progress) {
                        setState(() {
                          _progress = progress;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
