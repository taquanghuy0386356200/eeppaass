part of 'list_report_bloc.dart';

@immutable
class ReportState extends Equatable {
  final List<FeedbackModel> listReport;
  final FeedbackChannel reportChannel;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;

  const ReportState({
    required this.listReport,
    this.reportChannel = FeedbackChannel.report,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
  });

  @override
  List<Object?> get props => [
    listReport,
    reportChannel,
    error,
    isLoading,
    isRefreshing,
  ];

  ReportState copyWith({
    List<FeedbackModel>? listReport,
    FeedbackChannel? reportChannel,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
  }) {
    return ReportState(
      listReport: listReport ?? this.listReport,
      reportChannel: reportChannel ?? this.reportChannel,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
    );
  }
}
