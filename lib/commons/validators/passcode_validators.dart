class PasscodeValidator {
  final _expression =
  RegExp(r'^(?=.*?[0-9]).{6,6}$');

  String? passcodeValidator(String? passcode) {
    if (passcode == null || passcode.isEmpty) {
      return 'Vui lòng nhập Mật khẩu';
    } else if (!_expression.hasMatch(passcode)) {
      return 'Mật khẩu yêu cầu 6 ký tự số';
    }
    return null;
  }
}
