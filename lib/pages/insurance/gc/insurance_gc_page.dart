import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/flavors.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InsuranceGCPage extends StatefulWidget {
  final String? contractNoEncrypted;

  const InsuranceGCPage({
    Key? key,
    this.contractNoEncrypted,
  }) : super(key: key);

  @override
  State<InsuranceGCPage> createState() => _InsuranceGCPageState();
}

class _InsuranceGCPageState extends State<InsuranceGCPage> {
  int _progress = 0;

  late String url;

  @override
  void initState() {
    context.read<HomeTabsBloc>().add(const HomeTabBarRequestHidden(isHidden: true));

    url = '${F.insuranceGCURL}'
        '?token=${F.insuranceGCToken}'
        '&optional_partner=${Uri.encodeQueryComponent(widget.contractNoEncrypted ?? '')}';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: BasePage(
        backgroundColor: Colors.white,
        leading: Container(),
        trailing: [CloseButton(color: Colors.white, onPressed: context.popRoute)],
        title: 'Bảo hiểm GC',
        child: Stack(
          children: [
            const FadeAnimation(
              delay: 0.5,
              child: GradientHeaderContainer(),
            ),
            SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _progress != 100
                      ? LinearProgressIndicator(
                          value: _progress.toDouble() / 100.0,
                          backgroundColor: ColorName.blue.withOpacity(0.3),
                          valueColor: const AlwaysStoppedAnimation(ColorName.blue),
                          minHeight: 2.h,
                        )
                      : SizedBox(height: 2.h),
                  Flexible(
                    fit: FlexFit.tight,
                    child: WebView(
                      initialUrl: url,
                      javascriptMode: JavascriptMode.unrestricted,
                      onWebViewCreated: (WebViewController webViewController) {},
                      onProgress: (progress) {
                        setState(() {
                          _progress = progress;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
