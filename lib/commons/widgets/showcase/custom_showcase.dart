import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';

class CustomShowcase extends StatelessWidget {
  final GlobalKey showCaseKey;
  final Widget child;
  final EdgeInsets? overlayPadding;
  final String description;

  const CustomShowcase({
    Key? key,
    required this.showCaseKey,
    required this.child,
    this.overlayPadding,
    required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Showcase(
      key: showCaseKey,
      overlayPadding: overlayPadding ?? const EdgeInsets.all(16.0),
      overlayColor: Colors.black87,
      overlayOpacity: 0.8,
      showcaseBackgroundColor: Colors.transparent,
      description: description,
      descTextStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
      child: child,
    );
  }
}
