import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';

import '../../../commons/constant.dart';

class FirebaseTrack {
  FirebaseTrack._();

  static const String EVENT_OPEN = "open_app";
  static const String EVENT_CLOSE = "close_app";
  static const String EVENT_LOGIN = "Login";
  static const String EVENT_SCREEN = "pageView";
  static const String EVENT_BUTTON_CLICK = "button_click";

  static final FlutterSecureStorage _secureStorage =
      GetIt.instance.get<FlutterSecureStorage>();

  static Future<String?> getDeviceId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor;
    } else if (Platform.isAndroid) {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.id;
    }
    return "";
  }

  static void track(String eventName, {Map<String, dynamic>? value}) async {
    var instance = FirebaseAnalytics.instance;
    var deviceId = await getDeviceId();

    final contractNo =
        await _secureStorage.read(key: Constant.encryptedUsernameKey) ??
            deviceId;

    await instance.setUserId(id: contractNo);
    var time = DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.now());
    Map<String, dynamic> map = {
      'userId': contractNo,
      'time': time,
    };
    if (value != null) map.addAll(value);
    await Future.forEach(map.entries, (MapEntry entry) async {
      await instance.setUserProperty(name: entry.key, value: entry.value);
    });
    instance.logEvent(name: eventName, parameters: map);
  }

  static void trackLogin() {
    track(EVENT_LOGIN);
  }

  static void trackScreen(String screenName, String type) {
    track(EVENT_SCREEN, value: {'type': type, 'screenName': screenName});
  }

  static void trackOpenApp() {
    track(EVENT_OPEN);
  }

  static void trackCloseApp() {
    track(EVENT_CLOSE);
  }

  static void trackClick(String buttonName) async {
    var instance = FirebaseAnalytics.instance;
    await instance.setUserProperty(name: "buttonName", value: buttonName);
    track(EVENT_BUTTON_CLICK, value: {'buttonName': buttonName});
  }
}
