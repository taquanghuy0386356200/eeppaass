// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'station_stage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StationStage _$StationStageFromJson(Map<String, dynamic> json) => StationStage(
      name: json['name'] as String?,
      code: json['code'] as String?,
      stationId: json['stationId'] as int?,
      stageId: json['stageId'] as int?,
      methodChargeId: $enumDecodeNullable(
          _$MethodChargeIntEnumMap, json['methodChargeId'],
          unknownValue: MethodChargeInt.unknown),
      longitude: (json['longitude'] as num?)?.toDouble(),
      latitude: (json['latitude'] as num?)?.toDouble(),
      address: json['address'] as String?,
      addressIn: json['addressIn'] as String?,
      addressOut: json['addressOut'] as String?,
      booCode: $enumDecodeNullable(_$BooCodeEnumMap, json['booCode'],
          unknownValue: BooCode.unknown),
      isEtc: json['isEtc'] as int?,
      priceImagePath: json['priceImagePath'] as String?,
      priceLinkWeb: json['priceLinkWeb'] as String?,
      stationType: json['stationType'] as int?,
    );

Map<String, dynamic> _$StationStageToJson(StationStage instance) =>
    <String, dynamic>{
      'name': instance.name,
      'code': instance.code,
      'stationId': instance.stationId,
      'stageId': instance.stageId,
      'methodChargeId': _$MethodChargeIntEnumMap[instance.methodChargeId],
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'address': instance.address,
      'addressIn': instance.addressIn,
      'addressOut': instance.addressOut,
      'booCode': _$BooCodeEnumMap[instance.booCode],
      'isEtc': instance.isEtc,
      'priceImagePath': instance.priceImagePath,
      'priceLinkWeb': instance.priceLinkWeb,
      'stationType': instance.stationType,
    };

const _$MethodChargeIntEnumMap = {
  MethodChargeInt.normal: 1,
  MethodChargeInt.block: 2,
  MethodChargeInt.unknown: 'unknown',
};

const _$BooCodeEnumMap = {
  BooCode.boo1: 'BOO1',
  BooCode.boo2: 'BOO2',
  BooCode.unknown: 'unknown',
};

StationStageData _$StationStageDataFromJson(Map<String, dynamic> json) =>
    StationStageData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => StationStage.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$StationStageDataToJson(StationStageData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

StationStageResponse _$StationStageResponseFromJson(
        Map<String, dynamic> json) =>
    StationStageResponse(
      data: json['data'] == null
          ? null
          : StationStageData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$StationStageResponseToJson(
        StationStageResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
