import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/bloc/transaction_vehicle_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionVehicleSearchForm extends StatefulWidget {
  final String? plateNumber;
  final DateTime timestampInFrom;
  final DateTime timestampInTo;

  const TransactionVehicleSearchForm({
    Key? key,
    this.plateNumber,
    required this.timestampInFrom,
    required this.timestampInTo,
  }) : super(key: key);

  @override
  State<TransactionVehicleSearchForm> createState() =>
      _TransactionVehicleSearchFormState();
}

class _TransactionVehicleSearchFormState
    extends State<TransactionVehicleSearchForm> {
  final _plateNumberController = TextEditingController();
  final _timestampInFromController = TextEditingController();
  final _timestampInToController = TextEditingController();

  late DateTime _timestampInFrom;
  late DateTime _timestampInTo;

  @override
  void initState() {
    super.initState();
    context.read<TransactionVehicleBloc>().isSearching = false;
    _plateNumberController.text = widget.plateNumber ?? '';

    _timestampInFrom = widget.timestampInFrom;
    _timestampInTo = widget.timestampInTo;

    _timestampInFromController.text =
        Jiffy(_timestampInFrom).format('dd/MM/yyyy');
    _timestampInToController.text = Jiffy(_timestampInTo).format('dd/MM/yyyy');
  }

  @override
  void dispose() {
    _plateNumberController.dispose();
    _timestampInFromController.dispose();
    _timestampInToController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Positioned(
            top: 12.h,
            right: 12.w,
            child: SplashIconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: ColorName.borderColor,
              ),
              onTap: () => Navigator.of(context).pop(),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'TÌM KIẾM',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(height: 24.h),
                PrimaryTextField(
                  controller: _plateNumberController,
                  labelText: 'Biển số xe',
                  hintText: 'Biển số xe (00X1234)',
                  isPlateNumber: true,
                  maxLength: 20,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _timestampInFromController,
                  labelText: 'Ngày qua trạm: từ ngày*',
                  hintText: 'Từ ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  hasClearButton: false,
                  onTap: _showTimestampInRangePicker,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _timestampInToController,
                  labelText: 'Ngày qua trạm: đến ngày*',
                  hintText: 'Đến ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  hasClearButton: false,
                  onTap: _showTimestampInRangePicker,
                ),
                SizedBox(height: 34.h),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: SecondaryButton(
                          onTap: () {
                            context
                                .read<TransactionVehicleBloc>()
                                .add(const TransactionVehicleFetched());
                            context.read<TransactionVehicleBloc>().isSearching =
                                false;
                            Navigator.of(context).pop();
                          },
                          title: 'Đặt lại',
                        ),
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: PrimaryButton(
                          title: 'Tìm kiếm',
                          onTap: () {
                            context.read<TransactionVehicleBloc>().add(
                                  TransactionVehicleSearched(
                                    plateNumber: _plateNumberController.text,
                                    timestampInFrom: _timestampInFrom,
                                    timestampInTo: _timestampInTo,
                                  ),
                                );
                            context.read<TransactionVehicleBloc>().isSearching =
                                true;
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // SizedBox(height: 16.h),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _showTimestampInRangePicker() async {
    final initialDateRange = DateTimeRange(
      start: _timestampInFrom,
      end: _timestampInTo,
    );

    final dateRange = await showDateRangePicker(
          context: context,
          locale: const Locale('vi'),
          initialDateRange: initialDateRange,
          firstDate: Jiffy().subtract(years: 2).startOf(Units.DAY).dateTime,
          lastDate: Jiffy().dateTime,
          initialEntryMode: DatePickerEntryMode.calendarOnly,
        ) ??
        initialDateRange;

    _timestampInFrom = dateRange.start;
    _timestampInTo = dateRange.end;

    _timestampInFromController.text =
        Jiffy(_timestampInFrom).format('dd/MM/yyyy');
    _timestampInToController.text = Jiffy(_timestampInTo).format('dd/MM/yyyy');
  }
}
