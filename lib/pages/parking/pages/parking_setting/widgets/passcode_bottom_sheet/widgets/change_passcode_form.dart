import 'package:epass/commons/validators/passcode_validators.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/textfield/password_text_field.dart';
import 'package:epass/pages/parking/pages/parking_setting/bloc/parking_setting_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/passcode_bottom_sheet/bloc/passcode_bottom_sheet_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChangePasscodeForm extends StatefulWidget {
  final ParkingSettingBloc parkingBloc;
  final bool isCreatePassCode;

  const ChangePasscodeForm({Key? key, required this.parkingBloc, required this.isCreatePassCode}) : super(key: key);

  @override
  State<ChangePasscodeForm> createState() => _ChangePasscodeFormState();
}

class _ChangePasscodeFormState extends State<ChangePasscodeForm> with PasscodeValidator{

  final _formKey = GlobalKey<FormState>();
  final _newPasscodeController = TextEditingController();
  final _confirmPasscodeController = TextEditingController();

  @override
  void dispose() {
    _newPasscodeController.dispose();
    _confirmPasscodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PasscodeBottomSheetBloc, PasscodeBottomSheetState>(
      listener: (context, state) {
        // TODO: passcode change
      },
      builder: (context, state) {
        return Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 16.h),
              PasswordTextField(
                inputType: TextInputType.number,
                controller: _newPasscodeController,
                maxLength: 6,
                labelText: 'Mật khẩu (6 chữ số) *',
                hintText: 'Mật khẩu (6 chữ số) *',
                obscureText: true,
                autofocus: true,
                enabled: state is! ChangePasscodeFormSubmittedInProgress,
                validator: passcodeValidator,
              ),
              SizedBox(height: 16.h),
              PasswordTextField(
                inputType: TextInputType.number,
                controller: _confirmPasscodeController,
                maxLength: 6,
                labelText: 'Nhập lại mật khẩu *',
                hintText: 'Nhập lại mật khẩu *',
                obscureText: true,
                enabled: state is! ChangePasscodeFormSubmittedInProgress,
                validator: (confirmPassword) {
                  if (confirmPassword == null || confirmPassword.isEmpty) {
                    return 'Vui lòng nhập Xác nhận mật khẩu';
                  } else if (confirmPassword != _newPasscodeController.text) {
                    return 'Xác nhận mật khẩu không khớp với mật khẩu mới';
                  } else {
                    return null;
                  }
                },
              ),
              SizedBox(height: 32.h),
              SizedBox(
                height: 56.h,
                width: double.infinity,
                child: LoadingPrimaryButton(
                  title: widget.isCreatePassCode ? 'Tạo mật khẩu' : 'Đổi mật khẩu',
                  isLoading: state is ChangePasscodeFormSubmittedInProgress,
                  onTap: state is! ChangePasscodeFormSubmittedInProgress
                      ? () {
                    FocusScope.of(context).unfocus();
                    if (_formKey.currentState!.validate()) {
                      widget.parkingBloc.add(PassCodeSubmitted(passCode: _newPasscodeController.text,
                        confirmType: widget.isCreatePassCode ? 19 : 18));
                      Navigator.of(context).pop();

                    }
                  } : null,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
