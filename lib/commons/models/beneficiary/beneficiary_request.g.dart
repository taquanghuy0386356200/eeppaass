// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beneficiary_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BeneficiaryRequest _$BeneficiaryRequestFromJson(Map<String, dynamic> json) =>
    BeneficiaryRequest(
      contractNo: json['contractNo'] as String,
      customerName: json['customerName'] as String,
      reminiscentName: json['reminiscentName'] as String,
    );

Map<String, dynamic> _$BeneficiaryRequestToJson(BeneficiaryRequest instance) =>
    <String, dynamic>{
      'contractNo': instance.contractNo,
      'customerName': instance.customerName,
      'reminiscentName': instance.reminiscentName,
    };

BeneficiaryRequestUpdate _$BeneficiaryRequestUpdateFromJson(
        Map<String, dynamic> json) =>
    BeneficiaryRequestUpdate(
      beneficiaryInformationId: json['beneficiaryInformationId'] as int,
      reminiscentName: json['reminiscentName'] as String,
    );

Map<String, dynamic> _$BeneficiaryRequestUpdateToJson(
        BeneficiaryRequestUpdate instance) =>
    <String, dynamic>{
      'beneficiaryInformationId': instance.beneficiaryInformationId,
      'reminiscentName': instance.reminiscentName,
    };
