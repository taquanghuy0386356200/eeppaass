// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ekyc_image_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EkycImageRequest _$EkycImageRequestFromJson(Map<String, dynamic> json) =>
    EkycImageRequest(
      image_id: json['image_id'] as String?,
    );

Map<String, dynamic> _$EkycImageRequestToJson(EkycImageRequest instance) =>
    <String, dynamic>{
      'image_id': instance.image_id,
    };
