import 'package:dio/dio.dart';
import 'package:epass/commons/models/text_to_speech/text_to_speech_request.dart';
import 'package:retrofit/retrofit.dart';
import 'package:retrofit/http.dart' as http;

part 'text_to_speech_client.g.dart';

@RestApi()
abstract class TextToSpeechClient {
  factory TextToSpeechClient(
    Dio dio, {
    String baseUrl,
  }) = _TextToSpeechClient;

  @POST('/voice/api/tts/v1/rest/syn')
  @http.Headers({
    'Accept': 'application/json',
  })
  @DioResponseType(ResponseType.bytes)
  Future<HttpResponse<List<int>>> getSpeech({
    @Header('token') required String token,
    @Body() required TextToSpeechRequest request,
  });
}
