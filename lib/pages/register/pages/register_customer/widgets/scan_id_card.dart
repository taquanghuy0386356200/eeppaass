import 'dart:io';

import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/register/bloc/register_paper_scan/register_paper_scan_permission_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScanIDCard extends StatelessWidget {
  final bool isFront;
  final String? front;
  final String? back;

  const ScanIDCard({
    Key? key,
    required this.isFront,
    this.front,
    this.back,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(12.0),
      onTap: () =>
          context.read<RegisterPaperScanPermissionBloc>().add(RegisterPaperScanPermissionRequested(isFront: isFront)),
      child: SizedBox(
        height: 150.h,
        width: 240.w,
        child: Stack(
          children: [
            isFront
                ? front != null
                    ? Image.file(File(front!))
                    : Assets.images.idCard.front.image()
                : back != null
                    ? Image.file(File(back!))
                    : Assets.images.idCard.back.image(),
            if (front == null && back == null)
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 6.h),
                  child: Text(
                    'Giấy tờ cá nhân'.toUpperCase(),
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12.sp,
                        ),
                  ),
                ),
              ),
            Container(
              decoration: BoxDecoration(
                color: ColorName.textGray1.withOpacity(0.6),
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                isFront ? 'Chụp ảnh mặt trước' : 'Chụp ảnh mặt sau',
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
