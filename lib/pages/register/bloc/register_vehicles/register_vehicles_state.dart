part of 'register_vehicles_bloc.dart';

class VehicleInfo extends Equatable {
  final String? owner;
  final String? plateNumber;
  final CategoryModel? plateType;
  final CategoryModel? vehicleType;

  const VehicleInfo({
    this.owner,
    this.plateNumber,
    this.plateType,
    this.vehicleType,
  });

  @override
  List<Object?> get props => [owner, plateNumber, plateType, vehicleType];

  VehicleInfo copyWith({
    String? owner,
    String? plateNumber,
    CategoryModel? plateType,
    CategoryModel? vehicleType,
  }) {
    return VehicleInfo(
      owner: owner ?? this.owner,
      plateNumber: plateNumber ?? this.plateNumber,
      plateType: plateType ?? this.plateType,
      vehicleType: vehicleType ?? this.vehicleType,
    );
  }
}

class AddressInfo extends Equatable {
  final String? regCertImage;
  final String? vehicleRegCertImage;
  final String? phoneNumber;
  final String? email;
  final String? addressDetails;
  final AddressVTT? province;
  final AddressVTT? district;
  final AddressVTT? ward;
  final bool? reuseAddress;
  final RegisterLevel? registerLevel;
  final bool? reuseTimeFrame;

  const AddressInfo({
    this.regCertImage,
    this.vehicleRegCertImage,
    this.phoneNumber,
    this.email,
    this.addressDetails,
    this.province,
    this.district,
    this.ward,
    this.reuseAddress,
    this.registerLevel = RegisterLevel.normal,
    this.reuseTimeFrame,
  });

  @override
  List<Object?> get props => [
        regCertImage,
        vehicleRegCertImage,
        phoneNumber,
        email,
        addressDetails,
        province,
        district,
        ward,
        reuseAddress,
        registerLevel,
        reuseTimeFrame,
      ];

  AddressInfo copyWith({
    required String? regCertImage,
    required String? vehicleRegCertImage,
    required String? phoneNumber,
    required String? email,
    required String? addressDetails,
    required AddressVTT? noticeProvince,
    required AddressVTT? noticeDistrict,
    required AddressVTT? noticeWard,
    bool? reuseAddress,
    required RegisterLevel? timeFrame,
    bool? reuseTimeFrame,
  }) {
    return AddressInfo(
      regCertImage: regCertImage,
      vehicleRegCertImage: vehicleRegCertImage,
      phoneNumber: phoneNumber,
      email: email,
      addressDetails: addressDetails,
      province: noticeProvince,
      district: noticeDistrict,
      ward: noticeWard,
      reuseAddress: reuseAddress ?? this.reuseAddress,
      registerLevel: timeFrame,
      reuseTimeFrame: reuseTimeFrame ?? this.reuseTimeFrame,
    );
  }
}

class RegisterVehicleItem extends Equatable {
  final VehicleInfo vehicleInfo;
  final AddressInfo addressInfo;

  const RegisterVehicleItem({
    required this.vehicleInfo,
    required this.addressInfo,
  });

  @override
  List<Object> get props => [vehicleInfo, addressInfo];

  RegisterVehicleItem copyWith({
    VehicleInfo? vehicleInfo,
    AddressInfo? addressInfo,
  }) {
    return RegisterVehicleItem(
      vehicleInfo: vehicleInfo ?? this.vehicleInfo,
      addressInfo: addressInfo ?? this.addressInfo,
    );
  }
}

class RegisterVehiclesState extends Equatable {
  final List<RegisterVehicleItem> listRegisterVehicle;

  const RegisterVehiclesState({required this.listRegisterVehicle});

  @override
  List<Object> get props => [listRegisterVehicle];

  RegisterVehiclesState copyWith({
    List<RegisterVehicleItem>? listRegisterVehicle,
  }) {
    return RegisterVehiclesState(
      listRegisterVehicle: listRegisterVehicle ?? this.listRegisterVehicle,
    );
  }
}
