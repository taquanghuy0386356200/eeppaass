import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:epass/commons/widgets/animations/shimmer_widget.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/cash_in/pages/cash_in_fee/bloc/cash_in_fee_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'widget/cash_in_fee_card_update.dart';

class CashInFeePage extends StatelessWidget {
  const CashInFeePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: 'Biểu phí giao dịch',
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          const GradientHeaderContainer(),
          SafeArea(
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: RoundedTopContainer(
                margin: EdgeInsets.only(top: 16.h),
                child: BlocProvider(
                  create: (context) => CashInFeeBloc(
                    cashInRepository: getIt<ICashInRepository>(),
                  )..add(CashInFeeFetched()),
                  child: BlocConsumer<CashInFeeBloc, CashInFeeState>(
                    listener: (context, state) {
                      if (state is CashInFeeFetchedFailure) {
                        showErrorSnackBBar(
                          context: context,
                          message: state.message,
                        );
                      }
                    },
                    builder: (context, state) {
                      if (state is CashInFeeFetchedSuccess) {
                        final fees = state.fees;

                        return fees.isNotEmpty
                            ?
                        //code cũ
                        // ListView.separated(
                        //   shrinkWrap: true,
                        //   physics: const NeverScrollableScrollPhysics(),
                        //   padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                        //   itemBuilder: (context, index) => TopupFeeCard(topupFee: fees[index]),
                        //   separatorBuilder: (context, index) => SizedBox(height: 20.h),
                        //   itemCount: fees.length,
                        // )
                        Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  ListView.separated(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemBuilder: (context, index) =>
                                        CashInFeeCardUpdate(
                                      topupFee: fees[index],
                                    ),
                                    separatorBuilder: (context, index) =>
                                        Container(
                                      height: 4.h,
                                      color: const Color(0xFFFEF5F3).withOpacity(0.5),
                                    ),
                                    itemCount: fees.length,
                                  ),
                                ],
                              )
                            : NoDataPage(
                                onTap: () => context
                                    .read<CashInFeeBloc>()
                                    .add(CashInFeeFetched()),
                              );
                      } else if (state is CashInFeeFetchedInProgress) {
                        return ShimmerColor(
                          child: ListView.separated(
                            padding:
                                EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) => ShimmerWidget(
                              height: index < 2 ? 160.h : 100.h,
                              width: double.infinity,
                            ),
                            separatorBuilder: (context, index) =>
                                SizedBox(height: 20.h),
                            itemCount: 4,
                          ),
                        );
                      } else if (state is CashInFeeFetchedFailure) {
                        return CommonErrorPage(
                          message: state.message,
                          onTap: () => context
                              .read<CashInFeeBloc>()
                              .add(CashInFeeFetched()),
                        );
                      } else {
                        return const SizedBox.shrink();
                      }
                    },
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
