import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/report/pages/create_report/bloc/create_report_bloc.dart';
import 'package:epass/pages/report/pages/create_report/bloc/create_report_otp_bloc.dart';
import 'package:epass/pages/report/pages/create_report/widgets/report_form/bloc/report_form_bloc.dart';
import 'package:epass/pages/report/pages/create_report/widgets/otp_countdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class CreateReportOTPPage extends StatefulWidget {
  const CreateReportOTPPage({
    Key? key,
  }) : super(key: key);

  @override
  State<CreateReportOTPPage> createState() => _CreateReportOTPPageState();
}

class _CreateReportOTPPageState extends State<CreateReportOTPPage> {
  final _errorController = StreamController<ErrorAnimationType>();

  @override
  void deactivate() {
    context.read<CreateReportOtpBloc>().add(const ReportOtpPaused());
    super.deactivate();
  }

  @override
  void dispose() {
    _errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final phoneNumber = context.read<AppBloc>().state.user?.noticePhoneNumber;

    return Builder(builder: (context) {
      return BlocListener<CreateReportBloc, CreateReportState>(
        listener: (context, state) {
          if (state is CreateReportFormSubmittedInProgress) {
            context.loaderOverlay.show();
          } else {
            context.loaderOverlay.hide();
          }
        },
        child: BasePage(
          backgroundColor: Colors.white,
          title: 'Gửi báo lỗi dịch vụ',
          resizeToAvoidBottomInset: false,
          child: Stack(
            children: [
              const GradientHeaderContainer(),
              SafeArea(
                child: FadeAnimation(
                  delay: 1,
                  direction: FadeDirection.up,
                  child: RoundedTopContainer(
                    padding: EdgeInsets.fromLTRB(16.w, 48.h, 16.w, 0.h),
                    child: BlocConsumer<CreateReportBloc, CreateReportState>(
                      listener: (context, state) {
                        if (state is CreateReportFormSubmittedInProgress) {
                          context.loaderOverlay.show();
                        } else if (state is CreateReportFormSubmittedSuccess) {
                          context.loaderOverlay.hide();
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (dialogContext) {
                              return SuccessDialog(
                                content: 'Báo lỗi dịch vụ của quý khách đã được ePass tiếp nhận. '
                                    'Bạn có thể xem lại báo lỗi tại mục Theo dõi báo lỗi',
                                buttonTitle: 'Quay lại',
                                onButtonTap: () async {
                                  Navigator.of(dialogContext).pop();
                                  Navigator.of(context).pop();
                                  context.navigateTo(const SupportRoute());
                                },
                              );
                            },
                          );
                        } else if (state is CreateReportFormSubmittedFailure) {
                          context.loaderOverlay.hide();
                          _errorController.add(ErrorAnimationType.shake);
                        }
                      },
                      builder: (context, state) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: double.infinity,
                              child: Text(
                                'Xác nhận',
                                style: Theme.of(context).textTheme.headline6!.copyWith(
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(height: 24.h),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.w),
                              child: RichText(
                                text: TextSpan(
                                  text: 'Nhập mã OTP đã được gửi đến số điện thoại ',
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: '${phoneNumber ?? ''}.',
                                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                textAlign: TextAlign.center,
                                maxLines: 2,
                              ),
                            ),
                            SizedBox(height: 24.h),
                            PinCodeTextField(
                              appContext: context,
                              errorAnimationController: _errorController,
                              length: 6,
                              obscureText: false,
                              animationType: AnimationType.fade,
                              pinTheme: PinTheme(
                                shape: PinCodeFieldShape.box,
                                fieldHeight: 46.r,
                                fieldWidth: 46.r,
                                borderRadius: BorderRadius.circular(8.0),
                                errorBorderColor: ColorName.error,
                                borderWidth: 1.0,
                                activeColor: ColorName.disabledBorderColor,
                                activeFillColor: Colors.white,
                                inactiveColor: ColorName.disabledBorderColor,
                                inactiveFillColor: Colors.white,
                                selectedColor: ColorName.disabledBorderColor,
                                selectedFillColor: Colors.white,
                              ),
                              backgroundColor: Colors.transparent,
                              autoDismissKeyboard: true,
                              autoFocus: true,
                              keyboardType: TextInputType.number,
                              textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                                fontSize: 22.sp,
                                fontWeight: FontWeight.w600,
                              ),
                              onCompleted: _onOTPCompleted,
                              onChanged: (value) {},
                              beforeTextPaste: (text) {
                                //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                return false;
                              },
                            ),
                            if (state is CreateReportFormSubmittedFailure) SizedBox(height: 8.h),
                            if (state is CreateReportFormSubmittedFailure)
                              Text(
                                state.message,
                                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                  color: ColorName.error,
                                ),
                              ),
                            SizedBox(height: 20.h),
                            BlocSelector<CreateReportOtpBloc, CreateReportOtpState,
                                CreateReportOtpRequestedSuccess?>(
                              selector: (state) => state is CreateReportOtpRequestedSuccess ? state : null,
                              builder: (context, state) {
                                return state != null
                                    ? OTPCountdown(
                                  otpSubmittedTime: state.submittedTime,
                                )
                                    : const SizedBox.shrink();
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  void _onOTPCompleted(String otp) {
    FocusScope.of(context).unfocus();

    final reportFormState = context.read<ReportFormBloc>().state;
    final filePickerState = context.read<FilePickerBloc>().state;


    final groupReportState = reportFormState.groupReportState;
    final locationInfo = reportFormState.locationInfo;
    final areaCode = locationInfo.ward != null ? locationInfo.ward?.areaCode :
        locationInfo.district != null ? locationInfo.district?.areaCode : locationInfo.province?.areaCode;

    context.read<CreateReportBloc>().add(CreateReportFormSubmitted(
      reporterName: reportFormState.reporterName,
      email: reportFormState.email,
      plateNumber: reportFormState.plateNumber,
      plateTypeCode: reportFormState.plateTypeCode,
      location: reportFormState.location,
      priorityId: '1',
      l1TicketTypeId: groupReportState.l1TicketType?.ticketTypeId.toString() ?? '',
      l2TicketTypeId: groupReportState.l2TicketType?.ticketTypeId.toString() ?? '',
      l3TicketTypeId: groupReportState.l3TicketType?.ticketTypeId.toString(),
      provinceName: locationInfo.province?.name ?? '',
      districtName: locationInfo.district?.name,
      communeName: locationInfo.ward?.name,
      areaCode: areaCode ?? '',
      stationId: reportFormState.stationId,
      stationName: reportFormState.stationId != null ? reportFormState.stationStageName : null,
      stageId: reportFormState.stageId,
      stageName: reportFormState.stageId != null ? reportFormState.stationStageName : null,
      contentReceive: reportFormState.contentReceive,
      supportInfo: reportFormState.supportInfo,
      ticketKind: reportFormState.ticketKind,
      filePaths: filePickerState.filePaths,
      phoneContact: reportFormState.phoneContact,
      otp: otp,
    ));
  }
}
