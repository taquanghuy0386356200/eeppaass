part of 'mb_pay_gate_bloc.dart';

abstract class MBPayGateEvent extends Equatable {
  const MBPayGateEvent();
}

class MBGetTokenCompleted extends MBPayGateEvent {
  final String grantType;

  const MBGetTokenCompleted({
    required this.grantType,
  });

  @override
  List<Object> get props => [grantType];
}

class MBCreateOrderCompleted extends MBPayGateEvent {
  final String amount;
  final String transactionId;
  final String token;

  const MBCreateOrderCompleted({
    required this.amount,
    required this.transactionId,
    required this.token,
  });

  @override
  List<Object> get props => [amount, transactionId, token];
}

class MBCheckSumCompleted extends MBPayGateEvent {
  final String checkSumKey;
  final String token;

  const MBCheckSumCompleted({
    required this.checkSumKey,
    required this.token,
  });

  @override
  List<Object> get props => [checkSumKey, token];
}

class MBCheckPaymentResultCompleted extends MBPayGateEvent {
  final String transactionId;
  final String token;

  const MBCheckPaymentResultCompleted({
    required this.transactionId,
    required this.token,
  });

  @override
  List<Object> get props => [transactionId, token];
}
