import 'dart:convert';

import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/utility/vietmap/link_vietmap/link_viet_map_bloc.dart';
import 'package:epass/pages/utility/vietmap/vietmap_products/viet_map_product_bloc.dart';
import 'package:epass/pages/utility/widgets/vietmap_product_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:vietmap_ecommerce_flutter_plugin/vietmap_ecommerce_flutter_plugin.dart';

import '../../../../commons/models/create_vietmap_transaction/create_vietmap_transaction_request.dart';
import '../../../../commons/widgets/modal/confirm_dialog.dart';
import '../../../../commons/widgets/modal/success_dialog.dart';

class VietMapProductPage extends StatefulWidget {
  const VietMapProductPage({Key? key}) : super(key: key);

  @override
  State<VietMapProductPage> createState() => _VietMapProductPageState();
}

class _VietMapProductPageState extends State<VietMapProductPage> {
  late RefreshController _refreshController;
  JSCommunicator? communicator;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
    context.read<VietMapProductBloc>().add(const VietMapProductFetched());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<VietMapProductBloc, VietMapProductState>(
      listener: (context, state) {
        if (state.isLoading) {
          context.loaderOverlay.show();
        } else {
          context.loaderOverlay.hide();
        }
        if (state.isPaymentSuccess == true){
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (dialogContext) {
              return SuccessDialog(
                title: 'Thanh toán thành công',
                content: 'Bạn vui lòng kiểm tra thông báo để kích hoạt mã',
                contentTextAlign: TextAlign.center,
                buttonTitle: 'Về trang chủ',
                onButtonTap: () {
                  Navigator.of(dialogContext).pop();
                  Navigator.of(context).pop();
                },
              );
            },
          );
        }
        if (state.isPaymentSuccess == false){
          showErrorDialog();
        }
      },
      builder: (context, state) {
        final listData = state.listData;
        return listData.isEmpty
            ? NoDataPage(
                onTap: () => context
                    .read<VietMapProductBloc>()
                    .add(const VietMapProductFetched()),
              )
            : Expanded(
                child: GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: listData.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 0.w,
                  mainAxisSpacing: 0.h,
                  childAspectRatio: 0.8,
                ),
                itemBuilder: (context, index) {
                  return VietMapProductCard(
                    onTap: () {
                      VietmapEcommerceFlutterPlugin.start(
                        context,
                        onLoginRequired: () {
                          final linkVietMapState =
                              context.read<LinkVietMapBloc>().state;
                          if (linkVietMapState is LinkedVietMap) {
                            communicator?.updateAuthInfo(
                              accessToken: linkVietMapState.token ?? '',
                              phoneNumber: linkVietMapState.phone ?? '',
                              customerName: linkVietMapState.name ?? '',
                            );
                          }
                        },
                        onCreated: (JSCommunicator communicator) {
                          this.communicator = communicator;
                          final linkVietMapState =
                              context.read<LinkVietMapBloc>().state;
                          if (linkVietMapState is LinkedVietMap) {
                            communicator.updateAuthInfo(
                              accessToken:linkVietMapState.token ?? '',
                              phoneNumber: linkVietMapState.phone ?? '',
                              customerName: linkVietMapState.name ?? '',
                            );
                          }
                          communicator.openProductDetailScreen(
                              productId: listData[index].id ?? '');
                        },
                        ecommerceMethodCallHander:
                            (VIETMAPJsMethod method) async {
                          switch (method.name) {
                            case 'authInfoUpdated':
                              final data = jsonDecode(method.params as String)
                                      .cast<String, dynamic>()
                                  as Map<String, dynamic>;
                              final accessToken = data['accessToken'];
                              final phoneNumber = data['phoneNumber'];
                              final customerName = data['customerName'] ?? '';
                              context.read<LinkVietMapBloc>().add(LinkVietMap(
                                  accountName: customerName,
                                  phoneNumber: phoneNumber,
                                  token: accessToken));
                              break;
                            case 'orderCreated':
                              final data = jsonDecode(method.params as String)
                                      .cast<String, dynamic>()
                                  as Map<String, dynamic>;
                              final orderId = data['orderId'];
                              final paymentId = data['paymentId'];

                              print(data);

                              context.loaderOverlay.show();
                              final userRepository = getIt<IUserRepository>();
                              final request = CreateVietMapTransactionRequest(
                                  refTransactionId: orderId,
                                  expireDate: '',
                                  price: listData[index].price);
                              final response = await userRepository
                                  .createVietMapTransaction(request: request);
                              context.loaderOverlay.hide();
                              response.when(
                                success: (response) {
                                },
                                failure: (failure) {
                                  Navigator.of(context).pop();
                                  showErrorDialog();
                                },
                              );
                              break;
                          }
                        },
                        onOrderResult: (OrderResultEntity orderResult) {
                          if (orderResult.sucess) {
                            context.read<VietMapProductBloc>().add(
                                UpdateVietMapTransaction(
                                    transactionId: orderResult.orderId));
                          }else{
                            showErrorDialog();
                          }
                        },
                        onOrderStatusScreenClosed:
                            (OrderResultEntity orderResult) {
                          if (orderResult.sucess) {
                            context.read<VietMapProductBloc>().add(
                                UpdateVietMapTransaction(
                                    transactionId: orderResult.orderId));
                          }else{
                            showErrorDialog();
                          }
                        },
                        onCustomerNameChanged: (String newName) {},
                      );
                    },
                    item: listData[index],
                  );
                },
              ));
      },
    );
  }

  void showErrorDialog(){
    showDialog(
      context: context,
      builder: (dialogContext) {
        return ConfirmDialog(
          title: 'Thanh toán thất bại',
          content: 'Có lỗi xảy ra trong quá trình đặt hàng. Mong bạn thông cảm và thử lại sau',
          contentTextAlign: TextAlign.center,
          primaryButtonTitle: 'Quay lại',
          hasSecondaryButton: false,
          onPrimaryButtonTap: () {
            Navigator.of(dialogContext).pop();
          },
        );
      },
    );
  }
}
