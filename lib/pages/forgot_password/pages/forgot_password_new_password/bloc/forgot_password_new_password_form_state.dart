part of 'forgot_password_new_password_form_bloc.dart';

class ForgotPasswordNewPasswordFormState {
  final String? newPassword;

  const ForgotPasswordNewPasswordFormState({this.newPassword});
}
