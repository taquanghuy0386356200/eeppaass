import 'dart:convert';

import 'package:epass/commons/models/mb_bank/mb_check_sum.dart';
import 'package:epass/flavors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

class MBCheckSumService {
  Future<MBCheckSumResponse?> checkSum(
      MBCheckSumRequest request, String token) async {
    try {
      /// url auth token
      String url = F.mbCheckSumUrl + '?checkSumKey=' + request.checkSumKey;

      var uuid = const Uuid();

      var clientMessageId = uuid.v4();

      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
        'clientMessageId': '$clientMessageId',
      };

      var uri = Uri.parse(url);

      var response = await http.get(uri, headers: headers);
      if (kDebugMode) {
        print('Data from MBB: ${response.body}');
      }
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        return MBCheckSumResponse.fromJson(data);
      } else {
        return null;
      }
    } on Exception {
      return null;
    }
  }
}
