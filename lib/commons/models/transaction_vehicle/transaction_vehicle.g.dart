// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_vehicle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionVehicle _$TransactionVehicleFromJson(Map<String, dynamic> json) =>
    TransactionVehicle(
      id: json['id'] as int?,
      ticketId: json['ticketId'] as String?,
      epc: json['epc'] as String?,
      price: json['price'] as int?,
      adjustPaid: json['adjustPaid'] as int?,
      ticketType: json['ticketType'] as int?,
      ticketTypeName: json['ticketTypeName'] as String?,
      isTicketException: json['isTicketException'] as String?,
      exceptionName: json['exceptionName'] as String?,
      plateNumber: json['plateNumber'] as String?,
      vehicleType: json['vehicleType'] as int?,
      vehicleTypeName: json['vehicleTypeName'] as String?,
      sourceTransaction: json['sourceTransaction'] as String?,
      vehiclePriorityType: json['vehiclePriorityType'] as int?,
      stationInId: json['stationInId'] as int?,
      stationInName: json['stationInName'] as String?,
      stationOutId: json['stationOutId'] as int?,
      stationOutName: json['stationOutName'] as String?,
      laneInId: json['laneInId'] as int?,
      laneInName: json['laneInName'] as String?,
      timestampIn: json['timestampIn'] as String?,
      timestampOut: json['timestampOut'] as String?,
      stationType: json['stationType'] as int?,
      stationTypeName: json['stationTypeName'] as String?,
      eventTimestamp: json['eventTimestamp'] as String?,
      createDate: json['createDate'] as String?,
      roamingType: json['roamingType'] as int?,
      roamingTypeStr: json['roamingTypeStr'] as String?,
      tranStatus: json['tranStatus'] as String?,
    );

Map<String, dynamic> _$TransactionVehicleToJson(TransactionVehicle instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ticketId': instance.ticketId,
      'epc': instance.epc,
      'price': instance.price,
      'adjustPaid': instance.adjustPaid,
      'ticketType': instance.ticketType,
      'ticketTypeName': instance.ticketTypeName,
      'isTicketException': instance.isTicketException,
      'exceptionName': instance.exceptionName,
      'plateNumber': instance.plateNumber,
      'vehicleType': instance.vehicleType,
      'vehicleTypeName': instance.vehicleTypeName,
      'sourceTransaction': instance.sourceTransaction,
      'vehiclePriorityType': instance.vehiclePriorityType,
      'stationInId': instance.stationInId,
      'stationInName': instance.stationInName,
      'stationOutId': instance.stationOutId,
      'stationOutName': instance.stationOutName,
      'laneInId': instance.laneInId,
      'laneInName': instance.laneInName,
      'timestampIn': instance.timestampIn,
      'timestampOut': instance.timestampOut,
      'stationType': instance.stationType,
      'stationTypeName': instance.stationTypeName,
      'eventTimestamp': instance.eventTimestamp,
      'createDate': instance.createDate,
      'roamingType': instance.roamingType,
      'roamingTypeStr': instance.roamingTypeStr,
      'tranStatus': instance.tranStatus,
    };

TransactionVehicleData _$TransactionVehicleDataFromJson(
        Map<String, dynamic> json) =>
    TransactionVehicleData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => TransactionVehicle.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$TransactionVehicleDataToJson(
        TransactionVehicleData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

TransactionVehicleResponse _$TransactionVehicleResponseFromJson(
        Map<String, dynamic> json) =>
    TransactionVehicleResponse(
      data: json['data'] == null
          ? null
          : TransactionVehicleData.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TransactionVehicleResponseToJson(
        TransactionVehicleResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
