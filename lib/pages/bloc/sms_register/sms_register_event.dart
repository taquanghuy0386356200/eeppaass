part of 'sms_register_bloc.dart';

abstract class SmsRegisterEvent extends Equatable {
  const SmsRegisterEvent();
}

class CheckUserRegister extends SmsRegisterEvent {
  const CheckUserRegister();

  @override
  List<Object?> get props => [];
}

class SMSVehicleRegistered extends SmsRegisterEvent {
  final String smsRegisCode;
  final bool autoRenew;

  const SMSVehicleRegistered({
    required this.smsRegisCode,
    required this.autoRenew,
  });

  @override
  List<Object> get props => [smsRegisCode, autoRenew];
}

class CheckUserNotRegisterYet extends SmsRegisterEvent {
  const CheckUserNotRegisterYet();

  @override
  List<Object> get props => [];
}

class SMSVehicleRegisterUpdated extends SmsRegisterEvent {
  final int status;
  final bool autoRenew;

  const SMSVehicleRegisterUpdated({
    required this.status,
    required this.autoRenew,
  });

  @override
  List<Object?> get props => [status, autoRenew];
}

class SelectedSMSOption extends SmsRegisterEvent {
  final CategoryModel? selectedSMSOption;

  const SelectedSMSOption({this.selectedSMSOption});

  @override
  List<Object?> get props => [selectedSMSOption];
}

class SmsRegisterDispose extends SmsRegisterEvent {
  @override
  List<Object?> get props => throw UnimplementedError();
}
