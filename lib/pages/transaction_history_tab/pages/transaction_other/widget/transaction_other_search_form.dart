import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_other/bloc/transaction_other_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionOtherSearchForm extends StatefulWidget {
  final String? plateNumber;
  final DateTime? fromDate;
  final DateTime? toDate;

  const TransactionOtherSearchForm({
    Key? key,
    this.plateNumber,
    this.fromDate,
    this.toDate,
  }) : super(key: key);

  @override
  State<TransactionOtherSearchForm> createState() =>
      _TransactionOtherSearchFormState();
}

class _TransactionOtherSearchFormState
    extends State<TransactionOtherSearchForm> {
  final _plateNumberController = TextEditingController();
  final _fromDateController = TextEditingController();
  final _toDateController = TextEditingController();

  DateTime? _fromDate;
  DateTime? _toDate;

  @override
  void initState() {
    super.initState();
    context.read<TransactionOtherBloc>().isSearching = false;
    _plateNumberController.text = widget.plateNumber ?? '';

    _fromDate = widget.fromDate;
    _toDate = widget.toDate;

    if (_fromDate != null) {
      _fromDateController.text = Jiffy(_fromDate).format('dd/MM/yyyy');
    }

    if (_toDate != null) {
      _toDateController.text = Jiffy(_toDate).format('dd/MM/yyyy');
    }
  }

  @override
  void dispose() {
    _plateNumberController.dispose();
    _fromDateController.dispose();
    _toDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Positioned(
            top: 12.h,
            right: 12.w,
            child: SplashIconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: ColorName.borderColor,
              ),
              onTap: () => Navigator.of(context).pop(),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'TÌM KIẾM',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(height: 24.h),
                PrimaryTextField(
                  controller: _plateNumberController,
                  labelText: 'Biển số xe',
                  hintText: 'Biển số xe (00X1234)',
                  isPlateNumber: true,
                  maxLength: 20,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _fromDateController,
                  labelText: 'Ngày giao dịch: từ ngày',
                  hintText: 'Từ ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showTransactionOtherDateRangePicker,
                  onClear: () => _fromDate = null,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _toDateController,
                  labelText: 'Ngày giao dịch: đến ngày',
                  hintText: 'Đến ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showTransactionOtherDateRangePicker,
                  onClear: () => _toDate = null,
                ),
                SizedBox(height: 34.h),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: SecondaryButton(
                          onTap: () {
                            context
                                .read<TransactionOtherBloc>()
                                .add(const TransactionOtherFetched());
                            context.read<TransactionOtherBloc>().isSearching =
                                false;
                            Navigator.of(context).pop();
                          },
                          title: 'Đặt lại',
                        ),
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: PrimaryButton(
                          title: 'Tìm kiếm',
                          onTap: () {
                            context.read<TransactionOtherBloc>().add(
                                  TransactionOtherSearched(
                                    plateNumber: _plateNumberController.text,
                                    fromDate: _fromDate,
                                    toDate: _toDate,
                                  ),
                                );
                            context.read<TransactionOtherBloc>().isSearching =
                                true;
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // SizedBox(height: 16.h),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _showTransactionOtherDateRangePicker() async {
    DateTimeRange? initialDateRange;
    if (_fromDate != null) {
      initialDateRange = DateTimeRange(
        start: _fromDate!,
        end: _toDate ?? Jiffy().dateTime,
      );
    }

    final dateRange = await showDateRangePicker(
          context: context,
          locale: const Locale('vi'),
          initialDateRange: initialDateRange,
          firstDate: Jiffy().subtract(years: 2).startOf(Units.DAY).dateTime,
          lastDate: Jiffy().dateTime,
          initialEntryMode: DatePickerEntryMode.calendarOnly,
        ) ??
        initialDateRange;

    _fromDate = dateRange?.start;
    _toDate = dateRange?.end;

    if (_fromDate != null) {
      _fromDateController.text = Jiffy(_fromDate).format('dd/MM/yyyy');
    }

    if (_toDate != null) {
      _toDateController.text = Jiffy(_toDate).format('dd/MM/yyyy');
    }
  }
}
