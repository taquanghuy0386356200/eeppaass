import 'package:dio/dio.dart';
import 'package:epass/commons/models/transaction_vehicle/transaction_vehicle.dart';
import 'package:retrofit/retrofit.dart';
import 'package:epass/commons/api/endpoint.dart' as endpoint;

part 'reconcil_client.g.dart';

@RestApi()
abstract class ReconcilClient {
  factory ReconcilClient(
    Dio dio, {
    String baseUrl,
  }) = _ReconcilClient;

  /// /api/v1/transactions-vehicles
  @GET(endpoint.transactionVehicles)
  Future<TransactionVehicleResponse> getTransactionVehicle({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,

    /// active = 1, inactive = 2
    @Query('efficiencyId') int? efficiencyId,
    @Query('code') String? plateNumber,

    /// format: dd/MM/yyyy
    @Query('timestampInFrom') String? timestampInFrom,

    /// format: dd/MM/yyyy
    @Query('timestampInTo') String? timestampInTo,
  });
}
