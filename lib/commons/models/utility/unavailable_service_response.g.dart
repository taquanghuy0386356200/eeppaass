// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unavailable_service_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnavailableServiceData _$UnavailableServiceDataFromJson(
        Map<String, dynamic> json) =>
    UnavailableServiceData(
      serviceCanNotAccess: (json['serviceCanNotAccess'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$UnavailableServiceDataToJson(
        UnavailableServiceData instance) =>
    <String, dynamic>{
      'serviceCanNotAccess': instance.serviceCanNotAccess,
    };

UnavailableServiceResponse _$UnavailableServiceResponseFromJson(
        Map<String, dynamic> json) =>
    UnavailableServiceResponse(
      data: json['data'] == null
          ? null
          : UnavailableServiceData.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UnavailableServiceResponseToJson(
        UnavailableServiceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
