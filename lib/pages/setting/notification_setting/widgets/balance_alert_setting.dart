import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/setting/notification_setting/bloc/balance_alert_bloc/balance_alert_bloc.dart';
import 'package:epass/pages/setting/notification_setting/widgets/balance_alert_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:epass/commons/extensions/number_ext.dart';

class BalanceAlertSetting extends StatelessWidget {
  const BalanceAlertSetting({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<BalanceAlertBloc, BalanceAlertState>(
      listener: (context, state) {
        if (state is BalanceAlertUpdatedInProgress) {
          context.loaderOverlay.show();
        } else if (state is BalanceAlertUpdatedFailure) {
          context.loaderOverlay.hide();
          showErrorSnackBBar(context: context, message: state.message);
        } else if (state is BalanceAlertUpdatedSuccess) {
          context.loaderOverlay.hide();
          showSuccessSnackBBar(context: context, message: 'Thành công');
        }
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(16.w, 20.h, 17.w, 10.h),
        child: BlocSelector<AppBloc, AppState, User?>(
          selector: (state) => state.user,
          builder: (context, user) {
            final isAlertMoneyOn = (user?.isAlertMoney ?? 0) == 1;
            final alertMoney = user?.alertMoney;

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Cảnh báo số dư tối thiểu',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    const Spacer(),
                    CupertinoSwitch(
                      value: isAlertMoneyOn,
                      activeColor: ColorName.primaryColor,
                      onChanged: (isOn) async {
                        if (isOn) {
                          await showDialog(
                            context: context,
                            builder: (dialogContext) {
                              return BlocProvider.value(
                                value: context.read<BalanceAlertBloc>(),
                                child: BalanceAlertDialog(amount: alertMoney),
                              );
                            },
                          );
                        } else {
                          context.read<BalanceAlertBloc>().add(BalanceAlertUpdated(isBalanceAlertOn: isOn));
                        }
                      },
                    ),
                  ],
                ),
                if (alertMoney != null)
                  Row(
                    children: [
                      Text(
                        'Số dư tối thiểu:',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      TextButton(
                        onPressed: () async {
                          await showDialog(
                            context: context,
                            builder: (dialogContext) {
                              return BlocProvider.value(
                                value: context.read<BalanceAlertBloc>(),
                                child: BalanceAlertDialog(amount: alertMoney),
                              );
                            },
                          );
                        },
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: alertMoney.formatCurrency(symbol: 'đ'),
                                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                      color: ColorName.linkBlue,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                              WidgetSpan(
                                child: Assets.icons.pencil.svg(
                                  color: ColorName.linkBlue,
                                  height: 18.r,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                if (alertMoney == null) SizedBox(height: 10.h),
              ],
            );
          },
        ),
      ),
    );
  }
}


