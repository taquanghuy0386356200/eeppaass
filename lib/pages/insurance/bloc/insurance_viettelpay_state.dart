part of 'insurance_viettelpay_bloc.dart';

@immutable
abstract class InsuranceViettelPayState extends Equatable {
  const InsuranceViettelPayState();
}

class InsuranceViettelPayInitial extends InsuranceViettelPayState {
  @override
  List<Object> get props => [];
}

class CountInsuranceFetchedInProgress extends InsuranceViettelPayState {
  const CountInsuranceFetchedInProgress();

  @override
  List<Object> get props => [];
}

class CountInsuranceFetchedFailure extends InsuranceViettelPayState {
  final String message;

  const CountInsuranceFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CountInsuranceFetchedSuccess extends InsuranceViettelPayState {
  final int count;

  const CountInsuranceFetchedSuccess(this.count);

  @override
  List<Object> get props => [count];
}

class InsuranceServiceAccepted extends CountInsuranceFetchedSuccess {
  const InsuranceServiceAccepted(int count) : super(count);
}

class InsuranceServiceAccessRequested extends CountInsuranceFetchedSuccess {
  const InsuranceServiceAccessRequested() : super(0);
}
