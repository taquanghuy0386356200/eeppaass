part of 'cash_in_target_bloc.dart';

@immutable
class CashInTargetState extends Equatable {
  final CashInFor cashInFor;
  final String? contractNo;
  final String? contractId;
  final String target;
  final String? userName;
  final String? error;
  final bool isLoading;

  String? get helperText {
    String text = '';

    if (cashInFor == CashInFor.other) {
      if (contractNo != null) {
        text += 'Số hợp đồng: $contractNo';
      }

      if (userName != null) {
        text += '\nChủ hợp đồng: $userName';
      }
    }
    return text.isEmpty ? null : text;
  }

  const CashInTargetState({
    required this.cashInFor,
    this.contractNo,
    this.contractId,
    required this.target,
    this.userName,
    this.error,
    this.isLoading = false,
  });

  @override
  List<Object?> get props => [cashInFor, contractNo, target, error, isLoading];

  @override
  bool? get stringify => true;

  CashInTargetState copyWith({
    CashInFor? cashInFor,
    String? contractNo,
    String? contractId,
    String? target,
    String? userName,
    String? error,
    bool? isLoading,
  }) {
    return CashInTargetState(
      cashInFor: cashInFor ?? this.cashInFor,
      contractNo: contractNo ?? this.contractNo,
      contractId: contractId ?? this.contractId,
      target: target ?? this.target,
      userName: userName ?? this.userName,
      error: error,
      isLoading: isLoading ?? this.isLoading,
    );
  }
}
