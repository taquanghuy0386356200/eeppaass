import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/models/buy_ticket/buy_ticket_request.dart';
import 'package:epass/commons/models/buy_ticket/buy_ticket_response.dart';
import 'package:epass/commons/models/buy_ticket/cart.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_request.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_response.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

abstract class IBuyTicketRepository {
  Future<Result<List<TicketPrice>, Failure>> getTicketPrices({
    required List<Vehicle> vehicles,
    required ServicePlanType servicePlanType,
    required DateTime effectiveDate,
    required DateTime expireDate,
    required StationStage stationStage,
  });

  Future<Result<BuyTicketResponseDataNotNull, Failure>> buyTickets({
    required String customerId,
    required String contractId,
    required List<CartItem> cartItems,
  });
}

class BuyTicketRepository implements IBuyTicketRepository {
  final CrmClient _crmClient;

  BuyTicketRepository({
    required CrmClient crmClient,
  }) : _crmClient = crmClient;

  @override
  Future<Result<List<TicketPrice>, Failure>> getTicketPrices({
    required List<Vehicle> vehicles,
    required ServicePlanType servicePlanType,
    required DateTime effectiveDate,
    required DateTime expireDate,
    required StationStage stationStage,
  }) async {
    try {
      final ticketPriceRequests = vehicles
          .map(
            (vehicle) => TicketPriceRequest(
              plateNumber: vehicle.plateNumber,
              vehicleGroupId: vehicle.vehicleGroupId,
              epc: vehicle.epc,
              seatNumber: vehicle.seatNumber,
              vehicleId: vehicle.vehicleId,
              vehicleTypeId: vehicle.vehicleTypeId,
              cargoWeight: vehicle.cargoWeight,
              netWeight: vehicle.netWeight,

              // autoRenew: 1
              booCode: stationStage.booCode,
              chargeMethodId: stationStage.methodChargeId,
              stationId: stationStage.stationId,
              stageId: stationStage.stageId,
              stationType: stationStage.stationType,

              effDate: Jiffy(effectiveDate).format('dd/MM/yyyy'),
              expDate: Jiffy(expireDate).format('dd/MM/yyyy'),

              servicePlanTypeId: servicePlanType,
              // fixed value

              quantity: 1,
              status: 2,
            ),
          )
          .toList();

      final response = await _crmClient.getTicketPrices(
        request: TicketPriceListRequest(ticketPriceList: ticketPriceRequests),
      );

      final ticketPrices = response.data?.listServicePlan;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || ticketPrices == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(ticketPrices);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<BuyTicketResponseDataNotNull, Failure>> buyTickets({
    required String customerId,
    required String contractId,
    required List<CartItem> cartItems,
  }) async {
    try {
      var totalAmount = 0;
      final buyTicketRequestItems = cartItems.map((e) {
        totalAmount += e.ticketPrice.fee ?? 0;

        return BuyTicketRequestItem(
          price: e.ticketPrice.fee,
          plateNumber: e.ticketPrice.plateNumber,
          vehicleId: e.vehicle.vehicleId,
          vehiclesGroupId: e.vehicle.vehicleGroupId,
          vehicleTypeId: e.vehicle.vehicleTypeId,
          effDate: Jiffy(e.effDate).format('dd/MM/yyyy HH:mm:ss'),
          expDate: Jiffy(e.expDate).format('dd/MM/yyyy HH:mm:ss'),
          chargeMethodId: e.ticketPrice.chargeMethodId,
          booCode: e.ticketPrice.booCode,
          stationType: e.ticketPrice.stationType,
          servicePlanTypeId: e.servicePlanType,
          // offerId: e.ticketPrice.ocsCode,
          seatNumber: e.vehicle.seatNumber,
          cargoWeight: e.vehicle.cargoWeight?.round(),
          netWeight: e.vehicle.netWeight?.round(),
          epc: e.vehicle.epc,
          stageId: e.stationStage.stageId,
          stageName:
              e.stationStage.stageId != null ? e.stationStage.name : null,
          stationId: e.stationStage.stationId,
          stationName:
              e.stationStage.stationId != null ? e.stationStage.name : null,
        );
      }).toList();

      final response = await _crmClient.buyTickets(
        customerId: customerId,
        contractId: contractId,
        request: BuyTicketRequest(
          list: buyTicketRequestItems,
          amount: totalAmount,
          quantity: buyTicketRequestItems.length,
        ),
      );

      final listSuccess = response.data?.listSuccess;
      final listFail = response.data?.listFail;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          listSuccess == null ||
          listFail == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(BuyTicketResponseDataNotNull(
        listSuccess: listSuccess,
        listFail: listFail,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }
}
