import 'package:json_annotation/json_annotation.dart';

part 'beneficiary_request.g.dart';

@JsonSerializable(explicitToJson: true)
class BeneficiaryRequest {
  BeneficiaryRequest({
    required this.contractNo,
    required this.customerName,
    required this.reminiscentName,
  });

  final String contractNo;
  final String customerName;
  final String reminiscentName;

  Map<String, dynamic> toJson() => _$BeneficiaryRequestToJson(this);
}

@JsonSerializable(explicitToJson: true)
class BeneficiaryRequestUpdate {
  BeneficiaryRequestUpdate({
    required this.beneficiaryInformationId,
    required this.reminiscentName,
  });
  final int beneficiaryInformationId;
  final String reminiscentName;

  Map<String, dynamic> toJson() => _$BeneficiaryRequestUpdateToJson(this);
}
