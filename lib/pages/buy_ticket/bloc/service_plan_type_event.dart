part of 'service_plan_type_bloc.dart';

abstract class ServicePlanTypeEvent extends Equatable {
  const ServicePlanTypeEvent();
}

class ServicePlanTypeChanged extends ServicePlanTypeEvent {
  final ServicePlanType servicePlanType;

  const ServicePlanTypeChanged(this.servicePlanType);

  @override
  List<Object> get props => [servicePlanType];
}
