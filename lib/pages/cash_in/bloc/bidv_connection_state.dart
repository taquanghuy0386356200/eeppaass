part of 'bidv_connection_bloc.dart';

abstract class BidvConnectionState extends CashInState {
  const BidvConnectionState();
}

class BidvConnectionInitial extends BidvConnectionState {
  @override
  List<Object> get props => [];
}

class BidvConnectionInProgress extends BidvConnectionState {
  @override
  List<Object> get props => [];
}

class BidvConnectionSuccess extends BidvConnectionState {
  final String serviceId;
  final String merchantId;
  final String tranDate;
  final String transId;
  final String responseCode;
  final String moreInfo;
  final String secureCode;
  final String redirectUrl;

  const BidvConnectionSuccess({
    required this.serviceId,
    required this.merchantId,
    required this.tranDate,
    required this.transId,
    required this.responseCode,
    required this.moreInfo,
    required this.secureCode,
    required this.redirectUrl,
  });

  @override
  List<Object> get props => [
        serviceId,
        merchantId,
        tranDate,
        transId,
        responseCode,
        moreInfo,
        secureCode,
        redirectUrl
      ];
}

class BidvConnectionFailure extends BidvConnectionState {
  final String message;

  const BidvConnectionFailure(this.message);

  @override
  List<Object> get props => [message];
}
