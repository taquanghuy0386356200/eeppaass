part of 'forgot_password_form_bloc.dart';

class ForgotPasswordFormState {
  final String? plateNumber;
  final String? plateTypeCode;
  final String? phoneNumber;

  const ForgotPasswordFormState({
    this.plateNumber,
    this.plateTypeCode,
    this.phoneNumber,
  });

  @override
  String toString() {
    return 'ForgotPasswordFormState{'
        ' plateNumber: $plateNumber,'
        ' plateTypeCode: $plateTypeCode,'
        ' phoneNumber: $phoneNumber,'
        '}';
  }
}
