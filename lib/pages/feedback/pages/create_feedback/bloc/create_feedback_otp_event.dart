part of 'create_feedback_otp_bloc.dart';

abstract class CreateFeedbackOtpEvent extends Equatable {
  const CreateFeedbackOtpEvent();
}

class CreateFeedbackOtpRequested extends CreateFeedbackOtpEvent {
  const CreateFeedbackOtpRequested();

  @override
  List<Object> get props => [];
}

class FeedbackOtpPaused extends CreateFeedbackOtpEvent {
  const FeedbackOtpPaused();

  @override
  List<Object> get props => [];
}
