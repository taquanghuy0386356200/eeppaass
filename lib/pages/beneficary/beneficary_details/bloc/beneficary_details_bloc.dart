import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:equatable/equatable.dart';

part 'beneficary_details_event.dart';
part 'beneficary_details_state.dart';

class BeneficiaryDetailsBloc
    extends Bloc<BeneficiaryDetailsEvent, BeneficiaryDetailsState> {
  final IBeneficiaryRepository _beneficiaryRepository;

  BeneficiaryDetailsBloc({
    required IBeneficiaryRepository beneficiaryRepository,
  })  : _beneficiaryRepository = beneficiaryRepository,
        super(BeneficiaryDetailsInitial()) {
    on<BeneficiaryUpdated>(_onBeneficiaryUpdated);
    on<BeneficiaryDelete>(_onBeneficiaryDelete);
    on<BeneficiaryChanged>((event, emit) => emit(
        BeneficiaryChangedSuccess(reminiscentName: event.reminiscentName)));
  }

  FutureOr<void> _onBeneficiaryUpdated(
    BeneficiaryUpdated event,
    Emitter<BeneficiaryDetailsState> emit,
  ) async {
    emit(const BeneficiaryUpdatedInProgress());

    final result = await _beneficiaryRepository.updateBeneficiary(
        beneficiaryInformationId: event.beneficiaryInformationId,
        reminiscentName: event.reminiscentName);

    result.when(
      success: (_) => emit(const BeneficiaryUpdatedSuccess()),
      failure: (failure) =>
          emit(BeneficiaryUpdatedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onBeneficiaryDelete(
    BeneficiaryDelete event,
    Emitter<BeneficiaryDetailsState> emit,
  ) async {
    emit(const BeneficiaryDeleteInProgress());

    final result = await _beneficiaryRepository.deleteBeneficiary(
        beneficiaryId: event.beneficiaryId);

    result.when(
      success: (_) => emit(const BeneficiaryDeleteSuccess()),
      failure: (failure) =>
          emit(BeneficiaryDeleteFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
