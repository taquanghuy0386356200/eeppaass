// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_info_contract.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChangeInfoContractTotalResponse _$ChangeInfoContractTotalResponseFromJson(
        Map<String, dynamic> json) =>
    ChangeInfoContractTotalResponse(
      data: json['data'] == null
          ? null
          : ChangeInfoContractResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChangeInfoContractTotalResponseToJson(
        ChangeInfoContractTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ChangeInfoContractResponse _$ChangeInfoContractResponseFromJson(
        Map<String, dynamic> json) =>
    ChangeInfoContractResponse(
      contractDetailOrginal: json['contractDetailOrginal'] == null
          ? null
          : ContractDetailOrginal.fromJson(
              json['contractDetailOrginal'] as Map<String, dynamic>),
      contractDetail: json['contractDetail'] == null
          ? null
          : ContractDetail.fromJson(
              json['contractDetail'] as Map<String, dynamic>),
      phoneContact: json['phoneContact'] as String?,
      ticketId: json['ticketId'] as int?,
    )..ticketProfiles = (json['ticketProfiles'] as List<dynamic>?)
        ?.map((e) => TicketProfiles.fromJson(e as Map<String, dynamic>))
        .toList();

Map<String, dynamic> _$ChangeInfoContractResponseToJson(
        ChangeInfoContractResponse instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'phoneContact': instance.phoneContact,
      'contractDetailOrginal': instance.contractDetailOrginal,
      'contractDetail': instance.contractDetail,
      'ticketProfiles': instance.ticketProfiles,
    };

ContractDetailOrginal _$ContractDetailOrginalFromJson(
        Map<String, dynamic> json) =>
    ContractDetailOrginal(
      cusTypeId: json['cusTypeId'] as int?,
      userName: json['userName'] as String?,
      userId: json['userId'] as String?,
      signName: json['signName'] as String?,
      birth: json['birth'] as String?,
      gender: json['gender'] as String?,
      address: json['address'] as String?,
      identifier: json['identifier'] as String?,
      dateOfIssue: json['dateOfIssue'] as String?,
      custName: json['custName'] as String?,
      placeOfIssue: json['placeOfIssue'] as String?,
      contractNo: json['contractNo'] as String?,
      signDate: json['signDate'] as String?,
      effDate: json['effDate'] as String?,
      expDate: json['expDate'] as String?,
      phone: json['phone'] as String?,
      email: json['email'] as String?,
      customerId: json['customerId'] as String?,
      contractId: json['contractId'] as String?,
      documentType: json['documentType'] as int?,
      isAdditional: json['isAdditional'] as int?,
      smsNotification: json['smsNotification'] as String?,
      accountAlias: json['accountAlias'] as String?,
      billCycle: json['billCycle'] as String?,
      billCycleMergeType: json['billCycleMergeType'] as String?,
      noticeAreaName: json['noticeAreaName'] as String?,
      noticeStreet: json['noticeStreet'] as String?,
      noticeAreaCode: json['noticeAreaCode'] as String?,
      noticeEmail: json['noticeEmail'] as String?,
      noticePhoneNumber: json['noticePhoneNumber'] as String?,
      noticeName: json['noticeName'] as String?,
    );

Map<String, dynamic> _$ContractDetailOrginalToJson(
        ContractDetailOrginal instance) =>
    <String, dynamic>{
      'cusTypeId': instance.cusTypeId,
      'userName': instance.userName,
      'userId': instance.userId,
      'birth': instance.birth,
      'gender': instance.gender,
      'address': instance.address,
      'identifier': instance.identifier,
      'dateOfIssue': instance.dateOfIssue,
      'placeOfIssue': instance.placeOfIssue,
      'custName': instance.custName,
      'contractNo': instance.contractNo,
      'signDate': instance.signDate,
      'effDate': instance.effDate,
      'expDate': instance.expDate,
      'phone': instance.phone,
      'email': instance.email,
      'customerId': instance.customerId,
      'contractId': instance.contractId,
      'documentType': instance.documentType,
      'isAdditional': instance.isAdditional,
      'smsNotification': instance.smsNotification,
      'accountAlias': instance.accountAlias,
      'signName': instance.signName,
      'billCycle': instance.billCycle,
      'billCycleMergeType': instance.billCycleMergeType,
      'noticeAreaName': instance.noticeAreaName,
      'noticeStreet': instance.noticeStreet,
      'noticeAreaCode': instance.noticeAreaCode,
      'noticeEmail': instance.noticeEmail,
      'noticePhoneNumber': instance.noticePhoneNumber,
      'noticeName': instance.noticeName,
    };

ContractDetail _$ContractDetailFromJson(Map<String, dynamic> json) =>
    ContractDetail(
      birthDate: json['birthDate'] as String?,
    )
      ..noticeName = json['noticeName'] as String?
      ..noticeAreaCode = json['noticeAreaCode'] as String?
      ..noticeAreaName = json['noticeAreaName'] as String?
      ..noticeStreet = json['noticeStreet'] as String?
      ..noticePhoneNumber = json['noticePhoneNumber'] as String?
      ..noticeEmail = json['noticeEmail'] as String?;

Map<String, dynamic> _$ContractDetailToJson(ContractDetail instance) =>
    <String, dynamic>{
      'noticeName': instance.noticeName,
      'noticeAreaCode': instance.noticeAreaCode,
      'noticeAreaName': instance.noticeAreaName,
      'noticeStreet': instance.noticeStreet,
      'noticePhoneNumber': instance.noticePhoneNumber,
      'noticeEmail': instance.noticeEmail,
      'birthDate': instance.birthDate,
    };

Profiles _$ProfilesFromJson(Map<String, dynamic> json) => Profiles(
      id: json['id'] as int?,
      ticketId: json['ticketId'] as int?,
      ticketPayloadId: json['ticketPayloadId'] as int?,
      profileType: json['profileType'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
    );

Map<String, dynamic> _$ProfilesToJson(Profiles instance) => <String, dynamic>{
      'id': instance.id,
      'ticketId': instance.ticketId,
      'ticketPayloadId': instance.ticketPayloadId,
      'profileType': instance.profileType,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
    };

Attachments _$AttachmentsFromJson(Map<String, dynamic> json) => Attachments(
      attachmentId: json['attachmentId'] as int?,
      ticketId: json['ticketId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
      type: json['type'] as int?,
      objectsId: json['objectsId'] as int?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
    );

Map<String, dynamic> _$AttachmentsToJson(Attachments instance) =>
    <String, dynamic>{
      'attachmentId': instance.attachmentId,
      'ticketId': instance.ticketId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
      'type': instance.type,
      'objectsId': instance.objectsId,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
    };
