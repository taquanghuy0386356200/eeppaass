// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'id_card_ocr.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IdCardOcr _$IdCardOcrFromJson(Map<String, dynamic> json) => IdCardOcr(
      address: json['address'] as String?,
      birthday: json['birthday'] as String?,
      birthplace: json['birthplace'] as String?,
      cardExtractionConfidence:
          (json['card_extraction_confidence'] as num?)?.toDouble(),
      classificationConfidence:
          (json['classification_confidence'] as num?)?.toDouble(),
      completeCardConfidence:
          (json['complete_card_confidence'] as num?)?.toDouble(),
      confidence: json['confidence'] as String?,
      district: json['district'] as String?,
      districtName: json['district_name'] as String?,
      document: json['document'] as String?,
      ethnicity: json['ethnicity'] as String?,
      expiry: json['expiry'] as String?,
      id: json['id'] as String?,
      idCheck: json['id_check'] as String?,
      idLogic: json['id_logic'] as int?,
      idLogicMessage: json['id_logic_message'] as String?,
      idSpoofConfidence: json['id_spoof_confidence'] as int?,
      idSpoofConfident: json['id_spoof_confident'] as int?,
      idType: $enumDecodeNullable(_$IdTypeEnumMap, json['id_type'],
          unknownValue: IdType.unknown),
      idconf: json['idconf'] as String?,
      isBinary: json['is_binary'] as bool?,
      isCompleteCard: json['is_complete_card'] as bool?,
      issueBy: json['issue_by'] as String?,
      issueDate: json['issue_date'] as String?,
      name: json['name'] as String?,
      nameConfidence: json['name_confidence'] as String?,
      nationality: json['nationality'] as String?,
      precinct: json['precinct'] as String?,
      precinctName: json['precinct_name'] as String?,
      province: json['province'] as String?,
      provinceName: json['province_name'] as String?,
      qualityImageConfidence:
          (json['quality_image_confidence'] as num?)?.toDouble(),
      religion: json['religion'] as String?,
      resultCode: json['result_code'] as int?,
      message: json['message'] as String?,
      sex: json['sex'] as String?,
      street: json['street'] as String?,
      streetName: json['street_name'] as String?,
    );

Map<String, dynamic> _$IdCardOcrToJson(IdCardOcr instance) => <String, dynamic>{
      'address': instance.address,
      'birthday': instance.birthday,
      'birthplace': instance.birthplace,
      'card_extraction_confidence': instance.cardExtractionConfidence,
      'classification_confidence': instance.classificationConfidence,
      'complete_card_confidence': instance.completeCardConfidence,
      'confidence': instance.confidence,
      'district': instance.district,
      'district_name': instance.districtName,
      'document': instance.document,
      'ethnicity': instance.ethnicity,
      'expiry': instance.expiry,
      'id': instance.id,
      'id_check': instance.idCheck,
      'id_logic': instance.idLogic,
      'id_logic_message': instance.idLogicMessage,
      'id_spoof_confidence': instance.idSpoofConfidence,
      'id_spoof_confident': instance.idSpoofConfident,
      'id_type': _$IdTypeEnumMap[instance.idType],
      'idconf': instance.idconf,
      'is_binary': instance.isBinary,
      'is_complete_card': instance.isCompleteCard,
      'issue_by': instance.issueBy,
      'issue_date': instance.issueDate,
      'name': instance.name,
      'name_confidence': instance.nameConfidence,
      'nationality': instance.nationality,
      'precinct': instance.precinct,
      'precinct_name': instance.precinctName,
      'province': instance.province,
      'province_name': instance.provinceName,
      'quality_image_confidence': instance.qualityImageConfidence,
      'religion': instance.religion,
      'result_code': instance.resultCode,
      'message': instance.message,
      'sex': instance.sex,
      'street': instance.street,
      'street_name': instance.streetName,
    };

const _$IdTypeEnumMap = {
  IdType.front: 0,
  IdType.back: 1,
  IdType.unknown: 'unknown',
};
