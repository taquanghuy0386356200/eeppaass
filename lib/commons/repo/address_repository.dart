import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:simple_result/simple_result.dart';

abstract class IAddressRepository {
  Future<Result<List<AddressVTT>, Failure>> getProvinces();

  Future<Result<List<AddressVTT>, Failure>> getDistricts({required String provinceCode});

  Future<Result<List<AddressVTT>, Failure>> getWards({
    required String provinceCode,
    required String districtCode,
  });
}

class AddressRepository implements IAddressRepository {
  final CrmClient _crmClient;

  const AddressRepository({required CrmClient crmClient}) : _crmClient = crmClient;

  @override
  Future<Result<List<AddressVTT>, Failure>> getProvinces() async {
    try {
      final response = await _crmClient.getProvinces();
      final data = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || data == null) {
        return Result.failure(
          ResponseFailure(
            message: response.mess?.description ?? '',
          ),
        );
      }
      return Result.success(data);
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<List<AddressVTT>, Failure>> getDistricts({required String provinceCode}) async {
    try {
      final response = await _crmClient.getDistricts(provinceCode: provinceCode);
      final data = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || data == null) {
        return Result.failure(
          ResponseFailure(
            message: response.mess?.description ?? '',
          ),
        );
      }
      return Result.success(data);
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<List<AddressVTT>, Failure>> getWards({
    required String provinceCode,
    required String districtCode,
  }) async {
    try {
      final response = await _crmClient.getWards(
        provinceCode: provinceCode,
        districtCode: districtCode,
      );
      final data = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || data == null) {
        return Result.failure(
          ResponseFailure(
            message: response.mess?.description ?? '',
          ),
        );
      }
      return Result.success(data);
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }
}
