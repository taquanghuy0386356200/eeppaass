// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle_error_profile_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleErrorProfile _$VehicleErrorProfileFromJson(Map<String, dynamic> json) =>
    VehicleErrorProfile(
      vehicleProfileId: json['vehicleProfileId'] as int?,
      contractId: json['contractId'] as int?,
      vehicleId: json['vehicleId'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      fileSize: json['fileSize'] as String?,
      filePath: json['filePath'] as String?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
      status: json['status'] as String?,
      documentTypeName: json['documentTypeName'] as String?,
      documentCode: json['documentCode'] as String?,
      ocrStatus: json['ocrStatus'] as int?,
      ocrResult: json['ocrResult'] as String?,
      checkStatus: json['checkStatus'] as int?,
    );

Map<String, dynamic> _$VehicleErrorProfileToJson(
        VehicleErrorProfile instance) =>
    <String, dynamic>{
      'vehicleProfileId': instance.vehicleProfileId,
      'contractId': instance.contractId,
      'vehicleId': instance.vehicleId,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'fileSize': instance.fileSize,
      'filePath': instance.filePath,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'status': instance.status,
      'documentTypeName': instance.documentTypeName,
      'documentCode': instance.documentCode,
      'ocrStatus': instance.ocrStatus,
      'ocrResult': instance.ocrResult,
      'checkStatus': instance.checkStatus,
    };

VehicleErrorProfilesResponse _$VehicleErrorProfilesResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleErrorProfilesResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => VehicleErrorProfile.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleErrorProfilesResponseToJson(
        VehicleErrorProfilesResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
