
class CarDoctorGetOtpRequest {
  CarDoctorGetOtpRequest({
    required this.contractId,
    required this.name,
    required this.phone,
    required this.identifier
  });

  final String contractId;
  final String name;
  final String phone;
  final String identifier;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['contractId'] = contractId;
    map['name'] = name;
    map['phone'] = phone;
    map['identifiedCard'] = identifier;
    return map;
  }
}