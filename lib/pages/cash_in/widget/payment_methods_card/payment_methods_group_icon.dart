import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';

class PaymentMethodsGroupIcon extends StatelessWidget {
  final int group;
  final double? height;
  final double? width;

  const PaymentMethodsGroupIcon({
    Key? key,
    required this.group,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (group) {
      case 0:
        return Assets.icons.eWallet.image(
          height: height,
          width: width,
        );
      case 1:
        return Assets.icons.bank.image(
          height: height,
          width: width,
        );
      case 2:
        return Assets.icons.domesticCard.image(
          height: height,
          width: width,
        );
      case 3:
        return Assets.icons.interCard.image(
          height: height,
          width: width,
        );
      default:
        return const SizedBox.shrink();
    }
  }
}
