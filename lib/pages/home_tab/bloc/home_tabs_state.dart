part of 'home_tabs_bloc.dart';

abstract class HomeTabsState extends Equatable {
  const HomeTabsState();
}

class HomeTabBarHidden extends HomeTabsState {
  const HomeTabBarHidden();

  @override
  List<Object> get props => [];
}

class HomeTabBarShowed extends HomeTabsState {
  final int? currentTab;
  const HomeTabBarShowed({this.currentTab});

  @override
  List<Object?> get props => [currentTab];

  HomeTabBarShowed copyWith({
    int? currentTab
  }) {
    return HomeTabBarShowed(
      currentTab: currentTab ?? this.currentTab
    );
  }
}
