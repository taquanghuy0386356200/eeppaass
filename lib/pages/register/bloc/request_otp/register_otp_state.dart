part of 'register_otp_bloc.dart';

@immutable
abstract class RegisterOtpState extends Equatable {
  const RegisterOtpState();
}

class RegisterOtpInitial extends RegisterOtpState {
  @override
  List<Object> get props => [];
}

class RegisterOtpRequestedInProgress extends RegisterOtpState {
  @override
  List<Object> get props => [];
}

class RegisterOtpRequestedSuccess extends RegisterOtpState {
  final DateTime submittedSuccessTime;
  final RegisterOtpRequested reRequestedEvent;

  const RegisterOtpRequestedSuccess({
    required this.submittedSuccessTime,
    required this.reRequestedEvent,
  });

  @override
  List<Object> get props => [submittedSuccessTime, reRequestedEvent];
}

class RegisterOtpRequestedFailure extends RegisterOtpState {
  final String message;

  const RegisterOtpRequestedFailure(this.message);

  @override
  List<Object> get props => [message];
}
