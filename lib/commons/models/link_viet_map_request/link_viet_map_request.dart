import 'package:json_annotation/json_annotation.dart';

part 'link_viet_map_request.g.dart';

@JsonSerializable()
class LinkVietMapRequest {
  LinkVietMapRequest({
    this.phoneNumber,
    this.accountName,
    this.token,
  });

  final String? phoneNumber;
  final String? accountName;
  final String? token;

  factory LinkVietMapRequest.fromJson(Map<String, dynamic> json) => _$LinkVietMapRequestFromJson(json);
  Map<String, dynamic> toJson() => _$LinkVietMapRequestToJson(this);
}
