part of 'utility_bloc.dart';


class UtilityState extends Equatable {
  const UtilityState();

  @override
  List<Object?> get props => [];
}

class GetServiceSuccess extends UtilityState {
  final List<String> unavailableServices;

  const GetServiceSuccess(this.unavailableServices);

  @override
  List<Object?> get props => [
    unavailableServices
  ];
}

class GetServiceFail extends UtilityState {
  final String message;

  const GetServiceFail(this.message);

  @override
  List<Object> get props => [message];
}

