import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ContractDetailsCard extends StatelessWidget {
  const ContractDetailsCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShadowCard(
      borderRadius: 16.0,
      shadowOpacity: 0.2,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(
          vertical: 16.h,
          horizontal: 16.w,
        ),
        child: BlocSelector<AppBloc, AppState, User?>(
          selector: (state) => state.user,
          builder: (context, user) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _TextRow(title: 'Số hợp đồng', value: user?.contractNo ?? ''),
                SizedBox(height: 10.h),
                _TextRow(title: 'Ngày ký Hợp đồng', value: user?.effDate ?? ''),
                SizedBox(height: 10.h),
                _TextRow(title: 'Ngày cấp', value: user?.effDate ?? ''),
              ],
            );
          },
        ),
      ),
    );
  }
}

class _TextRow extends StatelessWidget {
  final String title;
  final String value;

  const _TextRow({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: ColorName.disabledBorderColor,
              ),
        ),
        SizedBox(width: 16.w),
        Expanded(
          child: Text(
            value,
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  fontWeight: FontWeight.w600,
                ),
            textAlign: TextAlign.right,
          ),
        ),
      ],
    );
  }
}
