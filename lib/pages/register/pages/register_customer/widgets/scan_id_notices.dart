import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScanIDNotices extends StatelessWidget {
  const ScanIDNotices({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Hướng dẫn chụp CMND/CCCD',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
          textAlign: TextAlign.start,
        ),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w),
              child: Icon(Icons.circle, size: 6.r, color: Colors.white),
            ),
            Text(
              'Chụp giấy tờ hợp lệ, không photocopy',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.start,
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w),
              child: Icon(Icons.circle, size: 6.r, color: Colors.white),
            ),
            Text(
              'Đảm bảo giấy tờ chụp đầy đủ thông tin',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.start,
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w),
              child: Icon(Icons.circle, size: 6.r, color: Colors.white),
            ),
            Text(
              'Ảnh chụp không bị chói sáng',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.start,
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w),
              child: Icon(Icons.circle, size: 6.r, color: Colors.white),
            ),
            Text(
              'Ảnh chụp không bị xoay, không mất góc',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.start,
            ),
          ],
        ),
      ],
    );
  }
}
