import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/services/vnpt/vnpay_payment_request.dart';
import 'package:epass/commons/services/vnpt/vnpt_create_order.dart';
import 'package:equatable/equatable.dart';

import '../../../commons/services/zalopay/zalo_pay_create_order.dart';
import '../../../commons/services/zalopay/zalopay_create_order_request.dart';
import 'cash_in_bloc.dart';

part 'vnpt_gate_event.dart';

part 'vnpt_gate_state.dart';

class VnptGateBloc extends Bloc<VnptGateEvent, VnptGateState> {
  final VnptCreateOrderService _vpntCreateOrderService;

  VnptGateBloc({required VnptCreateOrderService vpntCreateOrderService})
      : _vpntCreateOrderService = vpntCreateOrderService,
        super(VnptInitial()) {
    on<VnptPaymentProceeded>(_onVnptCreateOrder);
    on<VnptPaymentsSuccess>(_onVnptPaymentsSuccess);
  }

  FutureOr<void> _onVnptPaymentsSuccess(
    VnptPaymentsSuccess event,
    Emitter<VnptGateState> emit,
  ) {
    if (event.responseCode == "00") {
      emit(VnptPaymentSuccessOk(
          transactionId: event.transactionId,
          responseCode: event.responseCode,
          secureCode: event.secureCode));
    } else if (event.responseCode == "01") {
      emit(const VnptPaymentFailure('Giao dịch thất bại!'));
    } else if (event.responseCode == "02") {
      emit(const VnptPaymentFailure('Dữ liệu không đúng định dạng!'));
    } else if (event.responseCode == "03") {
      emit(const VnptPaymentFailure('Mã giao dịch đã tồn tại!'));
    } else if (event.responseCode == "04") {
      emit(const VnptPaymentFailure('Quá thời gian thực hiện!'));
    } else if (event.responseCode == "05") {
      emit(const VnptPaymentFailure('Không tìm thấy dữ liệu!'));
    } else if (event.responseCode == "06") {
      emit(const VnptPaymentFailure('Lỗi hệ thống!'));
    } else if (event.responseCode == "07") {
      emit(const VnptPaymentFailure('Chữ ký không đúng!'));
    } else if (event.responseCode == "08") {
      emit(const VnptPaymentFailure('Merchant service đang bị khoá!'));
    } else if (event.responseCode == "09") {
      emit(const VnptPaymentFailure('Merchant service không tồn tại!'));
    } else if (event.responseCode == "96") {
      emit(const VnptPaymentFailure('Hệ thống đang bảo trì!'));
    } else if (event.responseCode == "99") {
      emit(const VnptPaymentFailure('Lỗi không xác định!'));
    } else {
      emit(const VnptPaymentFailure('Giao dịch thất bại!'));
    }
  }

  FutureOr<void> _onVnptCreateOrder(
    VnptPaymentProceeded event,
    Emitter<VnptGateState> emit,
  ) async {
    final vnptRequest = VNPTRequest(
      contractId: "",
      amount: event.amount,
      client_ip: "10.1.36.106",
    );

    final result = await _vpntCreateOrderService.createPayVNPT(vnptRequest);
    if (result == null) {
      emit(const VnptPaymentFailure(''));
    } else {
      emit(VnptPaymentSuccess(
          description: result.description,
          redirectUrl: result.redirectUrl,
          responseCode: result.responseCode,
          secureCode: result.secureCode));
    }
  }
}
