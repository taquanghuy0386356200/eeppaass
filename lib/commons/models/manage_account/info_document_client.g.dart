// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'info_document_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InfoDocumentClientResponse _$InfoDocumentClientResponseFromJson(
        Map<String, dynamic> json) =>
    InfoDocumentClientResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => DataInfoDocumentClientResponse.fromJson(
              e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$InfoDocumentClientResponseToJson(
        InfoDocumentClientResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

DataInfoDocumentClientResponse _$DataInfoDocumentClientResponseFromJson(
        Map<String, dynamic> json) =>
    DataInfoDocumentClientResponse(
      id: json['id'] as int?,
      filePath: json['filePath'] as String?,
      fileName: json['fileName'] as String?,
      base64Data: json['base64Data'] as String?,
      profileType: json['profileType'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      documentName: json['documentName'] as String?,
    );

Map<String, dynamic> _$DataInfoDocumentClientResponseToJson(
        DataInfoDocumentClientResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'filePath': instance.filePath,
      'fileName': instance.fileName,
      'documentName': instance.documentName,
      'base64Data': instance.base64Data,
      'profileType': instance.profileType,
      'documentTypeId': instance.documentTypeId,
    };
