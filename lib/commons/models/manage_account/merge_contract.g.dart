// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merge_contract.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MergeContractTotalResponse _$MergeContractTotalResponseFromJson(
        Map<String, dynamic> json) =>
    MergeContractTotalResponse(
      data: json['data'] == null
          ? null
          : MergeContractResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MergeContractTotalResponseToJson(
        MergeContractTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

MergeContractResponse _$MergeContractResponseFromJson(
        Map<String, dynamic> json) =>
    MergeContractResponse(
      ticketId: json['ticketId'] as int?,
      phoneContact: json['phoneContact'] as String?,
      contractOrginal: json['contractOrginal'] == null
          ? null
          : ContractDetailOriginal.fromJson(
              json['contractOrginal'] as Map<String, dynamic>),
      ticketProfiles: (json['ticketProfiles'] as List<dynamic>?)
          ?.map((e) => TicketProfiles.fromJson(e as Map<String, dynamic>))
          .toList(),
      contractsTransfer: (json['contractsTransfer'] as List<dynamic>?)
          ?.map((e) => ContractsTransfer.fromJson(e as Map<String, dynamic>))
          .toList(),
      transferMoney: json['transferMoney'] as bool?,
    );

Map<String, dynamic> _$MergeContractResponseToJson(
        MergeContractResponse instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'phoneContact': instance.phoneContact,
      'contractOrginal': instance.contractOrginal,
      'ticketProfiles': instance.ticketProfiles,
      'contractsTransfer': instance.contractsTransfer,
      'transferMoney': instance.transferMoney,
    };

ContractDetailOriginal _$ContractDetailOriginalFromJson(
        Map<String, dynamic> json) =>
    ContractDetailOriginal(
      dateOfIssue: json['dateOfIssue'] as String?,
      placeOfIssue: json['placeOfIssue'] as String?,
      custName: json['custName'] as String?,
      email: json['email'] as String?,
      contractNo: json['contractNo'] as String?,
      signDate: json['signDate'] as String?,
      cusPhoneNumber: json['cusPhoneNumber'] as String?,
      signName: json['signName'] as String?,
      cusTypeId: json['cusTypeId'] as int?,
      userName: json['userName'] as String?,
      userId: json['userId'] as String?,
      birth: json['birth'] as String?,
      gender: json['gender'] as String?,
      address: json['address'] as String?,
      identifier: json['identifier'] as String?,
      effDate: json['effDate'] as String?,
      phone: json['phone'] as String?,
      customerId: json['customerId'] as String?,
      contractId: json['contractId'] as String?,
      documentType: json['documentType'] as int?,
      isAdditional: json['isAdditional'] as int?,
      isAlertMoney: json['isAlertMoney'] as int?,
      smsNotification: json['smsNotification'] as String?,
      billCycle: json['billCycle'] as String?,
      billCycleMergeType: json['billCycleMergeType'] as String?,
      noticeAreaName: json['noticeAreaName'] as String?,
      noticeStreet: json['noticeStreet'] as String?,
      noticeAreaCode: json['noticeAreaCode'] as String?,
      noticeEmail: json['noticeEmail'] as String?,
      noticePhoneNumber: json['noticePhoneNumber'] as String?,
      noticeName: json['noticeName'] as String?,
    )..status = json['status'] as String?;

Map<String, dynamic> _$ContractDetailOriginalToJson(
        ContractDetailOriginal instance) =>
    <String, dynamic>{
      'dateOfIssue': instance.dateOfIssue,
      'placeOfIssue': instance.placeOfIssue,
      'custName': instance.custName,
      'email': instance.email,
      'contractNo': instance.contractNo,
      'signDate': instance.signDate,
      'signName': instance.signName,
      'cusPhoneNumber': instance.cusPhoneNumber,
      'cusTypeId': instance.cusTypeId,
      'userName': instance.userName,
      'userId': instance.userId,
      'birth': instance.birth,
      'gender': instance.gender,
      'address': instance.address,
      'identifier': instance.identifier,
      'effDate': instance.effDate,
      'phone': instance.phone,
      'customerId': instance.customerId,
      'contractId': instance.contractId,
      'documentType': instance.documentType,
      'isAdditional': instance.isAdditional,
      'isAlertMoney': instance.isAlertMoney,
      'smsNotification': instance.smsNotification,
      'status': instance.status,
      'billCycle': instance.billCycle,
      'billCycleMergeType': instance.billCycleMergeType,
      'noticeAreaName': instance.noticeAreaName,
      'noticeStreet': instance.noticeStreet,
      'noticeAreaCode': instance.noticeAreaCode,
      'noticeEmail': instance.noticeEmail,
      'noticePhoneNumber': instance.noticePhoneNumber,
      'noticeName': instance.noticeName,
    };

TicketProfiles _$TicketProfilesFromJson(Map<String, dynamic> json) =>
    TicketProfiles(
      id: json['id'] as int?,
      ticketId: json['ticketId'] as int?,
      ticketPayloadId: json['ticketPayloadId'] as int?,
      profileType: json['profileType'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
    );

Map<String, dynamic> _$TicketProfilesToJson(TicketProfiles instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ticketId': instance.ticketId,
      'ticketPayloadId': instance.ticketPayloadId,
      'profileType': instance.profileType,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
    };

ContractsTransfer _$ContractsTransferFromJson(Map<String, dynamic> json) =>
    ContractsTransfer(
      custName: json['custName'] as String?,
      contractNo: json['contractNo'] as String?,
      status: json['status'] as String?,
      signDate: json['signDate'] as String?,
      signName: json['signName'] as String?,
      contractId: json['contractId'] as String?,
      noticePhoneNumber: json['noticePhoneNumber'] as String?,
      isTransfer: json['isTransfer'] as bool? ?? false,
    );

Map<String, dynamic> _$ContractsTransferToJson(ContractsTransfer instance) =>
    <String, dynamic>{
      'custName': instance.custName,
      'contractNo': instance.contractNo,
      'status': instance.status,
      'signDate': instance.signDate,
      'signName': instance.signName,
      'contractId': instance.contractId,
      'noticePhoneNumber': instance.noticePhoneNumber,
      'isTransfer': instance.isTransfer,
    };
