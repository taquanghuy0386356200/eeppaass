
import 'package:json_annotation/json_annotation.dart';

part 'verify_passcode_request.g.dart';

@JsonSerializable()
class VerifyPassCodeRequest {
  String? partnerCode;
  String? stationCode;
  String? laneInCode;
  String? laneOutCode;
  int? ticketId;
  int? transId;
  int? plateNumber;
  int? amount;
  String? currency;
  int? contractId;
  String? passCode;

  VerifyPassCodeRequest({this.partnerCode,
    this.stationCode,
    this.laneInCode,
    this.laneOutCode,
    this.ticketId,
    this.transId,
    this.plateNumber,
    this.amount,
    this.currency,
    this.contractId,
    this.passCode});

  Map<String, dynamic> toJson() => _$VerifyPassCodeRequestToJson(this);
}