import 'dart:convert';

import 'package:epass/commons/models/mb_bank/mb_create_order.dart';
import 'package:epass/flavors.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

class MBCreateOrderService {
  Future<MBCreateOrderResponse?> createOrder(
      MBCreateOrderRequest request, String token) async {
    try {
      /// url auth token
      String url = F.mbPaymentUrl;

      var uuid = const Uuid();

      var transactionId = 'RPAP${uuid.v4().substring(0, 8)}';

      var clientMessageId = uuid.v4();

      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
        'clientMessageId': '$clientMessageId',
        'transactionId': '$transactionId',
      };

      var body = {
        "authenType": "${request.authenType}",
        "bankCode": "${request.bankCode}",
        "billNumber": "${request.billNumber}",
        "creditAccount": "${request.creditAccount}",
        "creditName": "${request.creditName}",
        "deeplinkCallback": "${request.deeplinkCallback}",
        "discountAmt": "${request.discountAmt}",
        "feeAmt": "${request.feeAmt}",
        "merchantId": "${request.merchantId}",
        "paymentDetail": "${request.paymentDetail}",
        "prodName": "${request.prodName}",
        "terminalId": "${request.terminalId}",
        "transAmt": "${request.transAmt}",
        "transChannel": "${request.transChannel}",
        "transMultiple": "${request.transMultiple}",
        "transType": "${request.transType}",
        "transactionId": "${request.transactionId}",
        "voucherCode": "${request.voucherCode}"
      };

      var response = await http.post(Uri.parse(url),
          headers: headers, body: jsonEncode(body));

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        return MBCreateOrderResponse.fromJson(data);
      } else {
        return null;
      }
    } on Exception {
      return null;
    }
  }
}
