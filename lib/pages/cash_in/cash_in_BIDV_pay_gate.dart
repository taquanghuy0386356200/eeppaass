import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/services/bidv/bidv_check_link_request.dart';
import 'package:epass/commons/services/bidv/bidv_link_request.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_check_connection_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_connection_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_result_page.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/bloc/payment_methods_card_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CashInBIDVPayGatePage extends StatefulWidget {
  final String title;
  final CheckLinkBIDVPayGateRequest request;

  const CashInBIDVPayGatePage({
    Key? key,
    required this.title,
    required this.request,
  }) : super(key: key);

  @override
  State<CashInBIDVPayGatePage> createState() => _CashInBIDVPayGatePageState();
}

class _CashInBIDVPayGatePageState extends State<CashInBIDVPayGatePage> {
  int _progress = 0;
  String? surl;
  late WebViewController _controller;
  int _isLinked = 0;
  late LinkBIDVPayGateRequest linkRequest;

  int _statustConnection = 0;
  late BidvCheckingConnectionSuccess dataBidvCheckingConnection;
  late BidvConnectionSuccess dataBidvConnectionSuccess;

  @override
  void initState() {
    _onCheckingConnection();
    super.initState();
  }

//dumv
  void _onCheckingConnection() {
    context.loaderOverlay.show();
    final response = context.read<AppBloc>().state.user;
    context
        .read<BidvCheckingConnectionBloc>()
        .add(BidvCheckingConnectionProceeded(
          payerId: response!.phone!,
          payerName: response.userName!,
          payerMobile: response.phone!,
          payerIdentity: response.identifier!,
        ));
  }

  void _onCreateOrder(int amount) {
    context.loaderOverlay.show();
    final response = context.read<AppBloc>().state.user;
    context.read<CashInBloc>().add(BidvCreateOrder(
          payerAdd: response!.address!,
          payerId: response.phone!,
          payerIdentity: response.identifier!,
          payerMobile: response.phone!,
          payerName: response.userName!,
          transAmount: amount,
        ));
  }

  void _onConnection() {
    context.loaderOverlay.show();
    final response = context.read<AppBloc>().state.user;
    context.read<BidvConnectionBloc>().add(BidvConnectionProceeded(
          payerId: response!.phone!,
          payerName: response.userName!,
          payerMobile: response.phone!,
          payerIdentity: response.identifier!,
          payerAdd: response.address!,
        ));
  }

  void _onCheckOTP(int amount, String transID) async {
    var opt = await context.pushRoute(const CashInConfirmOTPRoute());
    if (opt != null) {
      context.loaderOverlay.show();
      final response = context.read<AppBloc>().state.user;
      context.read<CashInBloc>().add(BidvCheckOTP(
            payerAdd: response!.address!,
            payerId: response.phone!,
            transId: transID,
            payerIdentity: response.identifier!,
            payerMobile: response.phone!,
            payerName: response.userName!,
            transAmount: amount,
            otpNumber: opt.toString(),
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final amount = context.select((CashInAmountBloc bloc) {
      final state = bloc.state;
      return state is CashInAmountChangedSuccess ? state.amount : null;
    });

    final contractId = context.select(
      (CashInTargetBloc bloc) => bloc.state.contractId,
    );

    final paymentMethod = context.select((PaymentMethodsCardBloc bloc) {
      final state = bloc.state;
      return state is PaymentMethodsFetchedSuccess
          ? state.selectedPaymentMethod
          : null;
    });
    return Scaffold(
        resizeToAvoidBottomInset: true,
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            leading: BackButton(onPressed: context.popRoute)),
        body: MultiBlocListener(
          listeners: [
            BlocListener<CashInBloc, CashInState>(
                listener: (context, state) async {
              if (state is CashInContractsPaymentSuccess) {
                context.loaderOverlay.hide();
                context.read<BalanceBloc>().add(BalanceFetched());
                context.replaceRoute(CashInResultRoute(
                  isSuccess: true,
                ));
              } else if (state is CashInContractsPaymentFailure) {
                context.loaderOverlay.hide();
                context.replaceRoute(CashInResultRoute(
                  isSuccess: false,
                  message: state.message,
                ));
              }
            }),
            BlocListener<BidvCheckingConnectionBloc,
                BidvCheckingConnectionState>(listener: (context, state) async {
              if (state is BidvCheckingConnectionSuccess) {
                dataBidvCheckingConnection = state;
                if (state.responseCode == '111') {
                  context.loaderOverlay.hide();
                  _onConnection();
                } else if (state.responseCode == '110') {
                  context.loaderOverlay.hide();
                  List<String> tmp = state.moreInfo.split('|');
                  String ans = tmp[0];
                  if (ans == "0") {
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: "Tài khoản đang chờ duyệt",
                    ));
                  } else if (ans == "2") {
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: "Tài khoản đang chờ hủy dịch vụ",
                    ));
                  } else if (ans == "3") {
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: "Tài khoản đang đã khóa dịch vụ",
                    ));
                  } else if (ans == "-1") {
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: " Từ chối duyệt.",
                    ));
                  } else if (ans == "1") {
                    _onCreateOrder(amount!);
                    // bat du lieu tra ve
                  }
                } else {
                  context.loaderOverlay.hide();
                  context.replaceRoute(CashInResultRoute(
                    isSuccess: false,
                    message: "Không tìm thấy tài khoản",
                  ));
                }
              } else if (state is BidvCheckingConnectionFailure) {
                context.loaderOverlay.hide();
                context.replaceRoute(CashInResultRoute(
                  isSuccess: false,
                  message: "Kiểm tra liên kết thất bại",
                ));
              }
            }),
            BlocListener<BidvConnectionBloc, BidvConnectionState>(
                listener: (context, state) {
              if (state is BidvConnectionSuccess) {
                dataBidvConnectionSuccess = state;
                context.loaderOverlay.hide();
                if (state.responseCode == '000') {
                  _statustConnection = 0;
                  _controller.loadUrl(state.redirectUrl);
                } else {
                  context.replaceRoute(CashInResultRoute(
                    isSuccess: false,
                    message: "Liên kết tài khoản thất bại",
                  ));
                }
              } else if (state is BidvConnectionFailure) {
                context.replaceRoute(CashInResultRoute(
                  isSuccess: false,
                  message: "Có Lỗi",
                ));
              }
            }),
            //create order
            BlocListener<CashInBloc, CashInState>(listener: (context, state) {
              if (state is BidvCreateOrderSuccess) {
                //dumv check xy ly
                context.loaderOverlay.hide();
                if (state.responseCode == '112') {
                  _onCheckOTP(amount!, state.transId);
                }
              } else if (state is BidvCreateOrderFailure) {
                context.loaderOverlay.hide();
                context.replaceRoute(CashInResultRoute(
                  isSuccess: false,
                  message: "Tạo lệnh thanh toán không thành công",
                ));
              } else if (state is BidvCheckOTPSuccess) {
                //dumv check xy ly
                context.loaderOverlay.hide();
                if (state.responseCode == '000') {
                  context.read<CashInBloc>().add(CashInContractsPayment(
                        topupChannel: paymentMethod!.code.name,
                        destContractId: contractId!,
                        requestId: state.transId,
                        amount: amount!,
                      ));
                } else if (state.responseCode == '004') {
                  context.replaceRoute(CashInResultRoute(
                    isSuccess: false,
                    message: "Quá thời gian chờ xác thực OTP",
                  ));
                } else if (state.responseCode == '005') {
                  context.replaceRoute(CashInResultRoute(
                    isSuccess: false,
                    message: "Sai mã xác thực OTP",
                  ));
                }
              } else if (state is BidvCheckOTPFailure) {
                context.loaderOverlay.hide();
                context.replaceRoute(CashInResultRoute(
                  isSuccess: false,
                  message: "Sai mã xác thực OTP",
                ));
              }
            }),
          ],
          child: WebView(
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: surl,
            onWebViewCreated: (WebViewController webViewController) {
              _controller = webViewController;
            },
            navigationDelegate: (navigation) {
              final uri = Uri.parse(navigation.url);
              //dumv bat du lieu oi day va cap nhap lai lien ket
              if (_statustConnection == 0) {
                _statustConnection = 1;
              } else if (_statustConnection == 1) {
                //_onCheckingConnection();
              }
              return NavigationDecision.navigate;
            },
            onProgress: (progress) {
              setState(() {
                _progress = progress;
              });
            },
          ),
        ));
  }
}
