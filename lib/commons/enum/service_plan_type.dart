/*
SERVICE_PLAN_TYPE_ID	NAME
1	Vé lượt
2	Vé ưu tiên
3	Vé cấm
4	Vé tháng
5	Vé quý
6	Vé năm
...
 */
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonEnum(alwaysCreate: true)
enum ServicePlanType {
  @JsonValue(1)
  daily,

  @JsonValue(4)
  monthly,

  @JsonValue(5)
  quarterly,

  other
}

extension ServicePlanTypeExt on ServicePlanType {
  Color get color {
    switch (this) {
      case ServicePlanType.daily:
        return Colors.amber;
      case ServicePlanType.monthly:
        return Colors.blue;
      case ServicePlanType.quarterly:
        return Colors.green;
      case ServicePlanType.other:
        return Colors.deepPurple;
    }
  }

  String get label {
    switch (this) {
      case ServicePlanType.daily:
        return 'Vé lượt';
      case ServicePlanType.monthly:
        return 'Vé tháng';
      case ServicePlanType.quarterly:
        return 'Vé quý';
      case ServicePlanType.other:
        return 'Khác';
    }
  }
}
