part of 'register_vehicles_bloc.dart';

abstract class RegisterVehiclesEvent extends Equatable {
  const RegisterVehiclesEvent();
}

class VehicleInfoAdded extends RegisterVehiclesEvent {
  @override
  List<Object> get props => [];
}

class VehicleInfoRemoved extends RegisterVehiclesEvent {
  final int index;

  const VehicleInfoRemoved(this.index);

  @override
  List<Object> get props => [index];
}

class VehicleInfoUpdated extends RegisterVehiclesEvent {
  final int vehicleIndex;
  final String? owner;
  final String? plateNumber;
  final CategoryModel? plateType;
  final CategoryModel? vehicleType;

  const VehicleInfoUpdated({
    required this.vehicleIndex,
    this.owner,
    this.plateNumber,
    this.plateType,
    this.vehicleType,
  });

  @override
  List<Object?> get props => [vehicleIndex, owner, plateNumber, plateType, vehicleType,];
}

class AddressInfoUpdated extends RegisterVehiclesEvent {
  final int vehicleIndex;

  const AddressInfoUpdated({
    required this.vehicleIndex,
  });

  @override
  List<Object?> get props =>
      [
        vehicleIndex,
      ];
}

class AddressInfoRegCertImageUpdated extends AddressInfoUpdated {
  final String? regCertImage;

  const AddressInfoRegCertImageUpdated({
    required int index,
    this.regCertImage,
  }) : super(vehicleIndex: index);
}

class AddressInfoVehicleRegCertImageUpdated extends AddressInfoUpdated {
  final String? vehicleRegCertImage;

  const AddressInfoVehicleRegCertImageUpdated({required int index, this.vehicleRegCertImage})
      : super(vehicleIndex: index);
}

class AddressInfoPhoneNumberUpdated extends AddressInfoUpdated {
  final String? phoneNumber;

  const AddressInfoPhoneNumberUpdated({required int index, this.phoneNumber}) : super(vehicleIndex: index);
}

class AddressInfoEmailUpdated extends AddressInfoUpdated {
  final String? email;

  const AddressInfoEmailUpdated({required int index, this.email}) : super(vehicleIndex: index);
}

class AddressInfoAddressDetailsUpdated extends AddressInfoUpdated {
  final String? addressDetails;

  const AddressInfoAddressDetailsUpdated({required int index, this.addressDetails}) : super(vehicleIndex: index);
}

class AddressInfoProvinceUpdated extends AddressInfoUpdated {
  final AddressVTT? province;

  const AddressInfoProvinceUpdated({required int index, this.province}) : super(vehicleIndex: index);
}

class AddressInfoDistrictUpdated extends AddressInfoUpdated {
  final AddressVTT? district;

  const AddressInfoDistrictUpdated({required int index, this.district}) : super(vehicleIndex: index);
}

class AddressInfoWardUpdated extends AddressInfoUpdated {
  final AddressVTT? ward;

  const AddressInfoWardUpdated({required int index, this.ward}) : super(vehicleIndex: index);
}

class AddressInfoReuseAddressUpdated extends AddressInfoUpdated {
  final bool? reuseAddress;

  const AddressInfoReuseAddressUpdated({required int index, this.reuseAddress}) : super(vehicleIndex: index);
}

class AddressInfoRegisterLevelUpdated extends AddressInfoUpdated {
  final RegisterLevel? timeFrame;

  const AddressInfoRegisterLevelUpdated({required int index, this.timeFrame}) : super(vehicleIndex: index);
}

class AddressInfoReuseTimeFrameUpdated extends AddressInfoUpdated {
  final bool? reuseTimeFrame;

  const AddressInfoReuseTimeFrameUpdated({required int index, this.reuseTimeFrame}) : super(vehicleIndex: index);
}
