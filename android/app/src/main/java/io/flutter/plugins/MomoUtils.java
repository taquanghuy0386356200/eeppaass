package io.flutter.plugins;

import android.app.Activity;
import android.content.Intent;

import com.example.epass.MainActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

import vn.momo.momo_partner.AppMoMoLib;

public class MomoUtils {

    public void rechargeByMomo(@NotNull Activity activity, @Nullable HashMap<String, Object> arguments) {
        AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT);
        AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN);
        AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.PRODUCTION);

        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put("merchantname", arguments.get("merchantname").toString());
        eventValue.put("merchantcode", arguments.get("merchantcode").toString());
        eventValue.put("amount", Integer.parseInt(arguments.get("amount").toString()));
        eventValue.put("orderId", arguments.get("orderid").toString());
        eventValue.put("orderLabel", "Nap tien vao tai khoan ETC");
        eventValue.put("description", arguments.get("description").toString());

        AppMoMoLib.getInstance().requestMoMoCallBack(activity, eventValue);
    }
}
