part of 'setting_page_bloc.dart';

abstract class SettingPageState extends Equatable {
  const SettingPageState();
}

class SettingPageInitial extends SettingPageState {
  @override
  List<Object> get props => [];
}

class SettingPageLoading extends SettingPageState {
  @override
  List<Object> get props => [];
}

class SettingPageLogoutSuccess extends SettingPageState {
  @override
  List<Object> get props => [];
}

class SettingPageLogoutFailure extends SettingPageState {
  final String message;

  const SettingPageLogoutFailure(this.message);

  @override
  List<Object> get props => [message];
}
