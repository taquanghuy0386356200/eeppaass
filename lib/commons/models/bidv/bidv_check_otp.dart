import 'package:epass/commons/models/cash_in_order/cash_in_order.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'bidv_check_otp.g.dart';

@JsonSerializable()
class BIDVCheckOTPRequest {
  final String data;

  BIDVCheckOTPRequest({
    required this.data,
  });

  factory BIDVCheckOTPRequest.fromJson(Map<String, dynamic> json) =>
      _$BIDVCheckOTPRequestFromJson(json);

  Map<String, dynamic> toJson() => _$BIDVCheckOTPRequestToJson(this);
}

@JsonSerializable()
class BIDVCheckOTPResponse {
  @JsonKey(name: 'data')
  final BIDVCheckOTPData data;
  final MessageData? mess;

  BIDVCheckOTPResponse({required this.data, this.mess});

  factory BIDVCheckOTPResponse.fromJson(Map<String, dynamic> json) =>
      _$BIDVCheckOTPResponseFromJson(json);
}

@JsonSerializable()
class BIDVCheckOTPData {
  final String transId;
  final String responseCode;

  BIDVCheckOTPData({required this.transId, required this.responseCode});

  factory BIDVCheckOTPData.fromJson(Map<String, dynamic> json) =>
      _$BIDVCheckOTPDataFromJson(json);
}
