import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/animated_flip_counter.dart';
import 'package:epass/commons/widgets/navigation_bar/bottom_indicator_bar.dart';
import 'package:epass/commons/widgets/navigation_bar/bottom_indicator_navigation_bar_item.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/bloc/sms_register/sms_register_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/utility/widgets/sms_text_high_light.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeTabsPage extends StatelessWidget {
  const HomeTabsPage({Key? key}) : super(key: key);

  final _tabRoutes = const [
    HomeRouter(),
    UtilityRouter(),
    NotificationRouter(),
    SettingRouter(),
  ];

  @override
  Widget build(BuildContext context) {
    final homeItems = [
      BottomIndicatorNavigationBarItem(
        icon: Assets.icons.tabbar.homeInactive.svg(width: 24.0, height: 24.0),
        activeIcon:
            Assets.icons.tabbar.homeActive.svg(width: 24.0, height: 24.0),
        label: 'Trang chủ',
      ),
      BottomIndicatorNavigationBarItem(
        icon: Stack(
          clipBehavior: Clip.none,
          children: [
            Assets.icons.tabbar.utilityInactive.svg(width: 24.0, height: 24.0),
            const Positioned(
              left: 0,
              top: -6,
              child: UtilitiesBadge(),
            )
          ],
        ),
        activeIcon: Stack(
          clipBehavior: Clip.none,
          children: [
            Assets.icons.tabbar.utilityActive.svg(width: 24.0, height: 24.0),
            const Positioned(
              left: 0,
              top: -6,
              child: UtilitiesBadge(),
            )
          ],
        ),
        label: 'Tiện ích',
      ),
      BottomIndicatorNavigationBarItem(
        icon: Stack(
          clipBehavior: Clip.none,
          children: [
            Assets.icons.tabbar.notiInactive.svg(width: 24.0, height: 24.0),
            context.select((NotificationBloc bloc) => bloc.state.totalUnread) >
                    0
                ? const Positioned(
                    left: 10,
                    top: -6,
                    child: NotificationBadge(),
                  )
                : const SizedBox.shrink(),
          ],
        ),
        activeIcon: Stack(
          clipBehavior: Clip.none,
          children: [
            Assets.icons.tabbar.notiActive.svg(width: 24.0, height: 24.0),
            context.select((NotificationBloc bloc) => bloc.state.totalUnread) >
                    0
                ? const Positioned(
                    left: 10,
                    top: -6,
                    child: NotificationBadge(),
                  )
                : const SizedBox.shrink(),
          ],
        ),
        label: 'Thông báo',
      ),
      BottomIndicatorNavigationBarItem(
        icon:
            Assets.icons.tabbar.settingInactive.svg(width: 24.0, height: 24.0),
        activeIcon:
            Assets.icons.tabbar.settingActive.svg(width: 24.0, height: 24.0),
        label: 'Cài đặt',
      ),
    ];

    return AutoTabsRouter(
      routes: _tabRoutes,
      builder: (context, child, animation) {
        final tabsRouter = AutoTabsRouter.of(context);

        return BasePage(
          bottomNavigationBar: BottomIndicatorBar(
            items: homeItems,
            currentIndex: tabsRouter.activeIndex,
            indicatorColor: ColorName.textMainColor,
            onTap: (index) {
              tabsRouter.setActiveIndex(index);
              context
                  .read<HomeTabsBloc>()
                  .add(HomeTabChanged(currentTabIndex: index));
            },
          ),
          child: child,
        );
      },
    );
  }
}

class UtilitiesBadge extends StatelessWidget {
  const UtilitiesBadge({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SmsRegisterBloc, SmsRegisterTotalState>(
      builder: (context, state) {
        if (state.isRegister) {
          return const SizedBox.shrink();
        } else {
          return Container(
            padding: EdgeInsets.symmetric(
              horizontal: 1.w,
              vertical: 1.h,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6.r),
              border: Border.all(color: Colors.white, width: 1.w),
              color: ColorName.primaryGradientStart,
            ),
            constraints: BoxConstraints(
              minWidth: 14.w,
              minHeight: 14.h,
            ),
            child: const BlinkingText(text: 'Đăng ký SMS', fontSize: 9,),
          );
        }
      },
    );
  }
}

class NotificationBadge extends StatelessWidget {
  final bool isActive;

  const NotificationBadge({
    Key? key,
    this.isActive = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 4,
        vertical: 1,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(color: Colors.white, width: 1.0),
        // color: isActive
        //     ? ColorName.primaryGradientStart
        //     : ColorName.primaryGradientEnd,
        color: ColorName.primaryGradientStart,
      ),
      constraints: const BoxConstraints(
        minWidth: 14,
        minHeight: 14,
      ),
      child: context.select((NotificationBloc bloc) => bloc.state.totalUnread) <
              99
          ? AnimatedFlipCounter(
              value: context
                  .select((NotificationBloc bloc) => bloc.state.totalUnread),
              duration: const Duration(milliseconds: 500),
              textStyle: const TextStyle(
                color: Colors.white,
                fontSize: 10,
              ),
            )
          : const Text(
              '99+',
              style: TextStyle(
                color: Colors.white,
                fontSize: 10,
              ),
            ),
    );
  }
}
