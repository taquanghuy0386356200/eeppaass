import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../commons/services/zalopay/zalo_pay_create_order.dart';
import '../../../commons/services/zalopay/zalopay_create_order_request.dart';
import 'cash_in_bloc.dart';

part 'zalopay_gate_event.dart';

part 'zalopay_gate_state.dart';

class ZaloPayGateBloc extends Bloc<ZaloPayGateEvent, ZaloPayGateState> {
  final ZaloPayCreateOrderService _zalopayCreateOrderService;

  ZaloPayGateBloc(
      {required ZaloPayCreateOrderService zalopayCreateOrderService})
      : _zalopayCreateOrderService = zalopayCreateOrderService,
        super(ZaloPayInitial()) {
    on<ZaloPayPaymentProceeded>(_onZaloPayCreateOrder);
  }

  FutureOr<void> _onZaloPayCreateOrder(
    ZaloPayPaymentProceeded event,
    Emitter<ZaloPayGateState> emit,
  ) async {
    final zalopayCreateOrderRequest = ZaloPayCreateOrderRequest(
      amount: event.amount,
    );

    final result =
        await _zalopayCreateOrderService.createOrder(zalopayCreateOrderRequest);
    if (result == null) {
      emit(const ZaloPayPaymentFailure(''));
    } else {
      emit(ZaloPayPaymentSuccess(zptranstoken: result.zptranstoken));
    }
  }
}
