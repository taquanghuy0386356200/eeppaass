part of 'transaction_vehicle_bloc.dart';

@immutable
abstract class TransactionVehicleEvent extends Equatable {
  const TransactionVehicleEvent();
}

class TransactionVehicleFetched extends TransactionVehicleEvent {
  const TransactionVehicleFetched();

  @override
  List<Object?> get props => [];
}

class TransactionVehicleRefreshed extends TransactionVehicleEvent {
  const TransactionVehicleRefreshed();

  @override
  List<Object?> get props => [];
}

class TransactionVehicleLoadMore extends TransactionVehicleEvent {
  const TransactionVehicleLoadMore();

  @override
  List<Object?> get props => [];
}

class TransactionVehicleSearched extends TransactionVehicleEvent {
  final String? plateNumber;
  final DateTime timestampInFrom;
  final DateTime timestampInTo;

  const TransactionVehicleSearched({
    this.plateNumber,
    required this.timestampInFrom,
    required this.timestampInTo,
  });

  @override
  List<Object?> get props => [plateNumber, timestampInFrom, timestampInTo];
}

class TransactionVehicleDateRangeChanged extends TransactionVehicleEvent {
  final DateTimeRange timestampInRange;

  const TransactionVehicleDateRangeChanged(this.timestampInRange);

  @override
  List<Object> get props => [timestampInRange];
}

class TransactionVehiclePlateNumberChanged extends TransactionVehicleEvent {
  final String plateNumber;

  const TransactionVehiclePlateNumberChanged(this.plateNumber);

  @override
  List<Object> get props => [plateNumber];
}
