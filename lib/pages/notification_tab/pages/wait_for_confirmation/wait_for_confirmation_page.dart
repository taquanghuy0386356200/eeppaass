import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/container/container_icon.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/bloc/wait_for_confirmation/wait_for_confirmation_bloc.dart';
import 'package:epass/pages/notification_tab/pages/event/widget/event_notification_card.dart';
import 'package:epass/pages/notification_tab/pages/event/widget/event_notification_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/gen/assets.gen.dart';

class WaitForConfirmationPage extends StatefulWidget with AutoRouteWrapper {
  const WaitForConfirmationPage({Key? key}) : super(key: key);

  @override
  State<WaitForConfirmationPage> createState() =>
      _WaitForConfirmationPageState();
  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => WaitForConfirmationBloc(
            userRepository: getIt<IUserRepository>(),
          ),
        ),
      ],
      child: this,
    );
  }
}

class _WaitForConfirmationPageState extends State<WaitForConfirmationPage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    context.read<WaitForConfirmationBloc>().add(ListWaitingOrderFetched());
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<WaitForConfirmationBloc, WaitForConfirmationState>(
      listener: (context, state) {
        if (state.message == null) {
          _refreshController.refreshCompleted();
          _refreshController.loadComplete();
        } else if (state.message != null) {
          _refreshController.refreshFailed();
          _refreshController.loadFailed();

          if (state.waitingOrders!.isNotEmpty) {
            showErrorSnackBBar(
              context: context,
              message: state.message ?? 'Có lỗi xảy ra',
            );
          }
        } else {
          showErrorSnackBBar(
            context: context,
            message: state.message ?? 'Có lỗi xảy ra',
          );
        }
      },
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 24.h),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 16.w),
            //   child: PrimaryTextField(
            //     hintText: 'Tìm kiếm',
            //     onChanged: (value) => context
            //         .read<WaitForConfirmationBloc>()
            //         .add(ListWaitingOrderFetched()),
            //     suffix: Assets.icons.search.svg(
            //       width: 24.r,
            //       height: 24.r,
            //       color: ColorName.borderColor,
            //     ),
            //   ),
            // ),
            // SizedBox(height: 16.h),
            Expanded(
              child: Builder(
                builder: (_) {
                  if (state.isLoading ||
                      state.isRefreshing && state.waitingOrders != null) {
                    // return const EventNotificationShimmerLoading();
                  } else if (state.message != null &&
                      state.waitingOrders!.isEmpty) {
                    return Center(
                      child: Text(state.message ?? 'Có lỗi xảy ra'),
                    );
                  }
                  final listData = state.waitingOrders;
                  if (listData == null) {
                    return NoDataPage(
                      onTap: () => context
                          .read<WaitForConfirmationBloc>()
                          .add(ListWaitingOrderFetched()),
                      description: 'Bạn không có giao dịch nào chờ xác thực',
                      title: '',
                    );
                  }

                  return SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    onRefresh: () => context
                        .read<WaitForConfirmationBloc>()
                        .add(ListWaitingOrderFetched()),
                    onLoading: () => context
                        .read<WaitForConfirmationBloc>()
                        .add(ListWaitingOrderFetched()),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        final data = listData[index];
                        return InkWell(
                          onTap: () {
                            showModalBottomSheet<void>(
                              context: context,
                              builder: (dialogContext) {
                                return Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 20.w),
                                  height: 150,
                                  color: Colors.white,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      InkWell(
                                          onTap: () {
                                            Navigator.pop(dialogContext);

                                            context
                                                .read<WaitForConfirmationBloc>()
                                                .add(WaitingOrderConfirm(
                                                    saleOrderId:
                                                        data.saleOrderId));
                                          },
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 14),
                                            child: Row(
                                              children: [
                                                // ignore: prefer_const_constructors
                                                Icon(
                                                  Icons.done,
                                                  color: ColorName.success,
                                                  size: 30.0,
                                                ),
                                                SizedBox(
                                                  width: 34.w,
                                                ),
                                                Text(
                                                  'Xác nhận',
                                                  style: Theme.of(dialogContext)
                                                      .textTheme
                                                      .bodyText1!
                                                      .copyWith(
                                                        color:
                                                            ColorName.textGray1,
                                                      ),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                )
                                              ],
                                            ),
                                          )),
                                      // ignore: prefer_const_constructors
                                      Divider(
                                        color: ColorName.borderColor,
                                      ),
                                      InkWell(
                                          onTap: () {
                                            Navigator.pop(dialogContext);
                                            context
                                                .read<WaitForConfirmationBloc>()
                                                .add(WaitingOrderCancel(
                                                    saleOrderId:
                                                        data.saleOrderId));
                                          },
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 14),
                                            child: Row(
                                              children: [
                                                // ignore: prefer_const_constructors
                                                Icon(
                                                  Icons.close,
                                                  color: ColorName.error,
                                                  size: 30.0,
                                                ),
                                                SizedBox(
                                                  width: 34.w,
                                                ),
                                                Text(
                                                  'Hủy xác nhận',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1!
                                                      .copyWith(
                                                        color:
                                                            ColorName.textGray1,
                                                      ),
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                )
                                              ],
                                            ),
                                          )),
                                      // ignore: prefer_const_constructors
                                      Divider(
                                        color: ColorName.borderColor,
                                      ),
                                    ],
                                  ),
                                );
                              },
                            );
                          },
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 16.w,
                              ),
                              ContainerIcon(
                                backgroundColor: ColorName.foam,
                                icon: Assets.icons.megaphone.svg(),
                              ),
                              SizedBox(
                                width: 16.w,
                              ),
                              Expanded(
                                flex: 8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      data.serviceName ?? '',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline6!
                                          .copyWith(
                                            color: ColorName.textGray1,
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                          ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    SizedBox(height: 4.h),
                                    Text(
                                      data.checkinTime ?? '',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                            color: ColorName.borderColor,
                                          ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    SizedBox(height: 12.h),
                                    Text(
                                      'Biển số: ${data.plateNumber}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            color: ColorName.textGray1,
                                          ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    SizedBox(height: 2.h),
                                    Text(
                                      'Tuyến đường: ${data.stationName}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            color: ColorName.textGray1,
                                          ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    SizedBox(height: 2.h),
                                    Text(
                                      'Giá tiền: ${data.amount} VNĐ',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            color: ColorName.textGray1,
                                          ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                              ),
                              const Spacer(),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                child: Icon(Icons.more_horiz_outlined),
                              ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (context, index) => Divider(
                        color: ColorName.disabledBorderColor,
                        height: 0.5,
                        thickness: 0.5,
                        indent: 60.w,
                        endIndent: 24.w,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
