/// fileBase64 : ""
/// fileName : ""
/// documentTypeId : ""

class DocumentFile {
  DocumentFile({
    this.fileBase64,
    this.fileName,
    this.documentTypeId,
  });

  final String? fileBase64;
  final String? fileName;
  final String? documentTypeId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fileBase64'] = fileBase64;
    map['fileName'] = fileName;
    map['documentTypeId'] = documentTypeId;
    return map;
  }
}
