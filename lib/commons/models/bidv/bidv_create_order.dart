import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'bidv_create_order.g.dart';

@JsonSerializable()
class BidvCreateOrderRequest {
  final String data;

  BidvCreateOrderRequest({
    required this.data,
  });

  factory BidvCreateOrderRequest.fromJson(Map<String, dynamic> json) =>
      _$BidvCreateOrderRequestFromJson(json);

  Map<String, dynamic> toJson() => _$BidvCreateOrderRequestToJson(this);
}

@JsonSerializable()
class BidvCreateOrderResponse {
  @JsonKey(name: "data")
  final BIDVCreateOrderData data;
  final MessageData? mess;

  BidvCreateOrderResponse({
    required this.data,
    required this.mess,
  });

  factory BidvCreateOrderResponse.fromJson(Map<String, dynamic> json) =>
      _$BidvCreateOrderResponseFromJson(json);
}

@JsonSerializable()
class BIDVCreateOrderData {
  final String transId;
  final String responseCode;

  BIDVCreateOrderData({required this.transId, required this.responseCode});

  factory BIDVCreateOrderData.fromJson(Map<String, dynamic> json) =>
      _$BIDVCreateOrderDataFromJson(json);
}
