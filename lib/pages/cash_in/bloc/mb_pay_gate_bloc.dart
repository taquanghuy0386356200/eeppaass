import 'dart:async';

import 'package:epass/commons/models/mb_bank/mb_check_payment_result.dart';
import 'package:epass/commons/models/mb_bank/mb_check_sum.dart';
import 'package:epass/commons/models/mb_bank/mb_create_order.dart';
import 'package:epass/commons/models/mb_bank/mb_get_token.dart';
import 'package:epass/commons/services/mbbank/mb_check_payment_result_service.dart';
import 'package:epass/commons/services/mbbank/mb_check_sum_service.dart';
import 'package:epass/commons/services/mbbank/mb_get_token_service.dart';
import 'package:epass/commons/services/mbbank/mb_payment_request.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'mb_pay_gate_event.dart';
part 'mb_pay_gate_state.dart';

class MBPayGateBloc extends Bloc<MBPayGateEvent, MBPayGateState> {
  final MBGetTokenService _getTokenService;
  final MBCreateOrderService _createOrderService;
  final MBCheckSumService _checkSumService;
  final MBCheckPayResultService _checkPayResultService;

  MBPayGateBloc({
    required MBGetTokenService getTokenService,
    required MBCreateOrderService createOrderService,
    required MBCheckSumService checkSumService,
    required MBCheckPayResultService checkPayResultService,
  })  : _getTokenService = getTokenService,
        _createOrderService = createOrderService,
        _checkSumService = checkSumService,
        _checkPayResultService = checkPayResultService,
        super(MBPayGateInitial()) {
    on<MBGetTokenCompleted>(_onMBGetTokenCompleted);
    on<MBCreateOrderCompleted>(_onMBCreateOrderCompleted);
    on<MBCheckSumCompleted>(_onMBCheckSumCompleted);
    on<MBCheckPaymentResultCompleted>(_onMBCheckPaymentResultCompleted);
  }

  FutureOr<void> _onMBGetTokenCompleted(
      MBGetTokenCompleted event, Emitter<MBPayGateState> emit) async {
    final mbGetTokenRequest = MBGetTokenRequest(grant_type: event.grantType);

    final result = await _getTokenService.getToken(mbGetTokenRequest);

    if (result == null) {
      emit(const MBGetTokenCompletedFailure('Có lỗi khi kết nối với MB Bank'));
    } else {
      emit(MBGetTokenCompletedSuccess(
          accessToken: result.access_token,
          tokenType: result.token_type,
          expiresIn: result.expires_in,
          issuedAt: result.issued_at));
    }
  }

  FutureOr<void> _onMBCreateOrderCompleted(
      MBCreateOrderCompleted event, Emitter<MBPayGateState> emit) async {
    final mbCreateOrderRequest = MBCreateOrderRequest(
        transAmt: event.amount, transactionId: event.transactionId);

    final result = await _createOrderService.createOrder(
        mbCreateOrderRequest, event.token);

    if (result == null) {
      emit(const MBCreateOrderCompletedFailure('Tạo thanh toán thất bại'));
    } else {
      emit(MBCreateOrderCompletedSuccess(
          clientMessageId: result.clientMessageId,
          errorCode: result.errorCode,
          data: result.data));
    }
  }

  FutureOr<void> _onMBCheckSumCompleted(
      MBCheckSumCompleted event, Emitter<MBPayGateState> emit) async {
    final mbCheckSumRequest =
        MBCheckSumRequest(checkSumKey: event.checkSumKey, token: event.token);

    final result =
        await _checkSumService.checkSum(mbCheckSumRequest, event.token);

    if (result == null) {
      emit(const MBCheckSumCompletedFailure('Có lỗi khi kiểm tra thanh toán'));
    } else {
      emit(MBCheckSumCompletedSuccess(
        transactionID: result.data.transactionID,
        clientMessageId: result.clientMessageId,
        errorCode: result.errorCode!,
      ));
    }
  }

  FutureOr<void> _onMBCheckPaymentResultCompleted(
      MBCheckPaymentResultCompleted event, Emitter<MBPayGateState> emit) async {
    final mbCheckPayResultRequest = MBCheckPaymentResultRequest(
        transactionId: event.transactionId, token: event.token);

    final result = await _checkPayResultService.checkPayResult(
        mbCheckPayResultRequest, event.token);

    if (result == null || result.data.status == 'FAIL') {
      emit(const MBCheckPaymentResultCompletedFailure('Thanh toán thất bại'));
    } else {
      emit(MBCheckPaymentResultCompletedSuccess(
          clientMessageId: result.clientMessageId,
          status: result.data.status,
          errorCode: result.errorCode!));
    }
  }
}
