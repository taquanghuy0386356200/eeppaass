import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/contract_encrypted/insurance_encrypted.dart';
import 'package:simple_result/simple_result.dart';

abstract class IInsuranceRepository {
  Future<Result<int, Failure>> getCountInsurance();

  Future<Result<ContractEncrypted, Failure>> getContractEncrypted();
}

class InsuranceRepository implements IInsuranceRepository {
  final CrmClient _crmClient;

  const InsuranceRepository({required CrmClient crmClient})
      : _crmClient = crmClient;

  @override
  Future<Result<int, Failure>> getCountInsurance() async {
    try {
      final response = await _crmClient.getCountInsurance();

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          response.data == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data!);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ContractEncrypted, Failure>> getContractEncrypted() async {
    try {
      final response = await _crmClient.contractEncrypted();

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          response.data == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data!);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
