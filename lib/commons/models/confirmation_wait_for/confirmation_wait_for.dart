import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'confirmation_wait_for.g.dart';

@JsonSerializable()
class ConfirmationWaitForRequest {
  int? saleOrderId;

  ConfirmationWaitForRequest({this.saleOrderId});
  factory ConfirmationWaitForRequest.fromJson(Map<String, dynamic> json) =>
      _$ConfirmationWaitForRequestFromJson(json);
  Map<String, dynamic> toJson() => _$ConfirmationWaitForRequestToJson(this);
}
