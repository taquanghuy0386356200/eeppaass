part of 'feedback_form_bloc.dart';

abstract class FeedbackFormEvent extends Equatable {
  const FeedbackFormEvent();
}

class FeedbackTypeChanged extends FeedbackFormEvent {
  final FeedbackType? feedbackType;

  const FeedbackTypeChanged(this.feedbackType);

  @override
  List<Object?> get props => [feedbackType];
}

class ReceiveContentChanged extends FeedbackFormEvent {
  final String? content;

  const ReceiveContentChanged(this.content);

  @override
  List<Object?> get props => [content];
}

class FeedbackAttachmentFilesChanged extends FeedbackFormEvent {
  final List<String>? attachments;

  const FeedbackAttachmentFilesChanged(this.attachments);

  @override
  List<Object?> get props => [attachments];
}


