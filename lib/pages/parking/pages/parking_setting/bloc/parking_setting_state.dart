part of 'parking_setting_bloc.dart';

class ParkingSettingState extends Equatable {
  final List<ServiceModel> services;
  final List<VerifyConfigModel> serviceVerifyConfig;
  final List<VerifyConfigModel> verifyConfigs;
  final String? error;
  final String? success;
  final bool isLoading;
  final bool isOn;
  final bool isOnRoadSide;

  final bool isPolicyShowing;
  final bool isNotAllowedToUseService;
  final bool isPassCodeSetting;
  final bool isOtpConfirming;
  final bool isDoing;
  final int confirmType;
  final VerifyConfigModel? activeVerifyConfig; // For background use only
  final bool isActiveVerifyConfigOn; // For background use only
  final String newPassCode;
  final bool isOtpSuccess;

  const ParkingSettingState(
      {required this.services,
      required this.serviceVerifyConfig,
      required this.verifyConfigs,
      this.error,
      this.success,
      this.isLoading = false,
      this.isOn = false,
      this.isOnRoadSide = false,
      this.isPolicyShowing = false,
      this.isNotAllowedToUseService = false,
      this.isPassCodeSetting = false,
      this.isOtpConfirming = false,
      this.isDoing = false,
      this.confirmType = 19,
      this.activeVerifyConfig,
      this.isActiveVerifyConfigOn = false,
      this.newPassCode = '',
      this.isOtpSuccess = false});

  @override
  List<Object?> get props => [
        services,
        serviceVerifyConfig,
        verifyConfigs,
        error,
        success,
        isLoading,
        isOn,
        isOnRoadSide,
        isPolicyShowing,
        isNotAllowedToUseService,
        isDoing,
        isPassCodeSetting,
        isOtpConfirming,
        confirmType,
        activeVerifyConfig,
        isActiveVerifyConfigOn,
        newPassCode,
        isOtpSuccess
      ];

  ParkingSettingState copyWith(
      {List<ServiceModel>? services,
      List<VerifyConfigModel>? serviceVerifyConfig,
      List<VerifyConfigModel>? verifyConfigs,
      required String? error,
      String? success,
      bool? isLoading,
      bool? isOn,
      bool? isOnRoadSide,
      bool? isPolicyShowing,
      bool? isNotAllowedToUseService,
      bool? isPassCodeSetting,
      bool? isOtpConfirming,
      bool? isDoing,
      int? confirmType,
      VerifyConfigModel? activeVerifyConfig,
      bool? isActiveVerifyConfigOn,
      String? newPassCode,
      bool? isOtpSuccess}) {
    return ParkingSettingState(
        services: services ?? this.services,
        serviceVerifyConfig: serviceVerifyConfig ?? this.serviceVerifyConfig,
        verifyConfigs: verifyConfigs ?? this.verifyConfigs,
        error: error,
        success: success,
        isLoading: isLoading ?? this.isLoading,
        isOn: isOn ?? this.isOn,
        isOnRoadSide: isOnRoadSide ?? this.isOnRoadSide,
        isPolicyShowing: isPolicyShowing ?? this.isPolicyShowing,
        isNotAllowedToUseService:
            isNotAllowedToUseService ?? this.isNotAllowedToUseService,
        isPassCodeSetting: isPassCodeSetting ?? this.isPassCodeSetting,
        isOtpConfirming: isOtpConfirming ?? this.isOtpConfirming,
        isDoing: isDoing ?? this.isDoing,
        confirmType: confirmType ?? this.confirmType,
        activeVerifyConfig: activeVerifyConfig ?? this.activeVerifyConfig,
        isActiveVerifyConfigOn:
            isActiveVerifyConfigOn ?? this.isActiveVerifyConfigOn,
        newPassCode: newPassCode ?? this.newPassCode,
        isOtpSuccess: isOtpSuccess ?? false);
  }
}
