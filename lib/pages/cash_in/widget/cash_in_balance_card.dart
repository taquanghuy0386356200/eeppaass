import 'package:epass/commons/widgets/balance_alt_widget.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CashInBalanceCard extends StatelessWidget {
  const CashInBalanceCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 94.h,
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: ShadowCard(
        shadowOpacity: 0.3,
        child: Padding(
          padding: EdgeInsets.fromLTRB(24.w, 10.h, 20.w, 10.h),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Số dư ví:',
                        style: Theme.of(context).textTheme.subtitle2!.copyWith(
                              fontWeight: FontWeight.w600,
                              color: ColorName.borderColor,
                              fontSize: 15.sp,
                            ),
                      ),
                      SizedBox(height: 4.h),
                      const BalanceAltWidget(),
                    ],
                  ),
                ),
              ),
              // Assets.images.cashInWallet.svg(),
            ],
          ),
        ),
      ),
    );
  }
}
