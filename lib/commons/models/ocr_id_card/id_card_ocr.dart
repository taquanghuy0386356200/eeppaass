import 'package:json_annotation/json_annotation.dart';

/// address : "48Nguyễn Thái Học, P.Chi Lăng, TP.Lạng Sơn, Lạng Sơn"
/// birthday : "20-08-1996"
/// birthplace : "Nam Định"
/// card_extraction_confidence : 0.94
/// classification_confidence : 0.99
/// complete_card_confidence : 1.0
/// confidence : "0.93"
/// district : "001"
/// districtName : "Lạng Sơn"
/// document : "OLD ID"
/// ethnicity : ""
/// expiry : ""
/// id : "082265954"
/// id_check : "REAL"
/// id_logic : 1
/// id_logic_message : ""
/// id_spoof_confidence : 0
/// id_spoof_confident : 0
/// id_type : 0
/// idconf : "['0.99', '0.99', '0.99', '0.99', '0.99', '0.99', '0.99', '0.99', '0.99']"
/// is_binary : false
/// is_complete_card : true
/// issue_by : ""
/// issue_date : ""
/// name : "PHẠM MẠNH HIẾU"
/// name_confidence : "[0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99, 0.99]"
/// nationality : ""
/// precinct : "004"
/// precinctName : "Chi Lăng"
/// province : "L025"
/// provinceName : "Lạng Sơn"
/// quality image : ""
/// quality_image_confidence : 1.0
/// religion : ""
/// result_code : 200
/// sex : ""
/// street : ""
/// street_name : "48Nguyễn Thái Học"

part 'id_card_ocr.g.dart';

@JsonEnum(alwaysCreate: true)
enum IdType {
  @JsonValue(0)
  front,
  @JsonValue(1)
  back,
  unknown,
}

@JsonSerializable(fieldRename: FieldRename.snake)
class IdCardOcr {
  IdCardOcr({
    this.address,
    this.birthday,
    this.birthplace,
    this.cardExtractionConfidence,
    this.classificationConfidence,
    this.completeCardConfidence,
    this.confidence,
    this.district,
    this.districtName,
    this.document,
    this.ethnicity,
    this.expiry,
    this.id,
    this.idCheck,
    this.idLogic,
    this.idLogicMessage,
    this.idSpoofConfidence,
    this.idSpoofConfident,
    this.idType,
    this.idconf,
    this.isBinary,
    this.isCompleteCard,
    this.issueBy,
    this.issueDate,
    this.name,
    this.nameConfidence,
    this.nationality,
    this.precinct,
    this.precinctName,
    this.province,
    this.provinceName,
    this.qualityImageConfidence,
    this.religion,
    this.resultCode,
    this.message,
    this.sex,
    this.street,
    this.streetName,
  });

  final String? address;
  final String? birthday;
  final String? birthplace;
  final double? cardExtractionConfidence;
  final double? classificationConfidence;
  final double? completeCardConfidence;
  final String? confidence;
  final String? district;
  final String? districtName;
  final String? document;
  final String? ethnicity;
  final String? expiry;
  final String? id;
  final String? idCheck;
  final int? idLogic;
  final String? idLogicMessage;
  final int? idSpoofConfidence;
  final int? idSpoofConfident;

  @JsonKey(unknownEnumValue: IdType.unknown)
  final IdType? idType;

  final String? idconf;
  final bool? isBinary;
  final bool? isCompleteCard;
  final String? issueBy;
  final String? issueDate;
  final String? name;
  final String? nameConfidence;
  final String? nationality;
  final String? precinct;
  final String? precinctName;
  final String? province;
  final String? provinceName;
  final double? qualityImageConfidence;
  final String? religion;
  final int? resultCode;
  final String? message;
  final String? sex;
  final String? street;
  final String? streetName;

  factory IdCardOcr.fromJson(Map<String, dynamic> json) => _$IdCardOcrFromJson(json);

  IdCardOcr copyWith({required IdCardOcr idCard}) {
    return IdCardOcr(
      address: idCard.address?.isEmpty ?? true ? address : idCard.address,
      birthday: idCard.birthday?.isEmpty ?? true ? birthday : idCard.birthday,
      birthplace: idCard.birthplace?.isEmpty ?? true ? birthplace : idCard.birthplace,
      cardExtractionConfidence: idCard.cardExtractionConfidence ?? cardExtractionConfidence,
      classificationConfidence: idCard.classificationConfidence ?? classificationConfidence,
      completeCardConfidence: idCard.completeCardConfidence ?? completeCardConfidence,
      confidence: idCard.confidence?.isEmpty ?? true ? confidence : idCard.confidence,
      district: idCard.district?.isEmpty ?? true ? district : idCard.district,
      districtName: idCard.districtName?.isEmpty ?? true ? districtName : idCard.districtName,
      document: idCard.document?.isEmpty ?? true ? document : idCard.document,
      ethnicity: idCard.ethnicity?.isEmpty ?? true ? ethnicity : idCard.ethnicity,
      expiry: idCard.expiry?.isEmpty ?? true ? expiry : idCard.expiry,
      id: idCard.id?.isEmpty ?? true ? id : idCard.id,
      idCheck: idCard.idCheck?.isEmpty ?? true ? idCheck : idCard.idCheck,
      idLogic: idCard.idLogic ?? idLogic,
      idLogicMessage: idCard.idLogicMessage?.isEmpty ?? true ? idLogicMessage : idCard.idLogicMessage,
      idSpoofConfidence: idCard.idSpoofConfidence ?? idSpoofConfidence,
      idSpoofConfident: idCard.idSpoofConfident ?? idSpoofConfident,
      idType: idCard.idType ?? idType,
      idconf: idCard.idconf?.isEmpty ?? true ? idconf : idCard.idconf,
      isBinary: idCard.isBinary ?? isBinary,
      isCompleteCard: idCard.isCompleteCard ?? isCompleteCard,
      issueBy: idCard.issueBy?.isEmpty ?? true ? issueBy : idCard.issueBy,
      issueDate: idCard.issueDate?.isEmpty ?? true ? issueDate : idCard.issueDate,
      name: idCard.name?.isEmpty ?? true ? name : idCard.name,
      nameConfidence: idCard.nameConfidence?.isEmpty ?? true ? nameConfidence : idCard.nameConfidence,
      nationality: idCard.nationality?.isEmpty ?? true ? nationality : idCard.nationality,
      precinct: idCard.precinct?.isEmpty ?? true ? precinct : idCard.precinct,
      precinctName: idCard.precinctName?.isEmpty ?? true ? precinctName : idCard.precinctName,
      province: idCard.province?.isEmpty ?? true ? province : idCard.province,
      provinceName: idCard.provinceName?.isEmpty ?? true ? provinceName : idCard.provinceName,
      qualityImageConfidence: idCard.qualityImageConfidence ?? qualityImageConfidence,
      religion: idCard.religion?.isEmpty ?? true ? religion : idCard.religion,
      resultCode: idCard.resultCode ?? resultCode,
      message: idCard.message?.isEmpty ?? true ? message : idCard.message,
      sex: idCard.sex?.isEmpty ?? true ? sex : idCard.sex,
      street: idCard.street?.isEmpty ?? true ? street : idCard.street,
      streetName: idCard.streetName?.isEmpty ?? true ? streetName : idCard.streetName,
    );
  }
}
