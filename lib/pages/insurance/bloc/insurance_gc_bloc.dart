import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/insurance_repository.dart';
import 'package:equatable/equatable.dart';

part 'insurance_gc_event.dart';

part 'insurance_gc_state.dart';

class InsuranceGcBloc extends Bloc<InsuranceGcEvent, InsuranceGcState> {
  final IInsuranceRepository _insuranceRepository;

  InsuranceGcBloc({required IInsuranceRepository insuranceRepository})
      : _insuranceRepository = insuranceRepository,
        super(InsuranceGcInitial()) {
    on<ContractEncryptedFetched>(_onContractEncryptedFetched);
  }

  FutureOr<void> _onContractEncryptedFetched(
    ContractEncryptedFetched event,
    Emitter<InsuranceGcState> emit,
  ) async {
    final result = await _insuranceRepository.getContractEncrypted();

    result.when(
      success: (success) => emit(ContractEncryptedFetchedSuccess(success.contractNoEncrypted ?? '')),
      failure: (failure) => emit(ContractEncryptedFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
