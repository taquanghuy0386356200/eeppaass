// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mb_check_payment_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MBCheckPaymentResultRequest _$MBCheckPaymentResultRequestFromJson(
        Map<String, dynamic> json) =>
    MBCheckPaymentResultRequest(
      transactionId: json['transactionId'] as String,
      token: json['token'] as String,
    );

Map<String, dynamic> _$MBCheckPaymentResultRequestToJson(
        MBCheckPaymentResultRequest instance) =>
    <String, dynamic>{
      'transactionId': instance.transactionId,
      'token': instance.token,
    };

MBCheckPaymentResultResponse _$MBCheckPaymentResultResponseFromJson(
        Map<String, dynamic> json) =>
    MBCheckPaymentResultResponse(
      clientMessageId: json['clientMessageId'] as String,
      errorCode: json['errorCode'] as String?,
      data: MBPaymentResultData.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MBCheckPaymentResultResponseToJson(
        MBCheckPaymentResultResponse instance) =>
    <String, dynamic>{
      'clientMessageId': instance.clientMessageId,
      'errorCode': instance.errorCode,
      'data': instance.data,
    };

MBPaymentResultData _$MBPaymentResultDataFromJson(Map<String, dynamic> json) =>
    MBPaymentResultData(
      status: json['status'] as String,
      data: json['data'] as String,
    );

Map<String, dynamic> _$MBPaymentResultDataToJson(
        MBPaymentResultData instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };
