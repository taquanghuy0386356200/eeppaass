import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_response.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:equatable/equatable.dart';

class CartItem extends Equatable {
  final TicketPrice ticketPrice;
  final Vehicle vehicle;
  final StationStage stationStage;
  final ServicePlanType servicePlanType;
  final DateTime effDate;
  final DateTime expDate;

  const CartItem({
    required this.ticketPrice,
    required this.vehicle,
    required this.stationStage,
    required this.servicePlanType,
    required this.effDate,
    required this.expDate,
  });

  @override
  List<Object> get props => [
        ticketPrice,
        vehicle,
        stationStage,
        servicePlanType,
        effDate,
        expDate,
      ];
}
