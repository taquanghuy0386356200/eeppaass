import 'package:shared_preferences/shared_preferences.dart';

class PrefsService {
  static const _PREF_COUNT_REJECT_PERMISSION = '_PREF_COUNT_REJECT_PERMISSION';
  static const _PREF_APP_VERSION = '_PREF_APP_VERSION';
  static const _PREF_CHECK_TIME_SHOW_REGIS_SMS =
      '_PREF_CHECK_TIME_SHOW_REGIS_SMS';

  static SharedPreferences? _preferencesInstance;

  static Future<SharedPreferences> get _instancePrefs async =>
      _preferencesInstance ??= await SharedPreferences.getInstance();

  static Future<SharedPreferences> init() async {
    _preferencesInstance = await _instancePrefs;
    return _preferencesInstance!;
  }

  static Future<bool> saveLastShowSMS(int value) async {
    final prefs = await _instancePrefs;
    return prefs.setString(
      _PREF_CHECK_TIME_SHOW_REGIS_SMS,
      value.toString(),
    );
  }

  static Future<int> getDateSavePopupSMS() async {
    final prefs = await _instancePrefs;
    final result =
        prefs.getString(_PREF_CHECK_TIME_SHOW_REGIS_SMS) ?? '0';
    final resultFinal = int.parse(result);
    return resultFinal;
  }

  static Future<bool> saveCountReject(int value) async {
    final result = value.toString();
    final prefs = await _instancePrefs;
    return prefs.setString(_PREF_COUNT_REJECT_PERMISSION, result);
  }

  static Future<bool> saveAppVersion(String value) async {
    final result = value.toString();
    final prefs = await _instancePrefs;
    return prefs.setString(_PREF_APP_VERSION, result);
  }

  static Future<int> readCountReject() async {
    final result =
        _preferencesInstance?.getString(_PREF_COUNT_REJECT_PERMISSION) ?? '0';
    return int.parse(result);
  }
}
