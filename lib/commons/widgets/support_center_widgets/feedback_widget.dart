import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/widgets/support_center_widgets/feedback_vertical_widget.dart';
import 'package:flutter/material.dart';

import 'feedback_expanded_content.dart';

class FeedbackItem {
  final FeedbackModel feedback;
  bool isExpanded;

  FeedbackItem({
    required this.feedback,
    this.isExpanded = false,
  });
}

class FeedbackWidget extends StatefulWidget {
  final FeedbackItem feedbackItem;
  final void Function(bool)? onExpandToggle;

  const FeedbackWidget({
    Key? key,
    required this.feedbackItem,
    this.onExpandToggle,
  }) : super(key: key);

  @override
  State<FeedbackWidget> createState() => _FeedbackWidgetState();
}

class _FeedbackWidgetState extends State<FeedbackWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final feedback = widget.feedbackItem.feedback;

    return ExpansionPanelList(
      elevation: 0,
      expansionCallback: (int index, bool isExpanded) {
        widget.onExpandToggle?.call(!isExpanded);
        setState(() => {});
      },
      children: [
        ExpansionPanel(
          isExpanded: widget.feedbackItem.isExpanded,
          headerBuilder: (context, isExpanded) => FeedbackVerticalWidget(
            content: feedback.l1TicketTypeName ?? '',
          ),
          body: FeedbackExpandedContent(
            feedback: feedback,
          ),
        ),
      ],
    );
  }
}
