import 'package:epass/commons/services/zalopay/zalo_pay_constant.dart';
import '../../utils/zalo_pay/zalo_pay_utils.dart';
import 'package:sprintf/sprintf.dart';

class ZaloPayCreateOrderRequest {
  final String appId = ZaloPayConstant.appId;
  final String appUser = ZaloPayConstant.appUser;
  final String appTime = DateTime.now().millisecondsSinceEpoch.toString();
  final int amount;
  final String appTransId = getAppTransId();
  final String embedData = "{}";
  final String item = "[]";
  final String bankCode = getBankCode();
  final String description = getDescription(getAppTransId());

  String get mac {
    var dataGetMac = sprintf("%s|%s|%s|%s|%s|%s|%s",
        [appId, appTransId, appUser, amount, appTime, embedData, item]);
    return getMacCreateOrder(dataGetMac);
  }

  ZaloPayCreateOrderRequest({
    required this.amount,
  });
}
