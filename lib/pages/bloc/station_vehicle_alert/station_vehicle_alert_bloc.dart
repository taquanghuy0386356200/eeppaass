import 'dart:async';

import 'package:audiofileplayer/audiofileplayer.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/locals/prefs_service.dart';
import 'package:epass/commons/models/station/station_detail.dart';
import 'package:epass/commons/repo/alert_repository.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/services/permission/permission.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';

part 'station_vehicle_alert_event.dart';

part 'station_vehicle_alert_state.dart';

class StationVehicleAlertBloc
    extends HydratedBloc<StationVehicleAlertEvent, StationVehicleAlertState> {
  final IAlertRepository _alertRepository;
  final ICategoryRepository _categoryRepository;

  StreamSubscription<LocationData>? _locationStream;
  Timer? _timer;
  Audio? _audio;
  late int countReject;

  StationVehicleAlertBloc({
    required IAlertRepository alertRepository,
    required ICategoryRepository categoryRepository,
  })  : _alertRepository = alertRepository,
        _categoryRepository = categoryRepository,
        super(const StationVehicleAlertState()) {
    on<StationVehicleAlerted>(_onStationVehicleAlerted);
    getCountReject();
  }

  Future<int> getCountReject() async {
    return countReject = await PrefsService.readCountReject();
  }

  FutureOr<void> _onStationVehicleAlerted(
    StationVehicleAlerted event,
    emit,
  ) async {
    final isOn = event.isOn ?? state.isOn ?? false;

    emit(state.copyWith(
      isOn: isOn,
      isLoading: false,
      error: null,
    ));

    if (!isOn) {
      final location = Location();
      location.enableBackgroundMode(enable: false);
      _timer?.cancel();
      await _locationStream?.cancel();
      return;
    }

    // get permission
    final permissionStatus = await requestPermission(
      Permission.location,
    );

    await permissionStatus.when(
      success: (_) async {
        if (state.stationDetails?.isEmpty ?? true) {
          emit(state.copyWith(
            isLoading: true,
            error: null,
          ));

          // get state details
          final stateDetailsResult = await _alertRepository.getStationDetails();

          await stateDetailsResult.when(
            success: (success) async {
              // partially done
              emit(state.copyWith(
                isOn: isOn,
                stationDetails: success.listData,
                isLoading: false,
                error: null,
              ));

              // start the service
              await _startTrackingService(emit);
            },
            failure: (failure) {
              emit(state.copyWith(
                error: failure.message ?? 'Có lỗi xảy ra',
                isLoading: false,
                isOn: false,
              ));
            },
          );
        } else {
          // start the service
          await _startTrackingService(emit);
        }
      },
      failure: (failure) async {
        if (failure.message == 'Không có quyền truy cập') {
          //check < 3 do default quyền truy cập thiết bị chỉ hỏi 2 lần
          //lần thứ 3 sẽ manual vào
          if (countReject < 3) {
            countReject++;
            await PrefsService.saveCountReject(countReject);
          }
          await getCountReject();
          if (countReject > 2) {
            return emit(state.copyWith(
              isOn: false,
              isLoading: false,
              error: '',
              isOpenLocationPermission: true,
            ));
          } else {
            return emit(state.copyWith(
              isOn: false,
              isLoading: false,
              error: '',
            ));
          }
        }
        return emit(state.copyWith(
          isOn: false,
          isLoading: false,
          error: failure.message ?? 'Có lỗi xảy ra',
        ));
      },
    );
  }

  Future<void> _startTrackingService(emit) async {
    final tokenTTSResult = await _categoryRepository.getCategories(
      tableName: 'CONFIG_PARAM',
      code: 'TOKEN_TTS',
    );

    tokenTTSResult.when(
      success: (token) {
        final tokenTTS = token.listData
                .firstWhere((element) => element.code == 'TOKEN_TTS')
                .name ??
            '';

        var reachedStations = <StationDetail>[];

        final location = Location();

        location.enableBackgroundMode(enable: true);
        location.changeSettings(distanceFilter: 200, interval: 5000);

        _locationStream = location.onLocationChanged.listen((position) async {
          final lat = position.latitude, long = position.longitude;

          final stations = state.stationDetails ?? [];

          if (lat != null && long != null) {
            // for (var station in (state.stationDetails ?? [])) {
            for (var station in stations) {
              // check if reachedStations already contain station
              if (reachedStations
                  .where((element) => element.id == station.id)
                  .isEmpty) {
                _validateCheckpoint1(
                  warLat1: station.warLeftLat1,
                  warLong1: station.warLeftLong1,
                  lat: lat,
                  long: long,
                  callback: () => reachedStations.add(station),
                );

                _validateCheckpoint1(
                  warLat1: station.warRightLat1,
                  warLong1: station.warRightLong1,
                  lat: lat,
                  long: long,
                  callback: () => reachedStations.add(station),
                );
              }
            }

            var messages = <String?>[];

            for (var station in reachedStations) {
              final message1 = await _validateCheckpoint2(
                tokenTTS: tokenTTS,
                stationId: station.id,
                warLat2: station.warLeftLat2,
                warLong2: station.warLeftLong2,
                lat: lat,
                long: long,
              );

              final message2 = await _validateCheckpoint2(
                tokenTTS: tokenTTS,
                stationId: station.id,
                warLat2: station.warRightLat2,
                warLong2: station.warRightLong2,
                lat: lat,
                long: long,
              );

              messages.addAll([message1, message2]);
            }

            final filteredMessages =
                messages.where((element) => element != null).toList();

            if (filteredMessages.isNotEmpty) {
              messages.clear();
              reachedStations.clear();

              final message = filteredMessages.first!;

              final alertSuccess = await _textToSpeech(message, tokenTTS);

              if (alertSuccess) {
                _locationStream?.pause();
                _timer = Timer(const Duration(minutes: 30), () {
                  if (_locationStream?.isPaused ?? false) {
                    _locationStream?.resume();
                  }
                });
              }
            }
          }
        });
      },
      failure: (failure) {
        emit(state.copyWith(
          error: failure.message ?? 'Có lỗi xảy ra',
          isLoading: false,
          isOn: false,
        ));
      },
    );
  }

  Future<bool> _textToSpeech(String message, String tokenTTS) async {
    final getSpeechResult = await _alertRepository.getSpeech(
      text: message,
      ttsToken: tokenTTS,
    );

    // play sound
    final result = await getSpeechResult.when(
      success: (buffer) async {
        final bytes = Uint8List.fromList(buffer);
        final byteData = ByteData.view(bytes.buffer);
        _audio = Audio.loadFromByteData(byteData);
        await _audio?.play();
        await _audio?.dispose();
        return Future.value(true);
      },
      failure: (_) {
        return Future.value(false);
      },
    );

    return result;
  }

  Future<String?> _validateCheckpoint2({
    required int? stationId,
    required double? warLat2,
    required double? warLong2,
    required double lat,
    required double long,
    required String tokenTTS,
  }) async {
    if (warLat2 != null && warLong2 != null) {
      final distanceInMeters = const Distance()
          .as(LengthUnit.Meter, LatLng(lat, long), LatLng(warLat2, warLong2));
      if (distanceInMeters <= Constant.distanceRadius) {
        if (stationId != null) {
          final trackResult = await _alertRepository.vehicleAlert(
            stationId: stationId,
            lat: lat,
            long: long,
          );

          final message = await trackResult.when(
            success: (vehicleTrack) async {
              return vehicleTrack.message;
            },
            failure: (_) {
              return null;
            },
          );

          return message;
        }
      }
    }
    return null;
  }

  void _validateCheckpoint1({
    required double? warLat1,
    required double? warLong1,
    required double lat,
    required double long,
    required VoidCallback callback,
  }) {
    if (warLat1 != null && warLong1 != null) {
      final distanceInMeters = const Distance()
          .as(LengthUnit.Meter, LatLng(lat, long), LatLng(warLat1, warLong1));
      if (distanceInMeters <= Constant.distanceRadius) {
        callback();
      }
    }
  }

  @override
  StationVehicleAlertState? fromJson(Map<String, dynamic> json) {
    return state.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(StationVehicleAlertState state) {
    return state.toJson();
  }
}
