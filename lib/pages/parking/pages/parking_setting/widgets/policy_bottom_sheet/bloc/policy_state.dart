part of 'policy_bloc.dart';

class PolicyState extends Equatable {
  final bool isPolicyChecked;

  const PolicyState({this.isPolicyChecked = false});

  @override
  List<Object> get props => [isPolicyChecked];

  PolicyState copyWith({
    bool? isPolicyChecked
  }) {
    return PolicyState(
      isPolicyChecked: isPolicyChecked ?? this.isPolicyChecked
    );
  }
}
