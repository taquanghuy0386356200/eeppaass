import 'package:auto_route/auto_route.dart';
import 'package:car_doctor_epass_sos/car_doctor_epass_sos.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/avatar_widget.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home/widgets/balance_widget.dart';
import 'package:epass/pages/utility/car_doctor/car_doctor_bloc.dart';
import 'package:epass/pages/utility/car_doctor/car_doctor_fnc.dart';
import 'package:epass/pages/utility/widgets/utility_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class InfoCard extends StatelessWidget {
  final double shadowOpacity;

  const InfoCard({
    Key? key,
    required this.shadowOpacity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CarDoctorBloc(
          userRepository: getIt<IUserRepository>(),
          appBloc: context.read<AppBloc>()),
      child: BlocConsumer<CarDoctorBloc, CarDoctorState>(
          listener: (context, state) {
        if (state is GetAccessTokenSuccess) {
          context.loaderOverlay.hide();
          final option = CarDoctorOption(
            bearerToken: state.accessToken,
            apiUrl: dotenv.env['APIURL']!,
            imageUrl: dotenv.env['IMAGEURL']!,
            sosUrl: '',
            mapKey: dotenv.env['MAPKEY']!,
          );
          final sdkSos = CarDoctorSdkSos.instance;
          sdkSos.init(option);
          sdkSos.open(context);
        } else {
          context.loaderOverlay.hide();
        }
        if (state is GetConfigSuccess) {
          CarDoctorFnc.requestLocationPermission(
              context, state.config.name ?? '', 'SOS');
        }
      }, builder: (context, state) {
        return ShadowCard(
          shadowOpacity: shadowOpacity,
          child: Padding(
            padding: EdgeInsets.fromLTRB(10.w, 10.h, 10.w, 10.h),
            child: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  padding: EdgeInsets.only(top: 16.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 10.w),
                        child: Row(
                          children: [
                            InkWell(
                                onTap: () {
                                  AutoTabsRouter.of(context).setActiveIndex(
                                    3,
                                    notify: true,
                                  );
                                },
                                child: const AvatarWidget()),
                            SizedBox(width: 10.w),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Xin chào,',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2!
                                        .copyWith(color: ColorName.borderColor),
                                  ),
                                  SizedBox(height: 2.h),
                                  BlocSelector<AppBloc, AppState, String?>(
                                    selector: (state) => state.user?.userName,
                                    builder: (context, fullName) {
                                      return Container(
                                        constraints: const BoxConstraints(
                                            minWidth: 100, maxWidth: 175),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Text(
                                          (fullName ?? ''),
                                          //fix eclipse text
                                          // fullName == null
                                          //     ? ''
                                          //     : (fullName.length >= 55
                                          //         ? fullName
                                          //         : (fullName + ' ').padRight(
                                          //             55,
                                          //             String.fromCharCode(
                                          //                 0020),
                                          //           )),
                                          // (data.length >= 55
                                          //     ? data
                                          //     : (data + ' ').padRight(
                                          //         55,
                                          //         String.fromCharCode(0020),
                                          //       )),
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline5!
                                              .copyWith(
                                                fontWeight: FontWeight.w700,
                                                fontSize: 16.sp,
                                              ),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10.h,),
                      Padding(
                        padding: EdgeInsets.only(left: 10.w),
                        child: Text(
                          'Số dư ví:',
                          style:
                              Theme.of(context).textTheme.subtitle2!.copyWith(
                                    fontWeight: FontWeight.w600,
                                    color: ColorName.borderColor,
                                    fontSize: 15.sp,
                                  ),
                        ),
                      ),
                      const BalanceWidget(),
                    ],
                  ),
                ),
                Positioned.fill(
                  right: 0,
                  bottom: 25,
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: PrimaryButton(
                      padding: EdgeInsets.symmetric(vertical: 18.h),
                      paddingButton: 12,
                      // widthCustom: 115,
                      borderRadius: 15.0,
                      title: 'Nạp tiền',
                      onTap: () {
                        context.pushRoute(CashInRouter());
                      },
                    ),
                  ),
                ),
                Positioned.fill(
                    child: Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.all(1.r),
                    child: ShadowCard(
                      shadowOpacity: 1.0,
                      child: Material(
                        borderRadius: BorderRadius.circular(15.0),
                        color: Colors.transparent,
                        child: InkWell(
                          borderRadius: BorderRadius.circular(15.0),
                          onTap: () {
                            context.loaderOverlay.show();
                            context
                                .read<CarDoctorBloc>()
                                .add(GetCarDoctorConfig());
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              color: Colors.grey.shade300,
                              gradient: const LinearGradient(
                                colors: <Color>[
                                  ColorName.primaryGradientStart,
                                  ColorName.primaryGradientEnd,
                                ],
                              ),
                            ),
                            height: 59.h,
                            padding: EdgeInsets.fromLTRB(4.w, 4.h, 4.w, 4.h),
                            child: Column(
                              children: [
                                Assets.images.home.sosSdkCardoctor
                                    .svg(width: 59.0, height: 44.0)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  // InkWell(
                  //   child: ClipRRect(
                  //     borderRadius: BorderRadius.circular(0.0),
                  //     child: Assets.images.home.sosSdkCardoctor
                  //         .svg(width: 59.0, height: 89.0),
                  //   ),
                  //   onTap: () {
                  //     context.loaderOverlay.show();
                  //     context
                  //         .read<CarDoctorBloc>()
                  //         .add(GetCarDoctorConfig());
                  //   },
                  // ),
                )),
              ],
            ),
          ),
        );
      }),
    );
  }
}
