import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

Future<T?> showFullScreenBottomSheet<T>({
  required BuildContext context,
  required WidgetBuilder builder,
}) async {
  final result = await showMaterialModalBottomSheet(
    context: context,
    expand: true,
    useRootNavigator: true,
    enableDrag: false,
    builder: builder,
  );

  return result;
}
