// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ocr_image_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OCRImageRequest _$OCRImageRequestFromJson(Map<String, dynamic> json) =>
    OCRImageRequest(
      image: json['image'] as String?,
    );

Map<String, dynamic> _$OCRImageRequestToJson(OCRImageRequest instance) =>
    <String, dynamic>{
      'image': instance.image,
    };
