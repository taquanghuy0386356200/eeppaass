import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// topupEtcId : 1002660
/// topupDate : "27/12/2021 15:43:42"
/// amount : 11744
/// topupType : 1
/// topupCode : "M2096"
/// status : "1"

part 'contract_topup.g.dart';

@JsonEnum()
enum TopupType {
  @JsonValue(0)
  offline,
  @JsonValue(1)
  momo,
  @JsonValue(2)
  viettelMoney,
  @JsonValue(3)
  bank,
  @JsonValue(4)
  adjustment,
  @JsonValue(7)
  zaloPay,
  @JsonValue(8)
  vnptMoney,
  @JsonValue(9)
  mbBank,
  @JsonValue(10)
  bidv,
  other,
}

extension TopupTypeExt on TopupType {
  String get label {
    switch (this) {
      case TopupType.offline:
        return 'Nạp tiền tại quầy';
      case TopupType.momo:
        return 'Nạp tiền qua ví MoMo';
      case TopupType.viettelMoney:
        return 'Nạp tiền qua Viettel Money';
      case TopupType.bank:
        return 'Nạp qua tài khoản ngân hàng/VNPAY';
      case TopupType.adjustment:
        return 'Điều chỉnh tài khoản';
      case TopupType.zaloPay:
        return 'Nạp tiền qua ví ZaloPay';
      case TopupType.vnptMoney:
        return 'Nạp tiền qua ví VNPT Money';
      case TopupType.mbBank:
        return 'Nạp tiền qua ngân hàng MB Bank';
      case TopupType.bidv:
        return 'Nạp tiền qua ngân hàng BIDV';
      case TopupType.other:
        return 'Khác';
    }
  }
}

@JsonSerializable()
class ContractTopup {
  ContractTopup({
    this.topupEtcId,
    this.topupDate,
    this.amount,
    this.topupType,
    this.topupCode,
    this.status,
  });

  final int? topupEtcId;
  final String? topupDate;
  final int? amount;
  @JsonKey(unknownEnumValue: TopupType.other)
  final TopupType? topupType;
  final String? topupCode;
  final String? status;

  factory ContractTopup.fromJson(Map<String, dynamic> json) =>
      _$ContractTopupFromJson(json);

  @override
  String toString() {
    return 'ContractTopup: $topupEtcId';
  }
}

@JsonSerializable()
class ContractTopupData {
  final List<ContractTopup>? listData;
  final int? count;

  ContractTopupData({
    this.listData,
    this.count,
  });

  factory ContractTopupData.fromJson(Map<String, dynamic> json) =>
      _$ContractTopupDataFromJson(json);
}

class ContractTopupDataNotNull {
  final List<ContractTopup> listData;
  final int count;

  ContractTopupDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class ContractTopupResponse {
  final ContractTopupData? data;
  final MessageData? mess;

  ContractTopupResponse({
    this.data,
    this.mess,
  });

  factory ContractTopupResponse.fromJson(Map<String, dynamic> json) =>
      _$ContractTopupResponseFromJson(json);
}

@JsonSerializable()
class ContractTopupEtc {
  ContractTopupEtc(
      {this.topupEtcId, this.amount, this.contractId, this.topupChannel});

  final int? topupEtcId;
  final int? amount;
  final int? contractId;
  final String? topupChannel;

  factory ContractTopupEtc.fromJson(Map<String, dynamic> json) =>
      _$ContractTopupEtcFromJson(json);

  @override
  String toString() {
    return 'ContractTopup: $topupEtcId';
  }
}

@JsonSerializable()
class ContractTopupLastChargeResponse {
  final ContractTopupEtc? data;
  final MessageData? mess;

  ContractTopupLastChargeResponse({
    this.data,
    this.mess,
  });

  factory ContractTopupLastChargeResponse.fromJson(Map<String, dynamic> json) =>
      _$ContractTopupLastChargeResponseFromJson(json);
}

//dumv
@JsonSerializable()
class ContractTopupType {
  ContractTopupType({this.deposit, this.limit, this.custType});

  final bool? deposit;
  final int? limit;
  final String? custType;

  factory ContractTopupType.fromJson(Map<String, dynamic> json) =>
      _$ContractTopupTypeFromJson(json);

  @override
  String toString() {
    return 'ContractTopupType: $custType';
  }
}

@JsonSerializable()
class ContractTopupTypeResponse {
  final ContractTopupType? data;
  final MessageData? mess;

  ContractTopupTypeResponse({
    this.data,
    this.mess,
  });

  factory ContractTopupTypeResponse.fromJson(Map<String, dynamic> json) =>
      _$ContractTopupTypeResponseFromJson(json);
}

@JsonSerializable()
class VehicleInContractResponse {
  @JsonKey(name: 'data')
  final bool? data;
  final MessageData? mess;

  VehicleInContractResponse({this.data, this.mess});

  factory VehicleInContractResponse.fromJson(Map<String, dynamic> json) =>
      _$VehicleInContractResponseFromJson(json);
}
