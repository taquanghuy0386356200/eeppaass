part of 'station_stage_dropdown_bloc.dart';

abstract class StationStageDropdownState extends Equatable {
  const StationStageDropdownState();
}

class StationStageDropdownInitial extends StationStageDropdownState {
  @override
  List<Object> get props => [];
}

class StationStageDropdownFetchedInProgress extends StationStageDropdownState {
  const StationStageDropdownFetchedInProgress();

  @override
  List<Object> get props => [];
}

class StationStageDropdownFetchedSuccess extends StationStageDropdownState {
  final List<StationStage> data;

  const StationStageDropdownFetchedSuccess({required this.data});

  @override
  List<Object> get props => [data];
}

class StationStageDropdownFetchedFailure extends StationStageDropdownState {
  final String message;

  const StationStageDropdownFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}
