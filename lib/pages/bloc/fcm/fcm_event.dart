part of 'fcm_bloc.dart';

@immutable
abstract class FcmEvent extends Equatable {
  const FcmEvent();
}

class FcmPermissionRequest extends FcmEvent {
  const FcmPermissionRequest();

  @override
  List<Object> get props => [];
}

class FcmTokenFetched extends FcmEvent {
  const FcmTokenFetched();

  @override
  List<Object> get props => [];
}

class FcmTokenUpdated extends FcmEvent {
  final String token;

  const FcmTokenUpdated(this.token);

  @override
  List<Object> get props => [];
}

class FcmMessageReceived extends FcmEvent {
  final RemoteMessage message;

  const FcmMessageReceived(this.message);

  @override
  List<Object> get props => [message];
}
