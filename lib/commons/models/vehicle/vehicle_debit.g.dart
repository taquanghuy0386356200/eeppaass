// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle_debit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleDebitResponse _$VehicleDebitResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleDebitResponse(
      data: json['data'] == null
          ? null
          : VehicleDebitData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleDebitResponseToJson(
        VehicleDebitResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

VehicleDebitData _$VehicleDebitDataFromJson(Map<String, dynamic> json) =>
    VehicleDebitData(
      statusDebit: json['statusDebit'] as int?,
      debitStatusName: json['debitStatusName'] as String?,
    );

Map<String, dynamic> _$VehicleDebitDataToJson(VehicleDebitData instance) =>
    <String, dynamic>{
      'statusDebit': instance.statusDebit,
      'debitStatusName': instance.debitStatusName,
    };

VehicleDebitTransactionResponse _$VehicleDebitTransactionResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleDebitTransactionResponse(
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleDebitTransactionResponseToJson(
        VehicleDebitTransactionResponse instance) =>
    <String, dynamic>{
      'mess': instance.mess,
    };
