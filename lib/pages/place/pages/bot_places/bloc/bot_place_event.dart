part of 'bot_place_bloc.dart';

abstract class BotPlaceEvent extends Equatable {
  const BotPlaceEvent();
}

class BotPlaceFetched extends BotPlaceEvent {
  const BotPlaceFetched();

  @override
  List<Object> get props => [];
}

class BotPlaceRefreshed extends BotPlaceEvent {
  const BotPlaceRefreshed();

  @override
  List<Object?> get props => [];
}

class BotPlaceSearched extends BotPlaceEvent {
  final String? input;

  const BotPlaceSearched(this.input);

  @override
  List<Object?> get props => [input];
}
