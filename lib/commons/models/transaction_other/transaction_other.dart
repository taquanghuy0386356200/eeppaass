import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// serviceFeeName : "Đổi thẻ RFID - Khách hàng yêu cầu"
/// createUser : "VDTC-TEST"
/// saleTransDate : "31/12/2021 01:02:50"
/// amount : 120000
/// saleTransCode : "DV8187.20211231010250"
/// actReasonName : "Khách hàng yêu cầu"
/// actTypeName : "Đổi thẻ RFID"

part 'transaction_other.g.dart';

@JsonSerializable()
class TransactionOther {
  final String? serviceFeeName;
  final String? createUser;
  final String? saleTransDate;
  final int? amount;
  final String? saleTransCode;
  final String? actReasonName;
  final String? actTypeName;

  TransactionOther({
    this.serviceFeeName,
    this.createUser,
    this.saleTransDate,
    this.amount,
    this.saleTransCode,
    this.actReasonName,
    this.actTypeName,
  });

  factory TransactionOther.fromJson(Map<String, dynamic> json) =>
      _$TransactionOtherFromJson(json);
}

@JsonSerializable()
class TransactionOtherData {
  final List<TransactionOther>? listData;
  final int? count;

  TransactionOtherData({
    this.listData,
    this.count,
  });

  factory TransactionOtherData.fromJson(Map<String, dynamic> json) =>
      _$TransactionOtherDataFromJson(json);
}

class TransactionOtherDataNotNull {
  final List<TransactionOther> listData;
  final int count;

  TransactionOtherDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class TransactionOtherResponse {
  final TransactionOtherData? data;
  final MessageData? mess;

  TransactionOtherResponse({this.data, this.mess});

  factory TransactionOtherResponse.fromJson(Map<String, dynamic> json) =>
      _$TransactionOtherResponseFromJson(json);
}
