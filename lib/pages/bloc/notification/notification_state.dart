part of 'notification_bloc.dart';

@immutable
class NotificationState extends Equatable {
  final List<NotificationMsg> listData;
  final int totalUnread;
  final bool isFull;
  final String? searchInput;
  final int? notificationType;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const NotificationState({
    required this.listData,
    this.totalUnread = 0,
    this.isFull = false,
    this.searchInput,
    this.notificationType,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        totalUnread,
        isFull,
        searchInput,
        notificationType,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  NotificationState copyWith({
    List<NotificationMsg>? listData,
    int? totalUnread,
    bool? isFull,
    required String? searchInput,
    int? notificationType,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return NotificationState(
      listData: listData ?? this.listData,
      totalUnread: totalUnread ?? this.totalUnread,
      isFull: isFull ?? this.isFull,
      searchInput: searchInput,
      notificationType: notificationType ?? this.notificationType,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
