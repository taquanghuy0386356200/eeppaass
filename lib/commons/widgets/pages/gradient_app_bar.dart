import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GradientTabBar extends Container implements PreferredSizeWidget {
  GradientTabBar({
    Key? key,
    required this.tabBar,
  }) : super(key: key);

  final TabBar tabBar;

  @override
  Size get preferredSize => Size.fromHeight(tabBar.preferredSize.height + 16.h);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
      ),
      child: tabBar,
    );
  }
}
