import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/clients/reconcil/reconcil_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/contract_action/contract_action.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:epass/commons/models/transaction_other/transaction_other.dart';
import 'package:epass/commons/models/transaction_ticket_purchase/transaction_ticket_purchase.dart';
import 'package:epass/commons/models/transaction_vehicle/transaction_vehicle.dart';
import 'package:simple_result/simple_result.dart';
import '../models/transaction_packing/transaction_parking.dart';

abstract class ITransactionHistoryRepository {
  Future<Result<TransactionVehicleDataNotNull, Failure>> getTransactionVehicle({
    int pageSize = 20,
    int startRecord = 0,

    /// active = 1, inactive = 2
    int efficiencyId = 1,
    String? plateNumber,

    /// format: dd/MM/yyyy
    required String timestampInFrom,

    /// format: dd/MM/yyyy
    required String timestampInTo,
  });

  Future<Result<ContractTopupDataNotNull, Failure>> getContractTopup({
    int pageSize = 20,
    int startRecord = 0,

    /// format: dd/MM/yyyy
    String? topupDateFrom,

    /// format: dd/MM/yyyy
    String? topupDateTo,
  });

  Future<Result<ContractActionDataNotNull, Failure>> getContractAction({
    int pageSize = 20,
    int startRecord = 0,
    required String customerId,
    required String contractId,

    /// format: dd/MM/yyyy
    String? startDate,

    /// format: dd/MM/yyyy
    String? endDate,
  });

  Future<Result<TransactionOtherDataNotNull, Failure>> getTransactionOther({
    int pageSize = 20,
    int startRecord = 0,
    String? plateNumber,

    /// format: dd/MM/yyyy
    String? fromDate,

    /// format: dd/MM/yyyy
    String? toDate,
  });

  Future<Result<TransactionTicketPurchaseDataNotNull, Failure>>
      getTransactionTicketPurchase({
    int pageSize = 20,
    int startRecord = 0,
    String? plateNumber,

    /// active: 1, inactive: 2
    required int efficiencyId,

    /// format: dd/MM/yyyy
    String? saleTransDateFrom,

    /// format: dd/MM/yyyy
    String? saleTransDateTo,
  });

  Future<Result<TransactionParkingDataNotNull, Failure>> getTransactionParking({
    int pageSize = 20,
    int startRecord = 0,
    String? plateNumber,
    // String serviceCode = "PARKING_SERVICE",

    /// format: dd/MM/yyyy
    String? startDate,

    /// format: dd/MM/yyyy
    String? endDate,
  });
}

class TransactionHistoryRepository extends ITransactionHistoryRepository {
  final CrmClient _crmClient;
  final ReconcilClient _reconcilClient;

  TransactionHistoryRepository({
    required CrmClient crmClient,
    required ReconcilClient reconcilClient,
  })  : _crmClient = crmClient,
        _reconcilClient = reconcilClient;

  @override
  Future<Result<TransactionVehicleDataNotNull, Failure>> getTransactionVehicle({
    int pageSize = 20,
    int startRecord = 0,
    int efficiencyId = 1,
    String? plateNumber,
    required String timestampInFrom,
    required String timestampInTo,
  }) async {
    try {
      final response = await _reconcilClient.getTransactionVehicle(
        pageSize: pageSize,
        startRecord: startRecord,
        efficiencyId: efficiencyId,
        plateNumber: plateNumber,
        timestampInFrom: timestampInFrom,
        timestampInTo: timestampInTo,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(TransactionVehicleDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ContractTopupDataNotNull, Failure>> getContractTopup({
    int pageSize = 20,
    int startRecord = 0,
    String? topupDateFrom,
    String? topupDateTo,
  }) async {
    try {
      final response = await _crmClient.getContractTopup(
        pageSize: pageSize,
        startRecord: startRecord,
        topupDateFrom: topupDateFrom,
        topupDateTo: topupDateTo,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(ContractTopupDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ContractActionDataNotNull, Failure>> getContractAction({
    int pageSize = 20,
    int startRecord = 0,
    required String customerId,
    required String contractId,
    String? startDate,
    String? endDate,
  }) async {
    try {
      final response = await _crmClient.getContractAction(
        pageSize: pageSize,
        startRecord: startRecord,
        customerId: customerId,
        contractId: contractId,
        startDate: startDate,
        endDate: endDate,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(ContractActionDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<TransactionOtherDataNotNull, Failure>> getTransactionOther({
    int pageSize = 20,
    int startRecord = 0,
    String? plateNumber,
    String? fromDate,
    String? toDate,
  }) async {
    try {
      final response = await _crmClient.getTransactionOther(
        pageSize: pageSize,
        startRecord: startRecord,
        plateNumber: plateNumber,
        fromDate: fromDate,
        toDate: toDate,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(TransactionOtherDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<TransactionTicketPurchaseDataNotNull, Failure>>
      getTransactionTicketPurchase({
    int pageSize = 20,
    int startRecord = 0,
    String? plateNumber,
    required int efficiencyId,
    String? saleTransDateFrom,
    String? saleTransDateTo,
  }) async {
    try {
      final response = await _crmClient.getTransactionTicketPurchase(
        pageSize: pageSize,
        startRecord: startRecord,
        plateNumber: plateNumber,
        efficiencyId: efficiencyId,
        saleTransDateFrom: saleTransDateFrom,
        saleTransDateTo: saleTransDateTo,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(TransactionTicketPurchaseDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<TransactionParkingDataNotNull, Failure>> getTransactionParking({
    int pageSize = 20,
    int startRecord = 0,
    String? plateNumber,
    // String serviceCode = "PARKING_SERVICE",
    String? startDate,
    String? endDate,
  }) async {
    try {
      final response = await _crmClient.getTransactionParking(
        pageSize: pageSize,
        startRecord: startRecord,
        plateNumber: plateNumber,
        // serviceCode: serviceCode,
        saleTransDateFrom: startDate,
        saleTransDateTo: endDate,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(TransactionParkingDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
