import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_logo_page.dart';
import 'package:epass/pages/register/widgets/register_steps_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseRegisterPage extends StatelessWidget {
  final String title;
  final int step;
  final Widget child;
  final bool resizeToAvoidBottomInset;

  const BaseRegisterPage({
    Key? key,
    required this.child,
    required this.title,
    required this.step,
    this.resizeToAvoidBottomInset = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseLogoPage(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      leading: BackButton(onPressed: context.popRoute),
      child: Stack(
        children: [
          const GradientHeaderContainer(),
          SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: FadeAnimation(
              delay: 1.5,
              direction: FadeDirection.up,
              child: SafeArea(
                child: RoundedTopContainer(
                  padding: EdgeInsets.fromLTRB(16.w, 32.h, 16.w, 0.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          title,
                          style: Theme.of(context).textTheme.headline6!.copyWith(
                            fontSize: 22.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 16.h),
                      RegisterStepsIndicator(step: step, lineLength: (ScreenUtil().screenWidth - 44.w) / 5),
                      SizedBox(height: 32.h),
                      child,
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}