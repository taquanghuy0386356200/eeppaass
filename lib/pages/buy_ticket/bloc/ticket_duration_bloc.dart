import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:equatable/equatable.dart';
import 'package:jiffy/jiffy.dart';

part 'ticket_duration_event.dart';

part 'ticket_duration_state.dart';

class TicketDurationBloc
    extends Bloc<TicketDurationEvent, TicketDurationState> {
  TicketDurationBloc() : super(const TicketDurationState()) {
    on<MonthChanged>(_onMonthChanged);
    on<QuarterChanged>(_onQuarterChanged);
    on<EffectiveDateChanged>(_onEffectiveDateChanged);
    on<TicketDurationReset>((event, emit) => emit(const TicketDurationState()));
  }

  FutureOr<void> _onMonthChanged(
    MonthChanged event,
    Emitter<TicketDurationState> emit,
  ) async {
    final startOfMonth = event.startOfMonth;
    if (startOfMonth == null) {
      return;
    }

    emit(TicketDurationState(
      effectiveDate: startOfMonth,
      expireDate: Jiffy(startOfMonth).endOf(Units.MONTH).dateTime,
    ));
  }

  FutureOr<void> _onQuarterChanged(
    QuarterChanged event,
    Emitter<TicketDurationState> emit,
  ) async {
    final startOfQuarter = event.startOfQuarter;
    if (startOfQuarter == null) {
      return;
    }

    emit(TicketDurationState(
      effectiveDate: startOfQuarter,
      expireDate:
          Jiffy(startOfQuarter).add(months: 2).endOf(Units.MONTH).dateTime,
    ));
  }

  FutureOr<void> _onEffectiveDateChanged(
    EffectiveDateChanged event,
    Emitter<TicketDurationState> emit,
  ) async {
    final effectiveDate = event.effectiveDate;
    if (effectiveDate == null) return;
    switch (event.servicePlanType) {
      case ServicePlanType.monthly:
        emit(TicketDurationState(
          effectiveDate: effectiveDate,
          expireDate: Jiffy(effectiveDate)
              .add(days: Constant.blockMonthDays)
              .endOf(Units.DAY)
              .dateTime,
        ));
        return;
      case ServicePlanType.quarterly:
        emit(TicketDurationState(
          effectiveDate: effectiveDate,
          expireDate: Jiffy(effectiveDate)
              .add(days: Constant.blockQuarterDays)
              .endOf(Units.DAY)
              .dateTime,
        ));
        return;
      case ServicePlanType.daily:
      case ServicePlanType.other:
        return;
    }
  }
}
