import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/invoice_setting/bloc/invoice_setting_bloc.dart';
import 'package:epass/pages/invoice_setting/invoice_cycle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InvoiceSettingPage extends StatelessWidget {
  const InvoiceSettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      backgroundColor: Colors.white,
      title: 'Chu kỳ hoá đơn',
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                  child: BlocSelector<AppBloc, AppState, User?>(
                    selector: (state) => state.user,
                    builder: (context, user) {
                      int? billCycleInt, billCycleMergeTypeInt;
                      final billCycle = user?.billCycle;
                      final billCycleMergeType = user?.billCycleMergeType;

                      if (billCycle != null) {
                        billCycleInt = int.tryParse(billCycle);
                      }

                      if (billCycleMergeType != null) {
                        billCycleMergeTypeInt = int.tryParse(billCycleMergeType);
                      }

                      return BlocProvider(
                        create: (context) => InvoiceSettingBloc(
                          appBloc: getIt<AppBloc>(),
                          userRepository: getIt<IUserRepository>(),
                          invoiceRepository: getIt<IInvoiceRepository>(),
                        ),
                        child: InvoiceSettingForm(
                          billCycle: billCycleInt,
                          billCycleMergeType: billCycleMergeTypeInt,
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class InvoiceSettingForm extends StatefulWidget {
  final int? billCycle;
  final int? billCycleMergeType;

  const InvoiceSettingForm({
    Key? key,
    this.billCycle,
    this.billCycleMergeType,
  }) : super(key: key);

  @override
  State<InvoiceSettingForm> createState() => _InvoiceSettingFormState();
}

class _InvoiceSettingFormState extends State<InvoiceSettingForm> {
  InvoiceCycle _selectedInvoiceCycle = InvoiceCycle.timely;
  InvoiceCycleGroupType? _selectedInvoiceCycleGroupType;

  @override
  void initState() {
    if (widget.billCycle != null) {
      for (var element in InvoiceCycle.values) {
        if (element.value == widget.billCycle) {
          _selectedInvoiceCycle = element;
        }
      }
    }

    if (widget.billCycleMergeType != null) {
      for (var element in InvoiceCycleGroupType.values) {
        if (element.value == widget.billCycleMergeType) {
          _selectedInvoiceCycleGroupType = element;
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<InvoiceSettingBloc, InvoiceSettingState>(
      listener: (context, state) async {
        if (state is InvoiceSettingCycleUpdatedSuccess) {
          await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (dialogContext) {
              return SuccessDialog(
                content: 'Thay đổi chu kỳ hóa đơn thành công. Chu kỳ hóa đơn mới sẽ được áp dụng từ tháng tiếp theo',
                contentTextAlign: TextAlign.center,
                buttonTitle: 'Đóng',
                onButtonTap: () {
                  Navigator.of(dialogContext).pop();
                },
              );
            },
          );
        }
      },
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Chu kỳ hoá đơn',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.sp,
                  ),
            ),
            SizedBox(height: 8.h),
            RadioGroupButtons<InvoiceCycle>(
              scrollDirection: Axis.horizontal,
              labels: InvoiceCycle.values.map((e) => e.label).toList(),
              values: InvoiceCycle.values,
              defaultValue: _selectedInvoiceCycle,
              onChanged: (invoiceCycle) {
                setState(() {
                  _selectedInvoiceCycle = invoiceCycle ?? _selectedInvoiceCycle;
                  if (_selectedInvoiceCycle == InvoiceCycle.monthly) {
                    _selectedInvoiceCycleGroupType = InvoiceCycleGroupType.byContract;
                  }
                });
              },
            ),
            if (_selectedInvoiceCycle == InvoiceCycle.monthly)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 16.h),
                  Text(
                    'Gộp hoá đơn',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.sp,
                        ),
                  ),
                  SizedBox(height: 8.h),
                  RadioGroupButtons<InvoiceCycleGroupType>(
                    labels: InvoiceCycleGroupType.values.map((e) => e.label).toList(),
                    values: InvoiceCycleGroupType.values,
                    defaultValue: _selectedInvoiceCycleGroupType,
                    onChanged: (invoiceCycleGroupType) {
                      setState(() => _selectedInvoiceCycleGroupType = invoiceCycleGroupType);
                    },
                  ),
                ],
              ),
            SizedBox(height: 32.h),
            SizedBox(
              height: 56.h,
              width: double.infinity,
              child: LoadingPrimaryButton(
                isLoading: state is InvoiceSettingCycleUpdatedInProgress,
                title: 'Thay đổi chu kỳ hoá đơn',
                onTap: () {
                  context.read<InvoiceSettingBloc>().add(InvoiceSettingCycleUpdated(
                        billCycle: _selectedInvoiceCycle.value,
                        billCycleMergeType: _selectedInvoiceCycleGroupType?.value,
                      ));
                },
              ),
            )
          ],
        );
      },
    );
  }
}
