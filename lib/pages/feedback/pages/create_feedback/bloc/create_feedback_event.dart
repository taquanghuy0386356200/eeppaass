part of 'create_feedback_bloc.dart';

abstract class CreateFeedbackEvent extends Equatable {
  const CreateFeedbackEvent();
}

class CreateFeedbackFormSubmitted extends CreateFeedbackEvent {
  final FeedbackType feedbackType;
  final String receiveContent;
  final List<String> filePaths;
  final String otp;

  const CreateFeedbackFormSubmitted({
    required this.feedbackType,
    required this.receiveContent,
    this.filePaths = const [],
    required this.otp,
  });

  @override
  List<Object> get props => [feedbackType, receiveContent, filePaths, otp];
}
