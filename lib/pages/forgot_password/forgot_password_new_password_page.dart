import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_logo_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:epass/pages/forgot_password/widgets/forgot_password_new_password_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ForgotPasswordNewPasswordPage extends StatelessWidget {
  const ForgotPasswordNewPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
      listener: (context, state) {
        if (state is ForgotPasswordSubmittedInProgress) {
          context.loaderOverlay.show();
        } else if (state is ForgotPasswordSubmittedSuccess) {
          context.loaderOverlay.hide();
          const forgotPasswordOTPRoute = ForgotPasswordOTPRoute();
          if (context.router.current.path != forgotPasswordOTPRoute.path) {
            context.pushRoute(forgotPasswordOTPRoute);
          }
        } else if (state is ForgotPasswordSubmittedFailure) {
          context.loaderOverlay.hide();
          showErrorSnackBBar(
            context: context,
            message: state.message,
            actionLabel: 'Thử lại',
            onActionTap: () => context.read<ForgotPasswordBloc>().add(state.resubmitEvent),
          );
        }
      },
      child: BaseLogoPage(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        child: Stack(
          children: [
            const GradientHeaderContainer(),
            SafeArea(
              child: FadeAnimation(
                delay: 1,
                direction: FadeDirection.up,
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: RoundedTopContainer(
                    padding: EdgeInsets.fromLTRB(16.w, 56.h, 16.w, 0.h),
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Text(
                              'Quên mật khẩu',
                              style: Theme.of(context).textTheme.headline6!.copyWith(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ),
                          SizedBox(height: 24.h),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 6.w),
                            child: Text(
                              'Quý khách vui lòng nhập mật khẩu mới'
                              ' và xác nhận để yêu cầu đặt lại mật khẩu.',
                              style: Theme.of(context).textTheme.bodyText1,
                              // textAlign: TextAlign.center,
                            ),
                          ),

                          SizedBox(height: 32.h),

                          // FORGOT PASSWORD FORM
                          const ForgotPasswordNewPasswordForm(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
