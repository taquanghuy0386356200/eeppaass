part of 'car_doctor_bloc.dart';

@immutable
abstract class CarDoctorState extends Equatable {
  const CarDoctorState();
}

class GetAccessTokenInit extends CarDoctorState {
  @override
  List<Object> get props => [];
}

class GetAccessTokenSuccess extends CarDoctorState {
  final String accessToken;
  final int expiredIn;

  const GetAccessTokenSuccess(this.accessToken, this.expiredIn);

  @override
  List<Object> get props => [accessToken, expiredIn];
}

class GetAccessTokenFail extends CarDoctorState {
  final String message;

  const GetAccessTokenFail(this.message);

  @override
  List<Object> get props => [message];
}

class GetConfigFail extends CarDoctorState {
  final String message;

  const GetConfigFail(this.message);

  @override
  List<Object> get props => [message];
}

class GetConfigSuccess extends CarDoctorState {
  final CarDoctorConfig config;

  const GetConfigSuccess(this.config);

  @override
  List<Object> get props => [config];
}
