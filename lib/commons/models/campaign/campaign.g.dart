// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'campaign.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Campaign _$CampaignFromJson(Map<String, dynamic> json) => Campaign(
      campaignId: json['campaignId'] as int?,
      name: json['name'] as String?,
      imagePath: json['imagePath'] as String?,
      slogan: json['slogan'] as String?,
      isUriExternal: json['isUriExternal'] as String?,
      uri: json['uri'] as String?,
      isActive: json['isActive'] as int?,
      note: json['note'] as String?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as int?,
      type: $enumDecodeNullable(_$CampaignTypeEnumMap, json['type'],
          unknownValue: CampaignType.unknown),
    );

Map<String, dynamic> _$CampaignToJson(Campaign instance) => <String, dynamic>{
      'campaignId': instance.campaignId,
      'name': instance.name,
      'imagePath': instance.imagePath,
      'slogan': instance.slogan,
      'isUriExternal': instance.isUriExternal,
      'uri': instance.uri,
      'isActive': instance.isActive,
      'note': instance.note,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'type': _$CampaignTypeEnumMap[instance.type],
    };

const _$CampaignTypeEnumMap = {
  CampaignType.popup: 1,
  CampaignType.banner: 2,
  CampaignType.popupSMS: 3,
  CampaignType.unknown: 99,
};

CampaignData _$CampaignDataFromJson(Map<String, dynamic> json) => CampaignData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => Campaign.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$CampaignDataToJson(CampaignData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

CampaignResponse _$CampaignResponseFromJson(Map<String, dynamic> json) =>
    CampaignResponse(
      campaignData: json['data'] == null
          ? null
          : CampaignData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CampaignResponseToJson(CampaignResponse instance) =>
    <String, dynamic>{
      'data': instance.campaignData,
      'mess': instance.mess,
    };
