import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/parking/add_service_request.dart';
import 'package:epass/commons/models/parking/add_service_response.dart';
import 'package:epass/commons/models/parking/change_passcode_request.dart';
import 'package:epass/commons/models/parking/change_passcode_response.dart';
import 'package:epass/commons/models/parking/check_passcode_request.dart';
import 'package:epass/commons/models/parking/check_passcode_response.dart';
import 'package:epass/commons/models/parking/condition_service_request.dart';
import 'package:epass/commons/models/parking/condition_service_response.dart';
import 'package:epass/commons/models/parking/config_method_request.dart';
import 'package:epass/commons/models/parking/config_method_response.dart';
import 'package:epass/commons/models/parking/config_service_response.dart';
import 'package:epass/commons/models/parking/verify_config_request.dart';
import 'package:epass/commons/models/parking/verify_config_response.dart';
import 'package:simple_result/simple_result.dart';

abstract class IParkingRepository {
  Future<Result<ConfigServiceModel?, Failure>> getConfigServices();

  Future<Result<List<VerifyConfigModel>?, Failure>> getCurrentVerifyConfig({
    List<int>? listServiceId,
  });

  Future<Result<ConditionServiceResponse?, Failure>> checkConditionUseService({
    required bool isRegisterService,
    required int serviceId
  });

  Future<Result<List<AddServiceModel>, Failure>> addService({
    required int serviceId,
    required int actionType
  });

  Future<Result<ConfigMethodVerifyResponse?, Failure>> configMethodVerify({
    required int serviceId,
    required int verifyType,
    required int actionType,
    required String otp,
    String passCode
  });

  Future<Result<CheckPassCodeModel?, Failure>> checkPassCode({
    required int contractId,
    required int serviceId,
    required int passCode
  });

  Future<Result<ChangePassCodeResponse?, Failure>> changePassCode({
    required int serviceId,
    required String passCode,
    required String otp
  });

  Future<Result<String?, Failure>> requestOtp({
    required String phone,
    required int confirmType
  });
}

class ParkingRepository implements IParkingRepository {
  final CrmClient _crmClient;

  ParkingRepository({
    required CrmClient crmClient,
  }) : _crmClient = crmClient;

  @override
  Future<Result<ConfigServiceModel?, Failure>> getConfigServices() async {
    try {
      final response = await _crmClient.getConfigServices();

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<VerifyConfigModel>?, Failure>> getCurrentVerifyConfig({
    List<int>? listServiceId,
  }) async {
    try {
      final request = VerifyConfigRequest(
          listServiceId: listServiceId
      );

      final response = await _crmClient.getCurrentVerifyConfig(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ConditionServiceResponse?, Failure>> checkConditionUseService({
    required bool isRegisterService,
    required int serviceId}) async {
    try {
      final request = ConditionServiceRequest(
          isRegisterService: isRegisterService,
          serviceId: serviceId
      );

      final response = await _crmClient.checkConditionUseService(request: request);
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<AddServiceModel>, Failure>> addService({
    required int serviceId,
    required int actionType}) async {
    try {
      final action = ActionModel(
          serviceId: serviceId,
          actionType: actionType);
      final request = AddServiceRequest(
          actions: [action]
      );

      final response = await _crmClient.addService(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ConfigMethodVerifyResponse?, Failure>> configMethodVerify({
    required int serviceId,
    required int verifyType,
    required int actionType,
    required String otp,
    String passCode = ''}) async {
    try {
      final request = ConfigMethodVerifyRequest(
          serviceId: serviceId,
          verifyType: verifyType,
          actionType: actionType,
          passCode: passCode,
          otp: otp
      );

      final response = await _crmClient.configMethodVerify(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<CheckPassCodeModel?, Failure>> checkPassCode({
    required int contractId, required int serviceId,
    required int passCode}) async {
    try {
      final request = CheckPassCodeRequest(
          contractId: contractId,
          serviceId: serviceId,
          passCode: passCode
      );

      final response = await _crmClient.checkPassCode(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ChangePassCodeResponse?, Failure>> changePassCode({
    required int serviceId,
    required String passCode, required String otp}) async {
    try {
      final request = ChangePassCodeRequest(
          serviceId: serviceId,
          passCode: passCode,
          otp: otp
      );

      final response = await _crmClient.changePassCode(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> requestOtp({required String phone,
    required int confirmType}) async {
    try {
      final response = await _crmClient.requestOtp(phone: phone, confirmType: confirmType);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
