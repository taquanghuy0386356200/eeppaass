part of 'ticket_duration_bloc.dart';

abstract class TicketDurationEvent extends Equatable {
  const TicketDurationEvent();
}

class MonthChanged extends TicketDurationEvent {
  final DateTime? startOfMonth;

  const MonthChanged(this.startOfMonth);

  @override
  List<Object?> get props => [startOfMonth];
}

class QuarterChanged extends TicketDurationEvent {
  final DateTime? startOfQuarter;

  const QuarterChanged(this.startOfQuarter);

  @override
  List<Object?> get props => [startOfQuarter];
}

class EffectiveDateChanged extends TicketDurationEvent {
  final DateTime? effectiveDate;
  final ServicePlanType servicePlanType;

  const EffectiveDateChanged({
    this.effectiveDate,
    required this.servicePlanType,
  });

  @override
  List<Object?> get props => [effectiveDate];
}

class TicketDurationReset extends TicketDurationEvent {
  const TicketDurationReset();

  @override
  List<Object> get props => [];
}
