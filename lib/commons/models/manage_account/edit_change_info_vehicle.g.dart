// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_change_info_vehicle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EditChangeInfoVehicleTotalResponse _$EditChangeInfoVehicleTotalResponseFromJson(
        Map<String, dynamic> json) =>
    EditChangeInfoVehicleTotalResponse(
      data: json['data'] == null
          ? null
          : EditChangeInfoVehicleResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EditChangeInfoVehicleTotalResponseToJson(
        EditChangeInfoVehicleTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

EditChangeInfoVehicleResponse _$EditChangeInfoVehicleResponseFromJson(
        Map<String, dynamic> json) =>
    EditChangeInfoVehicleResponse(
      ticketId: json['ticketId'] as int?,
      phoneContact: json['phoneContact'] as String?,
      vehicleOrginal: json['vehicleOrginal'] == null
          ? null
          : VehicleOriFtChangeResponse.fromJson(
              json['vehicleOrginal'] as Map<String, dynamic>),
      vehicle: json['vehicle'] == null
          ? null
          : VehicleOriFtChangeResponse.fromJson(
              json['vehicle'] as Map<String, dynamic>),
      ticketProfiles: (json['ticketProfiles'] as List<dynamic>?)
          ?.map((e) => TicketProfiles.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$EditChangeInfoVehicleResponseToJson(
        EditChangeInfoVehicleResponse instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'phoneContact': instance.phoneContact,
      'vehicleOrginal': instance.vehicleOrginal,
      'vehicle': instance.vehicle,
      'ticketProfiles': instance.ticketProfiles,
    };

VehicleOriFtChangeResponse _$VehicleOriFtChangeResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleOriFtChangeResponse(
      plateNumber: json['plateNumber'] as String?,
      plateTypeCode: json['plateTypeCode'] as int?,
      engineNumber: json['engineNumber'] as String?,
      vehicleColourId: json['vehicleColourId'] as int?,
      vehicleBrandId: json['vehicleBrandId'] as int?,
      vehicleMarkId: json['vehicleMarkId'] as int?,
      vehicleId: json['vehicleId'] as int?,
      contractId: json['contractId'] as int?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      vehicleGroupId: json['vehicleGroupId'] as int?,
      vehicleGroupCode: json['vehicleGroupCode'] as int?,
      vehicleGroupName: json['vehicleGroupName'] as String?,
      vehicleGroupDescription: json['vehicleGroupDescription'] as String?,
      seatNumber: json['seatNumber'] as int?,
      netWeight: (json['netWeight'] as num?)?.toDouble(),
      cargoWeight: (json['cargoWeight'] as num?)?.toDouble(),
      grossWeight: (json['grossWeight'] as num?)?.toDouble(),
      pullingWeight: (json['pullingWeight'] as num?)?.toDouble(),
      chassicNumber: json['chassicNumber'] as String?,
      owner: json['owner'] as String?,
      custName: json['custName'] as String?,
    );

Map<String, dynamic> _$VehicleOriFtChangeResponseToJson(
        VehicleOriFtChangeResponse instance) =>
    <String, dynamic>{
      'plateNumber': instance.plateNumber,
      'plateTypeCode': instance.plateTypeCode,
      'vehicleId': instance.vehicleId,
      'contractId': instance.contractId,
      'vehicleTypeId': instance.vehicleTypeId,
      'vehicleGroupId': instance.vehicleGroupId,
      'vehicleGroupCode': instance.vehicleGroupCode,
      'vehicleGroupName': instance.vehicleGroupName,
      'vehicleGroupDescription': instance.vehicleGroupDescription,
      'seatNumber': instance.seatNumber,
      'netWeight': instance.netWeight,
      'cargoWeight': instance.cargoWeight,
      'grossWeight': instance.grossWeight,
      'pullingWeight': instance.pullingWeight,
      'engineNumber': instance.engineNumber,
      'chassicNumber': instance.chassicNumber,
      'vehicleColourId': instance.vehicleColourId,
      'vehicleBrandId': instance.vehicleBrandId,
      'vehicleMarkId': instance.vehicleMarkId,
      'owner': instance.owner,
      'custName': instance.custName,
    };

AttachmentFileVehicle _$AttachmentFileVehicleFromJson(
        Map<String, dynamic> json) =>
    AttachmentFileVehicle(
      attachmentId: json['attachmentId'] as int?,
      ticketId: json['ticketId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
      type: json['type'] as int?,
      objectsId: json['objectsId'] as int?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
    );

Map<String, dynamic> _$AttachmentFileVehicleToJson(
        AttachmentFileVehicle instance) =>
    <String, dynamic>{
      'attachmentId': instance.attachmentId,
      'ticketId': instance.ticketId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
      'type': instance.type,
      'objectsId': instance.objectsId,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
    };
