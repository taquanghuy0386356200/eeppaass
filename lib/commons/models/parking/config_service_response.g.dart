// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_service_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceModel _$ServiceModelFromJson(Map<String, dynamic> json) => ServiceModel(
      serviceId: json['serviceId'] as int,
      code: json['code'] as String,
      serviceName: json['serviceName'] as String,
      status: json['status'] as int,
      serviceRules: (json['serviceRules'] as List<dynamic>?)
          ?.map((e) => ServiceRule.fromJson(e as Map<String, dynamic>))
          .toList(),
      serviceConfig: (json['serviceConfig'] as List<dynamic>?)
          ?.map((e) => ServiceConfig.fromJson(e as Map<String, dynamic>))
          .toList(),
      verifyConfig: (json['verifyConfig'] as List<dynamic>?)
          ?.map((e) => VerifyConfig.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ServiceModelToJson(ServiceModel instance) =>
    <String, dynamic>{
      'serviceId': instance.serviceId,
      'code': instance.code,
      'serviceName': instance.serviceName,
      'status': instance.status,
      'serviceRules': instance.serviceRules,
      'serviceConfig': instance.serviceConfig,
      'verifyConfig': instance.verifyConfig,
    };

ServiceRule _$ServiceRuleFromJson(Map<String, dynamic> json) => ServiceRule(
      serviceRuleId: json['serviceRuleId'] as int,
      serviceId: json['serviceId'] as int,
      ruleContent: json['ruleContent'] as String,
      status: json['status'] as int,
    );

Map<String, dynamic> _$ServiceRuleToJson(ServiceRule instance) =>
    <String, dynamic>{
      'serviceRuleId': instance.serviceRuleId,
      'serviceId': instance.serviceId,
      'ruleContent': instance.ruleContent,
      'status': instance.status,
    };

ServiceConfig _$ServiceConfigFromJson(Map<String, dynamic> json) =>
    ServiceConfig(
      serviceConfigId: json['serviceConfigId'] as int,
      serviceId: json['serviceId'] as int,
      serviceImage: json['serviceImage'] as String?,
      serviceIntroduce: json['serviceIntroduce'] as String?,
      status: json['status'] as int,
    );

Map<String, dynamic> _$ServiceConfigToJson(ServiceConfig instance) =>
    <String, dynamic>{
      'serviceConfigId': instance.serviceConfigId,
      'serviceId': instance.serviceId,
      'serviceImage': instance.serviceImage,
      'serviceIntroduce': instance.serviceIntroduce,
      'status': instance.status,
    };

VerifyConfig _$VerifyConfigFromJson(Map<String, dynamic> json) => VerifyConfig(
      verifyConfigId: json['verifyConfigId'] as int,
      serviceId: json['serviceId'] as int,
      verifyType: json['verifyType'] as int,
      description: json['description'] as String?,
      status: json['status'] as int,
    );

Map<String, dynamic> _$VerifyConfigToJson(VerifyConfig instance) =>
    <String, dynamic>{
      'verifyConfigId': instance.verifyConfigId,
      'serviceId': instance.serviceId,
      'verifyType': instance.verifyType,
      'description': instance.description,
      'status': instance.status,
    };

ConfigServiceModel _$ConfigServiceModelFromJson(Map<String, dynamic> json) =>
    ConfigServiceModel(
      services: (json['services'] as List<dynamic>?)
          ?.map((e) => ServiceModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ConfigServiceModelToJson(ConfigServiceModel instance) =>
    <String, dynamic>{
      'services': instance.services,
    };

ConfigServiceResponse _$ConfigServiceResponseFromJson(
        Map<String, dynamic> json) =>
    ConfigServiceResponse(
      data: json['data'] == null
          ? null
          : ConfigServiceModel.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfigServiceResponseToJson(
        ConfigServiceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
