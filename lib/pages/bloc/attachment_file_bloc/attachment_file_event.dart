part of 'attachment_file_bloc.dart';

abstract class AttachmentFileEvent extends Equatable {
  const AttachmentFileEvent();
}

class AttachmentFileDownloaded extends AttachmentFileEvent {
  final String fileName;
  final int attachmentFileId;

  const AttachmentFileDownloaded({
    required this.fileName,
    required this.attachmentFileId,
  });

  @override
  List<Object> get props => [fileName, attachmentFileId];
}
