import 'dart:io';

import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';

class CameraCard extends StatelessWidget {
  final double? height;
  final String? imagePath;
  final VoidCallback? onTap;

  const CameraCard({
    Key? key,
    this.height,
    this.imagePath,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    File? imageFile;

    if (imagePath != null) {
      imageFile = File(imagePath!);
    }

    return Material(
      color: ColorName.textGray7,
      borderRadius: BorderRadius.circular(8.0),
      child: InkWell(
        borderRadius: BorderRadius.circular(8.0),
        onTap: onTap,
        child: Container(
          height: height ?? 100,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Stack(
            alignment: Alignment.center,
            children: [
              if (imageFile != null)
                Image.file(
                  imageFile,
                  fit: BoxFit.fitWidth,
                ),
              if (imageFile != null)
                Container(
                  decoration: BoxDecoration(
                    color: ColorName.textGray1.withOpacity(0.6),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              Icon(
                Icons.camera_alt_rounded,
                color: imagePath == null ? const Color(0xFF8F9294) : Colors.white,
                size: 28.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
