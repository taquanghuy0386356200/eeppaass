part of 'transaction_topup_bloc.dart';

@immutable
abstract class TransactionTopupEvent extends Equatable {
  const TransactionTopupEvent();
}

class TransactionTopupFetched extends TransactionTopupEvent {
  const TransactionTopupFetched();

  @override
  List<Object?> get props => [];
}

class TransactionTopupRefreshed extends TransactionTopupEvent {
  const TransactionTopupRefreshed();

  @override
  List<Object?> get props => [];
}

class TransactionTopupLoadMore extends TransactionTopupEvent {
  const TransactionTopupLoadMore();

  @override
  List<Object?> get props => [];
}

class TransactionTopupSearched extends TransactionTopupEvent {
  final DateTime? topupDateFrom;
  final DateTime? topupDateTo;

  const TransactionTopupSearched({
    this.topupDateFrom,
    this.topupDateTo,
  });

  @override
  List<Object?> get props => [topupDateFrom, topupDateTo];
}

class TransactionTopupDateRangeChanged extends TransactionTopupEvent {
  final DateTime topupDateFrom;
  final DateTime topupDateTo;

  const TransactionTopupDateRangeChanged({
    required this.topupDateFrom,
    required this.topupDateTo,
  });

  @override
  List<Object> get props => [topupDateFrom, topupDateTo];
}
