enum VNPayPaymentStatus {
  success,
  failure,
}

class VNPayPaymentResponse {
  final String? vnpTmnCode;
  final String? vnpTxnRef;
  final int? vnpAmount;
  final String? vnpOrderInfo;
  final int? vnpResponseCode;
  final String? vnpBankCode;
  final String? vnpBankTranNo;
  final String? vnpPayDate;
  final String? vnpTransactionNo;
  final int? vnpTransactionStatus;
  final String? vnpSecureHashType;
  final String? vnpSecureHash;

  VNPayPaymentStatus get paymentStatus {
    return vnpTransactionStatus == 0
        ? VNPayPaymentStatus.success
        : VNPayPaymentStatus.failure;
  }

  VNPayPaymentResponse({
    this.vnpTmnCode,
    this.vnpTxnRef,
    this.vnpAmount,
    this.vnpOrderInfo,
    this.vnpResponseCode,
    this.vnpBankCode,
    this.vnpBankTranNo,
    this.vnpTransactionStatus,
    this.vnpPayDate,
    this.vnpTransactionNo,
    this.vnpSecureHashType,
    this.vnpSecureHash,
  });

  factory VNPayPaymentResponse.fromMap(Map<String, dynamic> map) {
    return VNPayPaymentResponse(
      vnpTmnCode: map['vnpTmnCode'] as String?,
      vnpTxnRef: map['vnpTxnRef'] as String?,
      vnpAmount: map['vnpAmount'] as int?,
      vnpOrderInfo: map['vnpOrderInfo'] as String?,
      vnpResponseCode: map['vnpResponseCode'] as int?,
      vnpBankCode: map['vnpBankCode'] as String?,
      vnpBankTranNo: map['vnpBankTranNo'] as String?,
      vnpPayDate: map['vnpPayDate'] as String?,
      vnpTransactionNo: map['vnpTransactionNo'] as String?,
      vnpTransactionStatus: map['vnpTransactionStatus'] as int?,
      vnpSecureHashType: map['vnpSecureHashType'] as String?,
      vnpSecureHash: map['vnpSecureHash'] as String?,
    );
  }
}
