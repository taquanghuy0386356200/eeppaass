import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/widgets/container/dashed_separator.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/gradient_text.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/vertical_label.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionVehicleCard extends StatelessWidget {
  final String plateNumber;
  final String timeStampIn;
  final int price;
  final String stationInName;
  final String? stationOutName;
  final String vehicleTypeName;
  final String ticketTypeName;
  final String sourceTransaction;

  const TransactionVehicleCard({
    Key? key,
    required this.plateNumber,
    required this.timeStampIn,
    required this.price,
    required this.stationInName,
    this.stationOutName,
    required this.vehicleTypeName,
    required this.ticketTypeName,
    required this.sourceTransaction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShadowCard(
      shadowOpacity: 0.2,
      hasBorder: true,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 10.w,
          vertical: 16.h,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Plate number & transaction date
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    plateNumber,
                    style: Theme.of(context).textTheme.headline6!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 16.w),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          'Ngày qua trạm',
                          style:
                              Theme.of(context).textTheme.bodyText2!.copyWith(
                                    color: ColorName.borderColor,
                                  ),
                        ),
                        SizedBox(height: 8.h),
                        Text(
                          timeStampIn,
                          style:
                              Theme.of(context).textTheme.subtitle2!.copyWith(
                                    fontSize: 15.sp,
                                    fontWeight: FontWeight.w600,
                                  ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            //
            SizedBox(height: 24.h),

            // Price and station
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Row(
                children: [
                  Expanded(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GradientText(
                          price.formatCurrency(),
                          gradient: const LinearGradient(colors: [
                            ColorName.primaryGradientStart,
                            ColorName.primaryGradientEnd,
                          ]),
                          style:
                              Theme.of(context).textTheme.subtitle2!.copyWith(
                                    fontSize: 25.sp,
                                    fontWeight: FontWeight.w700,
                                  ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          ' ₫',
                          style:
                              Theme.of(context).textTheme.subtitle1!.copyWith(
                                    color: ColorName.primaryGradientEnd,
                                    fontWeight: FontWeight.bold,
                                  ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 8.w),
                  Expanded(
                    child: Text(
                      '$stationInName'
                      '${stationOutName?.isNotEmpty ?? false ? ' - $stationOutName' : ''}',
                      style: Theme.of(context).textTheme.subtitle2!.copyWith(
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold,
                          ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.end,
                    ),
                  )
                ],
              ),
            ),

            //
            SizedBox(height: 24.h),
            const DashedSeparator(
              height: 0.5,
              color: ColorName.borderColor,
            ),
            SizedBox(height: 24.h),

            // Misc
            Row(
              children: [
                Expanded(
                  child: VerticalLabel(
                    label: 'Loại xe',
                    value: vehicleTypeName,
                  ),
                ),
                Expanded(
                  child: VerticalLabel(
                    label: 'Loại thu phí',
                    value: sourceTransaction,
                  ),
                ),
                Expanded(
                  child: VerticalLabel(
                    label: 'Loại vé',
                    value: ticketTypeName,
                  ),
                ),
              ],
            ),
            SizedBox(height: 4.h),
          ],
        ),
      ),
    );
  }
}
