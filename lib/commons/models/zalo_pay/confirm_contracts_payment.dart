import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

import '../cash_in_order/cash_in_order.dart';

part 'confirm_contracts_payment.g.dart';

@JsonSerializable()
class ConfirmContractsPaymentRequest {
  final int amount;
  final String destContractId;
  final String requestId;
  final String topupChannel;

  ConfirmContractsPaymentRequest({
    required this.amount,
    required this.destContractId,
    required this.requestId,
    required this.topupChannel,
  });

  factory ConfirmContractsPaymentRequest.fromJson(Map<String, dynamic> json) =>
      _$ConfirmContractsPaymentRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ConfirmContractsPaymentRequestToJson(this);
}

@JsonSerializable()
class ConfirmContractsPaymentResponse {
  @JsonKey(name: 'data')
  final CashInOrder? order;
  final MessageData? mess;

  ConfirmContractsPaymentResponse({this.order, this.mess});

  factory ConfirmContractsPaymentResponse.fromJson(Map<String, dynamic> json) =>
      _$ConfirmContractsPaymentResponseFromJson(json);
}
