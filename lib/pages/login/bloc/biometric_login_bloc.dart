import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/services/encryption/encryption.dart';
import 'package:epass/commons/services/local_auth/local_auth_service.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

part 'biometric_login_event.dart';

part 'biometric_login_state.dart';

class BiometricLoginBloc extends Bloc<BiometricLoginEvent, BiometricLoginState> {
  final ILocalAuthService _localAuth;
  final FlutterSecureStorage _secureStorage;
  final IEncryption _encryption;

  BiometricLoginBloc({
    required ILocalAuthService localAuth,
    required FlutterSecureStorage secureStorage,
    required IEncryption encryption,
  })  : _localAuth = localAuth,
        _secureStorage = secureStorage,
        _encryption = encryption,
        super(BiometricLoginInitial()) {
    on<GetBiometricAuthenticationType>(_onGetBiometricAuthenticationType);
    on<BiometricAuthenticated>(_onBiometricAuthenticated);
  }

  FutureOr<void> _onGetBiometricAuthenticationType(
    GetBiometricAuthenticationType event,
    Emitter<BiometricLoginState> emit,
  ) async {
    final result = await _localAuth.getAvailableAuthMethod();
    result.when(
      success: (success) => emit(
        GetBiometricAuthenticationTypeSuccess(success),
      ),
      failure: (failure) => emit(
        GetBiometricAuthenticationTypeFailure(failure.message ?? 'Có lỗi xảy ra'),
      ),
    );
  }

  FutureOr<void> _onBiometricAuthenticated(
    BiometricAuthenticated event,
    Emitter<BiometricLoginState> emit,
  ) async {
    final authenticatedResult = await _localAuth.authenticate();

    await authenticatedResult.when<FutureOr<void>>(
      success: (_) async {
        // Get contractNo from secure storage
        final contractNo = await _secureStorage.read(key: Constant.encryptedUsernameKey);

        if (contractNo == null || contractNo.isEmpty) {
          emit(
            const BiometricAuthenticatedFailure('Mã hợp đồng không hợp lệ (client)'),
          );
          return;
        }

        // Get password from secure storage
        final encryptedPassword = await _secureStorage.read(key: Constant.encryptedPasswordKey(contractNo));

        if (encryptedPassword == null || encryptedPassword.isEmpty) {
          emit(
            const BiometricAuthenticatedFailure('Mật khẩu không hợp lệ (client)'),
          );
          return;
        }

        // Decrypt password
        final decryptedPassword = _encryption.decrypt(encryptedPassword);

        // Emit success
        emit(BiometricAuthenticatedSuccess(
          username: contractNo,
          password: decryptedPassword,
        ));
      },
      failure: (failure) => emit(BiometricAuthenticatedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
