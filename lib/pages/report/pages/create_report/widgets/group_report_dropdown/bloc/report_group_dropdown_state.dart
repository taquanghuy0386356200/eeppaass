part of 'report_group_dropdown_bloc.dart';

abstract class ReportGroupDropdownState extends Equatable {
  const ReportGroupDropdownState();
}

class ReportGroupDropdownInitial extends ReportGroupDropdownState {
  @override
  List<Object> get props => [];
}

class ReportGroupDropdownFetchedInProgress extends ReportGroupDropdownState {
  @override
  List<Object> get props => [];
}

class ReportGroupDropdownFetchedSuccess extends ReportGroupDropdownState {
  final List<FeedbackType> data;

  const ReportGroupDropdownFetchedSuccess({required this.data});

  @override
  List<Object> get props => [data];
}

class ReportGroupDropdownFetchedFailure extends ReportGroupDropdownState {
  final String message;

  const ReportGroupDropdownFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}
