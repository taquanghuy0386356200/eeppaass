// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'confirm_momo_payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfirmMomoPaymentRequest _$ConfirmMomoPaymentRequestFromJson(
        Map<String, dynamic> json) =>
    ConfirmMomoPaymentRequest(
      partnerCode: json['partnerCode'] as String,
      partnerRefId: json['partnerRefId'] as String,
      amount: json['amount'] as int,
      customerNumber: json['customerNumber'] as String,
      appData: json['appData'] as String,
    );

Map<String, dynamic> _$ConfirmMomoPaymentRequestToJson(
        ConfirmMomoPaymentRequest instance) =>
    <String, dynamic>{
      'partnerCode': instance.partnerCode,
      'partnerRefId': instance.partnerRefId,
      'amount': instance.amount,
      'customerNumber': instance.customerNumber,
      'appData': instance.appData,
    };

ConfirmMomoPaymentData _$ConfirmMomoPaymentDataFromJson(
        Map<String, dynamic> json) =>
    ConfirmMomoPaymentData(
      amount: json['amount'] as int?,
      momoTransId: json['momoTransId'] as String?,
      partnerRefId: json['partnerRefId'] as String?,
      partnerCode: json['partnerCode'] as String?,
    );

Map<String, dynamic> _$ConfirmMomoPaymentDataToJson(
        ConfirmMomoPaymentData instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'momoTransId': instance.momoTransId,
      'partnerRefId': instance.partnerRefId,
      'partnerCode': instance.partnerCode,
    };

ConfirmMomoPaymentResponse _$ConfirmMomoPaymentResponseFromJson(
        Map<String, dynamic> json) =>
    ConfirmMomoPaymentResponse(
      data: json['data'] == null
          ? null
          : ConfirmMomoPaymentData.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfirmMomoPaymentResponseToJson(
        ConfirmMomoPaymentResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
