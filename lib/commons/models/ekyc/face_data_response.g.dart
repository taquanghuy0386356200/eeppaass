// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'face_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FaceDataResponse _$FaceDataResponseFromJson(Map<String, dynamic> json) =>
    FaceDataResponse(
      code: json['code'] as int?,
      message: json['message'] as String?,
      notice: json['notice'] as String?,
      liveImageId: json['live_image_id'] as String?,
      frontImageId: json['front_image_id'] as String?,
      advanceFmResult: (json['advance_fm_result'] as List<dynamic>?)
          ?.map((e) => AdvanceResult.fromJson(e as Map<String, dynamic>))
          .toList(),
      advanceLivenessResult: (json['advance_liveness_result'] as List<dynamic>?)
          ?.map((e) => AdvanceResult.fromJson(e as Map<String, dynamic>))
          .toList(),
      livenessResult: json['liveness_result'] == null
          ? null
          : LivenessResult.fromJson(
              json['liveness_result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FaceDataResponseToJson(FaceDataResponse instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'notice': instance.notice,
      'live_image_id': instance.liveImageId,
      'front_image_id': instance.frontImageId,
      'advance_fm_result': instance.advanceFmResult,
      'advance_liveness_result': instance.advanceLivenessResult,
      'liveness_result': instance.livenessResult,
    };

AdvanceResult _$AdvanceResultFromJson(Map<String, dynamic> json) =>
    AdvanceResult(
      code: json['code'] as int?,
      message: json['message'] as String?,
      imageId: json['image_id'] as String?,
      plt: json['plt'] as int?,
      transId: json['trans_id'] as String?,
      verifyResult: json['verify_result'] as bool?,
    );

Map<String, dynamic> _$AdvanceResultToJson(AdvanceResult instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'image_id': instance.imageId,
      'verify_result': instance.verifyResult,
      'plt': instance.plt,
      'trans_id': instance.transId,
    };

LivenessResult _$LivenessResultFromJson(Map<String, dynamic> json) =>
    LivenessResult(
      code: json['code'] as int?,
      message: json['message'] as String?,
      imageId: json['image_id'] as String?,
      plt: json['plt'] as int?,
      transId: json['trans_id'] as String?,
      verifyResult: json['verify_result'] as bool?,
    );

Map<String, dynamic> _$LivenessResultToJson(LivenessResult instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'image_id': instance.imageId,
      'verify_result': instance.verifyResult,
      'plt': instance.plt,
      'trans_id': instance.transId,
    };
