part of 'vnpay_bloc.dart';

abstract class VNPayEvent extends Equatable {
  const VNPayEvent();
}

class VNPayPaymentProceeded extends VNPayEvent {
  final String orderId;
  final int amount;
  final String contractNo;
  final String ipAddress = '10.10.10.100';

  const VNPayPaymentProceeded({
    required this.orderId,
    required this.amount,
    required this.contractNo,
  });

  @override
  List<Object> get props => [orderId, amount, contractNo];
}
