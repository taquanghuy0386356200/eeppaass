part of 'cash_in_amount_bloc.dart';

@immutable
abstract class CashInAmountEvent extends Equatable {
  const CashInAmountEvent();
}

class CashInAmountChanged extends CashInAmountEvent {
  final int? cashInAmount;

  const CashInAmountChanged(this.cashInAmount);

  @override
  List<Object?> get props => [cashInAmount];
}
