part of 'avatar_bloc.dart';

class AvatarState extends Equatable {
  final String? contractNo;
  final String? avatarFilePath;
  final bool isLoading;
  final String? message;

  @override
  List<Object?> get props => [contractNo, avatarFilePath, isLoading, message];

  const AvatarState({
    this.contractNo,
    this.avatarFilePath,
    this.isLoading = false,
    this.message,
  });

  AvatarState copyWith({
    String? contractNo,
    String? avatarFilePath,
    bool? isLoading,
    required String? message,
  }) {
    return AvatarState(
      contractNo: contractNo ?? this.contractNo,
      avatarFilePath: avatarFilePath ?? this.avatarFilePath,
      isLoading: isLoading ?? false,
      message: message,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'avatarFilePath': avatarFilePath,
    };
  }

  factory AvatarState.fromJson(Map<String, dynamic> map) {
    return AvatarState(
      avatarFilePath: map['avatarFilePath'] != null
          ? map['avatarFilePath'] as String
          : null,
    );
  }
}
