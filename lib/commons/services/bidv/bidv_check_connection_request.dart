import 'package:epass/commons/services/zalopay/zalo_pay_constant.dart';
import '../../../flavors.dart';
import '../../utils/zalo_pay/zalo_pay_utils.dart';
import 'package:sprintf/sprintf.dart';

class BidvCheckingConnectionRequest {
  /// Mã dịch vụ. Do BIDV cung cấp
  final String serviceID = F.bidvPayGateMerchantId;

  /// Mã đối tác mà BIDV đã cung cấp
  final String merchantCode = F.bidvPayGateMerchantId;

  /// Tên đối tác
  final String merchantName = 'EPASS';

  /// Ngày liên kết
  String transDate = '';

  /// Mã giao dịch duy nhất bên phía đối tác. Lưu ý không dùng tiếng Việt
  String transId = '';

  /// Thông tin mô tả
  final String transDes = 'Nap tien vao tai khoan ePass';

  /// Giá trị giao dịch. Mặc định bằng 0
  final int transAmount = 0;

  String curr = '';

  /// Payer_Id của đối tác. Mặc định là số điện thoại của khách hàng
  final String? payerId;

  /// Payer_Name của đối tác. Mặc định là họ và tên tên của khách hàng
  final String? payerName;

  /// Địa chỉ của khách hàng
  String payerAdd = '';

  String type = '';

  String customerId = '';

  String customerName = '';

  String issueDate = '';

  /// Mã kênh giao dịch. Mặc định Mobile=211701, Web=211601
  final String channelId = '211701';

  final String linkType = '1';

  String otpNumber = '';

  /// Thông tin bổ sung. Bao gồm:
  /// 0 KH chủ động hủy liên kết. 1 hủy liên kết hộ khách hàng
  final String unlinkType = '';

  /// Số điện thoại của khách hàng
  final String? payerMobile;

  /// Số CMT/CCCD/hộ chiếu của khách hàng
  final String? payerIdentity;

  /// Địa chỉ để chuyển sau khi KH liên kết. Url này sử dụng trong trường
  /// hợp đối tác không truyền trong quá trình liên kết.
  final String returnUrl = '/success';

  /// Địa chỉ để chuyển sau nếu KH hủy giao dịch
  final String cancelUrl = '/cancel';

  BidvCheckingConnectionRequest({
    required this.payerId,
    required this.payerName,
    required this.payerMobile,
    required this.payerIdentity,
  });
}
