part of 'register_id_card_bloc.dart';

class RegisterIdCardState extends Equatable {
  final String? front;
  final String? back;
  final IdCardOcr? idCard;

  const RegisterIdCardState({
    this.front,
    this.back,
    this.idCard,
  });

  @override
  List<Object?> get props => [front, back, idCard];

  RegisterIdCardState copyWith({
    String? front,
    String? back,
    IdCardOcr? idCard,
  }) {
    return RegisterIdCardState(
      front: front ?? this.front,
      back: back ?? this.back,
      idCard: idCard ?? this.idCard,
    );
  }
}
