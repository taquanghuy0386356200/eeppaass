// import 'package:epass/commons/models/data_fake/data_payment.dart';
// import 'package:epass/gen/assets.gen.dart';
// import 'package:flutter/material.dart';
//
// class CashInFeeGroupIcon extends StatelessWidget {
//   final TitleBank? title;
//   final double? height;
//   final double? width;
//
//   const CashInFeeGroupIcon({
//     Key? key,
//     required this.title,
//     this.height,
//     this.width,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     switch (title) {
//       case TitleBank.internetBanking:
//         return Assets.icons.bank.image(
//           height: height,
//           width: width,
//         );
//       case TitleBank.wallet:
//         return Assets.icons.eWallet.image(
//           height: height,
//           width: width,
//         );
//       case TitleBank.paymentGateways:
//         return Assets.icons.paymentGateway.svg(
//           height: height,
//           width: width,
//         );
//       case TitleBank.atm:
//         return Assets.icons.domesticCard.image(
//           height: height,
//           width: width,
//         );
//       case TitleBank.debitCard:
//         return Assets.icons.interCard.image(
//           height: height,
//           width: width,
//         );
//       default:
//         return const SizedBox.shrink();
//     }
//   }
// }
