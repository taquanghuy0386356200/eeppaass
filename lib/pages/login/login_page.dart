import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/extensions/image_extension.dart';
import 'package:epass/commons/repo/app_version_repository.dart';
import 'package:epass/commons/repo/authentication_repository.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/services/encryption/encryption.dart';
import 'package:epass/commons/services/local_auth/local_auth_service.dart';
import 'package:epass/commons/utils/number_extension/number_extension.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/pages/base_logo_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/fcm/fcm_bloc.dart';
import 'package:epass/pages/login/bloc/app_version_bloc.dart';
import 'package:epass/pages/login/bloc/biometric_login_bloc.dart';
import 'package:epass/pages/login/bloc/login_form_bloc.dart';
import 'package:epass/pages/login/widgets/login_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'dart:io' show Platform;

class LoginPage extends StatefulWidget implements AutoRouteWrapper {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<LoginFormBloc>(
          create: (context) => LoginFormBloc(
            appBloc: context.read<AppBloc>(),
            authenticationRepository: getIt<IAuthenticationRepository>(),
            userRepository: getIt<IUserRepository>(),
            secureStorage: getIt<FlutterSecureStorage>(),
            fcmBloc: getIt<FcmBloc>(),
          ),
        ),
        BlocProvider<BiometricLoginBloc>(
          create: (context) => BiometricLoginBloc(
            localAuth: getIt<ILocalAuthService>(),
            secureStorage: getIt<FlutterSecureStorage>(),
            encryption: getIt<IEncryption>(),
          ),
        ),
        BlocProvider(
          create: (context) => AppVersionBloc(
              appVersionRepository: getIt<IAppVersionRepository>(),
              categoryRepository: getIt<ICategoryRepository>()),
        ),
      ],
      child: this,
    );
  }
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
    context.read<FcmBloc>().add(const FcmTokenFetched());
    context.read<BiometricLoginBloc>().add(GetBiometricAuthenticationType());
    context.read<AppVersionBloc>().add(const AppVersionFetched());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: BlocListener<AppVersionBloc, AppVersionState>(
        listener: _appVersionBlocListener,
        child: BaseLogoPage(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: false,
          child: Stack(
            children: [
              const GradientHeaderContainer(),
              SafeArea(
                child: FadeAnimation(
                  delay: 1.5,
                  direction: FadeDirection.up,
                  child: RoundedTopContainer(
                    padding: EdgeInsets.fromLTRB(16.w, 56.h, 16.w, 0.h),
                    child: Stack(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Text(
                                'Đăng nhập',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .copyWith(
                                      fontSize: 22.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            ),
                            SizedBox(height: 24.h),

                            // LOGIN FORM
                            const LoginForm(),

                            BlocBuilder<AppVersionBloc, AppVersionState>(
                              builder: (context, state) {
                                if (state is RegistFlagFetchedSuccess) {
                                  if (state.isOnRegistration == "0") {
                                    return Center(
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            'Chưa có tài khoản?',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                          TextButton(
                                            onPressed: () => context.pushRoute(
                                                const RegisterRouter()),
                                            child: Text(
                                              'Đăng ký',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                    color: ColorName.linkBlue,
                                                    decoration: TextDecoration
                                                        .underline,
                                                  ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  } else {
                                    return const Center();
                                  }
                                } else {
                                  return const Center();
                                }
                              },
                            ),
                            const CheckUpdateOptionalWidget(),
                          ],
                        ),
                        Positioned.fill(
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: EdgeInsets.only(bottom: 16.h),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Hotline: ',
                                    style:
                                        Theme.of(context).textTheme.bodyText2!,
                                  ),
                                  SizedBox(width: 2.w),
                                  GestureDetector(
                                    child: Text(
                                      Constant.hotline,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1!
                                          .copyWith(
                                            color: ColorName.waiting,
                                            // fontWeight: FontWeight.w600,
                                          ),
                                    ),
                                    onTap: () async {
                                      await launchUrlString(
                                          'tel:${Constant.hotline}');
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _appVersionBlocListener(
      BuildContext context, AppVersionState state) async {
    // if (kDebugMode) return;

    if (state is AppVersionFetchedInProgress) {
      context.loaderOverlay.show();
    } else if (state is AppVersionFetchedSuccess) {
      context.read<AppVersionBloc>().add(const RegistFlagFetched());

      /// Invalid: 1 - update required, 0 - update optional
      context.loaderOverlay.hide();

      final remoteVersion = state.appVersion.versionName;
      final isUpdateRequired = state.appVersion.invalid == 1;
      final currentVersion = (await PackageInfo.fromPlatform()).version;

      if (remoteVersion != null) {
        if ((remoteVersion.getExtendedVersionNumber() >
                currentVersion.getExtendedVersionNumber()) &&
            isUpdateRequired) {
          // check update
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (dialogContext) {
              return WillPopScope(
                onWillPop: () async => false,
                child: ConfirmDialog(
                  title: 'Cập nhật phần mềm',
                  contentTextAlign: TextAlign.center,
                  image: Assets.icons.download.svg(
                    height: 50.r,
                    width: 50.r,
                    color: ColorName.borderColor,
                  ),
                  content: 'Phiên bản ePass $remoteVersion đã sẵn sàng.'
                      '\nVui lòng cập nhật ứng dụng'
                      ' để trải nghiệm những tính năng mới nhất.',
                  hasSecondaryButton: false,
                  primaryButtonTitle: 'Cập nhật',
                  onPrimaryButtonTap: () async {
                    final String url = Platform.isAndroid
                        ? 'https://play.google.com/store/apps/details?id=com.viettel.etc.epass'
                        : 'itms-apps://itunes.apple.com/app/epass/id1535426844';

                    if (await canLaunchUrlString(url)) {
                      try {
                        await launchUrlString(
                          url,
                          mode: Platform.isAndroid
                              ? LaunchMode.externalNonBrowserApplication
                              : LaunchMode.platformDefault,
                        );
                      } on Exception {
                        showErrorSnackBBar(
                          context: context,
                          message:
                              'Có lỗi xảy ra, Vui lòng cập nhật ePass trong chợ ứng dụng để trải nghiệm những tính năng mới nhất',
                        );
                      }
                    } else {
                      showErrorSnackBBar(
                        context: context,
                        message:
                            'Có lỗi xảy ra, Vui lòng cập nhật ePass trong chợ ứng dụng để trải nghiệm những tính năng mới nhất',
                      );
                    }
                  },
                ),
              );
            },
          );
        }
      }
    } else if (state is AppVersionFetchedFailure) {
      context.read<AppVersionBloc>().add(const RegistFlagFetched());

      context.loaderOverlay.hide();
      // showErrorSnackBBar(context: context, message: 'Có lỗi xảy')
      // showDialog(
      //   context: context,
      //   barrierDismissible: true,
      //   builder: (_) {
      //     return ConfirmDialog(
      //       title: 'Có lỗi xảy ra',
      //       contentTextAlign: TextAlign.center,
      //       image: Assets.icons.download.svg(
      //         height: 50.r,
      //         width: 50.r,
      //         color: ColorName.borderColor,
      //       ),
      //       content: state.message,
      //       hasSecondaryButton: false,
      //       primaryButtonTitle: 'Thử lại',
      //       onPrimaryButtonTap: () {
      //         Navigator.of(context).pop();
      //         // context.read<AppVersionBloc>().add(const AppVersionFetched());
      //       },
      //     );
      //   },
      // );
    }
  }
}

class CheckUpdateOptionalWidget extends StatelessWidget {
  const CheckUpdateOptionalWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: context.read<AppVersionBloc>(),
      child: BlocBuilder<AppVersionBloc, AppVersionState>(
        builder: (context, state) {
          if (state is RegistFlagFetchedSuccess) {
            return Visibility(
              visible: context.read<AppVersionBloc>().isAppHaveNewVersion,
              child: Container(
                margin: EdgeInsets.only(top: 12.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Stack(
                      children: [
                        Image.asset(
                          ImageAssests.download,
                          height: 30.h,
                          width: 30.w,
                          color: ColorName.primaryColor,
                        ),
                        Positioned(
                          right: 0,
                          child: Container(
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.red,
                            ),
                            height: 7.h,
                            width: 7.w,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    InkWell(
                      onTap: () async {
                        showDialog(
                          context: context,
                          barrierDismissible: true,
                          builder: (dialogContext) {
                            return ConfirmDialog(
                              title: 'Thông báo',
                              contentTextAlign: TextAlign.center,
                              image: Assets.icons.download.svg(
                                height: 50.r,
                                width: 50.r,
                                color: ColorName.borderColor,
                              ),
                              content:
                                  'ePass đã có phiên bản mới, cập nhật để sử dụng những tính năng mới nhất',
                              primaryButtonTitle: 'Cập nhật',
                              secondaryButtonTitle: 'Đóng',
                              onSecondaryButtonTap: () {
                                Navigator.of(dialogContext).pop();
                              },
                              onPrimaryButtonTap: () async {
                                final String url = Platform.isAndroid
                                    ? 'https://play.google.com/store/apps/details?id=com.viettel.etc.epass'
                                    : 'itms-apps://itunes.apple.com/app/epass/id1535426844';

                                if (await canLaunchUrlString(url)) {
                                  try {
                                    await launchUrlString(
                                      url,
                                      mode: Platform.isAndroid
                                          ? LaunchMode
                                              .externalNonBrowserApplication
                                          : LaunchMode.platformDefault,
                                    );
                                  } on Exception {
                                    showErrorSnackBBar(
                                      context: context,
                                      message:
                                          'Có lỗi xảy ra, Vui lòng cập nhật ePass trong chợ ứng dụng để trải nghiệm những tính năng mới nhất',
                                    );
                                  }
                                } else {
                                  showErrorSnackBBar(
                                    context: context,
                                    message:
                                        'Có lỗi xảy ra, Vui lòng cập nhật ePass trong chợ ứng dụng để trải nghiệm những tính năng mới nhất',
                                  );
                                }
                              },
                            );
                          },
                        );
                      },
                      child: Text(
                        'V${context.read<AppVersionBloc>().versionApp}',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}
