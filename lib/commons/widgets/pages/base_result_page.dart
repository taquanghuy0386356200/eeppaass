import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseResultPage extends StatelessWidget {
  final String? title;
  final Widget? leading;
  final Widget? image;
  final String? heading;
  final Widget content;
  final Widget? footer;

  const BaseResultPage({
    Key? key,
    this.title,
    this.leading,
    this.image,
    this.heading,
    required this.content,
    this.footer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: title,
      backgroundColor: Colors.white,
      leading: leading,
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                  child: Column(
                    children: [
                      SizedBox(height: 16.h),
                      if (image != null)
                        Center(
                          child: image,
                        ),
                      if (heading != null) SizedBox(height: 32.h),
                      if (heading != null)
                        Text(
                          heading!,
                          style:
                              Theme.of(context).textTheme.headline6!.copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: ColorName.primaryColor,
                                  ),
                        ),
                      SizedBox(height: 48.h),
                      content,
                      SizedBox(height: 100.h),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (footer != null)
            Align(
              alignment: Alignment.bottomCenter,
              child: footer != null
                  ? FadeAnimation(
                      delay: 1,
                      direction: FadeDirection.up,
                      child: footer!,
                    )
                  : null,
            ),
        ],
      ),
    );
  }
}
