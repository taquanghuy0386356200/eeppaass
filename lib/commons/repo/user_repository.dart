import 'dart:io';

import 'package:epass/commons/api/clients/category/category_client.dart';
import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/clients/gateway/gateway_client.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/alias/update_alias_request.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_config_response.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_verify_otp_request.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_verify_otp_response.dart';
import 'package:epass/commons/models/change_password/change_password_request.dart';
import 'package:epass/commons/models/confirmation_wait_for/confirmation_wait_for.dart';
import 'package:epass/commons/models/contract_payment/contract_payment.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/create_vietmap_transaction/create_vietmap_transaction_request.dart';
import 'package:epass/commons/models/link_viet_map_request/link_viet_map_request.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/notification_setting/update_balance_alert.dart';
import 'package:epass/commons/models/reset_password/request_otp_reset_password_request.dart';
import 'package:epass/commons/models/reset_password/reset_password_otp_confirm_request.dart';
import 'package:epass/commons/models/user/update_user_request.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/sms_vehicle_register_request.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/sms_vehicle_update_request.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/user_sms_vehicle_setting.dart';
import 'package:epass/commons/models/utility/unavailable_service_response.dart';
import 'package:epass/commons/models/waiting_order/confirm_order_response.dart';
import 'package:epass/commons/models/waiting_order/waiting_order.dart';
import 'package:epass/commons/models/waiting_order/confirm_order_request.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simple_result/simple_result.dart';

import '../api/clients/cardoctor/cardoctor_client.dart';
import '../models/update_vietmap_transaction/update_vietmap_transaction_request.dart';
import '../models/viet_map_status/viet_map_status_response.dart';

abstract class IUserRepository {
  Future<Result<User, Failure>> getUser();

  Future<Result<ContractPayment?, Failure>> getContractPayment(
      String contractId);

  Future<Result<int, Failure>> getBalance(String customerId, String contractId);

  Future<Result<int, Failure>> getViettelMoneyBalance();

  Future<Result<String?, Failure>> requestOTPResetPassword({
    String? contractNo,
    String? plateNumber,
    String? plateTypeCode,
    required String phoneNumber,
    String userType = '1',
    String confirmType = '1',
  });

  Future<Result<String?, Failure>> resetPasswordOTPConfirm({
    String? contractNo,
    String? plateNumber,
    String? plateTypeCode,
    required String phoneNumber,
    String userType = '1',
    required String otp,
    required String newPassword,
  });

  Future<Result<String?, Failure>> updateAlias({
    required String alias,
  });

  Future<Result<String?, Failure>> changePassword({
    required String oldPassword,
    required String newPassword,
  });

  Future<Result<String?, Failure>> updateUser({
    String? email,
    String? phone,
    String? fileName,
    String? fileBase64,
  });

  Future<Result<File, Failure>> downloadUserAvatar();

  Future<Result<String?, Failure>> updateBalanceAlert({
    required bool isAlertMoney,
    int? alertMoney,
  });

  Future<Result<UserSmsVehicleSetting, Failure>> getUserSMSVehicle();

  Future<Result<String?, Failure>> registerSMSVehicle({
    required String code,
    required bool autoRenew,
    bool? isNextMonth,
  });

  Future<Result<String?, Failure>> updateSMSVehicle({
    required int status,
    required bool autoRenew,
  });

  Future<Result<CarDoctorVerifyOtpResponse, Failure>> verifyCarDoctorOtp({
    required String token,
    required CarDoctorVerifyOtpRequest request,
  });

  Future<Result<CarDoctorVerifyOtpResponse, Failure>> getCarDoctorAccessToken(
      {required String token});

  Future<Result<List<CarDoctorConfig>?, Failure>> getCarDoctorConfig();

  Future<Result<List<WaitingOrder>?, Failure>> getWaitingOrder(
      {int? serviceId});

  Future<Result<NoData, Failure>> waitForConfirmationCancel({
    int? saleOrderId,
  });

  Future<Result<NoData, Failure>> waitForConfirmation({
    int? saleOrderId,
  });

  Future<Result<ConfirmOrderResponse?, Failure>> confirmWaitingRequest({
    required List<WaitingOrder> waitingOrders,
  });

  Future<Result<List<String>?, Failure>> getUnavailableService();

  Future<Result<VietMapStatus, Failure>> checkVietMapStatus();

  Future<Result<NoData, Failure>> linkVietMap(
      {required LinkVietMapRequest request});

  Future<Result<NoData, Failure>> unLinkVietMap();

  Future<Result<NoData, Failure>> createVietMapTransaction(
      {required CreateVietMapTransactionRequest request});

  Future<Result<NoData, Failure>> updateVietMapTransaction(
      {required UpdateVietMapTransactionRequest request});
}

class UserRepository extends IUserRepository {
  final CrmClient _crmClient;
  final GatewayClient _gatewayClient;
  final CategoryClient _categoryClient;
  final CarDoctorClient _carDoctorClient;

  UserRepository(
      {required CrmClient crmClient,
      required GatewayClient gatewayClient,
      required CategoryClient categoryClient,
      required CarDoctorClient carDoctorClient})
      : _crmClient = crmClient,
        _gatewayClient = gatewayClient,
        _categoryClient = categoryClient,
        _carDoctorClient = carDoctorClient;

  @override
  Future<Result<User, Failure>> getUser() async {
    try {
      final response = await _crmClient.getUser();
      final user = response.user;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || user == null) {
        return Result.failure(
          ResponseFailure(
            message: response.mess?.description ?? '',
          ),
        );
      }

      return Result.success(user);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<ContractPayment?, Failure>> getContractPayment(
      String contractId) async {
    try {
      final response = await _crmClient.getContractPayment(contractId);
      final contractPayment = response.contractPayment;

      if (response.mess?.code == 5 && contractPayment == null) {
        // Code 5 = No Viettel Money linked
        return Result.success(contractPayment);
        // return Result.success(ContractPayment());
      }

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          contractPayment == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(contractPayment);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<int, Failure>> getBalance(
      String customerId, String contractId) async {
    try {
      final response = await _crmClient.getOCSInfo(customerId, contractId);
      final balance = response.ocsInfo?.balance;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || balance == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(balance);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> requestOTPResetPassword({
    String? contractNo,
    String? plateNumber,
    String? plateTypeCode,
    required String phoneNumber,
    String userType = '1',
    String confirmType = '1',
  }) async {
    try {
      final millisecond = DateTime.now().millisecondsSinceEpoch.toString();
      final user = contractNo ?? plateNumber ?? '';

      final request = RequestOtpResetPasswordRequest(
        phone: phoneNumber,
        user: user,
        plateTypeCode: plateTypeCode,
        userType: userType,
        confirmType: confirmType,
        requestTime: millisecond,
      );

      final response =
          await _crmClient.requestOTPResetPassword(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE &&
              response.mess?.code != 1030 // Mã lỗi mã giao dịch đã dc gửi
          ) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> resetPasswordOTPConfirm({
    String? contractNo,
    String? plateNumber,
    String? plateTypeCode,
    required String phoneNumber,
    String userType = '1',
    required String otp,
    required String newPassword,
  }) async {
    try {
      final user = contractNo ?? plateNumber ?? '';

      final request = ResetPasswordOTPConfirmRequest(
        phone: phoneNumber,
        user: user,
        plateTypeCode: plateTypeCode,
        userType: userType,
        otp: otp,
        value: newPassword,
      );

      final response =
          await _crmClient.resetPasswordOTPConfirm(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> updateAlias({
    required String alias,
  }) async {
    try {
      final response = await _crmClient.updateAlias(
        request: UpdateAliasRequest(aliasName: alias),
      );

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? '',
        ));
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> changePassword({
    required String oldPassword,
    required String newPassword,
  }) async {
    try {
      final response = await _crmClient.changePassword(
        request: ChangePasswordRequest(
          value: oldPassword,
          newValue: newPassword,
        ),
      );

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> updateUser({
    String? email,
    String? phone,
    String? fileName,
    String? fileBase64,
  }) async {
    try {
      final response = await _crmClient.updateUser(
        request: UpdateUserRequest(
          email: email,
          phone: phone,
          fileName: fileName,
          fileBase64: fileBase64,
        ),
      );

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<File, Failure>> downloadUserAvatar() async {
    try {
      var response = await _crmClient.downloadAvatar();
      if (response.response.statusCode != 200) {
        return const Result.failure(UnknownFailure());
      }

      // save file to local
      final dir = await getApplicationSupportDirectory();

      final data = response.data;
      final contentDisposition =
          response.response.headers.value('Content-Disposition');

      final regexp = RegExp(r'filename=(.+\..+)');

      if (data.isEmpty ||
          contentDisposition == null ||
          contentDisposition.isEmpty ||
          !regexp.hasMatch(contentDisposition)) {
        return const Result.failure(UnknownFailure());
      }

      final match = regexp.firstMatch(contentDisposition);
      final fileName = match?.group(1);

      final file = File('${dir.path}/$fileName');
      final urlFile = await file.writeAsBytes(response.data);

      return Result.success(urlFile);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> updateBalanceAlert({
    required bool isAlertMoney,
    int? alertMoney,
  }) async {
    try {
      final response = await _crmClient.updateAlertMoney(
        request: UpdateBalanceAlertRequest(
          isAlertMoney: isAlertMoney ? 1 : 0,
          alertMoney: alertMoney,
        ),
      );

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<UserSmsVehicleSetting, Failure>> getUserSMSVehicle() async {
    try {
      final response = await _crmClient.getUserSmsVehicleSetting();
      final userSMSVehicleSetting = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          userSMSVehicleSetting == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(userSMSVehicleSetting);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> registerSMSVehicle({
    required String code,
    required bool autoRenew,
    bool? isNextMonth,
  }) async {
    final req = SmsVehicleRegisterRequest(
      code: code,
      autoRenew: autoRenew ? '1' : '0',
      nextMonth: isNextMonth,
    );

    try {
      final response = await _crmClient.registerSMSVehicle(request: req);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> updateSMSVehicle({
    required int status,
    required bool autoRenew,
  }) async {
    final req = SmsVehicleUpdateRequest(
      status: status,
      autoRenew: autoRenew ? '1' : '0',
    );

    try {
      final response = await _crmClient.updateSMSVehicle(request: req);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<int, Failure>> getViettelMoneyBalance() async {
    try {
      final response = await _crmClient.getViettelMoneyBalance();
      final balance = response.data?.balanceAmount;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || balance == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(balance);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<CarDoctorVerifyOtpResponse, Failure>> verifyCarDoctorOtp(
      {required String token,
      required CarDoctorVerifyOtpRequest request}) async {
    try {
      final response = await _carDoctorClient.verifyCarDoctorOtp(
          token: token, request: request);

      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<CarDoctorVerifyOtpResponse, Failure>> getCarDoctorAccessToken(
      {required String token}) async {
    try {
      final response =
          await _carDoctorClient.getCarDoctorAccessToken(token: token);

      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<List<CarDoctorConfig>?, Failure>> getCarDoctorConfig() async {
    try {
      final response = await _categoryClient.getCarDoctorConfig();
      return Result.success(response.data.listData);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<List<WaitingOrder>?, Failure>> getWaitingOrder(
      {int? serviceId}) async {
    try {
      final response =
          await _gatewayClient.getWaitingOrder(serviceId: serviceId);
      final listData = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(listData);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<NoData, Failure>> waitForConfirmation(
      {int? saleOrderId}) async {
    try {
      final request = ConfirmationWaitForRequest(saleOrderId: saleOrderId);
      final response =
          await _gatewayClient.waitForConfirmation(saleOrderId: request);

      if (response.mess?.code == REQUEST_SUCCESS_CODE) {
        return Result.success(response);
      } else {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<NoData, Failure>> waitForConfirmationCancel(
      {int? saleOrderId}) async {
    try {
      final request = ConfirmationWaitForRequest(saleOrderId: saleOrderId);

      final response =
          await _gatewayClient.waitForConfirmationCancel(saleOrderId: request);

      if (response.mess?.code == REQUEST_SUCCESS_CODE) {
        return Result.success(response);
      } else {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ConfirmOrderResponse?, Failure>> confirmWaitingRequest(
      {required List<WaitingOrder> waitingOrders}) async {
    try {
      final list = waitingOrders
          .where((element) => element.isAgree || element.isRefuse)
          .toList()
          .map((order) => ConfirmOrderItem(
              ticketId: order.ticketId,
              verifyAction: order.isAgree ? '2' : '1'))
          .toList();
      final response = await _crmClient.confirmWaitingOrder(
        request: ConfirmOrderRequest(lstVerify: list),
      );

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<List<String>?, Failure>> getUnavailableService() async {
    try {
      final response = await _crmClient.getUnavailableService();

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          response.data?.serviceCanNotAccess == null) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response.data?.serviceCanNotAccess);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<VietMapStatus, Failure>> checkVietMapStatus() async {
    try {
      final response = await _crmClient.checkVietMapStatus();
      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          response.data == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<NoData, Failure>> linkVietMap(
      {required LinkVietMapRequest request}) async {
    try {
      final response = await _crmClient.linkVietMap(request: request);
      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<NoData, Failure>> unLinkVietMap() async {
    try {
      final response = await _crmClient.unLinkVietMap();
      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<NoData, Failure>> createVietMapTransaction(
      {required CreateVietMapTransactionRequest request}) async {
    try {
      final response =
          await _crmClient.createVietMapTransaction(request: request);
      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<NoData, Failure>> updateVietMapTransaction(
      {required UpdateVietMapTransactionRequest request}) async {
    try {
      final response =
          await _crmClient.updateVietMapTransaction(request: request);
      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }
}
