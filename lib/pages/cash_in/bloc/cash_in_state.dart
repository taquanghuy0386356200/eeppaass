part of 'cash_in_bloc.dart';

@immutable
abstract class CashInState extends Equatable {
  const CashInState();
}

class CashInInitial extends CashInState {
  @override
  List<Object> get props => [];
}

class CashInFetchedTopupFeeInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class CashInFetchedTopupFeeSuccess extends CashInState {
  final TopupChannelFee fee;

  const CashInFetchedTopupFeeSuccess(this.fee);

  @override
  List<Object> get props => [fee];
}

class CashInFetchedTopupFeeFailure extends CashInState {
  final String message;

  const CashInFetchedTopupFeeFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CashInOrderCreatedInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class CashInOrderCreatedFailure extends CashInState {
  final String message;

  const CashInOrderCreatedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CashInOrderMomoCreatedSuccess extends CashInState {
  final String orderId;
  final String contractNo;
  final int amount;

  const CashInOrderMomoCreatedSuccess({
    required this.orderId,
    required this.contractNo,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, contractNo, amount];
}

class CashInMomoPaymentInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class CashInMomoPaymentSuccess extends CashInState {
  final String orderId;
  final int amount;
  final String phoneNumber;
  final String paymentToken;

  const CashInMomoPaymentSuccess({
    required this.orderId,
    required this.amount,
    required this.phoneNumber,
    required this.paymentToken,
  });

  @override
  List<Object> get props => [orderId, amount, phoneNumber, paymentToken];
}

class CashInMomoPaymentFailure extends CashInState {
  final String message;

  const CashInMomoPaymentFailure(this.message);

  @override
  List<Object> get props => [message];
}

class ConfirmMomoPaymentInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class ConfirmMomoPaymentSuccess extends CashInState {
  final int? amount;
  final String? momoTransId;
  final String? orderId;

  const ConfirmMomoPaymentSuccess({
    this.amount,
    this.momoTransId,
    this.orderId,
  });

  @override
  List<Object?> get props => [amount, momoTransId, orderId];
}

class ConfirmMomoPaymentFailure extends CashInState {
  final String message;

  const ConfirmMomoPaymentFailure(this.message);

  @override
  List<Object?> get props => [message];
}

class CashInOrderViettelPayCreatedSuccess extends CashInState {
  final String orderId;
  final String contractNo;
  final int amount;

  const CashInOrderViettelPayCreatedSuccess({
    required this.orderId,
    required this.contractNo,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, contractNo, amount];
}

class CashInOrderVNPayCreatedSuccess extends CashInState {
  final String orderId;
  final String contractNo;
  final int amount;

  const CashInOrderVNPayCreatedSuccess({
    required this.orderId,
    required this.contractNo,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, contractNo, amount];
}

class CashInOrderVNPayCreatedFailure extends CashInState {
  final String message;

  const CashInOrderVNPayCreatedFailure(this.message);

  @override
  List<Object?> get props => [message];
}

class CashInOrderBIDVConnnectedSuccess extends CashInState {
  final String contractNo;
  final int amount;

  const CashInOrderBIDVConnnectedSuccess({
    required this.contractNo,
    required this.amount,
  });

  @override
  List<Object> get props => [contractNo, amount];
}

class CashInOrderBIDVConnnectedFailure extends CashInState {
  final String message;

  const CashInOrderBIDVConnnectedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CashInContractsPaymentInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class CashInContractsPaymentSuccess extends CashInState {
  final String topupChannel;
  final String destContractId;
  final int amount;
  final String requestId;

  const CashInContractsPaymentSuccess(
      {required this.topupChannel,
      required this.destContractId,
      required this.amount,
      required this.requestId});

  @override
  List<Object> get props => [topupChannel, destContractId, amount, requestId];
}

class CashInContractsPaymentFailure extends CashInState {
  final String message;

  const CashInContractsPaymentFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CheckCustomerTypeInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class CheckCustomerTypeSuccess extends CashInState {
  final bool? deposit;
  final String? custType;
  final int? limit;

  const CheckCustomerTypeSuccess(
      {required this.deposit, required this.custType, required this.limit});

  @override
  List<Object?> get props => [deposit, custType, limit];
}

class CheckCustomerTypeFailure extends CashInState {
  final String message;

  const CheckCustomerTypeFailure(this.message);

  @override
  List<Object> get props => [message];
}

//dumv
class BidvCreateOrderInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class BidvCreateOrderSuccess extends CashInState {
  final String transId;
  final String responseCode;

  const BidvCreateOrderSuccess(
      {required this.transId, required this.responseCode});

  @override
  List<Object> get props => [transId, responseCode];
}

class BidvCreateOrderFailure extends CashInState {
  final String message;

  const BidvCreateOrderFailure(this.message);

  @override
  List<Object> get props => [message];
}

class BidvCheckOTPProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class BidvCheckOTPSuccess extends CashInState {
  final String transId;
  final String responseCode;

  const BidvCheckOTPSuccess(
      {required this.transId, required this.responseCode});

  @override
  List<Object> get props => [transId, responseCode];
}

class BidvCheckOTPFailure extends CashInState {
  final String message;

  const BidvCheckOTPFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CheckVehicleInContractInProgress extends CashInState {
  @override
  List<Object> get props => [];
}

class CheckVehicleInContractSuccess extends CashInState {
  final bool? hasVehicle;
  final String? message;

  const CheckVehicleInContractSuccess(
      {required this.hasVehicle, required this.message});

  @override
  List<Object?> get props => [hasVehicle, message];
}

class CheckVehicleInContractFailure extends CashInState {
  final String message;

  const CheckVehicleInContractFailure(this.message);

  @override
  List<Object> get props => [message];
}
