// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_service_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActionModel _$ActionModelFromJson(Map<String, dynamic> json) => ActionModel(
      serviceId: json['serviceId'] as int,
      actionType: json['actionType'] as int,
    );

Map<String, dynamic> _$ActionModelToJson(ActionModel instance) =>
    <String, dynamic>{
      'serviceId': instance.serviceId,
      'actionType': instance.actionType,
    };

AddServiceRequest _$AddServiceRequestFromJson(Map<String, dynamic> json) =>
    AddServiceRequest(
      actions: (json['actions'] as List<dynamic>)
          .map((e) => ActionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AddServiceRequestToJson(AddServiceRequest instance) =>
    <String, dynamic>{
      'actions': instance.actions.map((e) => e.toJson()).toList(),
    };
