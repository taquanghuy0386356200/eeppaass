import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DefaultContractNo extends StatelessWidget {
  const DefaultContractNo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<AppBloc, AppState, String?>(
      selector: (state) => state.user?.contractNo,
      builder: (context, contractNo) {
        return RichText(
          text: TextSpan(
            text: 'Tài khoản đăng nhập mặc định của bạn là: ',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(height: 1.4),
            children: [
              TextSpan(
                text: contractNo ?? '',
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: ColorName.primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ],
          ),
        );
      },
    );
  }
}
