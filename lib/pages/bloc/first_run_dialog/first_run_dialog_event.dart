part of 'first_run_dialog_bloc.dart';

abstract class FirstRunDialogEvent extends Equatable {
  const FirstRunDialogEvent();
}

class FirstRunDialogInit extends FirstRunDialogEvent {
  @override
  List<Object> get props => [];
}

class BiometricLoginDialogShowed extends FirstRunDialogEvent {
  @override
  List<Object> get props => [];
}

class AliasDialogShowed extends FirstRunDialogEvent {
  @override
  List<Object> get props => [];
}
