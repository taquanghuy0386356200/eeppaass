part of 'balance_bloc.dart';

@immutable
class BalanceState extends Equatable {
  final int? balance;
  final bool isLoading;
  final bool isHidden;
  final String? error;

  const BalanceState({
    this.balance,
    this.isLoading = false,
    this.isHidden = true,
    this.error,
  });

  @override
  List<Object?> get props => [balance, isLoading, isHidden];

  BalanceState copyWith({
    int? balance,
    bool? isLoading,
    bool? isHidden,
    String? error,
  }) {
    return BalanceState(
      balance: balance ?? this.balance,
      isLoading: isLoading ?? this.isLoading,
      isHidden: isHidden ?? this.isHidden,
      error: error ?? this.error,
    );
  }
}
