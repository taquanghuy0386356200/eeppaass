// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'confirm_order_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfirmOrderItem _$ConfirmOrderItemFromJson(Map<String, dynamic> json) =>
    ConfirmOrderItem(
      ticketId: json['ticketId'] as String?,
      verifyAction: json['verifyAction'] as String?,
    );

Map<String, dynamic> _$ConfirmOrderItemToJson(ConfirmOrderItem instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'verifyAction': instance.verifyAction,
    };

ConfirmOrderRequest _$ConfirmOrderRequestFromJson(Map<String, dynamic> json) =>
    ConfirmOrderRequest(
      lstVerify: (json['lstVerify'] as List<dynamic>?)
          ?.map((e) => ConfirmOrderItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ConfirmOrderRequestToJson(
        ConfirmOrderRequest instance) =>
    <String, dynamic>{
      'lstVerify': instance.lstVerify,
    };
