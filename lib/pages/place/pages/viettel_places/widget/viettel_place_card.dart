import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/place/pages/bot_places/ext/viettel_place_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher_string.dart';

class ViettelPlaceCard extends StatelessWidget {
  final ViettelPlaceType placeType;

  const ViettelPlaceCard({Key? key, required this.placeType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final googleMapAppURL = placeType.getGoogleMapUrl(openApp: true);
    final googleMapWebURL = placeType.getGoogleMapUrl(openApp: false);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          placeType.name,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
        SizedBox(height: 4.h),
        SizedBox(height: 6.h),
        Text(
          placeType.desc,
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: ColorName.borderColor),
        ),
        SizedBox(height: 10.h),
        Row(
          children: [
            const Spacer(),
            Material(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(12.0),
              child: InkWell(
                borderRadius: BorderRadius.circular(12.0),
                onTap: () async {
                  if (googleMapAppURL != null &&
                      await canLaunchUrlString(googleMapAppURL)) {
                    await launchUrlString(googleMapAppURL);
                  } else if (googleMapWebURL != null &&
                      await canLaunchUrlString(googleMapWebURL)) {
                    await launchUrlString(googleMapWebURL);
                  }
                },
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 12.w, vertical: 6.h),
                  child: Row(
                    children: [
                      Assets.icons.search.svg(
                        height: 20.r,
                        width: 20.r,
                        color: ColorName.linkBlue,
                      ),
                      SizedBox(width: 8.w),
                      Text(
                        'Tìm kiếm',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.linkBlue,
                            ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
