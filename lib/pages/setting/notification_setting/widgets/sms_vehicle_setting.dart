import 'package:epass/commons/models/user_sms_vehicle_setting/user_sms_vehicle_setting.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/modal/neutral_confirm_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/setting/notification_setting/bloc/sms_vehicle_bloc/sms_vehicle_bloc.dart';
import 'package:epass/pages/setting/notification_setting/bloc/sms_vehicle_bloc/sms_vehicle_category_bloc.dart';
import 'package:epass/pages/setting/notification_setting/widgets/sms_vehicle_dialog.dart';
import 'package:epass/pages/setting/notification_setting/widgets/sms_vehicle_dialog_shimmer_loading.dart';
import 'package:epass/pages/setting/notification_setting/widgets/sms_vehicle_setting_shimmer_loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class SMSVehicleSetting extends StatelessWidget {
  const SMSVehicleSetting({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final phoneNumber = context.select((AppBloc bloc) => bloc.state.user?.phone);

    return Padding(
      padding: EdgeInsets.fromLTRB(16.w, 20.h, 17.w, 16.h),
      child: BlocConsumer<SMSVehicleBloc, SMSVehicleState>(
        listener: (context, state) {
          if (state is SMSVehicleRegisteredFailure) {
            context.loaderOverlay.hide();
            showErrorSnackBBar(context: context, message: state.message);
          } else if (state is SMSVehicleRegisteredSuccess) {
            context.loaderOverlay.hide();
            context.read<SMSVehicleBloc>().add(UserSMSVehicleFetched());
          } else if (state is SMSVehicleUpdatedFailure) {
            context.loaderOverlay.hide();
            showErrorSnackBBar(context: context, message: state.message);
          } else if (state is SMSVehicleUpdatedSuccess) {
            context.loaderOverlay.hide();
            context.read<SMSVehicleBloc>().add(UserSMSVehicleFetched());
          }
        },
        builder: (context, state) {
          if (state is UserSMSVehicleFetchedInProgress) {
            return const SMSVehicleSettingShimmerLoading();
          } else {
            UserSmsVehicleSetting? userSMSVehicle;

            if (state is UserSMSVehicleFetchedSuccess) {
              userSMSVehicle = state.userSMSVehicleSetting;
            }

            final name = userSMSVehicle?.name;
            final effDate = userSMSVehicle?.effDate;
            final remain = userSMSVehicle?.smsRemain;
            final autoRenew = userSMSVehicle?.autoRenew == '1';

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        'Thông báo trừ tiền xe qua trạm bằng SMS',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                        maxLines: 2,
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Visibility(
                      visible: userSMSVehicle == null,
                      maintainState: true,
                      maintainAnimation: true,
                      maintainSize: true,
                      child: CupertinoSwitch(
                        value: userSMSVehicle != null,
                        activeColor: ColorName.primaryColor,
                        onChanged: (isOn) async {
                          if (isOn) {
                            await _showSMSRegisterDialog(context);
                          } else {
                            await _showSMSUnregisterDialog(context);
                          }
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20.h),
                state is SMSVehicleRegisteredInProgress || state is SMSVehicleUpdatedInProgress
                    ? const SMSVehicleDialogShimmerLoading()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // SMS NAME
                          if (name != null)
                            Row(
                              children: [
                                Icon(Icons.circle, size: 6.r, color: ColorName.primaryColor),
                                SizedBox(width: 10.w),
                                Text(
                                  name,
                                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                        fontWeight: FontWeight.bold,
                                      ),
                                ),
                              ],
                            ),

                          if (name != null) SizedBox(height: 16.h),
                          if (name != null)
                            Container(
                              padding: EdgeInsets.fromLTRB(16.w, 12.h, 16.w, 6.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(color: const Color(0xffc4c4c4), width: 1.0),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // SMS PHONE NUMBER
                                  if (phoneNumber != null)
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: 'Số điện thoại nhận:  ',
                                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  color: ColorName.borderColor,
                                                ),
                                          ),
                                          TextSpan(
                                            text: phoneNumber,
                                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  fontWeight: FontWeight.w600,
                                                ),
                                          ),
                                        ],
                                      ),
                                    ),

                                  // SMS EXP
                                  if (effDate != null) SizedBox(height: 10.h),
                                  if (effDate != null)
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: 'Ngày hiệu lực:  ',
                                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  color: ColorName.borderColor,
                                                ),
                                          ),
                                          TextSpan(
                                            text: effDate,
                                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  fontWeight: FontWeight.w600,
                                                ),
                                          ),
                                        ],
                                      ),
                                    ),

                                  // SMS REMAIN
                                  if (remain != null) SizedBox(height: 10.h),
                                  if (remain != null)
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: 'Số SMS còn lại:  ',
                                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  color: ColorName.borderColor,
                                                ),
                                          ),
                                          TextSpan(
                                            text: remain.toString(),
                                            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                  fontWeight: FontWeight.w600,
                                                ),
                                          ),
                                        ],
                                      ),
                                    ),

                                  // AUTO RENEW
                                  Row(
                                    children: [
                                      Text(
                                        'Tự động gia hạn:',
                                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                              color: ColorName.borderColor,
                                            ),
                                      ),
                                      SizedBox(width: 10.w),
                                      Transform.scale(
                                        scale: 0.8,
                                        child: CupertinoSwitch(
                                          value: autoRenew,
                                          activeColor: ColorName.primaryColor,
                                          onChanged: (isOn) async {
                                            if (isOn) {
                                              await _showSMSAutoRenewDialog(context);
                                            } else {
                                              await _showCancelSMSAutoRenewDialog(context);
                                            }
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                        ],
                      )
              ],
            );
          }
        },
      ),
    );
  }

  Future<void> _showSMSRegisterDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (dialogContext) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => SMSVehicleCategoryBloc(
                categoryRepository: getIt<ICategoryRepository>(),
              ),
            ),
            BlocProvider.value(value: context.read<SMSVehicleBloc>())
          ],
          child: const SMSVehicleDialog(),
        );
      },
    );
  }

  Future<void> _showSMSUnregisterDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (dialogContext) {
        return BlocProvider.value(
          value: context.read<SMSVehicleBloc>(),
          child: NeutralConfirmDialog(
            title: 'Huỷ gói cước SMS',
            content: 'Bạn có chắc chắn muốn huỷ gói cước thông báo trừ tiền xe qua trạm bằng SMS?'
                '\nLưu ý: Sau khi hủy gói cước, bạn vẫn được nhận tin nhắn thông báo đến hết tháng đăng ký.',
            contentTextAlign: TextAlign.start,
            secondaryButtonTitle: 'Đóng',
            primaryButtonTitle: 'Huỷ gói cước',
            onPrimaryButtonTap: () async {
              Navigator.of(dialogContext).pop();
              context.read<SMSVehicleBloc>().add(const SMSVehicleUpdated(
                    status: 0,
                    autoRenew: false,
                  ));
            },
          ),
        );
      },
    );
  }

  Future<void> _showCancelSMSAutoRenewDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (dialogContext) {
        return BlocProvider.value(
          value: context.read<SMSVehicleBloc>(),
          child: NeutralConfirmDialog(
            title: 'Huỷ gia hạn tự động',
            content: 'Sau khi hủy gia hạn gói tin nhắn sẽ không tự động gia hạn vào đầu tháng sau.'
                ' Bạn có muốn hủy gia hạn không?',
            contentTextAlign: TextAlign.start,
            secondaryButtonTitle: 'Đóng',
            primaryButtonTitle: 'Huỷ gia hạn',
            onPrimaryButtonTap: () async {
              Navigator.of(dialogContext).pop();
              context.read<SMSVehicleBloc>().add(const SMSVehicleUpdated(
                    status: 1,
                    autoRenew: false,
                  ));
            },
          ),
        );
      },
    );
  }

  Future<void> _showSMSAutoRenewDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (dialogContext) {
        return BlocProvider.value(
          value: context.read<SMSVehicleBloc>(),
          child: ConfirmDialog(
            title: 'Tự động gia hạn gói cước',
            content: 'Gói cước sẽ tự động gia hạn vào đầu tháng sau. Bạn có chắc chắn muốn gia hạn không?',
            contentTextAlign: TextAlign.start,
            secondaryButtonTitle: 'Đóng',
            primaryButtonTitle: 'Gia hạn',
            onPrimaryButtonTap: () async {
              Navigator.of(dialogContext).pop();
              context.read<SMSVehicleBloc>().add(const SMSVehicleUpdated(
                    status: 1,
                    autoRenew: true,
                  ));
            },
          ),
        );
      },
    );
  }
}
