// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buy_ticket_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BuyTicketRequestItem _$BuyTicketRequestItemFromJson(
        Map<String, dynamic> json) =>
    BuyTicketRequestItem(
      price: json['price'] as int?,
      plateNumber: json['plateNumber'] as String?,
      quantity: json['quantity'] as int? ?? 1,
      vehicleId: json['vehicleId'] as int?,
      vehiclesGroupId: json['vehiclesGroupId'] as int?,
      effDate: json['effDate'] as String?,
      expDate: json['expDate'] as String?,
      autoRenew: json['autoRenew'] as int? ?? 0,
      chargeMethodId:
          $enumDecodeNullable(_$MethodChargeIntEnumMap, json['chargeMethodId']),
      booCode: $enumDecodeNullable(_$BooCodeEnumMap, json['booCode']),
      stationType: json['stationType'] as int?,
      servicePlanTypeId: $enumDecodeNullable(
          _$ServicePlanTypeEnumMap, json['servicePlanTypeId']),
      seatNumber: json['seatNumber'] as int?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      cargoWeight: json['cargoWeight'] as int?,
      netWeight: json['netWeight'] as int?,
      epc: json['epc'] as String?,
      stageId: json['stageId'] as int?,
      stageName: json['stageName'] as String?,
      stationId: json['stationId'] as int?,
      stationName: json['stationName'] as String?,
    );

Map<String, dynamic> _$BuyTicketRequestItemToJson(
    BuyTicketRequestItem instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('price', instance.price);
  writeNotNull('plateNumber', instance.plateNumber);
  writeNotNull('quantity', instance.quantity);
  writeNotNull('vehicleId', instance.vehicleId);
  writeNotNull('vehiclesGroupId', instance.vehiclesGroupId);
  writeNotNull('effDate', instance.effDate);
  writeNotNull('expDate', instance.expDate);
  writeNotNull('autoRenew', instance.autoRenew);
  writeNotNull(
      'chargeMethodId', _$MethodChargeIntEnumMap[instance.chargeMethodId]);
  writeNotNull('booCode', _$BooCodeEnumMap[instance.booCode]);
  writeNotNull('stationType', instance.stationType);
  writeNotNull('servicePlanTypeId',
      _$ServicePlanTypeEnumMap[instance.servicePlanTypeId]);
  writeNotNull('seatNumber', instance.seatNumber);
  writeNotNull('vehicleTypeId', instance.vehicleTypeId);
  writeNotNull('cargoWeight', instance.cargoWeight);
  writeNotNull('netWeight', instance.netWeight);
  writeNotNull('epc', instance.epc);
  writeNotNull('stageId', instance.stageId);
  writeNotNull('stageName', instance.stageName);
  writeNotNull('stationId', instance.stationId);
  writeNotNull('stationName', instance.stationName);
  return val;
}

const _$MethodChargeIntEnumMap = {
  MethodChargeInt.normal: 1,
  MethodChargeInt.block: 2,
  MethodChargeInt.unknown: 'unknown',
};

const _$BooCodeEnumMap = {
  BooCode.boo1: 'BOO1',
  BooCode.boo2: 'BOO2',
  BooCode.unknown: 'unknown',
};

const _$ServicePlanTypeEnumMap = {
  ServicePlanType.daily: 1,
  ServicePlanType.monthly: 4,
  ServicePlanType.quarterly: 5,
  ServicePlanType.other: 'other',
};
