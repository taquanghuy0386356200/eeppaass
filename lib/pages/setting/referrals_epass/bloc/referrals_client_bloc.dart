import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/referral_client/referral_client_request.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/repo/insurance_repository.dart';
import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:equatable/equatable.dart';

part 'referrals_client_event.dart';

part 'referrals_client_state.dart';

class ReferralsClientBloc
    extends Bloc<ReferralsClientEvent, ReferralsClientState> {
  final IInsuranceRepository _accessKeyRepository;
  final IInvoiceRepository _referralRepository;
  final ICategoryRepository _configTimeRepository;

  ReferralsClientBloc({
    required IInsuranceRepository accessKeyRepository,
    required IInvoiceRepository referralRepository,
    required ICategoryRepository configTimeRepository,
  })  : _accessKeyRepository = accessKeyRepository,
        _referralRepository = referralRepository,
        _configTimeRepository = configTimeRepository,
        super(ReferralsClientInitState()) {
    on<SendInfoReferralsClientEvent>(_sendInfoReferralsClient);
    on<CloseSuccessReferralsClientEvent>(_onCloseSuccessReferralsClient);
    on<ConfirmReferralsClientEvent>(_confirmReferralClient);
    on<GetConfigTimeDisableBtn>(_getConfigTimeDisableBtn);
  }

  int timer = 10;
  bool firstValidate = true;

  FutureOr<void> _getConfigTimeDisableBtn(
    GetConfigTimeDisableBtn event,
    emit,
  ) async {
    final result = await _configTimeRepository.getCategories(
      tableName: event.tableName,
    );
    result.when(
      success: (response) {
        for (final category in response.listData) {
          if (category.code == 'COUNT_DOWN_1') {
            timer = int.tryParse(category.name ?? '') ?? 10;
            break;
          }
        }
      },
      failure: (failure) {
        timer = 10;
      },
    );
  }

  FutureOr<void> _sendInfoReferralsClient(
    SendInfoReferralsClientEvent event,
    emit,
  ) async {
    emit(SendingInfoReferralsClientState());
    final resultEncrypted = await _accessKeyRepository.getContractEncrypted();
    resultEncrypted.when(
      success: (response) async {
        if ((response.secretKey ?? '').isNotEmpty) {
          add(
            ConfirmReferralsClientEvent(
              plateNumber: event.plateNumber,
              phoneNumber: event.phoneNumber,
              secretKey: response.secretKey ?? '',
            ),
          );
        } else {
          emit(
            const FailReferralsClientState(
              messageError: 'Có lỗi xảy ra, Vui lòng thử lại!',
            ),
          );
        }
      },
      failure: (failure) {
        emit(
          FailReferralsClientState(
            messageError: failure.message ?? 'Có lỗi xảy ra, Vui lòng thử lại!',
          ),
        );
      },
    );
  }

  FutureOr<void> _confirmReferralClient(
      ConfirmReferralsClientEvent event, emit) async {
    final resultReferralClient = await _referralRepository.referralClient(
      referralClientRequest: ReferralClientRequest(
        plateNumber: event.plateNumber,
        phoneNumber: event.phoneNumber,
        accessKey: event.secretKey,
      ),
    );
    resultReferralClient.when(
      success: (response) {
        emit(SuccessReferralsClientState());
      },
      failure: (failure) {
        emit(
          FailReferralsClientState(
            messageError: failure.message ?? 'Có lỗi xảy ra, Vui lòng thử lại!',
          ),
        );
      },
    );
  }

  FutureOr<void> _onCloseSuccessReferralsClient(
      CloseSuccessReferralsClientEvent event, emit) async {
    emit(DisableSendReferralsClientState());
  }
}
