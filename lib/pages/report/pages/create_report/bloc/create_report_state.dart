part of 'create_report_bloc.dart';

abstract class CreateReportState extends Equatable {
  const CreateReportState();
}

class CreateReportInitial extends CreateReportState {
  @override
  List<Object> get props => [];
}

class CreateReportFormSubmittedInProgress extends CreateReportState {
  const CreateReportFormSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class CreateReportFormSubmittedSuccess extends CreateReportState {
  final String? message;

  const CreateReportFormSubmittedSuccess({this.message});

  @override
  List<Object?> get props => [message];
}

class CreateReportFormSubmittedFailure extends CreateReportState {
  final String message;

  const CreateReportFormSubmittedFailure(this.message);

  @override
  List<Object> get props => [message];
}
