part of 'transaction_other_bloc.dart';

@immutable
class TransactionOtherState extends Equatable {
  final List<TransactionOther> listData;
  final bool isFull;
  final String? plateNumber;
  final DateTime? fromDate;
  final DateTime? toDate;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const TransactionOtherState({
    required this.listData,
    this.isFull = false,
    this.plateNumber,
    this.fromDate,
    this.toDate,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        fromDate,
        toDate,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  TransactionOtherState copyWith({
    List<TransactionOther>? listData,
    bool? isFull,
    required String? plateNumber,
    required DateTime? fromDate,
    required DateTime? toDate,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return TransactionOtherState(
      listData: listData ?? this.listData,
      isFull: isFull ?? this.isFull,
      plateNumber: plateNumber,
      fromDate: fromDate,
      toDate: toDate,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
