import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:epass/commons/services/local_file_service/local_file_service.dart';
import 'package:equatable/equatable.dart';

part 'invoice_details_event.dart';

part 'invoice_details_state.dart';

class InvoiceDetailsBloc
    extends Bloc<InvoiceDetailsEvent, InvoiceDetailsState> {
  final IInvoiceRepository _invoiceRepository;
  final ILocalFileService _localFileService;

  InvoiceDetailsBloc({
    required IInvoiceRepository invoiceRepository,
    required ILocalFileService localFileService,
  })  : _invoiceRepository = invoiceRepository,
        _localFileService = localFileService,
        super(InvoiceDetailsInitial()) {
    on<InvoiceViewed>(_onInvoiceViewed);
    on<InvoiceDownloaded>(_onInvoiceDownloaded);
  }

  FutureOr<void> _onInvoiceDownloaded(InvoiceDownloaded event, emit) async {
    emit(InvoiceDownloadedLoading());

    final result = await _invoiceRepository.downloadInvoice(
        invoiceId: event.invoiceId,
        callback: () {
          emit(InvoiceDiscardLoading());
        });
    result.when(
      success: (invoiceData) async {
        final saveFileResult = await _localFileService.saveFile(
          data: invoiceData,
          fileName: 'invoice-${event.invoiceId}',
          directory: 'Invoice',
          fileExtension: 'pdf',
        );

        saveFileResult.when(
          success: (file) => emit(InvoiceDownloadedSuccess(file)),
          failure: (failure) => emit(
            InvoiceDownloadedFailure(failure.message ?? 'Có lỗi xảy ra'),
          ),
        );
      },
      failure: (failure) => emit(
        InvoiceDownloadedFailure(failure.message ?? 'Có lỗi xảy ra'),
      ),
    );
  }

  FutureOr<void> _onInvoiceViewed(
    InvoiceViewed event,
    Emitter<InvoiceDetailsState> emit,
  ) async {
    emit(InvoiceDownloadedLoading());

    final result = await _invoiceRepository.downloadInvoice(
        invoiceId: event.invoiceId,
        callback: () => {
              emit(
                const InvoiceDownloadedFailure('Có lỗi xảy ra'),
              ),
            });

    result.when(
      success: (invoiceData) => emit(InvoiceViewedSuccess(
        invoiceId: event.invoiceId,
        invoiceData: invoiceData,
      )),
      failure: (failure) => emit(
        InvoiceDownloadedFailure(failure.message ?? 'Có lỗi xảy ra'),
      ),
    );
  }
}
