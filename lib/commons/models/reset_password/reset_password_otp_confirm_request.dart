/*
    let user: String?
    let userType: Int?
    let plateTypeCode: String?
    let phone: String?
    var otp: String?
    // new password
    var value: String?
 */

class ResetPasswordOTPConfirmRequest {
  /// Could be ContractNo or PlateNumber
  final String user;

  /// ContractNo: 0, PlateNumber: 1
  final String userType;

  final String? plateTypeCode;
  final String phone;
  final String otp;

  // New password
  final String value;

  const ResetPasswordOTPConfirmRequest({
    required this.user,
    required this.userType,
    this.plateTypeCode,
    required this.phone,
    required this.otp,
    required this.value,
  });

  Map<String, dynamic> toJson() {
    return {
      'user': user,
      'userType': userType,
      'plateTypeCode': plateTypeCode,
      'phone': phone,
      'otp': otp,
      'value': value,
    };
  }
}
