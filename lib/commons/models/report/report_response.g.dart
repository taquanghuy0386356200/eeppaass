// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportModel _$ReportModelFromJson(Map<String, dynamic> json) => ReportModel(
      l1TicketTypeName: json['l1TicketTypeName'] as String?,
      l2TicketTypeName: json['l2TicketTypeName'] as String?,
      l3TicketTypeName: json['l3TicketTypeName'] as String?,
      contentReceive: json['contentReceive'] as String?,
      createDate: json['createDate'] as String?,
      statusName: json['statusName'] as String?,
      processDate: json['processDate'] as String?,
      staffName: json['staffName'] as String?,
      processContent: json['processContent'] as String?,
      attachmentFiles: (json['attachmentFiles'] as List<dynamic>?)
          ?.map((e) =>
              ReportAttachmentFileResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      fileName: json['fileName'] as String?,
      attachmentId: json['attachmentId'] as String?,
    );

Map<String, dynamic> _$ReportModelToJson(ReportModel instance) =>
    <String, dynamic>{
      'l1TicketTypeName': instance.l1TicketTypeName,
      'l2TicketTypeName': instance.l2TicketTypeName,
      'l3TicketTypeName': instance.l3TicketTypeName,
      'contentReceive': instance.contentReceive,
      'createDate': instance.createDate,
      'statusName': instance.statusName,
      'processDate': instance.processDate,
      'staffName': instance.staffName,
      'processContent': instance.processContent,
      'attachmentFiles': instance.attachmentFiles,
      'fileName': instance.fileName,
      'attachmentId': instance.attachmentId,
    };

ReportResponse _$ReportResponseFromJson(Map<String, dynamic> json) =>
    ReportResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => ReportModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ReportResponseToJson(ReportResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
