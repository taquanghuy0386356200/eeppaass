import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/transaction_ticket_purchase/transaction_ticket_purchase.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'transaction_ticket_purchase_event.dart';

part 'transaction_ticket_purchase_state.dart';

class TransactionTicketPurchaseBloc extends Bloc<TransactionTicketPurchaseEvent,
    TransactionTicketPurchaseState> {
  final ITransactionHistoryRepository _transRepository;

  final int _pageSize;
  int _startRecord = 0;
  bool isSearching = false;

  TransactionTicketPurchaseBloc({
    required ITransactionHistoryRepository transRepository,
    int pageSize = 10,
  })  : _transRepository = transRepository,
        _pageSize = pageSize,
        super(const TransactionTicketPurchaseState(listData: [])) {
    on<TransactionTicketPurchaseFetched>(_onTransactionTicketPurchaseFetched);
    on<TransactionTicketPurchaseRefreshed>(
        _onTransactionTicketPurchaseRefreshed);
    on<TransactionTicketPurchaseLoadMore>(_onTransactionTicketPurchaseLoadMore);
    on<TransactionTicketPurchaseSearched>(_onTransactionTicketPurchaseSearched);
  }

  FutureOr<void> _onTransactionTicketPurchaseFetched(
    TransactionTicketPurchaseFetched event,
    Emitter<TransactionTicketPurchaseState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      plateNumber: null,
      saleTransDateFrom: null,
      saleTransDateTo: null,
      error: null,
    ));

    final result = await _getTransactionTicketPurchase();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: null,
            saleTransDateFrom: null,
            saleTransDateTo: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: null,
        saleTransDateFrom: null,
        saleTransDateTo: null,
      )),
    );
  }

  FutureOr<void> _onTransactionTicketPurchaseRefreshed(
      TransactionTicketPurchaseRefreshed event,
      Emitter<TransactionTicketPurchaseState> emit) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      plateNumber: state.plateNumber,
      saleTransDateFrom: state.saleTransDateFrom,
      saleTransDateTo: state.saleTransDateTo,
      error: null,
    ));

    final result = await _getTransactionTicketPurchase();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            saleTransDateFrom: state.saleTransDateFrom,
            saleTransDateTo: state.saleTransDateTo,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        saleTransDateFrom: state.saleTransDateFrom,
        saleTransDateTo: state.saleTransDateTo,
      )),
    );
  }

  FutureOr<void> _onTransactionTicketPurchaseLoadMore(
    TransactionTicketPurchaseLoadMore event,
    Emitter<TransactionTicketPurchaseState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      plateNumber: state.plateNumber,
      saleTransDateFrom: state.saleTransDateFrom,
      saleTransDateTo: state.saleTransDateTo,
      error: null,
    ));

    final result = await _getTransactionTicketPurchase();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(state.copyWith(
          isLoadingMore: false,
          listData: listData,
          isFull: listData.length == data.count,
          plateNumber: state.plateNumber,
          saleTransDateFrom: state.saleTransDateFrom,
          saleTransDateTo: state.saleTransDateTo,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        saleTransDateFrom: state.saleTransDateFrom,
        saleTransDateTo: state.saleTransDateTo,
      )),
    );
  }

  FutureOr<void> _onTransactionTicketPurchaseSearched(
    TransactionTicketPurchaseSearched event,
    Emitter<TransactionTicketPurchaseState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
      plateNumber: event.plateNumber,
      efficiencyId: event.efficiencyId,
      saleTransDateFrom: event.saleTransDateFrom,
      saleTransDateTo: event.saleTransDateTo,
      error: null,
    ));

    _startRecord = 0;

    final result = await _getTransactionTicketPurchase();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: event.plateNumber,
            saleTransDateFrom: event.saleTransDateFrom,
            saleTransDateTo: event.saleTransDateTo,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: event.plateNumber,
        saleTransDateFrom: event.saleTransDateFrom,
        saleTransDateTo: event.saleTransDateTo,
      )),
    );
  }

  Future<Result<TransactionTicketPurchaseDataNotNull, Failure>>
      _getTransactionTicketPurchase() async {
    final result = await _transRepository.getTransactionTicketPurchase(
      pageSize: _pageSize,
      startRecord: _startRecord,
      efficiencyId: state.efficiencyId,
      plateNumber: state.plateNumber,
      saleTransDateFrom: state.saleTransDateFrom != null
          ? Jiffy(state.saleTransDateFrom).format('dd/MM/yyyy')
          : null,
      saleTransDateTo: state.saleTransDateTo != null
          ? Jiffy(state.saleTransDateTo).format('dd/MM/yyyy')
          : null,
    );
    return result;
  }
}
