import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

part 'first_run_dialog_event.dart';

part 'first_run_dialog_state.dart';

class FirstRunDialogBloc extends HydratedBloc<FirstRunDialogEvent, FirstRunDialogState> {
  final AppBloc _appBloc;
  final SettingBloc _settingBloc;

  FirstRunDialogBloc({
    required AppBloc appBloc,
    required SettingBloc settingBloc,
  })  : _appBloc = appBloc,
        _settingBloc = settingBloc,
        super(const FirstRunDialogState()) {
    on<FirstRunDialogInit>((event, emit) {
      final alias = _appBloc.state.user?.accountAlias;
      bool hasAlias = false;
      if (alias != null && alias.isNotEmpty) {
        hasAlias = true;
      }

      final isBiometricLoginEnabled = _settingBloc.state.isBiometricLoginEnabled;

      emit(state.copyWith(
        aliasDialogShowed: hasAlias || (state.aliasDialogShowed ?? false),
        biometricLoginDialogShowed: isBiometricLoginEnabled || (state.biometricLoginDialogShowed ?? false),
      ));
    });

    on<BiometricLoginDialogShowed>((event, emit) {
      emit(state.copyWith(biometricLoginDialogShowed: true));
    });

    on<AliasDialogShowed>((event, emit) {
      emit(state.copyWith(aliasDialogShowed: true));
    });
  }

  @override
  String get id {
    return _appBloc.state.user?.contractNo ?? '';
  }

  @override
  FirstRunDialogState? fromJson(Map<String, dynamic> json) {
    return FirstRunDialogState.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(FirstRunDialogState state) {
    return state.toJson();
  }
}
