import 'package:epass/commons/models/feedback/feedback_attachment_file.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// l1TicketTypeName : "1. Hỗ trợ ePass CPT"
/// l2TicketTypeName : "Hỗ trợ khác"
/// l3TicketTypeName : "Điều chỉnh tài khoản ePass"
/// contentReceive : "- Số hợp đồng: hâjak\n- Tên chủ hợp đồng:\n- Số tiền điều chỉnh: \n- Nội dung hỗ trợ chi tiết: \n- Lý do điều chỉnh: \n- Số gọi lên: \n- Số liên hệ: hjejj\nzjkzksk"
/// statusName : "Đang xử lý"
/// createDate : "10/03/2022 13:40:31"
/// processTime : "18/03/2022 23:59:59"
/// attachmentFiles : [{"fileName":"CMND3.jpg","attachmentId":908}]
/// staffName : "Nhân viên CSKH test"
/// fileName : "CMND3.jpg"
/// attachmentId : "908"

part 'feedback_response.g.dart';

@JsonEnum(alwaysCreate: true)
enum FeedbackChannel {
  report,
  suggestion,
}

extension FeedbackChannelExt on FeedbackChannel {
  int get value {
    switch (this) {
      case FeedbackChannel.report:
        return 1;
      case FeedbackChannel.suggestion:
        return 2;
    }
  }
}

@JsonSerializable()
class FeedbackModel {
  FeedbackModel({
    this.l1TicketTypeName,
    this.l2TicketTypeName,
    this.l3TicketTypeName,
    this.contentReceive,
    this.statusName,
    this.createDate,
    this.processTime,
    this.processContent,
    this.attachmentFiles,
    this.staffName,
    this.fileName,
    this.attachmentId,
  });

  final String? l1TicketTypeName;
  final String? l2TicketTypeName;
  final String? l3TicketTypeName;
  final String? contentReceive;
  final String? statusName;
  final String? createDate;
  final String? processTime;
  final String? processContent;
  final List<FeedbackAttachmentFileResponse>? attachmentFiles;
  final String? staffName;
  final String? fileName;
  final String? attachmentId;

  factory FeedbackModel.fromJson(Map<String, dynamic> json) => _$FeedbackModelFromJson(json);
}

@JsonSerializable()
class FeedbackResponse {
  FeedbackResponse({
    this.data,
    this.mess,
  });

  final List<FeedbackModel>? data;
  final MessageData? mess;

  factory FeedbackResponse.fromJson(Map<String, dynamic> json) => _$FeedbackResponseFromJson(json);
}
