import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';

part 'attachment_file_event.dart';

part 'attachment_file_state.dart';

class AttachmentFileBloc extends Bloc<AttachmentFileEvent, AttachmentFileState> {
  final IFeedbackRepository _feedbackRepository;

  AttachmentFileBloc({
    required IFeedbackRepository feedbackRepository,
  })  : _feedbackRepository = feedbackRepository,
        super(AttachmentFileInitial()) {
    on<AttachmentFileDownloaded>(
      (event, emit) async {
        emit(const AttachmentFileDownloadedInProgress());

        final result = await _feedbackRepository.downloadAttachmentFile(
          attachmentId: event.attachmentFileId,
        );

        result.when(
          success: (success) => emit(AttachmentFileDownloadedSuccess(
            fileName: event.fileName,
            attachmentFileData: success,
          )),
          failure: (failure) => emit(AttachmentFileDownloadedFailure(message: failure.message ?? 'Có lỗi xảy ra')),
        );
      },
    );
  }
}
