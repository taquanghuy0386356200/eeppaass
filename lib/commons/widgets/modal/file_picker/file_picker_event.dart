part of 'file_picker_bloc.dart';

abstract class FilePickerEvent extends Equatable {
  const FilePickerEvent();
}

class FilePicked extends FilePickerEvent {
  final FileSource source;
  final List<String>? extensions;

  const FilePicked({
    required this.source,
    this.extensions,
  });

  @override
  List<Object?> get props => [source, extensions];
}

class FileRemoved extends FilePickerEvent {
  final String path;

  const FileRemoved(this.path);

  @override
  List<Object> get props => [path];
}
