import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:epass/commons/repo/notification_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/config_local_notification.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/home_tab/home_tabs_page.dart';
import 'package:epass/pages/notification_tab/notification_tab_page.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:loggy/loggy.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

part 'fcm_event.dart';

part 'fcm_state.dart';

/*
There are a few things to keep in mind about your background message handler:

It must not be an anonymous function.
It must be a top-level function (e.g. not a class method which requires initialization).
 */
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  logInfo("Handling a background message: ${message.messageId}");
  getIt<FcmBloc>().add(FcmMessageReceived(message));
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class FcmBloc extends Bloc<FcmEvent, FcmState> {
  final FirebaseMessaging _firebaseMessaging;
  final NotificationBloc _notificationBloc;
  final EventNotificationBloc _eventNotificationBloc;
  final BalanceChangeNotificationBloc _balanceChangeNotificationBloc;
  final DiscountNotificationBloc _discountNotificationBloc;
  final NewsNotificationBloc _newsNotificationBloc;
  final INotificationRepository _notificationRepository;
  final DeviceInfoPlugin _deviceInfo;
  final AppBloc _appBloc;

  FcmBloc({
    required FirebaseMessaging firebaseMessaging,
    required NotificationBloc notificationBloc,
    required EventNotificationBloc eventNotificationBloc,
    required BalanceChangeNotificationBloc balanceChangeNotificationBloc,
    required DiscountNotificationBloc discountNotificationBloc,
    required NewsNotificationBloc newsNotificationBloc,
    required INotificationRepository notificationRepository,
    required DeviceInfoPlugin deviceInfo,
    required AppBloc appBloc,
  })  : _firebaseMessaging = firebaseMessaging,
        _notificationBloc = notificationBloc,
        _eventNotificationBloc = eventNotificationBloc,
        _balanceChangeNotificationBloc = balanceChangeNotificationBloc,
        _discountNotificationBloc = discountNotificationBloc,
        _newsNotificationBloc = newsNotificationBloc,
        _notificationRepository = notificationRepository,
        _deviceInfo = deviceInfo,
        _appBloc = appBloc,
        super(const FcmState()) {
    final context =
        // Config firebase messaging
        _firebaseMessaging.onTokenRefresh.listen((fcmToken) {
      add(FcmTokenUpdated(fcmToken));
    });

    // Firebase foreground message handling
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      final service = LocalNotificationService();
      print(service);

      if (message.notification != null) {
        print(
            'Message also contained a notification: ${message.notification?.title}');
      }

      add(FcmMessageReceived(message));
    });

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    on<FcmPermissionRequest>(_onFcmPermissionRequest);
    on<FcmTokenFetched>(_onFcmTokenFetched);
    on<FcmTokenUpdated>(_onFcmTokenUpdated);
    on<FcmMessageReceived>(_onFcmMessageReceived);
  }

  ///CONFIRM_ROAD_PACKING_PAYMENT
  FutureOr<void> _onFcmPermissionRequest(
    FcmPermissionRequest event,
    Emitter<FcmState> emit,
  ) async {
    final notificationSettings =
        await _firebaseMessaging.getNotificationSettings();
    switch (notificationSettings.authorizationStatus) {
      case AuthorizationStatus.authorized:
      case AuthorizationStatus.provisional:
      case AuthorizationStatus.denied:
        emit(state.copyWith(
          authorizationStatus: notificationSettings.authorizationStatus,
          message: null,
        ));
        return;
      case AuthorizationStatus.notDetermined:
        final settings = await _firebaseMessaging.requestPermission(
          sound: true,
          provisional: false,
          criticalAlert: false,
          carPlay: false,
          badge: true,
          announcement: false,
          alert: true,
        );
        emit(state.copyWith(
          authorizationStatus: settings.authorizationStatus,
          message: null,
        ));
    }
  }

  FutureOr<void> _onFcmTokenFetched(
    FcmTokenFetched event,
    Emitter<FcmState> emit,
  ) async {
    final fcmToken = await _firebaseMessaging.getToken();
    if (kDebugMode) {
      print('fcmToken: $fcmToken');
    }

    if (fcmToken != null) {
      await _registerToken(token: fcmToken);
    }

    emit(state.copyWith(
      token: fcmToken,
      message: null,
    ));
  }

  FutureOr<void> _onFcmTokenUpdated(
    FcmTokenUpdated event,
    Emitter<FcmState> emit,
  ) async {
    await _registerToken(token: event.token);

    emit(state.copyWith(
      token: event.token,
      message: null,
    ));
  }

  Future<void> _registerToken({
    required String token,
  }) async {
    final appInfo = await PackageInfo.fromPlatform();

    var info = '';

    if (Platform.isIOS) {
      final iosInfo = await _deviceInfo.iosInfo;
      info +=
          '${iosInfo.utsname.machine} (${iosInfo.systemVersion ?? ''}) | ${appInfo.version}';
    } else if (Platform.isAndroid) {
      final androidInfo = await _deviceInfo.androidInfo;
      info +=
          '${androidInfo.model} (${androidInfo.version.release ?? ''}) | ${appInfo.version}.${appInfo.buildNumber}';
    }

    final contractId = _appBloc.state.user?.contractId;
    final contractNo = _appBloc.state.user?.contractNo;

    if (contractNo != null && contractId != null) {
      await _notificationRepository.registerFirebaseToken(
        contractId: contractId,
        contractNo: contractNo,
        token: token,
        deviceInfo: info,
      );
    }
  }

  FutureOr<void> _onFcmMessageReceived(
    FcmMessageReceived event,
    Emitter<FcmState> emit,
  ) async {
    // Reload all notification blocs

    _notificationBloc.add(const NotificationRefreshed());
    _eventNotificationBloc.add(const NotificationRefreshed());
    _balanceChangeNotificationBloc.add(const NotificationRefreshed());
    _discountNotificationBloc.add(const NotificationRefreshed());
    _newsNotificationBloc.add(const NotificationRefreshed());

    emit(state.copyWith(
      message: event.message,
    ));
  }
}
