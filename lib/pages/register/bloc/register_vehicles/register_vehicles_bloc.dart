import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/enum/register_level.dart';
import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:equatable/equatable.dart';
import 'package:rxdart/rxdart.dart';

part 'register_vehicles_event.dart';

part 'register_vehicles_state.dart';

enum Address { PROVINCE, DISTRICT, WARD }

class RegisterVehiclesBloc
    extends Bloc<RegisterVehiclesEvent, RegisterVehiclesState> {
  RegisterVehiclesBloc()
      : super(
          const RegisterVehiclesState(
            listRegisterVehicle: [
              RegisterVehicleItem(
                vehicleInfo: VehicleInfo(),
                addressInfo: AddressInfo(),
              )
            ],
          ),
        ) {
    on<VehicleInfoAdded>(_onVehicleInfoAdded);
    on<VehicleInfoRemoved>(_onVehicleInfoRemoved);
    on<VehicleInfoUpdated>(_onVehicleInfoUpdated);

    on<AddressInfoRegCertImageUpdated>(_onAddressInfoRegCertImageUpdated);
    on<AddressInfoVehicleRegCertImageUpdated>(
        _onAddressInfoVehicleRegCertImageUpdated);
    on<AddressInfoPhoneNumberUpdated>(_onAddressInfoPhoneNumberUpdated);
    on<AddressInfoEmailUpdated>(_onAddressInfoEmailUpdated);
    on<AddressInfoAddressDetailsUpdated>(_onAddressInfoAddressDetailsUpdated);
    on<AddressInfoProvinceUpdated>(_onAddressInfoProvinceUpdated);
    on<AddressInfoDistrictUpdated>(_onAddressInfoDistrictUpdated);
    on<AddressInfoWardUpdated>(_onAddressInfoWardUpdated);
    on<AddressInfoRegisterLevelUpdated>(_onRegisterLevelUpdated);
    on<AddressInfoReuseAddressUpdated>(_onAddressInfoReuseAddressUpdated);
    on<AddressInfoReuseTimeFrameUpdated>(_onAddressInfoReuseTimeFrameUpdated);
  }

  final BehaviorSubject<String> provinceAddressError =
      BehaviorSubject.seeded('');
  final BehaviorSubject<String> districtAddressError =
      BehaviorSubject.seeded('');
  final BehaviorSubject<String> wardAddressError = BehaviorSubject.seeded('');
  AddressVTT? provinceAddress;
  AddressVTT? districtAddress;
  AddressVTT? wardAddress;

  void init() {
    provinceAddressError.sink.add('');
    districtAddressError.sink.add('');
    wardAddressError.sink.add('');
  }

  void showWarningError(Address typeAddress) {
    switch (typeAddress) {
      case Address.PROVINCE:
        if (provinceAddress == null) {
          provinceAddressError.sink.add('Vui lòng nhập Tỉnh/Thành phố');
        } else {
          provinceAddressError.sink.add('');
        }
        break;
      case Address.DISTRICT:
        if (districtAddress == null) {
          districtAddressError.sink.add('Vui lòng nhập Quận/Huyện');
        } else {
          districtAddressError.sink.add('');
        }
        break;
      case Address.WARD:
        if (wardAddress == null) {
          wardAddressError.sink.add('Vui lòng nhập Phường/Xã');
        } else {
          wardAddressError.sink.add('');
        }
        break;
      default:
        break;
    }
  }

  bool validateAddress() {
    showWarningError(Address.PROVINCE);
    showWarningError(Address.DISTRICT);
    showWarningError(Address.WARD);

    if(provinceAddress != null && districtAddress != null && wardAddress != null) {
      return true;
    } else {
      return false;
    }
  }

  // Implementation
  FutureOr<void> _onVehicleInfoAdded(
      VehicleInfoAdded event, Emitter<RegisterVehiclesState> emit) {
    emit(state.copyWith(
      listRegisterVehicle: [
        ...state.listRegisterVehicle,
        const RegisterVehicleItem(
            vehicleInfo: VehicleInfo(), addressInfo: AddressInfo())
      ],
    ));
  }

  FutureOr<void> _onVehicleInfoRemoved(
      VehicleInfoRemoved event, Emitter<RegisterVehiclesState> emit) {
    var listRegisterVehicle = [...state.listRegisterVehicle]
      ..removeAt(event.index);
    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onVehicleInfoUpdated(
      VehicleInfoUpdated event, Emitter<RegisterVehiclesState> emit) {
    final registerItem = state.listRegisterVehicle[event.vehicleIndex];
    final newVehicleInfo = registerItem.vehicleInfo.copyWith(
      owner: event.owner,
      plateNumber: event.plateNumber,
      plateType: event.plateType,
      vehicleType: event.vehicleType,
    );
    final newRegisterItem = registerItem.copyWith(vehicleInfo: newVehicleInfo);

    final listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterItem;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoRegCertImageUpdated(
    AddressInfoRegCertImageUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    final addressInfo = registerVehicle.addressInfo;
    final newAddressInfo = addressInfo.copyWith(
      regCertImage: event.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoVehicleRegCertImageUpdated(
    AddressInfoVehicleRegCertImageUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    final addressInfo = registerVehicle.addressInfo;
    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: event.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoPhoneNumberUpdated(
    AddressInfoPhoneNumberUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];
    final addressInfo = registerVehicle.addressInfo;

    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: event.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoEmailUpdated(
    AddressInfoEmailUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];
    final addressInfo = registerVehicle.addressInfo;

    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: event.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoAddressDetailsUpdated(
    AddressInfoAddressDetailsUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    final addressInfo = registerVehicle.addressInfo;

    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: event.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoProvinceUpdated(
    AddressInfoProvinceUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    provinceAddress = event.province;

    showWarningError(Address.PROVINCE);

    final addressInfo = registerVehicle.addressInfo;

    final isOldProvince =
        addressInfo.province?.province == event.province?.province;

    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: event.province,
      noticeDistrict: isOldProvince ? addressInfo.district : null,
      noticeWard: isOldProvince ? addressInfo.ward : null,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    final listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoDistrictUpdated(
    AddressInfoDistrictUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    districtAddress = event.district;

    showWarningError(Address.DISTRICT);

    final addressInfo = registerVehicle.addressInfo;

    final isOldDistrict =
        addressInfo.district?.district == event.district?.district;

    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: event.district,
      noticeWard: isOldDistrict ? addressInfo.ward : null,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoWardUpdated(
      AddressInfoWardUpdated event, Emitter<RegisterVehiclesState> emit) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    wardAddress = event.ward;

    showWarningError(Address.WARD);

    final addressInfo = registerVehicle.addressInfo;
    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: event.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoReuseAddressUpdated(
    AddressInfoReuseAddressUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    final addressInfo = registerVehicle.addressInfo;
    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: event.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onAddressInfoReuseTimeFrameUpdated(
    AddressInfoReuseTimeFrameUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    final addressInfo = registerVehicle.addressInfo;
    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: addressInfo.registerLevel,
      reuseTimeFrame: event.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }

  FutureOr<void> _onRegisterLevelUpdated(
    AddressInfoRegisterLevelUpdated event,
    Emitter<RegisterVehiclesState> emit,
  ) {
    final registerVehicle = state.listRegisterVehicle[event.vehicleIndex];

    final addressInfo = registerVehicle.addressInfo;
    final newAddressInfo = addressInfo.copyWith(
      regCertImage: addressInfo.regCertImage,
      vehicleRegCertImage: addressInfo.vehicleRegCertImage,
      phoneNumber: addressInfo.phoneNumber,
      email: addressInfo.email,
      addressDetails: addressInfo.addressDetails,
      noticeProvince: addressInfo.province,
      noticeDistrict: addressInfo.district,
      noticeWard: addressInfo.ward,
      reuseAddress: addressInfo.reuseAddress,
      timeFrame: event.timeFrame,
      reuseTimeFrame: addressInfo.reuseTimeFrame,
    );

    final newRegisterVehicle =
        registerVehicle.copyWith(addressInfo: newAddressInfo);

    var listRegisterVehicle = [...state.listRegisterVehicle];

    listRegisterVehicle[event.vehicleIndex] = newRegisterVehicle;

    emit(state.copyWith(listRegisterVehicle: listRegisterVehicle));
  }
}
