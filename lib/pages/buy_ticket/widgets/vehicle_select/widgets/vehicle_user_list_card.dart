import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/widgets/primary_checkbox.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_shadow/simple_shadow.dart';

class VehicleUserListCard extends StatelessWidget {
  const VehicleUserListCard({
    Key? key,
    required this.vehicle,
    this.isSelected = false,
    this.onTap,
  }) : super(key: key);

  final Vehicle vehicle;
  final bool isSelected;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return SimpleShadow(
      offset: const Offset(0, 1),
      opacity: 0.1,
      sigma: 10,
      child: Material(
        borderRadius: BorderRadius.circular(16.0),
        color: Colors.white,
        child: InkWell(
          borderRadius: BorderRadius.circular(16.0),
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 16.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
              border: Border.all(
                color: ColorName.primaryColor,
                style: isSelected ? BorderStyle.solid : BorderStyle.none,
                width: 1.0,
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        vehicle.plateNumber ?? '',
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      SizedBox(height: 4.h),
                      Text(
                        vehicle.vehicleGroupName ?? '',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.borderColor,
                            ),
                      ),
                    ],
                  ),
                ),
                const Spacer(),
                AbsorbPointer(
                  child: SizedBox(
                    width: 22.r,
                    height: 22.r,
                    child: PrimaryCheckbox(
                      isCheck: isSelected,
                      onChanged: (bool? value) {},
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
