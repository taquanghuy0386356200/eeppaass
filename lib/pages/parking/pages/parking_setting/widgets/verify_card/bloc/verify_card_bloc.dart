import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/parking/config_service_response.dart';
import 'package:epass/commons/models/parking/verify_config_response.dart';
import 'package:epass/commons/repo/parking_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'verify_card_event.dart';
part 'verify_card_state.dart';

class VerifyCardBloc extends Bloc<VerifyCardEvent, VerifyCardState> {
  final IParkingRepository _parkingRepository;
  final AppBloc _appBloc;

  VerifyCardBloc({
    required IParkingRepository parkingRepository,
    required AppBloc appBloc,
  }) : _parkingRepository = parkingRepository,
        _appBloc = appBloc,
        super(const VerifyCardState()) {
    on<VerifyCardEvent>((event, emit) {
      // on<VerifyConfigFetched>(_onVerifyConfigFetched);
      on<VerifyConfigAlerted>(_onVerifyConfigAlerted);
    });
  }

  // FutureOr<void> _onVerifyConfigFetched(
  //     VerifyConfigFetched event,
  //     Emitter<VerifyCardState> emit,
  //     ) async {
  //   print(event.serviceVerifyConfig.length);
  //   bool isOn = false;
  //   for (var p in event.serviceVerifyConfig){
  //     print('Config: ${p.verifyType} // Event ${event.verifyConfig.verifyType}');
  //     if (p.verifyType != null && p.verifyType == event.verifyConfig.verifyType){
  //       isOn = p.status == 1;
  //       break;
  //     }
  //   }
  //   emit(
  //     state.copyWith(
  //         isOn: isOn
  //     ),
  //   );
  // }

  FutureOr<void> _onVerifyConfigAlerted(
      VerifyConfigAlerted event,
      emit,
      ) async {
    print(event.isOn);
    emit(state.copyWith(
      isOn: event.isOn,
    ));


    if (event.isOn!) {
      // Check condition use service
      final user = _appBloc.state.user;

      // final result = await _parkingRepository.checkConditionUseService(
      //     plateNumber: plateNumber,
      //     plateColor: plateColor,
      //     isRegisterService: isRegisterService,
      //     serviceId: serviceId);
    } else {
      // Turn off parking service
      final user = _appBloc.state.user;
      final contractId = user?.contractId;
      final serviceId = event.serviceId;
      print('ContractId: ${contractId}');
      // final result = await _parkingRepository.addService(
      //     contractId: int.parse(contractId!),
      //     serviceId: serviceId!,
      //     actionType: event.isOn! ? 1 : 0);
    }
  }
}
