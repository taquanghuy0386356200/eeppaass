part of 'alias_form_bloc.dart';

abstract class AliasFormState extends Equatable {
  const AliasFormState();
}

class AliasFormInitial extends AliasFormState {
  @override
  List<Object> get props => [];
}

class AliasFormSubmittedInProgress extends AliasFormState {
  const AliasFormSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class AliasFormSubmittedSuccess extends AliasFormState {
  const AliasFormSubmittedSuccess();

  @override
  List<Object> get props => [];
}

class AliasFormSubmittedFailure extends AliasFormState {
  final String message;

  const AliasFormSubmittedFailure(this.message);

  @override
  List<Object> get props => [message];
}
