import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/viettel_pay_insurance/viettel_pay_insurance_request.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InsuranceViettelPayPage extends StatefulWidget {
  const InsuranceViettelPayPage({
    Key? key,
  }) : super(key: key);

  @override
  State<InsuranceViettelPayPage> createState() => _InsuranceViettelPayPageState();
}

class _InsuranceViettelPayPageState extends State<InsuranceViettelPayPage> {
  int _progress = 0;
  late ViettelPayInsuranceRequest _request;
  WebViewController? _controller;

  @override
  void initState() {
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));
    final contractNo = context.read<AppBloc>().state.user?.contractNo ?? '';
    final phone = context.read<AppBloc>().state.user?.phone ?? '';
    final millisecond = DateTime.now().millisecondsSinceEpoch;

    _request = ViettelPayInsuranceRequest(
      msisdn: phone,
      time: millisecond,
      userName: contractNo,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: BasePage(
        backgroundColor: Colors.white,
        title: 'Bảo hiểm VDS',
        leading: BackButton(
          color: Colors.white,
          onPressed: () async {
            final router = context.router;

            if (_controller == null) {
              router.pop();
              return;
            }
            final canGoBack = await _controller?.canGoBack() ?? false;
            if (canGoBack) {
              _controller?.goBack();
            } else {
              router.pop();
            }
          },
        ),
        trailing: [
          SplashIconButton(
            icon: const Icon(CupertinoIcons.xmark, color: Colors.white),
            onTap: context.popRoute,
          )
        ],
        child: Stack(
          children: [
            const FadeAnimation(
              delay: 0.5,
              child: GradientHeaderContainer(),
            ),
            SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _progress != 100
                      ? LinearProgressIndicator(
                    value: _progress.toDouble() / 100.0,
                    backgroundColor: ColorName.blue.withOpacity(0.3),
                    valueColor:
                    const AlwaysStoppedAnimation(ColorName.blue),
                    minHeight: 2.h,
                  )
                      : SizedBox(height: 2.h),
                  Flexible(
                    fit: FlexFit.tight,
                    child: WebView(
                      initialUrl: _request.insuranceUrl,
                      javascriptMode: JavascriptMode.unrestricted,
                      onWebViewCreated: (WebViewController webViewController) {
                        _controller = webViewController;
                      },
                      onProgress: (progress) {
                        setState(() {
                          _progress = progress;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
