import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'bidv_pay_gate_event.dart';
part 'bidv_pay_gate_state.dart';

class BIDVPayGateBloc extends Bloc<BIDVPayGateEvent, BIDVPayGateState> {
  BIDVPayGateBloc() : super(BIDVPayGateInitial()) {
    on<BIDVPayLinkedCompleted>(_onBIDVPayLinkedCompleted);
    on<CashInOrderBIDVConnected>(_onCashInBIDVConnected);
    on<BIDVPayCheckLinkedCompleted>(_onCheckLinkBIDVCompleted);
  }

  FutureOr<void> _onCashInBIDVConnected(
    CashInOrderBIDVConnected event,
    Emitter<BIDVPayGateState> emit,
  ) async {
    emit(
      CashInOrderBIDVConnnectedSuccess(
        amount: event.amount,
        contractNo: event.contractNo,
      ),
    );
  }

  FutureOr<void> _onBIDVPayLinkedCompleted(
    BIDVPayLinkedCompleted event,
    Emitter<BIDVPayGateState> emit,
  ) {
    switch (event.paymentStatus) {
      case 1:
        // Giao dịch thành công
        emit(BIDVPayGateLinkedCompletedSuccess(
          orderId: event.orderId,
          amount: event.transAmount,
        ));
        break;
      case 2:
      case 5:
      default:
        // Giao dịch thất bại
        final errorCode = event.errorCode;
        if (errorCode != null) {
          emit(BIDVPayGateLinkedCompletedFailure(
              'Liên kết thất bại (Mã lỗi: $errorCode)'));
        }
        emit(const BIDVPayGateLinkedCompletedFailure('Liên kết thất bại'));
        break;
    }
  }

  FutureOr<void> _onCheckLinkBIDVCompleted(
      BIDVPayCheckLinkedCompleted event, Emitter<BIDVPayGateState> emit) async {
        // xu ly tai day trungnh
    // if (event.linkStatus.contains('1')) {
    //   emit(CheckLinkBIDVConnnectedSuccess(status: event.linkStatus));
    // } else {
    //   emit(const CheckLinkBIDVConnnectedFailure('Tài khoản chưa được liên kết'));
    // }
  }

}
