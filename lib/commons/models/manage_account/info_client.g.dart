// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'info_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InfoClientTotalResponse _$InfoClientTotalResponseFromJson(
        Map<String, dynamic> json) =>
    InfoClientTotalResponse(
      data: json['data'] == null
          ? null
          : InfoClientListResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$InfoClientTotalResponseToJson(
        InfoClientTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

InfoClientListResponse _$InfoClientListResponseFromJson(
        Map<String, dynamic> json) =>
    InfoClientListResponse(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => InfoClientResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$InfoClientListResponseToJson(
        InfoClientListResponse instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

InfoClientResponse _$InfoClientResponseFromJson(Map<String, dynamic> json) =>
    InfoClientResponse(
      documentTypeName: json['documentTypeName'] as String?,
      birthDate: json['birthDate'] as String?,
      dateOfIssue: json['dateOfIssue'] as String?,
      type: json['type'] as String?,
      areaCode: json['areaCode'] as String?,
      custId: json['custId'] as int?,
      custTypeId: json['custTypeId'] as int?,
      documentNumber: json['documentNumber'] as String?,
      documentTypeId: json['documentTypeId'] as int?,
      placeOfIssue: json['placeOfIssue'] as String?,
      custName: json['custName'] as String?,
      areaName: json['areaName'] as String?,
      street: json['street'] as String?,
      email: json['email'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      status: json['status'] as String?,
      documentCode: json['documentCode'] as String?,
    );

Map<String, dynamic> _$InfoClientResponseToJson(InfoClientResponse instance) =>
    <String, dynamic>{
      'documentTypeName': instance.documentTypeName,
      'birthDate': instance.birthDate,
      'dateOfIssue': instance.dateOfIssue,
      'type': instance.type,
      'areaCode': instance.areaCode,
      'custId': instance.custId,
      'custTypeId': instance.custTypeId,
      'documentNumber': instance.documentNumber,
      'documentTypeId': instance.documentTypeId,
      'placeOfIssue': instance.placeOfIssue,
      'custName': instance.custName,
      'areaName': instance.areaName,
      'street': instance.street,
      'email': instance.email,
      'phoneNumber': instance.phoneNumber,
      'status': instance.status,
      'documentCode': instance.documentCode,
    };

Profiles _$ProfilesFromJson(Map<String, dynamic> json) => Profiles(
      id: json['id'] as int?,
      ticketId: json['ticketId'] as int?,
      ticketPayloadId: json['ticketPayloadId'] as int?,
      profileType: json['profileType'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
    );

Map<String, dynamic> _$ProfilesToJson(Profiles instance) => <String, dynamic>{
      'id': instance.id,
      'ticketId': instance.ticketId,
      'ticketPayloadId': instance.ticketPayloadId,
      'profileType': instance.profileType,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
    };

Attachments _$AttachmentsFromJson(Map<String, dynamic> json) => Attachments(
      attachmentId: json['attachmentId'] as int?,
      ticketId: json['ticketId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
      type: json['type'] as int?,
      objectsId: json['objectsId'] as int?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
    );

Map<String, dynamic> _$AttachmentsToJson(Attachments instance) =>
    <String, dynamic>{
      'attachmentId': instance.attachmentId,
      'ticketId': instance.ticketId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
      'type': instance.type,
      'objectsId': instance.objectsId,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
    };
