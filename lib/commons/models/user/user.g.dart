// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      cusTypeId: json['cusTypeId'] as int?,
      userName: json['userName'] as String?,
      userId: json['userId'] as String?,
      birth: json['birth'] as String?,
      gender: $enumDecodeNullable(_$GenderEnumMap, json['gender'],
          unknownValue: Gender.unknown),
      address: json['address'] as String?,
      identifier: json['identifier'] as String?,
      dateOfIssue: json['dateOfIssue'] as String?,
      placeOfIssue: json['placeOfIssue'] as String?,
      contractNo: json['contractNo'] as String?,
      signDate: json['signDate'] as String?,
      effDate: json['effDate'] as String?,
      phone: json['phone'] as String?,
      email: json['email'] as String?,
      customerId: json['customerId'] as String?,
      contractId: json['contractId'] as String?,
      documentType: json['documentType'] as int?,
      isAdditional: json['isAdditional'] as int?,
      isAlertMoney: json['isAlertMoney'] as int?,
      alertMoney: json['alertMoney'] as int?,
      smsNotification: json['smsNotification'] as String?,
      accountAlias: json['accountAlias'] as String?,
      billCycle: json['billCycle'] as String?,
      billCycleMergeType: json['billCycleMergeType'] as String?,
      noticeAreaName: json['noticeAreaName'] as String?,
      noticeStreet: json['noticeStreet'] as String?,
      noticeAreaCode: json['noticeAreaCode'] as String?,
      noticeEmail: json['noticeEmail'] as String?,
      noticePhoneNumber: json['noticePhoneNumber'] as String?,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'cusTypeId': instance.cusTypeId,
      'userName': instance.userName,
      'userId': instance.userId,
      'birth': instance.birth,
      'gender': _$GenderEnumMap[instance.gender],
      'address': instance.address,
      'identifier': instance.identifier,
      'dateOfIssue': instance.dateOfIssue,
      'placeOfIssue': instance.placeOfIssue,
      'contractNo': instance.contractNo,
      'signDate': instance.signDate,
      'effDate': instance.effDate,
      'phone': instance.phone,
      'email': instance.email,
      'customerId': instance.customerId,
      'contractId': instance.contractId,
      'documentType': instance.documentType,
      'isAdditional': instance.isAdditional,
      'isAlertMoney': instance.isAlertMoney,
      'alertMoney': instance.alertMoney,
      'smsNotification': instance.smsNotification,
      'accountAlias': instance.accountAlias,
      'billCycle': instance.billCycle,
      'billCycleMergeType': instance.billCycleMergeType,
      'noticeAreaName': instance.noticeAreaName,
      'noticeStreet': instance.noticeStreet,
      'noticeAreaCode': instance.noticeAreaCode,
      'noticeEmail': instance.noticeEmail,
      'noticePhoneNumber': instance.noticePhoneNumber,
    };

const _$GenderEnumMap = {
  Gender.male: '1',
  Gender.female: '2',
  Gender.unknown: 'unknown',
};

UserResponse _$UserResponseFromJson(Map<String, dynamic> json) => UserResponse(
      user: json['data'] == null
          ? null
          : User.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserResponseToJson(UserResponse instance) =>
    <String, dynamic>{
      'data': instance.user,
      'mess': instance.mess,
    };
