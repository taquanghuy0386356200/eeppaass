import 'dart:collection';
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:epass/flavors.dart';
import 'package:jiffy/jiffy.dart';

class VNPayRequest {
  /// Phiên bản api mà merchant kết nối. Phiên bản hiện tại là 2.0.0
  final String vnpVersion = '2.0.0';

  /// Mã API sử dụng, mã cho giao dịch thanh toán là pay
  final String vnpCommand = 'pay';

  /// Mã website của merchant trên hệ thống của VNPAY
  final String vnpTmnCode = F.vnPayTmnCode;

  /// Mã Ngân hàng thanh toán
  final String vnpBankCode = 'VNPAYQR';

  /// Ngôn ngữ khách hàng đang sử dụng. Hiện tại hỗ trợ Tiếng Việt (vn),
  /// Tiếng Anh (en)
  final String vnpLocale = 'vn';

  /// Đơn vị tiền tệ sử dụng thanh toán. Hiện tại chỉ hỗ trợ VND,USD
  final String vnpCurrCode = 'VND';

  /// Mã tham chiếu của giao dịch tại hệ thống của merchant. Mã này là duy nhất
  /// đùng để phân biệt các đơn hàng gửi sang VNPAY. Không được trùng lặp trong
  /// ngày
  final String vnpTxnRef;

  /// Thông tin mô tả nội dung thanh toán (Tiếng Việt, không dấu)
  final String vnpOrderInfo;

  /// Mã danh mục hàng hóa. Mỗi hàng hóa sẽ thuộc một nhóm danh mục do VNPAY quy
  /// định. VNPAY sẽ cung cấp bảng danh mục này tại trang dành cho merchant
  /// của VNPAY
  final String vnpOrderType = 'other';

  /// Số tiền thanh toán. Số tiền không mang các ký tự phân tách thập phân, phần
  /// nghìn, ký tự tiền tệ. Để gửi số tiền thanh toán là 10,000 VND
  /// (mười nghìn VNĐ) thì merchant cần nhân thêm 100 lần (khử phần thập phân),
  /// sau đó gửi sang VNPAY là: 1000000
  final int vnpAmount;

  /// Địa chỉ trả về khi khách hàng thực hiện thanh toán xong
  final String vnpReturnUrl = 'https://epass-vdtc.com.vn';

  /// Địa chỉ IP của khách hàng thực hiện giao dịch.
  final String vnpIpAddr;

  /// Thời gian ghi nhận giao dịch tại website của merchant GMT+7,
  /// định dạng: yyyyMMddHHmmss
  final String vnpCreateDate = Jiffy().format('yyyyMMddHHmmss');

  /// Thời gian hết hạn thanh toán GMT+7, định dạng: yyyyMMddHHmmss
  final String vnpExpireDate =
      Jiffy().add(minutes: 10).format('yyyyMMddHHmmss');

  /// Giải thuật checksum sử dụng. Phiên bản hiện tại hỗ trợ SHA256
  final String vnpSecureHashType = 'SHA256';

  String get queryString {
    final sortedMap =
        SplayTreeMap<String, String>.from(_toMap(), (a, b) => a.compareTo(b));
    return Uri(queryParameters: sortedMap).query;
  }

  String get secureHash {
    final sortedMap =
        SplayTreeMap<String, String>.from(_toMap(), (a, b) => a.compareTo(b));
    final queryString = Uri(queryParameters: sortedMap).query;
    final hashData = Uri.decodeQueryComponent(queryString);
    final hash = sha256.convert(utf8.encode(F.vnPayHashSecret + hashData));
    return hash.toString();
  }

  String get paymentUrl {
    final paymentUrl =
        '${F.vnPayUrl}?$queryString&vnp_SecureHashType=$vnpSecureHashType&vnp_SecureHash=$secureHash';
    return paymentUrl;
  }

  VNPayRequest({
    required this.vnpTxnRef,
    required this.vnpAmount,
    required this.vnpOrderInfo,
    required this.vnpIpAddr,
  });

  Map<String, String> _toMap() {
    return {
      'vnp_Version': vnpVersion,
      'vnp_Command': vnpCommand,
      'vnp_TmnCode': vnpTmnCode,
      'vnp_BankCode': vnpBankCode,
      'vnp_Locale': vnpLocale,
      'vnp_CurrCode': vnpCurrCode,
      'vnp_TxnRef': vnpTxnRef,
      'vnp_OrderInfo': vnpOrderInfo,
      'vnp_OrderType': vnpOrderType,
      'vnp_Amount': (vnpAmount * 100).toString(),
      'vnp_ReturnUrl': vnpReturnUrl,
      'vnp_IpAddr': vnpIpAddr,
      'vnp_CreateDate': vnpCreateDate,
      'vnp_ExpireDate': vnpExpireDate,
      // 'vnpSecureHashType': vnpSecureHashType,
    };
  }
}

class VNPayPushPaymentRequest {
  final String paymentUrl;
  final String title;
  final String beginColor;
  final String endColor;
  final String titleColor;
  final String tmnCode;
  final bool isSandbox;

  const VNPayPushPaymentRequest({
    required this.paymentUrl,
    required this.title,
    required this.beginColor,
    required this.endColor,
    required this.titleColor,
    required this.tmnCode,
    required this.isSandbox,
  });

  Map<String, dynamic> toMap() {
    return {
      'paymentUrl': paymentUrl,
      'title': title,
      'beginColor': beginColor,
      'endColor': endColor,
      'titleColor': titleColor,
      'tmnCode': tmnCode,
      'isSandbox': isSandbox,
    };
  }
}
