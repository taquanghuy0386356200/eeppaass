part of 'link_viet_map_bloc.dart';

@immutable
abstract class LinkVietMapEvent extends Equatable {
  const LinkVietMapEvent();
}

class GetLinkVietMapStatus extends LinkVietMapEvent {
  @override
  List<Object?> get props => [];
}

class LinkVietMap extends LinkVietMapEvent {
  final String accountName;
  final String phoneNumber;
  final String token;

  const LinkVietMap({
    required this.accountName,
    required this.phoneNumber,
    required this.token});

  @override
  List<Object?> get props => [accountName, phoneNumber, token];
}

class UnLinkVietMap extends LinkVietMapEvent {
  @override
  List<Object?> get props => [];
}
