part of 'service_plan_type_bloc.dart';

class ServicePlanTypeState extends Equatable {
  final ServicePlanType servicePlanType;

  const ServicePlanTypeState({
    this.servicePlanType = ServicePlanType.monthly,
  });

  @override
  List<Object> get props => [servicePlanType];
}
