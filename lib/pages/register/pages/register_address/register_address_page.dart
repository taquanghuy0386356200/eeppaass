import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/pages/register_address/widgets/register_address_form/register_address_form.dart';
import 'package:epass/pages/register/widgets/base_register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterAddressPage extends StatefulWidget {
  const RegisterAddressPage({Key? key}) : super(key: key);

  @override
  State<RegisterAddressPage> createState() => _RegisterAddressPageState();
}

class _RegisterAddressPageState extends State<RegisterAddressPage> with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BaseRegisterPage(
      title: 'Địa chỉ nhận dán thẻ',
      step: 3,
      resizeToAvoidBottomInset: true,
      child: BlocBuilder<RegisterVehiclesBloc, RegisterVehiclesState>(
        builder: (context, state) {
          final bloc = context.read<RegisterVehiclesBloc>();
          return Form(
            key: _formKey,
            child: Column(
              children: [
                ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: state.listRegisterVehicle.length,
                  itemBuilder: (context, index) {
                    return RegisterAddressForm(index: index, bloc: bloc,);
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(height: 20.h);
                  },
                ),
                SizedBox(height: 32.h),
                SizedBox(
                  height: 56.h,
                  width: double.infinity,
                  child: PrimaryButton(
                    title: 'Tiếp theo',
                    onTap: () {
                      bool validateForm = bloc.validateAddress();
                      if (_formKey.currentState!.validate() && validateForm) {
                        context.pushRoute(const RegisterConfirmRoute());
                      }
                    },
                  ),
                ),
                SizedBox(height: 20.h),
              ],
            ),
          );
        },
      ),
    );
  }
}
