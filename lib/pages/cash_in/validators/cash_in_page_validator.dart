class CashInPageValidators {
  static const contractNoMaxLength = 20;
  static const plateNoMaxLength = 20;
  final pattern = RegExp(r'^[A-Za-z0-9]{4,20}');

  String? contractNoOrPlateNoValidator(String? target) {
    if (target == null || target.isEmpty) {
      return 'Vui lòng nhập số hợp đồng/biển số xe';
    } else if (target.length > plateNoMaxLength) {
      return 'Tối đa $plateNoMaxLength ký tự';
    } else if (!pattern.hasMatch(target)) {
      return 'Số hợp đồng/Biển số xe không hợp lệ';
    } else {
      return null;
    }
  }
}
