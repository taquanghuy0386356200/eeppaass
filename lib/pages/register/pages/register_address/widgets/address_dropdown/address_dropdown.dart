import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/repo/address_repository.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:epass/commons/widgets/textfield/text_field_circular_progress_indicator.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/register/pages/register_address/widgets/address_dropdown/address_dropdown_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum AddressDropdownType { province, district, ward }

extension AddressDropdownTypeExt on AddressDropdownType {
  String get label {
    switch (this) {
      case AddressDropdownType.province:
        return 'Tỉnh/Thành phố';
      case AddressDropdownType.district:
        return 'Quận/Huyện';
      case AddressDropdownType.ward:
        return 'Phường/Xã';
    }
  }
}

String getAreaFromAdress(String? address, AddressDropdownType areaType) {
  final re = RegExp(r"[-_,./]");
  List<String> items = address!.split(re);
  items = items.reversed.toList();
  switch (areaType) {
    case AddressDropdownType.province:
      return items.isNotEmpty ? items[0].trim() : '';
    case AddressDropdownType.district:
      return items.length > 1 ? items[1].trim() : '';
    case AddressDropdownType.ward:
      return items.length > 2 ? items[2].trim() : '';
  }
}

String getAreaCode(String? areaCode, AddressDropdownType areaType) {
  switch (areaType) {
    case AddressDropdownType.province:
      return areaCode!.length > 3 ? areaCode.substring(0, 4) : '';
    case AddressDropdownType.district:
      return areaCode!.length > 6 ? areaCode.substring(0, 7) : '';
    case AddressDropdownType.ward:
      return areaCode!.length > 9 ? areaCode.substring(0, 10) : '';
    default:
      return areaCode!;
  }
}

String getCode(String? areaCode, AddressDropdownType areaType) {
  switch (areaType) {
    case AddressDropdownType.province:
      return areaCode!.length > 3 ? areaCode.substring(0, 4) : '';
    case AddressDropdownType.district:
      return areaCode!.length > 6 ? areaCode.substring(4, 7) : '';
    case AddressDropdownType.ward:
      return areaCode!.length > 9 ? areaCode.substring(7, 10) : '';
    default:
      return areaCode!;
  }
}

class AddressDropdown extends StatelessWidget {
  final AddressDropdownType dropdownType;
  final String? provinceCode;
  final String? districtCode;
  final AddressVTT? selectedItem;
  final Function(AddressVTT? addressVTT)? onChanged;
  final bool? isValidate;

  const AddressDropdown({
    Key? key,
    required this.dropdownType,
    this.provinceCode,
    this.districtCode,
    this.onChanged,
    this.selectedItem,
    this.isValidate = true
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AddressDropdownBloc(addressRepository: getIt<IAddressRepository>())
        ..add(AddressVTTDropdownFetched(
          dropdownType: dropdownType,
          provinceCode: provinceCode,
          districtCode: districtCode,
        )),
      child: Builder(
        builder: (context) {
          return BlocBuilder<AddressDropdownBloc, AddressDropdownState>(
            builder: (context, state) {
              return PrimaryDropdown<AddressVTT>(
                label: dropdownType.label,
                enabled: state is AddressVTTFetchedSuccess && state.data.isNotEmpty,
                dropdownHeight: 205.h,
                items: state is AddressVTTFetchedSuccess ? state.data : [],
                selectedItem: selectedItem,
                validator: (value) {
                  // if (isValidate! && value == null) {
                  //   return 'Vui lòng chọn ${dropdownType.label}';
                  // }
                  // return null;
                },
                onChanged: (addressVTT) {
                  onChanged?.call(addressVTT);
                },
                suffix: state is AddressVTTFetchedInProgress
                    ? const TextFieldCircularProgressIndicator()
                    : Assets.icons.chevronDown.svg(),
              );
            },
          );
        },
      ),
    );
  }
}
