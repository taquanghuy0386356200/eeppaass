import 'package:json_annotation/json_annotation.dart';

part 'mb_get_token.g.dart';

@JsonSerializable()
class MBGetTokenRequest {
  final String grant_type;

  MBGetTokenRequest({required this.grant_type});

  factory MBGetTokenRequest.fromJson(Map<String, dynamic> json) =>
      _$MBGetTokenRequestFromJson(json);

  Map<String, dynamic> toJson() => _$MBGetTokenRequestToJson(this);
}

@JsonSerializable()
class MBGetTokenResponse {
  final String access_token;
  final int expires_in;
  final String scope;
  final String issued_at;
  final String token_type;

  MBGetTokenResponse(
      {required this.access_token,
      required this.expires_in,
      required this.scope,
      required this.issued_at,
      required this.token_type
      });

  factory MBGetTokenResponse.fromJson(Map<String, dynamic> json) =>
      _$MBGetTokenResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MBGetTokenResponseToJson(this);
}
