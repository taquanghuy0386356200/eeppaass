// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_passcode_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckPassCodeModel _$CheckPassCodeModelFromJson(Map<String, dynamic> json) =>
    CheckPassCodeModel(
      verifyResultCode: json['verifyResultCode'] as String,
      verifyDesc: json['verifyDesc'] as String,
    );

Map<String, dynamic> _$CheckPassCodeModelToJson(CheckPassCodeModel instance) =>
    <String, dynamic>{
      'verifyResultCode': instance.verifyResultCode,
      'verifyDesc': instance.verifyDesc,
    };

CheckPassCodeResponse _$CheckPassCodeResponseFromJson(
        Map<String, dynamic> json) =>
    CheckPassCodeResponse(
      data: json['data'] == null
          ? null
          : CheckPassCodeModel.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CheckPassCodeResponseToJson(
        CheckPassCodeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
