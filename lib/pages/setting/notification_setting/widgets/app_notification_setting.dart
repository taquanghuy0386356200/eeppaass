import 'package:app_settings/app_settings.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppNotificationSetting extends StatelessWidget {
  const AppNotificationSetting({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(16.0),
        topRight: Radius.circular(16.0),
      ),
      child: InkWell(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
        onTap: () => AppSettings.openNotificationSettings(),
        child: Padding(
          padding: EdgeInsets.fromLTRB(16.w, 20.h, 17.w, 20.h),
          child: Row(
            children: [
              Text(
                'Thông báo qua ứng dụng',
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Spacer(),
              Assets.icons.chevronRight.svg(),
            ],
          ),
        ),
      ),
    );
  }
}