// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'document_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DocumentClientResponse _$DocumentClientResponseFromJson(
        Map<String, dynamic> json) =>
    DocumentClientResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) =>
              DataDocumentClientResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DocumentClientResponseToJson(
        DocumentClientResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

DataDocumentClientResponse _$DataDocumentClientResponseFromJson(
        Map<String, dynamic> json) =>
    DataDocumentClientResponse(
      id: json['id'] as int?,
      code: json['code'] as String?,
      val: json['val'] as String?,
      createDate: json['createDate'] as String?,
    )..type = json['type'] as String?;

Map<String, dynamic> _$DataDocumentClientResponseToJson(
        DataDocumentClientResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'val': instance.val,
      'type': instance.type,
      'createDate': instance.createDate,
    };
