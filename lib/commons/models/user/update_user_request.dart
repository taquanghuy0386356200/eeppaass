/// email : "hieupm6@viettel.com.vn"
/// phone : "0116688007"
/// fileName : "image.png"
/// fileBase64 : ""

class UpdateUserRequest {
  UpdateUserRequest({
    this.email,
    this.phone,
    this.fileName,
    this.fileBase64,
  });

  final String? email;
  final String? phone;
  final String? fileName;
  final String? fileBase64;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['email'] = email;
    map['phone'] = phone;
    map['fileName'] = fileName;
    map['fileBase64'] = fileBase64;
    return map;
  }
}
