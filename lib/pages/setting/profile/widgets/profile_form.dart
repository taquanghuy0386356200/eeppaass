import 'package:epass/commons/validators/email_validator.dart';
import 'package:epass/commons/validators/phone_number_validator.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/email_text_field.dart';
import 'package:epass/commons/widgets/textfield/phone_number_text_field.dart';
import 'package:epass/pages/setting/profile/bloc/profile_form_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ProfileForm extends StatefulWidget {
  final String? phone;
  final String? email;

  const ProfileForm({
    Key? key,
    this.phone,
    this.email,
  }) : super(key: key);

  @override
  State<ProfileForm> createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> with PhoneNumberValidator, EmailValidator {
  final _formKey = GlobalKey<FormState>();
  final _phoneNumberController = TextEditingController();
  final _emailController = TextEditingController();

  @override
  void initState() {
    _phoneNumberController.text = widget.phone ?? '';
    _emailController.text = widget.email ?? '';
    super.initState();
  }

  @override
  void dispose() {
    _phoneNumberController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileFormBloc, ProfileFormState>(
      listener: (context, state) {
        if (state is ProfileFormSubmittedInProgress) {
          context.loaderOverlay.show();
        } else if (state is ProfileFormSubmittedSuccess) {
          context.loaderOverlay.hide();
          showSuccessSnackBBar(context: context, message: 'Thành công!');
        } else if (state is ProfileFormSubmittedFailure) {
          context.loaderOverlay.hide();
          showErrorSnackBBar(context: context, message: state.message);
        }
      },
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            PhoneNumberTextField(
              controller: _phoneNumberController,
              isMandatory: true,
            ),
            SizedBox(height: 24.h),
            EmailTextField(
              controller: _emailController,
              isMandatory: true,
            ),
            SizedBox(height: 28.h),
            Row(
              children: [
                Expanded(
                  child: SizedBox(
                    height: 56.h,
                    child: SecondaryButton(
                      title: 'Đặt lại',
                      onTap: () {
                        FocusScope.of(context).unfocus();
                        final phone = widget.phone ?? '';
                        _phoneNumberController.text = phone;
                        _phoneNumberController.selection = TextSelection.collapsed(offset: phone.length);

                        final email = widget.email ?? '';
                        _emailController.text = email;
                        _emailController.selection = TextSelection.collapsed(offset: email.length);
                      },
                    ),
                  ),
                ),
                SizedBox(width: 16.w),
                Expanded(
                  child: SizedBox(
                    height: 56.h,
                    child: PrimaryButton(
                      title: 'Lưu',
                      onTap: () {
                        FocusScope.of(context).unfocus();

                        if (_phoneNumberController.text != widget.phone || _emailController.text != widget.email) {
                          if (_formKey.currentState!.validate()) {
                            context.read<ProfileFormBloc>().add(ProfileFormSubmitted(
                                  phone: _phoneNumberController.text,
                                  email: _emailController.text,
                                ));
                          }
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
