// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'waiting_order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WaitingOrder _$WaitingOrderFromJson(Map<String, dynamic> json) => WaitingOrder(
      saleOrderId: json['saleOrderId'] as int?,
      saleOrderDate: json['saleOrderDate'] as String?,
      saleOrderType: json['saleOrderType'] as int?,
      status: json['status'] as int?,
      amount: json['amount'] as int?,
      custId: json['custId'] as int?,
      requestId: json['requestId'] as String?,
      contractNo: json['contractNo'] as String?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
      plateNumber: json['plateNumber'] as String?,
      stationId: json['stationId'] as int?,
      laneInId: json['laneInId'] as int?,
      laneOutId: json['laneOutId'] as int?,
      ticketType: json['ticketType'] as String?,
      checkinTime: json['checkinTime'] as String?,
      plateColor: json['plateColor'] as String?,
      epc: json['epc'] as String?,
      partnerId: json['partnerId'] as int?,
      ticketId: json['ticketId'] as String?,
      checkoutTime: json['checkoutTime'] as String?,
      serviceName: json['serviceName'] as String?,
      partnerName: json['partnerName'] as String?,
      stationName: json['stationName'] as String?,
      laneInName: json['laneInName'] as String?,
      laneOutName: json['laneOutName'] as String?,
    );

Map<String, dynamic> _$WaitingOrderToJson(WaitingOrder instance) =>
    <String, dynamic>{
      'saleOrderId': instance.saleOrderId,
      'saleOrderDate': instance.saleOrderDate,
      'saleOrderType': instance.saleOrderType,
      'status': instance.status,
      'amount': instance.amount,
      'custId': instance.custId,
      'requestId': instance.requestId,
      'contractNo': instance.contractNo,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'plateNumber': instance.plateNumber,
      'stationId': instance.stationId,
      'laneInId': instance.laneInId,
      'laneOutId': instance.laneOutId,
      'ticketType': instance.ticketType,
      'checkinTime': instance.checkinTime,
      'plateColor': instance.plateColor,
      'epc': instance.epc,
      'partnerId': instance.partnerId,
      'ticketId': instance.ticketId,
      'checkoutTime': instance.checkoutTime,
      'serviceName': instance.serviceName,
      'partnerName': instance.partnerName,
      'stationName': instance.stationName,
      'laneInName': instance.laneInName,
      'laneOutName': instance.laneOutName,
    };

WaitingOrderResponse _$WaitingOrderResponseFromJson(
        Map<String, dynamic> json) =>
    WaitingOrderResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => WaitingOrder.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$WaitingOrderResponseToJson(
        WaitingOrderResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
