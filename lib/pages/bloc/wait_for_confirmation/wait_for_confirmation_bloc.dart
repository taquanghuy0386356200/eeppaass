import 'dart:async';

import 'package:epass/commons/models/confirmation_wait_for/confirmation_wait_for.dart';
import 'package:epass/commons/models/waiting_order/waiting_order.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'wait_for_confirmation_event.dart';

part 'wait_for_confirmation_state.dart';

class WaitForConfirmationBloc
    extends Bloc<WaitForConfirmationEvent, WaitForConfirmationState> {
  final IUserRepository _userRepository;

  WaitForConfirmationBloc({
    required IUserRepository userRepository,
  })  : _userRepository = userRepository,
        super(const WaitForConfirmationState()) {
    on<ListWaitingOrderFetched>(_onWaitingOrderFetched);
    on<WaitingOrderCancel>(_onWaitingOrderCancel);
    on<WaitingOrderConfirm>(_onWaitingOrderConfirm);
  }
  FutureOr<void> _onWaitingOrderFetched(
    ListWaitingOrderFetched event,
    Emitter<WaitForConfirmationState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
      searchInput: null,
      message: null,
    ));

    final result = await _userRepository.getWaitingOrder(serviceId: 256);

    result.when(
      success: (data) {
        if (data != null && data.isNotEmpty) {
          emit(
            state.copyWith(
              waitingOrders: data,
              isLoading: false,
              searchInput: null,
              message: null,
            ),
          );
        }
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        message: failure.message,
        searchInput: null,
      )),
    );
  }

  FutureOr<void> _onWaitingOrderCancel(
    WaitingOrderCancel event,
    Emitter<WaitForConfirmationState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
    ));
    final result = await _userRepository.waitForConfirmationCancel(
        saleOrderId: event.saleOrderId);

    result.when(
        success: (data) {
          emit(
            state.copyWith(
              isLoading: false,
              searchInput: null,
              message: null,
            ),
          );
        },
        failure: (failure) => {
              emit(state.copyWith(
                isLoading: false,
                message: failure.message,
                searchInput: null,
              )),
            });
    add(ListWaitingOrderFetched());
  }

  FutureOr<void> _onWaitingOrderConfirm(
    WaitingOrderConfirm event,
    Emitter<WaitForConfirmationState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
    ));
    final result = await _userRepository.waitForConfirmation(
        saleOrderId: event.saleOrderId);

    result.when(
        success: (data) {
          emit(
            state.copyWith(
              isLoading: false,
              searchInput: null,
              message: null,
            ),
          );
        },
        failure: (failure) => {
              emit(state.copyWith(
                isLoading: false,
                message: failure.message,
                searchInput: null,
              )),
            });
    add(ListWaitingOrderFetched());
  }
}
