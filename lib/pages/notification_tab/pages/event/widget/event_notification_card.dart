import 'package:epass/commons/models/notification/notification_msg.dart';
import 'package:epass/commons/widgets/container/container_icon.dart';
import 'package:epass/commons/widgets/container/unread_dot.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:url_launcher/url_launcher_string.dart';

class EventNotificationCard extends StatelessWidget {
  final VoidCallback? onTap;
  final NotificationMsg notification;

  const EventNotificationCard({
    Key? key,
    required this.notification,
    this.onTap,
  }) : super(key: key);

  List<String> splitMessage(String? message) {
    if (message == null) {
      return [];
    }
    final splitMessageBySpace = message.split(RegExp(r'[\s\n\t,]'));
    List<String> urls = [];
    splitMessageBySpace.forEach((el) {
      if (isUrl(el)) {
        urls.add(el);
      }
    });
    List<String> messageList = [message];
    while (urls.isNotEmpty) {
      String last = messageList.last;
      int urlFirstIndex = last.indexOf(urls.first);
      List<String> newList = [
        last.substring(0, urlFirstIndex),
        urls.first,
        last.substring(urlFirstIndex + urls.first.length, last.length)
      ];
      messageList.removeLast();
      messageList.insertAll(messageList.length, newList);
      urls.removeAt(0);
    }
    if (messageList.last == '') {
      messageList.removeLast();
    }
    return messageList;
  }

  bool isUrl(String url) {
    return url.contains('http');
  }

  @override
  Widget build(BuildContext context) {
    final messagePieces = splitMessage(notification.message);
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.fromLTRB(20.w, 28.h, 20.w, 20.h),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 4.h),
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    ContainerIcon(
                      backgroundColor: ColorName.foam,
                      icon: Assets.icons.megaphone.svg(),
                    ),
                    notification.viewStatus == ViewStatus.unread
                        ? Positioned(
                            top: -4.r,
                            left: -4.r,
                            child: const UnreadDot(),
                          )
                        : const SizedBox.shrink(),
                  ],
                ),
              ),
              SizedBox(width: 16.w),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      notification.notificationName ?? '',
                      style: Theme.of(context).textTheme.headline6!.copyWith(
                            color: notification.viewStatus == ViewStatus.unread
                                ? ColorName.textGray1
                                : ColorName.borderColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                          ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 4.h),
                    Text(
                      notification.pushDate ?? '',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            color: ColorName.borderColor,
                          ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 12.h),
                    RichText(
                      text: TextSpan(
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: notification.viewStatus == ViewStatus.unread
                              ? ColorName.textGray1
                              : ColorName.borderColor,
                        ),
                        children: List.generate(messagePieces.length, (index) {
                          return TextSpan(
                            text: messagePieces[index],
                            style: isUrl(messagePieces[index]) ? Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: ColorName.primaryColor,
                              decoration: TextDecoration.underline,
                            ) : Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: notification.viewStatus == ViewStatus.unread
                                  ? ColorName.textGray1
                                  : ColorName.borderColor,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = isUrl(messagePieces[index]) ? () async {
                                final canLaunchExternal = await canLaunchUrlString(messagePieces[index]);
                                if (canLaunchExternal) {
                                  await launchUrlString(messagePieces[index]);
                                }
                              } : null
                          );
                        })
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
