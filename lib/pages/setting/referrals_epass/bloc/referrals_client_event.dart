part of 'referrals_client_bloc.dart';

abstract class ReferralsClientEvent extends Equatable {
  const ReferralsClientEvent();
}

class GetConfigTimeDisableBtn extends ReferralsClientEvent {
  final String tableName;

  const GetConfigTimeDisableBtn({
    this.tableName = 'CONFIG_PARAM',
  });

  @override
  List<Object?> get props => [];
}

class SendInfoReferralsClientEvent extends ReferralsClientEvent {
  final String plateNumber;
  final String phoneNumber;

  const SendInfoReferralsClientEvent({
    required this.plateNumber,
    required this.phoneNumber,
  });

  @override
  List<Object?> get props => [
        plateNumber,
        phoneNumber,
      ];
}

class ConfirmReferralsClientEvent extends ReferralsClientEvent {
  final String plateNumber;
  final String phoneNumber;
  final String secretKey;

  const ConfirmReferralsClientEvent({
    required this.plateNumber,
    required this.phoneNumber,
    required this.secretKey,
  });

  @override
  List<Object?> get props => [];
}

class CloseSuccessReferralsClientEvent extends ReferralsClientEvent {
  @override
  List<Object?> get props => [];
}
