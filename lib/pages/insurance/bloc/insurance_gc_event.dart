part of 'insurance_gc_bloc.dart';

abstract class InsuranceGcEvent extends Equatable {
  const InsuranceGcEvent();
}

class ContractEncryptedFetched extends InsuranceGcEvent {
  const ContractEncryptedFetched();

  @override
  List<Object> get props => [];
}
