import 'package:epass/commons/models/transaction_packing/transaction_parking.dart';
import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/widgets/container/dashed_separator.dart';
import 'package:epass/commons/widgets/container/transaction_parking_clipper.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionParkingCard extends StatelessWidget {
  final TransactionPacking item;

  const TransactionParkingCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleShadow(
      offset: const Offset(0, 1),
      opacity: 0.1,
      sigma: 10,
      child: ClipPath(
        clipper: ParkingClipper(),
        child: Container(
          padding: EdgeInsets.fromLTRB(14.w, 16.h, 16.w, 16.h),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16.0),
              topRight: Radius.circular(16.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          item.plateNumber ?? "",
                          textAlign: TextAlign.center,
                          style:
                              Theme.of(context).textTheme.headline6!.copyWith(
                                    fontWeight: FontWeight.w700,
                                    color: ColorName.textGray,
                                  ),
                        ),
                        SizedBox(height: 8.h),
                        Text(
                          item.lanOut ?? "",
                          textAlign: TextAlign.center,
                          style:
                              Theme.of(context).textTheme.bodyText2!.copyWith(
                                    color: ColorName.textGray,
                                  ),
                        ),
                        SizedBox(height: 8.h),
                        Text(
                          item.stationName ?? "",
                          textAlign: TextAlign.center,
                          style:
                              Theme.of(context).textTheme.bodyText2!.copyWith(
                                    fontWeight: FontWeight.w700,
                                    color: ColorName.textGray,
                                  ),
                          maxLines: 2,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "Giờ gửi xe",
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.borderColor,
                                  ),
                        ),
                        SizedBox(height: 8.h),
                        Text(
                          item.timeRequestIn ?? "",
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.textGray,
                                  ),
                        ),
                        SizedBox(height: 8.h),
                        Text(
                          "Giờ lấy xe",
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.borderColor,
                                  ),
                        ),
                        SizedBox(height: 8.h),
                        Text(
                          item.timeRequestOut ?? "",
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.textGray,
                                  ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 20.h),
              const DashedSeparator(
                height: 0.3,
                color: ColorName.borderColor,
              ),
              SizedBox(height: 12.h),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Loại vé",
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.borderColor,
                                  ),
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          item.typeTicket ?? '',
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.textGray,
                                  ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      alignment: Alignment.topRight,
                      child: GradientText(
                        double.tryParse(item.fee ?? "0")?.toInt()
                                .formatCurrency(symbol: '₫') ??
                            '',
                        gradient: const LinearGradient(colors: [
                          ColorName.primaryGradientStart,
                          ColorName.primaryGradientEnd,
                        ]),
                        style: Theme.of(context).textTheme.headline5!.copyWith(
                              fontWeight: FontWeight.w700,
                            ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
