import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/validators/password_validator.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/password_text_field.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/setting/change_password/bloc/change_password_form_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChangePasswordForm extends StatefulWidget {
  const ChangePasswordForm({Key? key}) : super(key: key);

  @override
  State<ChangePasswordForm> createState() => _ChangePasswordFormState();
}

class _ChangePasswordFormState extends State<ChangePasswordForm>
    with PasswordValidator {
  final _formKey = GlobalKey<FormState>();
  final _oldPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  @override
  void dispose() {
    _oldPasswordController.dispose();
    _newPasswordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ChangePasswordFormBloc, ChangePasswordFormState>(
      listener: (context, state) {
        if (state is ChangePasswordFormSubmittedSuccess) {
          // Turn off biometric login
          final isBiometricLoginEnabled =
              context.read<SettingBloc>().state.isBiometricLoginEnabled;

          if (isBiometricLoginEnabled) {
            context.read<SettingBloc>().add(const BiometricLoginToggled(false));
          }

          var message =
              'Đổi mật khẩu thành công, vui lòng đăng nhập lại bằng mật khẩu mới.';
          message += isBiometricLoginEnabled
              ? '\nTính năng đăng nhập'
                  ' nhanh sẽ tạm thời được tắt đi để bảo đảm bảo mật cho tài'
                  ' khoản của bạn, vui lòng bật lại sau khi đăng nhập nếu có'
                  ' nhu cầu sử dụng.'
              : '';

          // show success dialog
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (dialogContext) {
              return SuccessDialog(
                content: message,
                buttonTitle: 'Đăng nhập lại',
                onButtonTap: () {
                  Navigator.of(dialogContext).pop();
                  context.router.replaceAll([const LoginRoute()]);
                },
              );
            },
          );
        } else if (state is ChangePasswordFormSubmittedFailure) {
          showErrorSnackBBar(context: context, message: state.message);
        }
      },
      builder: (context, state) {
        return Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PasswordTextField(
                controller: _oldPasswordController,
                maxLength: 20,
                labelText: 'Mật khẩu hiện tại',
                hintText: 'Nhập mật khẩu hiện tại',
                obscureText: true,
                enabled: state is! ChangePasswordFormSubmittedInProgress,
                validator: passwordJustValidateLength,
              ),
              SizedBox(height: 16.h),
              PasswordTextField(
                controller: _newPasswordController,
                labelText: 'Mật khẩu mới',
                maxLength: 20,
                hintText: 'Nhập mật khẩu mới',
                obscureText: true,
                enabled: state is! ChangePasswordFormSubmittedInProgress,
                validator: passwordJustValidateLength,
              ),
              SizedBox(height: 16.h),
              PasswordTextField(
                controller: _confirmPasswordController,
                labelText: 'Xác nhận mật khẩu',
                hintText: 'Nhập lại mật khẩu mới',
                obscureText: true,
                maxLength: 20,
                enabled: state is! ChangePasswordFormSubmittedInProgress,
                validator: (confirmPassword) {
                  if (confirmPassword == null || confirmPassword.isEmpty) {
                    return 'Vui lòng nhập Xác nhận mật khẩu';
                  } else if (confirmPassword != _newPasswordController.text) {
                    return 'Xác nhận mật khẩu không khớp với mật khẩu mới';
                  } else {
                    return null;
                  }
                },
              ),
              SizedBox(height: 32.h),
              SizedBox(
                height: 56.h,
                width: double.infinity,
                child: LoadingPrimaryButton(
                  title: 'Đổi mật khẩu',
                  isLoading: state is ChangePasswordFormSubmittedInProgress,
                  onTap: state is! ChangePasswordFormSubmittedInProgress
                      ? () {
                          FocusScope.of(context).unfocus();
                          if (_formKey.currentState!.validate()) {
                            context
                                .read<ChangePasswordFormBloc>()
                                .add(ChangePasswordFormSubmitted(
                                  oldPassword: _oldPasswordController.text,
                                  newPassword: _newPasswordController.text,
                                ));
                          }
                        }
                      : null,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
