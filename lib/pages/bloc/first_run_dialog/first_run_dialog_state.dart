part of 'first_run_dialog_bloc.dart';

class FirstRunDialogState extends Equatable {
  final bool? biometricLoginDialogShowed;
  final bool? aliasDialogShowed;

  const FirstRunDialogState({
    this.biometricLoginDialogShowed,
    this.aliasDialogShowed,
  });

  @override
  List<Object?> get props => [biometricLoginDialogShowed, aliasDialogShowed];

  FirstRunDialogState copyWith({
    bool? biometricLoginDialogShowed,
    bool? aliasDialogShowed,
  }) {
    return FirstRunDialogState(
      biometricLoginDialogShowed:
          biometricLoginDialogShowed ?? this.biometricLoginDialogShowed,
      aliasDialogShowed: aliasDialogShowed ?? this.aliasDialogShowed,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'biometricLoginDialogShown': biometricLoginDialogShowed,
      'aliasDialogShown': aliasDialogShowed,
    };
  }

  factory FirstRunDialogState.fromJson(Map<String, dynamic> json) {
    return FirstRunDialogState(
      biometricLoginDialogShowed: json['biometricLoginDialogShown'] != null
          ? json['biometricLoginDialogShown'] as bool
          : null,
      aliasDialogShowed: json['aliasDialogShown'] != null
          ? json['aliasDialogShown'] as bool
          : null,
    );
  }
}
