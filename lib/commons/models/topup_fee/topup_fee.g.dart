// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'topup_fee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopupFee _$TopupFeeFromJson(Map<String, dynamic> json) => TopupFee(
      topupChannelCode: $enumDecode(
          _$TopupChannelCodeEnumMap, json['topupChannelCode'],
          unknownValue: TopupChannelCode.unknown),
      topupChannelName: json['topupChannelName'] as String?,
      valueMin: json['valueMin'] as int?,
      serviceFeeType: json['serviceFeeType'] as int?,
      serviceFeeValAdd: json['serviceFeeValAdd'] as int?,
      serviceFeeVal: (json['serviceFeeVal'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$TopupFeeToJson(TopupFee instance) => <String, dynamic>{
      'topupChannelCode': _$TopupChannelCodeEnumMap[instance.topupChannelCode]!,
      'topupChannelName': instance.topupChannelName,
      'valueMin': instance.valueMin,
      'serviceFeeType': instance.serviceFeeType,
      'serviceFeeValAdd': instance.serviceFeeValAdd,
      'serviceFeeVal': instance.serviceFeeVal,
    };

const _$TopupChannelCodeEnumMap = {
  TopupChannelCode.BankPlus: 'BankPlus',
  TopupChannelCode.DomesticATM: 'DomesticATM',
  TopupChannelCode.InternationalCard: 'InternationalCard',
  TopupChannelCode.ViettelPay: 'ViettelPay',
  TopupChannelCode.MoMo: 'MoMo',
  TopupChannelCode.VNPAY: 'VNPAY',
  TopupChannelCode.BankTransfer: 'BankTransfer',
  TopupChannelCode.PayNow: 'PayNow',
  TopupChannelCode.MobileMoney: 'MobileMoney',
  TopupChannelCode.MB: 'MB',
  TopupChannelCode.BIDV: 'BIDV',
  TopupChannelCode.VPBank: 'VPBank',
  TopupChannelCode.Agribank: 'Agribank',
  TopupChannelCode.TPBank: 'TPBank',
  TopupChannelCode.VIB: 'VIB',
  TopupChannelCode.OCBBank: 'OCBBank',
  TopupChannelCode.PVcomBank: 'PVcomBank',
  TopupChannelCode.MSB: 'MSB',
  TopupChannelCode.ZaloPay: 'ZaloPay',
  TopupChannelCode.VnptMoney: 'VnptMoney',
  TopupChannelCode.unknown: 'unknown',
};

TopupFeeData _$TopupFeeDataFromJson(Map<String, dynamic> json) => TopupFeeData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => TopupFee.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$TopupFeeDataToJson(TopupFeeData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

TopupFeesResponse _$TopupFeesResponseFromJson(Map<String, dynamic> json) =>
    TopupFeesResponse(
      data: json['data'] == null
          ? null
          : TopupFeeData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TopupFeesResponseToJson(TopupFeesResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
