import 'package:epass/commons/api/clients/cc/cc_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/report/report_attachment_file.dart';
import 'package:epass/commons/models/report/report_request.dart';
import 'package:simple_result/simple_result.dart';

abstract class IReportRepository {
  Future<Result<String?, Failure>> createReport({
    required String custId,
    required String contractNo,
    required String contractId,
    required String custTypeId,
    required String custName,
    String? email,
    String? plateNumber,
    String? plateTypeCode,
    String? phoneNumber,
    String? location,
    required String priorityId,
    required String l1TicketTypeId,
    required String l2TicketTypeId,
    String? l3TicketTypeId,
    required String provinceName,
    String? districtName,
    String? communeName,
    required String areaCode,
    String? stationId,
    String? stationName,
    String? stageId,
    String? stageName,
    required String contentReceive,
    String? ticketKind,
    required String requestDate,
    String? supportInfo,
    required String phoneContact,
    List<ReportAttachmentFileRequest>? attachmentFiles,
    String? otp,
  });
}

class ReportRepository implements IReportRepository {
  final CCClient _ccClient;

  ReportRepository({
    required CCClient ccClient,
  }) : _ccClient = ccClient;

  @override
  Future<Result<String?, Failure>> createReport({
    required String custId,
    required String contractNo,
    required String contractId,
    required String custTypeId,
    required String custName,
    String? email,
    String? plateNumber,
    String? plateTypeCode,
    String? phoneNumber,
    String? location,
    required String priorityId,
    required String l1TicketTypeId,
    required String l2TicketTypeId,
    String? l3TicketTypeId,
    required String provinceName,
    String? districtName,
    String? communeName,
    required String areaCode,
    String? stationId,
    String? stationName,
    String? stageId,
    String? stageName,
    required String contentReceive,
    String? ticketKind,
    required String requestDate,
    String? supportInfo,
    required String phoneContact,
    List<ReportAttachmentFileRequest>? attachmentFiles,
    String? otp,
  }) async {
    try {
      final request = ReportRequest(
        custId: custId,
        contractNo: contractNo,
        contractId: contractId,
        custTypeId: custTypeId,
        custName: custName,
        email: email,
        plateNumber: plateNumber,
        plateTypeCode: plateTypeCode,
        phoneNumber: phoneNumber,
        location: location,
        priorityId: priorityId,
        l1TicketTypeId: l1TicketTypeId,
        l2TicketTypeId: l2TicketTypeId,
        l3TicketTypeId: l3TicketTypeId,
        provinceName: provinceName,
        districtName: districtName,
        communeName: communeName,
        areaCode: areaCode,
        stationId: stationId,
        stationName: stationName,
        stageId: stageId,
        stageName: stageName,
        contentReceive: contentReceive,
        ticketKind: ticketKind,
        requestDate: requestDate,
        supportInfo: supportInfo,
        phoneContact: phoneContact,
        attachmentFiles: attachmentFiles,
        otp: otp,
      );

      final response = await _ccClient.report(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
