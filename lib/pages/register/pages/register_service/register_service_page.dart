import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/pages/invoice_setting/invoice_cycle.dart';
import 'package:epass/pages/register/bloc/register_customer/register_customer_bloc.dart';
import 'package:epass/pages/register/bloc/register_service/register_service_bloc.dart';
import 'package:epass/pages/register/bloc/request_otp/register_otp_bloc.dart';
import 'package:epass/pages/register/widgets/base_register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterServicePage extends StatefulWidget {
  const RegisterServicePage({Key? key}) : super(key: key);

  @override
  State<RegisterServicePage> createState() => _RegisterServicePageState();
}

class _RegisterServicePageState extends State<RegisterServicePage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: BaseRegisterPage(
        title: 'Lựa chọn phương án',
        step: 5,
        child: BlocBuilder<RegisterServiceBloc, RegisterServiceState>(
          builder: (context, state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Chu kỳ hoá đơn',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.sp,
                      ),
                ),
                SizedBox(height: 8.h),
                RadioGroupButtons<InvoiceCycle>(
                  scrollDirection: Axis.horizontal,
                  labels: InvoiceCycle.values.map((e) => e.label).toList(),
                  values: InvoiceCycle.values,
                  defaultValue: state.invoiceCycle,
                  onChanged: (invoiceCycle) {
                    context.read<RegisterServiceBloc>().add(RegisterServiceUpdated(
                          invoiceCycle: invoiceCycle,
                        ));
                  },
                ),
                if (state.invoiceCycle == InvoiceCycle.monthly)
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 16.h),
                      Text(
                        'Gộp hoá đơn',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                            ),
                      ),
                      SizedBox(height: 8.h),
                      RadioGroupButtons<InvoiceCycleGroupType>(
                        labels: InvoiceCycleGroupType.values.map((e) => e.label).toList(),
                        values: InvoiceCycleGroupType.values,
                        defaultValue: state.invoiceCycleGroupType,
                        onChanged: (invoiceCycleGroupType) {
                          context.read<RegisterServiceBloc>().add(RegisterServiceUpdated(
                                invoiceCycleGroupType: invoiceCycleGroupType,
                              ));
                        },
                      ),
                    ],
                  ),
                SizedBox(height: 32.h),
                Builder(
                  builder: (context) {
                    return BlocConsumer<RegisterOtpBloc, RegisterOtpState>(
                      listener: (context, state) async {
                        if (state is RegisterOtpRequestedFailure) {
                          await showErrorSnackBBar(context: context, message: state.message);
                        } else if (state is RegisterOtpRequestedSuccess) {
                          if (context.router.current.name != RegisterOTPRoute.name) {
                            context.pushRoute(const RegisterOTPRoute());
                          }
                        }
                      },
                      builder: (context, state) {
                        return SizedBox(
                          height: 56.h,
                          width: double.infinity,
                          child: LoadingPrimaryButton(
                            title: 'Đăng ký',
                            enabled: state is! RegisterOtpRequestedInProgress,
                            isLoading: state is RegisterOtpRequestedInProgress,
                            onTap: () async {
                              final phoneNumber = context.read<RegisterCustomerBloc>().state.phoneNumber;

                              if (phoneNumber != null) {
                                context.read<RegisterOtpBloc>().add(RegisterOtpRequested(phoneNumber));
                              } else {
                                await showErrorSnackBBar(context: context, message: 'Chưa nhập số điện thoại');
                              }
                            },
                          ),
                        );
                      },
                    );
                  }
                ),
                SizedBox(height: 16.h),
              ],
            );
          },
        ),
      ),
    );
  }
}
