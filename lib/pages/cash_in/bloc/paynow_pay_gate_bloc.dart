import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'paynow_pay_gate_event.dart';

part 'paynow_pay_gate_state.dart';

class PayNowPayGateBloc
    extends Bloc<PayNowPayGateEvent, PayNowPayGateState> {
  PayNowPayGateBloc() : super(PayNowPayGateInitial()) {
    on<PayNowPayLinkedCompleted>(_onPayNowPayLinkedCompleted);
  }

  FutureOr<void> _onPayNowPayLinkedCompleted(
    PayNowPayLinkedCompleted event,
    Emitter<PayNowPayGateState> emit,
  ) {
    switch (event.paymentStatus) {
      case 1:
        // Giao dịch thành công
        emit(PayNowPayGateLinkedCompletedSuccess(
          orderId: event.orderId,
          amount: event.transAmount,
        ));
        break;
      case 2:
      case 5:
      default:
        // Giao dịch thất bại
        final errorCode = event.errorCode;
        if (errorCode != null) {
          emit(PayNowPayGateLinkedCompletedFailure(
              'Giao dịch thất bại (Mã lỗi: $errorCode)'));
        }
        emit(const PayNowPayGateLinkedCompletedFailure('Giao dịch thất bại'));
        break;
    }
  }
}
