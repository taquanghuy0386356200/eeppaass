import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/attachment_file_bloc/attachment_file_bloc.dart';
import 'package:epass/commons/widgets/support_center_widgets/attachment_files_widget.dart';
import 'package:epass/commons/widgets/support_center_widgets/feedback_horizontal_widget.dart';
import 'package:epass/commons/widgets/support_center_widgets/feedback_vertical_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FeedbackExpandedContent extends StatefulWidget {
  const FeedbackExpandedContent({
    Key? key,
    required this.feedback,
  }) : super(key: key);

  final FeedbackModel feedback;

  @override
  State<FeedbackExpandedContent> createState() => _FeedbackExpandedContentState();
}

class _FeedbackExpandedContentState extends State<FeedbackExpandedContent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.feedback.l2TicketTypeName?.isNotEmpty ?? false)
          FeedbackHorizontalWidget(
            title: 'Thể loại',
            content: widget.feedback.l2TicketTypeName!,
          ),
        if (widget.feedback.l2TicketTypeName?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (widget.feedback.l3TicketTypeName?.isNotEmpty ?? false)
          FeedbackHorizontalWidget(
            title: 'Loại phản ánh',
            content: widget.feedback.l3TicketTypeName!,
          ),
        if (widget.feedback.l3TicketTypeName?.isNotEmpty ?? false) SizedBox(height: 16.h),
        FeedbackVerticalWidget(
          title: 'Ý kiến đóng góp',
          content: widget.feedback.contentReceive ?? '',
        ),
        SizedBox(height: 16.h),
        FeedbackHorizontalWidget(
          title: 'Thời gian góp ý',
          content: widget.feedback.createDate ?? '',
        ),
        SizedBox(height: 16.h),
        FeedbackHorizontalWidget(
          title: 'Trạng thái xử lý',
          content: widget.feedback.statusName!,
          contentFontWeight: FontWeight.w700,
        ),
        SizedBox(height: 16.h),
        if (widget.feedback.processTime?.isNotEmpty ?? false)
          FeedbackHorizontalWidget(
            title: 'Thời gian xử lý',
            content: widget.feedback.processTime ?? '',
          ),
        if (widget.feedback.processTime?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (widget.feedback.staffName?.isNotEmpty ?? false)
          FeedbackHorizontalWidget(
            title: 'Người xử lý',
            content: widget.feedback.staffName ?? '',
          ),
        if (widget.feedback.staffName?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (widget.feedback.processContent?.isNotEmpty ?? false)
          FeedbackVerticalWidget(
            title: 'Nội dung xử lý',
            content: widget.feedback.processContent ?? '',
          ),
        if (widget.feedback.processContent?.isNotEmpty ?? false) SizedBox(height: 16.h),
        if (widget.feedback.attachmentFiles?.isNotEmpty ?? false)
          AttachmentFilesWidget(
            attachmentFiles: widget.feedback.attachmentFiles ?? [],
            onAttachmentTap: (String fileName, int attachmentFileId) {
              context.read<AttachmentFileBloc>().add(AttachmentFileDownloaded(
                    fileName: fileName,
                    attachmentFileId: attachmentFileId,
                  ));
            },
          ),
        if (widget.feedback.attachmentFiles?.isNotEmpty ?? false) SizedBox(height: 16.h),
      ],
    );
  }
}
