import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// data : 1
/// mess : {"code":1,"description":"Thành công"}

part 'count_insurance_response.g.dart';

@JsonSerializable()
class CountInsuranceResponse {
  CountInsuranceResponse({
    this.data,
    this.mess,
  });

  final int? data;
  final MessageData? mess;

  factory CountInsuranceResponse.fromJson(Map<String, dynamic> json) =>
      _$CountInsuranceResponseFromJson(json);
}
