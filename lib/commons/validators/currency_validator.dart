import 'package:intl/intl.dart';

class CurrencyValidator {
  final maxLength = 13;
  final _expression = RegExp(r'^[0-9][0-9.]*[0-9]$');

  String? currencyValidator(String? currency) {
    if (currency == null || currency.isEmpty) {
      return 'Vui lòng nhập Số tiền';
    } else if (!_expression.hasMatch(currency) ||
        NumberFormat().parse(currency.replaceAll('.', '')) <= 0) {
      return 'Số tiền không hợp lệ';
    }
    return null;
  }
}
