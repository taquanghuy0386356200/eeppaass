// // ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'dart:convert';

// import 'package:crypto/crypto.dart';
// import 'package:http/http.dart';
// import 'package:uuid/uuid.dart';
// import 'package:xml/xml.dart' as xml;

// import 'package:epass/flavors.dart';

// class LinkBIDVPayGateRequest {
//   /// Mã dịch vụ. Do BIDV cung cấp
//   final String serviceID = '287002';

//   /// Mã đối tác mà BIDV đã cung cấp
//   final String merchantCode = F.bidvPayGateMerchantId;

//   /// Tên đối tác
//   final String merchantName = 'EPASS';

//   /// Ngày liên kết
//   String transDate = '';

//   /// Mã giao dịch duy nhất bên phía đối tác. Lưu ý không dùng tiếng Việt
//   String transId = '';

//   /// Thông tin mô tả
//   final String transDes = 'Nap tien vao tai khoan ePass';

//   /// Giá trị giao dịch. Mặc định bằng 0
//   final int transAmount = 0;

//   String curr = '';

//   /// Payer_Id của đối tác. Mặc định là số điện thoại của khách hàng
//   final String payerId;

//   /// Payer_Name của đối tác. Mặc định là họ và tên tên của khách hàng
//   final String payerName;

//   /// Địa chỉ của khách hàng
//   String payerAdd;

//   String type = '';

//   String customerId = '';

//   String customerName = '';

//   String issueDate = '';

//   /// Mã kênh giao dịch. Mặc định Mobile=211701, Web=211601
//   final String channelId = '211701';

//   final String linkType = '1';

//   String otpNumber;

//   /// Thông tin bổ sung. Bao gồm:
//   /// 0 KH chủ động hủy liên kết. 1 hủy liên kết hộ khách hàng
//   final String unlinkType;

//   /// Số điện thoại của khách hàng
//   final String payerMobile;

//   /// Số CMT/CCCD/hộ chiếu của khách hàng
//   final String payerIdentity;

//   /// Địa chỉ để chuyển sau khi KH liên kết. Url này sử dụng trong trường
//   /// hợp đối tác không truyền trong quá trình liên kết.
//   final String returnUrl = '/success';

//   /// Địa chỉ để chuyển sau nếu KH hủy giao dịch
//   final String cancelUrl = '/cancel';

//   LinkBIDVPayGateRequest({
//     required this.payerId,
//     required this.payerName,
//     required this.payerAdd,
//     required this.otpNumber,
//     required this.unlinkType,
//     required this.payerMobile,
//     required this.payerIdentity,
//   });

//   Future<String> getBidvGateUrl() async {
//     var uuid = const Uuid();

//     String transId = uuid.v1();

//     String transDate = DateTime.now().toString();

//     const privateKey = 'BIDV_EPASS#123';

//     final moreInfo =
//         '${unlinkType.isNotEmpty ? unlinkType : ''}|${payerMobile.isNotEmpty ? payerMobile : ''}|${payerIdentity.isNotEmpty ? payerIdentity : ''}';

//     // Encode your message with the private key
//     var message = '$privateKey|'
//         '$serviceID|'
//         '$merchantCode|'
//         '${merchantName.isNotEmpty ? merchantName : ''}|'
//         '${transDate.isNotEmpty ? transDate : ''}|'
//         '$transId|'
//         '$transDes|'
//         '$transAmount|'
//         '$curr|'
//         '$payerId|'
//         '$payerName|'
//         '$payerAdd|'
//         '$type|'
//         '$customerId|'
//         '$customerName|'
//         '$issueDate|'
//         '$channelId|'
//         '$linkType|'
//         '$otpNumber|'
//         '$moreInfo';
//     final secureCode = md5.convert(utf8.encode(message)).toString();

//     Response response = await post(
//       Uri.parse("https://www.bidv.net/BIDVGateway/WS"),
//       headers: {
//         'content-type': 'text/xml; charset=utf-8',
//         'SOAPAction': '/NCC_WSDL.serviceagent//link',
//       },
//       body: utf8.encode('''<?xml version="1.0" encoding="utf-8"?>
//     <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ncc="NCCWalletInput_Schema">
//     <soapenv:Header/>
//     <soapenv:Body>
//       <ncc:root>
//          <ncc:Service_Id>$serviceID</ncc:Service_Id>
//          <ncc:Merchant_Id>$merchantCode</ncc:Merchant_Id>
//          <ncc:Merchant_Name>$merchantName</ncc:Merchant_Name>
//          <ncc:Trandate>$transDate</ncc:Trandate>
//          <ncc:Trans_Id>$transId</ncc:Trans_Id>
//          <ncc:Trans_Desc>$transDes</ncc:Trans_Desc>
//          <ncc:Amount>$transAmount</ncc:Amount>
//          <ncc:Curr/>
//          <ncc:Payer_Id>$payerId</ncc:Payer_Id>
//          <ncc:Payer_Name>$payerName</ncc:Payer_Name>
//          <ncc:Payer_Addr/>
//          <ncc:Type/>
//          <ncc:Custmer_Id/>
//          <ncc:Customer_Name/>
//          <ncc:IssueDate/>
//          <ncc:Channel_Id>$channelId</ncc:Channel_Id>
//          <ncc:Link_Type>$linkType</ncc:Link_Type>
//          <ncc:Otp_Number/>
//          <ncc:More_Info>$moreInfo</ncc:More_Info>
//          <ncc:Secure_Code>$secureCode</ncc:Secure_Code>
//          <ncc:FromProcess/>
//       </ncc:root>
//     </soapenv:Body>
//     </soapenv:Envelope>'''),
//     );

//     final xmlString = response.body;
//     final xml.XmlDocument document = xml.XmlDocument.parse(xmlString);

//     final elements = document.findAllElements('ns0:Redirect_Url').first.text;

//     return elements;
//   }
// }

// class LinkBIDVPayGateRequestResponse {
//   final String transId;
//   final int paymentStatus;
//   final String? errorCode;
//   final int transAmount;

//   LinkBIDVPayGateRequestResponse({
//     required this.transId,
//     required this.paymentStatus,
//     this.errorCode,
//     required this.transAmount,
//   });
// }
