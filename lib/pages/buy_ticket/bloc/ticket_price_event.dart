part of 'ticket_price_bloc.dart';

@immutable
abstract class TicketPriceEvent extends Equatable {
  const TicketPriceEvent();
}

class TicketPriceFetched extends TicketPriceEvent {
  final List<Vehicle> vehicles;
  final ServicePlanType servicePlanType;
  final StationStage stationStage;
  final DateTime effectiveDate;
  final DateTime expireDate;

  const TicketPriceFetched({
    required this.vehicles,
    required this.servicePlanType,
    required this.stationStage,
    required this.effectiveDate,
    required this.expireDate,
  });

  @override
  List<Object> get props =>
      [vehicles, servicePlanType, stationStage, effectiveDate, expireDate];
}

class TicketPriceInValid extends TicketPriceEvent {
  @override
  List<Object?> get props => throw UnimplementedError();
}
