/*
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJiOEZHOURyWm9fOVJ6X1NCWFRJM0ZtN2hyT3Z4S0JPVklsLXU3d2dQbmVzIn0.eyJleHAiOjE2Mzg0NzQ1OTksImlhdCI6MTYzODQzMTM5OSwianRpIjoiOTlkMWVjZmEtZTQ2ZS00YjM5LWEzOTMtNTRjZWIxNTAxYzRiIiwiaXNzIjoiaHR0cDovL2xvZ2luLmVwYXNzLXZkdGMuY29tLnZuL2F1dGgvcmVhbG1zL2V0Yy1pbnRlcm5hbCIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI1OGI0MzhhZC0wMGFkLTRjZDktOGUwOS05ODc4MmY5ODVhODEiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJtb2JpbGUtYXBwLWNodXB0Iiwic2Vzc2lvbl9zdGF0ZSI6IjdkZDRhZDZhLTZiZWMtNGNkZi1hYmZmLTM2Mzk3Njg3M2JiMyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovL2xvY2FsaG9zdDo0MjAwIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiQsOZSSBIxq_GoE5HIEdJQU5HIiwicHJlZmVycmVkX3VzZXJuYW1lIjoidjAwMDAwODE4NyIsImdpdmVuX25hbWUiOiJCw5lJIEjGr8agTkcgR0lBTkciLCJlbWFpbCI6ImdpYW5nYmgxQHZpZXR0ZWwuY29tLnZuIn0.EKMHY8ch7uoiQ0I6kk9Sll1VpauVjTtVSymrX-JHaQms0m4fc6-k9WZ3BvKwcOy1qEZAw2--JOZLjYaX4JZu8yg6NeM11cEGGokzt3sGzZZPuo6WRn0zDxJMWBlABBPSrQVMCtlpY0H64ywFicVhBPvzBJ_V476FwdbVzgf5FHhhupqlUkJ_acn669UtdKkVybamLeZ87xzK6y9ZEe35B6HMFno3NT10oFthG422JTa-a0lU_UPy6nbqjvvfunhVyeoPF5J9eGsgmYvJj3AqYUUf2iYDNC8bnIuIJwffgM_8lIUrXk4VJBgIBwpM-3DoeyUJeE0ERXgEsrpbHLEGWg",
    "expires_in": 43200,
    "refresh_expires_in": 43200,
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjNTExMjYzMC1jMjM4LTQ0N2ItOWIzZC0yZGYwZDI3Y2I2ZWMifQ.eyJleHAiOjE2Mzg0NzQ1OTksImlhdCI6MTYzODQzMTM5OSwianRpIjoiMDQ1ZDdiMjAtZGMwYy00YjZlLWI2MTYtMmFiMTk0OGIzZWNmIiwiaXNzIjoiaHR0cDovL2xvZ2luLmVwYXNzLXZkdGMuY29tLnZuL2F1dGgvcmVhbG1zL2V0Yy1pbnRlcm5hbCIsImF1ZCI6Imh0dHA6Ly9sb2dpbi5lcGFzcy12ZHRjLmNvbS52bi9hdXRoL3JlYWxtcy9ldGMtaW50ZXJuYWwiLCJzdWIiOiI1OGI0MzhhZC0wMGFkLTRjZDktOGUwOS05ODc4MmY5ODVhODEiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoibW9iaWxlLWFwcC1jaHVwdCIsInNlc3Npb25fc3RhdGUiOiI3ZGQ0YWQ2YS02YmVjLTRjZGYtYWJmZi0zNjM5NzY4NzNiYjMiLCJzY29wZSI6InByb2ZpbGUgZW1haWwifQ.Fs66-WzG7UOMuV2k5pv679OlOZ7mbxcCcoEpGZzIydg",
    "token_type": "bearer",
    "session_state": "7dd4ad6a-6bec-4cdf-abff-363976873bb3",
    "scope": "profile email",
    "isETC": 1
}
 */

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auth_info.g.dart';

@JsonSerializable(
  fieldRename: FieldRename.snake,
)
class AuthInfo extends Equatable {
  final String? accessToken;
  final int? expiresIn;
  final String? refreshToken;
  final int? refreshExpiresIn;
  final String? tokenType;
  final String? sessionState;
  final String? scope;
  final bool? isEtc;

  const AuthInfo({
    this.accessToken,
    this.expiresIn,
    this.refreshToken,
    this.refreshExpiresIn,
    this.tokenType,
    this.sessionState,
    this.scope,
    this.isEtc,
  });

  factory AuthInfo.fromJson(Map<String, dynamic> json) =>
      _$AuthInfoFromJson(json);

  @override
  List<Object?> get props => [accessToken, refreshToken, sessionState, scope];
}

@JsonSerializable(fieldRename: FieldRename.snake)
class AuthMessage extends Equatable {
  final String? error;
  final String? errorDescription;

  const AuthMessage({
    this.error,
    this.errorDescription,
  });

  factory AuthMessage.fromJson(Map<String, dynamic> json) =>
      _$AuthMessageFromJson(json);

  @override
  List<Object?> get props => [error, errorDescription];
}
