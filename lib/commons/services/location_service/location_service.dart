import 'package:location/location.dart';

class LocationService {
  Location get location {
    final location = Location();
    // TODO: convert to inject lazy singleton async
    // await location.enableBackgroundMode(enable: true);
    // await location.changeSettings(interval: 2000, distanceFilter: 0);
    return location;
  }
}
