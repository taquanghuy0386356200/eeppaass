import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/setting/alias/bloc/alias_form_bloc.dart';
import 'package:epass/pages/setting/alias/validator/alias_validator.dart';
import 'package:epass/pages/setting/alias/widget/alias_form.dart';
import 'package:epass/pages/setting/alias/widget/current_alias.dart';
import 'package:epass/pages/setting/alias/widget/default_contract_no.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AliasPage extends StatefulWidget {
  const AliasPage({Key? key}) : super(key: key);

  @override
  State<AliasPage> createState() => _AliasPageState();
}

class _AliasPageState extends State<AliasPage> with AliasValidator {
  bool _firstRun = true;
  bool _showAliasForm = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AliasFormBloc(
        userRepository: getIt<IUserRepository>(),
        appBloc: getIt<AppBloc>(),
      ),
      child: BlocListener<AliasFormBloc, AliasFormState>(
        listener: (context, state) {
          if (state is AliasFormSubmittedSuccess) {
            setState(() => _showAliasForm = false);
            showSuccessSnackBBar(
              context: context,
              message: 'Thay đổi bí danh thành công',
            );
          } else if (state is AliasFormSubmittedFailure) {
            setState(() => _showAliasForm = false);
            showErrorSnackBBar(context: context, message: state.message);
          }
        },
        child: BasePage(
          title: 'Bí danh đăng nhập',
          backgroundColor: Colors.white,
          child: Stack(
            children: [
              FadeAnimation(
                delay: 0.5,
                playAnimation: _firstRun,
                child: const GradientHeaderContainer(),
              ),
              SafeArea(
                child: FadeAnimation(
                  delay: 1,
                  playAnimation: _firstRun,
                  direction: FadeDirection.up,
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: RoundedTopContainer(
                      margin: EdgeInsets.only(top: 16.h),
                      padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                      // Your widget go below
                      child: BlocSelector<AppBloc, AppState, String?>(
                        selector: (state) => state.user?.accountAlias,
                        builder: (context, alias) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const DefaultContractNo(),
                              SizedBox(height: 24.h),
                              Text(
                                'Bạn có thể thêm 01 tên đăng nhập tùy thích để dễ nhớ hơn khi đăng nhập\n'
                                '- Số điện thoại,\n'
                                '- Email\n'
                                '- Bí danh',
                                style: Theme.of(context).textTheme.bodyText1?.copyWith(fontWeight: FontWeight.w600, fontSize: 20.sp),
                              ),
                              Text(
                                '(Tiếng Việt không dấu, không chứa khoảng trắng, không phân biệt hoa thường) ',
                                style: Theme.of(context).textTheme.bodyText1?.copyWith(fontWeight: FontWeight.w600, fontSize: 20.sp,fontStyle:  FontStyle.italic),
                              ),
                              SizedBox(height: 24.h),
                              _showAliasForm
                                  ? AliasForm(
                                      currentAlias: alias,
                                      onCancelTap: () => setState(
                                          () => _showAliasForm = false),
                                    )
                                  : alias != null
                                      ? CurrentAlias(
                                          alias: alias,
                                          onTap: () => setState(
                                              () => _showAliasForm = true),
                                        )
                                      : PrimaryTextField(
                                          labelText: 'Bí danh',
                                          hintText: 'Nhập bí danh',
                                          validator: aliasValidator,
                                          hasClearButton: true,
                                          readonly: true,
                                          onTap: () => setState(
                                              () => _showAliasForm = true),
                                        ),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
