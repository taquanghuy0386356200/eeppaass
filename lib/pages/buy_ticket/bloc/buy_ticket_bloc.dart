import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/buy_ticket/buy_ticket_response.dart';
import 'package:epass/commons/models/buy_ticket/cart.dart';
import 'package:epass/commons/repo/buy_ticket_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'buy_ticket_event.dart';

part 'buy_ticket_state.dart';

class BuyTicketBloc extends Bloc<BuyTicketEvent, BuyTicketState> {
  final IBuyTicketRepository _buyTicketRepository;
  final AppBloc _appBloc;

  BuyTicketBloc({
    required IBuyTicketRepository buyTicketRepository,
    required AppBloc appBloc,
  })  : _buyTicketRepository = buyTicketRepository,
        _appBloc = appBloc,
        super(BuyTicketInitial()) {
    on<BuyTicketConfirmed>(_onBuyTicketConfirmed);
  }

  FutureOr<void> _onBuyTicketConfirmed(
    BuyTicketConfirmed event,
    Emitter<BuyTicketState> emit,
  ) async {
    emit(const BuyTicketConfirmedInProgress());

    final customerId = _appBloc.state.user?.customerId;
    final contractId = _appBloc.state.user?.contractId;

    if (customerId == null || contractId == null) {
      return emit(const BuyTicketConfirmedFailure(
          'Thông tin đăng nhập không hợp lệ (contractNo - client)'));
    }

    final result = await _buyTicketRepository.buyTickets(
      customerId: customerId,
      contractId: contractId,
      cartItems: event.cartItems,
    );

    result.when(
      success: (data) {
        if (data.listFail.isEmpty && data.listSuccess.isNotEmpty) {
          emit(BuyTicketConfirmedAllSuccess(listSuccess: data.listSuccess));
        } else if (data.listFail.isNotEmpty && data.listSuccess.isNotEmpty) {
          emit(BuyTicketConfirmedSomeSuccess(
            listSuccess: data.listSuccess,
            listFailure: data.listFail,
          ));
        } else if (data.listFail.isNotEmpty && data.listSuccess.isEmpty) {
          emit(BuyTicketConfirmedNoSuccess(listFailure: data.listFail));
        } else {
          emit(const BuyTicketConfirmedFailure('Có lỗi xảy ra'));
        }
      },
      failure: (failure) =>
          emit(BuyTicketConfirmedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
