import 'package:epass/commons/widgets/animations/shimmer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeLoadingShimmer extends StatelessWidget {
  const HomeLoadingShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final shimmers = [
      SizedBox(height: 8.h),

      // LOGO AND SEARCH ICON
      ShimmerWidget(
        width: ScreenUtil().screenWidth,
        height: 50.h,
      ),

      // INFO CARD
      SizedBox(height: 20.h),
      ShimmerWidget(
        width: ScreenUtil().screenWidth,
        height: 200.h,
      ),

      // FEATURE GRID
      SizedBox(height: 20.h),

      Padding(
        padding: EdgeInsets.symmetric(horizontal: 8.w),
        child: GridView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: 6,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            crossAxisSpacing: 16.w,
            mainAxisSpacing: 16.h,
            childAspectRatio: 1,
          ),
          itemBuilder: (context, index) {
            return ShimmerWidget(
              width: 32.r,
              height: 32.r,
            );
          },
        ),
      ),

      // BANNER CAROUSEL
      SizedBox(height: 32.h),
      ShimmerWidget(
        width: ScreenUtil().screenWidth,
        height: 120.h,
      ),
    ];

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: ShimmerColor(
          child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (_, index) => shimmers[index],
            itemCount: shimmers.length,
          ),
        ),
      ),
    );
  }
}
