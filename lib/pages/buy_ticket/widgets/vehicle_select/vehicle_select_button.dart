import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/text_field_circular_progress_indicator.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VehicleSelectButton extends StatefulWidget {
  final VoidCallback? onTap;
  final String title;

  const VehicleSelectButton({
    Key? key,
    this.title = 'Xe',
    required this.onTap,
  }) : super(key: key);

  @override
  State<VehicleSelectButton> createState() => _VehicleSelectButtonState();
}

class _VehicleSelectButtonState extends State<VehicleSelectButton> {
  @override
  void initState() {
    context.read<VehicleListBloc>().add(VehicleListFetched());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<VehicleListBloc, VehicleListState>(
      listener: (context, state) {
        if (state.error != null) {
          showErrorSnackBBar(
            context: context,
            message: state.error ?? 'Có lỗi xảy ra',
          );
        } else {
          if (state.listData.length == 1) {
            context
                .read<TicketVehicleBloc>()
                .add(TicketVehicleUserChanged(state.listData));
          }
        }
      },
      builder: (context, state) {
        return Material(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(16.0),
          child: InkWell(
            borderRadius: BorderRadius.circular(16.0),
            onTap: widget.onTap,
            child: Container(
              height: 64.h,
              padding: EdgeInsets.fromLTRB(12.w, 10.h, 20.w, 10.h),
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorName.borderColor,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: BlocSelector<TicketVehicleBloc, TicketVehicleState,
                  List<Vehicle>>(
                selector: (state) => [
                  ...state.userVehicles,
                  ...state.otherVehicles,
                ],
                builder: (context, ticketVehicles) {
                  return Row(
                    children: [
                      Builder(
                        builder: (context) {
                          if (ticketVehicles.isEmpty) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.w),
                              child: Text(
                                widget.title,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                      color: const Color(0xFF818181),
                                    ),
                              ),
                            );
                          } else if (ticketVehicles.length <= 2) {
                            return ListView.separated(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                return _PlateNumberBadge(
                                  plateNumber:
                                      ticketVehicles[index].plateNumber ?? '',
                                );
                              },
                              separatorBuilder: (context, index) =>
                                  SizedBox(width: 10.w),
                              itemCount: ticketVehicles.length,
                            );
                          } else {
                            final twoVehicles = [
                              ticketVehicles[0],
                              ticketVehicles[2]
                            ];
                            return Row(
                              children: [
                                ListView.separated(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    return _PlateNumberBadge(
                                      plateNumber:
                                          twoVehicles[index].plateNumber ?? '',
                                    );
                                  },
                                  separatorBuilder: (context, index) =>
                                      SizedBox(width: 10.w),
                                  itemCount: twoVehicles.length,
                                ),
                                SizedBox(width: 10.w),
                                Text(
                                  '...',
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                SizedBox(width: 10.w),
                                _PlateNumberBadge(
                                  plateNumber: ticketVehicles.length - 2 < 100
                                      ? (ticketVehicles.length - 2).toString()
                                      : '99+',
                                ),
                              ],
                            );
                          }
                        },
                      ),
                      const Spacer(),
                      state.isLoading
                          ? const TextFieldCircularProgressIndicator()
                          : ticketVehicles.isEmpty
                              ? Assets.icons.chevronRight.svg()
                              : Assets.icons.add.svg(),
                    ],
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }
}

class _PlateNumberBadge extends StatelessWidget {
  final String plateNumber;

  const _PlateNumberBadge({
    Key? key,
    required this.plateNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShadowCard(
      borderRadius: 12.0,
      shadowOpacity: 0.2,
      backgroundColor: ColorName.primaryColor,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 8.w,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Center(
          child: Text(
            plateNumber,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
      ),
    );
  }
}
