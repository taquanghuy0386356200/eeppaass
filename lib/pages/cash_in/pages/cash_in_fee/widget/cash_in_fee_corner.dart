import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';

class CashInFeeCorner extends StatelessWidget {
  final TopupChannelCode topupChannelCode;
  final double? height;
  final double? width;

  const CashInFeeCorner({
    Key? key,
    required this.topupChannelCode,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (topupChannelCode) {
      case TopupChannelCode.DomesticATM:
        return Assets.images.cardCorner.blueCorner.svg(
          height: height,
          width: width,
        );
      case TopupChannelCode.InternationalCard:
        return Assets.images.cardCorner.orangeCorner.svg(
          height: height,
          width: width,
        );
      case TopupChannelCode.VNPAY:
        return Assets.images.cardCorner.cyanCorner.svg(
          height: height,
          width: width,
        );
      case TopupChannelCode.MoMo:
        return Assets.images.cardCorner.pinkCorner.svg(
          height: height,
          width: width,
        );
      case TopupChannelCode.ViettelPay:
      case TopupChannelCode.BankPlus:
      default:
        return const SizedBox.shrink();
    }
  }
}
