// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_view_transfer_contract.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailViewTransferContractTotalResponse
    _$DetailViewTransferContractTotalResponseFromJson(
            Map<String, dynamic> json) =>
        DetailViewTransferContractTotalResponse(
          data: json['data'] == null
              ? null
              : DetailViewTransferContractResponse.fromJson(
                  json['data'] as Map<String, dynamic>),
          mess: json['mess'] == null
              ? null
              : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
        );

Map<String, dynamic> _$DetailViewTransferContractTotalResponseToJson(
        DetailViewTransferContractTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

DetailViewTransferContractResponse _$DetailViewTransferContractResponseFromJson(
        Map<String, dynamic> json) =>
    DetailViewTransferContractResponse(
      phoneContact: json['phoneContact'] as String?,
      contractDetailOrginal: json['contractDetailOrginal'] == null
          ? null
          : ContractDetailOrginal.fromJson(
              json['contractDetailOrginal'] as Map<String, dynamic>),
      vehicles: (json['vehicles'] as List<dynamic>?)
          ?.map((e) => VehiclesDetailViewTransferContract.fromJson(
              e as Map<String, dynamic>))
          .toList(),
      contractTransfer: json['contractTransfer'] == null
          ? null
          : ContractDetailDetailViewTransfer.fromJson(
              json['contractTransfer'] as Map<String, dynamic>),
      transferMoney: json['transferMoney'] as bool?,
    );

Map<String, dynamic> _$DetailViewTransferContractResponseToJson(
        DetailViewTransferContractResponse instance) =>
    <String, dynamic>{
      'phoneContact': instance.phoneContact,
      'contractDetailOrginal': instance.contractDetailOrginal,
      'vehicles': instance.vehicles,
      'contractTransfer': instance.contractTransfer,
      'transferMoney': instance.transferMoney,
    };

ContractDetailDetailViewTransfer _$ContractDetailDetailViewTransferFromJson(
        Map<String, dynamic> json) =>
    ContractDetailDetailViewTransfer(
      contractNo: json['contractNo'] as String?,
      signName: json['signName'] as String?,
      contractId: json['contractId'] as String?,
    );

Map<String, dynamic> _$ContractDetailDetailViewTransferToJson(
        ContractDetailDetailViewTransfer instance) =>
    <String, dynamic>{
      'contractNo': instance.contractNo,
      'signName': instance.signName,
      'contractId': instance.contractId,
    };

VehiclesDetailViewTransferContract _$VehiclesDetailViewTransferContractFromJson(
        Map<String, dynamic> json) =>
    VehiclesDetailViewTransferContract(
      plateNumber: json['plateNumber'] as String?,
      plateType: json['plateType'] as int?,
      plateTypeCode: json['plateTypeCode'] as int?,
      contractId: json['contractId'] as int?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      vehicleGroupId: json['vehicleGroupId'] as int?,
      seatNumber: json['seatNumber'] as int?,
      cargoWeight: (json['cargoWeight'] as num?)?.toDouble(),
      chassicNumber: json['chassicNumber'] as String?,
      vehicleColourId: json['vehicleColourId'] as int?,
      vehicleMarkId: json['vehicleMarkId'] as int?,
      owner: json['owner'] as String?,
      vehicleTypeName: json['vehicleTypeName'] as String?,
      activeStatus: json['activeStatus'] as String?,
      rfidSerial: json['rfidSerial'] as String?,
    );

Map<String, dynamic> _$VehiclesDetailViewTransferContractToJson(
        VehiclesDetailViewTransferContract instance) =>
    <String, dynamic>{
      'plateNumber': instance.plateNumber,
      'plateType': instance.plateType,
      'plateTypeCode': instance.plateTypeCode,
      'contractId': instance.contractId,
      'vehicleTypeId': instance.vehicleTypeId,
      'vehicleGroupId': instance.vehicleGroupId,
      'seatNumber': instance.seatNumber,
      'cargoWeight': instance.cargoWeight,
      'chassicNumber': instance.chassicNumber,
      'vehicleColourId': instance.vehicleColourId,
      'vehicleMarkId': instance.vehicleMarkId,
      'owner': instance.owner,
      'activeStatus': instance.activeStatus,
      'rfidSerial': instance.rfidSerial,
      'vehicleTypeName': instance.vehicleTypeName,
    };
