part of 'feedback_form_bloc.dart';

@immutable
class FeedbackFormState extends Equatable {
  final FeedbackType? feedbackType;
  final String? receiveContent;
  final List<String> attachments;

  const FeedbackFormState({
    this.feedbackType,
    this.receiveContent,
    this.attachments = const [],
  });

  @override
  List<Object?> get props => [feedbackType, receiveContent, attachments];

  FeedbackFormState copyWith({
    FeedbackType? feedbackType,
    String? receiveContent,
    List<String>? attachments,
  }) {
    return FeedbackFormState(
      feedbackType: feedbackType ?? this.feedbackType,
      receiveContent: receiveContent ?? this.receiveContent,
      attachments: attachments ?? this.attachments,
    );
  }
}
