part of 'beneficary_details_bloc.dart';

abstract class BeneficiaryDetailsState extends Equatable {
  const BeneficiaryDetailsState();
}

class BeneficiaryDetailsInitial extends BeneficiaryDetailsState {
  @override
  List<Object> get props => [];
}

class BeneficiaryUpdatedInProgress extends BeneficiaryDetailsState {
  const BeneficiaryUpdatedInProgress();

  @override
  List<Object> get props => [];
}

class BeneficiaryUpdatedSuccess extends BeneficiaryDetailsState {
  const BeneficiaryUpdatedSuccess();

  @override
  List<Object> get props => [];
}

class BeneficiaryUpdatedFailure extends BeneficiaryDetailsState {
  final String message;

  const BeneficiaryUpdatedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class BeneficiaryDeleteInProgress extends BeneficiaryDetailsState {
  const BeneficiaryDeleteInProgress();

  @override
  List<Object> get props => [];
}

class BeneficiaryDeleteSuccess extends BeneficiaryDetailsState {
  const BeneficiaryDeleteSuccess();

  @override
  List<Object> get props => [];
}

class BeneficiaryDeleteFailure extends BeneficiaryDetailsState {
  final String message;

  const BeneficiaryDeleteFailure(this.message);

  @override
  List<Object> get props => [message];
}

class BeneficiaryChangedSuccess extends BeneficiaryDetailsState {
  final String? reminiscentName;

  const BeneficiaryChangedSuccess({this.reminiscentName});

  @override
  List<Object?> get props => [reminiscentName];
}
