part of 'buy_ticket_bloc.dart';

abstract class BuyTicketState extends Equatable {
  const BuyTicketState();
}

class BuyTicketInitial extends BuyTicketState {
  @override
  List<Object> get props => [];
}

class BuyTicketConfirmedInProgress extends BuyTicketState {
  const BuyTicketConfirmedInProgress();

  @override
  List<Object> get props => [];
}

class BuyTicketConfirmedFailure extends BuyTicketState {
  final String message;

  const BuyTicketConfirmedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class BuyTicketConfirmedSuccess extends BuyTicketState {
  final List<BuyTicketResponseItem> listSuccess;
  final List<BuyTicketResponseItem> listFailure;

  const BuyTicketConfirmedSuccess({
    required this.listSuccess,
    required this.listFailure,
  });

  @override
  List<Object> get props => [listSuccess, listFailure];
}

class BuyTicketConfirmedAllSuccess extends BuyTicketConfirmedSuccess {
  const BuyTicketConfirmedAllSuccess({
    required List<BuyTicketResponseItem> listSuccess,
  }) : super(
          listSuccess: listSuccess,
          listFailure: const [],
        );

  @override
  List<Object> get props => [listSuccess];
}

class BuyTicketConfirmedSomeSuccess extends BuyTicketConfirmedSuccess {
  const BuyTicketConfirmedSomeSuccess({
    required List<BuyTicketResponseItem> listSuccess,
    required List<BuyTicketResponseItem> listFailure,
  }) : super(listSuccess: listSuccess, listFailure: listFailure);

  @override
  List<Object> get props => [listSuccess, listFailure];
}

class BuyTicketConfirmedNoSuccess extends BuyTicketConfirmedSuccess {
  const BuyTicketConfirmedNoSuccess({
    required List<BuyTicketResponseItem> listFailure,
  }) : super(
          listSuccess: const [],
          listFailure: listFailure,
        );

  @override
  List<Object> get props => [listFailure];
}
