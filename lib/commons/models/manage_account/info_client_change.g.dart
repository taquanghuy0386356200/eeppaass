// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'info_client_change.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InfoClientChangeTotalResponse _$InfoClientChangeTotalResponseFromJson(
        Map<String, dynamic> json) =>
    InfoClientChangeTotalResponse(
      json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
      json['data'] == null
          ? null
          : InfoClientChangeResponse.fromJson(
              json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$InfoClientChangeTotalResponseToJson(
        InfoClientChangeTotalResponse instance) =>
    <String, dynamic>{
      'mess': instance.mess,
      'data': instance.data,
    };

InfoClientChangeResponse _$InfoClientChangeResponseFromJson(
        Map<String, dynamic> json) =>
    InfoClientChangeResponse(
      json['customerOrginal'] == null
          ? null
          : InfoClientResponse.fromJson(
              json['customerOrginal'] as Map<String, dynamic>),
      json['customerChange'] == null
          ? null
          : InfoClientResponse.fromJson(
              json['customerChange'] as Map<String, dynamic>),
    )
      ..ticketId = json['ticketId'] as int?
      ..phoneContact = json['phoneContact'] as String?
      ..ticketProfiles = (json['ticketProfiles'] as List<dynamic>?)
          ?.map((e) => TicketProfiles.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$InfoClientChangeResponseToJson(
        InfoClientChangeResponse instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'phoneContact': instance.phoneContact,
      'customerOrginal': instance.customerOrginal,
      'customerChange': instance.customerChange,
      'ticketProfiles': instance.ticketProfiles,
    };

TicketProfiles _$TicketProfilesFromJson(Map<String, dynamic> json) =>
    TicketProfiles(
      id: json['id'] as int?,
      ticketId: json['ticketId'] as int?,
      ticketPayloadId: json['ticketPayloadId'] as int?,
      profileType: json['profileType'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
    );

Map<String, dynamic> _$TicketProfilesToJson(TicketProfiles instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ticketId': instance.ticketId,
      'ticketPayloadId': instance.ticketPayloadId,
      'profileType': instance.profileType,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
    };
