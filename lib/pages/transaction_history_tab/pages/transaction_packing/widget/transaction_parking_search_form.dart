import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/bloc/transaction_parking_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionParkingSearchForm extends StatefulWidget {
  final String? plateNumber;
  final DateTime? startDate;
  final DateTime? endDate;

  const TransactionParkingSearchForm({
    Key? key,
    this.plateNumber,
    this.startDate,
    this.endDate,
  }) : super(key: key);

  @override
  State<TransactionParkingSearchForm> createState() =>
      _TransactionParkingSearchFormState();
}

class _TransactionParkingSearchFormState
    extends State<TransactionParkingSearchForm> {
  final _formKey = GlobalKey<FormState>();
  final _plateNumberController = TextEditingController();
  final _startDateController = TextEditingController();
  final _endDateController = TextEditingController();

  int _efficiencyId = 1;

  DateTime? _startDate;
  DateTime? _endDate;

  @override
  void initState() {
    super.initState();
    context.read<TransactionParkingBloc>().isSearching = false;
    _plateNumberController.text = widget.plateNumber ?? '';
    _startDate = widget.startDate;
    _endDate = widget.endDate;

    if (_startDate != null) {
      _startDateController.text = Jiffy(_startDate).format('dd/MM/yyyy');
    }

    if (_endDate != null) {
      _endDateController.text = Jiffy(_endDate).format('dd/MM/yyyy');
    }
  }

  @override
  void dispose() {
    _plateNumberController.dispose();
    _startDateController.dispose();
    _endDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      'TÌM KIẾM',
                      style: Theme.of(context).textTheme.subtitle1,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 24.h),
                  PrimaryTextField(
                    controller: _plateNumberController,
                    labelText: 'Biển số xe',
                    hintText: 'Biển số xe (00X1234)',
                    isPlateNumber: true,
                    maxLength: 20,
                  ),
                  SizedBox(height: 28.h),
                  PrimaryTextField(
                    controller: _startDateController,
                    labelText: 'Ngày lấy xe: từ ngày *',
                    hintText: 'Từ ngày',
                    maxLength: 20,
                    suffix: Assets.icons.calendar.svg(),
                    readonly: true,
                    onTap: _showTransactionParkingDateRangePicker,
                    onClear: () => _startDate = null,
                    validator: context
                        .read<TransactionParkingBloc>()
                        .startDateValidator,
                  ),
                  SizedBox(height: 28.h),
                  PrimaryTextField(
                    controller: _endDateController,
                    labelText: 'Ngày lấy xe: đến ngày *',
                    hintText: 'Đến ngày',
                    maxLength: 20,
                    suffix: Assets.icons.calendar.svg(),
                    readonly: true,
                    onTap: _showTransactionParkingDateRangePicker,
                    onClear: () => _endDate = null,
                    validator:
                        context.read<TransactionParkingBloc>().endDateValidator,
                  ),
                  SizedBox(height: 34.h),
                  Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 56.h,
                          child: SecondaryButton(
                            onTap: () {
                              context
                                  .read<TransactionParkingBloc>()
                                  .add(const TransactionParkingFetched());
                              context
                                  .read<TransactionParkingBloc>()
                                  .isSearching = false;
                              Navigator.of(context).pop();
                            },
                            title: 'Đặt lại',
                          ),
                        ),
                      ),
                      SizedBox(width: 16.w),
                      Expanded(
                        child: SizedBox(
                          height: 56.h,
                          child: PrimaryButton(
                            title: 'Tìm kiếm',
                            onTap: () {
                              if (_formKey.currentState!.validate()) {
                                context.read<TransactionParkingBloc>().add(
                                      TransactionParkingSearched(
                                        plateNumber:
                                            _plateNumberController.text,
                                        startDate: _startDate,
                                        endDate: _endDate,
                                      ),
                                    );
                                context
                                    .read<TransactionParkingBloc>()
                                    .isSearching = true;
                                Navigator.of(context).pop();
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  // SizedBox(height: 16.h),
                ],
              ),
            ),
          ),
          Positioned(
            top: 12.h,
            right: 12.w,
            child: SplashIconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: ColorName.borderColor,
              ),
              onTap: () => Navigator.of(context).pop(),
            ),
          ),
        ],
      ),
    );
  }

  void _showTransactionParkingDateRangePicker() async {
    DateTimeRange? initialDateRange;
    if (_startDate != null) {
      initialDateRange = DateTimeRange(
        start: _startDate!,
        end: _endDate ?? Jiffy().dateTime,
      );
    }

    final dateRange = await showDateRangePicker(
          context: context,
          locale: const Locale('vi'),
          initialDateRange: initialDateRange,
          firstDate: Jiffy().subtract(years: 2).startOf(Units.DAY).dateTime,
          lastDate: Jiffy().dateTime,
          initialEntryMode: DatePickerEntryMode.calendarOnly,
        ) ??
        initialDateRange;

    _startDate = dateRange?.start;
    _endDate = dateRange?.end;

    if (_startDate != null) {
      _startDateController.text = Jiffy(_startDate).format('dd/MM/yyyy');
    }

    if (_endDate != null) {
      _endDateController.text = Jiffy(_endDate).format('dd/MM/yyyy');
    }
  }
}
