import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:equatable/equatable.dart';

part 'station_stage_event.dart';
part 'station_stage_state.dart';

class StationStageBloc extends Bloc<StationStageEvent, StationStageState> {
  StationStageBloc() : super(const StationStageState()) {
    on<StationStageChanged>((event, emit) {
      emit(StationStageState(stationStage: event.stationStage));
    });
  }
}
