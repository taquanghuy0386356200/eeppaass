part of 'feedback_type_bloc.dart';

abstract class FeedbackTypeState extends Equatable {
  const FeedbackTypeState();
}

class FeedbackTypeInitial extends FeedbackTypeState {
  @override
  List<Object> get props => [];
}

class FeedbackTypeFetchedInProgress extends FeedbackTypeState {
  const FeedbackTypeFetchedInProgress();

  @override
  List<Object> get props => [];
}

class FeedbackTypeFetchedSuccess extends FeedbackTypeState {
  final List<FeedbackType> feedbackTypes;

  const FeedbackTypeFetchedSuccess({required this.feedbackTypes});

  @override
  List<Object> get props => [feedbackTypes];
}

class FeedbackTypeFetchedFailure extends FeedbackTypeState {
  final String? message;

  const FeedbackTypeFetchedFailure({this.message});

  @override
  List<Object?> get props => [message];
}
