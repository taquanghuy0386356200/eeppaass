//
//  VTPaySDKManager.h
//  ViettelPayLiteSDK
//
//  Created by Eragon on 12/21/18.
//  Copyright © 2018 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, EnvSDK) {
    Staging = 0,
    UAT,
    Product,
};

typedef NS_ENUM(NSInteger, VTPSDKFunctions) {
    Login,
    Full, // Hiện màn Home đủ 4 chức năng
    Recharge, // Nạp tiền
    QRCodePay, // Thanh Toán QR
    ChangePassword, // Đổi mật khẩu VTP
    History, // Lịch sử giao dịch
    MuaVe, // Mua vé
    GiaHan, // Gia hạn vé
    HuyVe, // Hủy vé
    LienKet, // Liên kết
    HuyLienKet, // Hủy Liên kết
};

NS_ASSUME_NONNULL_BEGIN

@interface VTPaySDKManager: NSObject

//@property (assign, nonatomic) BOOL isSandbox;

// Version 1.3
+ (instancetype)sharedInstance;

@property (assign, nonatomic) EnvSDK env;

- (void)initVTPSDKWithFunctionETC:(VTPSDKFunctions)function phoneNumber:(NSString *)phoneNumber withToken: (NSString *)token dicTicket:(NSMutableDictionary *)dic navigationController:(UINavigationController *)navigationController onError:(void (^)(id * _Nonnull))onError;

//** Khởi tạo các chức năng ViettelPaySDK theo enum ở trên, đã check đủ sdt đăng kí hay chưa, tự động chuyển vào đăng ký, đăng nhập **//
- (void)initVTPSDKWithFunction:(VTPSDKFunctions)function phoneNumber:(NSString *)phoneNumber navigationController:(UINavigationController *)navigationController onError:(void (^_Nullable)(NSString *message))onError;

//** UNREGISTERED   - chưa đăng ký **//
//** ACTIVATED  - đã đăng ký và đã kích hoạt **//
//** UNACTIVATED - đã đăng ký nhưng chưa ra quầy để hoàn thiện hồ sơ **//
- (void)checkAccountStatus:(NSString *)phoneNumber onSuccess:(void (^_Nullable)(NSString *responseCode))onSuccess onError:(void (^_Nullable)(id *message))onError;

//** Lấy số dư khi đã đăng nhập, cần check login hay chưa(gọi hàm isLogged ở dưới) để ẩn hiện UI phần số dư **//
- (void)getBalance:(void (^_Nullable)(NSString *balance))onSuccess onError:(void (^_Nullable)(NSString *message))onError;

- (void)logOut;

- (BOOL)isLogged;

@end

NS_ASSUME_NONNULL_END
