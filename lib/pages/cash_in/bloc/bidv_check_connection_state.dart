part of 'bidv_check_connection_bloc.dart';

abstract class BidvCheckingConnectionState extends CashInState {
  const BidvCheckingConnectionState();
}

class BidvCheckingConnectionInitial extends BidvCheckingConnectionState {
  @override
  List<Object> get props => [];
}

class BidvCheckingConnectionInProgress extends BidvCheckingConnectionState {
  @override
  List<Object> get props => [];
}

class BidvCheckingConnectionSuccess extends BidvCheckingConnectionState {
  final String serviceId;
  final String merchantId;
  final String tranDate;
  final String transId;
  final String responseCode;
  final String moreInfo;
  final String secureCode;

  const BidvCheckingConnectionSuccess({
    required this.serviceId,
    required this.merchantId,
    required this.tranDate,
    required this.transId,
    required this.responseCode,
    required this.moreInfo,
    required this.secureCode,
  });

  @override
  List<Object> get props => [
        serviceId,
        merchantId,
        tranDate,
        transId,
        responseCode,
        moreInfo,
        secureCode
      ];
}

class BidvCheckingConnectionFailure extends BidvCheckingConnectionState {
  final String message;

  const BidvCheckingConnectionFailure(this.message);

  @override
  List<Object> get props => [message];
}
