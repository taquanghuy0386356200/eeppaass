import 'dart:convert';

import 'package:epass/commons/services/zalopay/zalo_pay_constant.dart';
import 'package:epass/commons/services/zalopay/zalopay_create_order_request.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import '../../models/zalo_pay/create_order_response.dart';
import '../../utils/zalo_pay/zalo_pay_utils.dart';
import 'package:sprintf/sprintf.dart';

class ZaloPayCreateOrderService {
  Future<CreateOrderResponse?> createOrder(
    ZaloPayCreateOrderRequest request,
  ) async {
    try {
      var header = <String, String>{};
      header["Content-Type"] = "application/x-www-form-urlencoded";

var body = <String, String>{};
      body["app_id"] = ZaloPayConstant.appId;
      body["app_user"] = ZaloPayConstant.appUser;
      body["app_time"] = DateTime.now().millisecondsSinceEpoch.toString();
      body["amount"] = request.amount.toStringAsFixed(0);
      body["app_trans_id"] = getAppTransId();
      body["embed_data"] = "{}";
      body["item"] = "[]";
      body["bank_code"] = getBankCode();
      body["description"] = getDescription(body["app_trans_id"]!);

      var dataGetMac = sprintf("%s|%s|%s|%s|%s|%s|%s", [
        body["app_id"],
        body["app_trans_id"],
        body["app_user"],
        body["amount"],
        body["app_time"],
        body["embed_data"],
        body["item"]
      ]);
      body["mac"] = getMacCreateOrder(dataGetMac);

      http.Response response = await http.post(
        Uri.parse(ZaloPayConstant.urlCreateOrder),
        headers: header,
        body: body,
      );
      if (response.statusCode != 200) {
        return null;
      }

      var data = jsonDecode(response.body);

      return CreateOrderResponse.fromJson(data);
    } on PlatformException {
      return null;
    }
  }
}
