/// billCycle : 1
/// billCycleMergeType : 3
/// actTypeId : 14
/// reasonId : 26

class UpdateInvoiceCycleRequest {
  UpdateInvoiceCycleRequest({
    required this.billCycle,
    this.billCycleMergeType,
    this.actTypeId = 14,
    this.reasonId = 26,
  });

  final int billCycle;
  final int? billCycleMergeType;
  final int actTypeId;
  final int reasonId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['billCycle'] = billCycle;

    if (billCycleMergeType != null) {
      map['billCycleMergeType'] = billCycleMergeType;
    }

    map['actTypeId'] = actTypeId;
    map['reasonId'] = reasonId;
    return map;
  }
}
