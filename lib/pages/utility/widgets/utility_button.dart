import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../track/firebase_track.dart';

class UtilityButton extends StatelessWidget {
  final String title;
  final Widget icon;
  final void Function()? onTap;

  const UtilityButton({
    Key? key,
    required this.title,
    required this.icon,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.r),
      child: ShadowCard(
        shadowOpacity: 0.2,
        child: Material(
          borderRadius: BorderRadius.circular(16.0),
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(16.0),
            onTap: (){
              onTap?.call();
              FirebaseTrack.trackClick(title);
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(4.w, 16.h, 4.w, 12.h),
              child: Column(
                children: [
                  Expanded(child: icon),
                  SizedBox(height: 12.h),
                  Expanded(
                    child: Text(
                      title,
                      style: Theme.of(context).textTheme.bodyText2,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
