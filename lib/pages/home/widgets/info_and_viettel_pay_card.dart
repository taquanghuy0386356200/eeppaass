import 'package:epass/pages/home/widgets/info_card.dart';
import 'package:epass/pages/home/widgets/viettel_money_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InfoAndViettelPayCards extends StatefulWidget {
  const InfoAndViettelPayCards({
    Key? key,
  }) : super(key: key);

  @override
  State<InfoAndViettelPayCards> createState() => _InfoAndViettelPayCardsState();
}

class _InfoAndViettelPayCardsState extends State<InfoAndViettelPayCards> {
  var infoCardShadow = 0.3;
  var viettelPayCardShadow = 0.1;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 210.h,
      child: NotificationListener<ScrollUpdateNotification>(
        onNotification: (notification) {
          var pixels = notification.metrics.pixels;
          var maxScrollExtent = notification.metrics.maxScrollExtent;
          setState(() {
            final shadowValue1 = 0.2 * (-1 * pixels / maxScrollExtent + 1.5);
            if (shadowValue1 < 0) {
              infoCardShadow = 0.1;
            } else if (shadowValue1 > 1) {
              infoCardShadow = 0.3;
            } else {
              infoCardShadow = shadowValue1;
            }
            final shadowValue2 = 0.2 * (pixels / maxScrollExtent + 0.5);
            if (shadowValue2 < 0) {
              viettelPayCardShadow = 0.1;
            } else if (shadowValue2 > 1) {
              viettelPayCardShadow = 0.3;
            } else {
              viettelPayCardShadow = shadowValue2;
            }
          });
          return true;
        },
        child: ListView.separated(
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            if (index == 0) {
              return Container(
                width: 340.w,
                padding: EdgeInsets.fromLTRB(
                  index == 0 ? 16.w : 0,
                  4.h,
                  index == 1 ? 16.w : 0,
                  16.h,
                ),
                child: InfoCard(
                  shadowOpacity: infoCardShadow,
                ),
              );
            } else {
              return Container(
                width: 340.w,
                padding: EdgeInsets.fromLTRB(
                  index == 0 ? 16.w : 0,
                  4.h,
                  index == 1 ? 16.w : 0,
                  16.h,
                ),
                child: ViettelMoneyCard(
                  shadowOpacity: viettelPayCardShadow,
                ),
              );
            }
          },
          separatorBuilder: (context, index) => SizedBox(width: 16.w),
          itemCount: 2,
        ),
      ),
    );
  }
}
