import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// custInvoiceId : 66121
/// custId : 1000966
/// contractId : 1001022
/// invoiceNo : "AD/20E0000079"
/// taxRateString : "10%"
/// invoiceIssuedDate : "30/12/2021 18:57:09"
/// eventTimeStamp : "30/12/2021 18:55:43"
/// invoiceSeries : "AD/20E"
/// transactionTypeName : "Giao dịch thẻ"
/// templateCode : "01GTKT0/001"
/// currencyCode : "VND"
/// transType : 3
/// adjustmentType : 1
/// adjustmentTypeName : "Hóa đơn gốc"
/// invoiceStatus : 2
/// invoiceStatusName : "Đã phát hành"
/// custName : "Giang0204"
/// noticeArea : "12 - An Phú - An Phú - An Giang"
/// totAmountTax : 109090.91
/// taxRate : 10
/// totAmount : 120000.0
/// taxAmount : 10909.09
/// staDate : 1640866223000
/// endDate : 1640866223000
/// contractNo : "V001001022"
/// itemName : "Thẻ E-tag dán kính"
/// transTypeName : "Tháng"
/// strInvoiceIssuedDate : "30/12/2021 18:57:09"
/// unitInvoice : "Lượt"
/// countInvoice : "1"
/// itemTaxAmount : 10909.09
/// itemTotAmountTax : 109090.91
/// itemTotAmount : 120000.0

part 'invoice.g.dart';

@JsonSerializable()
class Invoice {
  Invoice({
    this.custInvoiceId,
    this.custId,
    this.contractId,
    this.invoiceNo,
    this.taxRateString,
    this.invoiceIssuedDate,
    this.eventTimeStamp,
    this.invoiceSeries,
    this.transactionTypeName,
    this.templateCode,
    this.currencyCode,
    this.transType,
    this.adjustmentType,
    this.adjustmentTypeName,
    this.invoiceStatus,
    this.invoiceStatusName,
    this.custName,
    this.noticeArea,
    this.totAmountTax,
    this.taxRate,
    this.totAmount,
    this.taxAmount,
    this.staDate,
    this.endDate,
    this.contractNo,
    this.itemName,
    this.transTypeName,
    this.strInvoiceIssuedDate,
    this.unitInvoice,
    this.countInvoice,
    this.itemTaxAmount,
    this.itemTotAmountTax,
    this.itemTotAmount,
  });

  final int? custInvoiceId;
  final int? custId;
  final int? contractId;
  final String? invoiceNo;
  final String? taxRateString;
  final String? invoiceIssuedDate;
  final String? eventTimeStamp;
  final String? invoiceSeries;
  final String? transactionTypeName;
  final String? templateCode;
  final String? currencyCode;
  final int? transType;
  final int? adjustmentType;
  final String? adjustmentTypeName;
  final int? invoiceStatus;
  final String? invoiceStatusName;
  final String? custName;
  final String? noticeArea;
  final double? totAmountTax;
  final int? taxRate;
  final double? totAmount;
  final double? taxAmount;
  final int? staDate;
  final int? endDate;
  final String? contractNo;
  final String? itemName;
  final String? transTypeName;
  final String? strInvoiceIssuedDate;
  final String? unitInvoice;
  final String? countInvoice;
  final double? itemTaxAmount;
  final double? itemTotAmountTax;
  final double? itemTotAmount;

  @override
  String toString() {
    return 'invoice $custInvoiceId';
  }

  factory Invoice.fromJson(Map<String, dynamic> json) =>
      _$InvoiceFromJson(json);
}

@JsonSerializable()
class InvoiceData {
  final List<Invoice>? listData;
  final int? count;

  InvoiceData({
    this.listData,
    this.count,
  });

  factory InvoiceData.fromJson(Map<String, dynamic> json) =>
      _$InvoiceDataFromJson(json);
}

class InvoiceDataNotNull {
  final List<Invoice> listData;
  final int count;

  InvoiceDataNotNull({required this.listData, required this.count});
}

@JsonSerializable()
class InvoiceResponse {
  final InvoiceData? data;
  final MessageData? mess;

  InvoiceResponse({this.data, this.mess});

  factory InvoiceResponse.fromJson(Map<String, dynamic> json) =>
      _$InvoiceResponseFromJson(json);
}
