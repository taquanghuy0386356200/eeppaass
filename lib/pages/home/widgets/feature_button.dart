import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FeatureButton extends StatelessWidget {
  final Widget icon;
  final String? title;
  final void Function()? onTap;

  const FeatureButton({
    Key? key,
    required this.icon,
    this.title,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.circular(10.0),
        highlightColor: Colors.grey.withOpacity(0.1),
        splashColor: Colors.grey.withOpacity(0.1),
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 8.h,
            horizontal: 4.w,
          ),
          child: Column(
            children: [
              Container(
                height: 56.r,
                width: 56.r,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      ColorName.homeSecondaryButtonGradientStart,
                      ColorName.homeSecondaryButtonGradientEnd,
                    ],
                  ),
                ),
                child: Center(
                  child: icon,
                ),
              ),
              if (title != null) SizedBox(height: 8.h),
              if (title != null)
                Text(
                  title!,
                  style: Theme.of(context).textTheme.bodyText2,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
