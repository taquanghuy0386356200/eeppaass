import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/pages/bloc/bloc_transforms.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'vehicle_other_event.dart';

part 'vehicle_other_state.dart';

class VehicleOtherBloc extends Bloc<VehicleOtherEvent, VehicleOtherState> {
  final IVehicleRepository _vehicleRepository;

  final List<int> _statuses;
  final List<int> _activeStatuses;

  VehicleOtherBloc({
    required IVehicleRepository vehicleRepository,
    List<int> statuses = const [1],
    List<int> activeStatuses = const [1, 4],
  })  : _statuses = statuses,
        _activeStatuses = activeStatuses,
        _vehicleRepository = vehicleRepository,
        super(VehicleOtherInitial()) {
    on<VehicleOtherSearched>(
      _onVehicleOtherSearched,
      transformer: throttleDroppable(const Duration(milliseconds: 200)),
    );
  }

  FutureOr<void> _onVehicleOtherSearched(
    VehicleOtherSearched event,
    Emitter<VehicleOtherState> emit,
  ) async {
    emit(const VehicleOtherSearchedInProgress());

    final result = await _vehicleRepository.getUserVehicles(
      statuses: _statuses,
      activeStatuses: _activeStatuses,
      inContract: false,
      plateNumber: event.plateNumber.trim(),
    );

    result.when(
      success: (data) => emit(VehicleOtherSearchedSuccess(data.listData)),
      failure: (failure) =>
          emit(VehicleOtherSearchedFailure(failure.message ?? '')),
    );
  }
}
