import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FeaturePage extends StatefulWidget {
  const FeaturePage({Key? key}) : super(key: key);

  @override
  State<FeaturePage> createState() => _FeaturePageState();
}

class _FeaturePageState extends State<FeaturePage> {
  bool _firstRun = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: 'Page Title',
      child: Stack(
        children: [
          FadeAnimation(
            delay: 0.5,
            playAnimation: _firstRun,
            child: const GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              playAnimation: _firstRun,
              direction: FadeDirection.up,
              child: RoundedTopContainer(
                margin: EdgeInsets.only(top: 16.h),
                padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                // Your widget go below
                child: const Center(
                  child: Text('Your Widget'),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
