/// text : "Hệ thống tổng hợp tiếng nói trung tâm không gian mạng"
/// voice : "hn-quynhanh"
/// id : "2"
/// without_filter : false
/// speed : 1.0
/// tts_return_option : 2

class TextToSpeechRequest {
  TextToSpeechRequest({
    required this.text,
    this.voice = 'hn-quynhanh',
    this.id = '2',
    this.withoutFilter = false,
    this.speed = 1.0,
    this.ttsReturnOption = 2,
  });

  final String text;
  final String? voice;
  final String? id;
  final bool? withoutFilter;
  final double? speed;
  final int? ttsReturnOption;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['text'] = text;
    map['voice'] = voice;
    map['id'] = id;
    map['without_filter'] = withoutFilter;
    map['speed'] = speed;
    map['tts_return_option'] = ttsReturnOption;
    return map;
  }
}
