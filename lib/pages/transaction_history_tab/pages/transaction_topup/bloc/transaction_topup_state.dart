part of 'transaction_topup_bloc.dart';

@immutable
class TransactionTopupState extends Equatable {
  final List<ContractTopup> listData;
  final bool isFull;
  final DateTime? topupDateFrom;
  final DateTime? topupDateTo;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const TransactionTopupState({
    required this.listData,
    this.isFull = false,
    this.topupDateFrom,
    this.topupDateTo,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        topupDateFrom,
        topupDateTo,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  TransactionTopupState copyWith({
    List<ContractTopup>? listData,
    bool? isFull,
    required DateTime? topupDateFrom,
    required DateTime? topupDateTo,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return TransactionTopupState(
      listData: listData ?? this.listData,
      isFull: isFull ?? this.isFull,
      topupDateFrom: topupDateFrom,
      topupDateTo: topupDateTo,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
