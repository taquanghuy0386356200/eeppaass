import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'balance_event.dart';

part 'balance_state.dart';

class BalanceBloc extends Bloc<BalanceEvent, BalanceState> {
  final IUserRepository _userRepository;
  final AppBloc _appBloc;

  BalanceBloc({
    required IUserRepository userRepository,
    required AppBloc appBloc,
  })  : _userRepository = userRepository,
        _appBloc = appBloc,
        super(const BalanceState()) {
    on<BalanceToggled>(_onBalanceToggled);
    on<BalanceFetched>(_onBalanceFetched);

    on<BalanceViettelMoneyToggled>(_onBalanceViettelMoneyToggled);
  }

  FutureOr<void> _onBalanceToggled(BalanceToggled event, Emitter<BalanceState> emit) async {
    if (state.isHidden == true) {
      await _balanceFetched(emit);
    } else {
      emit(state.copyWith(
        isHidden: true,
        balance: null,
        error: null,
      ));
    }
  }

  FutureOr<void> _onBalanceFetched(
    BalanceFetched event,
    Emitter<BalanceState> emit,
  ) async {
    await _balanceFetched(emit);
  }

  Future<void> _balanceFetched(Emitter<BalanceState> emit) async {
    final contractId = _appBloc.state.user?.contractId;
    final customerId = _appBloc.state.user?.customerId;

    if (contractId != null && customerId != null) {
      emit(state.copyWith(
        isLoading: true,
        isHidden: false,
        error: null,
      ));

      final response = await _userRepository.getBalance(customerId, contractId);

      response.when(
        success: (balance) => emit(state.copyWith(
          balance: balance,
          isLoading: false,
          isHidden: false,
          error: null,
        )),
        failure: (failure) => emit(state.copyWith(
          balance: null,
          isLoading: false,
          isHidden: true,
          error: failure.message ?? '',
        )),
      );
    }
  }

  FutureOr<void> _onBalanceViettelMoneyToggled(
    BalanceViettelMoneyToggled event,
    Emitter<BalanceState> emit,
  ) async {
    if (state.isHidden == true) {
      await _balanceViettelMoneyFetched(emit);
    } else {
      emit(state.copyWith(
        isHidden: true,
        balance: null,
        error: null,
      ));
    }
  }

  Future<void> _balanceViettelMoneyFetched(Emitter<BalanceState> emit) async {
    emit(state.copyWith(
      isLoading: true,
      isHidden: false,
      error: null,
    ));

    final response = await _userRepository.getViettelMoneyBalance();

    response.when(
      success: (balance) => emit(state.copyWith(
        balance: balance,
        isLoading: false,
        isHidden: false,
        error: null,
      )),
      failure: (failure) => emit(state.copyWith(
        balance: null,
        isLoading: false,
        isHidden: true,
        error: failure.message ?? '',
      )),
    );
  }
}
