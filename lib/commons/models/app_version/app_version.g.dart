// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_version.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppVersion _$AppVersionFromJson(Map<String, dynamic> json) => AppVersion(
      appCode: json['appCode'] as String?,
      versionName: json['versionName'] as String?,
      deviceType: json['deviceType'] as String?,
      version: json['version'] as int?,
      invalid: json['invalid'] as int?,
    );

Map<String, dynamic> _$AppVersionToJson(AppVersion instance) =>
    <String, dynamic>{
      'appCode': instance.appCode,
      'versionName': instance.versionName,
      'deviceType': instance.deviceType,
      'version': instance.version,
      'invalid': instance.invalid,
    };

AppVersionResponse _$AppVersionResponseFromJson(Map<String, dynamic> json) =>
    AppVersionResponse(
      data: json['data'] == null
          ? null
          : AppVersion.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AppVersionResponseToJson(AppVersionResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
