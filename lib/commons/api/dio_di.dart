import 'package:dio/dio.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/interceptors/auth_interceptor.dart';
import 'package:epass/commons/api/interceptors/json_interceptor.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
// import 'package:flutter_loggy_dio/flutter_loggy_dio.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

abstract class DioDi {
  Dio get dio {
    final dio = Dio();
    dio.options.connectTimeout = const Duration(seconds: 30).inMilliseconds;
    dio.options.receiveTimeout = const Duration(seconds: 30).inMilliseconds;

    dio.interceptors.add(JsonResponseConverter());
    dio.interceptors.add(ErrorInterceptor());
    dio.interceptors.add(AuthInterceptor(bloc: getIt<AppBloc>()));
    // dio.interceptors.add(LoggyDioInterceptor(
    //   responseBody: true,
    //   requestBody: true,
    // ));
    if(foundation.kDebugMode) {
      dio.interceptors.add(dioLogger());
    }


    return dio;
  }

  PrettyDioLogger dioLogger() {
    return PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      maxWidth: 100,
    );
  }

}
