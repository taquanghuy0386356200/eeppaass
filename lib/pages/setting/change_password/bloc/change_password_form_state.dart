part of 'change_password_form_bloc.dart';

abstract class ChangePasswordFormState extends Equatable {
  const ChangePasswordFormState();
}

class ChangePasswordFormInitial extends ChangePasswordFormState {
  @override
  List<Object> get props => [];
}

class ChangePasswordFormSubmittedInProgress extends ChangePasswordFormState {
  const ChangePasswordFormSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class ChangePasswordFormSubmittedSuccess extends ChangePasswordFormState {
  const ChangePasswordFormSubmittedSuccess();

  @override
  List<Object> get props => [];
}

class ChangePasswordFormSubmittedFailure extends ChangePasswordFormState {
  final String message;

  const ChangePasswordFormSubmittedFailure(this.message);

  @override
  List<Object> get props => [];
}
