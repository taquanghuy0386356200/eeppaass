import 'package:epass/commons/widgets/animations/shimmer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SMSVehicleSettingShimmerLoading extends StatelessWidget {
  const SMSVehicleSettingShimmerLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShimmerColor(
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) => ShimmerWidget(
          height: index == 0 ? 40.h : 24.h,
          width: double.infinity,
        ),
        separatorBuilder: (context, index) => SizedBox(height: index == 0 ? 24.h : 10.h),
        itemCount: 4,
      ),
    );
  }
}
