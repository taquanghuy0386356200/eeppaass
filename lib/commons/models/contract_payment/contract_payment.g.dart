// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractPayment _$ContractPaymentFromJson(Map<String, dynamic> json) =>
    ContractPayment(
      contractPaymentId: json['contractPaymentId'] as int?,
      custId: json['custId'] as int?,
      contractId: json['contractId'] as int?,
      methodRechargeId: json['methodRechargeId'] as int?,
      accountNumber: json['accountNumber'] as String?,
      accountOwner: json['accountOwner'] as String?,
      accountBankId: json['accountBankId'] as int?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
      description: json['description'] as String?,
      status: json['status'] as String?,
      token: json['token'] as String?,
      orderId: json['orderId'] as String?,
      methodRechargeCode: json['methodRechargeCode'] as String?,
      topupAuto: json['topupAuto'] as int?,
      topupAmount: json['topupAmount'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      documentTypeCode: json['documentTypeCode'] as String?,
      documentNo: json['documentNo'] as String?,
      channel: json['channel'] as int?,
      linkPhone: json['linkPhone'] as String?,
    );

Map<String, dynamic> _$ContractPaymentToJson(ContractPayment instance) =>
    <String, dynamic>{
      'contractPaymentId': instance.contractPaymentId,
      'custId': instance.custId,
      'contractId': instance.contractId,
      'methodRechargeId': instance.methodRechargeId,
      'accountNumber': instance.accountNumber,
      'accountOwner': instance.accountOwner,
      'accountBankId': instance.accountBankId,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'description': instance.description,
      'status': instance.status,
      'token': instance.token,
      'orderId': instance.orderId,
      'methodRechargeCode': instance.methodRechargeCode,
      'topupAuto': instance.topupAuto,
      'topupAmount': instance.topupAmount,
      'documentTypeId': instance.documentTypeId,
      'documentTypeCode': instance.documentTypeCode,
      'documentNo': instance.documentNo,
      'channel': instance.channel,
      'linkPhone': instance.linkPhone,
    };

ContractPaymentResponse _$ContractPaymentResponseFromJson(
        Map<String, dynamic> json) =>
    ContractPaymentResponse(
      contractPayment: json['data'] == null
          ? null
          : ContractPayment.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContractPaymentResponseToJson(
        ContractPaymentResponse instance) =>
    <String, dynamic>{
      'data': instance.contractPayment,
      'mess': instance.mess,
    };
