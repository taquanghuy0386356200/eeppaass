package io.flutter.plugins;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.bplus.vtpay.fragment.etc.model.object.Vehicle;
import com.example.epass.MainActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import vn.viettelpay.sdk.ViettelPaySDK;
import vn.viettelpay.sdk.etc.model.object.DataTicket;
import vn.viettelpay.sdk.etc.model.object.Ticket;
import vn.viettelpay.sdk.utils.Toolbox;

public class VtMoneyUtils {
    static {
        System.loadLibrary("native-lib");
    }

    public void linkEtc(@NotNull Activity activity, @Nullable HashMap<String, Object> arguments) {
        ArrayList<Vehicle> vehicleList = (ArrayList<Vehicle>) arguments.get("vehicles");
        String contractId = arguments.get("contractId").toString();
        DataTicket dataTicket = new DataTicket(genOderId(), //orderId
                contractId, //contractId
                arguments.get("msisdn").toString(), // msisdn - phone
                arguments.get("contractIdNo").toString(), //contractIdNo = cmt
                arguments.get("accountNo").toString(), //accountNo - username
                arguments.get("contractNo").toString(), //contractNo --- require
                arguments.get("contractFullName").toString(), //contractFullName - ten nguoi dung --- require
                arguments.get("contractObjetType").toString(), //contractObjetType --- require
                arguments.get("contractIdType").toString(), //contractIdType --- IC/CC/PP
                vehicleList);
        ViettelPaySDK.getInstance().linkETC(activity, contractId, dataTicket);
    }

    private String genOderId() {
        String orderId = "";
        Date date = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat") DateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        orderId = simpleDateFormat.format(date);
        orderId += new Random().nextInt(999999);
        return orderId;
    }

    public void unLinkETC(@NotNull MainActivity activity, @Nullable HashMap<String, Object> arguments) {
        String token = arguments.get("token").toString();
        String contractId = arguments.get("contractId").toString();
        String phone = arguments.get("accountNumber").toString();
        String etcOrderId = Toolbox.getOrderId();

        ArrayList<Ticket> arrTicket = new ArrayList<>();
        DataTicket dataTicket = new DataTicket(etcOrderId, contractId,
                Toolbox.formatMobileHead84(phone), token, "nội dung giao dịch", "thông tin khác", "",
                "Miễn phí", "200000", "21724721741924", "", "", arrTicket);
        ViettelPaySDK.getInstance().inputPinToCancelLinkETC(activity, phone, contractId, token, dataTicket);
    }
}
