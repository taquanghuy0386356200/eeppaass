import 'package:epass/commons/enum/boo_code.dart';
import 'package:epass/commons/enum/method_charge.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// code : "960"
/// name : "Cầu Phú Mỹ"
/// id : 960
/// name_short : "Cầu Phú Mỹ"
/// station_type : "1"
/// is_active : "1"
/// address_detail : "Thạch Mỹ Lợi, Quận 2, Tp. Hồ Chí Minh, Thành Phố Hồ Chí Minh"
/// method_charge_id : "1"
/// province : "A076"
/// longitude : 106.764903
/// latitude : 10.76006
/// booCode : "BOO1"
/// isEtc : 1
/// priceImagePath : "station/960.png"
/// priceLinkWeb : "https://epass-vdtc.com.vn/bieu-muc-thu-phi-tai-tram-bot/bieu-muc-thu-phi-su-dung-duong-bo-tai-tram-thu-phi-do-vetc-van-hanh/#tram-thu-phi-cau-phu-my"

part 'station.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Station {
  const Station({
    this.code,
    this.name,
    this.id,
    this.nameShort,
    this.stationType,
    this.isActive,
    this.addressDetail,
    this.methodChargeId,
    this.province,
    this.longitude,
    this.latitude,
    this.booCode,
    this.isEtc,
    this.priceImagePath,
    this.priceLinkWeb,
  });

  final String? code;
  final String? name;
  final int? id;
  final String? nameShort;
  final String? stationType;
  final String? isActive;
  final String? addressDetail;

  @JsonKey(unknownEnumValue: MethodChargeString.unknown)
  final MethodChargeString? methodChargeId;

  final String? province;
  final double? longitude;
  final double? latitude;

  @JsonKey(unknownEnumValue: BooCode.unknown)
  final BooCode? booCode;

  final int? isEtc;
  final String? priceImagePath;
  final String? priceLinkWeb;

  factory Station.fromJson(Map<String, dynamic> json) =>
      _$StationFromJson(json);
}

@JsonSerializable()
class StationData {
  final List<Station>? listData;
  final int? count;

  StationData({this.listData, this.count});

  factory StationData.fromJson(Map<String, dynamic> json) =>
      _$StationDataFromJson(json);
}

class StationDataNotNull {
  final List<Station> listData;
  final int count;

  StationDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class StationResponse {
  final StationData? data;
  final MessageData? mess;

  StationResponse({this.data, this.mess});

  factory StationResponse.fromJson(Map<String, dynamic> json) =>
      _$StationResponseFromJson(json);
}
