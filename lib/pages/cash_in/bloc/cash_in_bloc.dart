import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/models/topup_channel/topup_channel_fee.dart';
import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:epass/commons/services/momo/momo_payment.dart';
import 'package:epass/commons/services/momo/momo_payment_request.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment.dart';
import 'package:epass/flavors.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'cash_in_event.dart';
part 'cash_in_state.dart';

class CashInBloc extends Bloc<CashInEvent, CashInState> {
  final ICashInRepository _cashInRepository;
  final MomoPayment _momoService;

  CashInBloc({
    required ICashInRepository cashInRepository,
    required MomoPayment momoService,
    required VNPayService vnpayService,
  })  : _cashInRepository = cashInRepository,
        _momoService = momoService,
        super(CashInInitial()) {
    on<CashInFetchedTopupFee>(_onCashInFetchedTopupFee);
    on<CashInOrderMomoCreated>(_onCashInMomoCreated);
    on<CashInMomoPayment>(_onCashInMomoPayment);
    on<ConfirmMomoOrder>(_onConfirmMomoPayment);
    on<CashInOrderViettelPayCreated>(_onCashInViettelPayCreated);
    on<CashInOrderVNPayCreated>(_onCashInVNPayCreated);
    on<CashInContractsPayment>(_onCashInEpassPayment);
    on<BidvCreateOrder>(_onBidvCreateOrder);
    on<BidvCheckOTP>(_onCheckOtpBidv);
    on<CheckCustomerType>(_onCheckCustType);
    on<CheckVehicleInContract>(_onCheckVehicleInContract);
  }

  FutureOr<void> _onCashInBIDVConnected(
    CashInOrderBIDVConnected event,
    Emitter<CashInState> emit,
  ) async {
    emit(CashInOrderCreatedInProgress());

    emit(
      CashInOrderBIDVConnnectedSuccess(
        amount: event.amount,
        contractNo: event.contractNo,
      ),
    );
  }

  FutureOr<void> _onCashInFetchedTopupFee(
    CashInFetchedTopupFee event,
    Emitter<CashInState> emit,
  ) async {
    emit(CashInFetchedTopupFeeInProgress());

    final result = await _cashInRepository.getTopupChannelFee(
      topupChannelCode: event.topupChannelCode.name,
      amount: event.value,
    );

    result.when(
      success: (fee) => emit(
        CashInFetchedTopupFeeSuccess(fee),
      ),
      failure: (failure) => emit(
        CashInFetchedTopupFeeFailure(failure.message ?? ''),
      ),
    );
  }

  FutureOr<void> _onCashInMomoCreated(
    CashInOrderMomoCreated event,
    Emitter<CashInState> emit,
  ) async {
    emit(CashInOrderCreatedInProgress());

    // Call repository function
    final result = await _cashInRepository.createCashInMomoOrder(
      contractId: event.contractId,
      contractNo: event.contractNo,
      customerId: event.customerId,
      amount: event.amount,
      quantity: event.quantity,
      receiverContractId: event.desContractId,
    );

    // Result
    result.when(
      success: (orderId) => emit(CashInOrderMomoCreatedSuccess(
        orderId: orderId,
        contractNo: event.contractNo,
        amount: event.amount,
      )),
      failure: (failure) => emit(CashInOrderCreatedFailure(
        failure.message ?? '',
      )),
    );
  }

  FutureOr<void> _onCashInMomoPayment(
    CashInMomoPayment event,
    Emitter<CashInState> emit,
  ) async {
    emit(CashInMomoPaymentInProgress());

    final request = MomoPaymentRequest(
      merchantName: F.momoMerchantName,
      merchantCode: F.momoPartnerCode,
      appScheme: F.momoPartnerSchemeId,
      partnerCode: F.momoPartnerCode,
      partner: 'merchant',
      orderId: event.orderId,
      orderLabel: 'Mã đơn hàng',
      username: event.contractNo,
      amount: event.amount,
      merchantNameLabel: 'Thu phí không dừng ePass',
      fee: 0,
      description: 'Thanh toán nạp tiền ứng dụng ePass',
      isTestMode: F.appFlavor != Flavor.prod,
    );

    final result = await _momoService.requestPayment(request);

    result.when(
      success: (response) {
        final phoneNumber = response.phoneNumber, token = response.token;
        if (phoneNumber == null || token == null) {
          emit(const CashInMomoPaymentFailure(
              'Có lỗi xảy ra, vui lòng thử lại'));
        } else {
          emit(CashInMomoPaymentSuccess(
            orderId: event.orderId,
            amount: event.amount,
            phoneNumber: phoneNumber,
            paymentToken: token,
          ));
        }
      },
      failure: (failure) =>
          emit(CashInMomoPaymentFailure(failure.message ?? '')),
    );
  }

  FutureOr<void> _onConfirmMomoPayment(
      ConfirmMomoOrder event, Emitter<CashInState> emit) async {
    emit(ConfirmMomoPaymentInProgress());

    final result = await _cashInRepository.confirmMomoPayment(
      partnerCode: event.partnerCode,
      orderId: event.orderId,
      amount: event.amount,
      phoneNumber: event.phoneNumber,
      token: event.token,
    );

    result.when(
      success: (data) => emit(ConfirmMomoPaymentSuccess(
          amount: data.amount,
          momoTransId: data.momoTransId,
          orderId: data.partnerRefId)),
      failure: (failure) => emit(
        ConfirmMomoPaymentFailure(failure.message ?? ''),
      ),
    );
  }

  FutureOr<void> _onCashInViettelPayCreated(
    CashInOrderViettelPayCreated event,
    Emitter<CashInState> emit,
  ) async {
    emit(CashInOrderCreatedInProgress());

    final result = await _cashInRepository.createCashInViettelPayOrder(
      contractId: event.contractId,
      contractNo: event.contractNo,
      customerId: event.customerId,
      amount: event.amount,
      quantity: event.quantity,
      receiverContractId: event.desContractId,
      paymentMethodName: event.paymentMethodName,
    );

    result.when(
      success: (orderId) => emit(CashInOrderViettelPayCreatedSuccess(
        orderId: orderId,
        contractNo: event.contractNo,
        amount: event.amount,
      )),
      failure: (failure) => emit(CashInOrderCreatedFailure(
        failure.message ?? '',
      )),
    );
  }

  FutureOr<void> _onCashInVNPayCreated(
    CashInOrderVNPayCreated event,
    Emitter<CashInState> emit,
  ) async {
    emit(CashInOrderCreatedInProgress());

    final result = await _cashInRepository.createCashInVNPayOrder(
      contractId: event.contractId,
      contractNo: event.contractNo,
      customerId: event.customerId,
      amount: event.amount,
      quantity: event.quantity,
      receiverContractId: event.desContractId,
    );

    result.when(
      success: (orderId) => emit(CashInOrderVNPayCreatedSuccess(
        orderId: orderId,
        contractNo: event.contractNo,
        amount: event.amount,
      )),
      failure: (failure) => emit(CashInOrderVNPayCreatedFailure(
        failure.message ?? '',
      )),
    );
  }

  FutureOr<void> _onCashInEpassPayment(
      CashInContractsPayment event, Emitter<CashInState> emit) async {
    final result = await _cashInRepository.topupEWallet(
        requestId: event.requestId,
        amount: event.amount,
        destContractId: event.destContractId,
        topupChannel: event.topupChannel);

    result.when(
      success: (success) => emit(CashInContractsPaymentSuccess(
          topupChannel: event.topupChannel,
          destContractId: event.destContractId,
          amount: event.amount,
          requestId: event.requestId)),
      failure: (failure) => emit(CashInContractsPaymentFailure(
        failure.message ?? '',
      )),
    );
  }

  //dumv
  FutureOr<void> _onCheckCustType(
      CheckCustomerType event, Emitter<CashInState> emit) async {
    emit(CheckCustomerTypeInProgress());
    final result = await _cashInRepository.checkTypeCust(
        contractId: event.contractId, amount: event.amount);
    result.when(
        success: (data) => emit(CheckCustomerTypeSuccess(
            deposit: data.data?.deposit,
            custType: data.data?.custType,
            limit: data.data?.limit)),
        failure: ((failure) =>
            emit(CheckCustomerTypeFailure(failure.message ?? ''))));
  }

  FutureOr<void> _onBidvCreateOrder(
      BidvCreateOrder event, Emitter<CashInState> emit) async {
    final result = await _cashInRepository.bidvCreateOrder(
      payerAdd: event.payerAdd,
      payerId: event.payerId,
      payerIdentity: event.payerIdentity,
      payerMobile: event.payerMobile,
      payerName: event.payerName,
      transAmount: event.transAmount,
    );
    result.when(
      success: (success) => emit(BidvCreateOrderSuccess(
          transId: result.success!.data.transId,
          responseCode: result.success!.data.responseCode)),
      failure: (failure) => emit(BidvCreateOrderFailure(
        failure.message ?? '',
      )),
    );
  }

  FutureOr<void> _onCheckOtpBidv(
      BidvCheckOTP event, Emitter<CashInState> emit) async {
    final result = await _cashInRepository.bidvCheckOTP(
        payerAdd: event.payerAdd,
        transId: event.transId,
        payerId: event.payerId,
        payerIdentity: event.payerIdentity,
        payerMobile: event.payerMobile,
        payerName: event.payerName,
        transAmount: event.transAmount,
        otpNumber: event.otpNumber);
    result.when(
      success: (success) => emit(BidvCheckOTPSuccess(
          transId: result.success!.data.transId,
          responseCode: result.success!.data.responseCode)),
      failure: (failure) => emit(BidvCheckOTPFailure(failure.message ?? '')),
    );
  }

  FutureOr<void> _onCheckVehicleInContract(
      CheckVehicleInContract event, Emitter<CashInState> emit) async {
    final result = await _cashInRepository.checkVehicleInContract(
        contractId: event.contractId);
    result.when(
      success: (success) => emit(CheckVehicleInContractSuccess(
          hasVehicle: result.success?.data,
          message: result.success?.mess?.description)),
      failure: (failure) => emit(
        CheckVehicleInContractFailure(failure.message ?? ''),
      ),
    );
  }
}
