// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car_doctor_config_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarDoctorConfig _$CarDoctorConfigFromJson(Map<String, dynamic> json) =>
    CarDoctorConfig(
      name: json['name'] as String?,
      code: json['code'] as String?,
      id: json['id'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$CarDoctorConfigToJson(CarDoctorConfig instance) =>
    <String, dynamic>{
      'name': instance.name,
      'code': instance.code,
      'id': instance.id,
      'description': instance.description,
    };

CarDoctorConfigData _$CarDoctorConfigDataFromJson(Map<String, dynamic> json) =>
    CarDoctorConfigData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => CarDoctorConfig.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CarDoctorConfigDataToJson(
        CarDoctorConfigData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
    };

CarDoctorConfigResponse _$CarDoctorConfigResponseFromJson(
        Map<String, dynamic> json) =>
    CarDoctorConfigResponse(
      data: CarDoctorConfigData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CarDoctorConfigResponseToJson(
        CarDoctorConfigResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
