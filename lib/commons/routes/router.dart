import 'package:auto_route/auto_route.dart';
import 'package:auto_route/empty_router_widgets.dart';
import 'package:epass/commons/routes/guards/first_run_guard.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/pages/beneficary/beneficary_bloc_provider_page.dart';
import 'package:epass/pages/beneficary/beneficary_details/beneficary_details_page.dart';
import 'package:epass/pages/beneficary/beneficary_list/beneficary_list_page.dart';
import 'package:epass/pages/buy_ticket/buy_ticket_bloc_provider_page.dart';
import 'package:epass/pages/buy_ticket/buy_ticket_page.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_confirm/buy_ticket_confirm_page.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_result/buy_ticket_result_details_page.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_result/buy_ticket_result_page.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/vehicle_other_page.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/vehicle_select_tab_page.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/vehicle_user_page.dart';
import 'package:epass/pages/cash_in/cash_in_BIDV_pay_gate.dart';
import 'package:epass/pages/cash_in/cash_in_vnpt_pay_gate.dart';

// import 'package:epass/pages/cash_in/cash_in_bidv_pay_gate.dart';
import 'package:epass/pages/cash_in/pages/cash_in_bank_transfer/cash_in_bank_transfer_page.dart';
import 'package:epass/pages/cash_in/cash_in_bloc_provider_page.dart';
import 'package:epass/pages/cash_in/cash_in_page.dart';
import 'package:epass/pages/cash_in/cash_in_viettel_pay_gate.dart';
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_confirm_otp_page.dart';
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_confirm_page.dart';
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_result_page.dart';
import 'package:epass/pages/cash_in/pages/cash_in_fee/cash_in_fee_page.dart';
import 'package:epass/pages/feedback/feedback_tab_page.dart';
import 'package:epass/pages/feedback/pages/create_feedback/create_feedback_page.dart';
import 'package:epass/pages/feedback/pages/list_feedback/list_feedback_page.dart';
import 'package:epass/pages/insurance/anbinh/insurance_an_binh_page.dart';
import 'package:epass/pages/notification_tab/pages/wait_for_confirmation/wait_for_confirmation_page.dart';
import 'package:epass/pages/notification_tab/pages/wait_for_confirmation/wait_for_confirmation_page.dart';
import 'package:epass/pages/parking/pages/parking_setting/parking_bloc_provider_page.dart';
import 'package:epass/pages/parking/pages/parking_setting/parking_bloc_provider_roadSide_page.dart';
import 'package:epass/pages/parking/pages/parking_setting/parking_roadSide_page.dart';
import 'package:epass/pages/parking/pages/parking_setting/parking_setting_page.dart';
import 'package:epass/pages/report/pages/create_report/create_report_page.dart';
import 'package:epass/pages/report/pages/list_report/list_report_page.dart';
import 'package:epass/pages/forgot_password/forgot_password_bloc_provider_page.dart';
import 'package:epass/pages/forgot_password/forgot_password_new_password_page.dart';
import 'package:epass/pages/forgot_password/forgot_password_page.dart';
import 'package:epass/pages/home/home_page.dart';
import 'package:epass/pages/home_tab/home_tabs_page.dart';
import 'package:epass/pages/insurance/gc/insurance_gc_page.dart';
import 'package:epass/pages/insurance/insurance_page.dart';
import 'package:epass/pages/insurance/viettelpay/insurance_viettelpay_page.dart';
import 'package:epass/pages/invoice/invoice_page.dart';
import 'package:epass/pages/invoice_setting/invoice_setting_page.dart';
import 'package:epass/pages/login/login_page.dart';
import 'package:epass/pages/notification_tab/notification_tab_page.dart';
import 'package:epass/pages/notification_tab/pages/balance_change/balance_change_page.dart';
import 'package:epass/pages/notification_tab/pages/discount/discount_page.dart';
import 'package:epass/pages/notification_tab/pages/event/event_page.dart';
import 'package:epass/pages/notification_tab/pages/news/news_page.dart';
import 'package:epass/pages/onboarding/onboarding_page.dart';
import 'package:epass/pages/forgot_password/forgot_password_otp_page.dart';
import 'package:epass/pages/register/pages/register_address/register_address_page.dart';
import 'package:epass/pages/register/pages/register_confirm/register_confirm_page.dart';
import 'package:epass/pages/register/pages/register_customer/register_paper_scan_page.dart';
import 'package:epass/pages/register/pages/register_customer/register_paper_type_page.dart';
import 'package:epass/pages/register/pages/register_otp_confirm/regsiter_otp_page.dart';
import 'package:epass/pages/register/pages/register_service/register_service_page.dart';
import 'package:epass/pages/register/pages/register_vehicles/register_vehicles_page.dart';
import 'package:epass/pages/register/register_bloc_provider_page.dart';
import 'package:epass/pages/report/report_tab_page.dart';
import 'package:epass/pages/setting/alias/alias_page.dart';
import 'package:epass/pages/setting/app_info/app_info_page.dart';
import 'package:epass/pages/setting/change_password/change_password_page.dart';
import 'package:epass/pages/setting/notification_setting/notification_setting_page.dart';
import 'package:epass/pages/setting/profile/profile_page.dart';
import 'package:epass/pages/setting/referrals_epass/ui/referrals_client.dart';
import 'package:epass/pages/setting/setting_entrance/setting_page.dart';
import 'package:epass/pages/place/pages/bot_places/bot_places_page.dart';
import 'package:epass/pages/place/pages/viettel_places/viettel_places_page.dart';
import 'package:epass/pages/place/place_tab_page.dart';
import 'package:epass/pages/support/support_page.dart';
import 'package:epass/pages/transaction_history_tab/pages/contract_action/contract_action_page.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_other/transaction_other_page.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/transaction_parking_page.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_ticket_purchase/transaction_ticket_purchase_page.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_topup/transaction_topup_page.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/transaction_vehicle_page.dart';
import 'package:epass/pages/transaction_history_tab/transaction_history_tab_page.dart';
import 'package:epass/pages/utility/cool_penalty/cool_penalty_page.dart';
import 'package:epass/pages/utility/sms_register/sms_register_page.dart';
import 'package:epass/pages/utility/utility_page.dart';
import 'package:epass/pages/utility/vietmap/viet_map_page.dart';
import 'package:epass/pages/vehicle/vehicle_bloc_provider_page.dart';
import 'package:epass/pages/vehicle/vehicle_details/vehicle_details_page.dart';
import 'package:epass/pages/vehicle/vehicle_list/vehicle_list_page.dart';
import 'package:flutter/foundation.dart';

import '../../pages/car_service/car_service_page.dart';
import '../../pages/web/webview_page.dart';

@AdaptiveAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      path: '/onboarding',
      page: OnboardingPage,
    ),
    AutoRoute(
      path: '/login',
      page: LoginPage,
      guards: [FirstRunGuard],
      initial: true,
    ),
    AutoRoute(
      path: '/forgot-password',
      page: ForgotPasswordBlocProviderPage,
      name: 'ForgotPasswordRouter',
      children: [
        AutoRoute(path: '', page: ForgotPasswordPage),
        AutoRoute(path: 'new-password', page: ForgotPasswordNewPasswordPage),
        AutoRoute(path: 'otp', page: ForgotPasswordOTPPage),
      ],
    ),
    AutoRoute(
      path: '/register',
      page: RegisterBlocProviderPage,
      name: 'RegisterRouter',
      children: [
        AutoRoute(path: '', page: RegisterVehiclesPage),
        AutoRoute(path: 'verify', page: RegisterPaperTypePage),
        AutoRoute(path: 'scan', page: RegisterPaperScanPage),
        AutoRoute(path: 'address', page: RegisterAddressPage),
        AutoRoute(path: 'confirm', page: RegisterConfirmPage),
        AutoRoute(path: 'service', page: RegisterServicePage),
        AutoRoute(path: 'otp', page: RegisterOTPPage),
      ],
    ),
    AutoRoute(
      path: '/home',
      page: HomeTabsPage,
      children: [
        AutoRoute(
          path: '',
          page: EmptyRouterPage,
          name: 'HomeRouter',
          children: [
            AutoRoute(path: '', page: HomePage),
            AutoRoute(path: 'carServices', page: CarServicePage),
            AutoRoute(path: 'webview', page: WebViewPage),
            AutoRoute(
              name: 'CashInRouter',
              path: 'cash-in',
              page: CashInBlocProviderPage,
              children: [
                AutoRoute(path: '', page: CashInPage),
                AutoRoute(path: 'fee', page: CashInFeePage),
                AutoRoute(path: 'bank-transfer', page: CashInBankTransferPage),
                AutoRoute(path: 'confirm', page: CashInConfirmPage),
                AutoRoute(path: 'check-otp-bidv', page: CashInConfirmOTPPage),
                AutoRoute(path: 'result', page: CashInResultPage),
                AutoRoute(
                  path: 'viettel-pay-gate',
                  page: CashInViettelPayGatePage,
                ),
                AutoRoute(
                  path: 'bidv-pay-gate',
                  page: CashInBIDVPayGatePage,
                ),
                AutoRoute(path: 'vnpt-pay-gate', page: CashInVnptPayGatePage),
              ],
            ),
            AutoRoute(
              name: 'BuyTicketRouter',
              path: 'buy-ticket',
              page: BuyTicketBlocProviderPage,
              children: [
                AutoRoute(path: '', page: BuyTicketPage, initial: true),
                AutoRoute(
                  path: 'vehicle',
                  page: VehicleSelectTabPage,
                  children: [
                    AutoRoute(path: 'user', page: VehicleUserPage),
                    AutoRoute(path: 'other', page: VehicleOtherPage),
                  ],
                ),
                AutoRoute(path: 'confirm', page: BuyTicketConfirmPage),
                AutoRoute(
                  name: 'BuyTicketResultRouter',
                  path: 'result',
                  page: EmptyRouterPage,
                  children: [
                    AutoRoute(path: '', page: BuyTicketResultPage),
                    AutoRoute(path: 'details', page: BuyTicketResultDetailPage),
                  ],
                ),
              ],
            ),
            AutoRoute(path: 'invoice', page: InvoicePage),
            AutoRoute(
              path: 'transaction-history',
              page: TransactionHistoryTabPage,
              name: 'TransactionHistoryRouter',
              children: [
                AutoRoute(
                    path: 'transaction-ticket-purchase',
                    page: TransactionTicketPurchasePage),
                AutoRoute(
                  path: 'transaction-vehicle',
                  page: TransactionVehiclePage,
                ),
                AutoRoute(
                  path: 'transaction-parking',
                  page: TransactionParkingPage,
                ),
                AutoRoute(
                  path: 'transaction-topup',
                  page: TransactionTopupPage,
                ),
                AutoRoute(
                  path: 'contract-action',
                  page: ContractActionPage,
                ),
                AutoRoute(
                  path: 'transaction-other',
                  page: TransactionOtherPage,
                ),
              ],
            ),
            AutoRoute(
              path: 'vehicles',
              page: VehicleBlocProviderPage,
              name: 'VehicleRouter',
              children: [
                AutoRoute(path: '', page: VehicleListPage),
                AutoRoute(path: ':id', page: VehicleDetailsPage),
              ],
            ),
            AutoRoute(
              path: 'beneficary',
              page: BeneficiaryBlocProviderPage,
              name: 'BenficaryRouter',
              children: [
                AutoRoute(path: '', page: BeneficiaryListPage),
                AutoRoute(path: ':id', page: BeneficiaryDetailsPage),
              ],
            ),
            AutoRoute(
              path: 'insurance',
              name: 'InsuranceRouter',
              page: EmptyRouterPage,
              children: [
                AutoRoute(path: '', page: InsurancePage),
                AutoRoute(path: 'vds', page: InsuranceViettelPayPage),
                AutoRoute(path: 'aba', page: InsuranceAnBinhPage),
                AutoRoute(path: 'gc', page: InsuranceGCPage),
              ],
            ),
            AutoRoute(
                name: 'AliasDialogRoute',
                path: 'alias-dialog',
                page: AliasPage),
            AutoRoute(
              name: 'SupportRouter',
              path: 'support',
              page: EmptyRouterPage,
              children: [
                AutoRoute(path: '', page: SupportPage),
                AutoRoute(
                  path: 'feedback',
                  page: FeedbackTabPage,
                  children: [
                    AutoRoute(path: 'create', page: CreateFeedbackPage),
                    AutoRoute(path: 'list', page: ListFeedbackPage),
                  ],
                ),
                AutoRoute(
                  path: 'report',
                  page: ReportTabPage,
                  children: [
                    AutoRoute(path: 'create', page: CreateReportPage),
                    AutoRoute(path: 'list', page: ListReportPage),
                  ],
                )
              ],
            ),
          ],
        ),
        AutoRoute(
          path: 'utility',
          page: EmptyRouterPage,
          name: 'UtilityRouter',
          children: [
            AutoRoute(path: '', page: UtilityPage),
            AutoRoute(
              path: 'place',
              page: PlaceTabPage,
              children: [
                AutoRoute(path: 'bot', page: BotPlacesPage),
                AutoRoute(path: 'viettel', page: ViettelPlacesPage),
              ],
            ),
            AutoRoute(path: 'sms-register', page: SmsRegisterPage),
            AutoRoute(path: 'invoice-setting', page: InvoiceSettingPage),
            AutoRoute(path: 'cool-penalty', page: CoolPenaltyPage),
            AutoRoute(
                path: 'parking',
                page: ParkingBlocProviderPage,
                children: [AutoRoute(path: '', page: ParkingSettingPage)]),
            AutoRoute(
                path: 'parking-roadSide',
                page: ParkingBlocProviderRoadSidePage,
                children: [AutoRoute(path: '', page: ParkingRoadSidePage)]),
            AutoRoute(path: 'vietmap', page: VietMapPage),
            AutoRoute(path: 'referrals-client', page: ReferralsClientPage),
          ],
        ),
        AutoRoute(
          path: 'notification',
          page: NotificationTabPage,
          name: 'NotificationRouter',
          children: [
            AutoRoute(path: 'event', page: EventPage),
            AutoRoute(
                path: 'wait-for-confirmation', page: WaitForConfirmationPage),
            AutoRoute(path: 'balance-change', page: BalanceChangePage),
            AutoRoute(path: 'news', page: NewsPage),
            AutoRoute(path: 'discount', page: DiscountPage),
          ],
        ),
        AutoRoute(
          path: 'setting',
          page: EmptyRouterPage,
          name: 'SettingRouter',
          children: [
            AutoRoute(
              path: 'vehicles',
              page: VehicleBlocProviderPage,
              name: 'VehicleRouter',
              children: [
                AutoRoute(path: '', page: VehicleListPage),
                AutoRoute(path: ':id', page: VehicleDetailsPage),
              ],
            ),
            AutoRoute(
              name: 'CashInRouter',
              path: 'cash-in',
              page: CashInBlocProviderPage,
              children: [
                AutoRoute(path: '', page: CashInPage),
                AutoRoute(path: 'fee', page: CashInFeePage),
                AutoRoute(path: 'bank-transfer', page: CashInBankTransferPage),
                AutoRoute(path: 'confirm', page: CashInConfirmPage),
                AutoRoute(path: 'check-otp-bidv', page: CashInConfirmOTPPage),
                AutoRoute(path: 'result', page: CashInResultPage),
                AutoRoute(
                  path: 'viettel-pay-gate',
                  page: CashInViettelPayGatePage,
                ),
                AutoRoute(
                  path: 'bidv-pay-gate',
                  page: CashInBIDVPayGatePage,
                ),
                AutoRoute(path: 'vnpt-pay-gate', page: CashInVnptPayGatePage),
              ],
            ),
            AutoRoute(path: '', page: SettingPage),
            AutoRoute(path: 'profile', page: ProfilePage),
            AutoRoute(
                path: 'notification-setting', page: NotificationSettingPage),
            AutoRoute(path: 'alias', page: AliasPage),
            AutoRoute(path: 'change-password', page: ChangePasswordPage),
            AutoRoute(path: 'app-info', page: AppInfoPage),
          ],
        ),
      ],
    ),
    AutoRoute(path: 'sms-register', page: SmsRegisterPage),
  ],
)
class $AppRouter {}

class EmptyRouterPage extends AutoRouter {
  const EmptyRouterPage({Key? key}) : super(key: key);
}
