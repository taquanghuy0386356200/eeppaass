import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ocs_info.g.dart';

@JsonSerializable()
class OcsInfo {
  final int? contractId;
  final int? custId;
  final String? contractNo;
  final String? signDate;
  final String? effDate;
  final String? status;
  final String? emailNotification;
  final String? smsNotification;
  final String? pushNotification;
  final String? billCycle;
  final String? payCharge;
  final String? accountUser;
  final String? noticeName;
  final String? noticeAreaName;
  final String? noticeStreet;
  final String? noticeAreaCode;
  final String? noticeEmail;
  final String? noticePhoneNumber;
  final String? profileStatus;
  final String? approvedUser;
  final int? approvedDate;
  final String? signName;
  final String? smsRenew;
  final String? createUser;
  final String? createDate;
  final int? balance;
  final String? billCycleMergeType;
  final int? isAlertMoney;
  final int? alertMoney;
  final int? isSpecial;

  OcsInfo({
    this.contractId,
    this.custId,
    this.contractNo,
    this.signDate,
    this.effDate,
    this.status,
    this.emailNotification,
    this.smsNotification,
    this.pushNotification,
    this.billCycle,
    this.payCharge,
    this.accountUser,
    this.noticeName,
    this.noticeAreaName,
    this.noticeStreet,
    this.noticeAreaCode,
    this.noticeEmail,
    this.noticePhoneNumber,
    this.profileStatus,
    this.approvedUser,
    this.approvedDate,
    this.signName,
    this.smsRenew,
    this.createUser,
    this.createDate,
    this.balance,
    this.billCycleMergeType,
    this.isAlertMoney,
    this.alertMoney,
    this.isSpecial,
  });

  factory OcsInfo.fromJson(Map<String, dynamic> json) =>
      _$OcsInfoFromJson(json);

  Map<String, dynamic> toJson() => _$OcsInfoToJson(this);
}

@JsonSerializable()
class OCSInfoResponse {
  @JsonKey(name: 'data')
  final OcsInfo? ocsInfo;
  final MessageData? mess;

  OCSInfoResponse({
    this.ocsInfo,
    this.mess,
  });

  factory OCSInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$OCSInfoResponseFromJson(json);
}
