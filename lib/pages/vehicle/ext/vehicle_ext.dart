import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:flutter/material.dart';

extension VehicleExt on Vehicle {
  bool get isAvailable {
    switch (status) {
      case null:
      case VehicleStatus.inactive:
      case VehicleStatus.unknown:
        return false;
      case VehicleStatus.active:
        switch (activeStatus) {
          case null:
          case VehicleActiveStatus.inactive:
          case VehicleActiveStatus.canceled:
          case VehicleActiveStatus.closed:
          case VehicleActiveStatus.transfered:
          case VehicleActiveStatus.unknown:
            return false;
          case VehicleActiveStatus.active:
          case VehicleActiveStatus.open:
            return true;
        }
    }
  }

  Color get vehicleGroupColor {
    switch (vehicleGroupId) {
      case 1:
        return const Color(0xFFF2994A);
      case 2:
        return const Color(0xFF2F80ED);
      case 3:
        return const Color(0xFF9B51E0);
      case 4:
        return const Color(0xFFEB5757);
      case 5:
        return const Color(0xFF27AE60);
      default:
        return Colors.grey;
    }
  }

  Color get backgroundStatusColor {
    if (isAvailable) {
      return const Color(0xFFE7FFF4);
    } else {
      return const Color(0xFFFFF9D9);
    }
  }

  Color get textStatusColor {
    if (isAvailable) {
      return const Color(0xFF006848);
    } else {
      return const Color(0xFFCA9D00);
    }
  }
}

extension VehicleActiveStatusExt on VehicleActiveStatus {
  String get title {
    switch (this) {
      case VehicleActiveStatus.inactive:
        return 'Chưa kích hoạt';
      case VehicleActiveStatus.active:
        return 'Hoạt động';
      case VehicleActiveStatus.canceled:
        return 'Đã huỷ';
      case VehicleActiveStatus.closed:
        return 'Đã đóng';
      case VehicleActiveStatus.open:
        return 'Mở';
      case VehicleActiveStatus.transfered:
        return 'Đã chuyển nhượng';
      case VehicleActiveStatus.unknown:
      default:
        return 'N/A';
    }
  }
}

extension VehicleIsDebitStatus on  VehicleDebitStatus{
  String get statusDebit {
    switch (this) {
      case VehicleDebitStatus.inactive:
        return 'Không nợ';
      case VehicleDebitStatus.active:
        return 'Đang nợ phí';
      case VehicleDebitStatus.complete:
        return 'Đã thanh toán';
      default:
        return 'N/A';
    }
  }
}



