import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/repo/buy_ticket_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/buy_ticket_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/service_plan_type_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/station_stage_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_duration_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BuyTicketBlocProviderPage extends StatelessWidget {
  final List<Vehicle>? userVehicle;

  const BuyTicketBlocProviderPage({
    Key? key,
    this.userVehicle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => StationStageBloc()),
        BlocProvider(create: (context) => ServicePlanTypeBloc()),
        BlocProvider(create: (context) => TicketDurationBloc()),
        BlocProvider(create: (context) => TicketVehicleBloc(userVehicles: userVehicle)),
        BlocProvider(
          create: (context) => VehicleListBloc(
            pageSize: 100,
            vehicleRepository: getIt<IVehicleRepository>(),
            statuses: [1],
            activeStatuses: [1, 4],
          ),
        ),
        BlocProvider(
          create: (context) => BalanceBloc(userRepository: getIt<IUserRepository>(), appBloc: getIt<AppBloc>()),
        ),
        BlocProvider(
          create: (context) => TicketPriceBloc(
            buyTicketRepository: getIt<IBuyTicketRepository>(),
          ),
        ),
        BlocProvider(
          create: (context) => BuyTicketBloc(
            buyTicketRepository: getIt<IBuyTicketRepository>(),
            appBloc: getIt<AppBloc>(),
          ),
        ),
      ],
      child: const AutoRouter(),
    );
  }
}
