// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_passcode_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChangePassCodeRequest _$ChangePassCodeRequestFromJson(
        Map<String, dynamic> json) =>
    ChangePassCodeRequest(
      serviceId: json['serviceId'] as int,
      passCode: json['passCode'] as String,
      otp: json['otp'] as String,
    );

Map<String, dynamic> _$ChangePassCodeRequestToJson(
        ChangePassCodeRequest instance) =>
    <String, dynamic>{
      'serviceId': instance.serviceId,
      'passCode': instance.passCode,
      'otp': instance.otp,
    };
