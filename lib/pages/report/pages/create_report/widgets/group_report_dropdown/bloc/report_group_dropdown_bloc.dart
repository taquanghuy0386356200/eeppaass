import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:simple_result/simple_result.dart';

part 'report_group_dropdown_event.dart';

part 'report_group_dropdown_state.dart';

class ReportGroupDropdownBloc extends Bloc<ReportGroupDropdownEvent, ReportGroupDropdownState> {
  final IFeedbackRepository _feedbackRepository;

  ReportGroupDropdownBloc({required IFeedbackRepository feedbackRepository})
      : _feedbackRepository = feedbackRepository,
        super(ReportGroupDropdownInitial()) {
    on<ReportGroupDropdownFetched>(_onReportGroupDropdownFetched);
  }

  FutureOr<void> _onReportGroupDropdownFetched(
    ReportGroupDropdownFetched event,
    Emitter<ReportGroupDropdownState> emit,
  ) async {
    emit(ReportGroupDropdownFetchedInProgress());

    final Result<FeedbackTypeDataNotNull, Failure> result =
        await _feedbackRepository.getFeedbackTypes(feedbackChannel: FeedbackChannel.report, parentId: event.parentId);

    result.when(
      success: (success) => emit(ReportGroupDropdownFetchedSuccess(data: success.listData)),
      failure: (failure) => emit(ReportGroupDropdownFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
