import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/cash_in/widget/cash_in_currency_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/commons/extensions/number_ext.dart';

class QuickCashInWidget extends StatelessWidget {
  final int? selectedValue;
  final void Function(int? value)? onChange;

  const QuickCashInWidget({
    Key? key,
    this.selectedValue,
    this.onChange,
  }) : super(key: key);

  final _quickCashIn = const [
    500000,
    1000000,
    2000000,
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: 2.15,
        crossAxisSpacing: 16.w,
        mainAxisSpacing: 12.h,
      ),
      itemCount: _quickCashIn.length,
      itemBuilder: (context, index) {
        final isSelected = selectedValue == _quickCashIn[index];

        return Material(
          borderRadius: BorderRadius.circular(8.0),
          color: isSelected ? ColorName.primaryColor : Colors.white,
          child: InkWell(
            onTap: () =>
                onChange?.call(!isSelected ? _quickCashIn[index] : null),
            borderRadius: BorderRadius.circular(8.0),
            child: Container(
              height: 40.h,
              padding: EdgeInsets.symmetric(
                horizontal: 4.w,
                vertical: 12.h,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(
                  color: ColorName.disabledBorderColor,
                  width: 1,
                  style: isSelected ? BorderStyle.none : BorderStyle.solid,
                ),
              ),
              child: Center(
                child: CashInCurrencyText(
                  text: _quickCashIn[index].formatCurrency(),
                  color: isSelected ? Colors.white : ColorName.textGray1,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
