part of 'beneficary_list_bloc.dart';

abstract class BeneficiaryListEvent extends Equatable {}

class BeneficiaryListFetched extends BeneficiaryListEvent {
  @override
  List<Object> get props => [];
}

class BeneficiaryListRefreshed extends BeneficiaryListEvent {
  @override
  List<Object> get props => [];
}

class BeneficiaryListLoadMore extends BeneficiaryListEvent {
  @override
  List<Object> get props => [];
}

class BeneficiaryListSearched extends BeneficiaryListEvent {
  final String? reminiscentName;

  BeneficiaryListSearched({
    this.reminiscentName,
  });

  @override
  List<Object?> get props => [reminiscentName];
}
