import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';

part 'active_otp_event.dart';
part 'active_otp_state.dart';

class ActiveOtpBloc extends Bloc<ActiveOtpEvent, ActiveOtpState> {
  final IFeedbackRepository _feedbackRepository;

  ActiveOtpBloc({
    required IFeedbackRepository feedbackRepository,
  })  : _feedbackRepository = feedbackRepository,
        super(ActiveOtpInitial()) {
    on<ActiveOTPConfirmed>(_onActiveOTPConfirmed);
    on<ActiveOTPRequested>(_onActiveOTPRequest);
    on<ActiveOTPPaused>((event, emit) async {
      if (state is ActiveOtpConfirmedSuccess) {
        final submittedTime =
            (state as ActiveOtpConfirmedSuccess).submittedTime;
        emit(ActiveOtpPaused(submittedTime));
      }
    });
  }

  FutureOr<void> _onActiveOTPConfirmed(
      ActiveOTPConfirmed event, Emitter<ActiveOtpState> emit) async {
    emit(ActiveOtpConfirmedInProgress());
  }

  FutureOr<void> _onActiveOTPRequest(
      ActiveOTPRequested event, Emitter<ActiveOtpState> emit) async {
    if (state is ActiveOtpConfirmedSuccess) {
      final submittedTime = (state as ActiveOtpConfirmedSuccess).submittedTime;

      final count = DateTime.now().difference(submittedTime).inSeconds;
      if (count < 500) {
        emit(ActiveOtpConfirmedSuccess(submittedTime));
        return;
      }
    }
    if (state is ActiveOtpPaused) {
      final submittedTime = (state as ActiveOtpPaused).submittedTime;
      emit(ActiveOtpConfirmedSuccess(submittedTime));
      return;
    }

    final result =
        await _feedbackRepository.feedbackRequestOtp(confirmType: 19);
    
    result.when(
      success: (success) {
        emit(ActiveOtpConfirmedSuccess(DateTime.now()));
      },
      failure: (failure) {
        emit(ActiveOtpConfirmedFailure(
            message: failure.message ?? 'Có lỗi xảy ra'));
      },
    );
  }
}
