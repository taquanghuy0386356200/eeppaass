import 'dart:async';

import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/services/momo/mom_constant.dart';
import 'package:epass/commons/services/momo/momo_payment_request.dart';
import 'package:epass/commons/services/momo/momo_payment_response.dart';
import 'package:flutter/services.dart';
import 'package:simple_result/simple_result.dart';

class MomoPayment {
  static const MethodChannel _channel = MethodChannel(MomoConstant.channelName);

  Future<Result<MomoPaymentResponse, Failure>> requestPayment(
      MomoPaymentRequest request) async {
    try {
      Map<String, dynamic>? result = await _channel.invokeMapMethod(
        MomoConstant.methodRequestPayment,
        request.toJson(),
      );
      if (result == null) {
        return const Result.failure(UnknownFailure());
      }
      final response = MomoPaymentResponse.fromMap(result);
      if (response.status == MomoPaymentStatus.failure) {
        return const Result.failure(UnknownFailure());
      }
      return Result.success(response);
    } on PlatformException catch (error) {
      return Result.failure(UnknownFailure(message: error.toString()));
    }
  }
}
