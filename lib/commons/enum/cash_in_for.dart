enum CashInFor {
  me,
  other,
}

extension CashInForExt on CashInFor {
  String toLabel() {
    switch (this) {
      case CashInFor.me:
        return 'Nạp tiền cho tài khoản của tôi';
      case CashInFor.other:
        return 'Nạp tiền cho tài khoản khác';
    }
  }
}
