import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../commons/widgets/animations/fade_animation.dart';
import '../../../commons/widgets/buttons/primary_button.dart';
import '../../../commons/widgets/buttons/secondary_button.dart';
import '../../../commons/widgets/container/gradient_header_container.dart';
import '../../bloc/app/app_bloc.dart';
import '../model/list_app.dart';

typedef Int2VoidFunc = void Function(BuildContext context);

class PolicyScreen extends StatefulWidget {
  const PolicyScreen({Key? key, this.appsModel, this.nextAction}) : super(key: key);
  final ListApp? appsModel;
  final Int2VoidFunc? nextAction;

  @override
  State<PolicyScreen> createState() => _PolicyScreen();
}

class _PolicyScreen extends State<PolicyScreen> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: BlocSelector<AppBloc, AppState, String?>(
        selector: (state) => state.user?.userName,
        builder: (context, userName) {
          return BasePage(
              backgroundColor: Colors.white,
              title: "Điều khoản ${widget.appsModel?.appName}",
              leading: BackButton(
                color: Colors.white,
                onPressed: () async {
                  final router = context.router;
                  router.pop();
                  return;
                },
              ),
              child: Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FadeAnimation(
                        delay: 0.5,
                        child: GradientHeaderContainer(
                          height: 120.0.h,
                        ),
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 16.0.w,right: 16.0.w,bottom: 69.h),
                          child: SingleChildScrollView(
                            child: Text(
                                widget.appsModel?.contentCondition ?? "",
                              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Colors.black),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding:  EdgeInsets.only(left: 16.h, right: 16.h, bottom: 16.h),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: PrimaryButton(
                          onTap: () {
                            if (widget.nextAction != null) {
                              widget.nextAction!(context);
                            }
                          },
                          title: 'Tiếp tục',
                        ),
                      ),
                    ),
                  )
                ],
              ));
        },
      ),
    );
  }
}
