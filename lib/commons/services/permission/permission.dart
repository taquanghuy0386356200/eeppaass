import 'package:epass/commons/api/failures/failures.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:simple_result/simple_result.dart';

Future<Result<String?, Failure>> requestPermission(
  Permission permission,
) async {
  final permissionStatus = await permission.request();

  switch (permissionStatus) {
    case PermissionStatus.granted:
    case PermissionStatus.limited:
      return const Result.success('Thành công');
    case PermissionStatus.denied:
    case PermissionStatus.permanentlyDenied:
    case PermissionStatus.restricted:
      return const Result.failure(PermissionFailure(
        message: 'Không có quyền truy cập',
      ));
    default:
      return const Result.failure(PermissionFailure());
  }
}
