import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:epass/flavors.dart';

class ViettelPayInsuranceRequest {
  /// Mã đối tác mà Viettel đã cung cấp
  final String merchantCode = F.insuranceVDSMerchantCode;

  /// Số điện thoại người dùng
  final String msisdn;

  /// Thời gian truy cập (milliseconds)
  final int time;

  /// Mã hợp đồng
  final String userName;

  ViettelPayInsuranceRequest({
    required this.msisdn,
    required this.time,
    required this.userName,
  });

  String get checksum {
    var phone = msisdn;
    if (msisdn.startsWith('0')) {
      phone = msisdn.replaceFirst('0', '84');
    }

    final checkSumStr = '${F.insuranceVDSAccessCode}'
        '${F.insuranceVDSSecretKey}'
        '${F.insuranceVDSMerchantCode}'
        '$phone'
        '$userName'
        '$time';
    final data = utf8.encode(checkSumStr);
    final digest = sha256.convert(data);
    return digest.toString();
  }

  String get insuranceUrl {
    var phone = msisdn;
    if (msisdn.startsWith('0')) {
      phone = msisdn.replaceFirst('0', '84');
    }

    final params = 'merchant_code=$merchantCode'
        '&msisdn=$phone'
        '&time=$time'
        '&userName=$userName'
        '&check_sum=$checksum';

    return '${F.insuranceVDSURL}?${Uri.encodeFull(params)}';
  }
}
