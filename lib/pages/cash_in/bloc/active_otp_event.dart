part of 'active_otp_bloc.dart';

abstract class ActiveOtpEvent extends Equatable {
  const ActiveOtpEvent();
}

class ActiveOTPConfirmed extends ActiveOtpEvent {
  final String phoneNumber;
  final String otp;

  const ActiveOTPConfirmed({
    required this.phoneNumber,
    required this.otp,
  });

  @override
  List<Object> get props => [
        phoneNumber,
        otp,
      ];
}

class ActiveOTPRequested extends ActiveOtpEvent {
  const ActiveOTPRequested();

  @override
  List<Object> get props => [];
}

class ActiveOTPPaused extends ActiveOtpEvent {
  const ActiveOTPPaused();

  @override
  List<Object> get props => [];
}
