part of 'referrals_client_bloc.dart';

abstract class ReferralsClientState extends Equatable {
  const ReferralsClientState();
}

class ReferralsClientInitState extends ReferralsClientState {
  @override
  List<Object?> get props => [];
}

class SendingInfoReferralsClientState extends ReferralsClientState {
  @override
  List<Object?> get props => [];
}

class SuccessReferralsClientState extends ReferralsClientState {
  @override
  List<Object?> get props => [];
}

class FailReferralsClientState extends ReferralsClientState {
  final String messageError;

  const FailReferralsClientState({required this.messageError});

  @override
  List<Object?> get props => [messageError];
}

class DisableSendReferralsClientState extends ReferralsClientState {
  @override
  List<Object?> get props => [];
}
