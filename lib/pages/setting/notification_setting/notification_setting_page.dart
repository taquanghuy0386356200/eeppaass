import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/setting/notification_setting/bloc/balance_alert_bloc/balance_alert_bloc.dart';
import 'package:epass/pages/setting/notification_setting/bloc/sms_vehicle_bloc/sms_vehicle_bloc.dart';
import 'package:epass/pages/setting/notification_setting/widgets/app_notification_setting.dart';
import 'package:epass/pages/setting/notification_setting/widgets/balance_alert_setting.dart';
import 'package:epass/pages/setting/notification_setting/widgets/sms_vehicle_setting.dart';
import 'package:epass/pages/setting/notification_setting/widgets/station_vehicle_alert_setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NotificationSettingPage extends StatefulWidget {
  const NotificationSettingPage({Key? key}) : super(key: key);

  @override
  State<NotificationSettingPage> createState() => _NotificationSettingPageState();
}

class _NotificationSettingPageState extends State<NotificationSettingPage> {
  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: 'Cài đặt thông báo',
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                  child: ShadowCard(
                    shadowOpacity: 0.2,
                    borderRadius: 16.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const AppNotificationSetting(),
                        Divider(
                          color: ColorName.textGray6,
                          indent: 16.w,
                          endIndent: 16.w,
                          height: 1.0,
                          thickness: 1.0,
                        ),
                        BlocProvider(
                          create: (context) =>
                              BalanceAlertBloc(
                                userRepository: getIt<IUserRepository>(),
                                appBloc: getIt<AppBloc>(),
                              ),
                          child: const BalanceAlertSetting(),
                        ),
                        Divider(
                          color: ColorName.textGray6,
                          indent: 16.w,
                          endIndent: 16.w,
                          height: 1.0,
                          thickness: 1.0,
                        ),
                        const StationVehicleAlertSetting(),
                        Divider(
                          color: ColorName.textGray6,
                          indent: 16.w,
                          endIndent: 16.w,
                          height: 1.0,
                          thickness: 1.0,
                        ),
                        BlocProvider(
                          create: (context) =>
                          SMSVehicleBloc(
                            userRepository: getIt<IUserRepository>(),
                          )
                            ..add(UserSMSVehicleFetched()),
                          child: const SMSVehicleSetting(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
