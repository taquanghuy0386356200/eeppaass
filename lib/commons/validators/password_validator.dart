class PasswordValidator {
  final _expression =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');

  String? passwordValidator(String? password) {
    if (password == null || password.isEmpty) {
      return 'Vui lòng nhập Mật khẩu';
    } else if (!_expression.hasMatch(password)) {
      return 'Mật khẩu yêu cầu ít nhất 8 ký tự, gồm: ký tự hoa, ký tự thường, số và ký tự đặc biệt';
    }
    return null;
  }

  String? passwordJustValidateLength(String? password) {
    if (password == null || password.isEmpty) {
      return 'Vui lòng nhập Mật khẩu';
    } else if ((password ?? '').length < 6 || (password ?? '').length > 20) {
      return 'Mật khẩu yêu cầu độ dài 6-20 ký tự';
    }
    return null;
  }
}
