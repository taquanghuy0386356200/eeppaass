part of 'register_paper_scan_bloc.dart';

@immutable
abstract class RegisterPaperScanState extends Equatable {
  const RegisterPaperScanState();
}

class RegisterPaperScanInitial extends RegisterPaperScanState {
  const RegisterPaperScanInitial();

  @override
  List<Object> get props => [];
}

class RegisterPaperScannedInProgress extends RegisterPaperScanState {
  const RegisterPaperScannedInProgress();

  @override
  List<Object> get props => [];
}

class RegisterPaperScannedFailure extends RegisterPaperScanState {
  final String message;

  const RegisterPaperScannedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class RegisterPaperScannedSuccess extends RegisterPaperScanState {
  final IdCardOcr idCard;
  final String? front;
  final String? back;

  const RegisterPaperScannedSuccess({
    required this.idCard,
    this.front,
    this.back,
  });

  @override
  List<Object?> get props => [idCard, front, back];
}
