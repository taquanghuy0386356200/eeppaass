import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/authentication_repository.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/setting/setting_entrance/bloc/setting_page_bloc.dart';
import 'package:epass/pages/setting/setting_entrance/widgets/setting_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SettingPage extends StatefulWidget implements AutoRouteWrapper {
  const SettingPage({Key? key}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (context) => SettingPageBloc(
        authenticationRepository: getIt<IAuthenticationRepository>(),
        appBloc: getIt<AppBloc>(),
      ),
      child: this,
    );
  }
}

class _SettingPageState extends State<SettingPage>
    with AutomaticKeepAliveClientMixin {
  bool _firstRun = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BasePage(
      backgroundColor: Colors.white,
      title: 'Cài đặt',
      child: Stack(
        children: [
          Positioned(
            child: FadeAnimation(
              delay: 0.5,
              playAnimation: _firstRun,
              child: GradientHeaderContainer(height: 320.h),
            ),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              playAnimation: _firstRun,
              child: const SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: SettingList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
