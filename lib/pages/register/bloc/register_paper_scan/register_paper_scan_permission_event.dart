part of 'register_paper_scan_permission_bloc.dart';

abstract class RegisterPaperScanPermissionEvent extends Equatable {
  const RegisterPaperScanPermissionEvent();
}

class RegisterPaperScanPermissionRequested extends RegisterPaperScanPermissionEvent {
  final bool isFront;

  const RegisterPaperScanPermissionRequested({this.isFront = true});

  @override
  List<Object> get props => [isFront];
}
