import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_tab_page.dart';
import 'package:epass/commons/widgets/pages/gradient_app_bar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionHistoryTabPage extends StatefulWidget {
  const TransactionHistoryTabPage({
    Key? key,
  }) : super(key: key);

  @override
  State<TransactionHistoryTabPage> createState() =>
      _TransactionHistoryTabPageState();
}

class _TransactionHistoryTabPageState extends State<TransactionHistoryTabPage>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  final _tabs = const [
    FadeAnimation(delay: 0.75, child: Tab(text: 'Xe qua trạm')),
    FadeAnimation(delay: 1.0, child: Tab(text: 'Nạp tiền')),
    FadeAnimation(delay: 0.5, child: Tab(text: 'Mua vé')),
    FadeAnimation(delay: 0.5, child: Tab(text: 'Phí gửi xe')),
    FadeAnimation(delay: 1.25, child: Tab(text: 'Tác động')),
    FadeAnimation(delay: 1.5, child: Tab(text: 'Khác')),
  ];

  // Xe qua trạm => Nạp tiền => Mua vé => Phí gửi xe => Tác động => Khác

  final _tabRoutes = <PageRouteInfo<void>>[
    TransactionVehicleRoute(),
    const TransactionTopupRoute(),
    TransactionTicketPurchaseRoute(),
    TransactionParkingRoute(),
    const ContractActionRoute(),
    const TransactionOtherRoute(),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return AutoTabsRouter(
      routes: _tabRoutes,
      builder: (context, child, animation) {
        final tabsRouter = AutoTabsRouter.of(context);

        return DefaultTabController(
          initialIndex: tabsRouter.activeIndex,
          length: _tabs.length,
          child: BaseTabPage(
            title: 'Lịch sử giao dịch',
            backgroundColor: Colors.white,
            gradientHeaderContainer: GradientHeaderContainer(
              height: 240.h,
              radius: 54.0,
            ),
            tabBar: GradientTabBar(
              tabBar: TabBar(
                onTap: (index) => tabsRouter.setActiveIndex(index),
                tabs: _tabs,
                isScrollable: true,
                padding: EdgeInsets.fromLTRB(16.w, 0.0, 16.w, 16.h),
                labelPadding: EdgeInsets.symmetric(horizontal: 20.w),
                overlayColor: MaterialStateProperty.all(Colors.transparent),
                indicatorSize: TabBarIndicatorSize.tab,
                indicator: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(32.0),
                ),
                labelColor: ColorName.indicatorColor,
                labelStyle: Theme.of(context).textTheme.subtitle2,
                unselectedLabelColor: ColorName.textGray1,
              ),
            ),
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: RoundedTopContainer(
                child: child,
              ),
            ),
          ),
        );
      },
    );
  }
}
