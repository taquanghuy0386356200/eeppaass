import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/commons/widgets/buttons/floating_primary_button.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/bloc/transaction_vehicle_bloc.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/transaction_vehicle_card.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/transaction_vehicle_search_form.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/transaction_vehicle_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionVehiclePage extends StatefulWidget with AutoRouteWrapper {
  final String? plateNumber;

  const TransactionVehiclePage({
    Key? key,
    this.plateNumber,
  }) : super(key: key);

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (context) => TransactionVehicleBloc(
        transactionHistoryRepository: getIt<ITransactionHistoryRepository>(),
      ),
      child: this,
    );
  }

  @override
  State<TransactionVehiclePage> createState() => _TransactionVehiclePageState();
}

class _TransactionVehiclePageState extends State<TransactionVehiclePage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);

    final timestampInFrom =
        context.read<TransactionVehicleBloc>().state.timestampInFrom;
    final timestampInTo =
        context.read<TransactionVehicleBloc>().state.timestampInTo;

    context.read<TransactionVehicleBloc>().add(widget.plateNumber == null
        ? const TransactionVehicleFetched()
        : TransactionVehicleSearched(
            timestampInFrom: timestampInFrom,
            timestampInTo: timestampInTo,
            plateNumber: widget.plateNumber,
          ));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TransactionVehicleBloc, TransactionVehicleState>(
      listener: (context, state) {
        final error = state.error;

        if (error == null) {
          _refreshController.refreshCompleted();
          _refreshController.loadComplete();
        } else {
          _refreshController.refreshFailed();
          _refreshController.loadFailed();
        }
      },
      builder: (context, state) {
        final error = state.error;

        if (state.isLoading || state.isRefreshing && state.listData.isEmpty) {
          return const TransactionVehicleShimmerLoading();
        } else if (error != null && state.listData.isEmpty) {
          return CommonErrorPage(
            message: state.error,
            onTap: () => context
                .read<TransactionVehicleBloc>()
                .add(const TransactionVehicleFetched()),
          );
        }
        final listData = state.listData;
        return Scaffold(
          backgroundColor: Colors.transparent,
          floatingActionButton: GestureDetector(
            onTap: () => _onActionButtonTapped(
              context,
              plateNumber: state.plateNumber,
              timestampInFrom: state.timestampInFrom,
              timestampInTo: state.timestampInTo,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "Tìm kiếm",
                  style: Theme.of(context).textTheme.caption!.copyWith(
                      color: Colors.orange,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  width: 6.w,
                ),
                const FloatingPrimaryButton(),
              ],
            ),
          ),
          body: state.listData.isEmpty
              ? NoDataPage(
                  needItalicStyle: true,
                  title: context.read<TransactionVehicleBloc>().isSearching
                      ? 'Bạn không có giao dịch nào phát sinh trong thời gian tìm kiếm'
                      : 'Bạn chưa có giao dịch nào phát sinh trong 7 ngày gần nhất',
                  description:
                      'Để tra cứu khoảng thời gian khác, bạn vui lòng bấm vào nút 🔍',
                )
              : SmartRefresher(
                  physics: const BouncingScrollPhysics(),
                  enablePullDown: true,
                  enablePullUp: !state.isFull,
                  onRefresh: () => context.read<TransactionVehicleBloc>().add(
                        const TransactionVehicleRefreshed(),
                      ),
                  onLoading: state.isLoadingMore
                      ? null
                      : () => context.read<TransactionVehicleBloc>().add(
                            const TransactionVehicleLoadMore(),
                          ),
                  controller: _refreshController,
                  child: ListView.separated(
                    physics: const BouncingScrollPhysics(),
                    itemCount: listData.length,
                    itemBuilder: (context, index) {
                      final item = listData[index];
                      return Padding(
                        padding: EdgeInsets.fromLTRB(
                          16.w,
                          index == 0 ? 24.h : 8.h,
                          16.w,
                          index == listData.length - 1 ? 24.h : 8.h,
                        ),
                        child: TransactionVehicleCard(
                          plateNumber: item.plateNumber ?? '',
                          timeStampIn: item.timestampIn ?? '',
                          price: item.price ?? 0,
                          stationInName: item.stationInName ?? '',
                          stationOutName: item.stationOutName,
                          sourceTransaction: item.sourceTransaction ?? '',
                          ticketTypeName: item.ticketTypeName ?? '',
                          vehicleTypeName: item.vehicleTypeName ?? '',
                        ),
                      );
                    },
                    separatorBuilder: (context, index) =>
                        SizedBox(height: 12.h),
                  ),
                ),
        );
      },
    );
  }

  void _onActionButtonTapped(
    BuildContext context, {
    required String? plateNumber,
    required DateTime timestampInFrom,
    required DateTime timestampInTo,
  }) async {
    await showBarModalBottomSheet(
      context: context,
      expand: false,
      useRootNavigator: true,
      builder: (modalContext) {
        return BlocProvider.value(
          value: context.read<TransactionVehicleBloc>(),
          child: TransactionVehicleSearchForm(
            plateNumber: plateNumber,
            timestampInFrom: timestampInFrom,
            timestampInTo: timestampInTo,
          ),
        );
      },
    );
  }
}
