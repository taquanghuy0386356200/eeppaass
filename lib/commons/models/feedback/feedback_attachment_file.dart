import 'package:json_annotation/json_annotation.dart';

part 'feedback_attachment_file.g.dart';

/// fileName : "CMND3.jpg"
/// attachmentId : 908

@JsonSerializable()
class FeedbackAttachmentFileResponse {
  FeedbackAttachmentFileResponse({
    this.fileName,
    this.attachmentId,
  });

  final String? fileName;
  final int? attachmentId;

  factory FeedbackAttachmentFileResponse.fromJson(Map<String, dynamic> json) =>
      _$FeedbackAttachmentFileResponseFromJson(json);
}

@JsonSerializable()
class FeedbackAttachmentFileRequest {
  final String fileName;
  final String base64Data;
  final double fileSize;

  FeedbackAttachmentFileRequest({
    required this.fileName,
    required this.base64Data,
    required this.fileSize,
  });

  Map<String, dynamic> toJson() => _$FeedbackAttachmentFileRequestToJson(this);

  factory FeedbackAttachmentFileRequest.fromJson(Map<String, dynamic> json) =>
      _$FeedbackAttachmentFileRequestFromJson(json);
}
