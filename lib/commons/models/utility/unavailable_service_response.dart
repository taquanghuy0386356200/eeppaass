import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'unavailable_service_response.g.dart';

@JsonSerializable()
class UnavailableServiceData {
  final List<String>? serviceCanNotAccess;

  UnavailableServiceData({
    required this.serviceCanNotAccess
  });

  factory UnavailableServiceData.fromJson(Map<String, dynamic> json) => _$UnavailableServiceDataFromJson(json);
}


@JsonSerializable()
class UnavailableServiceResponse {
  final UnavailableServiceData? data;
  final MessageData? mess;

  UnavailableServiceResponse({this.data, this.mess});

  factory UnavailableServiceResponse.fromJson(Map<String, dynamic> json) => _$UnavailableServiceResponseFromJson(json);
}
