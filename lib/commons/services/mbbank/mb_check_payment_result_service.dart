import 'dart:convert';

import 'package:epass/commons/models/mb_bank/mb_check_payment_result.dart';
import 'package:epass/commons/models/mb_bank/mb_create_order.dart';
import 'package:epass/flavors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

class MBCheckPayResultService {
  Future<MBCheckPaymentResultResponse?> checkPayResult(
      MBCheckPaymentResultRequest request, String token) async {
    try {
      /// url auth token
      String url = F.mbCheckPaymentResultUrl;

      var uuid = Uuid();

      var clientMessageId = uuid.v4();

      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
        'clientMessageId': '$clientMessageId',
      };

      var body = {
        'checkSum': '${request.checkSum}',
        'debitAccount': '${request.debitAccount}',
        'debitName': '${request.debitName}',
        'paymentResource': '${request.paymentResource}',
        'requestToken': '${request.requestToken}',
        'transactionId': '${request.transactionId}',
      };

      if (kDebugMode) {
        print('Data from MBB: ${body}');
      }
      var response = await http.post(Uri.parse(url),
          headers: headers, body: jsonEncode(body));

      if (kDebugMode) {
        print('Data from MBB: ${response.body}');
      }
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        return MBCheckPaymentResultResponse.fromJson(data);
      } else {
        return null;
      }
    } on Exception {
      return null;
    }
  }
}
