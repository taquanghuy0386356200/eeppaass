import 'package:epass/commons/api/clients/category/category_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:simple_result/simple_result.dart';

abstract class IStationRepository {
  Future<Result<StationStageDataNotNull, Failure>> getStationStages({
    String? booCode,
    int? stationType,
  });
}

class StationRepository implements IStationRepository {
  final CategoryClient _categoryClient;

  StationRepository({required CategoryClient categoryClient}) : _categoryClient = categoryClient;

  @override
  Future<Result<StationStageDataNotNull, Failure>> getStationStages({
    String? booCode,
    int? stationType,
  }) async {
    try {
      final response = await _categoryClient.getStationStages(
        booCode: booCode,
        stationType: stationType,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || count == null || listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(StationStageDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
