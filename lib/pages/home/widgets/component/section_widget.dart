import 'package:epass/pages/home/model/apps_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../model/list_app.dart';
import 'group_widget.dart';

enum SectionType { popular, appGroup }

class SectionWidget extends StatelessWidget {
  const SectionWidget(
      {Key? key,
      required this.appModel,
      required this.sectionType,
      required this.appsModel})
      : super(key: key);
  final List<ListApp> appModel;
  final sectionType;
  final AppsModel appsModel;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
      child: GroupWidget(
        appModel: appModel,
        sectionType: sectionType,
        appsModel: appsModel,
      ),
    );
  }
}
