import 'package:dio/dio.dart';
import 'package:epass/commons/models/invoice/invoice.dart';
import 'package:epass/commons/models/invoice/invoice_details_request.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/referral_client/referral_client_request.dart';
import 'package:retrofit/retrofit.dart';
import 'package:retrofit/http.dart' as http;
import 'package:epass/commons/api/endpoint.dart' as endpoint;

part 'billing_client.g.dart';

@RestApi()
abstract class BillingClient {
  factory BillingClient(
    Dio dio, {
    String baseUrl,
  }) = _BillingClient;

  /// /api/v1/cust-invoices
  @GET(endpoint.custInvoices)
  Future<InvoiceResponse> getCustInvoices({
    @Query('pageSize') int? pageSize,
    @Query('start') int? startRecord,
    @Query('contractId') required String contractId,
    @Query('plateNumber') String? plateNumber,
    @Query('invoiceNo') String? invoiceNo,
    @Query('adjustmentType') int? adjustmentType,
    @Query('transactionType') int? transactionType,
    @Query('stationInId') int? stationInId,
    @Query('stationOutId') int? stationOutId,

    /// format: MM/dd/yyyy
    @Query('from') String? from,

    /// format: MM/dd/yyyy
    @Query('to') String? to,
  });

  @POST(endpoint.downloadInvoice)
  @http.Headers({
    'Accept': 'application/json',
  })
  @DioResponseType(ResponseType.bytes)
  Future<HttpResponse<List<int>>> downloadInvoice({
    @Path('invoiceId') required String invoiceId,
    @http.Body(nullToAbsent: true) required InvoiceDetailsRequest requestBody,
  });

  @POST(endpoint.referralClient)
  Future<NoData> referralClient({
    @Body(nullToAbsent: true) required ReferralClientRequest referralRequest,
  });
}
