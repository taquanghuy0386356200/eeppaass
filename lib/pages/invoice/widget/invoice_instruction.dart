import 'package:flutter/material.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

class InvoiceInstruction extends StatelessWidget {
  const InvoiceInstruction({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(width: 6.w),
        Assets.icons.infoBlue.svg(),
        SizedBox(width: 6.w),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  left: 6.w,
                ),
                child: Text(
                  'Đăng nhập ePass bằng PC để xuất báo cáo (Excel) và tải toàn bộ hóa đơn.',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
              TextButton(
                onPressed: ()async  {
                  const String guideInvoice = 'https://epass-vdtc.com.vn/tin-tuc-su-kien/hoa-don-tong/';
                  final canLaunchGuide = await canLaunchUrlString(guideInvoice);
                  if (canLaunchGuide) {
                    await launchUrlString(
                      guideInvoice,
                      mode: LaunchMode.externalApplication,
                    );
                  }
                },
                child: Text(
                  'Xem hướng dẫn',
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        color: ColorName.blue,
                        decoration: TextDecoration.underline,
                      ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
