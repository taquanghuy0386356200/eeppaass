import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'vietmap_product.g.dart';

@JsonSerializable()
class VietMapProductResponse {
  VietMapProductData? data;
  String? error;

  VietMapProductResponse({this.data, this.error});

  factory VietMapProductResponse.fromJson(Map<String, dynamic> json) =>
      _$VietMapProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$VietMapProductResponseToJson(this);
}

@JsonSerializable()
class VietMapProductData {
  List<VietMapProduct>? results;

  VietMapProductData({this.results});

  factory VietMapProductData.fromJson(Map<String, dynamic> json) =>
      _$VietMapProductDataFromJson(json);

  Map<String, dynamic> toJson() => _$VietMapProductDataToJson(this);
}

@JsonSerializable()
class VietMapProduct {
  String? id;
  String? name;
  String? thumbnail;
  String? shortDescription;
  String? description;
  int? price;
  int? oldPrice;
  int? categoryLayoutId;
  String? productDetailUrl;

  VietMapProduct(
      {this.id,
        this.name,
        this.thumbnail,
        this.shortDescription,
        this.description,
        this.price,
        this.oldPrice,
        this.categoryLayoutId,
        this.productDetailUrl});

  factory VietMapProduct.fromJson(Map<String, dynamic> json) =>
      _$VietMapProductFromJson(json);

  Map<String, dynamic> toJson() => _$VietMapProductToJson(this);
}

