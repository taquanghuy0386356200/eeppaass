import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'viettel_pay_gate_event.dart';

part 'viettel_pay_gate_state.dart';

class ViettelPayGateBloc
    extends Bloc<ViettelPayGateEvent, ViettelPayGateState> {
  ViettelPayGateBloc() : super(ViettelPayGateInitial()) {
    on<ViettelPayPaymentCompleted>(_onViettelPayPaymentCompleted);
  }

  FutureOr<void> _onViettelPayPaymentCompleted(
    ViettelPayPaymentCompleted event,
    Emitter<ViettelPayGateState> emit,
  ) {
    switch (event.paymentStatus) {
      case 1:
        // Giao dịch thành công
        emit(ViettelPayGatePaymentCompletedSuccess(
          orderId: event.orderId,
          amount: event.transAmount,
        ));
        break;
      case 2:
      case 5:
      default:
        // Giao dịch thất bại
        final errorCode = event.errorCode;
        if (errorCode != null) {
          emit(ViettelPayGatePaymentCompletedFailure(
              'Giao dịch thất bại (Mã lỗi: $errorCode)'));
        }
        emit(const ViettelPayGatePaymentCompletedFailure('Giao dịch thất bại'));
        break;
    }
  }
}
