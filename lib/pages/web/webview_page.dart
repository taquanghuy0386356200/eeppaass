import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../home_tab/bloc/home_tabs_bloc.dart';

class WebViewPage extends StatefulWidget {
  const WebViewPage({Key? key, required this.title, required this.initUrl})
      : super(key: key);

  final String title;
  final String initUrl;

  @override
  State<WebViewPage> createState() => _WebViewPageState(title, initUrl);
}

class _WebViewPageState extends State<WebViewPage> {
  int _progress = 0;
  WebViewController? _controller;
  late String title = '';
  late String initUrl = '';

  _WebViewPageState(this.title, this.initUrl);

  @override
  void initState() {
    super.initState();
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));
    // Enable virtual display.
    if (Platform.isAndroid) {
      WebView.platform = AndroidWebView();
    } else {
      WebView.platform = CupertinoWebView();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (_controller == null) {
            return Future.value(true);
          }
          final canGoBack = await _controller!.canGoBack();
          if (canGoBack) {
            _controller?.goBack();
            return Future.value(false);
          } else {
            return Future.value(true);
          }
        },
        child: Material(
          child: BasePage(
            backgroundColor: Colors.white,
            title: title,
            leading: BackButton(
              color: Colors.white,
              onPressed: () async {
                final router = context.router;
                if (_controller == null) {
                  context
                      .read<HomeTabsBloc>()
                      .add(const HomeTabBarRequestHidden(isHidden: false));
                  router.pop();
                  return;
                }
                final canGoBack = await _controller!.canGoBack();
                if (canGoBack) {
                  _controller?.goBack();
                } else {
                  context
                      .read<HomeTabsBloc>()
                      .add(const HomeTabBarRequestHidden(isHidden: false));
                  router.pop();
                }
              },
            ),
            trailing: [
              SplashIconButton(
                icon: const Icon(CupertinoIcons.xmark, color: Colors.white),
                onTap: () {
                  _controller = null;
                  context
                      .read<HomeTabsBloc>()
                      .add(const HomeTabBarRequestHidden(isHidden: false));
                  context.popRoute();
                },
              )
            ],
            child: Stack(
              children: [
                const FadeAnimation(
                  delay: 0.5,
                  child: GradientHeaderContainer(),
                ),
                SafeArea(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _progress != 100
                          ? LinearProgressIndicator(
                              value: _progress.toDouble() / 100.0,
                              backgroundColor: ColorName.blue.withOpacity(0.3),
                              valueColor:
                                  const AlwaysStoppedAnimation(ColorName.blue),
                              minHeight: 2.h,
                            )
                          : SizedBox(height: 2.h),
                      Flexible(
                        fit: FlexFit.tight,
                        child: WebView(
                          initialUrl: initUrl,
                          javascriptMode: JavascriptMode.unrestricted,
                          gestureNavigationEnabled: true,
                          onWebViewCreated:
                              (WebViewController webViewController) {
                            _controller = webViewController;
                          },
                          onProgress: (progress) {
                            setState(() {
                              _progress = progress;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
