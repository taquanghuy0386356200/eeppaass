import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../commons/repo/app_store_repository.dart';
import '../../../commons/repo/campaign_repository.dart';
import '../../../commons/repo/user_repository.dart';
import '../../../commons/widgets/animations/fade_animation.dart';
import '../../../commons/widgets/container/gradient_header_container.dart';
import '../../../injections.dart';
import '../../bloc/app/app_bloc.dart';
import '../bloc/home_bloc.dart';
import '../model/apps_model.dart';
import '../model/list_app.dart';
import '../model/list_app_category.dart';
import 'component/app_store_widget.dart';
import 'component/section_widget.dart';

class AppGroupScreen extends StatefulWidget {
  const AppGroupScreen({Key? key, required this.appsModel}) : super(key: key);
  final AppsModel appsModel;
  @override
  State<AppGroupScreen> createState() => _AppGroupScreenState();
}

class _AppGroupScreenState extends State<AppGroupScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: BlocSelector<AppBloc, AppState, String?>(
        selector: (state) => state.user?.userName,
        builder: (context, userName) {
          return BasePage(
              backgroundColor: Colors.white,
              title: "Xin chào,$userName",
              leading: BackButton(
                color: Colors.white,
                onPressed: () async {
                  final router = context.router;
                  router.pop();
                  return;
                },
              ),
              child: Column(
                children: [
                  FadeAnimation(
                    delay: 0.5,
                    child: GradientHeaderContainer(height: 120.0.h,),
                  ),
                  Expanded(
                    child: ListView.builder(
                      padding: EdgeInsets.only(top: 30.h, left: 16.w, right: 16.w, bottom: 16.h),
                      itemBuilder: (context, indexCategory) {
                        ListAppCategory listAppCategory = widget.appsModel.listAppCategory?[indexCategory] ?? ListAppCategory();
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: indexCategory == 0 ? 0 : 22.h,
                            ),
                            Text(
                              listAppCategory.cateName ?? '',
                              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Color(0xff73777A)),
                            ),
                            SizedBox(
                              height: 6.0.h,
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 10.h),
                              decoration: const BoxDecoration(
                                  color: Colors.white, // Set border width
                                  borderRadius: BorderRadius.all(Radius.circular(16.0)), // Set rounded corner radius
                                  boxShadow: [
                                    BoxShadow(blurRadius: 16, color: Colors.black12, offset: Offset(1, 3))
                                  ] // Make rounded corner of border
                              ),
                              child: ListView.builder(
                                padding: EdgeInsets.zero,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  List<ListApp> appsModel = listAppCategory.listAppSubsCategory?[index].listApp ?? [];
                                  return Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Visibility(
                                        visible: listAppCategory.listAppSubsCategory![index].cateName != null &&
                                            listAppCategory.listAppSubsCategory![index].cateName!.isNotEmpty,
                                        child: Column(
                                          children: [
                                            SizedBox(
                                              height: 8.h,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 13.0),
                                              child: Text(
                                                listAppCategory.listAppSubsCategory![index].cateName ?? '',
                                                style: const TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.w700),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SectionWidget(
                                        appModel: appsModel,
                                        sectionType: SectionType.appGroup,
                                        appsModel: widget.appsModel,
                                      ),
                                      Visibility(
                                          visible: index != listAppCategory.listAppSubsCategory!.length - 1,
                                          child: const Padding(
                                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                                            child: Divider(),
                                          )),


                                    ],
                                  );
                                },
                                itemCount: listAppCategory.listAppSubsCategory?.length ?? 0,
                              ),
                            ),
                          ],
                        );
                      },
                      itemCount: widget.appsModel.listAppCategory?.length ?? 0,
                    ),
                  ),
                ],
              ));
        },
      ),
    );
  }
}
