part of 'file_picker_bloc.dart';

class FilePickerState extends Equatable {
  final List<String> filePaths;
  final bool isLoading;
  final String? error;

  const FilePickerState({
    this.filePaths = const [],
    this.isLoading = false,
    this.error,
  });

  @override
  List<Object?> get props => [filePaths, isLoading, error];

  FilePickerState copyWith({
    List<String>? filePaths,
    required bool? isLoading,
    required String? error,
  }) {
    return FilePickerState(
      filePaths: filePaths ?? this.filePaths,
      isLoading: isLoading ?? this.isLoading,
      error: error,
    );
  }
}
