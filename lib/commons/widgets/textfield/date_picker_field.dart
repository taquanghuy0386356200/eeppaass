import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';

class DatePickerField extends StatefulWidget {
  final DateTime? defaultDate;
  final bool enabled;
  final bool isMandatory;
  final String? labelText;
  final String? hintText;
  final bool hasClearButton;
  final DateTime? firstDate;
  final DateTime? lastDate;
  final void Function(DateTime?)? onChanged;
  final VoidCallback? onClear;
  final String? Function(String? value)? validator;

  const DatePickerField({
    Key? key,
    this.defaultDate,
    this.enabled = true,
    this.isMandatory = false,
    this.labelText,
    this.hintText,
    this.onChanged,
    this.onClear,
    this.hasClearButton = true,
    this.firstDate,
    this.lastDate,
    this.validator,
  }) : super(key: key);

  @override
  State<DatePickerField> createState() => _DatePickerFieldState();
}

class _DatePickerFieldState extends State<DatePickerField> {
  final _controller = TextEditingController();
  DateTime? _selectedDate;

  @override
  void initState() {
    _selectedDate = widget.defaultDate;
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PrimaryTextField(
      isMandatory: widget.isMandatory,
      controller: _controller..text = _selectedDate != null ? Jiffy(_selectedDate).format('dd/MM/yyyy') : '',
      enabled: widget.enabled,
      labelText: widget.labelText,
      hintText: widget.hintText,
      maxLength: 20,
      suffix: Assets.icons.calendar.svg(),
      readonly: true,
      validator: widget.validator,
      onTap: _showDatePicker,
      hasClearButton: widget.hasClearButton,
      onClear: widget.onClear,
    );
  }

  void _showDatePicker() async {
    final currentDate = Jiffy().startOf(Units.DAY).dateTime;
    final selectedDate = await showDatePicker(
      context: context,
      locale: const Locale('vi'),
      initialDate: _selectedDate ?? Jiffy().startOf(Units.DAY).dateTime,
      firstDate: widget.firstDate ?? currentDate,
      lastDate: widget.lastDate ?? Jiffy().add(years: 2).startOf(Units.DAY).dateTime,
    );

    if (selectedDate != null) {
      setState(() => _selectedDate = selectedDate);
      _controller.text = Jiffy(_selectedDate).format('dd/MM/yyyy');
    }

    widget.onChanged?.call(selectedDate);
  }
}
