import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_config_response.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http_parser/http_parser.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_get_otp_request.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_verify_otp_request.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:flutter/widgets.dart';

part 'car_doctor_event.dart';

part 'car_doctor_state.dart';

class CarDoctorBloc extends Bloc<CarDoctorEvent, CarDoctorState> {
  final IUserRepository _userRepository;
  final AppBloc _appBloc;
  late final String token;

  CarDoctorBloc({
    required IUserRepository userRepository,
    required AppBloc appBloc,
  })  : _userRepository = userRepository,
        _appBloc = appBloc,
        super(GetAccessTokenInit()) {
    on<GetAccessToken>(_onGetAccessToken);
    on<GetAnonymousAccessToken>(_onGetAnonymousAccessToken);
    on<GetCarDoctorConfig>(_onGetCarDoctorConfig);

    String clientId = dotenv.env['CLIENTID']!;
    String clientSecret = dotenv.env['CLIENTSECRET']!;
    token = 'BASIC ${base64.encode(utf8.encode('$clientId:$clientSecret'))}';
  }

  FutureOr<void> _onGetAccessToken(
      GetAccessToken event, Emitter<CarDoctorState> emit) async {
    final contractId = _appBloc.state.user?.contractId ?? '';
    final name = "${_appBloc.state.user?.userName}";
    final phone = _appBloc.state.user?.phone ?? '';
    final identifier = _appBloc.state.user?.identifier ?? '';
    final source = CarDoctorGetOtpRequest(
        contractId: contractId,
        name: name,
        phone: phone,
        identifier: identifier);
    // print("CarDoctor: " + jsonEncode(source));

    final resultGetOtp =
        await getCarDoctorOtp(token: token, source: jsonEncode(source));
    if (resultGetOtp.isNotEmpty) {
      final request =
          CarDoctorVerifyOtpRequest(username: phone, password: resultGetOtp);
      final result = await _userRepository.verifyCarDoctorOtp(
          token: token, request: request);

      result.when(
        success: (response) {
          emit(GetAccessTokenSuccess(
              response.access_token ?? '', response.expires_in ?? 0));
        },
        failure: (failure) => emit(const GetAccessTokenFail('Có lỗi xảy ra')),
      );
    }
  }

  FutureOr<void> _onGetAnonymousAccessToken(
      GetAnonymousAccessToken event, Emitter<CarDoctorState> emit) async {
    final result = await _userRepository.getCarDoctorAccessToken(token: token);
    result.when(
      success: (response) {
        emit(GetAccessTokenSuccess(
            response.access_token ?? '', response.expires_in ?? 0));
      },
      failure: (failure) => emit(const GetAccessTokenFail('Có lỗi xảy ra')),
    );
  }

  Future<String> getCarDoctorOtp({
    required token,
    required source,
  }) async {
    final dio = Dio();
    const extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final headers = <String, dynamic>{r'Authorization': token};
    headers.removeWhere((k, v) => v == null);
    headers['accept-language'] = 'vi';
    final data = FormData();
    data.files.add(MapEntry(
        'source',
        MultipartFile.fromString(source,
            contentType: MediaType.parse('application/json'))));
    final result = await dio.fetch<String>(_setStreamType<String>(
        Options(method: 'POST', headers: headers, extra: extra)
            .compose(dio.options, '/cms/driver/from-ePass',
                queryParameters: queryParameters, data: data)
            .copyWith(baseUrl: dotenv.env['APIURL']!)));
    final value = result.data!;
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }

  FutureOr<void> _onGetCarDoctorConfig(
      GetCarDoctorConfig event, Emitter<CarDoctorState> emit) async {
    emit(GetConfigSuccess(new CarDoctorConfig()));
    // final result = await _userRepository.getCarDoctorConfig();
    // result.when(
    //   success: (response) {
    //     final config = response
    //         ?.firstWhere((element) => element.code == 'CARDOCTOR_SERVICE');
    //     if (config != null) {
    //       emit(GetConfigSuccess(config));
    //     } else {
    //       emit(GetConfigFail());
    //     }
    //   },
    //   failure: (failure) => emit(GetConfigFail()),
    // );
  }
}
