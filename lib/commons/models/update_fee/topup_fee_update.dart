import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:json_annotation/json_annotation.dart';

/// topupChannelCode : "DomesticATM"
/// topupChannelName : "Thẻ ATM nội địa"
/// valueMin : 1
/// serviceFeeType : 1
/// serviceFeeValAdd : 880
/// serviceFeeVal : 0.0066

part 'topup_fee_update.g.dart';

@JsonSerializable()
class TopUpFeeUpdate {
  TopUpFeeUpdate(
      {this.topupChannelCode,
      this.topupChannelName,
      this.valueMin,
      this.valueMax,
      this.serviceFeeType,
      this.serviceFeeValAdd,
      this.serviceFeeVal,
      this.groupName,
      this.groupImage,
      this.containButton = false,
      this.image,
      this.url});

  final String? topupChannelCode;
  final String? topupChannelName;
  final int? valueMin;
  final int? valueMax;
  final int? serviceFeeType;
  final int? serviceFeeValAdd;
  final double? serviceFeeVal;
  final String? groupName;
  final String? groupImage;
  final bool containButton;
  final String? image;
  final String? url;

  factory TopUpFeeUpdate.fromJson(Map<String, dynamic> json) =>
      _$TopUpFeeUpdateFromJson(json);
}

@JsonSerializable()
class TopupFeeDataUpdate {
  final String? groupName;
  final String? groupImage;
  final bool containButton;
  final List<TopUpFeeUpdate>? channels;

  TopupFeeDataUpdate(
      {this.groupName,
      this.groupImage,
      this.containButton = false,
      this.channels});

  factory TopupFeeDataUpdate.fromJson(Map<String, dynamic> json) =>
      _$TopupFeeDataUpdateFromJson(json);
}

@JsonSerializable()
class TopupFeeUpdateResponse {
  final List<TopupFeeDataUpdate>? data;
  final MessageData? mess;

  TopupFeeUpdateResponse({this.data, this.mess});

  factory TopupFeeUpdateResponse.fromJson(Map<String, dynamic> json) =>
      _$TopupFeeUpdateResponseFromJson(json);
}
