part of 'verify_card_bloc.dart';

abstract class VerifyCardEvent extends Equatable {
  const VerifyCardEvent();
}

// class VerifyConfigFetched extends VerifyCardEvent {
//   final VerifyConfig verifyConfig;
//   final List<VerifyConfigModel> serviceVerifyConfig;
//
//   const VerifyConfigFetched({
//     required this.verifyConfig,
//     required this.serviceVerifyConfig
//   });
//
//   @override
//   List<Object?> get props => [verifyConfig, serviceVerifyConfig];
// }

class VerifyConfigAlerted extends VerifyCardEvent {
  final bool? isOn;
  final int? serviceId;
  const VerifyConfigAlerted({this.isOn, this.serviceId});

  @override
  List<Object?> get props => [isOn, serviceId];
}
