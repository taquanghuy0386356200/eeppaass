import 'package:flutter/material.dart';

class PhotoViewModal extends StatelessWidget {
  final String? title;
  final Widget child;

  const PhotoViewModal({
    Key? key,
    this.title,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: CloseButton(
          onPressed: Navigator.of(context).pop,
          color: Colors.white,
        ),
      ),
      body: child,
    );
  }
}
