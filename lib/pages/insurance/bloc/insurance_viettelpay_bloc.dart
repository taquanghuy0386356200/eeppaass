import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/insurance_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'insurance_viettlelpay_event.dart';

part 'insurance_viettelpay_state.dart';

class InsuranceViettelPayBloc extends Bloc<InsuranceViettelPayEvent, InsuranceViettelPayState> {
  final IInsuranceRepository _insuranceRepository;

  InsuranceViettelPayBloc({required IInsuranceRepository insuranceRepository})
      : _insuranceRepository = insuranceRepository,
        super(InsuranceViettelPayInitial()) {
    on<CountInsuranceFetched>(_onCountInsuranceFetched);
  }

  FutureOr<void> _onCountInsuranceFetched(
    CountInsuranceFetched event,
    Emitter<InsuranceViettelPayState> emit,
  ) async {
    emit(const CountInsuranceFetchedInProgress());

    final result = await _insuranceRepository.getCountInsurance();

    result.when(
      success: (count) {
        if (count > 0) {
          return emit(InsuranceServiceAccepted(count));
        }
        return emit(const InsuranceServiceAccessRequested());
      },
      failure: (failure) => emit(
          CountInsuranceFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
