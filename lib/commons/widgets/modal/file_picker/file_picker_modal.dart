import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_source.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FileResourceModal extends StatelessWidget {
  const FileResourceModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoActionSheet(
      // title: const Text('Thao tác'),
      // message: const Text('Message'),
      actions: <CupertinoActionSheetAction>[
        CupertinoActionSheetAction(
          child: Text(
            'Chụp ảnh',
            style: Theme.of(context).textTheme.button!.copyWith(
                  color: Colors.orange,
                  fontSize: 18.sp,
                ),
          ),
          onPressed: () {
            context.read<FilePickerBloc>().add(const FilePicked(source: FileSource.camera));
            Navigator.of(context).pop();
          },
        ),
        CupertinoActionSheetAction(
          child: Text(
            'Thư viện hình ảnh',
            style: Theme.of(context).textTheme.button!.copyWith(
                  color: Colors.orange,
                  fontSize: 18.sp,
                ),
          ),
          onPressed: () {
            context.read<FilePickerBloc>().add(const FilePicked(source: FileSource.gallery));
            Navigator.of(context).pop();
          },
        ),
        CupertinoActionSheetAction(
          child: Text(
            'Tệp tin',
            style: Theme.of(context).textTheme.button!.copyWith(
                  color: Colors.orange,
                  fontSize: 18.sp,
                ),
          ),
          onPressed: () {
            context.read<FilePickerBloc>().add(const FilePicked(source: FileSource.file));
            Navigator.of(context).pop();
          },
        ),
      ],
      cancelButton: CupertinoActionSheetAction(
        onPressed: Navigator.of(context).pop,
        child: Text(
          'Huỷ',
          style: Theme.of(context).textTheme.button!.copyWith(
                fontSize: 18.sp,
              ),
        ),
      ),
    );
  }
}

Future<void> showFilePickerModal(BuildContext context) async {
  await showCupertinoModalPopup(
    context: context,
    builder: (_) => BlocProvider.value(
      value: context.read<FilePickerBloc>(),
      child: const FileResourceModal(),
    ),
  );
}
