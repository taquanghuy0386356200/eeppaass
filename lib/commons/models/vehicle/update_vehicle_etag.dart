import 'package:json_annotation/json_annotation.dart';

/// custId : "1000966"
/// reasonId : "42"
/// actTypeId : 20

part 'update_vehicle_etag.g.dart';

@JsonSerializable()
class UpdateVehicleEtag {
  UpdateVehicleEtag({
    this.custId,
    this.reasonId,
    this.actTypeId = 20,
  });

  final String? custId;
  final String? reasonId;
  final int actTypeId;

  factory UpdateVehicleEtag.fromJson(Map<String, dynamic> json) =>
      _$UpdateVehicleEtagFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateVehicleEtagToJson(this);
}
