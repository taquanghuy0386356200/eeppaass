part of 'cash_in_fee_bloc.dart';

@immutable
abstract class CashInFeeState extends Equatable {
  const CashInFeeState();
}

class CashInFeeInitial extends CashInFeeState {
  @override
  List<Object> get props => [];
}

class CashInFeeFetchedInProgress extends CashInFeeState {
  @override
  List<Object> get props => [];
}

class CashInFeeFetchedSuccess extends CashInFeeState {
  final List<TopupFeeDataUpdate> fees;

  const CashInFeeFetchedSuccess(this.fees);

  @override
  List<Object> get props => [fees];
}

class CashInFeeFetchedFailure extends CashInFeeState {
  final String message;

  const CashInFeeFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}
