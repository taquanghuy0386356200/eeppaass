import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/widgets/textfield/cupertino_picker_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_type_picker/feedback_type_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FeedbackTypePicker extends StatelessWidget {
  final FeedbackType? selectedFeedbackType;
  final bool required;
  final ValueChanged<PickerItemWithValue<FeedbackType>?>? onItemChanged;

  const FeedbackTypePicker({
    Key? key,
    this.selectedFeedbackType,
    this.onItemChanged,
    this.required = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FeedbackTypeBloc(feedbackRepository: getIt<IFeedbackRepository>()),
      child: _FeedbackTypePicker(
        selectedFeedbackType: selectedFeedbackType,
        onItemChanged: onItemChanged,
        required: required,
      ),
    );
  }
}

class _FeedbackTypePicker extends StatefulWidget {
  final FeedbackType? selectedFeedbackType;
  final ValueChanged<PickerItemWithValue<FeedbackType>?>? onItemChanged;
  final bool required;

  const _FeedbackTypePicker({
    Key? key,
    this.selectedFeedbackType,
    this.onItemChanged,
    required this.required,
  }) : super(key: key);

  @override
  State<_FeedbackTypePicker> createState() => _FeedbackTypePickerState();
}

class _FeedbackTypePickerState extends State<_FeedbackTypePicker> {
  PickerItemWithValue<FeedbackType>? _selectedFeedbackType;

  @override
  void initState() {
    context.read<FeedbackTypeBloc>().add(const FeedbackTypeFetched(
          feedbackChannel: FeedbackChannel.suggestion,
        ));

    if (widget.selectedFeedbackType != null) {
      _selectedFeedbackType = PickerItemWithValue(
        title: widget.selectedFeedbackType!.name ?? '',
        value: widget.selectedFeedbackType!,
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FeedbackTypeBloc, FeedbackTypeState>(
      builder: (context, state) {
        List<PickerItemWithValue<FeedbackType>> items = [];
        if (state is FeedbackTypeFetchedSuccess) {
          items = state.feedbackTypes.map((e) => PickerItemWithValue(title: e.name ?? '', value: e)).toList();
        }
        return CupertinoPickerField(
          isMandatory: widget.required,
          items: items,
          defaultItem: _selectedFeedbackType,
          isLoading: state is FeedbackTypeFetchedInProgress,
          validator: widget.required
              ? (_) {
                  if (_selectedFeedbackType == null) {
                    return 'Vui lòng chọn Chủ đề góp ý';
                  }
                  return null;
                }
              : null,
          hintText: 'Chọn chủ đề góp ý',
          enabled: state is! FeedbackTypeFetchedInProgress,
          label: 'Chủ đề góp ý',
          errorText: state is FeedbackTypeFetchedFailure ? state.message : null,
          onItemChanged: (PickerItemWithValue<FeedbackType>? selectedItem) {
            setState(() => _selectedFeedbackType = selectedItem);
            widget.onItemChanged?.call(selectedItem);
          },
        );
      },
    );
  }
}
