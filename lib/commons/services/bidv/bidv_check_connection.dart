import 'dart:convert';

import 'package:epass/commons/services/bidv/bidv_check_connection_request.dart';
import 'package:epass/commons/services/bidv/bidv_check_connection_response.dart';
import 'package:epass/commons/services/zalopay/zalo_pay_constant.dart';
import 'package:simple_result/simple_result.dart';
import '../../../flavors.dart';
import '../../api/failures/failures.dart';
import '../../utils/zalo_pay/zalo_pay_utils.dart';
import 'package:sprintf/sprintf.dart';

import '../vnpay/vnpay_payment_response.dart';
import 'package:crypto/crypto.dart';
import 'package:http/http.dart';
import 'package:jiffy/jiffy.dart';
import 'package:uuid/uuid.dart';
import 'package:xml/xml.dart' as xml;

class BidvCheckingConnectionService {
  Future<Result<BidvCheckingConnectionResponse, Failure>>
      checkingConnectionBidv(
    BidvCheckingConnectionRequest request,
  ) async {
    try {
      var uuid = const Uuid();

      String transId = Jiffy().format('yyyyMMddHHmmssSSS');
      //String transId = uuid.v1();

      String transDate = Jiffy().format('YYMMDD');

      const privateKey = 'BIDV_EPASS#123';

      final moreInfo = '';
      final serviceID = request.serviceID;
      final merchantCode = request.merchantCode;
      final transDes = request.transDes;
      final transAmount = request.transAmount;
      final curr = request.curr;
      final payerId = request.payerId;
      final payerName = request.payerName;
      final payerAdd = request.payerAdd;
      final type = request.type;
      final customerId = request.customerId;
      final customerName = request.customerName;
      final issueDate = request.issueDate;
      final channelId = request.channelId;
      final linkType = request.linkType;
      final otpNumber = request.otpNumber;
      final merchantName = request.merchantName;

      // Encode your message with the private key
      var message = '$privateKey|'
          '$serviceID|'
          '$merchantCode|'
          '${request.merchantName.isNotEmpty ? request.merchantName : ''}|'
          '${transDate.isNotEmpty ? transDate : ''}|'
          '$transId|'
          '$transDes|'
          '$transAmount|'
          '$curr|'
          '$payerId|'
          '$payerName|'
          '$payerAdd|'
          '$type|'
          '$customerId|'
          '$customerName|'
          '$issueDate|'
          '$channelId|'
          '$linkType|'
          '$otpNumber|'
          '$moreInfo';
      final secureCode = md5.convert(utf8.encode(message)).toString();
      String val1 = '<ncc:Service_Id>$serviceID</ncc:Service_Id>' +
          '<ncc:Merchant_Id>$merchantCode</ncc:Merchant_Id>' +
          '<ncc:Merchant_Name>Nap tien vao tai khoan</ncc:Merchant_Name>' +
          '<ncc:Trandate>$transDate</ncc:Trandate>' +
          '<ncc:Trans_Id>$transId</ncc:Trans_Id>' +
          '<ncc:Trans_Desc>$transDes</ncc:Trans_Desc>' +
          '<ncc:Amount>500000</ncc:Amount>' +
          '<ncc:Curr>VND</ncc:Curr>' +
          '<ncc:Payer_Id>$payerId</ncc:Payer_Id>' +
          '<ncc:Payer_Name>$payerName</ncc:Payer_Name>' +
          '<ncc:Payer_Addr/>' +
          '<ncc:Type/>' +
          '<ncc:Custmer_Id/>' +
          '<ncc:Customer_Name/>' +
          '<ncc:IssueDate/>' +
          '<ncc:Channel_Id>$channelId</ncc:Channel_Id>' +
          '<ncc:Link_Type>$linkType</ncc:Link_Type>' +
          '<ncc:Otp_Number/>' +
          '<ncc:More_Info>$moreInfo</ncc:More_Info>';
      Response response = await post(
        Uri.parse(F.bidvPayGateUrl),
        headers: {
          'content-type': 'text/xml; charset=utf-8',
          'SOAPAction': '/NCC_WSDL.serviceagent//checkLink',
        },
        body: utf8.encode('''<?xml version="1.0" encoding="utf-8"?>
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ncc="NCCWalletInput_Schema">
    <soapenv:Header/>
    <soapenv:Body>
      <ncc:root>
         <ncc:Service_Id>$serviceID</ncc:Service_Id>
         <ncc:Merchant_Id>$merchantCode</ncc:Merchant_Id>
         <ncc:Merchant_Name>$merchantName</ncc:Merchant_Name>
         <ncc:Trandate>$transDate</ncc:Trandate>
         <ncc:Trans_Id>$transId</ncc:Trans_Id>
         <ncc:Trans_Desc>$transDes</ncc:Trans_Desc>
         <ncc:Amount>$transAmount</ncc:Amount>
         <ncc:Curr/>
         <ncc:Payer_Id>$payerId</ncc:Payer_Id>
         <ncc:Payer_Name>$payerName</ncc:Payer_Name>
         <ncc:Payer_Addr/>
         <ncc:Type/>
         <ncc:Custmer_Id/>
         <ncc:Customer_Name/>
         <ncc:IssueDate/>
         <ncc:Channel_Id>$channelId</ncc:Channel_Id>
         <ncc:Link_Type>$linkType</ncc:Link_Type>
         <ncc:Otp_Number/>
         <ncc:More_Info>$moreInfo</ncc:More_Info>
         <ncc:Secure_Code>$secureCode</ncc:Secure_Code>
         <ncc:FromProcess/>
      </ncc:root>
    </soapenv:Body>
    </soapenv:Envelope>'''),
      );

      final xmlString = response.body;
      final xml.XmlDocument document = xml.XmlDocument.parse(xmlString);

      final bidvResponse = BidvCheckingConnectionResponse.fromMap(document);

      return Result.success(bidvResponse);
    } catch (error) {
      return Result.failure(UnknownFailure(message: error.toString()));
    }
  }
}
