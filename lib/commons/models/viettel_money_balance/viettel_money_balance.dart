import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// orderId : "1653453063466V0000081878187"
/// contractId : "8187"
/// msisdn : "84984468800"
/// bankCode : "MB"
/// balanceAmount : 8009720

part 'viettel_money_balance.g.dart';

@JsonSerializable()
class ViettelMoneyBalance {
  ViettelMoneyBalance({
    this.orderId,
    this.contractId,
    this.msisdn,
    this.bankCode,
    this.balanceAmount,
  });

  final String? orderId;
  final String? contractId;
  final String? msisdn;
  final String? bankCode;
  final int? balanceAmount;

  factory ViettelMoneyBalance.fromJson(Map<String, dynamic> json) => _$ViettelMoneyBalanceFromJson(json);
}

@JsonSerializable()
class ViettelMoneyBalanceResponse {
  final ViettelMoneyBalance? data;
  final MessageData? mess;

  ViettelMoneyBalanceResponse({this.data, this.mess});

  factory ViettelMoneyBalanceResponse.fromJson(Map<String, dynamic> json) =>
      _$ViettelMoneyBalanceResponseFromJson(json);
}
