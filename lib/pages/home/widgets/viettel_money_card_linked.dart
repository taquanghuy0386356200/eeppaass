import 'package:epass/commons/models/contract_payment/contract_payment.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/home/bloc/viettel_money_link_bloc.dart';
import 'package:epass/pages/home/widgets/balance_viettel_money_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViettelMoneyCardLinked extends StatelessWidget {
  final ContractPayment contractPayment;

  const ViettelMoneyCardLinked({
    Key? key,
    required this.contractPayment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10.w, 24.h, 20.w, 18.h),
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(
                  children: [
                    contractPayment.methodRechargeCode == 'MB'
                        ? Assets.icons.viettelpay.svg(width: 44.r, height: 44.r)
                        : Assets.icons.mobileMoney.svg(width: 44.r, height: 44.r),
                    SizedBox(width: 10.w),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Tài khoản ${contractPayment.methodRechargeCode == 'MB' ? 'Viettel Pay' : 'Tiền di động'}',
                            style: Theme.of(context).textTheme.bodyText2!.copyWith(color: ColorName.borderColor),
                          ),
                          SizedBox(height: 2.h),
                          Text(
                            contractPayment.accountOwner ?? '',
                            style: Theme.of(context).textTheme.headline5!.copyWith(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20.sp,
                                ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20.h),
              Padding(
                padding: EdgeInsets.only(left: 10.w),
                child: Text(
                  'Số dư ví:',
                  style: Theme.of(context).textTheme.subtitle2!.copyWith(
                        fontWeight: FontWeight.w600,
                        color: ColorName.borderColor,
                        fontSize: 15.sp,
                      ),
                ),
              ),
              SizedBox(height: 2.h),
              Row(
                children: [
                  const BalanceViettelMoneyWidget(),
                  const Spacer(),
                  TextButton(
                    onPressed: () {
                      final contractId = contractPayment.contractId;
                      final token = contractPayment.token;
                      final accountNumber = contractPayment.accountNumber;

                      if (contractId != null && token != null && accountNumber != null) {
                        context.read<ViettelMoneyLinkBloc>().add(
                              ViettelMoneyUnlinkAccount(
                                accountNumber: accountNumber,
                                contractId: contractId.toString(),
                                token: token,
                              ),
                            );
                      }
                    },
                    child: Text(
                      'Huỷ liên kết',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: ColorName.borderColor,
                          ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
