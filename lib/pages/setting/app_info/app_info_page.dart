import 'dart:io';

import 'package:epass/commons/constant.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher_string.dart';

class AppInfoPage extends StatelessWidget {
  const AppInfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: 'Thông tin ứng dụng',
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          const GradientHeaderContainer(),
          SafeArea(
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: FadeAnimation(
                direction: FadeDirection.up,
                delay: 1,
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 36.h, 16.w, 32.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Assets.images.logoAlt.svg(
                          height: 60.r,
                          width: 60.r,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                      SizedBox(height: 16.h),
                      FutureBuilder<PackageInfo>(
                        future: PackageInfo.fromPlatform(),
                        builder: (context, snapshot) {
                          final info = snapshot.data;
                          if (snapshot.hasData && info != null) {
                            return Center(
                              child: Text(
                                'Phiên bản ${info.version}',
                                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            );
                          }
                          return const SizedBox.shrink();
                        },
                      ),
                      SizedBox(height: 32.h),
                      Text(
                        'Công ty Cổ phần Giao thông số Việt Nam',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      SizedBox(height: 24.h),
                      Text(
                        'Địa chỉ văn phòng: Tầng 15, toà nhà Cục Tần Số,'
                        ' 115 Trần Duy Hưng, quận Cầu Giấy, Hà Nội',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      SizedBox(height: 24.h),
                      Text(
                        'Giấy chứng nhận Đăng ký Kinh doanh số: 0109266456 do'
                        ' Sở Kế hoạch và Đầu tư Thành phố Hà Nội cấp lần đầu'
                        ' ngày 14/07/2020',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      SizedBox(height: 24.h),
                      FutureBuilder<bool>(
                        future: canLaunchUrlString('mailto:${Constant.csMail}'),
                        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                          final canLaunch = snapshot.hasData && snapshot.data;
                          return RichText(
                            text: TextSpan(
                              style: Theme.of(context).textTheme.bodyText1,
                              children: [
                                const TextSpan(text: 'Email: '),
                                TextSpan(
                                  text: Constant.csMail,
                                  style: canLaunch
                                      ? Theme.of(context).textTheme.bodyText1!.copyWith(
                                            color: ColorName.linkBlue,
                                            decoration: TextDecoration.underline,
                                          )
                                      : Theme.of(context).textTheme.bodyText1,
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = canLaunch
                                        ? () async {
                                            await launchUrlString('mailto:${Constant.csMail}');
                                          }
                                        : null,
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      SizedBox(height: 8.h),
                      FutureBuilder<bool>(
                        future: canLaunchUrlString('tel:${Constant.hotline}'),
                        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                          final canLaunch = snapshot.hasData && snapshot.data;
                          return RichText(
                            text: TextSpan(
                              style: Theme.of(context).textTheme.bodyText1,
                              children: [
                                const TextSpan(text: 'Hotline: '),
                                TextSpan(
                                  text: Constant.hotline,
                                  style: canLaunch
                                      ? Theme.of(context).textTheme.bodyText1!.copyWith(
                                            color: ColorName.linkBlue,
                                            decoration: TextDecoration.underline,
                                          )
                                      : Theme.of(context).textTheme.bodyText1,
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = canLaunch
                                        ? () async {
                                            await launchUrlString('tel:${Constant.hotline}');
                                          }
                                        : null,
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      SizedBox(height: 24.h),
                      SizedBox(
                        height: 56.h,
                        width: double.infinity,
                        child: SecondaryButton(
                          title: 'Bình chọn cho ePass',
                          onTap: () => _onRateAppTap(context),
                        ),
                      ),
                      SizedBox(height: 8.h),
                      SizedBox(
                        height: 56.h,
                        width: double.infinity,
                        child: SecondaryButton(
                          title: 'Điều khoản sử dụng',
                          onTap: () async {
                            await launchUrlString('https://epass-vdtc.com.vn/dieu-khoan');
                          },
                        ),
                      ),
                      SizedBox(height: 8.h),
                      SizedBox(
                        height: 56.h,
                        width: double.infinity,
                        child: SecondaryButton(
                          title: 'Chính sách bảo mật',
                          onTap: () async {
                            await launchUrlString('https://epass-vdtc.com.vn/chinh-sach-bao-mat');
                          },
                        ),
                      ),
                      SizedBox(height: 18.h),
                      Center(
                        child: Assets.images.bctCheckmark.image(
                          height: 56.h,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _onRateAppTap(BuildContext context) async {
    if (Platform.isIOS) {
      try {
        await launchUrlString('https://apps.apple.com/vn/app/epass/id1535426844');
      } on Exception {
        showErrorSnackBBar(
          context: context,
          message: 'Có lỗi xảy ra',
        );
      }
    } else if (Platform.isAndroid) {
      try {
        await launchUrlString('market://details?id=com.viettel.etc.epass');
      } on PlatformException {
        await launchUrlString('https://play.google.com/store/apps/details?id=com.viettel.etc.epass');
      } on Exception {
        showErrorSnackBBar(
          context: context,
          message: 'Có lỗi xảy ra',
        );
      }
    }
  }
}
