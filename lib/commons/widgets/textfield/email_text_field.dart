import 'package:epass/commons/validators/email_validator.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:flutter/material.dart';

class EmailTextField extends StatefulWidget {
  final TextEditingController controller;
  final ValueChanged<String?>? onChanged;
  final bool isMandatory;

  const EmailTextField({
    Key? key,
    required this.controller,
    this.isMandatory = false,
    this.onChanged,
  }) : super(key: key);

  @override
  State<EmailTextField> createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> with EmailValidator {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PrimaryTextField(
      controller: widget.controller,
      onChanged: widget.onChanged,
      labelText: 'Email',
      hintText:  'example@domain.com',
      validator: emailValidator,
      isMandatory: widget.isMandatory,
      maxLength: 100,
    );
  }
}
