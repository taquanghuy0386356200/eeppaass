import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:camera/camera.dart';
import 'package:epass/commons/models/ocr_id_card/id_card_ocr.dart';
import 'package:epass/commons/repo/register_repository.dart';
import 'package:epass/commons/services/image/image_processor.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/register/bloc/register_paper_scan/register_id_card_bloc.dart';
import 'package:epass/pages/register/bloc/register_paper_scan/register_paper_scan_bloc.dart';
import 'package:epass/pages/register/pages/register_customer/widgets/camera_button.dart';
import 'package:epass/pages/register/pages/register_customer/widgets/scan_id_notices.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_animations/simple_animations.dart';

class RegisterPaperScanPage extends StatefulWidget {
  final bool isFront;

  const RegisterPaperScanPage({Key? key, required this.isFront}) : super(key: key);

  @override
  State<RegisterPaperScanPage> createState() => _RegisterPaperScanPageState();
}

class _RegisterPaperScanPageState extends State<RegisterPaperScanPage> {
  late List<CameraDescription> _cameras;
  CameraController? _cameraController;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _initCamera();
  }

  Future<void> _initCamera() async {
    _cameras = await availableCameras();
    _cameraController = CameraController(_cameras[0], ResolutionPreset.veryHigh);
    _cameraController?.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    _cameraController?.dispose();
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterPaperScanBloc(registerRepository: getIt<IRegisterRepository>()),
      child: BlocConsumer<RegisterPaperScanBloc, RegisterPaperScanState>(
        listener: (context, state) async {
          if (state is RegisterPaperScannedSuccess) {
            final bloc = context.read<RegisterIdCardBloc>();

            await _cameraController?.resumePreview();

            // Back to previous screen when success
            _timer = Timer(const Duration(seconds: 2), () => context.popRoute());

            bloc.add(RegisterIdCardUpdated(
              idCard: state.idCard,
              front: state.front,
              back: state.back,
            ));
          } else if (state is RegisterPaperScannedFailure) {
            await _cameraController?.resumePreview();
            await showDialog(
              context: context,
              builder: (dialogContext) {
                return ConfirmDialog(
                  title: 'Ảnh chụp không hợp lệ',
                  image: Assets.icons.scanFailure.svg(),
                  primaryButtonTitle: 'Chụp lại',
                  onPrimaryButtonTap: () {
                    Navigator.of(dialogContext).pop();
                    context.read<RegisterPaperScanBloc>().add(RegisterPaperScannedReset());
                  },
                  secondaryButtonTitle: 'Quay lại',
                  onSecondaryButtonTap: () {
                    Navigator.of(dialogContext).pop();
                    context.popRoute();
                  },
                );
              },
            );
          }
        },
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: state is! RegisterPaperScannedInProgress
                  ? BackButton(onPressed: context.popRoute, color: Colors.white)
                  : null,
            ),
            extendBodyBehindAppBar: true,
            body: Builder(
              builder: (context) {
                if (_cameraController == null || !_cameraController!.value.isInitialized) {
                  return const Center(
                    child: CircularProgressIndicator.adaptive(),
                  );
                }

                var camera = _cameraController!.value;
                // fetch screen size
                final size = MediaQuery.of(context).size;

                // calculate scale depending on screen and camera ratios
                // this is actually size.aspectRatio / (1 / camera.aspectRatio)
                // because camera preview size is received as landscape
                // but we're calculating for portrait orientation
                var scale = size.aspectRatio * camera.aspectRatio;

                // to prevent scaling down, invert the value
                if (scale < 1) scale = 1 / scale;

                double centerMargin = 72.h;
                double width = size.shortestSide * .9;
                const ratio = 1.59;
                double height = width / ratio;

                return Stack(
                  children: [
                    Center(
                      child: Transform.scale(
                        scale: scale,
                        child: CameraPreview(_cameraController!),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        width: width,
                        height: height,
                        margin: EdgeInsets.only(bottom: centerMargin),
                        decoration: state is! RegisterPaperScannedInProgress
                            ? ShapeDecoration(
                                color: Colors.transparent,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  side: const BorderSide(width: 1, color: Colors.white),
                                ),
                              )
                            : null,
                        child: state is RegisterPaperScannedInProgress
                            ? const CircularProgressIndicator.adaptive()
                            : state is RegisterPaperScannedSuccess
                                ? PlayAnimation<double>(
                                    curve: Curves.easeOut,
                                    tween: Tween(begin: 0.0, end: 1.0),
                                    duration: const Duration(milliseconds: 200),
                                    builder: (context, child, value) {
                                      return Opacity(
                                        opacity: value,
                                        child: Center(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Assets.icons.scanSuccess.svg(),
                                              SizedBox(height: 24.h),
                                              Text(
                                                'Xác minh mặt ${widget.isFront ? 'trước' : 'sau'} thành công',
                                                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.bold,
                                                    ),
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  )
                                : const SizedBox.shrink(),
                      ),
                    ),
                    ColorFiltered(
                      colorFilter: const ColorFilter.mode(Colors.black54, BlendMode.srcOut),
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colors.transparent,
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: width,
                            height: width / ratio,
                            margin: EdgeInsets.only(bottom: centerMargin),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    state is RegisterPaperScanInitial
                        ? SafeArea(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.w),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Chụp ảnh giấy tờ ${widget.isFront ? 'mặt trước' : 'mặt sau'}',
                                    style: Theme.of(context).textTheme.headline6!.copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22.sp,
                                        ),
                                  ),
                                  SizedBox(height: 8.h),
                                  Text(
                                    'Vui lòng đưa giấy tờ vào khung',
                                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                          color: Colors.white,
                                        ),
                                  ),
                                  const Spacer(),
                                  CameraButton(
                                    onTap: () async => await _onCameraTapped(
                                      context: context,
                                      scale: scale,
                                      centerMargin: centerMargin,
                                      ratio: ratio,
                                    ),
                                  ),
                                  SizedBox(height: 72.h),
                                  const ScanIDNotices(),
                                  SizedBox(height: 32.h),
                                ],
                              ),
                            ),
                          )
                        : const SizedBox.shrink(),
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }

  Future<void> _onCameraTapped({
    required BuildContext context,
    required double ratio,
    required double centerMargin,
    required double scale,
  }) async {
    try {
      await _cameraController?.pausePreview();
      final file = await _cameraController!.takePicture();

      final result = await ImageProcessor.cropImage(
        srcPath: file.path,
        destPath: file.path,
        screenWidth: ScreenUtil().screenWidth * (ScreenUtil().pixelRatio ?? 1),
        screenHeight: ScreenUtil().screenHeight * (ScreenUtil().pixelRatio ?? 1),
        ratio: ratio,
        offsetY: -(centerMargin * (ScreenUtil().pixelRatio ?? 1) * scale),
      );

      await result.when(
        success: (path) async {
          context.read<RegisterPaperScanBloc>().add(
                RegisterPaperScanned(
                  filePath: path,
                  side: widget.isFront ? IdType.front : IdType.back,
                ),
              );
        },
        failure: (failure) async {
          await _cameraController?.resumePreview();
          return showErrorSnackBBar(context: context, message: failure.message ?? 'Có lỗi xảy ra');
        },
      );
    } on CameraException catch(e) {
      await _cameraController?.resumePreview();
      if(e.code!='Previous capture has not returned yet.'){
        showErrorSnackBBar(context: context, message: 'Có lỗi xảy ra');
      }
    }
  }
}
