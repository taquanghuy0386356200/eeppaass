import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/link_viet_map_request/link_viet_map_request.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter/widgets.dart';

part 'link_viet_map_event.dart';
part 'link_viet_map_state.dart';

class LinkVietMapBloc extends Bloc<LinkVietMapEvent, LinkVietMapState> {
  final IUserRepository _userRepository;
  late final String token;

  LinkVietMapBloc({
    required IUserRepository userRepository,
    required AppBloc appBloc,
  })  : _userRepository = userRepository,
        super(LinkVietMapInit()) {
    on<GetLinkVietMapStatus>(_onGetLinkVietMapStatus);
    on<LinkVietMap>(_onLinkVietMap);
    on<UnLinkVietMap>(_onUnLinkVietMap);

  }

  FutureOr<void> _onGetLinkVietMapStatus(GetLinkVietMapStatus event, Emitter<LinkVietMapState> emit) async{
    final response = await _userRepository.checkVietMapStatus();
    response.when(
      success: (response) {
        if (response.linked ?? false){
          emit(LinkedVietMap(
              name: response.accountName,
              phone: response.phoneNumber,
              token: response.token));
        }else{
          emit(const NotLinkedVietMap());
        }
      },
      failure: (failure) => emit(const NotLinkedVietMap()),
    );
  }

  FutureOr<void> _onLinkVietMap(LinkVietMap event, Emitter<LinkVietMapState> emit) async{
    final request = LinkVietMapRequest(
      accountName: event.accountName,
      phoneNumber: event.phoneNumber,
      token: event.token);
    final response = await _userRepository.linkVietMap(request: request);
    response.when(
      success: (response) {
        emit(LinkedVietMap(
            name: event.accountName,
            phone: event.phoneNumber,
            token: event.token,
            message: response.mess?.description ?? 'Liên kết thành công!'
        ));
      },
      failure: (failure) => emit(NotLinkedVietMap(message: failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onUnLinkVietMap(UnLinkVietMap event, Emitter<LinkVietMapState> emit) async{
    final response = await _userRepository.unLinkVietMap();
    response.when(
      success: (response) {
        emit(const NotLinkedVietMap());
      },
      failure: (failure) => emit(NotLinkedVietMap(message: failure.message ?? 'Có lỗi xảy ra')),
    );
  }

}
