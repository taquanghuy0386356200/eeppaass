part of 'app_bloc.dart';

@immutable
class AppState extends Equatable {
  final AuthInfo? authInfo;
  final User? user;
  final String? encryptedPassword;
  final ContractPayment? contractPayment;
  final bool? isSyncCarDoctor;

  const AppState({
    this.authInfo,
    this.user,
    this.encryptedPassword,
    this.contractPayment,
    this.isSyncCarDoctor
  });

  AppState copyWith({
    required AuthInfo? authInfo,
    required User? user,
    required String? encryptedPassword,
    ContractPayment? contractPayment,
    bool? isSyncCarDoctor
  }) =>
      AppState(
        authInfo: authInfo,
        user: user,
        encryptedPassword: encryptedPassword,
        contractPayment: contractPayment ?? this.contractPayment,
        isSyncCarDoctor: isSyncCarDoctor ?? this.isSyncCarDoctor,
      );

  @override
  List<Object?> get props => [
        authInfo,
        user,
        encryptedPassword,
        contractPayment,
        isSyncCarDoctor
      ];
}
