import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/user_sms_vehicle_setting.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:equatable/equatable.dart';

part 'sms_vehicle_event.dart';

part 'sms_vehicle_state.dart';

class SMSVehicleBloc extends Bloc<SMSVehicleEvent, SMSVehicleState> {
  final IUserRepository _userRepository;

  SMSVehicleBloc({required IUserRepository userRepository})
      : _userRepository = userRepository,
        super(SMSVehicleInitial()) {
    on<UserSMSVehicleFetched>(_onUserSMSVehicleFetched);
    on<SMSVehicleRegistered>(_onSMSVehicleRegistered);
    on<SMSVehicleUpdated>(_onSMSVehicleUpdated);
  }

  FutureOr<void> _onUserSMSVehicleFetched(
    UserSMSVehicleFetched event,
    Emitter<SMSVehicleState> emit,
  ) async {
    emit(UserSMSVehicleFetchedInProgress());

    final result = await _userRepository.getUserSMSVehicle();

    result.when(
      success: (success) => emit(UserSMSVehicleFetchedSuccess(success)),
      failure: (failure) => emit(UserSMSVehicleFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onSMSVehicleRegistered(
    SMSVehicleRegistered event,
    Emitter<SMSVehicleState> emit,
  ) async {
    emit(SMSVehicleRegisteredInProgress());

    final result = await _userRepository.registerSMSVehicle(
      code: event.smsRegisCode,
      autoRenew: event.autoRenew,
    );

    result.when(
      success: (success) => emit(SMSVehicleRegisteredSuccess(message: success)),
      failure: (failure) => emit(SMSVehicleRegisteredFailure(failure.message ?? '')),
    );
  }

  FutureOr<void> _onSMSVehicleUpdated(
    SMSVehicleUpdated event,
    Emitter<SMSVehicleState> emit,
  ) async {
    emit(SMSVehicleUpdatedInProgress());

    final result = await _userRepository.updateSMSVehicle(
      status: event.status,
      autoRenew: event.autoRenew,
    );

    result.when(
      success: (success) => emit(SMSVehicleUpdatedSuccess(message: success)),
      failure: (failure) => emit(SMSVehicleUpdatedFailure(failure.message ?? '')),
    );
  }
}
