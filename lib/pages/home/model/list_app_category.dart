import 'package:json_annotation/json_annotation.dart';

import 'list_app_subs_category.dart';

@JsonSerializable()
class ListAppCategory {
  ListAppCategory({
    this.cateId,
    this.cateName,
    this.catePriority,
    this.listAppSubsCategory,
  });

  ListAppCategory.fromJson(dynamic json) {
    cateId = json['cateId'];
    cateName = json['cateName'];
    catePriority = json['catePriority'];
    if (json['listAppSubsCategory'] != null) {
      listAppSubsCategory = [];
      json['listAppSubsCategory'].forEach((v) {
        listAppSubsCategory?.add(ListAppSubsCategory.fromJson(v));
      });
    }
  }

  int? cateId;
  String? cateName;
  int? catePriority;
  List<ListAppSubsCategory>? listAppSubsCategory;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['cateId'] = cateId;
    map['cateName'] = cateName;
    map['catePriority'] = catePriority;
    if (listAppSubsCategory != null) {
      map['listAppSubsCategory'] = listAppSubsCategory?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
