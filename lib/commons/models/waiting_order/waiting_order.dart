
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';
part 'waiting_order.g.dart';


@JsonSerializable()
class WaitingOrder{
  int? saleOrderId;
  String? saleOrderDate;
  int? saleOrderType;
  int? status;
  int? amount;
  int? custId;
  String? requestId;
  String? contractNo;
  String? createUser;
  String? createDate;
  String? plateNumber;
  int? stationId;
  int? laneInId;
  int? laneOutId;
  String? ticketType;
  String? checkinTime;
  String? plateColor;
  String? epc;
  int? partnerId;
  String? ticketId;
  String? checkoutTime;
  String? serviceName;
  String? partnerName;
  String? stationName;
  String? laneInName;
  String? laneOutName;

  @JsonKey(ignore: true)
  bool isRefuse = false;

  @JsonKey(ignore: true)
  bool isAgree = false;

  WaitingOrder(
      {this.saleOrderId,
        this.saleOrderDate,
        this.saleOrderType,
        this.status,
        this.amount,
        this.custId,
        this.requestId,
        this.contractNo,
        this.createUser,
        this.createDate,
        this.plateNumber,
        this.stationId,
        this.laneInId,
        this.laneOutId,
        this.ticketType,
        this.checkinTime,
        this.plateColor,
        this.epc,
        this.partnerId,
        this.ticketId,
        this.checkoutTime,
        this.serviceName,
        this.partnerName,
        this.stationName,
        this.laneInName,
        this.laneOutName});

  factory WaitingOrder.fromJson(Map<String, dynamic> json)  => _$WaitingOrderFromJson(json);

  Map<String, dynamic> toJson() => _$WaitingOrderToJson(this);
}

@JsonSerializable()
class WaitingOrderResponse {
  List<WaitingOrder>? data;
  MessageData? mess;

  WaitingOrderResponse({this.data, this.mess});

  factory WaitingOrderResponse.fromJson(Map<String, dynamic> json) => _$WaitingOrderResponseFromJson(json);
}