import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/avatar_widget.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/modal/image_picker/image_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/image_picker/image_picker_modal.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/profile/avatar_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ProfileCard extends StatelessWidget {
  const ProfileCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ImagePickerBloc(imagePicker: getIt<ImagePicker>()),
      child: MultiBlocListener(
        listeners: [
          BlocListener<ImagePickerBloc, ImagePickerState>(
            listener: (context, state) {
              if (state is ImagePickedInProgress) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
                if (state is ImagePickedSuccess) {
                  context
                      .read<AvatarBloc>()
                      .add(AvatarUploaded(filePath: state.filePath));
                } else if (state is ImagePickedFailure) {
                  context.loaderOverlay.hide();
                  showErrorSnackBBar(context: context, message: state.message);
                }
              }
            },
          ),
          BlocListener<AvatarBloc, AvatarState>(
            listener: (context, state) {
              if (state.isLoading) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
              }
            },
          ),
        ],
        child: ShadowCard(
          shadowOpacity: 0.2,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(24.0),
            ),
            child: Stack(
              fit: StackFit.loose,
              children: [
                Positioned(
                  top: 2.h,
                  right: 4.w,
                  child: SplashIconButton(
                    icon: Assets.icons.pencil.svg(),
                    onTap: () => context.pushRoute(const ProfileRoute()),
                  ),
                ),
                Center(
                  child: BlocSelector<AppBloc, AppState, User?>(
                    selector: (state) => state.user,
                    builder: (context, user) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(height: 24.h),
                            Stack(
                              clipBehavior: Clip.none,
                              children: [
                                AvatarWidget(
                                  height: 100.r,
                                  width: 100.r,
                                ),
                                Positioned(
                                  right: 0.w,
                                  bottom: -8.h,
                                  child: SplashIconButton(
                                    icon: Container(
                                      padding: EdgeInsets.all(6.r),
                                      decoration: const BoxDecoration(
                                        color: ColorName.primaryColor,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Icon(
                                        Icons.camera_alt,
                                        color: Colors.white,
                                        size: 16.r,
                                      ),
                                    ),
                                    onTap: () async => await showImagePickerModal(context),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 12.h),
                            Text(
                              user?.userName ?? 'N/A',
                              style:
                                  Theme.of(context).textTheme.headline6!.copyWith(
                                        fontWeight: FontWeight.bold,
                                      ),
                            ),
                            SizedBox(height: 8.h),
                            Text(
                              '${user?.contractNo ?? ''}'
                              '${user?.contractNo != null ? ' - ' : ''}'
                              '${user?.accountAlias ?? ''}',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            SizedBox(height: 32.h),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
