import 'dart:async';

import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/auth_info/auth_info.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/flavors.dart';
import 'package:simple_result/simple_result.dart';

enum AuthenticationStatus { unknown, authenticated, unauthenticated }

abstract class IAuthenticationRepository {
  Future<Result<AuthInfo, Failure>> authenticate({
    required String username,
    required String password,
  });

  Future<Result<NoData, Failure>> logout({
    required String refreshToken,
  });
}

class AuthenticationRepository extends IAuthenticationRepository {
  final CrmClient _client;

  AuthenticationRepository({
    required CrmClient crmClient,
  }) : _client = crmClient;

  @override
  Future<Result<AuthInfo, Failure>> authenticate({
    required String username,
    required String password,
  }) async {
    try {
      final response = await _client.login(
        username,
        password,
        F.keycloakClientId,
        F.keycloakGrantType,
      );
      if (response != null) {
        return Result.success(response);
      }
      return const Result.failure(UnknownFailure());
    } on BadNetworkException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<NoData, Failure>> logout({
    required String refreshToken,
  }) async {
    try {
      final response = await _client.logout(
        clientId: F.keycloakClientId,
        refreshToken: refreshToken,
      );

      if (response != null) {
        if (response.mess?.code == REQUEST_SUCCESS_CODE) {
          return Result.success(response);
        } else {
          return Result.failure(
            ResponseFailure(message: response.mess?.description ?? ''),
          );
        }
      }

      return const Result.failure(UnknownFailure());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
