import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// vehicleTrackId : 1
/// contractId : 12345
/// contractNo : "V001001022"
/// trackDate : ""
/// stationId : ""
/// status : 0
/// warLat1 : ""
/// warLong1 : ""
/// createUser : ""
/// createDate : ""
/// updateUser : ""
/// updateDate : ""
/// message : ""

part 'station_vehicle_alert_response.g.dart';

@JsonSerializable()
class StationVehicleAlert {
  final int? vehicleTrackId;
  final int? contractId;
  final String? contractNo;
  final String? trackDate;
  final String? stationId;
  final int? status;
  final double? warLat1;
  final double? warLong1;
  final String? createUser;
  final String? createDate;
  final String? updateUser;
  final String? updateDate;
  final String? message;

  StationVehicleAlert({
    this.vehicleTrackId,
    this.contractId,
    this.contractNo,
    this.trackDate,
    this.stationId,
    this.status,
    this.warLat1,
    this.warLong1,
    this.createUser,
    this.createDate,
    this.updateUser,
    this.updateDate,
    this.message,
  });

  factory StationVehicleAlert.fromJson(Map<String, dynamic> json) => _$StationVehicleAlertFromJson(json);

  Map<String, dynamic> toJson() => _$StationVehicleAlertToJson(this);
}

@JsonSerializable()
class StationVehicleAlertResponse {
  final StationVehicleAlert? data;
  final MessageData? mess;

  StationVehicleAlertResponse({this.data, this.mess});

  factory StationVehicleAlertResponse.fromJson(Map<String, dynamic> json) =>
      _$StationVehicleAlertResponseFromJson(json);
}
