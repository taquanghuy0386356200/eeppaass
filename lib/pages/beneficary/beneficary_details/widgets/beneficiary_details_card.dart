import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/pages/beneficary/beneficary_details/bloc/beneficary_details_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BeneficiaryDetailsCard extends StatefulWidget {
  final Beneficiary? beneficiary;

  const BeneficiaryDetailsCard({Key? key, this.beneficiary}) : super(key: key);

  @override
  State<BeneficiaryDetailsCard> createState() => _BeneficiaryDetailsCardState();
}

class _BeneficiaryDetailsCardState extends State<BeneficiaryDetailsCard> {
  final _reminiscentNameController = TextEditingController();
  final _contractNo = TextEditingController();
  final _customerName = TextEditingController();

  @override
  void dispose() {
    _reminiscentNameController.dispose();
    _contractNo.dispose();
    _customerName.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _reminiscentNameController.addListener(() {
      var name = _reminiscentNameController.text;
      context.read<BeneficiaryDetailsBloc>().add(BeneficiaryChanged(name));
    });
    final reminiscentName = widget.beneficiary?.reminiscentName;
    _reminiscentNameController.text = reminiscentName.toString();
  }

  @override
  Widget build(BuildContext context) {
    final contractNo = widget.beneficiary?.contractNo;
    if (contractNo != null) _contractNo.text = contractNo.toString();
    final customerName = widget.beneficiary?.customerName;
    if (customerName != null) _customerName.text = customerName.toString();

    return ShadowCard(
      shadowOpacity: 0.2,
      borderRadius: 20.0,
      child: Container(
        padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 20.h),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 20.0,
            ),
            PrimaryTextField(
              controller: _contractNo,
              labelText: 'Số hợp đồng',
              maxLength: 13,
              inputType: TextInputType.text,
              isCurrency: true,
              enabled: false,
              hasClearButton: false,
              suffix: const Icon(
                Icons.account_circle_rounded,
                size: 26,
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            PrimaryTextField(
              controller: _customerName,
              labelText: 'Tên khách hàng',
              inputType: TextInputType.text,
              enabled: false,
              hasClearButton: false,
              suffix: const Icon(
                Icons.account_circle_rounded,
                size: 26,
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            PrimaryTextField(
              controller: _reminiscentNameController,
              labelText: 'Tên gợi nhớ',
              inputType: TextInputType.text,
              isPlateNumber: true,
              suffix: const Icon(
                Icons.account_circle_rounded,
                size: 26,
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}
