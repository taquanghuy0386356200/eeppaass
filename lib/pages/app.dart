import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/api/bad_certificate_overrides.dart';
import 'package:epass/commons/observers/simple_bloc_observer.dart';
import 'package:epass/commons/repo/app_store_repository.dart';
import 'package:epass/commons/repo/campaign_repository.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/routes/guards/first_run_guard.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/flavors.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/category/category_bloc.dart';
import 'package:epass/pages/bloc/fcm/fcm_bloc.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/bloc/profile/avatar_bloc.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/bloc/sms_register/sms_register_bloc.dart';
import 'package:epass/pages/bloc/station_vehicle_alert/station_vehicle_alert_bloc.dart';
import 'package:epass/pages/home/bloc/home_bloc.dart';
import 'package:epass/pages/home/home_page.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/home_tab/home_tabs_page.dart';
import 'package:epass/pages/notification_tab/notification_tab_page.dart';
import 'package:epass/pages/parking/pages/parking_setting/parking_roadSide_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// import 'package:flutter_loggy/flutter_loggy.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:loggy/loggy.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:car_doctor_sdk_tracking/car_doctor_sdk_tracking.dart';

void startApp() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Firebase
  await Firebase.initializeApp();
  await AppTracking.instance
      .init('https://api.cardoctor.com.vn/log/', null, null);
  // GetIt
  configureDependencies();

  //if (kDebugMode) {
  HttpOverrides.global = BadCertificateOverrides();
  //}

  // // Loggy
  // Loggy.initLoggy(
  //   logPrinter: const PrettyDeveloperPrinter(),
  // );

  // Hydrated bloc
  final storage = await HydratedStorage.build(
    storageDirectory: await getApplicationSupportDirectory(),
  );

  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getTemporaryDirectory(),
  );
  runApp(const App());

  await dotenv.load(fileName: "cardoctor/${F.appFlavor!.name}.cnf");
}

//Biến này tạo ra để thông báo popup gợi ý làm lại mật khẩu
//do chuyển đổi team buildIOS đẩy live sẽ mất các dữ liệu về keyToken, keyAccess
bool canNotShowPopupUpdateApp = true;

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

final _appRouter =
    AppRouter(firstRunGuard: FirstRunGuard(), navigatorKey: navigatorKey);

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void deactivate() {
    // TODO: implement deactivate
    super.deactivate();
  }

  @override
  void initState() {
    // print("check innit ${context}");
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => const HomePage()),
    // );
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _portraitModeOnly();

    final textTheme = Theme.of(context).textTheme;

    return ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      builder: (_, __) {
        return FutureBuilder(
          future: getIt.allReady(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }
            return MaterialApp.router(
              routerDelegate: AutoRouterDelegate(_appRouter),
              routeInformationParser: _appRouter.defaultRouteParser(),
              routeInformationProvider: _appRouter.routeInfoProvider(),
              title: F.title,
              localizationsDelegates: const [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              theme: ThemeData(
                primarySwatch: Colors.orange,
                textTheme: GoogleFonts.mulishTextTheme(
                  textTheme
                      .apply(
                        bodyColor: ColorName.textGray1,
                        displayColor: ColorName.textGray1,
                      )
                      .copyWith(
                        headline6:
                            textTheme.headline6!.copyWith(fontSize: 20.sp),
                        subtitle1:
                            textTheme.subtitle1!.copyWith(fontSize: 16.sp),
                        subtitle2:
                            textTheme.subtitle2!.copyWith(fontSize: 14.sp),
                        bodyText1:
                            textTheme.bodyText1!.copyWith(fontSize: 16.sp),
                        bodyText2:
                            textTheme.bodyText2!.copyWith(fontSize: 14.sp),
                        caption: textTheme.caption!.copyWith(fontSize: 12.sp),
                        button: textTheme.button!.copyWith(fontSize: 23.sp, fontWeight: FontWeight.bold),
                        overline: textTheme.button!.copyWith(fontSize: 10.sp),
                      ),
                ),
              ),
              builder: (context, child) {
                return LoaderOverlay(
                  disableBackButton: true,
                  overlayColor: Colors.black45,
                  child: _flavorBanner(
                    child: MediaQuery(
                      data:
                          MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                      child: MultiBlocProvider(
                        providers: [
                          BlocProvider.value(value: getIt<AppBloc>()),
                          BlocProvider.value(value: getIt<SettingBloc>()),
                          // NotificationBLoc
                          BlocProvider.value(value: getIt<NotificationBloc>()),
                          BlocProvider.value(
                              value: getIt<EventNotificationBloc>()),
                          BlocProvider.value(
                              value: getIt<BalanceChangeNotificationBloc>()),
                          BlocProvider.value(
                              value: getIt<DiscountNotificationBloc>()),
                          BlocProvider.value(
                              value: getIt<NewsNotificationBloc>()),
                          //
                          BlocProvider.value(
                              value: getIt<FcmBloc>()
                                ..add(const FcmPermissionRequest())),
                          BlocProvider.value(value: getIt<CategoryBloc>()),
                          BlocProvider(create: (context) => HomeTabsBloc()),
                          BlocProvider(
                              create: (context) => SmsRegisterBloc(
                                  categoryRepository:
                                      getIt<ICategoryRepository>(),
                                  userRepository: getIt<IUserRepository>(),
                                  appBloc: getIt<AppBloc>())),
                          BlocProvider(
                            create: (context) => HomeBloc(
                                userRepository: getIt<IUserRepository>(),
                                campaignRepository:
                                    getIt<ICampaignRepository>(),
                                appBloc: context.read<AppBloc>(),
                                iAppStoreRepository:
                                    getIt<IAppStoreRepository>()),
                          ),
                          BlocProvider(
                              create: (context) => AvatarBloc(
                                  userRepository: getIt<IUserRepository>(),
                                  appBloc: getIt<AppBloc>())),
                          BlocProvider.value(
                              value: getIt<StationVehicleAlertBloc>()),
                        ],
                        child: RefreshConfiguration(
                          headerBuilder: () => const ClassicHeader(
                            completeText: 'Tải lại thành công',
                            refreshingText: 'Đang tải...',
                            failedText: 'Tải lại thất bại',
                            idleText: 'Kéo xuống để tải lại',
                            releaseText: 'Thả để tải lại',
                          ),
                          footerBuilder: () => const ClassicFooter(
                            noDataText: 'Không có dữ liệu',
                            loadingText: 'Đang tải...',
                            failedText: 'Tải thêm thất bại',
                            idleText: 'Tải thêm',
                            canLoadingText: 'Thả để tải thêm',
                          ),
                          child: GestureDetector(
                            onTap: FocusScope.of(context).unfocus,
                            child: child,
                          ),
                        ),
                      ),
                    ),
                    show: kDebugMode,
                  ),
                );
              },
            );
          },
        );
      },
    );
  }

  Widget _flavorBanner({
    required Widget child,
    bool show = true,
  }) =>
      show
          ? Banner(
              location: BannerLocation.topStart,
              message: F.name,
              color: Colors.green.withOpacity(0.6),
              textStyle: const TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 12.0,
                  letterSpacing: 1.0),
              textDirection: TextDirection.ltr,
              child: child,
            )
          : Container(child: child);
}

mixin PortraitModeMixin on StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _portraitModeOnly();
    return const Scaffold();
  }
}

/// blocks rotation; sets orientation to: portrait
void _portraitModeOnly() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}

void _enableRotation() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]);
}
