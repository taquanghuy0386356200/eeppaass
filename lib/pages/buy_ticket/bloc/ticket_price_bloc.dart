import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_response.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/repo/buy_ticket_repository.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_duration_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'ticket_price_event.dart';

part 'ticket_price_state.dart';

class TicketPriceBloc extends Bloc<TicketPriceEvent, TicketPriceState> {
  final IBuyTicketRepository _buyTicketRepository;

  TicketPriceBloc({required IBuyTicketRepository buyTicketRepository})
      : _buyTicketRepository = buyTicketRepository,
        super(TicketPriceInitial()) {
    on<TicketPriceFetched>(_onTicketPriceFetched);
    on<TicketPriceInValid>(_onTicketPriceInValid);
  }

  FutureOr<void> _onTicketPriceFetched(
    TicketPriceFetched event,
    Emitter<TicketPriceState> emit,
  ) async {
    emit(const TicketPriceFetchedInProgress());
    final result = await _buyTicketRepository.getTicketPrices(
      vehicles: event.vehicles,
      servicePlanType: event.servicePlanType,
      effectiveDate: event.effectiveDate,
      expireDate: event.expireDate,
      stationStage: event.stationStage,
    );

    result.when(
      success: (success) => emit(TicketPriceFetchedSuccess(success)),
      failure: (failure) =>
          emit(TicketPriceFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onTicketPriceInValid(
      TicketPriceInValid event, Emitter<TicketPriceState> emit) {
    emit(TicketPriceInitial());
  }
}
