import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/pages/register/bloc/register_paper_scan/register_id_card_bloc.dart';
import 'package:epass/pages/register/bloc/register_paper_scan/register_paper_scan_permission_bloc.dart';
import 'package:epass/pages/register/pages/register_customer/widgets/scan_id_card.dart';
import 'package:epass/pages/register/widgets/base_register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class RegisterPaperTypePage extends StatelessWidget {
  const RegisterPaperTypePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterPaperScanPermissionBloc, RegisterPaperScanPermissionState>(
      listener: (context, state) {
        if (state is RegisterPaperScanPermissionRequestedInProgress) {
          context.loaderOverlay.show();
        } else if (state is RegisterPaperScanPermissionRequestedFailure) {
          context.loaderOverlay.hide();
          showErrorSnackBBar(context: context, message: state.message);
        } else if (state is RegisterPaperScanPermissionRequestedSuccess) {
          context.loaderOverlay.hide();
          context.pushRoute(RegisterPaperScanRoute(isFront: state.isFront));
        }
      },
      child: BaseRegisterPage(
        title: 'Xác minh thông tin',
        step: 2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              maxLines: 5,
              text: TextSpan(
                style: Theme.of(context).textTheme.bodyText1!.copyWith(height: 1.4),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Lưu ý: ',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text: 'Giấy tờ cá nhân phải còn hiệu lực tại thời điểm đăng ký.\n',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  TextSpan(
                    text: 'Lưu ý: ',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(color: Colors.transparent),
                  ),
                  TextSpan(
                    text: 'Giấy tờ cá nhân của cá nhân sở hữu nhiều xe hoặc chủ doanh nghiệp.',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            ),
            SizedBox(height: 24.h),
            Center(
              child: BlocSelector<RegisterIdCardBloc, RegisterIdCardState, String?>(
                selector: (state) => state.front,
                builder: (context, filePath) {
                  return ScanIDCard(
                    isFront: true,
                    front: filePath,
                  );
                },
              ),
            ),
            SizedBox(height: 16.h),
            Center(
              child: BlocSelector<RegisterIdCardBloc, RegisterIdCardState, String?>(
                selector: (state) => state.back,
                builder: (context, filePath) {
                  return ScanIDCard(
                    isFront: false,
                    back: filePath,
                  );
                },
              ),
            ),
            SizedBox(height: 32.h),
            SizedBox(
              height: 56.h,
              width: double.infinity,
              child: BlocBuilder<RegisterIdCardBloc, RegisterIdCardState>(
                builder: (context, state) {
                  return PrimaryButton(
                    title: 'Tiếp theo',
                    enabled: state.front != null && state.back != null,
                    onTap: state.front != null && state.back != null
                        ? () => context.pushRoute(const RegisterAddressRoute())
                        : null,
                  );
                },
              ),
            ),
            SizedBox(height: 16.h),
          ],
        ),
      ),
    );
  }
}
