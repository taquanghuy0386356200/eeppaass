part of 'ticket_price_bloc.dart';

abstract class TicketPriceState extends Equatable {
  const TicketPriceState();
}

class TicketPriceInitial extends TicketPriceState {
  @override
  List<Object> get props => [];
}

class TicketPriceFetchedInProgress extends TicketPriceState {
  const TicketPriceFetchedInProgress();

  @override
  List<Object> get props => [];
}

class TicketPriceFetchedFailure extends TicketPriceState {
  final String message;

  const TicketPriceFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class TicketPriceFetchedSuccess extends TicketPriceState {
  final List<TicketPrice> ticketPrices;

  const TicketPriceFetchedSuccess(this.ticketPrices);

  @override
  List<Object> get props => [ticketPrices];
}
