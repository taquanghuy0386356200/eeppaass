extension VersionApp on String {
  int getExtendedVersionNumber() {
    List versionCells = split('.');
    versionCells = versionCells.map((i) => int.parse(i)).toList();
    return versionCells[0] * 10000 + versionCells[1] * 100 + versionCells[2];
  }
}
