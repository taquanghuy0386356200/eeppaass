part of 'ticket_duration_bloc.dart';

class TicketDurationState extends Equatable {
  final DateTime? effectiveDate;
  final DateTime? expireDate;

  const TicketDurationState({this.effectiveDate, this.expireDate});

  @override
  List<Object?> get props => [effectiveDate, expireDate];

  TicketDurationState copyWith({
    DateTime? effectiveDate,
    DateTime? expireDate,
  }) {
    return TicketDurationState(
      effectiveDate: effectiveDate ?? this.effectiveDate,
      expireDate: expireDate ?? this.expireDate,
    );
  }
}
