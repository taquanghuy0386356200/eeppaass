
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:epass/commons/models/parking/verify_config_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'change_passcode_response.g.dart';

@JsonSerializable()
class ChangePassCodeResponse {
  ChangePassCodeResponse({
    this.data,
    this.mess,
  });

  final VerifyConfigModel? data;
  final MessageData? mess;

  factory ChangePassCodeResponse.fromJson(Map<String, dynamic> json) => _$ChangePassCodeResponseFromJson(json);
}