import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:equatable/equatable.dart';

part 'forgot_password_otp_event.dart';

part 'forgot_password_otp_state.dart';

class ForgotPasswordOtpBloc extends Bloc<ForgotPasswordOtpEvent, ForgotPasswordOtpState> {
  final IUserRepository _userRepository;

  ForgotPasswordOtpBloc({required IUserRepository userRepository})
      : _userRepository = userRepository,
        super(ForgotPasswordOtpInitial()) {
    on<ForgotPasswordOTPConfirmed>(_onForgotPasswordOTPConfirmed);
  }

  FutureOr<void> _onForgotPasswordOTPConfirmed(
    ForgotPasswordOTPConfirmed event,
    Emitter<ForgotPasswordOtpState> emit,
  ) async {
    emit(ForgotPasswordOTPConfirmedInProgress());

    final result = await _userRepository.resetPasswordOTPConfirm(
      phoneNumber: event.phoneNumber,
      plateNumber: event.plateNumber,
      plateTypeCode: event.plateTypeCode,
      otp: event.otp,
      newPassword: event.newPassword,
    );

    result.when(
      success: (success) => emit(ForgotPasswordOTPConfirmedSuccess()),
      failure: (failure) => emit(ForgotPasswordOTPConfirmedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
