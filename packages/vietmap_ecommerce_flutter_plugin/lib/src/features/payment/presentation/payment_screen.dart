import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentScreen extends StatefulWidget {
  final String paymentUrl;
  const PaymentScreen({
    Key? key,
    required this.paymentUrl,
  }) : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          webViewController.loadUrl(widget.paymentUrl);
        },
        onPageStarted: (url) {
          _checkUrl(context, url);
        },
        onWebResourceError: (error) {
          if (kDebugMode) {
            print(error);
          }
        },
        onPageFinished: (url) {},
        navigationDelegate: (NavigationRequest request) {
          var url = request.url;
          if (Uri.parse(url).queryParameters['paymentSuccess'] != null) {
            _checkUrl(context, url);
            return NavigationDecision.prevent;
          }
          if (url.startsWith(_AGAIN_LINK)) {
            return NavigationDecision.prevent;
          }
          if (url.startsWith(_VIETMAP_LIVE_LINK)) {
            return NavigationDecision.prevent;
          }
          // if (url.startsWith(_VIETMAP_LIVE_LINK)) {
          //   return NavigationDecision.prevent;
          // }
          if (!url.startsWith("http")) {
            launch(url);
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
      ),
    );
  }

  void _checkUrl(BuildContext context, String url) {
    if (kDebugMode) {
      print('_checkUrl: $url');
    }
    if (url.contains('paymentSuccess')) {
      if (mounted) {
        Navigator.pop(
          context,
          Uri.parse(url).queryParameters['paymentSuccess'],
        );
      }
    }
  }
}

const _AGAIN_LINK = "https://localhost/again_link";
const _VIETMAP_LIVE_LINK = 'vietmaplive';
