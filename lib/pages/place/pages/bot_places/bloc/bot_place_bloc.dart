import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/enum/boo_code.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:epass/commons/repo/station_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:tiengviet/tiengviet.dart';

part 'bot_place_event.dart';

part 'bot_place_state.dart';

class BotPlaceBloc extends Bloc<BotPlaceEvent, BotPlaceState> {
  final IStationRepository _stationRepository;

  BotPlaceBloc({required IStationRepository stationRepository})
      : _stationRepository = stationRepository,
        super(const BotPlaceState(
          listData: [],
          filteredListData: [],
        )) {
    on<BotPlaceFetched>(_onBotPlaceFetched);
    on<BotPlaceRefreshed>(_onBotPlaceRefreshed);
    on<BotPlaceSearched>(_onBotPlaceSearched);
  }

  FutureOr<void> _onBotPlaceFetched(
    BotPlaceFetched event,
    Emitter<BotPlaceState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      error: null,
    ));

    await _botPlaceFetched(emit);
  }

  Future<void> _botPlaceFetched(Emitter<BotPlaceState> emit) async {
    final result = await _stationRepository.getStationStages(
      booCode: BooCode.boo2.value,
      stationType: 1, // 1- station
    );

    result.when(
      success: (success) => emit(state.copyWith(
        isLoading: false,
        listData: success.listData,
        filteredListData: success.listData,
        error: null,
      )),
      failure: (failure) => emit(state.copyWith(
        error: failure.message,
        isLoading: false,
      )),
    );
  }

  FutureOr<void> _onBotPlaceRefreshed(
    BotPlaceRefreshed event,
    Emitter<BotPlaceState> emit,
  ) async {
    emit(state.copyWith(
      isRefreshing: true,
      listData: const [],
      filteredListData: const [],
      error: null,
    ));

    await _botPlaceFetched(emit);
  }

  FutureOr<void> _onBotPlaceSearched(
    BotPlaceSearched event,
    Emitter<BotPlaceState> emit,
  ) {
    final searchTerm = event.input?.trim().toLowerCase() ?? '';
    final searchTermNoAccent = TiengViet.parse(searchTerm);

    final filterListData = state.listData.where((element) {
      final name = element.name?.trim().toLowerCase() ?? '';
      final nameNoAccent = TiengViet.parse(name);
      return nameNoAccent.contains(searchTermNoAccent);
    }).toList();

    emit(state.copyWith(
      error: null,
      filteredListData: filterListData,
    ));
  }
}
