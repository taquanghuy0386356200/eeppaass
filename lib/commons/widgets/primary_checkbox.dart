import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';

class PrimaryCheckbox extends StatelessWidget {
  final bool isCheck;

  final void Function(bool?)? onChanged;

  const PrimaryCheckbox({
    Key? key,
    this.isCheck = false,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
        MaterialState.selected,
      };
      if (states.any(interactiveStates.contains)) {
        return ColorName.primaryColor;
      }
      return ColorName.borderColor;
    }

    return Checkbox(
      checkColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(3.0),
      ),
      fillColor: MaterialStateProperty.resolveWith(getColor),
      value: isCheck,
      onChanged: onChanged,
    );
  }
}
