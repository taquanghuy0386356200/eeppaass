import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/auth_info/auth_info.dart';
import 'package:epass/commons/models/contract_payment/contract_payment.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/services/encryption/encryption.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'app_event.dart';

part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final IEncryption _encryption;

  AppBloc({required IEncryption encryption})
      : _encryption = encryption,
        super(const AppState()) {
    on<UpdateAppAuthentication>(
      (event, emit) => emit(state.copyWith(
        authInfo: event.authInfo,
        user: state.user,
        encryptedPassword: state.encryptedPassword,
      )),
    );

    on<UpdateAppUser>(
      (event, emit) => emit(state.copyWith(
        user: event.user,
        authInfo: state.authInfo,
        encryptedPassword: state.encryptedPassword,
      )),
    );

    on<UserLogout>(
      (event, emit) => emit(state.copyWith(
        authInfo: null,
        user: null,
        encryptedPassword: null,
      )),
    );

    on<UpdateEncryptedPassword>((event, emit) {
      // Encrypted password in memory
      final encryptedPassword = _encryption.encrypt(event.password);
      emit(state.copyWith(
        authInfo: state.authInfo,
        user: state.user,
        encryptedPassword: encryptedPassword,
      ));
    });

    on<UpdateSyncCarDoctorStatus>((event, emit) {
      emit(state.copyWith(
        authInfo: state.authInfo,
        user: state.user,
        encryptedPassword: state.encryptedPassword,
        isSyncCarDoctor: event.isSyncCarDoctor
      ));
    });
  }
}
