import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/auto_topup/auto-topup.dart';
import 'package:epass/commons/models/cash_in_order/cash_in_order.dart';
import 'package:epass/commons/models/confirm_momo_payment/confirm_momo_payment.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:epass/commons/models/contracts_info/contracts_info.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/models/topup_channel/topup_channel_fee.dart';
import 'package:epass/commons/models/topup_fee/topup_fee.dart';
import 'package:simple_result/simple_result.dart';

abstract class IAutoCashInRepository {
  Future<Result<List<TopupChannel>, Failure>> getTopupChannel();

  Future<Result<List<ContractsInfo>, Failure>> getContractsInfo({
    String? inputSearch,
    int status = 2,
  });

  Future<Result<List<TopupFee>, Failure>> getTopupFee();

  Future<Result<TopupChannelFee, Failure>> getTopupChannelFee({
    required String topupChannelCode,
    required int amount,
  });

  Future<Result<String, Failure>> createCashInMomoOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  });

  Future<Result<ConfirmMomoPaymentData, Failure>> confirmMomoPayment({
    required String partnerCode,
    required String orderId,
    required int amount,
    required String phoneNumber,
    required String token,
  });

  Future<Result<String, Failure>> createCashInViettelPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  });

  Future<Result<String, Failure>> createCashInVNPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  });

  Future<Result<AutoTopupResponse, Failure>> saveAutoConfig({
    required int amount,
    required int limit,
    required int isActive,
    required String paymentMethod,
  });

  Future<Result<AutoTopupRequest, Failure>> getAutoConfig();

  Future<Result<AutoTopupResponse, Failure>> updateAutoConfig(
      {required AutoTopupRequest autoTopupRequest});

  Future<Result<ContractTopupLastChargeResponse, Failure>> getLastCharge();
}

class AutoCashInRepository extends IAutoCashInRepository {
  final CrmClient _crmClient;

  AutoCashInRepository({required CrmClient crmClient}) : _crmClient = crmClient;

  @override
  Future<Result<List<TopupChannel>, Failure>> getTopupChannel() async {
    try {
      final response = await _crmClient.getTopupChannel();
      final topupChannels = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          topupChannels == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(topupChannels);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<ContractsInfo>, Failure>> getContractsInfo({
    String? inputSearch,
    int status = 2,
  }) async {
    try {
      final response = await _crmClient.getContractsInfo(
        inputSearch: inputSearch,
        status: status,
      );
      final contractsInfo = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          contractsInfo == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(contractsInfo);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String, Failure>> createCashInMomoOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  }) async {
    try {
      final request = CashInOrderRequest(
        amount: amount,
        contractNo: contractNo,
        receiverContractId: receiverContractId,
        quantity: quantity,
      );

      final response = await _crmClient.createCashInOrderMomo(
        contractId: contractId,
        customerId: customerId,
        request: request,
      );
      final orderId = response.order?.billingCode;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || orderId == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(orderId.toString());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<TopupFee>, Failure>> getTopupFee() async {
    try {
      final response = await _crmClient.getTopupFee();
      final topupFees = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || topupFees == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(topupFees);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<TopupChannelFee, Failure>> getTopupChannelFee({
    required String topupChannelCode,
    required int amount,
  }) async {
    try {
      final response = await _crmClient.getTopupChannelFee(
        topupChannelCode: topupChannelCode,
        amount: amount,
      );
      final topupFee = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || topupFee == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(topupFee);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ConfirmMomoPaymentData, Failure>> confirmMomoPayment({
    required String partnerCode,
    required String orderId,
    required int amount,
    required String phoneNumber,
    required String token,
  }) async {
    try {
      final request = ConfirmMomoPaymentRequest(
        partnerCode: partnerCode,
        partnerRefId: orderId,
        amount: amount,
        customerNumber: phoneNumber,
        appData: token,
      );

      final response = await _crmClient.confirmMomoPayment(
        request: request,
      );
      final confirmMomoPaymentData = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          confirmMomoPaymentData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(confirmMomoPaymentData);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String, Failure>> createCashInViettelPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
    // required String paymentMethodName,
  }) async {
    try {
      final request = CashInOrderRequest(
        amount: amount,
        contractNo: contractNo,
        receiverContractId: receiverContractId,
        quantity: quantity,
        // paymentMethodName: paymentMethodName,
      );

      final response = await _crmClient.createCashInOrderViettelPay(
        contractId: contractId,
        customerId: customerId,
        request: request,
      );
      final orderId = response.order?.billingCode;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || orderId == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(orderId.toString());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String, Failure>> createCashInVNPayOrder({
    required String contractId,
    required String contractNo,
    required String customerId,
    required int amount,
    required int quantity,
    required String receiverContractId,
  }) async {
    try {
      final request = CashInOrderRequest(
        amount: amount,
        contractNo: contractNo,
        receiverContractId: receiverContractId,
        quantity: quantity,
      );

      final response = await _crmClient.createCashInOrderVNPay(
        contractId: contractId,
        customerId: customerId,
        request: request,
      );
      final orderId = response.order?.billingCode;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || orderId == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(orderId.toString());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<AutoTopupResponse, Failure>> saveAutoConfig({
    required int amount,
    required int limit,
    required int isActive,
    required String paymentMethod,
  }) async {
    try {
      final request = AutoTopupRequest(
        amount: amount,
        limit: limit,
        isActive: isActive,
        topupChannel: paymentMethod,
      );

      final response = await _crmClient.saveConfigAutoTopup(
        request: request,
      );
      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<AutoTopupRequest, Failure>> getAutoConfig() async {
    try {
      final response = await _crmClient.getConfigAutoTopup();
      final data = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || data == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<AutoTopupResponse, Failure>> updateAutoConfig({
    required AutoTopupRequest autoTopupRequest,
  }) async {
    try {
      final request = AutoTopupRequest(
        autoConfigTopupId: autoTopupRequest.autoConfigTopupId,
        limit: autoTopupRequest.limit,
        isActive: autoTopupRequest.isActive,
        topupChannel: autoTopupRequest.topupChannel,
        amount: autoTopupRequest.amount,
        contractId: autoTopupRequest.contractId,
      );

      final response = await _crmClient.updateConfigAutoTopup(
        request: request,
      );
      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<ContractTopupLastChargeResponse, Failure>> getLastCharge() async {
    try {
      final response = await _crmClient.getLastCharge();
      return Result.success(response);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
