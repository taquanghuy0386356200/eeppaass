import 'package:dio/dio.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_verify_otp_request.dart';
import 'package:epass/commons/models/car_doctor/car_doctor_verify_otp_response.dart';
import 'package:retrofit/http.dart' as http;
import 'package:retrofit/retrofit.dart';

part 'cardoctor_client.g.dart';

@RestApi()
abstract class CarDoctorClient {
  factory CarDoctorClient(
    Dio dio, {
    String baseUrl,
  }) = _CarDoctorClient;

  @POST('/verify-otp')
  @http.Headers({'Content-Type': 'application/json', 'Accept-Language': 'vi'})
  Future<CarDoctorVerifyOtpResponse> verifyCarDoctorOtp({
    @Header('Authorization') required String token,
    @Body() required CarDoctorVerifyOtpRequest request,
  });

  @POST('/oauth/token')
  @FormUrlEncoded()
  Future<CarDoctorVerifyOtpResponse> getCarDoctorAccessToken({
    @Header('Authorization') required String token,
    @Header('Accept-Language') String acceptLanguage = 'vi',
    @Field('grant_type') String grantType = 'password',
    @Field('username') String username = 'ANONYMOUS',
    @Field('password') String password = 'abc@123',
  });
}
