part of 'alias_form_bloc.dart';

abstract class AliasFormEvent extends Equatable {
  const AliasFormEvent();
}

class AliasFormSubmitted extends AliasFormEvent {
  final String alias;

  const AliasFormSubmitted(this.alias);

  @override
  List<Object> get props => [alias];
}
