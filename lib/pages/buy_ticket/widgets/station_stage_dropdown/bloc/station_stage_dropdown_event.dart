part of 'station_stage_dropdown_bloc.dart';

abstract class StationStageDropdownEvent extends Equatable {
  const StationStageDropdownEvent();
}

class StationStageDropdownFetched extends StationStageDropdownEvent {
  const StationStageDropdownFetched();

  @override
  List<Object> get props => [];
}
