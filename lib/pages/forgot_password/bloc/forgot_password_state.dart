part of 'forgot_password_bloc.dart';

abstract class ForgotPasswordState extends Equatable {
  const ForgotPasswordState();
}

class ForgotPasswordInitial extends ForgotPasswordState {
  @override
  List<Object> get props => [];
}

class ForgotPasswordSubmittedInProgress extends ForgotPasswordState {
  @override
  List<Object> get props => [];
}

class ForgotPasswordSubmittedSuccess extends ForgotPasswordState {
  final DateTime submittedSuccessTime;
  final ForgotPasswordSubmitted resubmitEvent;

  const ForgotPasswordSubmittedSuccess({
    required this.resubmitEvent,
    required this.submittedSuccessTime,
  });

  @override
  List<Object> get props => [resubmitEvent];
}

class ForgotPasswordSubmittedFailure extends ForgotPasswordState {
  final String message;
  final ForgotPasswordSubmitted resubmitEvent;

  const ForgotPasswordSubmittedFailure({
    required this.message,
    required this.resubmitEvent,
  });

  @override
  List<Object> get props => [message, resubmitEvent];
}
