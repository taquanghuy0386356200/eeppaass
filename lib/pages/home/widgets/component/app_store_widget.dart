import 'dart:convert';

import 'package:epass/pages/home/model/apps_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/home_bloc.dart';
import 'section_widget.dart';


class AppStore extends StatefulWidget {
  const AppStore({
    Key? key,
  }) : super(key: key);

  @override
  State<AppStore> createState() => _AppStoreState();
}

class _AppStoreState extends State<AppStore> {


  @override
  void initState() {
    // readJsonData().then((value) =>
    //     setState(() {
    //       appsModel = AppsModel();
    //       appsModel = value;
    //     }));
    super.initState();
  }

  // AppsModel? appsModel;

  @override
  Widget build(BuildContext context) {
  return Container(
      margin: const EdgeInsets.all(16),
      child: BlocSelector<HomeBloc, HomeState, AppsModel?>(
        selector: (state) {
          if (state is HomeLoaded) return state.appsStore;
          return null;
        },
        builder: (context, appsModel) {
          return appsModel==null || appsModel.labelMain==null?const SizedBox.shrink():
            Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (appsModel.labelMain != null && appsModel.labelMain!.isNotEmpty)
                Text(
                  appsModel.labelMain ?? '',
                  style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Color(0xff73777A)),
                ),
              const SizedBox(
                height: 6,
              ),
              ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                      decoration: const BoxDecoration(
                          color: Colors.white, // Set border width
                          borderRadius: BorderRadius.all(Radius.circular(16.0)), // Set rounded corner radius
                          boxShadow: [BoxShadow(blurRadius: 16, color: Colors.black12, offset: Offset(0.1, 3))] // Make rounded corner of border
                      ),
                      child: SectionWidget(
                        appModel: appsModel.listAppPopular ?? [],
                        sectionType: SectionType.popular,
                        appsModel: appsModel,
                      ));
                },
                itemCount: 1,
              ),
            ],
          );
        },
      ),
    );
  }
}

//
// Future<AppsModel?> readJsonData() async {
//   AppsModel? appsModel;
//   final String response = await rootBundle.loadString('assets/app_store.json');
//   appsModel = AppsModel();
//   appsModel = await AppsModel.fromJson(jsonDecode(response));
//   return appsModel;
// }