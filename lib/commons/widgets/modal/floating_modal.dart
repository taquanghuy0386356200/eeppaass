import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FloatingModal extends StatelessWidget {
  final Widget child;
  final Color? backgroundColor;

  const FloatingModal({Key? key, required this.child, this.backgroundColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 16.w,
        ),
        child: Material(
          color: backgroundColor,
          clipBehavior: Clip.antiAlias,
          borderRadius: BorderRadius.circular(16.0),
          child: child,
        ),
      ),
    );
  }
}

Future<T?> showFloatingModalBottomSheet<T>({
  required BuildContext context,
  required WidgetBuilder builder,
  Color? backgroundColor,
  bool enableDrag = true,
  bool isDismissible = true,
}) async {
  final result = await showCustomModalBottomSheet<T>(
    context: context,
    builder: builder,
    useRootNavigator: true,
    backgroundColor: backgroundColor,
    enableDrag: enableDrag,
    isDismissible: isDismissible,
    containerWidget: (_, animation, child) => FloatingModal(
      child: child,
    ),
    expand: false,
  );

  return result;
}
