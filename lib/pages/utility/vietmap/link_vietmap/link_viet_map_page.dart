import 'dart:convert';

import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/utility/vietmap/link_vietmap/link_viet_map_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:vietmap_ecommerce_login/vietmap_ecommerce_login.dart';

import '../../../../commons/widgets/modal/success_dialog.dart';

class LinkVietMapPage extends StatefulWidget {
  const LinkVietMapPage({Key? key}) : super(key: key);

  @override
  State<LinkVietMapPage> createState() => _LinkVietMapPageState();
}

class _LinkVietMapPageState extends State<LinkVietMapPage> {
  bool isLinked = false;

  @override
  void initState() {
    super.initState();
    context.read<LinkVietMapBloc>().add(GetLinkVietMapStatus());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LinkVietMapBloc, LinkVietMapState>(
      listener: (context, state) {
        if (state is NotLinkedVietMap && (state.message?.isNotEmpty ?? false)){
          showErrorSnackBBar(context: context, message: state.message ?? '');
        }
        if (state is LinkedVietMap && (state.message?.isNotEmpty ?? false)){
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (dialogContext) {
              return SuccessDialog(
                content: 'Liên kết tài khoản Vietmap Live thành công',
                contentTextAlign: TextAlign.center,
                buttonTitle: 'Đóng',
                onButtonTap: () {
                  Navigator.of(dialogContext).pop();
                },
              );
            },
          );
        }
      },
      builder: (context, state) {
        return Container(
            padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 16.w),
            alignment: Alignment.topLeft,
            child: state is LinkedVietMap
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Tài khoản Vietmap Live đã liên kết',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 16.h, horizontal: 16.w),
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 0.5, color: ColorName.borderColor),
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.r))),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  'Số điện thoại',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(color: ColorName.borderColor),
                                ),
                                Expanded(
                                  child: Text(
                                    state.phone ?? '',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(color: ColorName.textGray),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 16.h,
                            ),
                            const Divider(
                              color: ColorName.borderColor,
                              height: 0.5,
                              thickness: 0.5,
                            ),
                            SizedBox(
                              height: 16.h,
                            ),
                            Row(
                              children: [
                                Text(
                                  'Họ và tên',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(color: ColorName.borderColor),
                                ),
                                Expanded(
                                  child: Text(
                                    state.name ?? '',
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(color: ColorName.textGray),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 32.h,
                      ),
                      SizedBox(
                          width: double.infinity,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                horizontal: 16.w,
                                vertical: 12.h,
                              ),
                              visualDensity: VisualDensity.standard,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.r),
                              ),
                              side: const BorderSide(
                                color: ColorName.indicatorColor,
                                width: 1.0,
                              ),
                              primary: ColorName.indicatorColor,
                            ),
                            onPressed: () async {
                              await showDialog(
                                context: context,
                                builder: (dialogContext) {
                                  return ConfirmDialog(
                                    title: 'Hủy liên kết Vietmap Live',
                                    content:
                                        'Bạn có chắc chắn muốn hủy liên kết tài khoản Vietmap Live dưới đây không?\n'
                                            'Họ tên: ${state.name ?? ''}\n'
                                            'Số điện thoại: ${state.phone ?? ''}\n'
                                            'Lưu ý: Sau khi hủy liên kết, bạn sẽ không thể dùng chức năng cảnh báo số dư tài khoản khi đi qua trạm.',
                                    contentTextAlign: TextAlign.left,
                                    secondaryButtonTitle: 'Từ chối',
                                    primaryButtonTitle: 'Đồng ý',
                                    onPrimaryButtonTap: () {
                                      Navigator.of(dialogContext).pop();
                                      context.read<LinkVietMapBloc>().add(UnLinkVietMap());
                                    },
                                    onSecondaryButtonTap: () async {
                                      Navigator.of(dialogContext).pop();
                                    },
                                  );
                                },
                              );
                            },
                            child: Text(
                              'Huỷ liên kết tài khoản',
                              style: Theme.of(context)
                                  .textTheme
                                  .button!
                                  .copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w700,
                                    color: ColorName.indicatorColor,
                                  ),
                            ),
                          ))
                    ],
                  )
                : Column(
                    children: [
                      Text(
                        'Bạn chưa liên kết tài khoản Vietmap Live. Vui lòng liên kết tài khoản để nhận được cảnh báo số dư tài khoản khi xe qua trạm và nhiều tiện ích liên kết khác.',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(
                                fontStyle: FontStyle.italic, height: 1.8),
                      ),
                      SizedBox(
                        height: 32.h,
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: PrimaryButton(
                          title: 'Liên kết ngay',
                          onTap: () async {
                            await showDialog(
                              context: context,
                              builder: (dialogContext) {
                                return ConfirmDialog(
                                  title: 'Thông báo',
                                  content:
                                      'Sau khi liên kết thành công, các thông tin về số dư tài khoản giao thông, loại xe, giá vé của phương tiện sẽ được đồng bộ với tài khoản Vietmap Live. Quý khách có muốn tiếp tục?',
                                  contentTextAlign: TextAlign.start,
                                  secondaryButtonTitle: 'Từ chối',
                                  primaryButtonTitle: 'Tiếp tục',
                                  onPrimaryButtonTap: () {
                                    Navigator.of(dialogContext).pop();
                                    _openVietMapLoginPage(context);
                                  },
                                  onSecondaryButtonTap: () async {
                                    Navigator.of(dialogContext).pop();
                                  },
                                );
                              },
                            );
                          },
                        ),
                      )
                    ],
                  ));
      },
    );
  }

  void _openVietMapLoginPage(BuildContext context){
    openVIETMAPEcommerceLoginScreen(context,
        onContextChanged: (loginScreenContext){

        },
        onLoggedIn: (loginScreenContext, authResult) {
          Navigator.of(loginScreenContext).pop();
          print(authResult);
          context.read<LinkVietMapBloc>().add(LinkVietMap(
              accountName: authResult.name,
              phoneNumber: authResult.phoneNumber       ,
              token: authResult.accessToken
          ));
        });
  }
}
