import 'package:epass/commons/api/clients/notification/notification_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/firebase_cloud_messaging/register_firebase_token_request.dart';
import 'package:epass/commons/models/notification/notification_msg.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:simple_result/simple_result.dart';

abstract class INotificationRepository {
  Future<Result<NotificationMsgDataNotNull, Failure>> getNotifications({
    int pageSize = 20,
    int startRecord = 0,
    String? firebaseToken,
    String? searchInput,
    int? notificationType,
  });

  Future<Result<String?, Failure>> updateNotification({
    required String notificationId,
    required UpdateNotificationViewStatusRequest updateViewStatusRequest,
  });

  Future<Result<String?, Failure>> registerFirebaseToken({
    required String contractId,
    required String contractNo,
    required String token,
    String? deviceType,
    required String deviceInfo,
  });
}

class NotificationRepository implements INotificationRepository {
  final NotificationClient _notificationClient;

  NotificationRepository({
    required NotificationClient notificationClient,
  }) : _notificationClient = notificationClient;

  @override
  Future<Result<NotificationMsgDataNotNull, Failure>> getNotifications({
    int pageSize = 20,
    int startRecord = 0,
    String? firebaseToken,
    String? searchInput,
    int? notificationType,
  }) async {
    try {
      final response = await _notificationClient.getNotifications(
        pageSize: pageSize,
        startRecord: startRecord,
        firebaseToken: firebaseToken,
        searchInput: searchInput,
        notificationType: notificationType,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;
      final totalUnread = response.totalUnread;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || count == null || listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(NotificationMsgDataNotNull(
        listData: listData,
        count: count,
        totalUnread: totalUnread ?? 0,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> updateNotification({
    required String notificationId,
    required UpdateNotificationViewStatusRequest updateViewStatusRequest,
  }) async {
    try {
      final response = await _notificationClient.updateNotificationViewStatus(
        notificationId: notificationId,
        requestBody: updateViewStatusRequest,
      );

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> registerFirebaseToken({
    required String contractId,
    required String contractNo,
    required String token,
    String? deviceType,
    required String deviceInfo,
  }) async {
    try {
      final request = RegisterFirebaseTokenRequest(
        contractId: contractId,
        contractNo: contractNo,
        token: token,
        deviceInfo: deviceInfo,
      );

      final response = await _notificationClient.tokenFirebase(requestBody: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
