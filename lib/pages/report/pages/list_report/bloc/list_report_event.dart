part of 'list_report_bloc.dart';

@immutable
abstract class ReportEvent extends Equatable {
  const ReportEvent();
}

class ReportFetched extends ReportEvent {
  final FeedbackChannel? reportChannel;

  const ReportFetched({this.reportChannel});

  @override
  List<Object?> get props => [reportChannel];
}

class ReportRefreshed extends ReportEvent {
  const ReportRefreshed();

  @override
  List<Object?> get props => [];
}