import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/enum/register_level.dart';
import 'package:epass/commons/models/register/document_file.dart';
import 'package:epass/commons/models/register/register_request.dart';
import 'package:epass/commons/models/register/register_vehicle_dto.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_logo_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/invoice_setting/invoice_cycle.dart';
import 'package:epass/pages/register/bloc/register_bloc.dart';
import 'package:epass/pages/register/bloc/register_customer/register_customer_bloc.dart';
import 'package:epass/pages/register/bloc/register_service/register_service_bloc.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/bloc/request_otp/register_otp_bloc.dart';
import 'package:epass/pages/register/pages/register_otp_confirm/widgets/otp_countdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class RegisterOTPPage extends StatefulWidget {
  const RegisterOTPPage({
    Key? key,
  }) : super(key: key);

  @override
  State<RegisterOTPPage> createState() => _RegisterOTPPageState();
}

class _RegisterOTPPageState extends State<RegisterOTPPage> {
  final _errorController = StreamController<ErrorAnimationType>();

  @override
  void dispose() {
    _errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final phoneNumber = context.read<RegisterCustomerBloc>().state.phoneNumber;

    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterSubmittedInProgress) {
          context.loaderOverlay.show();
        } else {
          context.loaderOverlay.hide();
        }
      },
      child: BaseLogoPage(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        child: Stack(
          children: [
            const GradientHeaderContainer(),
            SafeArea(
              child: FadeAnimation(
                delay: 1,
                direction: FadeDirection.up,
                child: RoundedTopContainer(
                  padding: EdgeInsets.fromLTRB(16.w, 48.h, 16.w, 0.h),
                  child: BlocConsumer<RegisterBloc, RegisterState>(
                    listener: (context, state) {
                      if (state is RegisterSubmittedInProgress) {
                        context.loaderOverlay.show();
                      } else if (state is RegisterSubmittedSuccess) {
                        context.loaderOverlay.hide();
                        showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (dialogContext) {
                            return SuccessDialog(
                              content: 'Bạn đã đăng ký thành công tài khoản trên ứng dụng ePass, chúng tôi sẽ liên hệ'
                                  ' với bạn sớm nhất để tạo hợp đồng và cung cấp mật khẩu đăng nhập ứng dụng.'
                                  '\n\nCảm ơn vì đã sử dụng dịch vụ của ePass.',
                              contentTextAlign: TextAlign.center,
                              buttonTitle: 'Hoàn thành',
                              onButtonTap: () {
                                Navigator.of(dialogContext).pop();
                                context.router.replaceAll([const LoginRoute()]);
                              },
                            );
                          },
                        );
                      } else if (state is RegisterSubmittedFailure) {
                        context.loaderOverlay.hide();
                        _errorController.add(ErrorAnimationType.shake);
                      }
                    },
                    builder: (context, state) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: double.infinity,
                            child: Text(
                              'Xác nhận',
                              style: Theme.of(context).textTheme.headline6!.copyWith(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(height: 24.h),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 2.w),
                            child: RichText(
                              text: TextSpan(
                                text: 'Nhập mã OTP đã được gửi đến số điện thoại ',
                                children: <TextSpan>[
                                  TextSpan(
                                    text: '${phoneNumber ?? ''}.',
                                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                          fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                ],
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            ),
                          ),
                          SizedBox(height: 24.h),
                          PinCodeTextField(
                            appContext: context,
                            errorAnimationController: _errorController,
                            length: 6,
                            obscureText: false,
                            animationType: AnimationType.fade,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.box,
                              fieldHeight: 46.r,
                              fieldWidth: 46.r,
                              borderRadius: BorderRadius.circular(8.0),
                              errorBorderColor: ColorName.error,
                              borderWidth: 1.0,
                              activeColor: ColorName.disabledBorderColor,
                              activeFillColor: Colors.white,
                              inactiveColor: ColorName.disabledBorderColor,
                              inactiveFillColor: Colors.white,
                              selectedColor: ColorName.disabledBorderColor,
                              selectedFillColor: Colors.white,
                            ),
                            backgroundColor: Colors.transparent,
                            autoDismissKeyboard: true,
                            autoFocus: true,
                            keyboardType: TextInputType.number,
                            textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.w600,
                                ),
                            onCompleted: _onOTPCompleted,
                            onChanged: (value) {},
                            beforeTextPaste: (text) {
                              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                              //but you can show anything you want here, like your pop up saying wrong paste format or etc
                              return false;
                            },
                          ),
                          if (state is RegisterSubmittedFailure) SizedBox(height: 8.h),
                          if (state is RegisterSubmittedFailure)
                            Text(
                              state.message,
                              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.error,
                                  ),
                            ),
                          SizedBox(height: 20.h),
                          BlocSelector<RegisterOtpBloc, RegisterOtpState, RegisterOtpRequestedSuccess?>(
                            selector: (state) => state is RegisterOtpRequestedSuccess ? state : null,
                            builder: (context, state) {
                              return state != null
                                  ? OTPCountdown(
                                      otpSubmittedTime: state.submittedSuccessTime,
                                      resubmitEvent: state.reRequestedEvent,
                                    )
                                  : const SizedBox.shrink();
                            },
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _onOTPCompleted(String otp) async {
    FocusScope.of(context).unfocus();

    // TODO: move the logic to bloc

    const stickType = '1'; // 1 - home, 0 - store

    // CUSTOMER INFO
    final customerInfo = context.read<RegisterCustomerBloc>().state;

    final name = customerInfo.name;
    final customerType = customerInfo.customerType?.custTypeId;
    final gender = customerInfo.gender;

    final identifier = customerInfo.documentNumber;

    final dob = customerInfo.dob;
    String? dobRaw;
    if (dob != null) {
      dobRaw = Jiffy(dob).format('dd/MM/yyyy');
    }

    final dateOfIssue = customerInfo.dateOfIssue;
    String? dateOfIssueRaw;
    if (dateOfIssue != null) {
      dateOfIssueRaw = Jiffy(dateOfIssue).format('dd/MM/yyyy');
    }

    final placeOfIssue = customerInfo.placeOfIssue;
    final phoneNumber = customerInfo.phoneNumber;

    // final province = customerInfo.province;
    // final district = customerInfo.district;
    final ward = customerInfo.ward;
    final address = customerInfo.address;

    // SERVICE
    final serviceInfo = context.read<RegisterServiceBloc>().state;
    final billCycle = serviceInfo.invoiceCycle;
    final billCycleMergeType = serviceInfo.invoiceCycleGroupType;

    // Contract profile DTO
    var contractProfileDTOs = <DocumentFile>[];

    final idImageFrontPath = customerInfo.idImageFrontPath;

    if (idImageFrontPath != null) {
      final file = File(idImageFrontPath);

      final bytes = await file.readAsBytes();
      final base64 = base64Encode(bytes);

      final fileName = file.path.split(Platform.pathSeparator).last;

      final idImageFront = DocumentFile(documentTypeId: '1', fileBase64: base64, fileName: fileName);

      contractProfileDTOs.add(idImageFront);
    }

    final idImageBackPath = customerInfo.idImageBackPath;
    if (idImageBackPath != null) {
      final file = File(idImageBackPath);

      final bytes = await file.readAsBytes();
      final base64 = base64Encode(bytes);

      final fileName = file.path.split(Platform.pathSeparator).last;

      final idImageBack = DocumentFile(documentTypeId: '1', fileBase64: base64, fileName: fileName);

      contractProfileDTOs.add(idImageBack);
    }

    // VEHICLES
    // Vehicles must not be empty
    final vehicles = context.read<RegisterVehiclesBloc>().state.listRegisterVehicle;
    final vehicle = vehicles[0];

    final vehicleList = vehicles.map((e) {
      var vehicleProfileDTOs = <DocumentFile>[];

      final regCertImage = e.addressInfo.regCertImage;
      if (regCertImage != null) {
        final file = File(regCertImage);

        final bytes = file.readAsBytesSync();
        final base64 = base64Encode(bytes);

        final fileName = file.path.split(Platform.pathSeparator).last;

        final regCertImageDoc = DocumentFile(documentTypeId: '17', fileBase64: base64, fileName: fileName);

        vehicleProfileDTOs.add(regCertImageDoc);
      }

      final vehicleRegCertImage = e.addressInfo.vehicleRegCertImage;
      if (vehicleRegCertImage != null) {
        final file = File(vehicleRegCertImage);

        final bytes = file.readAsBytesSync();
        final base64 = base64Encode(bytes);

        final fileName = file.path.split(Platform.pathSeparator).last;

        final vehicleRegCertImageDoc = DocumentFile(documentTypeId: '16', fileBase64: base64, fileName: fileName);

        vehicleProfileDTOs.add(vehicleRegCertImageDoc);
      }

      return RegisterVehicleDto(
        owner: e.vehicleInfo.owner,
        plateNumber: e.vehicleInfo.plateNumber,
        plateType: e.vehicleInfo.plateType?.id.toString(),
        plateTypeCode: e.vehicleInfo.plateType?.code,
        vehicleTypeId: e.vehicleInfo.vehicleType?.id.toString(),
        vehicleTypeCode: e.vehicleInfo.vehicleType?.code,
        vehicleTypeName: e.vehicleInfo.vehicleType?.name,
        vehicleProfileDTOs: vehicleProfileDTOs,
      );
    }).toList();

    final request = RegisterRequest(
      otp: otp,
      stickType: stickType,

      // customer info
      fullName: name,
      customerType: customerType.toString(),
      // '1' - CN
      gender: gender?.value,
      birthDate: dobRaw,
      identifier: identifier,
      dateOfIssue: dateOfIssueRaw,
      placeOfIssue: placeOfIssue,
      phoneNumber: phoneNumber,
      contractProfileDTOs: contractProfileDTOs,

      // vehicle info
      // TODO:
      province: vehicle.addressInfo.province?.province,
      provinceName: vehicle.addressInfo.province?.name,
      district: vehicle.addressInfo.district?.district,
      districtName: vehicle.addressInfo.district?.name,
      ward: vehicle.addressInfo.ward?.precinct,
      wardName: vehicle.addressInfo.ward?.name,
      street: address,
      areaCode: vehicle.addressInfo.ward?.areaCode,
      areaCodeEtc: ward?.areaCode,
      noticeStreet: vehicle.addressInfo.addressDetails,
      noticeAreaCode: vehicle.addressInfo.ward?.areaCode,
      regisLevel: vehicle.addressInfo.registerLevel?.value.toString(),
      vehicleList: vehicleList,

      // TODO:
      pushNotification: '1',
      emailNotification: '1',

      noticeName: name,
      noticePhoneNumber: phoneNumber,

      // service info
      billCycle: billCycle.value.toString(),
      billCycleMergeType: billCycleMergeType?.value.toString() ?? '',
    );

    context.read<RegisterBloc>().add(RegisterSubmitted(request));
  }
}
