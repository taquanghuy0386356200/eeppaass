part of 'vehicle_details_bloc.dart';

abstract class VehicleDetailsState extends Equatable {
  const VehicleDetailsState();
}

class VehicleDetailsInitial extends VehicleDetailsState {
  @override
  List<Object> get props => [];
}

class VehicleRFIDUpdatedInProgress extends VehicleDetailsState {
  const VehicleRFIDUpdatedInProgress();

  @override
  List<Object> get props => [];
}

class VehicleRFIDUpdatedSuccess extends VehicleDetailsState {
  const VehicleRFIDUpdatedSuccess();

  @override
  List<Object> get props => [];
}

class VehicleRFIDUpdatedFailure extends VehicleDetailsState {
  final String message;

  const VehicleRFIDUpdatedFailure(this.message);

  @override
  List<Object> get props => [message];
}
class VehicleCheckDebitFetchSuccess extends VehicleDetailsState {
  final VehicleDebitData? vehicleDebitData;
  const VehicleCheckDebitFetchSuccess({this.vehicleDebitData});

  @override
  List<Object> get props => [];
}
class VehicleDebitTransactionSuccess extends VehicleDetailsState {
  const VehicleDebitTransactionSuccess();

  @override
  List<Object> get props => [];
}
class VehicleDebitTransactionFailure extends VehicleDetailsState {
  final String message;
  final int?  code;

  const VehicleDebitTransactionFailure({required this.message, this.code});

  @override
  List<Object> get props => [message];
}