import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'policy_event.dart';
part 'policy_state.dart';

class PolicyBloc extends Bloc<PolicyEvent, PolicyState> {
  PolicyBloc() : super(const PolicyState()) {
    on<PolicyChecked>(_onPolicyChecked);
  }

  FutureOr<void> _onPolicyChecked(
      PolicyChecked event,
      Emitter<PolicyState> emit,
      ) async {

    emit(state.copyWith(
        isPolicyChecked: event.isPolicyChecked
    ));
  }

}
