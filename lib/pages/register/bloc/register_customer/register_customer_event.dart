part of 'register_customer_bloc.dart';

abstract class RegisterCustomerEvent extends Equatable {
  const RegisterCustomerEvent();
}

class RegisterCustomerUpdated extends RegisterCustomerEvent {
  final CustomerTypeModel? customerType;
  final String? idImageFrontPath;
  final String? idImageBackPath;
  final String? name;
  final DateTime? dob;
  final Gender? gender;
  final String? documentNumber;
  final String? placeOfIssue;
  final DateTime? dateOfIssue;
  final String? email;
  final String? phoneNumber;
  final String? address;
  final AddressVTT? province;
  final AddressVTT? district;
  final AddressVTT? ward;

  const RegisterCustomerUpdated({
    this.customerType,
    this.idImageFrontPath,
    this.idImageBackPath,
    this.name,
    this.dob,
    this.gender,
    this.documentNumber,
    this.placeOfIssue,
    this.dateOfIssue,
    this.email,
    this.phoneNumber,
    this.address,
    this.province,
    this.district,
    this.ward,
  });

  @override
  List<Object?> get props =>
      [
        customerType,
        idImageFrontPath,
        idImageBackPath,
        name,
        dob,
        gender,
        documentNumber,
        placeOfIssue,
        dateOfIssue,
        email,
        phoneNumber,
        address,
        province,
        district,
        ward,
      ];
}

class RegisterCustomerProvinceUpdated extends RegisterCustomerEvent {
  final AddressVTT? province;

  const RegisterCustomerProvinceUpdated(this.province);

  @override
  List<Object?> get props => [province];
}

class RegisterCustomerDistrictUpdated extends RegisterCustomerEvent {
  final AddressVTT? district;

  const RegisterCustomerDistrictUpdated(this.district);

  @override
  List<Object?> get props => [district];
}
