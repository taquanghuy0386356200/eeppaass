part of 'home_bloc.dart';

@immutable
abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoading extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoaded extends HomeState {
  final ContractPayment? contractPayment;
  final Campaign? popup;
  final Campaign? popupSMS;
  final List<Campaign>? banners;
  final List<String>? errors;
  final AppsModel? appsStore;
  final List<WaitingOrder>? waitingOrders;
  final bool? isConfirmOrderSuccess;
  final bool? isLoading;
  final bool checkUserRegisterSMS;
  final bool checkShowSpamSMS;

  const HomeLoaded(
      {this.contractPayment,
      this.popup,
      this.popupSMS,
      this.banners,
      this.errors,
      this.appsStore,
      this.waitingOrders,
      this.checkUserRegisterSMS = false,
      this.checkShowSpamSMS = false,
      this.isConfirmOrderSuccess,
      this.isLoading});

  @override
  List<Object?> get props => [
        popupSMS,
        contractPayment,
        popup,
        banners,
        errors,
        appsStore,
        waitingOrders,
        isConfirmOrderSuccess,
        isLoading,
        checkUserRegisterSMS,
        checkShowSpamSMS,
      ];

  HomeLoaded copyWith(
      {ContractPayment? contractPayment,
      Campaign? popup,
      Campaign? popupSMS,
      List<Campaign>? banners,
      List<String>? errors,
      AppsModel? appsStore,
      List<WaitingOrder>? waitingOrders,
      bool? isConfirmOrderSuccess,
      bool? checkShowSpamSMS,
      bool? checkUserRegisterSMS,
      bool? isLoading}) {
    return HomeLoaded(
      contractPayment: contractPayment ?? this.contractPayment,
      checkUserRegisterSMS: checkUserRegisterSMS ?? this.checkUserRegisterSMS,
      popup: popup,
      banners: banners ?? this.banners,
      checkShowSpamSMS: checkShowSpamSMS ?? this.checkShowSpamSMS,
      errors: errors ?? this.errors,
      popupSMS: popupSMS ?? this.popupSMS,
      appsStore: appsStore ?? this.appsStore,
      waitingOrders: waitingOrders,
      isConfirmOrderSuccess: isConfirmOrderSuccess,
      isLoading: isLoading ?? false,
    );
  }
}
