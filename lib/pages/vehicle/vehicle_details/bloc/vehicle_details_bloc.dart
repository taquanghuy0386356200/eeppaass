import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/vehicle/vehicle_debit.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'vehicle_details_event.dart';

part 'vehicle_details_state.dart';

class VehicleDetailsBloc
    extends Bloc<VehicleDetailsEvent, VehicleDetailsState> {
  final IVehicleRepository _vehicleRepository;
  final AppBloc _appBloc;

  VehicleDetailsBloc({
    required IVehicleRepository vehicleRepository,
    required AppBloc appBloc,
  })  : _vehicleRepository = vehicleRepository,
        _appBloc = appBloc,
        super(VehicleDetailsInitial()) {
    on<VehicleRFIDUpdated>(_onVehicleRFIDUpdated);
    on<VehicleCheckDebitFetched>(_onVehicleCheckDebitFetched);
    on<VehicleDebitTransactionFetched>(_onVehicleDebtTransactionFetched);
  }

  FutureOr<void> _onVehicleRFIDUpdated(
    VehicleRFIDUpdated event,
    Emitter<VehicleDetailsState> emit,
  ) async {
    emit(const VehicleRFIDUpdatedInProgress());

    final custId = _appBloc.state.user?.customerId;

    if (custId != null) {
      final result = await _vehicleRepository.updateVehicleRFIDStatus(
        vehicleId: event.vehicleId,
        isOpen: event.isOpen,
        custId: custId,
      );

      result.when(
        success: (_) => emit(const VehicleRFIDUpdatedSuccess()),
        failure: (failure) =>
            emit(VehicleRFIDUpdatedFailure(failure.message ?? 'Có lỗi xảy ra')),
      );
    } else {
      emit(const VehicleRFIDUpdatedFailure('Có lỗi xảy ra'));
    }
  }

  FutureOr<void> _onVehicleCheckDebitFetched(
      VehicleCheckDebitFetched event,
    Emitter<VehicleDetailsState> emit,
  ) async {
    emit(const VehicleRFIDUpdatedInProgress());
    final result = await _vehicleRepository.getVehicleDebit(
      contractId: event.contractId,
      vehicleId: event.vehicleId,
    );

    result.when(
      success: (_) =>
          emit(VehicleCheckDebitFetchSuccess(vehicleDebitData: result.success)),
      failure: (failure) =>
          emit(VehicleRFIDUpdatedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onVehicleDebtTransactionFetched(
    VehicleDebitTransactionFetched event,
    Emitter<VehicleDetailsState> emit,
  ) async {
    emit(const VehicleRFIDUpdatedInProgress());
    final result = await _vehicleRepository.getVehicleTransactionDebit(
      vehicleId: event.vehicleId,
    );

    result.when(
      success: (_) => emit(const VehicleDebitTransactionSuccess()),
      failure: (failure) =>
          emit(VehicleDebitTransactionFailure(message: failure.message ?? 'Có lỗi xảy ra', code: failure.code)),
    );
  }
}
