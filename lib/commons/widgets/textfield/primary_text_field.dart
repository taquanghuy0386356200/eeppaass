import 'package:epass/commons/widgets/textfield/currency_input_formatter.dart';
import 'package:epass/commons/widgets/textfield/text_field_circular_progress_indicator.dart';
import 'package:epass/commons/widgets/textfield/uppercase_input_formatter.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/assets.gen.dart';

class PrimaryTextField extends StatefulWidget {
  final TextEditingController? controller;
  final String? initialValue;
  final bool enabled;
  final bool readonly;
  final bool isDateType;
  final bool autofocus;
  final bool isMandatory;
  final ValueChanged<String?>? onChanged;
  final VoidCallback? onTap;
  final VoidCallback? onEditingComplete;
  final VoidCallback? onClear;
  final String? labelText;
  final String? hintText;
  final String? helperText;
  final FocusNode? focusNode;
  final String? errorText;
  final int maxLength;
  final int? maxLines;
  final TextInputType? inputType;
  final String? Function(String? value)? validator;
  final Widget? suffix;
  final bool obscureText;
  final bool enableSuggestions;
  final bool autocorrect;
  final bool needExpandLabel;
  final bool isCurrency;
  final bool isPlateNumber;
  final double fontSize;
  final bool hasClearButton;
  final bool isLoading;
  final bool highLight;
  final bool isDouble;
  final TextInputAction? inputAction;
  final bool isAlignLabelWithHint;

  const PrimaryTextField({
    Key? key,
    this.controller,
    this.initialValue,
    this.enabled = true,
    this.needExpandLabel = false,
    this.isDouble = false,
    this.readonly = false,
    this.isDateType = false,
    this.autofocus = false,
    this.isMandatory = false,
    this.highLight = false,
    this.onChanged,
    this.onTap,
    this.onEditingComplete,
    this.onClear,
    this.labelText,
    this.hintText,
    this.helperText,
    this.focusNode,
    this.errorText,
    this.maxLength = 200,
    this.maxLines = 1,
    this.inputType,
    this.validator,
    this.suffix,
    this.fontSize = 18,
    this.inputAction,
    this.obscureText = false,
    this.enableSuggestions = false,
    this.autocorrect = false,
    this.isCurrency = false,
    this.isPlateNumber = false,
    this.hasClearButton = true,
    this.isLoading = false,
    this.isAlignLabelWithHint = true,
  }) : super(key: key);

  @override
  State<PrimaryTextField> createState() => _PrimaryTextFieldState();
}

class _PrimaryTextFieldState extends State<PrimaryTextField> {
  late List<TextInputFormatter> _inputFormatter;
  late String? labelText;

  @override
  void initState() {
    super.initState();
    labelText = widget.labelText;
    _inputFormatter = [
      LengthLimitingTextInputFormatter(widget.maxLength),
    ];
    if (widget.isCurrency) {
      _inputFormatter.add(CurrencyFormatter());
    }
    if (widget.isPlateNumber) {
      _inputFormatter.add(UpperCaseInputFormatter());
    }
    if (widget.isDouble) {
      _inputFormatter
          .add(FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d{0,2}')));
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget suffixOrLoading;
    if (widget.isLoading) {
      suffixOrLoading = const TextFieldCircularProgressIndicator();
    } else if (widget.suffix != null) {
      suffixOrLoading = widget.suffix!;
    } else {
      suffixOrLoading = const SizedBox.shrink();
    }

    final suffixIcon = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        !widget.hasClearButton || (widget.controller?.text.isEmpty ?? true)
            ? const SizedBox.shrink()
            : GestureDetector(
                onTap: () {
                  widget.controller?.clear();
                  widget.onClear?.call();
                  setState(() {});
                },
                child: Assets.icons.roundedClear.svg(),
              ),
        widget.hasClearButton && (widget.suffix != null || widget.isLoading)
            ? SizedBox(width: 12.w)
            : const SizedBox.shrink(),
        suffixOrLoading,
        SizedBox(width: 20.w),
      ],
    );

    return TextFormField(
      controller: widget.controller,
      initialValue: widget.initialValue,
      enabled: widget.enabled,
      readOnly: widget.readonly,
      autofocus: widget.autofocus,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: widget.validator,
      textInputAction: widget.inputAction,
      keyboardType: widget.inputType,
      focusNode: widget.focusNode,
      obscureText: widget.obscureText,
      enableSuggestions: widget.enableSuggestions,
      autocorrect: widget.autocorrect,
      inputFormatters: _inputFormatter,
      style: widget.isDateType
          ? (Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(fontSize: (widget.fontSize).sp)
              .copyWith(color: (widget.highLight ? Colors.red : null)))
          : Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(fontSize: (widget.fontSize).sp)
              .copyWith(
                color: widget.readonly
                    ? const Color(0xff333333).withOpacity(0.8)
                    : (widget.highLight ? Colors.red : null),
              ),
      maxLines: widget.maxLines,
      onChanged: (value) {
        widget.onChanged?.call(value);
        setState(() {});
      },
      onTap: widget.onTap,
      onEditingComplete: widget.onEditingComplete,
      decoration: InputDecoration(
        isDense: true,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: EdgeInsets.fromLTRB(13.w, 16.h, 13.w, 16.h),
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            widget.needExpandLabel
                ? Expanded(
                    child: Text(
                      labelText ?? '',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontSize: (widget.fontSize).sp,
                            overflow: TextOverflow.clip,
                          ),
                    ),
                  )
                : Text(
                    labelText ?? '',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontSize: (widget.fontSize).sp,
                          overflow: TextOverflow.clip,
                        ),
                  ),
            widget.isMandatory
                ? const Padding(
                    padding: EdgeInsets.all(3.0),
                  )
                : const SizedBox(),
            widget.isMandatory
                ? Text('*',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(fontSize: (widget.fontSize).sp)
                        .copyWith(color: Colors.red))
                : const SizedBox.shrink(),
          ],
        ),
        hintStyle: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(fontSize: (widget.fontSize).sp)
            .copyWith(
              color: const Color(0xffbdb9b1),
            ),
        hintText: widget.hintText,
        errorText: widget.errorText,
        alignLabelWithHint: widget.isAlignLabelWithHint,
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: ColorName.borderColor),
          borderRadius: BorderRadius.circular(16.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: ColorName.primaryColor),
          borderRadius: BorderRadius.circular(16.0),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: ColorName.error),
          borderRadius: BorderRadius.circular(16.0),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: ColorName.error),
          borderRadius: BorderRadius.circular(16.0),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: ColorName.disabledBorderColor),
          borderRadius: BorderRadius.circular(16.0),
        ),
        errorMaxLines: 2,
        errorStyle: TextStyle(
          fontSize: 12.sp,
          color: ColorName.error,
        ),
        suffixIcon: widget.hasClearButton || widget.suffix != null
            ? suffixIcon
            : null, //khi không cần clear button thì ko hiện suffix
      ),
    );
  }
}
