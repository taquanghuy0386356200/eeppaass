part of 'feedback_bloc.dart';

@immutable
class FeedbackState extends Equatable {
  final List<FeedbackModel> listFeedback;
  final FeedbackChannel feedbackChannel;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;

  const FeedbackState({
    required this.listFeedback,
    this.feedbackChannel = FeedbackChannel.suggestion,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
  });

  @override
  List<Object?> get props => [
        listFeedback,
        feedbackChannel,
        error,
        isLoading,
        isRefreshing,
      ];

  FeedbackState copyWith({
    List<FeedbackModel>? listFeedback,
    FeedbackChannel? feedbackChannel,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
  }) {
    return FeedbackState(
      listFeedback: listFeedback ?? this.listFeedback,
      feedbackChannel: feedbackChannel ?? this.feedbackChannel,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
    );
  }
}
