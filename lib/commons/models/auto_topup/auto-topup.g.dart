// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auto-topup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AutoTopupResponse _$AutoTopupResponseFromJson(Map<String, dynamic> json) =>
    AutoTopupResponse(
      data: json['data'] == null
          ? null
          : AutoTopupRequest.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AutoTopupResponseToJson(AutoTopupResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

AutoTopupRequest _$AutoTopupRequestFromJson(Map<String, dynamic> json) =>
    AutoTopupRequest(
      autoConfigTopupId: json['autoConfigTopupId'] as int?,
      contractId: json['contractId'] as int?,
      amount: json['amount'] as int?,
      limit: json['limit'] as int?,
      topupType: json['topupType'] as int?,
      topupChannel: json['topupChannel'] as String?,
      isActive: json['isActive'] as int?,
      accountNumber: json['accountNumber'] as String?,
      accountName: json['accountName'] as String?,
      documentNumber: json['documentNumber'] as String?,
    );

Map<String, dynamic> _$AutoTopupRequestToJson(AutoTopupRequest instance) =>
    <String, dynamic>{
      'autoConfigTopupId': instance.autoConfigTopupId,
      'contractId': instance.contractId,
      'amount': instance.amount,
      'limit': instance.limit,
      'topupType': instance.topupType,
      'topupChannel': instance.topupChannel,
      'isActive': instance.isActive,
      'accountNumber': instance.accountNumber,
      'accountName': instance.accountName,
      'documentNumber': instance.documentNumber,
    };
