import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/enum/cash_in_for.dart';
import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/showcase/custom_showcase.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/validators/cash_in_page_validator.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/widget/quick_cash_in_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../commons/validators/currency_validator.dart';

class CashInInfoCard extends StatefulWidget {
  final GlobalKey targetShowCaseKey;
  final GlobalKey amountShowCaseKey;
  final GlobalKey feeShowCaseKey;
  //final GlobalKey autoTopupShowCaseKey;

  const CashInInfoCard({
    Key? key,
    required this.targetShowCaseKey,
    required this.amountShowCaseKey,
    required this.feeShowCaseKey,
    //   required this.autoTopupShowCaseKey,
  }) : super(key: key);

  @override
  State<CashInInfoCard> createState() => _CashInInfoCardState();
}

class _CashInInfoCardState extends State<CashInInfoCard>
    with CurrencyValidator {
  final _targetController = TextEditingController();
  final _cashInAmountController = TextEditingController();

  @override
  void dispose() {
    _targetController.dispose();
    _cashInAmountController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    context.read<CashInAmountBloc>().add(CashInTargetLastRecharge());
    _cashInAmountController.addListener(
      () {
        var intValue =
            int.tryParse(_cashInAmountController.text.replaceAll('.', ''));
        context.read<CashInAmountBloc>().add(CashInAmountChanged(intValue));
      },
    );
    _targetController.addListener(() {
      var target = _targetController.text;
      context.read<CashInTargetBloc>().add(CashInTargetValidated(target));
    });
  }

  @override
  Widget build(BuildContext context) {
    final amount = context.select((CashInAmountBloc bloc) {
      final state = bloc.state;
      return state is CashInAmountChangedSuccess
          ? state.amount?.toInt().formatCurrency()
          : null;
    });
    if (amount != null && _cashInAmountController.text.isEmpty) {
      _cashInAmountController.text = amount.toString();
    }
    return ShadowCard(
      shadowOpacity: 0.2,
      child: Padding(
        padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 16.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // CONTRACT NO
            CustomShowcase(
              showCaseKey: widget.targetShowCaseKey,
              description:
                  'Lựa chọn tài khoản cần nạp tiền. Bạn có thể nạp tiền'
                  ' cho tài khoản ePass khác thông qua số hợp đồng hoặc biển số xe',
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RadioGroupButtons<CashInFor>(
                    labels: CashInFor.values.map((e) => e.toLabel()).toList(),
                    values: CashInFor.values,
                    defaultValue: CashInFor.me,
                    onChanged: (value) {
                      _targetController.text = '';
                      FocusScope.of(context).unfocus();
                      context
                          .read<CashInTargetBloc>()
                          .add(CashInForChanged(value ?? CashInFor.me));
                    },
                  ),
                  BlocBuilder<CashInTargetBloc, CashInTargetState>(
                    builder: (context, state) {
                      if (state.cashInFor == CashInFor.me) {
                        _targetController.text = state.target;
                      }
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          state.cashInFor == CashInFor.other
                              ? Column(
                                  children: [
                                    SizedBox(height: 24.h),
                                    PrimaryTextField(
                                      controller: _targetController,
                                      validator: context
                                          .read<CashInTargetBloc>()
                                          .contractNoOrPlateNoValidator,
                                      labelText: 'Số hợp đồng/Biển số xe',
                                      hintText: 'V012345678/30H12345',
                                      isPlateNumber: true,
                                      helperText: state.helperText,
                                      errorText: state.error,
                                      maxLength:
                                          CashInPageValidators.plateNoMaxLength,
                                      readonly: state.cashInFor == CashInFor.me,
                                      hasClearButton:
                                          state.cashInFor != CashInFor.me,
                                    ),
                                  ],
                                )
                              : const SizedBox.shrink(),
                        ],
                      );
                    },
                  )
                ],
              ),
            ),
            SizedBox(height: 20.h),

            // AMOUNT
            CustomShowcase(
              showCaseKey: widget.amountShowCaseKey,
              overlayPadding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 16.h),
              description: 'Nhập hoặc chọn nhanh số tiền cần nạp',
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PrimaryTextField(
                    controller: _cashInAmountController,
                    labelText: 'Số tiền',
                    hintText: 'Nhập số tiền muốn nạp',
                    maxLength: 13,
                    inputType: TextInputType.number,
                    isCurrency: true,
                    suffix: const Text('₫'),
                    validator: currencyValidator,
                  ),
                  SizedBox(height: 20.h),
                  Padding(
                    padding: EdgeInsets.only(left: 6.w),
                    child: Text(
                      'Hoặc chọn nhanh số tiền',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2!
                          .copyWith(color: ColorName.textGray1),
                    ),
                  ),
                  SizedBox(height: 12.h),
                  BlocBuilder<CashInAmountBloc, CashInAmountState>(
                    builder: (context, state) {
                      return QuickCashInWidget(
                        selectedValue:
                            (state as CashInAmountChangedSuccess).amount,
                        onChange: (value) {
                          _cashInAmountController.text =
                              value?.formatCurrency() ?? '';
                          _cashInAmountController.selection =
                              TextSelection.collapsed(
                            offset: _cashInAmountController.text.length,
                          );
                          context
                              .read<CashInAmountBloc>()
                              .add(CashInAmountChanged(value));
                        },
                      );
                    },
                  ),
                ],
              ),
            ),

            // CASH IN FEE
            SizedBox(height: 10.h),
            // Row(
            //   children: [
            //     Text(
            //       'Phương thức thanh toán chỉ định',
            //       style: Theme.of(context).textTheme.bodyText2!.copyWith(
            //             color: ColorName.textGray1,
            //           ),
            //     ),
            //     SizedBox(width: 5.h),
            //     Tooltip(
            //       message:
            //           'Là phương thức thanh toán thành công gần đây nhất của người dùng',
            //       child: SizedBox(
            //         child: Icon(Icons.info_outline,
            //             size: 20.r, color: ColorName.primaryColor),
            //       ),
            //     ),
            //   ],
            // ),
            // SizedBox(height: 10.h),
            // CASH IN FEE
            // SizedBox(
            //     height: 70.h,
            //     child: Container(
            //       decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(12.0),
            //           border: Border.all(color: Colors.blueAccent)),
            //       child: Row(children: [
            //         SizedBox(width: 20.w),
            //         PaymentMethodsIcon(
            //           topupChannelCode: TopupChannelCode.ViettelPay,
            //           width: 45.h,
            //         ),
            //         SizedBox(width: 28.w),
            //         Column(
            //           children: [
            //             SizedBox(height: 14.h),
            //             Text(
            //               'Ví điện tử Viettel Money',
            //               style:
            //                   Theme.of(context).textTheme.subtitle1!.copyWith(
            //                         color: ColorName.textGray1,
            //                         fontSize: 14.sp,
            //                         fontWeight: FontWeight.w600,
            //                       ),
            //             ),
            //             Text(
            //               'Số điện thoại: 0941254879',
            //               style:
            //                   Theme.of(context).textTheme.subtitle1!.copyWith(
            //                         color: ColorName.textGray1,
            //                         fontSize: 14.sp,
            //                         fontWeight: FontWeight.w600,
            //                       ),
            //             ),
            //           ],
            //         ),
            //         SizedBox(width: 12.w),
            //       ]),
            //     )),
            SizedBox(height: 12.h),
            Row(
              children: [
                // CustomShowcase(
                //   showCaseKey: widget.autoTopupShowCaseKey,
                //   overlayPadding: EdgeInsets.fromLTRB(0.w, 0.h, 0.w, 0.h),
                //   description: 'Bạn có thể cấu hình nạp tiền tự động tại đây',
                //   child: SizedBox(
                //     height: 35.h,
                //     child: Container(
                //       decoration: BoxDecoration(
                //           borderRadius: BorderRadius.circular(12.0),
                //           gradient: const LinearGradient(
                //             colors: <Color>[
                //               ColorName.homeSecondaryButtonGradientStart,
                //               ColorName.homeSecondaryButtonGradientEnd,
                //             ],
                //           ),
                //           border: Border.all(color: Colors.orange)),
                //       child: TextButton(
                //           style: TextButton.styleFrom(
                //             padding: EdgeInsets.symmetric(
                //               horizontal: 6.w,
                //               vertical: 6.h,
                //             ),
                //             visualDensity: VisualDensity.standard,
                //             shape: RoundedRectangleBorder(
                //               borderRadius: BorderRadius.circular(12.0),
                //             ),
                //           ),
                //           onPressed: () async {
                //             await Navigator.push(
                //                 context,
                //                 MaterialPageRoute(
                //                     builder: (context) =>
                //                         const AutoTopupPage()));
                //           },
                //           child: Row(
                //             children: [
                //               Assets.icons.hdrAuto
                //                   .svg(width: 30.r, height: 30.r),
                //               Text(
                //                 'Nạp tiền tự động',
                //                 style: Theme.of(context)
                //                     .textTheme
                //                     .button!
                //                     .copyWith(
                //                       fontSize: 13.sp,
                //                       fontWeight: FontWeight.w700,
                //                       color: Colors.grey.shade800,
                //                     ),
                //               ),
                //             ],
                //           )),
                //     ),
                //   ),
                // ),
                const Expanded(
                  child: SizedBox.shrink(),
                ),
                CustomShowcase(
                  showCaseKey: widget.feeShowCaseKey,
                  overlayPadding: EdgeInsets.fromLTRB(0.w, 0.h, 0.w, 0.h),
                  description: 'Bạn có thể xem biểu phí giao dịch các hình thức'
                      ' nạp tiền ePass tại đây',
                  child: SizedBox(
                    height: 35.h,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.0),
                        gradient: const LinearGradient(
                          colors: <Color>[
                            ColorName.homeSecondaryButtonGradientStart,
                            ColorName.homeSecondaryButtonGradientEnd,
                          ],
                        ),
                        border: Border.all(color: Colors.orange),
                      ),
                      child: TextButton(
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.symmetric(
                              horizontal: 6.w,
                              vertical: 6.h,
                            ),
                            visualDensity: VisualDensity.standard,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                          ),
                          onPressed: () =>
                              context.pushRoute(const CashInFeeRoute()),
                          child: Row(
                            children: [
                              Assets.icons.fee.image(width: 30.r, height: 30.r),
                              Text(
                                'Biểu phí giao dịch',
                                style: Theme.of(context)
                                    .textTheme
                                    .button!
                                    .copyWith(
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.grey.shade800,
                                    ),
                              ),
                            ],
                          )),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
