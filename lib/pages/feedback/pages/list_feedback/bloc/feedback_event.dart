part of 'feedback_bloc.dart';

@immutable
abstract class FeedbackEvent extends Equatable {
  const FeedbackEvent();
}

class FeedbackFetched extends FeedbackEvent {
  final FeedbackChannel? feedbackChannel;

  const FeedbackFetched({this.feedbackChannel});

  @override
  List<Object?> get props => [feedbackChannel];
}

class FeedbackRefreshed extends FeedbackEvent {
  const FeedbackRefreshed();

  @override
  List<Object?> get props => [];
}