import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashIconButton extends StatelessWidget {
  final Widget icon;
  final Color? backgroundColor;
  final EdgeInsetsGeometry? padding;
  final void Function()? onTap;

  const SplashIconButton({
    Key? key,
    required this.icon,
    this.onTap,
    this.backgroundColor, this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: backgroundColor ?? Colors.transparent,
      shape: const CircleBorder(),
      child: InkWell(
        customBorder: const CircleBorder(),
        onTap: onTap,
        child: Container(
          padding: padding ?? EdgeInsets.all(10.r),
          child: icon,
        ),
      ),
    );
  }
}
