import 'package:epass/commons/widgets/buttons/list_button.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BiometricLoginSetting extends StatelessWidget {
  const BiometricLoginSetting({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListButton(
      leading: Assets.icons.quickLogin.svg(
        width: 20.r,
        height: 20.r,
      ),
      title: 'Đăng nhập nhanh',
      trailing: SizedBox(
        height: 24.h,
        child: SizedBox(
          width: 40.w,
          child: Transform.scale(
            scale: 0.8,
            transformHitTests: false,
            child: BlocSelector<SettingBloc, SettingState, bool>(
              selector: (state) => state.isBiometricLoginEnabled,
              builder: (context, isBiometricLoginEnabled) {
                return CupertinoSwitch(
                  value: isBiometricLoginEnabled,
                  activeColor: ColorName.primaryColor,
                  onChanged: (value) => context
                      .read<SettingBloc>()
                      .add(BiometricLoginToggled(value)),
                );
              },
            ),
          ),
        ),
      ),
      // onTap: () {},
    );
  }
}
