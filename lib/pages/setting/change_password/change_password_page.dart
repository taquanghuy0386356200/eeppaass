import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/setting/change_password/bloc/change_password_form_bloc.dart';
import 'package:epass/pages/setting/change_password/widgets/change_password_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: 'Đổi mật khẩu',
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          const GradientHeaderContainer(),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                  // Your widget go below
                  child: BlocProvider(
                    create: (_) => ChangePasswordFormBloc(
                      userRepository: getIt<IUserRepository>(),
                    ),
                    child: const ChangePasswordForm(),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
