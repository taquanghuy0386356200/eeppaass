import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:simple_result/simple_result.dart';

part 'feedback_event.dart';

part 'feedback_state.dart';

class ListFeedbackBloc extends FeedbackBloc {
  ListFeedbackBloc({
    FeedbackBloc? feedbackBloc,
    required IFeedbackRepository feedbackRepository,
    required FirebaseMessaging firebaseMessaging,
  }) : super(feedbackBloc: feedbackBloc, feedbackRepository: feedbackRepository);
}

class FeedbackBloc extends Bloc<FeedbackEvent, FeedbackState> {
  final IFeedbackRepository _feedbackRepository;

  FeedbackBloc({
    FeedbackBloc? feedbackBloc,
    required IFeedbackRepository feedbackRepository,
  })  : _feedbackRepository = feedbackRepository,
        super(const FeedbackState(listFeedback: [])) {
    on<FeedbackFetched>(_onFeedbackFetched);
    on<FeedbackRefreshed>(_onFeedbackRefreshed);
  }

  FutureOr<void> _onFeedbackFetched(
    FeedbackFetched event,
    Emitter<FeedbackState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
      listFeedback: const [],
      error: null,
    ));

    final result = await _getFeedback();

    result.when(
      success: (data) {
        emit(
          state.copyWith(
            isLoading: false,
            listFeedback: data,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
      )),
    );
  }

  Future<void> _onFeedbackRefreshed(FeedbackRefreshed event, Emitter<FeedbackState> emit) async {
    emit(state.copyWith(
      isRefreshing: true,
      error: null,
    ));

    final result = await _getFeedback();

    result.when(
      success: (data) {
        emit(
          state.copyWith(
            isRefreshing: false,
            listFeedback: data,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
      )),
    );
  }

  Future<Result<List<FeedbackModel>, Failure>> _getFeedback() async {
    final result = await _feedbackRepository.getFeedback(
      feedbackChannel: state.feedbackChannel,
    );
    return result;
  }
}
