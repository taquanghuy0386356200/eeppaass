// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'motobike_register_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MotobikeRegisterRequest _$MotobikeRegisterRequestFromJson(
        Map<String, dynamic> json) =>
    MotobikeRegisterRequest(
      plateNumber: json['plateNumber'] as String?,
      plateType: json['plateType'] as int?,
      vehicleProfiles: (json['vehicleProfiles'] as List<dynamic>)
          .map((e) => VehicleProfile.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MotobikeRegisterRequestToJson(
        MotobikeRegisterRequest instance) =>
    <String, dynamic>{
      'plateNumber': instance.plateNumber,
      'plateType': instance.plateType,
      'vehicleProfiles': instance.vehicleProfiles,
    };

VehicleProfile _$VehicleProfileFromJson(Map<String, dynamic> json) =>
    VehicleProfile(
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      fileBase64: json['fileBase64'] as String?,
    );

Map<String, dynamic> _$VehicleProfileToJson(VehicleProfile instance) =>
    <String, dynamic>{
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'fileBase64': instance.fileBase64,
    };
