import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';

class PaymentMethodsIcon extends StatelessWidget {
  final TopupChannelCode topupChannelCode;
  final double? height;
  final double? width;

  const PaymentMethodsIcon({
    Key? key,
    required this.topupChannelCode,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (topupChannelCode) {
      case TopupChannelCode.BankPlus:
        return Assets.icons.bankPlus.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.DomesticATM:
        return Assets.icons.domesticCard.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.InternationalCard:
        return Assets.icons.interCard.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.ViettelPay:
        return Assets.icons.viettelMoneyRect.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.MoMo:
        return Assets.icons.momo.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.VNPAY:
        return Assets.icons.vnpay.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.BankTransfer:
        return Assets.icons.moneyTransfer.svg(
          height: height,
          width: width,
        );
      case TopupChannelCode.PayNow:
        return Assets.icons.paynow.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.MobileMoney:
        return Assets.icons.mobileMoney.svg(
          height: height,
          width: width,
        );
      case TopupChannelCode.MB:
        return Assets.icons.mBBank.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.BIDV:
        return Assets.icons.bidvIcon.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.VPBank:
        return Assets.icons.vPBank.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.Agribank:
        return Assets.icons.agribank.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.TPBank:
        return Assets.icons.tPBank.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.VIB:
        return Assets.icons.vib.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.OCBBank:
        return Assets.icons.ocb.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.PVcomBank:
        return Assets.icons.pVcomBank.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.MSB:
        return Assets.icons.msb.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.ZaloPay:
        return Assets.icons.zaloPay.image(
          height: height,
          width: width,
        );
      case TopupChannelCode.VnptMoney:
        return Assets.icons.vnptMoney.image(
          height: height,
          width: width,
        );
      default:
        return const SizedBox.shrink();
    }
  }
}
