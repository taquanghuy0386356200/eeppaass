import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/contract_action/contract_action.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'contract_action_event.dart';

part 'contract_action_state.dart';

class ContractActionBloc
    extends Bloc<ContractActionEvent, ContractActionState> {
  final AppBloc _appBloc;
  final ITransactionHistoryRepository _transRepository;

  final int _pageSize;
  int _startRecord = 0;
  bool isSearching = false;

  ContractActionBloc({
    required ITransactionHistoryRepository transactionHistoryRepository,
    required AppBloc appBloc,
    int pageSize = 10,
  })  : _pageSize = pageSize,
        _appBloc = appBloc,
        _transRepository = transactionHistoryRepository,
        super(const ContractActionState(listData: [])) {
    on<ContractActionFetched>(_onContractActionFetched);
    on<ContractActionRefreshed>(_onContractActionRefreshed);
    on<ContractActionLoadMore>(_onContractActionLoadMore);
    on<ContractActionStartDateChanged>(_onContractActionStartDateChanged);
    on<ContractActionEndDateChanged>(_onContractActionEndDateChanged);
    on<ContractActionSearched>(_onContractActionSearched);
  }

  FutureOr<void> _onContractActionFetched(
    ContractActionFetched event,
    Emitter<ContractActionState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      startDate: null,
      endDate: null,
      error: null,
    ));

    final result = await _getContractAction();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            startDate: null,
            endDate: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        startDate: null,
        endDate: null,
      )),
    );
  }

  FutureOr<void> _onContractActionRefreshed(
    ContractActionRefreshed event,
    Emitter<ContractActionState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      startDate: state.startDate,
      endDate: state.endDate,
      error: null,
    ));

    final result = await _getContractAction();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            startDate: state.startDate,
            endDate: state.endDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
        startDate: state.startDate,
        endDate: state.endDate,
      )),
    );
  }

  FutureOr<void> _onContractActionLoadMore(
    ContractActionLoadMore event,
    Emitter<ContractActionState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      startDate: state.startDate,
      endDate: state.endDate,
      error: null,
    ));

    final result = await _getContractAction();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(
          state.copyWith(
            isLoadingMore: false,
            listData: listData,
            isFull: listData.length == data.count,
            startDate: state.startDate,
            endDate: state.endDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        error: failure.message,
        startDate: state.startDate,
        endDate: state.endDate,
      )),
    );
  }

  FutureOr<void> _onContractActionSearched(
    ContractActionSearched event,
    Emitter<ContractActionState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      startDate: event.startDate,
      endDate: event.endDate,
      error: null,
    ));

    final result = await _getContractAction();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            startDate: state.startDate,
            endDate: state.endDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        startDate: state.startDate,
        endDate: state.endDate,
      )),
    );
  }

  Future<Result<ContractActionDataNotNull, Failure>>
      _getContractAction() async {
    final result = await _transRepository.getContractAction(
      pageSize: _pageSize,
      startRecord: _startRecord,
      customerId: _appBloc.state.user?.customerId ?? '',
      contractId: _appBloc.state.user?.contractId ?? '',
      startDate: state.startDate != null
          ? Jiffy(state.startDate).format('dd/MM/yyyy')
          : null,
      endDate: state.endDate != null
          ? Jiffy(state.endDate).format('dd/MM/yyyy')
          : null,
    );
    return result;
  }

  FutureOr<void> _onContractActionStartDateChanged(
    ContractActionStartDateChanged event,
    Emitter<ContractActionState> emit,
  ) {
    final startDateStr = event.startDate;
    DateTime? startDate;
    if (startDateStr?.isNotEmpty ?? false) {
      try {
        startDate = Jiffy(startDateStr, 'dd/MM/yyyy').dateTime;
      } on Exception {
        startDate = null;
      }
    }
    emit(state.copyWith(
      startDate: startDate,
      endDate: state.endDate,
      error: state.error,
    ));
  }

  FutureOr<void> _onContractActionEndDateChanged(
    ContractActionEndDateChanged event,
    Emitter<ContractActionState> emit,
  ) {
    final endDateStr = event.endDate;
    DateTime? endDate;
    if (endDateStr?.isNotEmpty ?? false) {
      try {
        endDate = Jiffy(endDateStr, 'dd/MM/yyyy').dateTime;
      } on Exception {
        endDate = null;
      }
    }
    emit(state.copyWith(
      startDate: state.startDate,
      endDate: endDate,
      error: state.error,
    ));
  }
}
