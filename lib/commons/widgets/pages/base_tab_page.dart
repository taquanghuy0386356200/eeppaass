import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseTabPage extends StatelessWidget {
  final Color backgroundColor;
  final Widget? gradientHeaderContainer;
  final String? title;
  final Widget? leading;
  final List<Widget>? trailing;
  final PreferredSizeWidget? tabBar;
  final Widget child;
  final Widget? bottomNavigationBar;
  final bool scrollable;
  final ScrollController? scrollController;

  const BaseTabPage({
    Key? key,
    this.gradientHeaderContainer,
    this.title,
    required this.child,
    this.leading,
    this.trailing,
    this.tabBar,
    this.backgroundColor = ColorName.backgroundColor,
    this.bottomNavigationBar,
    this.scrollable = true,
    this.scrollController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: false,
      extendBodyBehindAppBar: true,
      bottomNavigationBar: bottomNavigationBar,
      body: Stack(
        children: [
          gradientHeaderContainer ?? const SizedBox.shrink(),
          NestedScrollView(
            controller: scrollController,
            physics: scrollable
                ? const BouncingScrollPhysics()
                : const NeverScrollableScrollPhysics(),
            floatHeaderSlivers: true,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              if (title != null) {
                return [
                  SliverAppBar(
                    elevation: 0.0,
                    pinned: true,
                    floating: true,
                    iconTheme: const IconThemeData(color: Colors.white),
                    forceElevated: innerBoxIsScrolled,
                    flexibleSpace: GradientHeaderContainer(
                      height: 220.h,
                      radius: 0,
                    ),
                    bottom: tabBar,
                    centerTitle: true,
                    title: FadeAnimation(
                      delay: 0.5,
                      child: Text(
                        title!,
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                              color: Colors.white,
                            ),
                      ),
                    ),
                    leading: leading,
                    actions: trailing,
                    backgroundColor: Colors.transparent,
                  ),
                ];
              }
              return [];
            },
            body: child,
          ),
        ],
      ),
    );
  }
}
