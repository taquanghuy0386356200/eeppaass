part of 'contract_action_bloc.dart';

@immutable
class ContractActionState extends Equatable {
  final List<ContractAction> listData;
  final bool isFull;
  final DateTime? startDate;
  final DateTime? endDate;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const ContractActionState({
    required this.listData,
    this.isFull = false,
    this.startDate,
    this.endDate,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        startDate,
        endDate,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  ContractActionState copyWith({
    List<ContractAction>? listData,
    bool? isFull,
    required DateTime? startDate,
    required DateTime? endDate,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return ContractActionState(
      listData: listData ?? this.listData,
      isFull: isFull ?? this.isFull,
      startDate: startDate,
      endDate: endDate,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
