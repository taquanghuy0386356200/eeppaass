part of 'setting_bloc.dart';

abstract class SettingEvent extends Equatable {
  const SettingEvent();
}

class FirstRun extends SettingEvent {
  @override
  List<Object> get props => [];
}

class BiometricLoginToggled extends SettingEvent {
  final bool value;

  const BiometricLoginToggled(this.value);

  @override
  List<Object> get props => [value];
}

class CashInFirstRun extends SettingEvent {
  @override
  List<Object> get props => [];
}

class CashInBankTransferFirstRun extends SettingEvent {
  @override
  List<Object> get props => [];
}

class BeneficiaryFirstRun extends SettingEvent {
  @override
  List<Object> get props => [];
}