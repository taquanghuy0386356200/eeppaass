part of 'zalopay_gate_bloc.dart';

abstract class ZaloPayGateEvent extends Equatable {
  const ZaloPayGateEvent();
}

class ZaloPayPaymentProceeded extends ZaloPayGateEvent {
  final int amount;
  const ZaloPayPaymentProceeded({
    required this.amount,
  });

  @override
  List<Object> get props => [];
}
