import 'dart:convert';

import 'package:crypto/crypto.dart';

class ReferralClientRequest {
  final String plateNumber;
  final String phoneNumber;
  final String accessKey;

  ReferralClientRequest({
    required this.plateNumber,
    required this.phoneNumber,
    required this.accessKey,
  });

  String get checkSum {
    final result = '$plateNumber$phoneNumber$accessKey';
    final key = utf8.encode(accessKey);
    final data = utf8.encode(result);
    final hmacSha1 = Hmac(sha1, key);
    final digest = hmacSha1.convert(data);
    final resultBase64 = base64.encode(digest.bytes);
    return resultBase64;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['plateNumber'] = plateNumber;
    map['phoneNumber'] = phoneNumber;
    map['checksum'] = checkSum;
    return map;
  }
}
