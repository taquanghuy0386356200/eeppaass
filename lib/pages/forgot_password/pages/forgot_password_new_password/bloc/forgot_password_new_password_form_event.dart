part of 'forgot_password_new_password_form_bloc.dart';

abstract class ForgotPasswordNewPasswordFormEvent extends Equatable {
  const ForgotPasswordNewPasswordFormEvent();
}

class ForgotPasswordNewPasswordFormCompleted
    extends ForgotPasswordNewPasswordFormEvent {
  final String newPassword;

  const ForgotPasswordNewPasswordFormCompleted(this.newPassword);

  @override
  List<Object> get props => [newPassword];
}
