import 'package:epass/commons/models/register/document_file.dart';

/// plateType : ""
/// plateTypeCode : ""
/// plateNumber : ""
/// vehicleTypeId : ""
/// vehicleTypeCode : ""
/// vehicleTypeName : ""
/// owner : ""
/// seatNumber : ""
/// grossWeight : ""
/// netWeight : ""
/// cargoWeight : ""
/// pullingWeight : ""
/// chassicNumber : ""
/// engineNumber : ""
/// vehicleMarkId : ""
/// vehicleBrandId : ""
/// vehicleColourId : ""
/// vehicleProfileDTOs : []

class RegisterVehicleDto {
  RegisterVehicleDto({
    this.plateType,
    this.plateTypeCode,
    this.plateNumber,
    this.vehicleTypeId,
    this.vehicleTypeCode,
    this.vehicleTypeName,
    this.owner,
    this.seatNumber,
    this.grossWeight,
    this.netWeight,
    this.cargoWeight,
    this.pullingWeight,
    this.chassicNumber,
    this.engineNumber,
    this.vehicleMarkId,
    this.vehicleBrandId,
    this.vehicleColourId,
    this.vehicleProfileDTOs,
  });

  final String? plateType;
  final String? plateTypeCode;
  final String? plateNumber;
  final String? vehicleTypeId;
  final String? vehicleTypeCode;
  final String? vehicleTypeName;
  final String? owner;
  final String? seatNumber;
  final String? grossWeight;
  final String? netWeight;
  final String? cargoWeight;
  final String? pullingWeight;
  final String? chassicNumber;
  final String? engineNumber;
  final String? vehicleMarkId;
  final String? vehicleBrandId;
  final String? vehicleColourId;
  final List<DocumentFile>? vehicleProfileDTOs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['plateType'] = plateType;
    map['plateTypeCode'] = plateTypeCode;
    map['plateNumber'] = plateNumber;
    map['vehicleTypeId'] = vehicleTypeId;
    map['vehicleTypeCode'] = vehicleTypeCode;
    map['vehicleTypeName'] = vehicleTypeName;
    map['owner'] = owner;
    map['seatNumber'] = seatNumber;
    map['grossWeight'] = grossWeight;
    map['netWeight'] = netWeight;
    map['cargoWeight'] = cargoWeight;
    map['pullingWeight'] = pullingWeight;
    map['chassicNumber'] = chassicNumber;
    map['engineNumber'] = engineNumber;
    map['vehicleMarkId'] = vehicleMarkId;
    map['vehicleBrandId'] = vehicleBrandId;
    map['vehicleColourId'] = vehicleColourId;
    if (vehicleProfileDTOs != null) {
      map['vehicleProfileDTOs'] = vehicleProfileDTOs?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
