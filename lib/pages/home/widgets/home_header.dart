import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      height: 50.h,
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      color: Colors.transparent,
      child: Stack(
        children: [
          Positioned.fill(
            child: Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.only(left: 8.w),
                child: Assets.icons.logoWhite.svg(),
              ),
            ),
          ),
          // Positioned.fill(
          //   child: Align(
          //     alignment: Alignment.centerRight,
          //     child: SplashIconButton(
          //       icon: Assets.icons.search.svg(),
          //       onTap: () {
          //
          //       },
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
