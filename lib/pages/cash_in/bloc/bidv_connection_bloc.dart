import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/services/bidv/bidv_check_connection.dart';
import 'package:epass/commons/services/bidv/bidv_check_connection_request.dart';
import 'package:epass/commons/services/bidv/bidv_connection.dart';
import 'package:epass/commons/services/bidv/bidv_connection_request.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment_request.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:equatable/equatable.dart';

part 'bidv_connection_event.dart';

part 'bidv_connection_state.dart';

class BidvConnectionBloc
    extends Bloc<BidvConnectionEvent, BidvConnectionState> {
  final BidvConnectionService _bidvConnectionService;

  BidvConnectionBloc({required BidvConnectionService bidvConnectionService})
      : _bidvConnectionService = bidvConnectionService,
        super(BidvConnectionInitial()) {
    on<BidvConnectionProceeded>(_connectionBidv);
  }

  FutureOr<void> _connectionBidv(
    BidvConnectionProceeded event,
    Emitter<BidvConnectionState> emit,
  ) async {
    final request = BidvConnectionRequest(
      payerId: event.payerId,
      payerName: event.payerName,
      payerMobile: event.payerMobile,
      payerIdentity: event.payerIdentity,
      payerAdd: event.payerAdd,
    );

    final result = await _bidvConnectionService.getConnectionBidv(request);

    result.when(
      success: (data) => emit(BidvConnectionSuccess(
        serviceId: data.serviceId,
        merchantId: data.merchantId,
        tranDate: data.tranDate,
        transId: data.transId,
        responseCode: data.responseCode,
        moreInfo: data.moreInfo,
        secureCode: data.secureCode,
        redirectUrl: data.redirectUrl,
      )),
      failure: (failure) => emit(
        BidvConnectionFailure(failure.message ?? ''),
      ),
    );
  }
}
