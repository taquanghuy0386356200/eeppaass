import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/setting/profile/bloc/profile_form_bloc.dart';
import 'package:epass/pages/setting/profile/widgets/avatar_card.dart';
import 'package:epass/pages/setting/profile/widgets/contract_details_card.dart';
import 'package:epass/pages/setting/profile/widgets/profile_details_card.dart';
import 'package:epass/pages/setting/profile/widgets/profile_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: 'Sửa thông tin tài khoản',
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          const GradientHeaderContainer(),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                  // Your widget go below
                  child: Column(
                    children: [
                      ShadowCard(
                        shadowOpacity: 0.2,
                        borderRadius: 16.0,
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(
                            vertical: 24.h,
                            horizontal: 16.w,
                          ),
                          child: Column(
                            children: [
                              const AvatarCard(),
                              SizedBox(height: 44.h),
                              BlocProvider(
                                create: (context) => ProfileFormBloc(
                                  userRepository: getIt<IUserRepository>(),
                                  appBloc: getIt<AppBloc>(),
                                ),
                                child: BlocSelector<AppBloc, AppState, User?>(
                                  selector: (state) => state.user,
                                  builder: (context, user) {
                                    return ProfileForm(phone: user?.phone, email: user?.email);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20.h),
                      const ProfileDetailsCard(),
                      SizedBox(height: 20.h),
                      const ContractDetailsCard(),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
