import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'forgot_password_form_event.dart';

part 'forgot_password_form_state.dart';

class ForgotPasswordFormBloc
    extends Bloc<ForgotPasswordFormEvent, ForgotPasswordFormState> {
  ForgotPasswordFormBloc() : super(const ForgotPasswordFormState()) {
    on<ForgotPasswordFormCompleted>(_onForgotPasswordFormCompleted);
  }

  FutureOr<void> _onForgotPasswordFormCompleted(
    ForgotPasswordFormCompleted event,
    Emitter<ForgotPasswordFormState> emit,
  ) {
    emit(ForgotPasswordFormState(
      plateNumber: event.plateNumber,
      phoneNumber: event.phoneNumber,
      plateTypeCode: event.plateTypeCode,
    ));
  }
}
