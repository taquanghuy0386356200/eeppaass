part of 'bidv_pay_gate_bloc.dart';

abstract class BIDVPayGateEvent extends Equatable {
  const BIDVPayGateEvent();
}

class BIDVPayLinkedCompleted extends BIDVPayGateEvent {
  final String orderId;
  final int paymentStatus;
  final String? errorCode;
  final int transAmount;

  const BIDVPayLinkedCompleted({
    required this.orderId,
    required this.paymentStatus,
    this.errorCode,
    required this.transAmount,
  });

  @override
  List<Object?> get props => [orderId, paymentStatus, errorCode, transAmount];
}

class BIDVPayCheckLinkedCompleted extends BIDVPayGateEvent {
  final String contractId;
  final String contractNo;
  final String customerId;

  const BIDVPayCheckLinkedCompleted({
    required this.contractId,
    required this.contractNo,
    required this.customerId,
  });

  @override
  List<Object?> get props => [contractId, contractNo, customerId];
}

class CashInOrderBIDVConnected extends BIDVPayGateEvent {
  final String contractId;
  final String contractNo;
  final String customerId;
  final int amount;
  final String desContractId;
  final int quantity = 1;

  const CashInOrderBIDVConnected({
    required this.contractId,
    required this.contractNo,
    required this.customerId,
    required this.amount,
    required this.desContractId,
  });

  @override
  List<Object?> get props =>
      [contractId, contractNo, customerId, amount, desContractId];
}
