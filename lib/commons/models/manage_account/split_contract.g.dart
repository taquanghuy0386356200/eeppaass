// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'split_contract.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SplitContractTotalResponse _$SplitContractTotalResponseFromJson(
        Map<String, dynamic> json) =>
    SplitContractTotalResponse(
      data: json['data'] == null
          ? null
          : SplitContractResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SplitContractTotalResponseToJson(
        SplitContractTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

SplitContractResponse _$SplitContractResponseFromJson(
        Map<String, dynamic> json) =>
    SplitContractResponse(
      ticketId: json['ticketId'] as int?,
      phoneContact: json['phoneContact'] as String?,
      contractOrginal: json['contractOrginal'] == null
          ? null
          : ContractOrginal.fromJson(
              json['contractOrginal'] as Map<String, dynamic>),
      contractDetail: json['contractDetail'] == null
          ? null
          : ContractDetail.fromJson(
              json['contractDetail'] as Map<String, dynamic>),
      ticketProfiles: (json['ticketProfiles'] as List<dynamic>?)
          ?.map((e) => TicketProfiles.fromJson(e as Map<String, dynamic>))
          .toList(),
      vehiclesTransfer: (json['vehiclesTransfer'] as List<dynamic>?)
          ?.map((e) => VehiclesTransfer.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SplitContractResponseToJson(
        SplitContractResponse instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'phoneContact': instance.phoneContact,
      'contractOrginal': instance.contractOrginal,
      'contractDetail': instance.contractDetail,
      'ticketProfiles': instance.ticketProfiles,
      'vehiclesTransfer': instance.vehiclesTransfer,
    };

ContractOrginal _$ContractOrginalFromJson(Map<String, dynamic> json) =>
    ContractOrginal(
      dateOfIssue: json['dateOfIssue'] as String?,
      placeOfIssue: json['placeOfIssue'] as String?,
      custName: json['custName'] as String?,
      email: json['email'] as String?,
      contractNo: json['contractNo'] as String?,
      status: json['status'] as String?,
      signDate: json['signDate'] as String?,
      cusPhoneNumber: json['cusPhoneNumber'] as String?,
      signName: json['signName'] as String?,
      cusTypeId: json['cusTypeId'] as int?,
      userName: json['userName'] as String?,
      userId: json['userId'] as String?,
      birth: json['birth'] as String?,
      address: json['address'] as String?,
      identifier: json['identifier'] as String?,
      effDate: json['effDate'] as String?,
      phone: json['phone'] as String?,
      customerId: json['customerId'] as String?,
      contractId: json['contractId'] as String?,
      documentType: json['documentType'] as int?,
      isAdditional: json['isAdditional'] as int?,
      smsNotification: json['smsNotification'] as String?,
      accountAlias: json['accountAlias'] as String?,
      billCycle: json['billCycle'] as String?,
      billCycleMergeType: json['billCycleMergeType'] as String?,
      noticeAreaName: json['noticeAreaName'] as String?,
      noticeStreet: json['noticeStreet'] as String?,
      noticeAreaCode: json['noticeAreaCode'] as String?,
      noticeEmail: json['noticeEmail'] as String?,
      noticePhoneNumber: json['noticePhoneNumber'] as String?,
      noticeName: json['noticeName'] as String?,
    );

Map<String, dynamic> _$ContractOrginalToJson(ContractOrginal instance) =>
    <String, dynamic>{
      'dateOfIssue': instance.dateOfIssue,
      'placeOfIssue': instance.placeOfIssue,
      'custName': instance.custName,
      'email': instance.email,
      'contractNo': instance.contractNo,
      'status': instance.status,
      'signDate': instance.signDate,
      'signName': instance.signName,
      'cusTypeId': instance.cusTypeId,
      'userName': instance.userName,
      'userId': instance.userId,
      'birth': instance.birth,
      'address': instance.address,
      'identifier': instance.identifier,
      'effDate': instance.effDate,
      'phone': instance.phone,
      'cusPhoneNumber': instance.cusPhoneNumber,
      'customerId': instance.customerId,
      'contractId': instance.contractId,
      'documentType': instance.documentType,
      'isAdditional': instance.isAdditional,
      'smsNotification': instance.smsNotification,
      'accountAlias': instance.accountAlias,
      'billCycle': instance.billCycle,
      'billCycleMergeType': instance.billCycleMergeType,
      'noticeAreaName': instance.noticeAreaName,
      'noticeStreet': instance.noticeStreet,
      'noticeAreaCode': instance.noticeAreaCode,
      'noticeEmail': instance.noticeEmail,
      'noticePhoneNumber': instance.noticePhoneNumber,
      'noticeName': instance.noticeName,
    };

ContractDetail _$ContractDetailFromJson(Map<String, dynamic> json) =>
    ContractDetail(
      signDate: json['signDate'] as String?,
      signName: json['signName'] as String?,
      effDate: json['effDate'] as String?,
      billCycle: json['billCycle'] as String?,
      noticeStreet: json['noticeStreet'] as String?,
      billCycleMergeType: json['billCycleMergeType'] as String?,
      noticeAreaName: json['noticeAreaName'] as String?,
      noticeAreaCode: json['noticeAreaCode'] as String?,
      noticeEmail: json['noticeEmail'] as String?,
      noticePhoneNumber: json['noticePhoneNumber'] as String?,
      noticeName: json['noticeName'] as String?,
      payCharge: json['payCharge'] as String?,
      transferVehicleIds: (json['transferVehicleIds'] as List<dynamic>?)
          ?.map((e) => e as int)
          .toList(),
    )..smsNotification = json['smsNotification'] as String?;

Map<String, dynamic> _$ContractDetailToJson(ContractDetail instance) =>
    <String, dynamic>{
      'signDate': instance.signDate,
      'signName': instance.signName,
      'effDate': instance.effDate,
      'billCycle': instance.billCycle,
      'billCycleMergeType': instance.billCycleMergeType,
      'noticeAreaName': instance.noticeAreaName,
      'noticeAreaCode': instance.noticeAreaCode,
      'noticeEmail': instance.noticeEmail,
      'noticePhoneNumber': instance.noticePhoneNumber,
      'noticeName': instance.noticeName,
      'noticeStreet': instance.noticeStreet,
      'payCharge': instance.payCharge,
      'smsNotification': instance.smsNotification,
      'transferVehicleIds': instance.transferVehicleIds,
    };
