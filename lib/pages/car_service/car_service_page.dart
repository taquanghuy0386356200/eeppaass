import 'package:auto_route/auto_route.dart';
import 'package:car_doctor_epass_sos/car_doctor_epass_sos.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/modal/alert_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/flavors.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/utility/bloc/utility_bloc.dart';
import 'package:epass/pages/utility/car_doctor/car_doctor_bloc.dart';
import 'package:epass/pages/utility/car_doctor/car_doctor_fnc.dart';
import 'package:epass/pages/utility/widgets/utility_button.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:url_launcher/url_launcher.dart';

class CarServicePage extends StatefulWidget {
  const CarServicePage({Key? key}) : super(key: key);

  @override
  State<CarServicePage> createState() => _CarServicePageState();
}

class _CarServicePageState extends State<CarServicePage>
    with AutomaticKeepAliveClientMixin {
  bool _firstRun = true;
  late BuildContext providerContext;
  late String action = 'SOS';

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final carServiceButtons = [
      UtilityButton(
        icon: Assets.icons.carServiceSos.svg(width: 55.0, height: 55.0),
        title: 'Cứu hộ 24/7',
        onTap: () async {
          providerContext.loaderOverlay.show();
          action = 'SOS';
          providerContext.read<CarDoctorBloc>().add(GetCarDoctorConfig());
        },
      ),
      // UtilityButton(
      //   icon: Assets.icons.carServiceAutopart.svg(width: 55.0, height: 55.0),
      //   title: 'Chợ phụ kiện',
      //   onTap: () async {
      //     providerContext.loaderOverlay.show();
      //     action = 'Accessories';
      //     providerContext.read<CarDoctorBloc>().add(GetCarDoctorConfig());
      //   },
      // ),
      UtilityButton(
        icon: Assets.icons.carServiceCarfinance.svg(width: 55.0, height: 55.0),
        title: 'Tài chính Ô tô',
        onTap: () async {
          showAlertDialog(
            context,
            title: 'Tài chính ô tô',
            content: const Text(
              "Tính năng đang trong quá trình phát triển, sẽ được cung cấp trong thời gian sắp tới!",
              textAlign: TextAlign.center,
            ),
            primaryButtonTitle: 'Đóng',
            onPrimaryButtonTap: (context) async {
              Navigator.of(context).pop();
            },
          );
        },
      ),
      // UtilityButton(
      //   icon: Assets.icons.carDoctor.svg(width: 55.0, height: 55.0),
      //   title: 'Khám chữa xe',
      //   onTap: _createLink,
      // ),
    ];
    return WillPopScope(
      onWillPop: () async => true,
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) {
            providerContext = context;
            return CarDoctorBloc(
                userRepository: getIt<IUserRepository>(),
                appBloc: context.read<AppBloc>());
          }),
          BlocProvider(
              create: (context) =>
                  UtilityBloc(userRepository: getIt<IUserRepository>())
                    ..add(const UnavailableServiceFetched())),
        ],
        child: MultiBlocListener(
          listeners: [
            BlocListener<CarDoctorBloc, CarDoctorState>(
              listener: (context, state) async {
                providerContext = context;
                if (state is GetAccessTokenSuccess) {
                  providerContext.loaderOverlay.hide();
                  switch (action) {
                    case 'Accessories':
                      final url =
                          '${dotenv.env['ACCESSORY_URL'] ?? ""}?k=${dotenv.env['ACCESSORY_KEY'] ?? ""}&a=${state.accessToken}';
                      context.pushRoute(
                          WebViewRoute(title: 'Chợ phụ kiện', initUrl: url));
                      break;
                    case 'SOS':
                      final option = CarDoctorOption(
                        sosUrl: '',
                        bearerToken: state.accessToken,
                        apiUrl: dotenv.env['APIURL']!,
                        imageUrl: dotenv.env['IMAGEURL']!,
                        mapKey: dotenv.env['MAPKEY']!,
                      );
                      final sdkSos = CarDoctorSdkSos.instance;
                      sdkSos.init(option);
                      sdkSos.open(context);
                      break;
                    default:
                      break;
                  }
                } else {
                  providerContext.loaderOverlay.hide();
                }
                if (state is GetConfigSuccess) {
                  CarDoctorFnc.requestLocationPermission(
                      providerContext, state.config.name ?? '', action);
                }
                if (state is GetConfigFail) {
                  providerContext.loaderOverlay.hide();
                  showErrorSnackBBar(context: context, message: state.message);
                }
                if (state is GetAccessTokenFail) {
                  providerContext.loaderOverlay.hide();
                  showErrorSnackBBar(context: context, message: state.message);
                }
              },
            ),
          ],
          child: BlocBuilder<CarDoctorBloc, CarDoctorState>(
            builder: (context, state) {
              providerContext = context;
              return BasePage(
                title: 'Dịch vụ ô tô',
                backgroundColor: ColorName.backgroundAvatar,
                child: Stack(
                  children: [
                    FadeAnimation(
                      delay: 0.5,
                      playAnimation: _firstRun,
                      child: const GradientHeaderContainer(),
                    ),
                    SafeArea(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 10.w,
                          vertical: 16.h,
                        ),
                        child: BlocBuilder<UtilityBloc, UtilityState>(
                          builder: (context, state) {
                            return GridView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: carServiceButtons.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 0.w,
                                mainAxisSpacing: 0.h,
                                childAspectRatio: 0.85,
                              ),
                              itemBuilder: (context, index) {
                                return FadeAnimation(
                                  delay: 1 +
                                      (index / (carServiceButtons.length * 2)),
                                  playAnimation: _firstRun,
                                  child: carServiceButtons[index],
                                );
                              },
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Future<void> _createLink() async {
    String flavor;
    switch (F.appFlavor) {
      case Flavor.prod:
        {
          flavor = "com.mfunctions.driver.prod";
          break;
        }
      default:
        {
          flavor = "com.mfunctions.driver.dev";
        }
    }
    String? token =
        providerContext.read<AppBloc>().state.authInfo?.accessToken;
    final dynamicLinkParams = DynamicLinkParameters(
      link: Uri.parse("https://epass.cardoctor.com.vn/user?token=$token"),
      uriPrefix: "https://epassxcardoctor.page.link",
      androidParameters: AndroidParameters(
        packageName: flavor,
        minimumVersion: 0,
      ),
      iosParameters: IOSParameters(
        bundleId: flavor,
        appStoreId: '6444058831',
        minimumVersion: '0',
      ),
    );
    final dynamicLink =
        await FirebaseDynamicLinks.instance.buildLink(dynamicLinkParams);
    launchUrl(
      dynamicLink,
      mode: LaunchMode.externalApplication,
    );
  }
}
