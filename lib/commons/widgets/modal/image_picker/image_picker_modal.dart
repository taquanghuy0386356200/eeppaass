import 'package:epass/commons/widgets/modal/image_picker/image_picker_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

class ImageResourceModal extends StatelessWidget {
  const ImageResourceModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoActionSheet(
      // title: const Text('Thao tác'),
      // message: const Text('Message'),
      actions: <CupertinoActionSheetAction>[
        CupertinoActionSheetAction(
          child: Text(
            'Chụp ảnh',
            style: Theme.of(context).textTheme.button!.copyWith(
                  color: Colors.orange,
                  fontSize: 18.sp,
                ),
          ),
          onPressed: () {
            context
                .read<ImagePickerBloc>()
                .add(const ImagePicked(source: ImageSource.camera));
            Navigator.of(context).pop();
          },
        ),
        CupertinoActionSheetAction(
          child: Text(
            'Thư viện hình ảnh',
            style: Theme.of(context).textTheme.button!.copyWith(
                  color: Colors.orange,
                  fontSize: 18.sp,
                ),
          ),
          onPressed: () {
            context
                .read<ImagePickerBloc>()
                .add(const ImagePicked(source: ImageSource.gallery));
            Navigator.of(context).pop();
          },
        ),
      ],
      cancelButton: CupertinoActionSheetAction(
        onPressed: Navigator.of(context).pop,
        child: Text(
          'Huỷ',
          style: Theme.of(context).textTheme.button!.copyWith(
                fontSize: 18.sp,
              ),
        ),
      ),
    );
  }
}

Future<void> showImagePickerModal(BuildContext context) async {
  await showCupertinoModalPopup(
    context: context,
    builder: (_) => BlocProvider.value(
      value: context.read<ImagePickerBloc>(),
      child: const ImageResourceModal(),
    ),
  );
}
