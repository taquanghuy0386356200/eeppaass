part of 'setting_page_bloc.dart';

abstract class SettingPageEvent extends Equatable {
  const SettingPageEvent();
}

class SettingPageLogout extends SettingPageEvent {
  @override
  List<Object> get props => [];
}
