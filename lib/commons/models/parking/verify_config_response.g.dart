// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verify_config_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerifyConfigModel _$VerifyConfigModelFromJson(Map<String, dynamic> json) =>
    VerifyConfigModel(
      configId: json['configId'] as int?,
      contractId: json['contractId'] as int?,
      serviceId: json['serviceId'] as int?,
      serviceName: json['serviceName'] as String?,
      verifyType: json['verifyType'] as int?,
      status: json['status'] as int?,
      isCreatedPassCode: json['isCreatedPassCode'] as bool?,
    );

Map<String, dynamic> _$VerifyConfigModelToJson(VerifyConfigModel instance) =>
    <String, dynamic>{
      'configId': instance.configId,
      'contractId': instance.contractId,
      'serviceId': instance.serviceId,
      'serviceName': instance.serviceName,
      'verifyType': instance.verifyType,
      'status': instance.status,
      'isCreatedPassCode': instance.isCreatedPassCode,
    };

VerifyConfigResponse _$VerifyConfigResponseFromJson(
        Map<String, dynamic> json) =>
    VerifyConfigResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => VerifyConfigModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VerifyConfigResponseToJson(
        VerifyConfigResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
