import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class ItemWidget extends StatelessWidget {
  final TopupChannelCode? topupChannelCode;
  final String? content;
  final String? icon;
  final int? serviceFeeValAdd;
  final double? serviceFeeVal;
  final bool? containButton;
  final String? url;
  final String? topupChannelName;

  const ItemWidget(
      {Key? key,
      this.topupChannelCode,
      this.content,
      this.icon,
      this.serviceFeeValAdd,
      this.serviceFeeVal,
      this.containButton = false,
      this.url,
      this.topupChannelName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final content =
        "${containButton == true ? '' : 'Phí giao dịch: '}${serviceFeeValAdd != null ? "${serviceFeeValAdd!.formatCurrency().toString()}đ " : ''}${serviceFeeValAdd != null && serviceFeeVal != null ? '+ ' : ''}${serviceFeeVal != null ? "${(serviceFeeVal! * 100).toString()}% " : ''}giao dịch";
    return GestureDetector(
      onTap: () {
        url != null
            ? context.pushRoute(
                WebViewRoute(title: topupChannelName ?? '', initUrl: url ?? ''))
            : showErrorSnackBBar(
                context: context,
                message: 'Đường dẫn không hợp lệ',
                duration: const Duration(seconds: 3));
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          containButton == false
              ? icon?.split('.').last == 'svg'
                  ? SvgPicture.network(icon ?? '', width: 20.w, height: 20.h)
                  : Image.network(icon ?? '', width: 20.w, height: 20.h,
                      errorBuilder: (BuildContext context, Object exception,
                          StackTrace? stackTrace) {
                      return Assets.icons.feeArrow.svg(
                        height: 20.h,
                        width: 20.w,
                      );
                    })
              : Container(
                  width: 72.w,
                  height: 32.h,
                  padding: EdgeInsets.only(
                      left: 6.w, right: 10.w, top: 4.h, bottom: 4.h),
                  decoration: BoxDecoration(
                      color: const Color(0xFFAA9F9E).withOpacity(0.15),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(20.0))),
                  child: icon?.split('.').last == 'svg'
                      ? SvgPicture.network(
                          icon ?? '',
                          fit: BoxFit.cover,
                        )
                      : Image.network(icon ?? '', fit: BoxFit.cover,
                          errorBuilder: (BuildContext context, Object exception,
                              StackTrace? stackTrace) {
                          return Assets.icons.feeArrow.svg(
                            height: 20.h,
                            width: 72.w,
                          );
                        }),
                ),
          SizedBox(width: 6.w),
          serviceFeeValAdd != null || serviceFeeVal != null
              ? (Flexible(
                  child: Text(
                    content,
                    style:Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.w400, fontSize: 12.sp
                    ),
                  ),
                ))
              : Flexible(
                  child: Text(
                    'Miễn phí',
                    style:Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.w400, fontSize: 13.sp
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
