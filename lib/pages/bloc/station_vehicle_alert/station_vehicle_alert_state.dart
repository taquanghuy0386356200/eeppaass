part of 'station_vehicle_alert_bloc.dart';

class StationVehicleAlertState extends Equatable {
  final List<StationDetail>? stationDetails;
  final bool? isOn;
  final bool isLoading;
  final String? error;
  final bool isOpenLocationPermission;

  const StationVehicleAlertState({
    this.stationDetails,
    this.isOn,
    this.isLoading = false,
    this.isOpenLocationPermission = false,
    this.error,
  });

  @override
  List<Object?> get props => [stationDetails, isOn, error,isOpenLocationPermission];

  StationVehicleAlertState copyWith({
    List<StationDetail>? stationDetails,
    bool? isOn,
    bool isOpenLocationPermission = false,
    required bool isLoading,
    required String? error,
  }) {
    return StationVehicleAlertState(
      stationDetails: stationDetails ?? this.stationDetails,
      isOn: isOn ?? this.isOn,
      isLoading: isLoading,
      error: error,
      isOpenLocationPermission: isOpenLocationPermission,
    );
  }

  StationVehicleAlertState fromJson(
    Map<String, dynamic> json,
  ) =>
      StationVehicleAlertState(
        stationDetails: (json['stationDetails'] as List<dynamic>?)
                ?.map((e) => StationDetail.fromJson(e as Map<String, dynamic>))
                .toList() ??
            const [],
        isOn: json['isOn'] == null ? false : json['isOn'] as bool,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'stationDetails': stationDetails?.map((e) => e.toJson()).toList(),
        'isOn': isOn,
      };
}
