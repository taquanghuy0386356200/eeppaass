import 'package:epass/commons/validators/phone_number_validator.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:flutter/material.dart';

class PhoneNumberTextField extends StatefulWidget {
  final TextEditingController controller;
  final String? labelText;
  final ValueChanged<String?>? onChanged;
  final bool isMandatory;

  const PhoneNumberTextField({
    Key? key,
    required this.controller,
    this.onChanged,
    this.labelText,
    this.isMandatory = false,
  }) : super(key: key);

  @override
  State<PhoneNumberTextField> createState() => _PhoneNumberTextFieldState();
}

class _PhoneNumberTextFieldState extends State<PhoneNumberTextField>
    with PhoneNumberValidator {
  @override
  Widget build(BuildContext context) {
    return PrimaryTextField(
      inputAction: TextInputAction.next,
      controller: widget.controller,
      onChanged: widget.onChanged,
      labelText: widget.labelText ?? 'Số điện thoại',
      hintText: 'Nhập số điện thoại',
      inputType: TextInputType.number,
      isMandatory: widget.isMandatory,
      validator: phoneNumberValidator,
      maxLength: 10,
    );
  }
}
