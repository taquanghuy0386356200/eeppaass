part of 'register_paper_scan_bloc.dart';

abstract class RegisterPaperScanEvent extends Equatable {
  const RegisterPaperScanEvent();
}

class RegisterPaperScanned extends RegisterPaperScanEvent {
  final String filePath;
  final IdType side;

  const RegisterPaperScanned({
    required this.filePath,
    required this.side,
  });

  @override
  List<Object> get props => [filePath, side];
}

class RegisterPaperScannedReset extends RegisterPaperScanEvent {
  @override
  List<Object> get props => [];
}
