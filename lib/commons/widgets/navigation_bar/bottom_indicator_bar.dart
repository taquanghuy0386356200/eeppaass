import 'dart:ui' show lerpDouble;

import 'package:epass/commons/widgets/navigation_bar/bottom_indicator_navigation_bar_item.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ignore: must_be_immutable
class BottomIndicatorBar extends StatefulWidget {
  final Color indicatorColor;
  final Color? activeColor;
  final Color inactiveColor;
  final bool shadow;
  final double sidePadding;
  int currentIndex;
  Widget? iconData;
  final ValueChanged<int> onTap;
  final List<BottomIndicatorNavigationBarItem> items;

  BottomIndicatorBar({
    Key? key,
    required this.onTap,
    required this.items,
    this.activeColor,
    this.inactiveColor = Colors.grey,
    required this.indicatorColor,
    this.shadow = true,
    this.currentIndex = 0,
    this.sidePadding = 20.0,
  }) : super(key: key);

  @override
  State createState() => _BottomIndicatorBarState();
}

class _BottomIndicatorBarState extends State<BottomIndicatorBar> {
  static const double BAR_HEIGHT = 60;
  static const double INDICATOR_HEIGHT = 2;

  List<BottomIndicatorNavigationBarItem> get items => widget.items;

  double width = 0;
  Color? activeColor;
  Duration duration = const Duration(milliseconds: 170);

  double _getIndicatorPosition(int index) {
    final isLtr = Directionality.of(context) == TextDirection.ltr;
    if (isLtr) {
      return lerpDouble(-1.0, 1.0, index / (items.length - 1))!;
    } else {
      return lerpDouble(1.0, -1.0, index / (items.length - 1))!;
    }
  }

  @override
  void initState() {
    super.initState();
    widget.iconData = widget.items[0].activeIcon;
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    activeColor = widget.activeColor ?? Theme.of(context).indicatorColor;
    final height = BAR_HEIGHT + MediaQuery.of(context).viewPadding.bottom;

    return BlocBuilder<HomeTabsBloc, HomeTabsState>(
      builder: (context, state) {
        return AnimatedContainer(
          duration: const Duration(milliseconds: 500),
          height: state is HomeTabBarShowed ? height : 0,
          width: width,
          decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
            boxShadow: widget.shadow
                ? [
                    const BoxShadow(color: Colors.black12, blurRadius: 10),
                  ]
                : null,
          ),
          child: Stack(
            clipBehavior: Clip.none,
            children: <Widget>[
              Positioned(
                top: INDICATOR_HEIGHT,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: items.map((item) {
                    final index = items.indexOf(item);
                    return GestureDetector(
                      onTap: () => _select(index, item),
                      child: _buildItemWidget(
                        item,
                        index == widget.currentIndex,
                        index == 0,
                        index == items.length - 1,
                      ),
                    );
                  }).toList(),
                ),
              ),
              Positioned(
                top: 0,
                width: width,
                child: AnimatedAlign(
                  alignment:
                      Alignment(_getIndicatorPosition(widget.currentIndex), 0),
                  duration: duration,
                  child: Container(
                    margin: EdgeInsets.only(
                      left: widget.currentIndex == 0
                          ? widget.sidePadding * 1.4
                          : widget.sidePadding,
                      right: widget.currentIndex == widget.items.length - 1
                          ? widget.sidePadding * 1.4
                          : widget.sidePadding,
                    ),
                    color: widget.indicatorColor,
                    width: (width - 8 * widget.sidePadding) / items.length,
                    height: INDICATOR_HEIGHT,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _select(int index, BottomIndicatorNavigationBarItem item) {
    widget.currentIndex = index;
    widget.iconData = item.icon;
    widget.onTap(widget.currentIndex);

    setState(() {});
  }

  Widget _buildItemWidget(BottomIndicatorNavigationBarItem item,
      bool isSelected, bool isFirst, bool isLast) {
    return Container(
      padding: EdgeInsets.only(
        left: isFirst ? widget.sidePadding : 0,
        right: isLast ? widget.sidePadding : 0,
      ),
      color: item.backgroundColor,
      height: BAR_HEIGHT,
      width: width / items.length,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          if (isSelected) item.activeIcon else item.icon,
          const SizedBox(height: 4.0),
          Text(
            item.label ?? '',
            style: Theme.of(context).textTheme.caption!.copyWith(
                  color: isSelected ? ColorName.textMainColor : Colors.grey,
                ),
          ),
        ],
      ),
    );
  }
}
