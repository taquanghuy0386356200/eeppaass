part of 'create_report_otp_bloc.dart';

abstract class CreateReportOtpState extends Equatable {
  const CreateReportOtpState();
}

class CreateReportOtpInitial extends CreateReportOtpState {
  @override
  List<Object> get props => [];
}

class CreateReportOtpRequestedInProgress extends CreateReportOtpState {
  const CreateReportOtpRequestedInProgress();

  @override
  List<Object> get props => [];
}

class CreateReportOtpPaused extends CreateReportOtpState {
  final DateTime submittedTime;

  const CreateReportOtpPaused({required this.submittedTime});

  @override
  List<Object> get props => [submittedTime];
}

class CreateReportOtpRequestedSuccess extends CreateReportOtpState {
  final DateTime submittedTime;

  const CreateReportOtpRequestedSuccess(this.submittedTime);

  @override
  List<Object> get props => [submittedTime];
}

class CreateReportOtpRequestedFailure extends CreateReportOtpState {
  final String message;

  const CreateReportOtpRequestedFailure({
    required this.message,
  });

  @override
  List<Object> get props => [message];
}
