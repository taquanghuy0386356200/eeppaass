part of 'register_service_bloc.dart';

@immutable
class RegisterServiceState extends Equatable {
  final InvoiceCycle invoiceCycle;
  final InvoiceCycleGroupType? invoiceCycleGroupType;

  const RegisterServiceState({
    this.invoiceCycle = InvoiceCycle.timely,
    this.invoiceCycleGroupType,
  });

  @override
  List<Object?> get props => [invoiceCycle, invoiceCycleGroupType];

  RegisterServiceState copyWith({
    InvoiceCycle? invoiceCycle,
    InvoiceCycleGroupType? invoiceCycleGroupType,
  }) {
    return RegisterServiceState(
      invoiceCycle: invoiceCycle ?? this.invoiceCycle,
      invoiceCycleGroupType: invoiceCycleGroupType ?? this.invoiceCycleGroupType,
    );
  }
}
