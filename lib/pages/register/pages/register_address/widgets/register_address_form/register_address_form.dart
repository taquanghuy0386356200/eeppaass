import 'package:epass/commons/enum/register_level.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/modal/image_picker/image_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/image_picker/image_picker_modal.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/pages/register_address/widgets/address_dropdown/address_dropdown.dart';
import 'package:epass/pages/register/pages/register_address/widgets/camera_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rxdart/rxdart.dart';

class RegisterAddressForm extends StatefulWidget {
  final int index;
  final RegisterVehiclesBloc bloc;

  const RegisterAddressForm({
    Key? key,
    required this.index,
    required this.bloc,
  }) : super(key: key);

  @override
  State<RegisterAddressForm> createState() => _RegisterAddressFormState();
}

class _RegisterAddressFormState extends State<RegisterAddressForm>
    with SingleTickerProviderStateMixin {
  // final _phoneNumberController = TextEditingController();
  // final _emailController = TextEditingController();
  final _addressDetailsController = TextEditingController();

  @override
  void initState() {
    _addressDetailsController.text = context
            .read<RegisterVehiclesBloc>()
            .state
            .listRegisterVehicle[widget.index]
            .addressInfo
            .addressDetails ??
        '';
    widget.bloc.init();
    super.initState();
  }

  @override
  void dispose() {
    // _phoneNumberController.dispose();
    // _emailController.dispose();
    _addressDetailsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc =  context.read<RegisterVehiclesBloc>();
    return ShadowCard(
      shadowOpacity: 0.2,
      padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 24.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, String?>(
            selector: (state) =>
                state.listRegisterVehicle[widget.index].vehicleInfo.plateNumber,
            builder: (context, plateNumber) {
              return Text(
                'Hồ sơ xe: ${plateNumber ?? ''}',
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              );
            },
          ),
          SizedBox(height: 24.h),

          // CAR PAPER
          Text(
            'Giấy tờ xe (không bắt buộc)',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: ColorName.primaryColor,
                  fontWeight: FontWeight.bold,
                ),
          ),
          SizedBox(height: 8.h),
          Text(
            'Bổ sung ảnh chụp đăng ký, đăng kiểm xe giúp bạn tiết kiệm 5 phút mỗi xe khi'
            ' nhân viên tới dán thẻ ePass',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          SizedBox(height: 12.h),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Đăng ký',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    SizedBox(height: 8.h),
                    BlocProvider(
                      create: (context) =>
                          ImagePickerBloc(imagePicker: getIt<ImagePicker>()),
                      child: Builder(builder: (context) {
                        return BlocListener<ImagePickerBloc, ImagePickerState>(
                          listener: (context, state) async {
                            if (state is ImagePickedFailure) {
                              await showErrorSnackBBar(
                                  context: context, message: state.message);
                            } else if (state is ImagePickedSuccess) {
                              context
                                  .read<RegisterVehiclesBloc>()
                                  .add(AddressInfoRegCertImageUpdated(
                                    index: widget.index,
                                    regCertImage: state.filePath,
                                  ));
                            }
                          },
                          child: BlocSelector<RegisterVehiclesBloc,
                              RegisterVehiclesState, String?>(
                            selector: (state) => state
                                .listRegisterVehicle[widget.index]
                                .addressInfo
                                .regCertImage,
                            builder: (context, regCertImagePath) {
                              return CameraCard(
                                height: 100.h,
                                imagePath: regCertImagePath,
                                onTap: () async =>
                                    await showImagePickerModal(context),
                              );
                            },
                          ),
                        );
                      }),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 16.w),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Đăng kiểm',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    SizedBox(height: 8.h),
                    BlocProvider(
                      create: (context) =>
                          ImagePickerBloc(imagePicker: getIt<ImagePicker>()),
                      child: Builder(builder: (context) {
                        return BlocListener<ImagePickerBloc, ImagePickerState>(
                          listener: (context, state) async {
                            if (state is ImagePickedFailure) {
                              await showErrorSnackBBar(
                                  context: context, message: state.message);
                            } else if (state is ImagePickedSuccess) {
                              context
                                  .read<RegisterVehiclesBloc>()
                                  .add(AddressInfoVehicleRegCertImageUpdated(
                                    index: widget.index,
                                    vehicleRegCertImage: state.filePath,
                                  ));
                            }
                          },
                          child: BlocSelector<RegisterVehiclesBloc,
                              RegisterVehiclesState, String?>(
                            selector: (state) => state
                                .listRegisterVehicle[widget.index]
                                .addressInfo
                                .vehicleRegCertImage,
                            builder: (context, vehicleRegCertImage) {
                              return CameraCard(
                                height: 100.h,
                                imagePath: vehicleRegCertImage,
                                onTap: () async =>
                                    await showImagePickerModal(context),
                              );
                            },
                          ),
                        );
                      }),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 24.h),

          // ADDRESS
          BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, String?>(
            selector: (state) =>
                state.listRegisterVehicle[widget.index].vehicleInfo.plateNumber,
            builder: (context, plateNumber) {
              return Text(
                'Địa chỉ nhận dán thẻ xe ${plateNumber ?? ''}',
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: ColorName.primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
              );
            },
          ),
          SizedBox(height: 8.h),
          Text(
            'Bạn vui lòng điền chính xác thông tin,'
            ' nhân viên ePass liên hệ và tới dán thẻ tận nơi',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          // SizedBox(height: 16.h),
          // PhoneNumberTextField(
          //   controller: _phoneNumberController,
          //   onChanged: (phoneNumber) => context.read<RegisterVehiclesBloc>().add(AddressInfoPhoneNumberUpdated(
          //         index: widget.index,
          //         phoneNumber: phoneNumber,
          //       )),
          // ),
          // SizedBox(height: 20.h),
          // EmailTextField(
          //   controller: _emailController,
          //   onChanged: (email) => context.read<RegisterVehiclesBloc>().add(AddressInfoEmailUpdated(
          //         index: widget.index,
          //         email: email,
          //       )),
          // ),
          SizedBox(height: 24.h),
          Text(
            'Địa chỉ dán thẻ mong muốn *',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          SizedBox(height: 20.h),
          PrimaryTextField(
            controller: _addressDetailsController,
            maxLines: 2,
            hintText: 'Địa chỉ chi tiết',
            labelText: 'Địa chỉ chi tiết',
            validator: (value) {
              if (value?.isEmpty ?? true) {
                return 'Vui lòng nhập địa chỉ chi tiết';
              }
              return null;
            },
            onChanged: (address) {
              context
                  .read<RegisterVehiclesBloc>()
                  .add(AddressInfoAddressDetailsUpdated(
                    index: widget.index,
                    addressDetails: address,
                  ));
            },
          ),
          SizedBox(height: 20.h),
          BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState,
              AddressInfo>(
            selector: (state) =>
                state.listRegisterVehicle[widget.index].addressInfo,
            builder: (context, addressInfo) {
              return AddressDropdown(
                  dropdownType: AddressDropdownType.province,
                  selectedItem: addressInfo.province,
                  onChanged: (province) => context
                      .read<RegisterVehiclesBloc>()
                      .add(AddressInfoProvinceUpdated(
                        index: widget.index,
                        province: province,
                      )));
            },
          ),
          textErrorWidget(context, bloc.provinceAddressError),
          SizedBox(height: 20.h),
          BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState,
              AddressInfo>(
            selector: (state) =>
                state.listRegisterVehicle[widget.index].addressInfo,
            builder: (context, addressInfo) {
              return AddressDropdown(
                  key: UniqueKey(),
                  dropdownType: AddressDropdownType.district,
                  provinceCode: addressInfo.province?.province,
                  selectedItem: addressInfo.district,
                  onChanged: (district) => context
                      .read<RegisterVehiclesBloc>()
                      .add(AddressInfoDistrictUpdated(
                        index: widget.index,
                        district: district,
                      )));
            },
          ),
          textErrorWidget(context, bloc.districtAddressError),
          SizedBox(height: 20.h),
          BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState,
              AddressInfo>(
            selector: (state) =>
                state.listRegisterVehicle[widget.index].addressInfo,
            builder: (context, addressInfo) {
              return AddressDropdown(
                key: UniqueKey(),
                dropdownType: AddressDropdownType.ward,
                provinceCode: addressInfo.province?.province,
                districtCode: addressInfo.district?.district,
                selectedItem: addressInfo.ward,
                onChanged: (ward) => context
                    .read<RegisterVehiclesBloc>()
                    .add(AddressInfoWardUpdated(
                      index: widget.index,
                      ward: ward,
                    )),
              );
            },
          ),
          textErrorWidget(context, bloc.wardAddressError),
          // TODO:
          // if (widget.index == 0)
          //   Row(
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, bool>(
          //         selector: (state) => state.listRegisterVehicle[widget.index].addressInfo.reuseAddress ?? false,
          //         builder: (context, reuseAddress) {
          //           return PrimaryCheckbox(
          //             isCheck: reuseAddress,
          //             onChanged: (value) => context.read<RegisterVehiclesBloc>().add(AddressInfoReuseAddressUpdated(
          //                   index: widget.index,
          //                   reuseAddress: value,
          //                 )),
          //           );
          //         },
          //       ),
          //       Expanded(
          //         child: Padding(
          //           padding: EdgeInsets.only(top: 16.h),
          //           child: RichText(
          //             text: TextSpan(
          //               children: [
          //                 const TextSpan(text: 'Áp dụng địa chỉ này cho '),
          //                 TextSpan(
          //                     text: 'tất cả',
          //                     style: Theme.of(context).textTheme.bodyText1!.copyWith(
          //                           fontWeight: FontWeight.bold,
          //                         )),
          //                 const TextSpan(text: ' các xe phía sau'),
          //               ],
          //               style: Theme.of(context).textTheme.bodyText1,
          //             ),
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),

          SizedBox(height: 24.h),
          Text(
            'Thời gian dán thẻ mong muốn *',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          SizedBox(height: 16.h),
          BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState,
              RegisterLevel?>(
            selector: (state) => state
                .listRegisterVehicle[widget.index].addressInfo.registerLevel,
            builder: (context, registerLevel) {
              return RadioGroupButtons<RegisterLevel>(
                labels: RegisterLevel.values.map((e) => e.title).toList(),
                values: RegisterLevel.values,
                defaultValue: registerLevel,
                onChanged: (value) {
                  FocusScope.of(context).unfocus();
                  context
                      .read<RegisterVehiclesBloc>()
                      .add(AddressInfoRegisterLevelUpdated(
                        index: widget.index,
                        timeFrame: value,
                      ));
                },
              );
            },
          ),

          // TODO:
          // if (widget.index == 0)
          //   Row(
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, bool>(
          //         selector: (state) => state.listRegisterVehicle[widget.index].addressInfo.reuseTimeFrame ?? false,
          //         builder: (context, reuseAddress) {
          //           return PrimaryCheckbox(
          //             isCheck: reuseAddress,
          //             onChanged: (value) => context.read<RegisterVehiclesBloc>().add(AddressInfoReuseTimeFrameUpdated(
          //                   index: widget.index,
          //                   reuseTimeFrame: value,
          //                 )),
          //           );
          //         },
          //       ),
          //       Expanded(
          //         child: Padding(
          //           padding: EdgeInsets.only(top: 16.h),
          //           child: RichText(
          //             text: TextSpan(
          //               children: [
          //                 const TextSpan(text: 'Áp dụng thời gian này cho '),
          //                 TextSpan(
          //                     text: 'tất cả',
          //                     style: Theme.of(context).textTheme.bodyText1!.copyWith(fontWeight: FontWeight.bold)),
          //                 const TextSpan(text: ' các xe phía sau'),
          //               ],
          //               style: Theme.of(context).textTheme.bodyText1,
          //             ),
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
        ],
      ),
    );
  }

  Widget textErrorWidget(BuildContext context, BehaviorSubject<String> error) {
    return StreamBuilder<String>(
      stream: error.stream,
      initialData: error.valueOrNull,
      builder: (context, snapshot) {
        return (snapshot.data ?? '').isNotEmpty
            ? Container(
                padding: EdgeInsets.only(left: 20.w, top: 8.h),
                child: Text(
                  snapshot.data ?? '',
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: ColorName.error,
                  ),
                  maxLines: 2,
                ),
              )
            : const SizedBox.shrink();
      },
    );
  }
}
