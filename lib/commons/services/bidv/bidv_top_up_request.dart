// import 'package:crypto/crypto.dart';
import 'package:epass/flavors.dart';
import 'package:jiffy/jiffy.dart';
import 'package:uuid/uuid.dart';

class BIDVTopUpPayGateServiceRequest {
  /// Mã dịch vụ. Do BIDV cung cấp
  final String serviceID = F.bidvPayGateMerchantId;

  /// Mã đối tác mà BIDV đã cung cấp
  final String merchantCode = F.bidvPayGateMerchantId;

  /// Tên đối tác
  final String merchantName = 'EPASS';

  /// Ngày liên kết
  String transDate = '';

  /// Mã giao dịch duy nhất bên phía đối tác. Lưu ý không dùng tiếng Việt
  String transId = '';

  /// Thông tin mô tả
  final String transDes = 'Nap tien vao tai khoan ePass';

  /// Giá trị giao dịch. Mặc định bằng 0
  final int transAmount;

  String curr = 'VND';

  /// Payer_Id của đối tác. Mặc định là số điện thoại của khách hàng
  final String? payerId;

  /// Payer_Name của đối tác. Mặc định là họ và tên tên của khách hàng
  final String? payerName;

  /// Địa chỉ của khách hàng
  String? payerAdd;

  String type = '';

  String customerId = '';

  String customerName = '';

  String issueDate = '';

  /// Mã kênh giao dịch. Mặc định Mobile=211701, Web=211601
  final String channelId = '211701';

  final String linkType = '1';

  String otpNumber = '';

  /// Thông tin bổ sung. Bao gồm:
  /// 0 KH chủ động hủy liên kết. 1 hủy liên kết hộ khách hàng
  final String unlinkType = '';

  /// Số điện thoại của khách hàng
  final String? payerMobile;

  /// Số CMT/CCCD/hộ chiếu của khách hàng
  final String? payerIdentity;

  /// Địa chỉ để chuyển sau khi KH liên kết. Url này sử dụng trong trường
  /// hợp đối tác không truyền trong quá trình liên kết.
  final String returnUrl = '/success';

  /// Địa chỉ để chuyển sau nếu KH hủy giao dịch
  final String cancelUrl = '/cancel';

  /// String XML
  String wrapperXml = '';

  /// String message to encode Sha1withRSA
  String dataEncode = '';

  BIDVTopUpPayGateServiceRequest({
    required this.transAmount,
    required this.payerId,
    required this.payerName,
    required this.payerAdd,
    required this.payerMobile,
    required this.payerIdentity,
  });

  void getBidvPayInfo() async {
    var uuid = const Uuid();

    String transId = Jiffy().format('yyyyMMddHHmmssSSS');
    //String transId = uuid.v1();

    String transDate = '';

    const privateKey = 'BIDV_EPASS#123';

    final moreInfo =
        '${unlinkType.isNotEmpty ? unlinkType : ''}|$payerMobile|$payerIdentity';

    // Encode your message with the private key
    dataEncode = '$privateKey|'
        '$serviceID|'
        '$merchantCode|'
        '${merchantName.isNotEmpty ? merchantName : ''}|'
        '${transDate.isNotEmpty ? transDate : ''}|'
        '$transId|'
        '$transDes|'
        '$transAmount|'
        '$curr|'
        '$payerId|'
        '$payerName|'
        '$payerAdd|'
        '$type|'
        '$customerId|'
        '$customerName|'
        '$issueDate|'
        '$channelId|'
        '$linkType|'
        '$otpNumber|'
        '$moreInfo';

    wrapperXml = '''
         <ncc:Service_Id>$serviceID</ncc:Service_Id>
         <ncc:Merchant_Id>$merchantCode</ncc:Merchant_Id>
         <ncc:Merchant_Name>$merchantName</ncc:Merchant_Name>
         <ncc:Trandate>$transDate</ncc:Trandate>
         <ncc:Trans_Id>$transId</ncc:Trans_Id>
         <ncc:Trans_Desc>$transDes</ncc:Trans_Desc>
         <ncc:Amount>$transAmount</ncc:Amount>
         <ncc:Curr>$curr</ncc:Curr>
         <ncc:Payer_Id>$payerId</ncc:Payer_Id>
         <ncc:Payer_Name>$payerName</ncc:Payer_Name>
         <ncc:Payer_Addr>$payerAdd</ncc:Payer_Addr>
         <ncc:Type>$type</ncc:Type>
         <ncc:Custmer_Id>$customerId</ncc:Custmer_Id>
         <ncc:Customer_Name>$customerName</ncc:Customer_Name>
         <ncc:IssueDate>$issueDate</ncc:IssueDate>
         <ncc:Channel_Id>$channelId</ncc:Channel_Id>
         <ncc:Link_Type>$linkType</ncc:Link_Type>
         <ncc:Otp_Number>$otpNumber</ncc:Otp_Number>
         <ncc:More_Info>$moreInfo</ncc:More_Info>
      ''';

    final ans = <String>[];
    ans.addAll([wrapperXml, dataEncode]);
  }
}
