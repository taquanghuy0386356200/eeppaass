part of 'login_form_bloc.dart';

@immutable
abstract class LoginFormState extends Equatable {
  const LoginFormState();
}

class LoginFormInitial extends LoginFormState {
  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

class LoginFormLoading extends LoginFormState {
  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

class AuthenticationFailure extends LoginFormState {
  final String message;

  const AuthenticationFailure(this.message);

  @override
  List<Object?> get props => [message];

  @override
  bool? get stringify => true;
}

class AuthenticationSuccess extends LoginFormState with EquatableMixin {
  final AuthInfo authInfo;

  const AuthenticationSuccess(this.authInfo);

  @override
  List<Object> get props => [authInfo];

  @override
  bool? get stringify => true;
}

class LoginFormContractNoFetchedCompleted extends LoginFormState {
  final String? contractNo;

  const LoginFormContractNoFetchedCompleted(this.contractNo);

  @override
  List<Object?> get props => [contractNo];
}
