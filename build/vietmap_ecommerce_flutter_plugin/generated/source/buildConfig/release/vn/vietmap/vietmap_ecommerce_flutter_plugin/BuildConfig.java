/**
 * Automatically generated file. DO NOT MODIFY
 */
package vn.vietmap.vietmap_ecommerce_flutter_plugin;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "vn.vietmap.vietmap_ecommerce_flutter_plugin";
  public static final String BUILD_TYPE = "release";
}
