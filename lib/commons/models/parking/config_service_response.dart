

import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'config_service_response.g.dart';

@JsonSerializable()
class ServiceModel {
  int serviceId;
  String code;
  String serviceName;
  int status;
  List<ServiceRule>? serviceRules;
  List<ServiceConfig>? serviceConfig;
  List<VerifyConfig>? verifyConfig;

  ServiceModel({
    required this.serviceId,
    required this.code,
    required this.serviceName,
    required this.status,
    required this.serviceRules,
    required this.serviceConfig,
    required this.verifyConfig
  });

  factory ServiceModel.fromJson(Map<String, dynamic> json) => _$ServiceModelFromJson(json);
  Map<String, dynamic> toJson() => _$ServiceModelToJson(this);
}

@JsonSerializable()
class ServiceRule{
  int serviceRuleId;
  int serviceId;
  String ruleContent;
  int status;

  ServiceRule({
    required this.serviceRuleId,
    required this.serviceId,
    required this.ruleContent,
    required this.status
  });

  factory ServiceRule.fromJson(Map<String, dynamic> json) => _$ServiceRuleFromJson(json);
  Map<String, dynamic> toJson() => _$ServiceRuleToJson(this);
}

@JsonSerializable()
class ServiceConfig {
  int serviceConfigId;
  int serviceId;
  String? serviceImage;
  String? serviceIntroduce;
  int status;

  ServiceConfig({
    required this.serviceConfigId,
    required this.serviceId,
    this.serviceImage,
    required this.serviceIntroduce,
    required this.status
  });

  factory ServiceConfig.fromJson(Map<String, dynamic> json) => _$ServiceConfigFromJson(json);
  Map<String, dynamic> toJson() => _$ServiceConfigToJson(this);
}

@JsonSerializable()
class VerifyConfig{
  int verifyConfigId;
  int serviceId;
  int verifyType;
  String? description;
  int status;

  VerifyConfig({
    required this.verifyConfigId,
    required this.serviceId,
    required this.verifyType,
    this.description,
    required this.status
  });

  factory VerifyConfig.fromJson(Map<String, dynamic> json) => _$VerifyConfigFromJson(json);
  Map<String, dynamic> toJson() => _$VerifyConfigToJson(this);
}

@JsonSerializable()
class ConfigServiceModel {
  List<ServiceModel>? services;
  ConfigServiceModel({
    this.services,
  });

  factory ConfigServiceModel.fromJson(Map<String, dynamic> json) => _$ConfigServiceModelFromJson(json);
  Map<String, dynamic> toJson() => _$ConfigServiceModelToJson(this);

}

@JsonSerializable()
class ConfigServiceResponse {
  ConfigServiceResponse({
    this.data,
    this.mess,
  });

  final ConfigServiceModel? data;
  final MessageData? mess;

  factory ConfigServiceResponse.fromJson(Map<String, dynamic> json) => _$ConfigServiceResponseFromJson(json);
}