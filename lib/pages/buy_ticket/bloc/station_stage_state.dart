part of 'station_stage_bloc.dart';

class StationStageState extends Equatable {
  final StationStage? stationStage;

  const StationStageState({this.stationStage});

  @override
  List<Object?> get props => [stationStage];
}
