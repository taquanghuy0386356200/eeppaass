import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/register_repository.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/login/bloc/app_version_bloc.dart';
import 'package:epass/pages/register/bloc/register_bloc.dart';
import 'package:epass/pages/register/bloc/register_customer/register_customer_bloc.dart';
import 'package:epass/pages/register/bloc/register_paper_scan/register_id_card_bloc.dart';
import 'package:epass/pages/register/bloc/register_paper_scan/register_paper_scan_permission_bloc.dart';
import 'package:epass/pages/register/bloc/register_service/register_service_bloc.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/bloc/request_otp/register_otp_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterBlocProviderPage extends StatelessWidget {
  const RegisterBlocProviderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final registerCustomerBloc = RegisterCustomerBloc();

    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => RegisterBloc(registerRepository: getIt<IRegisterRepository>())),
        BlocProvider(create: (context) => RegisterVehiclesBloc()),
        BlocProvider(create: (context) => RegisterPaperScanPermissionBloc()),
        BlocProvider(create: (context) => RegisterIdCardBloc(registerCustomerBloc: registerCustomerBloc)),
        BlocProvider(create: (context) => registerCustomerBloc),
        BlocProvider(create: (context) => RegisterOtpBloc(registerRepository: getIt<IRegisterRepository>())),
        BlocProvider(create: (context) => RegisterServiceBloc()),
        BlocProvider.value(value: getIt<AppVersionBloc>())
      ],
      child: const AutoRouter(),
    );
  }
}
