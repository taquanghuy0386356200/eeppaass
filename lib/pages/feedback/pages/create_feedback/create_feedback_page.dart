import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/feedback/pages/create_feedback/bloc/create_feedback_otp_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_form/bloc/feedback_form_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_form/feedback_form.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

class CreateFeedbackPage extends StatelessWidget {
  const CreateFeedbackPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => FilePickerBloc(
            imagePicker: getIt<ImagePicker>(),
            filePicker: getIt<FilePicker>(),
          ),
        ),
        BlocProvider(
          create: (context) => CreateFeedbackOtpBloc(
            feedbackRepository: getIt<IFeedbackRepository>(),
          ),
        ),
        BlocProvider(create: (context) => FeedbackFormBloc()),
      ],
      child: const FeedbackForm(),
    );
  }
}
