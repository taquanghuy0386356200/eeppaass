part of 'app_version_bloc.dart';

abstract class AppVersionState extends Equatable {
  const AppVersionState();
}

class AppVersionInitial extends AppVersionState {
  @override
  List<Object> get props => [];
}

class AppVersionFetchedInProgress extends AppVersionState {
  const AppVersionFetchedInProgress();

  @override
  List<Object> get props => [];
}

class AppVersionFetchedSuccess extends AppVersionState {
  final AppVersion appVersion;

  const AppVersionFetchedSuccess(this.appVersion);

  @override
  List<Object> get props => [appVersion];
}

class AppVersionFetchedFailure extends AppVersionState {
  final String message;

  const AppVersionFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class RegistFlagFetchedSuccess extends AppVersionState {
  final String isOnRegistration;
  final String registerFee;

  const RegistFlagFetchedSuccess({
    required this.isOnRegistration,
    required this.registerFee,
  });

  @override
  List<Object> get props => [isOnRegistration, registerFee];
}

class RegistFlagFetchedFailure extends AppVersionState {
  final String message;

  const RegistFlagFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

