class DataPayment {
  final String? title;
  final List<DataBank>? dataBank;

  const DataPayment({
    this.title,
    this.dataBank,
  });
}

class DataBank {
  final String? icon;
  final String? content;

  const DataBank({
    this.icon,
    this.content,
  });
}

List<DataPayment> dataPayment =  const [
    DataPayment(
    title: "Internet Banking",
    dataBank: [
      DataBank(
        icon: 'Bidv',
        content: 'Miễn phí'
      ),
      DataBank(
          icon: 'OCB',
          content: 'Miễn phí'
      ),
      DataBank(
          icon: 'Mb',
          content: 'Miễn phí'
      ),
      DataBank(
          icon: 'Vib',
          content: 'Miễn phí'
      ),
      DataBank(
          icon: 'HDBank',
          content: 'Miễn phí'
      ),
      DataBank(
          icon: 'Vp',
          content: 'Miễn phí'
      )

    ]
  ),
   DataPayment(
      title: "Internet Banking",
      dataBank: [
        DataBank(
            icon: 'Bidv',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'OCB',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Mb',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Vib',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'HDBank',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Vp',
            content: 'Miễn phí'
        )

      ]
  ),
  DataPayment(
      title: "Internet Banking",
      dataBank: [
        DataBank(
            icon: 'Bidv',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'OCB',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Mb',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Vib',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'HDBank',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Vp',
            content: 'Miễn phí'
        )

      ]
  )
];
List<DataPayment> dataPaymentBottom =  const [
  DataPayment(
      title: "Thẻ ATM nội địa",
      dataBank: [
        DataBank(
            icon: 'icon',
            content: 'Phí giao dịch: 880đ +0.66% giao dịch'
        )
      ]
  ),
  DataPayment(
      title: "Thẻ ghi nợ/ tín dụng",
      dataBank: [
        DataBank(
            icon: 'Bidv',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'OCB',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Mb',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Vib',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'HDBank',
            content: 'Miễn phí'
        ),
        DataBank(
            icon: 'Vp',
            content: 'Miễn phí'
        )

      ]
  ),

];