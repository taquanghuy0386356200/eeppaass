import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/vietmap/vietmap_product.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VietMapProductCard extends StatelessWidget {
  final VietMapProduct item;
  final void Function()? onTap;

  const VietMapProductCard({
    Key? key,
    required this.item,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.r),
      child: ShadowCard(
        shadowOpacity: 0.2,
        child: InkWell(
          borderRadius: BorderRadius.circular(16.0),
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.fromLTRB(4.w, 16.h, 4.w, 12.h),
            child: Column(
              children: [
                Image.network(
                    item.thumbnail ?? '',
                  width: 50.w,
                  height: 50.h,
                ),
                SizedBox(height: 8.h),
                Text(item.name ?? ''),
                SizedBox(height: 8.h),
                Text(
                  (item.price ?? 0).formatCurrency(symbol: 'đ'),
                  style: Theme.of(context).textTheme.bodyText2?.copyWith(
                    color: ColorName.primaryColor
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
