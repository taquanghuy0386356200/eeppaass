class ForgotPasswordFormValidator {
  String? plateNumberValidator(String? plateNumber) {
    if (plateNumber?.trim().isEmpty ?? true) {
      return 'Vui lòng nhập Biển số xe';
    }
    return null;
  }

  String? plateTypeValidator(String? plateType) {
    if (plateType?.trim().isEmpty ?? true) {
      return 'Vui lòng chọn Loại biển';
    }
    return null;
  }
}
