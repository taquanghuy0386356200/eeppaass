import 'package:epass/commons/services/local_auth/local_auth_service.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';

extension LocalAuthenticationTypeExt on LocalAuthenticationType {
  Widget getIcon({
    Color? color,
    double? height,
    double? width,
  }) {
    switch (this) {
      case LocalAuthenticationType.face:
        return Assets.icons.faceId.svg(
          color: color,
          height: height,
          width: width,
        );
      case LocalAuthenticationType.fingerprint:
        return Assets.icons.touchId.svg(
          color: color,
          height: height,
          width: width,
        );
    }
  }
}
