import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/app_version_repository.dart';
import 'package:epass/commons/repo/auto_cash_in_repository.dart';
import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/services/bidv/bidv_check_connection.dart';
import 'package:epass/commons/services/bidv/bidv_connection.dart';
import 'package:epass/commons/services/mbbank/mb_check_payment_result_service.dart';
import 'package:epass/commons/services/mbbank/mb_check_sum_service.dart';
import 'package:epass/commons/services/mbbank/mb_get_token_service.dart';
import 'package:epass/commons/services/mbbank/mb_payment_request.dart';
import 'package:epass/commons/services/momo/momo_payment.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment.dart';
import 'package:epass/commons/services/vnpt/vnpt_create_order.dart';
import 'package:epass/commons/services/zalopay/zalo_pay_create_order.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/beneficary/beneficary_list/bloc/beneficary_list_bloc.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/cash_in/bloc/active_otp_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_check_connection_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_connection_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/bloc/create_beneficiary_bloc.dart';
import 'package:epass/pages/cash_in/bloc/mb_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/paynow_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/viettel_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/vnpay_bloc.dart';
import 'package:epass/pages/cash_in/bloc/vnpt_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/zalopay_gate_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/bloc/payment_methods_card_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CashInBlocProviderPage extends StatelessWidget {
  final Beneficiary? beneficary;

  const CashInBlocProviderPage({Key? key, this.beneficary}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => BalanceBloc(
            userRepository: getIt<IUserRepository>(),
            appBloc: context.read<AppBloc>(),
          )..add(BalanceFetched()),
        ),
        BlocProvider(
          create: (_) => CashInTargetBloc(
            appBloc: context.read<AppBloc>(),
            cashInRepository: getIt<ICashInRepository>(),
            beneficary: beneficary,
          ),
        ),
        BlocProvider(
            create: (_) =>
                CashInAmountBloc(cashInRepository: getIt<ICashInRepository>())),
        BlocProvider(
          create: (_) => PaymentMethodsCardBloc(
            cashInRepository: getIt<ICashInRepository>(),
          )..add(PaymentMethodsFetched()),
        ),
        BlocProvider(
          create: (_) => CashInBloc(
            cashInRepository: getIt<ICashInRepository>(),
            momoService: getIt<MomoPayment>(),
            vnpayService: getIt<VNPayService>(),
          ),
        ),
        BlocProvider(create: (_) => ViettelPayGateBloc()),
        BlocProvider(create: (_) => PayNowPayGateBloc()),
        BlocProvider(create: (_) => BIDVPayGateBloc()),
        BlocProvider(
            create: (_) => MBPayGateBloc(
                getTokenService: MBGetTokenService(),
                createOrderService: MBCreateOrderService(),
                checkSumService: MBCheckSumService(),
                checkPayResultService: MBCheckPayResultService())),
        BlocProvider(
            create: (_) =>
                VnptGateBloc(vpntCreateOrderService: VnptCreateOrderService())),
        BlocProvider(
            create: (_) => ZaloPayGateBloc(
                zalopayCreateOrderService: ZaloPayCreateOrderService())),
        BlocProvider(
            create: (_) => BidvCheckingConnectionBloc(
                bidvCheckingConnectionService:
                    BidvCheckingConnectionService())),
        BlocProvider(
            create: (_) => BidvConnectionBloc(
                bidvConnectionService: BidvConnectionService())),
        BlocProvider(
            create: (_) => MBPayGateBloc(
                getTokenService: MBGetTokenService(),
                createOrderService: MBCreateOrderService(),
                checkSumService: MBCheckSumService(),
                checkPayResultService: MBCheckPayResultService())),
        BlocProvider(
            create: (_) =>
                VnptGateBloc(vpntCreateOrderService: VnptCreateOrderService())),
        BlocProvider(
            create: (_) => ZaloPayGateBloc(
                zalopayCreateOrderService: ZaloPayCreateOrderService())),
        BlocProvider(
            create: (_) => CreateBeneficiaryBloc(
                beneficiaryRepository: getIt<IBeneficiaryRepository>())),
        BlocProvider(
          create: (_) => VNPayBloc(vnpayService: getIt<VNPayService>()),
        ),
        BlocProvider(create: (_) => ViettelPayGateBloc()),
        BlocProvider(
            create: (_) => ActiveOtpBloc(
                feedbackRepository: getIt<IFeedbackRepository>())),
        BlocProvider(
          create: (_) => VNPayBloc(vnpayService: getIt<VNPayService>()),
        ),
        BlocProvider(
          create: (_) => BeneficiaryListBloc(
              beneficiaryRepository: getIt<IBeneficiaryRepository>()),
        ),
      ],
      child: const AutoRouter(),
    );
  }
}
