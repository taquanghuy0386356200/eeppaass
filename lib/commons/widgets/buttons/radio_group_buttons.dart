import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RadioGroupButtons<T> extends StatefulWidget {
  final T? defaultValue;
  final List<T> values;
  final List<String> labels;
  final ValueChanged<T?>? onChanged;
  final Axis scrollDirection;

  const RadioGroupButtons({
    Key? key,
    this.defaultValue,
    required this.values,
    required this.labels,
    this.onChanged,
    this.scrollDirection = Axis.vertical,
  }) : super(key: key);

  @override
  State<RadioGroupButtons<T>> createState() => _RadioGroupButtonsState<T>();
}

class _RadioGroupButtonsState<T> extends State<RadioGroupButtons<T>> {
  T? _value;

  @override
  void initState() {
    super.initState();
    _value = widget.defaultValue;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.scrollDirection == Axis.horizontal ? 44.h : null,
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        scrollDirection: widget.scrollDirection,
        itemCount: widget.values.length,
        itemBuilder: (context, index) => Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Transform.scale(
              scale: 1.2,
              child: Radio<T>(
                value: widget.values[index],
                groupValue: _value,
                visualDensity: VisualDensity.compact,
                activeColor: ColorName.primaryColor,
                onChanged: (value) {
                  setState(() => _value = value);
                  widget.onChanged?.call(_value);
                },
              ),
            ),
            Flexible(
              fit: widget.scrollDirection == Axis.horizontal ? FlexFit.loose : FlexFit.tight,
              child: Material(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(8.0),
                child: InkWell(
                  borderRadius: BorderRadius.circular(8.0),
                  onTap: () {
                    setState(() => _value = widget.values[index]);
                    widget.onChanged?.call(_value);
                  },
                  child: Padding(
                    padding: EdgeInsets.all(8.r),
                    child: Text(
                      widget.labels[index],
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: ColorName.textGray1),
                      maxLines: 3,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        separatorBuilder: (BuildContext context, int index) {
          return widget.scrollDirection == Axis.horizontal
              ? SizedBox(width: 32.w)
              : const SizedBox.shrink();
        },
      ),
    );
  }
}
