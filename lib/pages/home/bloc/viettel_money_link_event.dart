part of 'viettel_money_link_bloc.dart';

abstract class ViettelMoneyLinkEvent extends Equatable {
  const ViettelMoneyLinkEvent();
}

class ViettelMoneyLinkAccount extends ViettelMoneyLinkEvent {
  final String contractId;
  final String contractNo;
  final String contractFullName;

  const ViettelMoneyLinkAccount({
    required this.contractId,
    required this.contractNo,
    required this.contractFullName,
  });

  @override
  List<Object> get props => [contractId, contractNo, contractFullName];
}

class ViettelMoneyUnlinkAccount extends ViettelMoneyLinkEvent {
  final String accountNumber;
  final String token;
  final String contractId;

  const ViettelMoneyUnlinkAccount({
    required this.accountNumber,
    required this.token,
    required this.contractId,
  });

  @override
  List<Object> get props => [accountNumber, token, contractId];
}
