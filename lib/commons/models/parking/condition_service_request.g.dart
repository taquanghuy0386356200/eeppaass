// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'condition_service_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConditionServiceRequest _$ConditionServiceRequestFromJson(
        Map<String, dynamic> json) =>
    ConditionServiceRequest(
      isRegisterService: json['isRegisterService'] as bool? ?? true,
      serviceId: json['serviceId'] as int,
    );

Map<String, dynamic> _$ConditionServiceRequestToJson(
        ConditionServiceRequest instance) =>
    <String, dynamic>{
      'isRegisterService': instance.isRegisterService,
      'serviceId': instance.serviceId,
    };
