import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'viet_map_status_response.g.dart';

@JsonSerializable()
class VietMapStatus {
  VietMapStatus({
    this.linked,
    this.accountName,
    this.phoneNumber,
    this.token,
  });

  final bool? linked;
  final String? accountName;
  final String? phoneNumber;
  final String? token;

  factory VietMapStatus.fromJson(Map<String, dynamic> json) =>
      _$VietMapStatusFromJson(json);
}

@JsonSerializable()
class VietMapStatusResponse {
  final VietMapStatus data;
  final MessageData? mess;

  VietMapStatusResponse({required this.data, this.mess});

  factory VietMapStatusResponse.fromJson(Map<String, dynamic> json) =>
      _$VietMapStatusResponseFromJson(json);
}
