import 'package:epass/commons/models/report/report_attachment_file.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// l1TicketTypeName : "3. Khiếu nại ePass CPT"
/// l2TicketTypeName : "Khiếu nại nhân viên trạm"
/// l3TicketTypeName : "Nghiệp vụ NV trạm"
/// contentReceive : "Noi dung phan anh test"
/// createDate : "27/08/2021 16:33:08"
/// statusName : "Tạo mới"
/// processDate: ""
/// staffName: ""
/// processContent: ""
/// attachmentFiles : [{"fileName":"Danhsachhoso_161439.pdf","attachmentId":300}]
/// fileName : "Danhsachhoso_161439.pdf"
/// attachmentId : "300"

part 'report_response.g.dart';

@JsonSerializable()
class ReportModel {
  ReportModel({
      this.l1TicketTypeName,
      this.l2TicketTypeName,
      this.l3TicketTypeName,
      this.contentReceive,
      this.createDate,
      this.statusName,
      this.processDate,
      this.staffName,
      this.processContent,
      this.attachmentFiles,
      this.fileName,
      this.attachmentId,
  });

  String? l1TicketTypeName;
  String? l2TicketTypeName;
  String? l3TicketTypeName;
  String? contentReceive;
  String? createDate;
  String? statusName;
  String? processDate;
  String? staffName;
  String? processContent;
  List<ReportAttachmentFileResponse>? attachmentFiles;
  String? fileName;
  String? attachmentId;


  factory ReportModel.fromJson(Map<String, dynamic> json) => _$ReportModelFromJson(json);
}

@JsonSerializable()
class ReportResponse {
  ReportResponse({
    this.data,
    this.mess,
  });

  final List<ReportModel>? data;
  final MessageData? mess;

  factory ReportResponse.fromJson(Map<String, dynamic> json) => _$ReportResponseFromJson(json);
}
