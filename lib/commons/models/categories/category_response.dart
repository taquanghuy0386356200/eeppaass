import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

/// id : "3658"
/// table_name : "PLATE_TYPE"
/// name : "Biển đỏ"
/// code : "3"
/// is_default : "1"
/// description : "Biển đỏ"
/// is_active : "1"

part 'category_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CategoryModel extends Equatable {
  const CategoryModel({
    this.id,
    this.tableName,
    this.name,
    this.code,
    this.isDefault,
    this.description,
    this.isActive,
  });

  final dynamic id;
  final String? tableName;
  final String? name;
  final String? code;
  final String? isDefault;
  final String? description;
  final String? isActive;

  @override
  List<Object?> get props => [
        id,
        tableName,
        name,
        code,
        isDefault,
        description,
        isActive,
      ];

  factory CategoryModel.fromJson(Map<String, dynamic> json) => _$CategoryModelFromJson(json);
}

@JsonSerializable()
class CategoryModelData {
  final List<CategoryModel>? listData;
  final int? count;

  CategoryModelData({this.listData, this.count});

  factory CategoryModelData.fromJson(Map<String, dynamic> json) => _$CategoryModelDataFromJson(json);
}

class CategoryModelDataNotNull {
  final List<CategoryModel> listData;
  final int count;

  CategoryModelDataNotNull({required this.listData, required this.count});
}

@JsonSerializable()
class CategoryModelResponse {
  final CategoryModelData? data;
  final MessageData? mess;

  CategoryModelResponse({this.data, this.mess});

  factory CategoryModelResponse.fromJson(Map<String, dynamic> json) => _$CategoryModelResponseFromJson(json);
}
