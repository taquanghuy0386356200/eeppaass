import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/bloc/payment_methods_card_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class CashInBuilder extends StatelessWidget {
  const CashInBuilder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<CashInAmountBloc>().add(CashInTargetLastRecharge());
    return Builder(
      builder: (context) {
        final amount = context.select((CashInAmountBloc bloc) {
          final state = bloc.state;
          return state is CashInAmountChangedSuccess ? state.amount : null;
        });

        final contractNo = context.select(
          (CashInTargetBloc bloc) => bloc.state.contractNo,
        );

        final contractId = context.select(
          (CashInTargetBloc bloc) => bloc.state.contractId,
        );

        var paymentMethod = context.select((PaymentMethodsCardBloc bloc) {
          final state = bloc.state;
          return state is PaymentMethodsFetchedSuccess
              ? state.selectedPaymentMethod
              : null;
        });
        // code cũ vdss1
        // final cashInEnabled =
        //     amount != null && contractNo != null && paymentMethod?.code != null;
        //fix bug nap tien vdss2
        final cashInEnabled =
            amount != null && contractNo != null && paymentMethod?.code.name != 'unknown';

        return BlocConsumer<CashInBloc, CashInState>(
          listener: (context, state) async {
            if (amount != null && paymentMethod != null && contractId != null) {
              if (state is CashInFetchedTopupFeeSuccess) {
                if (paymentMethod.code == TopupChannelCode.BankTransfer) {
                  context.pushRoute(const CashInBankTransferRoute());
                } else {
                  context.pushRoute(const CashInConfirmRoute());
                }
              } else if (state is CashInFetchedTopupFeeFailure) {
                showErrorSnackBBar(context: context, message: state.message);
              } else if (state is CheckCustomerTypeSuccess) {
                //
                if (state.deposit!) {
                  context.read<CashInBloc>().add(
                        CashInFetchedTopupFee(
                          topupChannelCode: paymentMethod.code,
                          value: amount,
                        ),
                      );
                } else {
                  final NumberFormat currencyFormatter =
                      NumberFormat('#,##0', 'en_US');
                  final limitTopup = currencyFormatter.format(state.limit);
                  showErrorSnackBBar(
                      context: context,
                      message: 'Số tiền nạp không quá ${limitTopup}đ');
                }
              } else if (state is CheckCustomerTypeFailure) {
                showErrorSnackBBar(context: context, message: state.message);
              }
            }
          },
          builder: (context, state) {
            return LoadingPrimaryButton(
              title: 'Nạp tiền',
              enabled: cashInEnabled,
              isLoading: state is CashInFetchedTopupFeeInProgress,
              onTap: cashInEnabled
                  ? () {
                      FocusScope.of(context).unfocus();
                      context.read<CashInBloc>().add(
                            CheckCustomerType(
                              contractId: contractId!,
                              amount: amount!,
                            ),
                          );
                    }
                  : null,
            );
          },
        );
      },
    );
  }
}
