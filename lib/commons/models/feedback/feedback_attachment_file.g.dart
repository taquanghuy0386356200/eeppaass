// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feedback_attachment_file.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackAttachmentFileResponse _$FeedbackAttachmentFileResponseFromJson(
        Map<String, dynamic> json) =>
    FeedbackAttachmentFileResponse(
      fileName: json['fileName'] as String?,
      attachmentId: json['attachmentId'] as int?,
    );

Map<String, dynamic> _$FeedbackAttachmentFileResponseToJson(
        FeedbackAttachmentFileResponse instance) =>
    <String, dynamic>{
      'fileName': instance.fileName,
      'attachmentId': instance.attachmentId,
    };

FeedbackAttachmentFileRequest _$FeedbackAttachmentFileRequestFromJson(
        Map<String, dynamic> json) =>
    FeedbackAttachmentFileRequest(
      fileName: json['fileName'] as String,
      base64Data: json['base64Data'] as String,
      fileSize: (json['fileSize'] as num).toDouble(),
    );

Map<String, dynamic> _$FeedbackAttachmentFileRequestToJson(
        FeedbackAttachmentFileRequest instance) =>
    <String, dynamic>{
      'fileName': instance.fileName,
      'base64Data': instance.base64Data,
      'fileSize': instance.fileSize,
    };
