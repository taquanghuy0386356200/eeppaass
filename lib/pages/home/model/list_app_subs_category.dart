
import 'package:epass/pages/home/model/list_app.dart';
import 'package:json_annotation/json_annotation.dart';
@JsonSerializable()
class ListAppSubsCategory {
  ListAppSubsCategory({
    this.listApp,
  });

  ListAppSubsCategory.fromJson(dynamic json) {
    if (json['listApp'] != null) {
      listApp = [];
      json['listApp'].forEach((v) {
        listApp?.add(ListApp.fromJson(v));
      });
    }
    cateId = json['cateId'];
    cateName = json['cateName'];
    catePriority = json['catePriority'];
  }

  List<ListApp>? listApp;
  int ? cateId;
  String ? cateName;
  int ? catePriority;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (listApp != null) {
      map['listApp'] = listApp?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
