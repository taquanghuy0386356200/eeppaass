// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/bidv_pay_gate/link_bidv_pay_gate.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/models/topup_channel/topup_channel_fee.dart';
import 'package:epass/commons/models/viettel_pay_gate/viettel_pay_gate.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/services/bidv/bidv_link_request.dart';
import 'package:epass/commons/services/bidv/bidv_top_up_request.dart';
import 'package:epass/commons/services/bidv/bidv_check_link_request.dart';
import 'package:epass/commons/services/vnpt/vnpay_payment_request.dart';
// import 'package:epass/commons/services/zalopay/zalo_pay_create_order.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/cash_in/bloc/bidv_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/bloc/mb_pay_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/vnpay_bloc.dart';
import 'package:epass/pages/cash_in/bloc/vnpt_gate_bloc.dart';
import 'package:epass/pages/cash_in/bloc/zalopay_gate_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/bloc/payment_methods_card_bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:uuid/uuid.dart';

class CashInConfirmBuilder extends StatefulWidget {
  const CashInConfirmBuilder({
    Key? key,
  }) : super(key: key);

  @override
  State<CashInConfirmBuilder> createState() => _CashInConfirmBuilderState();
}

class _CashInConfirmBuilderState extends State<CashInConfirmBuilder> {
  static const MethodChannel channelMBBCreateOrder =
      MethodChannel('flutter.native/channelMBBCreateOrder');
  static const EventChannel eventChannel =
      EventChannel('flutter.native/eventPayOrder');
  static const MethodChannel platform =
      MethodChannel('flutter.native/channelPayOrder');
  String payResult = "";
  String zpTransToken = "";
  bool showResult = false;

  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      eventChannel.receiveBroadcastStream().listen(_onEvent, onError: _onError);
    }
  }

  void _vnptPayok(
    int status,
    String topupChannel,
    String contractId,
    String transactionId,
    int amount,
  ) {
    context.loaderOverlay.hide();
    if (status == 0) {
      context.replaceRoute(CashInResultRoute(
        isSuccess: false,
        message: "Thanh Toan Thai Bai",
      ));
    } else if (status == 1) {
      context.replaceRoute(CashInResultRoute(
        isSuccess: false,
        message: "Thanh Toan Thai Bai",
      ));
      //thanh cong
      // context.read<CashInBloc>().add(CashInContractsPayment(
      //     topupChannel: 'VnptPay',
      //     destContractId: contractId,
      //    requestId: transactionId,
      //    amount: amount!
      //   ));
    }
  }

  String topupChannelZalo = "";
  String contractIdZalo = "";
  String zptranstokenZalo = "";
  int transAmount = 0;
  void _onEvent(var event) {
    var res = Map<String, dynamic>.from(event);
    if (res["poupcode"] == 'mbbankepass') {
      var url = res["url"];
      if (url.indexOf('checksum') >= 0) {
        _onMBCheckSum(url);
      } else {
        _onMBError("Payment failes");
      }
    } else if (res["poupcode"] == 'zalopay') {
      setState(() {
        if (res["errorCode"] == 1) {
          var appTransId = res["appTransId"];
          payResult = "Payment Success";
          context.loaderOverlay.show();
          context.read<CashInBloc>().add(CashInContractsPayment(
                topupChannel: topupChannelZalo,
                destContractId: contractIdZalo,
                requestId: appTransId,
                amount: transAmount,
              ));
          context.read<BalanceBloc>().add(BalanceFetched());
        } else if (res["errorCode"] == 4) {
          payResult = "User Canceled";
          context.loaderOverlay.hide();
          context.replaceRoute(CashInResultRoute(
            isSuccess: false,
            message: "Người dùng ngừng thanh toán",
          ));
        } else {
          payResult = "Payment failed";
          context.loaderOverlay.hide();
          context.replaceRoute(CashInResultRoute(
            isSuccess: false,
            message: "Thanh toán thất bại",
          ));
        }
      });
    }
  }

  void _onError(Object error) {
    print("_onError: '$error'.");
    setState(() {
      payResult = "Payment failed";
      context.loaderOverlay.hide();
      context.replaceRoute(CashInResultRoute(
        isSuccess: false,
        message: payResult,
      ));
    });
  }

  ///===== MB Bank
  //dumv luu lai thong tin trong qua trinh giao dich voi app mb
  late MBGetTokenCompletedSuccess mbStateTk;
  late MBCreateOrderCompletedSuccess mbStateOd;
  late MBCheckSumCompletedSuccess mbStateCS;
  late String checksum = ""; // chuoi thanh toan khi app mb tra ve

  void _onMBError(String message) {
    context.loaderOverlay.hide();
    context.pushRoute(CashInResultRoute(isSuccess: false, message: message));
  }

//lay token dang nhap de thuc hien thanh toan
  void _onMBGetToken() {
    context.loaderOverlay.show();
    context
        .read<MBPayGateBloc>()
        .add(const MBGetTokenCompleted(grantType: 'client_credentials'));
  }

  //tao order de thanh toan
  void _onMBCreateOrder(MBGetTokenCompletedSuccess state, int amount) {
    context.loaderOverlay.show();
    mbStateTk = MBGetTokenCompletedSuccess(
      accessToken: state.accessToken,
      tokenType: state.tokenType,
      expiresIn: state.expiresIn,
      issuedAt: state.issuedAt,
    );
    var transID = DateTime.now().microsecondsSinceEpoch.toString();
    context.read<MBPayGateBloc>().add(MBCreateOrderCompleted(
        amount: amount.toString(),
        transactionId: transID,
        token: mbStateTk.accessToken));
  }

  // ham mo app MB ?
  void _launchMBB(MBCreateOrderCompletedSuccess state) async {
    mbStateOd = MBCreateOrderCompletedSuccess(
      clientMessageId: state.clientMessageId,
      errorCode: state.errorCode,
      data: state.data,
    );
    context.loaderOverlay.hide();
    checksum = "";
    //android
    if (Platform.isAndroid) {
      final String results = await channelMBBCreateOrder
          .invokeMethod('payOrderMBB', {"urlMBB": mbStateOd.data.oneLink});
      var url = results;
      if (url.indexOf('checksum') >= 0) {
        _onMBCheckSum(url);
      } else {
        _onMBError(results);
      }
    } else if (Platform.isIOS) {
      //ios
      //dumvthieu ko bo bam nut xac nhan
      if (!await launchUrlString(mbStateOd.data.oneLink!)) {
        _onMBError("Không tìm thấy App MBBank");
      }
    }
  }

  // goi ham checksum
  void _onMBCheckSum(String url) async {
    context.loaderOverlay.show();
    checksum = url.split('checksum=')[1];
    context.read<MBPayGateBloc>().add(MBCheckSumCompleted(
        checkSumKey: checksum, token: mbStateTk.accessToken));
  }

  // kiem tra trang thai thanh toan
  void _onMBCheckPayment(MBCheckSumCompletedSuccess state) async {
    mbStateCS = MBCheckSumCompletedSuccess(
        clientMessageId: state.clientMessageId,
        errorCode: state.errorCode,
        transactionID: state.transactionID);
    if (state.errorCode == '000') {
      context.loaderOverlay.show();
      context.read<MBPayGateBloc>().add(MBCheckPaymentResultCompleted(
          transactionId: state.transactionID, token: mbStateTk.accessToken));
    } else {
      _onMBError('Payment failed');
    }
  }
  //=======

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        final amount = context.select((CashInAmountBloc bloc) {
          final state = bloc.state;
          return state is CashInAmountChangedSuccess ? state.amount : null;
        });

        final contractNo = context.select(
          (CashInTargetBloc bloc) => bloc.state.contractNo,
        );

        final contractId = context.select(
          (CashInTargetBloc bloc) => bloc.state.contractId,
        );

        final paymentMethod = context.select((PaymentMethodsCardBloc bloc) {
          final state = bloc.state;
          return state is PaymentMethodsFetchedSuccess
              ? state.selectedPaymentMethod
              : null;
        });

        final topupFee = context.select((CashInBloc bloc) {
          final state = bloc.state;
          return state is CashInFetchedTopupFeeSuccess ? state.fee : null;
        });

        final cashInEnabled = amount != null &&
            contractNo != null &&
            paymentMethod?.code != null &&
            topupFee != null &&
            paymentMethod != null &&
            paymentMethod.code.toString().contains('unknown') != true;

        return MultiBlocListener(
          listeners: [
            BlocListener<CashInBloc, CashInState>(
              listener: (context, state) async {
                if (amount != null &&
                    paymentMethod != null &&
                    contractId != null) {
                  if (state is CashInOrderCreatedInProgress) {
                    // create cash in order loading
                    context.loaderOverlay.show();
                  } else if (state is CashInOrderMomoCreatedSuccess) {
                    context.loaderOverlay.hide();
                    context.read<CashInBloc>().add(CashInMomoPayment(
                          orderId: state.orderId,
                          contractNo: state.contractNo,
                          amount: state.amount,
                        ));
                  } else if (state is CashInOrderCreatedFailure) {
                    context.loaderOverlay.hide();
                    // showErrorSnackBBar(context: context, message: state.message);
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: state.message,
                    ));
                  } else if (state is CashInMomoPaymentInProgress) {
                    context.loaderOverlay.show();
                  } else if (state is CashInMomoPaymentSuccess) {
                    context.loaderOverlay.hide();
                    context.read<CashInBloc>().add(
                          ConfirmMomoOrder(
                            orderId: state.orderId,
                            amount: state.amount,
                            phoneNumber: state.phoneNumber,
                            token: state.paymentToken,
                          ),
                        );
                  } else if (state is CashInMomoPaymentFailure) {
                    context.loaderOverlay.hide();
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: state.message,
                    ));
                  } else if (state is ConfirmMomoPaymentInProgress) {
                    context.loaderOverlay.show();
                  } else if (state is ConfirmMomoPaymentSuccess) {
                    context.loaderOverlay.hide();
                    context.read<BalanceBloc>().add(BalanceFetched());
                    context.replaceRoute(CashInResultRoute(isSuccess: true));
                  } else if (state is ConfirmMomoPaymentFailure) {
                    context.loaderOverlay.hide();
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: state.message,
                    ));
                  } else if (state is CashInOrderViettelPayCreatedSuccess) {
                    context.loaderOverlay.hide();
                    await _onCashInOrderViettelPayGateCreatedSuccess(
                      context: context,
                      state: state,
                      paymentMethod: paymentMethod,
                    );
                  } else if (state is CashInOrderVNPayCreatedSuccess) {
                    context.loaderOverlay.hide();
                    context.read<VNPayBloc>().add(VNPayPaymentProceeded(
                          orderId: state.orderId,
                          contractNo: state.contractNo,
                          amount: state.amount,
                        ));
                  } else if (state is CashInOrderVNPayCreatedFailure) {
                    context.loaderOverlay.hide();
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: state.message,
                    ));
                  } else if (state is CashInContractsPaymentSuccess) {
                    context.loaderOverlay.hide();
                    context.read<BalanceBloc>().add(BalanceFetched());
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: true,
                    ));
                    // context.replaceRoute(CashInBIDVPayGateRoute(title: 'abc', request: null));
                    // showErrorSnackBBar(
                    //     context: context, message: state.message);
                  } else if (state is CashInContractsPaymentFailure) {
                    context.loaderOverlay.hide();
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: state.message,
                    ));
                  } else if (state is CheckVehicleInContractFailure) {
                    context.loaderOverlay.hide();
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: state.message,
                    ));
                  } else if (state is CheckVehicleInContractSuccess) {
                    context.loaderOverlay.hide();
                    if (state.hasVehicle != null && state.hasVehicle == true) {
                      _onCashInConfirmed(
                        context: context,
                        amount: amount!,
                        contractId: contractId!,
                        paymentMethod: paymentMethod!,
                        fee: topupFee!,
                      );
                    } else {
                      context.replaceRoute(CashInResultRoute(
                        isSuccess: false,
                        message: state.message,
                      ));
                    }
                  }
                }
              },
            ),
            BlocListener<BIDVPayGateBloc, BIDVPayGateState>(
              listener: (context, state) async {
                if (amount != null &&
                    paymentMethod != null &&
                    contractId != null) {
                  if (state is BIDVPayGateLinkedCompletedSuccess) {
                    context.loaderOverlay.hide();
                    // await _onLinkToBIDVPayGateCreatedSuccess(
                    //     context: context, paymentMethod: paymentMethod);
                  } else if (state is BIDVPayGateLinkedCompletedFailure) {
                    context.loaderOverlay.hide();
                  } else if (state is BIDVPayCheckLinkedCompleted) {
                    context.loaderOverlay.hide();
                    await _onCheckLinkToBIDVPayGateCreatedSuccess(
                        context: context, paymentMethod: paymentMethod);
                  } else if (state is CheckLinkBIDVConnnectedFailure) {
                    context.loaderOverlay.hide();
                  }
                }
              },
            ),
            BlocListener<VnptGateBloc, VnptGateState>(
              listener: (context, state) {
                if (amount != null &&
                    paymentMethod != null &&
                    contractId != null) {
                  if (state is VnptPaymentSuccess) {
                    context.loaderOverlay.hide();
                    context.replaceRoute(CashInVnptPayGateRoute(
                        title: "VNPT Paygate",
                        request: VNPTRequest(
                            contractId: contractId,
                            amount: amount,
                            client_ip: ""),
                        state: state,
                        callback: _vnptPayok));
                  } else if (state is VnptPaymentFailure) {
                    context.replaceRoute(CashInResultRoute(
                        isSuccess: false, message: 'Payment Failed!'));
                    context.loaderOverlay.hide();
                  } else if (state is VnptPaymentSuccessOk) {
                    context.loaderOverlay.show();
                    context.read<CashInBloc>().add(CashInContractsPayment(
                          topupChannel: paymentMethod.code.name,
                          destContractId: contractId,
                          requestId: state.transactionId,
                          amount: amount,
                        ));
                  }
                }
              },
            ),
            BlocListener<ZaloPayGateBloc, ZaloPayGateState>(
              listener: (context, state) async {
                if (amount != null &&
                    paymentMethod != null &&
                    contractId != null) {
                  if (state is ZaloPayPaymentSuccess) {
                    setState(() {
                      topupChannelZalo = paymentMethod.code.name;
                      contractIdZalo = contractId;
                      zptranstokenZalo = state.zptranstoken;
                      transAmount = amount;
                    });
                    context.loaderOverlay.hide();
                    String response = "";
                    try {
                      final String results = await platform.invokeMethod(
                          'payOrder', {"zptoken": state.zptranstoken});
                      response = results;
                      // setState(() {
                      //   payResult = response;
                      // });
                      print(payResult);
                    } on PlatformException {
                      response = "Payment failed";
                      context.loaderOverlay.hide();
                    }
                    if (!Platform.isIOS) {
                      if (response.contains("Payment Success")) {
                        var appTransID =
                            response.replaceAll("Payment Success", "");
                        context.loaderOverlay.hide();
                        context.read<CashInBloc>().add(CashInContractsPayment(
                              topupChannel: paymentMethod.code.name,
                              destContractId: contractId,
                              requestId: appTransID,
                              amount: amount,
                            ));
                        context.read<BalanceBloc>().add(BalanceFetched());
                      } else if (response == "User Canceled") {
                        // nguoi dung khong thanh toan nua
                        context.loaderOverlay.hide();
                        context.replaceRoute(CashInResultRoute(
                          isSuccess: false,
                          message: "Người dùng ngừng thanh toán",
                        ));
                      } else if (response == "Payment failed") {
                        // thanh toan that bai
                        context.loaderOverlay.hide();
                        context.replaceRoute(CashInResultRoute(
                          isSuccess: false,
                          message: "Thanh toán thất bại",
                        ));
                      }
                    }
                  } else if (state is ZaloPayPaymentFailure) {
                    context.loaderOverlay.hide();
                    context.replaceRoute(CashInResultRoute(
                      isSuccess: false,
                      message: 'Có Lỗi Xảy ra',
                    ));
                  }
                }
              },
            ),
            BlocListener<VNPayBloc, VNPayState>(
              listener: (context, state) {
                if (state is VNPayPaymentInProgress) {
                  context.loaderOverlay.show();
                } else if (state is VNPayPaymentSuccess) {
                  context.loaderOverlay.hide();
                  context.read<BalanceBloc>().add(BalanceFetched());
                  context.replaceRoute(CashInResultRoute(isSuccess: true));
                } else if (state is VNPayPaymentFailure) {
                  context.loaderOverlay.hide();
                  context.replaceRoute(CashInResultRoute(
                    isSuccess: false,
                    message: state.message,
                  ));
                }
              },
            ),
            //trungnh
            BlocListener<MBPayGateBloc, MBPayGateState>(
              listener: (context, state) {
                if (amount != null &&
                    paymentMethod != null &&
                    contractId != null) {
                  if (state is MBPayGateInProgress) {
                    context.loaderOverlay.show();
                  } else if (state is MBGetTokenCompletedSuccess) {
                    context.loaderOverlay.hide();
                    _onMBCreateOrder(state, amount);
                  } else if (state is MBCreateOrderCompletedSuccess) {
                    _launchMBB(state);
                  } else if (state is MBCheckSumCompletedSuccess) {
                    context.loaderOverlay.hide();
                    _onMBCheckPayment(state);
                  } else if (state is MBCheckPaymentResultCompletedSuccess) {
                    context.loaderOverlay.hide();
                    if (state.errorCode == '000') {
                      context.loaderOverlay.show();
                      context.read<CashInBloc>().add(CashInContractsPayment(
                            topupChannel: paymentMethod.code.name,
                            destContractId: contractId,
                            requestId: mbStateCS.transactionID,
                            amount: amount,
                          ));
                      context.read<BalanceBloc>().add(BalanceFetched());
                    } else {
                      _onMBError('Payment failed');
                    }
                  } else if (state is MBGetTokenCompletedFailure) {
                    _onMBError(state.message);
                  } else if (state is MBCreateOrderCompletedFailure) {
                    _onMBError(state.message);
                  } else if (state is MBCheckSumCompletedFailure) {
                    _onMBError(state.message);
                  } else if (state is MBCheckPaymentResultCompletedFailure) {
                    _onMBError(state.message);
                  }
                }
              },
            )
          ],
          child: BlocBuilder<CashInBloc, CashInState>(
            builder: (context, state) {
              return LoadingPrimaryButton(
                title: 'Xác nhận',
                enabled: cashInEnabled,
                isLoading: state is CashInFetchedTopupFeeInProgress,
                onTap: cashInEnabled
                    ? () {
                        _onCheckVehicle(
                          context: context,
                          contractId: contractId!,
                        );
                      }
                    : null,
              );
            },
          ),
        );
      },
    );
  }

  Future<void> _onCashInOrderViettelPayGateCreatedSuccess({
    required BuildContext context,
    required CashInOrderViettelPayCreatedSuccess state,
    required TopupChannel paymentMethod,
  }) async {
    final request = ViettelPayGateRequest(
      billCode: state.orderId,
      desc: 'Nạp tiền vào tài khoản ePass',
      orderId: state.orderId,
      paymentMethod: paymentMethod.code.name,
      transAmount: state.amount,
    );
    context.router.replace(
      CashInViettelPayGateRoute(
        title: paymentMethod.name ?? '',
        request: request,
      ),
    );
  }

  // Future<void> _onLinkToBIDVPayGateCreatedSuccess({
  //   required BuildContext context,
  //   required TopupChannel paymentMethod,
  // }) async {
  //   final response = await _crmClient.getUser();

  //   final request = LinkBIDVPayGateRequest(
  //     payerId: response.user?.phone,
  //     payerName: response.user?.userName,
  //     payerMobile: response.user?.phone,
  //     payerIdentity: response.user?.identifier,
  //     payerAdd: response.user?.address,
  //   );
  //   context.router
  //       .replace(CashInBIDVPayGateRoute(title: 'BIDV', request: request));
  // }

  Future<void> _onCheckLinkToBIDVPayGateCreatedSuccess({
    required BuildContext context,
    required TopupChannel paymentMethod,
  }) async {
    final response = context.read<AppBloc>().state.user;

    final request = CheckLinkBIDVPayGateRequest(
      payerId: response?.phone,
      payerName: response?.userName,
      payerMobile: response?.phone,
      payerIdentity: response?.identifier,
    );
    context.router
        .replace(CashInBIDVPayGateRoute(title: 'BIDV', request: request));
  }

  // Future<void> _onTransFromBIDVPayGateCreatedSuccess({
  //   required BuildContext context,
  //   required TopupChannel paymentMethod,
  //   required bidv.CashInOrderBIDVConnnectedSuccess state,
  // }) async {
  //   final contractNo = state.contractNo;
  //   final response = await _crmClient.getUser();
  //   final request = BIDVTopUpPayGateRequest(
  //     payerId: response.user?.phone,
  //     payerName: response.user?.userName,
  //     transAmount: state.amount,
  //     payerMobile: response.user?.phone,
  //     payerIdentity: response.user?.identifier,
  //     payerAdd: response.user?.address,
  //     transDes: 'Nap tien tai khoan Epass $contractNo',
  //   );
  //   context.router
  //       .replace(CashInBIDVPayGateRoute(title: 'BIDV', request: request));
  // }

  Future<void> _onCashInConfirmed({
    required BuildContext context,
    required TopupChannelFee fee,
    required TopupChannel paymentMethod,
    required int amount,
    required String contractId,
  }) async {
    final currentUser = context.read<AppBloc>().state.user;

    // create cash in order
    final currentUserContractNo = currentUser?.contractNo;
    final currentUserContractId = currentUser?.contractId;
    final currentUserCustomerId = currentUser?.customerId;

    if (currentUserContractNo != null &&
        currentUserContractId != null &&
        currentUserCustomerId != null) {
      switch (paymentMethod.code) {
        case TopupChannelCode.BankPlus:
        case TopupChannelCode.DomesticATM:
        case TopupChannelCode.InternationalCard:
        case TopupChannelCode.ViettelPay:
        case TopupChannelCode.MobileMoney:
        case TopupChannelCode.Agribank:
        case TopupChannelCode.TPBank:
        case TopupChannelCode.VPBank:
        case TopupChannelCode.VIB:
        case TopupChannelCode.PVcomBank:
        case TopupChannelCode.MSB:
        case TopupChannelCode.OCBBank:
        case TopupChannelCode.VnptMoney:
        case TopupChannelCode.PayNow:
          context.read<CashInBloc>().add(CashInOrderViettelPayCreated(
                contractId: currentUserContractId,
                contractNo: currentUserContractNo,
                customerId: currentUserCustomerId,
                amount: amount,
                desContractId: contractId,
                paymentMethodName: paymentMethod.code.name,
              ));
          break;
        // case TopupChannelCode.VnptMoney:
        //   context.read<VnptGateBloc>().add(VnptPaymentProceeded(
        //         amount: amount,
        //         client_ip: '',
        //       ));
        //   break;
        case TopupChannelCode.MoMo:
          context.read<CashInBloc>().add(CashInOrderMomoCreated(
                contractId: currentUserContractId,
                contractNo: currentUserContractNo,
                customerId: currentUserCustomerId,
                amount: amount,
                desContractId: contractId,
              ));
          break;
        case TopupChannelCode.VNPAY:
          context.read<CashInBloc>().add(CashInOrderVNPayCreated(
                contractId: currentUserContractId,
                contractNo: currentUserContractNo,
                customerId: currentUserCustomerId,
                amount: amount,
                desContractId: contractId,
              ));
          break;
        case TopupChannelCode.BIDV:
          _onCheckLinkToBIDVPayGateCreatedSuccess(
              context: context, paymentMethod: paymentMethod);
          break;
        case TopupChannelCode.BankTransfer:
        case TopupChannelCode.ZaloPay:
          context.loaderOverlay.show();
          context.read<ZaloPayGateBloc>().add(ZaloPayPaymentProceeded(
                amount: amount,
              ));
          break;
        case TopupChannelCode.MB:
          _onMBGetToken();
          break;
        case TopupChannelCode.unknown:
          break;
      }
    }
  }
}

Future<void> _onCheckVehicle({
  required BuildContext context,
  required String contractId,
}) async {
  final currentUser = context.read<AppBloc>().state.user;

  final currentUserContractId = currentUser?.contractId;

  context
      .read<CashInBloc>()
      .add(CheckVehicleInContract(contractId: currentUserContractId!));
}
