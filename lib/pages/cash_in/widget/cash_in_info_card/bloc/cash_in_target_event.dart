part of 'cash_in_target_bloc.dart';

@immutable
abstract class CashInTargetEvent extends Equatable {
  const CashInTargetEvent();
}

class CashInForChanged extends CashInTargetEvent {
  final CashInFor cashInFor;

  const CashInForChanged(this.cashInFor);

  @override
  List<Object> get props => [cashInFor];
}

class CashInTargetValidated extends CashInTargetEvent {
  final String target;

  const CashInTargetValidated(this.target);

  @override
  List<Object> get props => [target];
}
