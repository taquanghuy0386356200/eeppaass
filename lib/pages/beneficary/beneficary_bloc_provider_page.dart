import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/beneficary/beneficary_list/bloc/beneficary_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BeneficiaryBlocProviderPage extends StatelessWidget {
  const BeneficiaryBlocProviderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BeneficiaryListBloc>(
      create: (context) => BeneficiaryListBloc(
        beneficiaryRepository: getIt<IBeneficiaryRepository>(),
        pageSize: 100,
      ),
      child: const AutoRouter(),
    );
  }
}
