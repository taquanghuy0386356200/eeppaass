import 'package:epass/commons/validators/password_validator.dart';
import 'package:epass/commons/validators/phone_number_validator.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/textfield/password_text_field.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_form_bloc.dart';
import 'package:epass/pages/forgot_password/pages/forgot_password_new_password/bloc/forgot_password_new_password_form_bloc.dart';
import 'package:epass/pages/forgot_password/validators/forgot_password_form_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ForgotPasswordNewPasswordForm extends StatefulWidget {
  const ForgotPasswordNewPasswordForm({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordNewPasswordForm> createState() => _ForgotPasswordNewPasswordFormState();
}

class _ForgotPasswordNewPasswordFormState extends State<ForgotPasswordNewPasswordForm>
    with PasswordValidator, PhoneNumberValidator, ForgotPasswordFormValidator {
  final _formKey = GlobalKey<FormState>();
  final _newPasswordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  @override
  void dispose() {
    _newPasswordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordNewPasswordFormBloc, ForgotPasswordNewPasswordFormState>(
      listener: (context, state) {
        final plateNumber = context.read<ForgotPasswordFormBloc>().state.plateNumber;
        final plateTypeCode = context.read<ForgotPasswordFormBloc>().state.plateTypeCode;
        final phoneNumber = context.read<ForgotPasswordFormBloc>().state.phoneNumber;

        if (plateNumber != null && plateTypeCode != null && phoneNumber != null) {
          context.read<ForgotPasswordBloc>().add(ForgotPasswordSubmitted(
                plateNumber: plateNumber,
                plateTypeCode: plateTypeCode,
                phoneNumber: phoneNumber,
              ));
        }
      },
      child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordState>(
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                PasswordTextField(
                  controller: _newPasswordController,
                  labelText: 'Mật khẩu mới',
                  hintText: 'Nhập mật khẩu mới',
                  obscureText: true,
                  maxLength: 20,
                  enabled: state is! ForgotPasswordSubmittedInProgress,
                  validator: passwordJustValidateLength,
                ),
                SizedBox(height: 20.h),
                PasswordTextField(
                  controller: _confirmPasswordController,
                  labelText: 'Xác nhận mật khẩu',
                  hintText: 'Nhập lại mật khẩu mới',
                  obscureText: true,
                  maxLength: 20,
                  enabled: state is! ForgotPasswordSubmittedInProgress,
                  validator: (confirmPassword) {
                    if (confirmPassword == null || confirmPassword.isEmpty) {
                      return 'Vui lòng nhập Xác nhận mật khẩu';
                    } else if (confirmPassword != _newPasswordController.text) {
                      return 'Xác nhận mật khẩu không khớp với mật khẩu mới';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(height: 40.h),
                SizedBox(
                  height: 56.h,
                  width: double.infinity,
                  child: LoadingPrimaryButton(
                    title: 'Gửi OTP',
                    isLoading: state is ForgotPasswordSubmittedInProgress,
                    onTap: state is! ForgotPasswordSubmittedInProgress
                        ? () {
                            FocusScope.of(context).unfocus();
                            final isValidated = _formKey.currentState!.validate();
                            if (isValidated) {
                              context
                                  .read<ForgotPasswordNewPasswordFormBloc>()
                                  .add(ForgotPasswordNewPasswordFormCompleted(_newPasswordController.text));
                            }
                          }
                        : null,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
