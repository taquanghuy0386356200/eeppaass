
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'add_service_response.g.dart';

@JsonSerializable()
class AddServiceModel {
  int mapId;
  int contractId;
  int serviceId;
  int status;

  AddServiceModel({
    required this.mapId,
    required this.contractId,
    required this.serviceId,
    required this.status
  });

  factory AddServiceModel.fromJson(Map<String, dynamic> json) => _$AddServiceModelFromJson(json);
  Map<String, dynamic> toJson() => _$AddServiceModelToJson(this);

}

@JsonSerializable()
class AddServiceResponse {
  AddServiceResponse({
    required this.data,
    this.mess,
  });

  final List<AddServiceModel> data;
  final MessageData? mess;

  factory AddServiceResponse.fromJson(Map<String, dynamic> json) => _$AddServiceResponseFromJson(json);
}