import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/notification/notification_msg.dart';
import 'package:epass/commons/repo/notification_repository.dart';
import 'package:epass/pages/bloc/bloc_transforms.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:simple_result/simple_result.dart';

part 'notification_event.dart';

part 'notification_state.dart';

class EventNotificationBloc extends NotificationBloc {
  EventNotificationBloc({
    NotificationBloc? notificationBloc,
    required INotificationRepository notificationRepository,
    required FirebaseMessaging firebaseMessaging,
  }) : super(
            notificationBloc: notificationBloc,
            notificationRepository: notificationRepository,
            firebaseMessaging: firebaseMessaging);
}

class BalanceChangeNotificationBloc extends NotificationBloc {
  BalanceChangeNotificationBloc({
    NotificationBloc? notificationBloc,
    required INotificationRepository notificationRepository,
    required FirebaseMessaging firebaseMessaging,
  }) : super(
            notificationBloc: notificationBloc,
            notificationRepository: notificationRepository,
            firebaseMessaging: firebaseMessaging);
}

class NewsNotificationBloc extends NotificationBloc {
  NewsNotificationBloc({
    NotificationBloc? notificationBloc,
    required INotificationRepository notificationRepository,
    required FirebaseMessaging firebaseMessaging,
  }) : super(
            notificationBloc: notificationBloc,
            notificationRepository: notificationRepository,
            firebaseMessaging: firebaseMessaging);
}

class DiscountNotificationBloc extends NotificationBloc {
  DiscountNotificationBloc({
    NotificationBloc? notificationBloc,
    required INotificationRepository notificationRepository,
    required FirebaseMessaging firebaseMessaging,
  }) : super(
            notificationBloc: notificationBloc,
            notificationRepository: notificationRepository,
            firebaseMessaging: firebaseMessaging);
}

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationBloc? _notificationBloc;
  final INotificationRepository _notificationRepository;
  final FirebaseMessaging _firebaseMessaging;

  final int _pageSize;
  int _startRecord = 0;

  NotificationBloc({
    int pageSize = 10,
    NotificationBloc? notificationBloc,
    required INotificationRepository notificationRepository,
    required FirebaseMessaging firebaseMessaging,
  })  : _pageSize = pageSize,
        _notificationBloc = notificationBloc,
        _notificationRepository = notificationRepository,
        _firebaseMessaging = firebaseMessaging,
        super(const NotificationState(listData: [])) {
    on<NotificationFetched>(_onNotificationFetched);
    on<NotificationRefreshed>(_onNotificationRefreshed);
    on<NotificationLoadMore>(_onNotificationLoadMore);
    on<NotificationSearched>(
      _onNotificationSearched,
      transformer: throttleDroppable(const Duration(milliseconds: 500)),
    );
    on<NotificationRead>(_onNotificationRead);
  }

  FutureOr<void> _onNotificationFetched(
    NotificationFetched event,
    Emitter<NotificationState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      notificationType: event.notificationType,
      searchInput: null,
      error: null,
    ));

    final result = await _getEventNotification();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            totalUnread: data.totalUnread,
            isFull: data.listData.length == data.count,
            searchInput: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        searchInput: null,
      )),
    );
  }

  FutureOr<void> _onNotificationRefreshed(
    NotificationRefreshed event,
    Emitter<NotificationState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      searchInput: state.searchInput,
      notificationType: state.notificationType,
      error: null,
    ));

    final result = await _getEventNotification();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            isFull: data.listData.length == data.count,
            listData: data.listData,
            totalUnread: data.totalUnread,
            searchInput: state.searchInput,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        searchInput: state.searchInput,
        error: failure.message,
      )),
    );
  }

  FutureOr<void> _onNotificationLoadMore(
    NotificationLoadMore event,
    Emitter<NotificationState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      searchInput: state.searchInput,
      error: null,
    ));

    final result = await _getEventNotification();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(state.copyWith(
          isLoadingMore: false,
          listData: listData,
          isFull: listData.length == data.count,
          searchInput: state.searchInput,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        searchInput: state.searchInput,
        error: failure.message,
      )),
    );
  }

  FutureOr<void> _onNotificationSearched(
    NotificationSearched event,
    Emitter<NotificationState> emit,
  ) async {
    final searchTerm = event.input?.trim();
    if (searchTerm == null) {
      return;
    }

    emit(state.copyWith(
      isLoading: true,
      searchInput: searchTerm.isEmpty ? null : searchTerm,
      error: null,
    ));

    _startRecord = 0;

    final result = await _getEventNotification();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            searchInput: state.searchInput,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        searchInput: state.searchInput,
        error: failure.message,
      )),
    );
  }

  Future<Result<NotificationMsgDataNotNull, Failure>>
      _getEventNotification() async {
    final fcmToken = await _firebaseMessaging.getToken();

    final result = await _notificationRepository.getNotifications(
      pageSize: _pageSize,
      startRecord: _startRecord,
      firebaseToken: fcmToken,
      searchInput: state.searchInput,
      notificationType: state.notificationType,
    );
    return result;
  }

  FutureOr<void> _onNotificationRead(
    NotificationRead event,
    Emitter<NotificationState> emit,
  ) async {
    final result = await _notificationRepository.updateNotification(
      notificationId: event.notificationId.toString(),
      updateViewStatusRequest: UpdateNotificationViewStatusRequest(),
    );

    result.when(
      success: (success) {
        final listData = state.listData;
        final updatedListData = listData.map((e) {
          if (e.notificationMsgId == event.notificationId) {
            return e.copyWith(viewStatus: ViewStatus.read);
          }
          return e;
        }).toList();

        _notificationBloc?.add(const NotificationRefreshed());

        emit(
          state.copyWith(
            listData: updatedListData,
            searchInput: state.searchInput,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        searchInput: state.searchInput,
        error: failure.message ?? 'Có lỗi xảy ra',
      )),
    );
  }
}
