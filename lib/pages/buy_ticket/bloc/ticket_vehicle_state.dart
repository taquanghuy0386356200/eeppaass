part of 'ticket_vehicle_bloc.dart';

@immutable
class TicketVehicleState extends Equatable {
  final List<Vehicle> userVehicles;
  final List<Vehicle> otherVehicles;

  const TicketVehicleState({
    required this.userVehicles,
    this.otherVehicles = const [],
  });

  @override
  List<Object> get props => [userVehicles, otherVehicles];

  TicketVehicleState copyWith({
    List<Vehicle>? userVehicles,
    List<Vehicle>? otherVehicles,
  }) {
    return TicketVehicleState(
      userVehicles: userVehicles ?? this.userVehicles,
      otherVehicles: otherVehicles ?? this.otherVehicles,
    );
  }
}
