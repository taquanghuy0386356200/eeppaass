import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// actionAuditId : "5751333"
/// actionUserName : "V000008187"
/// actionType : "Thay đổi thông tin hợp đồng"
/// actionDate : "29/12/2021 11:24:26"
/// userName : "BÙI HƯƠNG GIANG"
/// reason : "Khách hàng yêu cầu"

part 'contract_action.g.dart';

@JsonSerializable()
class ContractAction {
  ContractAction({
    this.actionAuditId,
    this.actionUserName,
    this.actionType,
    this.actionDate,
    this.userName,
    this.reason,
  });

  final String? actionAuditId;
  final String? actionUserName;
  final String? actionType;
  final String? actionDate;
  final String? userName;
  final String? reason;

  factory ContractAction.fromJson(Map<String, dynamic> json) =>
      _$ContractActionFromJson(json);

  @override
  String toString() {
    return 'ContractAction:$actionAuditId';
  }
}

@JsonSerializable()
class ContractActionData {
  final List<ContractAction>? listData;
  final int? count;

  ContractActionData({
    this.listData,
    this.count,
  });

  factory ContractActionData.fromJson(Map<String, dynamic> json) =>
      _$ContractActionDataFromJson(json);
}

class ContractActionDataNotNull {
  final List<ContractAction> listData;
  final int count;

  ContractActionDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class ContractActionResponse {
  final ContractActionData? data;
  final MessageData? mess;

  ContractActionResponse({
    this.data,
    this.mess,
  });

  factory ContractActionResponse.fromJson(Map<String, dynamic> json) =>
      _$ContractActionResponseFromJson(json);
}
