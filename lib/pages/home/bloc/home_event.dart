part of 'home_bloc.dart';

@immutable
abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class HomeFetched extends HomeEvent {
  @override
  List<Object?> get props => [];
}

class ContractPaymentFetched extends HomeEvent {
  @override
  List<Object> get props => [];
}
class AppStoreFetched extends HomeEvent{
  @override
  List<Object> get props => [];
}

class WaitingOrderFetched extends HomeEvent {
  @override
  List<Object> get props => [];
}

class SendConfirmOrderRequest extends HomeEvent {
  final List<WaitingOrder> waitingOrders;

  const SendConfirmOrderRequest(this.waitingOrders);

  @override
  List<Object> get props => [waitingOrders];
}
