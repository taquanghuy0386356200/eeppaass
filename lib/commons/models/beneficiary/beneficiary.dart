import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'beneficiary.g.dart';

@JsonEnum(alwaysCreate: true)
enum BeneficiaryStatus {
  @JsonValue('0')
  inactive,
  @JsonValue('1')
  active,
  @JsonValue('unknown')
  unknown,
}

@JsonSerializable()
@immutable
class Beneficiary {
  const Beneficiary({
    this.beneficiaryId,
    this.contractNo,
    this.customerName,
    this.contractId,
    this.reminiscentName,
    this.status,
  });

  final int? beneficiaryId;
  final String? contractNo;
  final String? customerName;
  final String? reminiscentName;
  final int? contractId;

  @JsonKey(unknownEnumValue: BeneficiaryStatus.unknown)
  final BeneficiaryStatus? status;

  factory Beneficiary.fromJson(Map<String, dynamic> json) =>
      _$BeneficiaryFromJson(json);

  Map<String, dynamic> toJson() => _$BeneficiaryToJson(this);

  @override
  String toString() {
    return '$beneficiaryId';
  }
}

@JsonSerializable()
@immutable
class BeneficiaryListData {
  final List<Beneficiary>? listData;
  final int? count;

  const BeneficiaryListData({
    this.listData,
    this.count,
  });

  factory BeneficiaryListData.fromJson(Map<String, dynamic> json) =>
      _$BeneficiaryListDataFromJson(json);
}

@immutable
class BeneficiaryListDataNotNull {
  final List<Beneficiary> listData;
  final int count;

  const BeneficiaryListDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
@immutable
class BeneficiaryListDataResponse {
  final BeneficiaryListData? data;
  final MessageData? mess;

  const BeneficiaryListDataResponse({
    this.data,
    this.mess,
  });

  factory BeneficiaryListDataResponse.fromJson(Map<String, dynamic> json) =>
      _$BeneficiaryListDataResponseFromJson(json);
}
