import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NoDataPage extends StatelessWidget {
  final void Function()? onTap;
  final String? title;
  final String? description;
  final bool isImageError;
  final bool needItalicStyle;

  const NoDataPage({
    Key? key,
    this.onTap,
    this.title,
    this.description,
    this.isImageError = true,
    this.needItalicStyle = false,
  }) : super(key: key);

  //todo
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 32.h),
        child: SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              isImageError
                  ? Padding(
                      padding: EdgeInsets.only(right: 16.w),
                      child: Assets.images.error.noData.image(
                        fit: BoxFit.cover,
                        width: 160.w,
                      ),
                    )
                  : const SizedBox.shrink(),
              SizedBox(height: 32.h),
              Text(
                title ?? 'Không có dữ liệu!',
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 16.h),
              Text(
                description ?? 'Vui lòng kiểm tra lại thông tin tìm kiếm',
                style: Theme.of(context).textTheme.bodyText1?.copyWith(
                      fontStyle: needItalicStyle ? FontStyle.italic : null,
                    ),
                textAlign: TextAlign.center,
              ),
              if (onTap != null) SizedBox(height: 36.h),
              if (onTap != null)
                SizedBox(
                  height: 56.h,
                  child: PrimaryButton(
                    title: 'Chạm để tải lại',
                    onTap: onTap,
                  ),
                ),
              SizedBox(height: 64.h),
            ],
          ),
        ),
      ),
    );
  }
}
