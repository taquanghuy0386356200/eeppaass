package com.example.epass;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u000e\u001a\u00020\u000f2\b\b\u0001\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004H\u0002J\u001a\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0013H\u0002J\"\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001c2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0013H\u0014J\u0012\u0010\u001f\u001a\u00020\u000f2\b\u0010 \u001a\u0004\u0018\u00010!H\u0014J\b\u0010\"\u001a\u00020\u000fH\u0014J\u0010\u0010#\u001a\u00020\u000f2\u0006\u0010$\u001a\u00020%H\u0007J\u0010\u0010&\u001a\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u0013H\u0014J\b\u0010\'\u001a\u00020\u000fH\u0014J\u0016\u0010(\u001a\u00020)2\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0018J\n\u0010*\u001a\u00020\u001c*\u00020)R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"}, d2 = {"Lcom/example/epass/MainActivity;", "Lio/flutter/embedding/android/FlutterFragmentActivity;", "()V", "MOMO_CHANNEL", "", "VNPAY_CHANNEL", "VTMONEY_CHANNEL", "momoChannel", "Lio/flutter/plugin/common/MethodChannel;", "myResult", "Lio/flutter/plugin/common/MethodChannel$Result;", "myResultMBB", "vnpayChannel", "vtmoneyChannel", "configureFlutterEngine", "", "flutterEngine", "Lio/flutter/embedding/engine/FlutterEngine;", "getDeepLinkIntent", "Landroid/content/Intent;", "deepLinkUri", "getResolveInfo", "Landroid/content/pm/ResolveInfo;", "context", "Landroid/content/Context;", "intent", "onActivityResult", "requestCode", "", "resultCode", "data", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onMessageEvent", "response", "Lcom/bplus/vtpay/fragment/etc/model/response/ContractInfoResponse;", "onNewIntent", "onStart", "resolveAndFire", "", "toInt", "app_prodRelease"})
public final class MainActivity extends io.flutter.embedding.android.FlutterFragmentActivity {
    private final java.lang.String MOMO_CHANNEL = "com.viettel.etc.epass/momo_payment";
    private final java.lang.String VNPAY_CHANNEL = "com.viettel.etc.epass/vnpay_payment";
    private final java.lang.String VTMONEY_CHANNEL = "com.viettel.etc.epass/viettel_money";
    private io.flutter.plugin.common.MethodChannel momoChannel;
    private io.flutter.plugin.common.MethodChannel vnpayChannel;
    private io.flutter.plugin.common.MethodChannel vtmoneyChannel;
    private io.flutter.plugin.common.MethodChannel.Result myResult;
    private io.flutter.plugin.common.MethodChannel.Result myResultMBB;
    private java.util.HashMap _$_findViewCache;
    
    public MainActivity() {
        super();
    }
    
    @java.lang.Override()
    public void configureFlutterEngine(@org.jetbrains.annotations.NotNull()
    @androidx.annotation.NonNull()
    io.flutter.embedding.engine.FlutterEngine flutterEngine) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onMessageEvent(@org.jetbrains.annotations.NotNull()
    com.bplus.vtpay.fragment.etc.model.response.ContractInfoResponse response) {
    }
    
    public final int toInt(boolean $this$toInt) {
        return 0;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onNewIntent(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
    }
    
    public final boolean resolveAndFire(@org.jetbrains.annotations.NotNull()
    java.lang.String deepLinkUri, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    private final android.content.pm.ResolveInfo getResolveInfo(android.content.Context context, android.content.Intent intent) {
        return null;
    }
    
    private final android.content.Intent getDeepLinkIntent(java.lang.String deepLinkUri) {
        return null;
    }
}