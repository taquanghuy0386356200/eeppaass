class PlateNumberValidator {
  final plateNumberMaxLength = 10;
  final _pattern = RegExp(r'^(?=.*\d)(?=.*[a-zA-Z])([a-zA-Z\d]+){2,10}$');

  String? plateNumberValidator(String? plateNumber) {
    if (plateNumber == null || plateNumber.isEmpty) {
      return 'Vui lòng nhập Biển số xe';
    } else if (!_pattern.hasMatch(plateNumber)) {
      return 'Biển số xe không hợp lệ';
    }
    return null;
  }
}