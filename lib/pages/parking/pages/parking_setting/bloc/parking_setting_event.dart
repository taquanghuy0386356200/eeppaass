part of 'parking_setting_bloc.dart';

abstract class ParkingSettingEvent extends Equatable {
  const ParkingSettingEvent();
}

class ServiceFetched extends ParkingSettingEvent {
  const ServiceFetched();

  @override
  List<Object?> get props => [];
}

class ServiceRoadSideFetched extends ParkingSettingEvent {
  const ServiceRoadSideFetched();

  @override
  List<Object?> get props => [];
}

class ServiceEnabled extends ParkingSettingEvent {
  final int? serviceId;
  const ServiceEnabled({this.serviceId});

  @override
  List<Object?> get props => [serviceId];
}

class ParkingSettingAlerted extends ParkingSettingEvent {
  final bool? isOn;
  final int? serviceId;
  const ParkingSettingAlerted({this.isOn, this.serviceId});

  @override
  List<Object?> get props => [isOn, serviceId];
}

class ParkingRoadSideSettingAlerted extends ParkingSettingEvent {
  final bool? isOnRoadSide;
  final int? serviceId;
  const ParkingRoadSideSettingAlerted({this.isOnRoadSide, this.serviceId});

  @override
  List<Object?> get props => [isOnRoadSide, serviceId];
}

class VerifyConfigAlerted extends ParkingSettingEvent {
  final bool? isOn;
  final VerifyConfigModel? verifyConfigModel;
  const VerifyConfigAlerted({this.isOn, this.verifyConfigModel});

  @override
  List<Object?> get props => [isOn, verifyConfigModel];
}

class OtpSubmitted extends ParkingSettingEvent {
  final String otp;
  final String passCode;

  const OtpSubmitted({required this.otp, this.passCode = ''});

  @override
  List<Object?> get props => [otp, passCode];
}

class OtpRequested extends ParkingSettingEvent {
  const OtpRequested();

  @override
  List<Object?> get props => [];
}

class PassCodeSubmitted extends ParkingSettingEvent {
  final String passCode;
  final int confirmType;

  const PassCodeSubmitted({required this.passCode, required this.confirmType});

  @override
  List<Object?> get props => [passCode];
}

class PassCodeChanged extends ParkingSettingEvent {
  final VerifyConfigModel? verifyConfigModel;
  const PassCodeChanged({required this.verifyConfigModel});

  @override
  List<Object?> get props => [verifyConfigModel];
}

class DialogCancelled extends ParkingSettingEvent {
  const DialogCancelled();

  @override
  List<Object?> get props => [];
}

class BottomSheetCancelled extends ParkingSettingEvent {
  const BottomSheetCancelled();

  @override
  List<Object?> get props => [];
}

class SnackBarCancelled extends ParkingSettingEvent {
  const SnackBarCancelled();

  @override
  List<Object?> get props => [];
}
