// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'topup_channel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopupChannel _$TopupChannelFromJson(Map<String, dynamic> json) => TopupChannel(
      code: $enumDecode(_$TopupChannelCodeEnumMap, json['code'],
          unknownValue: TopupChannelCode.unknown),
      name: json['name'] as String?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
      status: $enumDecodeNullable(_$TopupChannelStatusEnumMap, json['status'],
          unknownValue: TopupChannelStatus.unknown),
      charge: json['charge'] as int?,
      groupChannel: json['groupChannel'] as int?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$TopupChannelToJson(TopupChannel instance) =>
    <String, dynamic>{
      'code': _$TopupChannelCodeEnumMap[instance.code]!,
      'name': instance.name,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'status': _$TopupChannelStatusEnumMap[instance.status],
      'charge': instance.charge,
      'groupChannel': instance.groupChannel,
      'description': instance.description,
    };

const _$TopupChannelCodeEnumMap = {
  TopupChannelCode.BankPlus: 'BankPlus',
  TopupChannelCode.DomesticATM: 'DomesticATM',
  TopupChannelCode.InternationalCard: 'InternationalCard',
  TopupChannelCode.ViettelPay: 'ViettelPay',
  TopupChannelCode.MoMo: 'MoMo',
  TopupChannelCode.VNPAY: 'VNPAY',
  TopupChannelCode.BankTransfer: 'BankTransfer',
  TopupChannelCode.PayNow: 'PayNow',
  TopupChannelCode.MobileMoney: 'MobileMoney',
  TopupChannelCode.MB: 'MB',
  TopupChannelCode.BIDV: 'BIDV',
  TopupChannelCode.VPBank: 'VPBank',
  TopupChannelCode.Agribank: 'Agribank',
  TopupChannelCode.TPBank: 'TPBank',
  TopupChannelCode.VIB: 'VIB',
  TopupChannelCode.OCBBank: 'OCBBank',
  TopupChannelCode.PVcomBank: 'PVcomBank',
  TopupChannelCode.MSB: 'MSB',
  TopupChannelCode.ZaloPay: 'ZaloPay',
  TopupChannelCode.VnptMoney: 'VnptMoney',
  TopupChannelCode.unknown: 'unknown',
};

const _$TopupChannelStatusEnumMap = {
  TopupChannelStatus.inactive: '0',
  TopupChannelStatus.active: '1',
  TopupChannelStatus.unknown: 'unknown',
};

TopupChannelData _$TopupChannelDataFromJson(Map<String, dynamic> json) =>
    TopupChannelData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => TopupChannel.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$TopupChannelDataToJson(TopupChannelData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

TopupChannelResponse _$TopupChannelResponseFromJson(
        Map<String, dynamic> json) =>
    TopupChannelResponse(
      data: json['data'] == null
          ? null
          : TopupChannelData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TopupChannelResponseToJson(
        TopupChannelResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
