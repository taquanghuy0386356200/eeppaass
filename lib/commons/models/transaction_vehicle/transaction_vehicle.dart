import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// id : 290545043
/// ticketId : "5175451"
/// epc : "3416214B9400F274G0000474"
/// price : 40000
/// adjustPaid : 0
/// ticketType : 1
/// ticketTypeName : "Vé lượt thường"
/// isTicketException : "N"
/// exceptionName : "HienTest"
/// plateNumber : "03A45121"
/// vehicleType : 2
/// vehicleTypeName : "Loại 2"
/// sourceTransaction : "ETC"
/// vehiclePriorityType : 0
/// stationInId : 5033
/// stationInName : "Trạm Nam Cầu Giẽ"
/// stationOutId : 5033
/// stationOutName : "Trạm Nam Cầu Giẽ"
/// laneInId : 1
/// laneInName : "Làn 1"
/// timestampIn : "22/10/2021 07:01:31"
/// timestampOut : "22/10/2021 07:03:31"
/// stationType : 1
/// stationTypeName : "Mở"
/// eventTimestamp : "22/10/2021 07:02:31"
/// createDate : "22/10/2021 08:40:26"
/// roamingType : 0
/// roamingTypeStr : "Giao dịch không liên thông"
/// tranStatus : "Giao dịch không có bất thường"

part 'transaction_vehicle.g.dart';

@JsonSerializable()
class TransactionVehicle {
  final int? id;
  final String? ticketId;
  final String? epc;
  final int? price;
  final int? adjustPaid;
  final int? ticketType;
  final String? ticketTypeName;
  final String? isTicketException;
  final String? exceptionName;
  final String? plateNumber;
  final int? vehicleType;
  final String? vehicleTypeName;
  final String? sourceTransaction;
  final int? vehiclePriorityType;
  final int? stationInId;
  final String? stationInName;
  final int? stationOutId;
  final String? stationOutName;
  final int? laneInId;
  final String? laneInName;
  final String? timestampIn;
  final String? timestampOut;
  final int? stationType;
  final String? stationTypeName;
  final String? eventTimestamp;
  final String? createDate;
  final int? roamingType;
  final String? roamingTypeStr;
  final String? tranStatus;

  TransactionVehicle({
    this.id,
    this.ticketId,
    this.epc,
    this.price,
    this.adjustPaid,
    this.ticketType,
    this.ticketTypeName,
    this.isTicketException,
    this.exceptionName,
    this.plateNumber,
    this.vehicleType,
    this.vehicleTypeName,
    this.sourceTransaction,
    this.vehiclePriorityType,
    this.stationInId,
    this.stationInName,
    this.stationOutId,
    this.stationOutName,
    this.laneInId,
    this.laneInName,
    this.timestampIn,
    this.timestampOut,
    this.stationType,
    this.stationTypeName,
    this.eventTimestamp,
    this.createDate,
    this.roamingType,
    this.roamingTypeStr,
    this.tranStatus,
  });

  factory TransactionVehicle.fromJson(Map<String, dynamic> json) =>
      _$TransactionVehicleFromJson(json);

  @override
  String toString() {
    return 'TransactionVehicle: $ticketId';
  }
}

@JsonSerializable()
class TransactionVehicleData {
  final List<TransactionVehicle>? listData;
  final int? count;

  TransactionVehicleData({this.listData, this.count});

  factory TransactionVehicleData.fromJson(Map<String, dynamic> json) =>
      _$TransactionVehicleDataFromJson(json);
}

class TransactionVehicleDataNotNull {
  final List<TransactionVehicle> listData;
  final int count;

  TransactionVehicleDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class TransactionVehicleResponse {
  final TransactionVehicleData? data;
  final MessageData? mess;

  TransactionVehicleResponse({this.data, this.mess});

  factory TransactionVehicleResponse.fromJson(Map<String, dynamic> json) =>
      _$TransactionVehicleResponseFromJson(json);
}
