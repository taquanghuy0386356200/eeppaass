// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ticket_price_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TicketPrice _$TicketPriceFromJson(Map<String, dynamic> json) => TicketPrice(
      vehicleGroupId: json['vehicleGroupId'] as int?,
      stationType: json['stationType'] as int?,
      stationId: json['stationId'] as int?,
      chargeMethodId: $enumDecodeNullable(
          _$MethodChargeIntEnumMap, json['chargeMethodId'],
          unknownValue: MethodChargeInt.unknown),
      fee: json['fee'] as int?,
      status: json['status'] as int?,
      effDate: json['effDate'] as String?,
      plateNumber: json['plateNumber'] as String?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      netWeight: (json['netWeight'] as num?)?.toDouble(),
      cargoWeight: (json['cargoWeight'] as num?)?.toDouble(),
      seatNumber: json['seatNumber'] as int?,
      epc: json['epc'] as String?,
      booCode: $enumDecodeNullable(_$BooCodeEnumMap, json['booCode'],
          unknownValue: BooCode.unknown),
      expDate: json['expDate'] as String?,
      vehicleId: json['vehicleId'] as int?,
      dateOfUse: json['dateOfUse'] as String?,
      message: json['message'] as String?,
    );

Map<String, dynamic> _$TicketPriceToJson(TicketPrice instance) =>
    <String, dynamic>{
      'vehicleGroupId': instance.vehicleGroupId,
      'stationType': instance.stationType,
      'stationId': instance.stationId,
      'chargeMethodId': _$MethodChargeIntEnumMap[instance.chargeMethodId],
      'fee': instance.fee,
      'status': instance.status,
      'effDate': instance.effDate,
      'plateNumber': instance.plateNumber,
      'vehicleTypeId': instance.vehicleTypeId,
      'netWeight': instance.netWeight,
      'cargoWeight': instance.cargoWeight,
      'seatNumber': instance.seatNumber,
      'epc': instance.epc,
      'booCode': _$BooCodeEnumMap[instance.booCode],
      'expDate': instance.expDate,
      'vehicleId': instance.vehicleId,
      'dateOfUse': instance.dateOfUse,
      'message': instance.message,
    };

const _$MethodChargeIntEnumMap = {
  MethodChargeInt.normal: 1,
  MethodChargeInt.block: 2,
  MethodChargeInt.unknown: 'unknown',
};

const _$BooCodeEnumMap = {
  BooCode.boo1: 'BOO1',
  BooCode.boo2: 'BOO2',
  BooCode.unknown: 'unknown',
};

TicketPriceData _$TicketPriceDataFromJson(Map<String, dynamic> json) =>
    TicketPriceData(
      listServicePlan: (json['listServicePlan'] as List<dynamic>?)
          ?.map((e) => TicketPrice.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TicketPriceDataToJson(TicketPriceData instance) =>
    <String, dynamic>{
      'listServicePlan': instance.listServicePlan,
    };

TicketPriceResponse _$TicketPriceResponseFromJson(Map<String, dynamic> json) =>
    TicketPriceResponse(
      data: json['data'] == null
          ? null
          : TicketPriceData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TicketPriceResponseToJson(
        TicketPriceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
