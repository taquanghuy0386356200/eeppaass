import 'package:intl/intl.dart';

extension DateTimeExt on DateTime {
  String toFormattedString({required String format}) {
    return DateFormat(format).format(this);
  }
}
