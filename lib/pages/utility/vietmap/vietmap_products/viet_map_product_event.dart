part of 'viet_map_product_bloc.dart';

@immutable
abstract class VietMapProductEvent extends Equatable {
  const VietMapProductEvent();
}

class VietMapProductFetched extends VietMapProductEvent {
  const VietMapProductFetched();

  @override
  List<Object?> get props => [];
}

class CreateVietMapTransaction extends VietMapProductEvent {

  final String transactionId;
  final String expiredDate;
  final int price;

  const CreateVietMapTransaction({
    required this.transactionId,
    required this.expiredDate,
    required this.price});

  @override
  List<Object?> get props => [transactionId, expiredDate, price];
}

class UpdateVietMapTransaction extends VietMapProductEvent {

  final String transactionId;

  const UpdateVietMapTransaction({
    required this.transactionId});

  @override
  List<Object?> get props => [transactionId];
}
