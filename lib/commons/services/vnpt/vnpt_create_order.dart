import 'package:crypto/crypto.dart';
import 'package:epass/commons/services/vnpt/vnpay_payment_request.dart';
import 'package:epass/commons/services/vnpt/vnpt_payment_response.dart';
import 'package:http/http.dart' as http;
import 'package:jiffy/jiffy.dart';
import 'dart:async';
import 'dart:convert';

import 'package:sprintf/sprintf.dart';

import '../../models/zalo_pay/create_order_response.dart';
import '../../utils/zalo_pay/zalo_pay_utils.dart';

class VnptConfig {
  static const int MERCHANT_SERVICE_ID = 6578;
  static const String BASE_URI =
      "https://sandboxpaydev.vnptmedia.vn/rest/payment/v1.0.5";
  //"http://sandbox.vnptpay.vn/rest/payment/v1.0.5";
  static const String API_KEY = "63c8e8911bc833db9fcfda501bd76eac";
  static const String SECRET_KEY = "36e3c5b5a6d53fce2a2cc23d46ee5987";
}

class VnptCreateOrderService {
  // Future<VnptPaymentResponse?> createPayVNPT(int price, String ip) async {
  Future<VnptPaymentResponse?> createPayVNPT(VNPTRequest request) async {
    var header = <String, String>{};
    header["Content-Type"] = "application/json";
    header["Authorization"] = 'Bearer ${VnptConfig.API_KEY}';

    // VNPTRequest request = VNPTRequest(amount: price, client_ip: "");

    var entity = {
      "ACTION": request.action,
      "VERSION": request.version,
      "MERCHANT_SERVICE_ID": request.merchant_service_id,
      "MERCHANT_ORDER_ID": request.merchant_order_id,
      "AMOUNT": request.amount,
      "AMOUNT_DETAIL": request.amount,
      "SERVICE_ID": request.service_id,
      "SERVICE_CATEGORY": request.service_category,
      "LOCALE": request.locale,
      "CURRENCY_CODE": request.currency_code,
      "DESCRIPTION": request.description,
      "PAYMENT_METHOD": request.payment_methods,
      "CREATE_DATE": request.ceateDate,
      "CLIENT_IP": request.client_ip,
      "DEVICE": request.device,
      "SECURE_CODE": request.secureCode,
    };
    var body = json.encode(entity);

    // Future<String> getVnptGateUrl() async {
    //   http.Response response = await http.post(
    //   Uri.parse('${VnptConfig.BASE_URI}/init'),
    //   headers: header,
    //   body: body,
    // );

    // var data = jsonDecode(response.body);
    // return data;
    // }

    http.Response response = await http.post(
      Uri.parse('${VnptConfig.BASE_URI}/init'),
      headers: header,
      body: body,
    );

    var data = jsonDecode(response.body);
    print("data_response: $data}");

    return VnptPaymentResponse.fromMap(data);
  }
}
