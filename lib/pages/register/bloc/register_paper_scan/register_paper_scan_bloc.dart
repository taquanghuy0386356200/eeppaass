import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/ocr_id_card/id_card_ocr.dart';
import 'package:epass/commons/repo/register_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'register_paper_scan_event.dart';

part 'register_paper_scan_state.dart';

class RegisterPaperScanBloc extends Bloc<RegisterPaperScanEvent, RegisterPaperScanState> {
  final IRegisterRepository _registerRepository;

  RegisterPaperScanBloc({
    required IRegisterRepository registerRepository,
  })  : _registerRepository = registerRepository,
        super(const RegisterPaperScanInitial()) {
    on<RegisterPaperScanned>(_onRegisterPaperScanned);
    on<RegisterPaperScannedReset>((_, emit) => emit(const RegisterPaperScanInitial()));
  }

  FutureOr<void> _onRegisterPaperScanned(
    RegisterPaperScanned event,
    Emitter<RegisterPaperScanState> emit,
  ) async {
    emit(const RegisterPaperScannedInProgress());

    final file = File(event.filePath);
    final bytes = await file.readAsBytes();
    final base64 = base64Encode(bytes);

    final result = await _registerRepository.scanIdCard(imageBase64: base64);

    result.when(
      success: (idCard) {
        if (idCard.idType != event.side) {
          emit(const RegisterPaperScannedFailure('Ảnh chụp không hợp lệ'));
        }

        emit(RegisterPaperScannedSuccess(
          idCard: idCard,
          front: event.side == IdType.front ? event.filePath : null,
          back: event.side == IdType.back ? event.filePath : null,
        ));
      },
      failure: (failure) => emit(RegisterPaperScannedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
