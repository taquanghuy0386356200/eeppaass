import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'utility_event.dart';
part 'utility_state.dart';

class UtilityBloc extends Bloc<UtilityEvent, UtilityState> {
  final IUserRepository _userRepository;

  UtilityBloc({
    required IUserRepository userRepository,
  })  : _userRepository = userRepository,
        super(const UtilityState()) {
    on<UnavailableServiceFetched>(_onUnavailableServiceFetched);
  }

  FutureOr<void> _onUnavailableServiceFetched(
      UnavailableServiceFetched event, Emitter<UtilityState> emit) async {
    final result = await _userRepository.getUnavailableService();
    result.when(
      success: (response) {
        if (response != null) {
          emit(GetServiceSuccess(response));
        } else {
          emit(const GetServiceFail('Có lỗi xảy ra'));
        }
      },
      failure: (failure) => emit(GetServiceFail(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

}
