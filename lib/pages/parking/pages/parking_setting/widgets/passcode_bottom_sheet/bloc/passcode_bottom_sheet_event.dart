part of 'passcode_bottom_sheet_bloc.dart';

abstract class PasscodeBottomSheetEvent extends Equatable {
  const PasscodeBottomSheetEvent();
}

class ChangePasscodeSubmitted extends PasscodeBottomSheetEvent {
  final String newPassword;

  const ChangePasscodeSubmitted({
    required this.newPassword,
  });

  @override
  List<Object> get props => [newPassword];
}

class FormValidated extends PasscodeBottomSheetEvent {
  @override
  List<Object?> get props => [];
}