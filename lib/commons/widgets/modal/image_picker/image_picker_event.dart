part of 'image_picker_bloc.dart';

abstract class ImagePickerEvent extends Equatable {
  const ImagePickerEvent();
}

class ImagePicked extends ImagePickerEvent {
  final ImageSource source;
  final List<String> allowedExtension;

  const ImagePicked({
    required this.source,
    this.allowedExtension = const ['png', 'jpg', 'jpeg', 'heic'],
  });

  @override
  List<Object> get props => [source];
}
