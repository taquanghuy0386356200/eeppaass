part of 'profile_form_bloc.dart';

abstract class ProfileFormEvent extends Equatable {
  const ProfileFormEvent();
}

class ProfileFormSubmitted extends ProfileFormEvent {
  final String? phone;
  final String? email;

  const ProfileFormSubmitted({
    this.phone,
    this.email,
  });

  @override
  List<Object?> get props => [phone, email];
}
