import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:json_annotation/json_annotation.dart';

/// topupChannelCode : "DomesticATM"
/// topupChannelName : "Thẻ ATM nội địa"
/// valueMin : 1
/// serviceFeeType : 1
/// serviceFeeValAdd : 880
/// serviceFeeVal : 0.0066

part 'topup_fee.g.dart';

@JsonSerializable()
class TopupFee {
  TopupFee({
    required this.topupChannelCode,
    this.topupChannelName,
    this.valueMin,
    this.serviceFeeType,
    this.serviceFeeValAdd,
    this.serviceFeeVal,
  });

  @JsonKey(unknownEnumValue: TopupChannelCode.unknown)
  final TopupChannelCode topupChannelCode;
  final String? topupChannelName;
  final int? valueMin;
  final int? serviceFeeType;
  final int? serviceFeeValAdd;
  final double? serviceFeeVal;

  factory TopupFee.fromJson(Map<String, dynamic> json) =>
      _$TopupFeeFromJson(json);
}

@JsonSerializable()
class TopupFeeData {
  final List<TopupFee>? listData;
  final int? count;

  TopupFeeData({this.listData, this.count});

  factory TopupFeeData.fromJson(Map<String, dynamic> json) =>
      _$TopupFeeDataFromJson(json);
}

@JsonSerializable()
class TopupFeesResponse {
  final TopupFeeData? data;
  final MessageData? mess;

  TopupFeesResponse({this.data, this.mess});

  factory TopupFeesResponse.fromJson(Map<String, dynamic> json) =>
      _$TopupFeesResponseFromJson(json);
}
