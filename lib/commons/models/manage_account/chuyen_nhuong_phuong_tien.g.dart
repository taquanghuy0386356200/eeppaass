// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chuyen_nhuong_phuong_tien.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChuyenNhuongPhuongTienTotalResponse
    _$ChuyenNhuongPhuongTienTotalResponseFromJson(Map<String, dynamic> json) =>
        ChuyenNhuongPhuongTienTotalResponse(
          data: json['data'] == null
              ? null
              : ChuyenNhuongPhuongTienResponse.fromJson(
                  json['data'] as Map<String, dynamic>),
          mess: json['mess'] == null
              ? null
              : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
        );

Map<String, dynamic> _$ChuyenNhuongPhuongTienTotalResponseToJson(
        ChuyenNhuongPhuongTienTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ChuyenNhuongPhuongTienResponse _$ChuyenNhuongPhuongTienResponseFromJson(
        Map<String, dynamic> json) =>
    ChuyenNhuongPhuongTienResponse(
      ticketId: json['ticketId'] as int?,
      phoneContact: json['phoneContact'] as String?,
      contractDetail: json['contractDetail'] == null
          ? null
          : ContractDetail.fromJson(
              json['contractDetail'] as Map<String, dynamic>),
      ticketProfiles: (json['ticketProfiles'] as List<dynamic>?)
          ?.map((e) => TicketProfiles.fromJson(e as Map<String, dynamic>))
          .toList(),
      vehiclesTransfer: (json['vehiclesTransfer'] as List<dynamic>?)
          ?.map((e) => VehiclesTransfer.fromJson(e as Map<String, dynamic>))
          .toList(),
      transferMoney: json['transferMoney'] as bool?,
    );

Map<String, dynamic> _$ChuyenNhuongPhuongTienResponseToJson(
        ChuyenNhuongPhuongTienResponse instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'phoneContact': instance.phoneContact,
      'contractDetail': instance.contractDetail,
      'ticketProfiles': instance.ticketProfiles,
      'vehiclesTransfer': instance.vehiclesTransfer,
      'transferMoney': instance.transferMoney,
    };

ContractDetail _$ContractDetailFromJson(Map<String, dynamic> json) =>
    ContractDetail(
      contractNo: json['contractNo'] as String?,
      signName: json['signName'] as String?,
      contractId: json['contractId'] as String?,
    );

Map<String, dynamic> _$ContractDetailToJson(ContractDetail instance) =>
    <String, dynamic>{
      'contractNo': instance.contractNo,
      'signName': instance.signName,
      'contractId': instance.contractId,
    };

TicketProfiles _$TicketProfilesFromJson(Map<String, dynamic> json) =>
    TicketProfiles(
      id: json['id'] as int?,
      ticketId: json['ticketId'] as int?,
      ticketPayloadId: json['ticketPayloadId'] as int?,
      profileType: json['profileType'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      filePath: json['filePath'] as String?,
    );

Map<String, dynamic> _$TicketProfilesToJson(TicketProfiles instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ticketId': instance.ticketId,
      'ticketPayloadId': instance.ticketPayloadId,
      'profileType': instance.profileType,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'filePath': instance.filePath,
    };

VehiclesTransfer _$VehiclesTransferFromJson(Map<String, dynamic> json) =>
    VehiclesTransfer(
      plateNumber: json['plateNumber'] as String?,
      vehicleId: json['vehicleId'] as int?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      owner: json['owner'] as String?,
      isTransfer: json['isTransfer'] as bool? ?? false,
      rfidSerial: json['rfidSerial'] as String?,
      vehicleGroupName: json['vehicleGroupName'] as String?,
      vehicleGroupId: json['vehicleGroupId'] as int?,
      activeStatus: json['activeStatus'] as String?,
    );

Map<String, dynamic> _$VehiclesTransferToJson(VehiclesTransfer instance) =>
    <String, dynamic>{
      'plateNumber': instance.plateNumber,
      'vehicleId': instance.vehicleId,
      'vehicleTypeId': instance.vehicleTypeId,
      'owner': instance.owner,
      'isTransfer': instance.isTransfer,
      'rfidSerial': instance.rfidSerial,
      'vehicleGroupName': instance.vehicleGroupName,
      'activeStatus': instance.activeStatus,
      'vehicleGroupId': instance.vehicleGroupId,
    };
