/// accountNumber : ""
/// token : ""
/// contractId : ""

class ViettelMoneyUnlinkRequest {
  ViettelMoneyUnlinkRequest({
    required this.accountNumber,
    required this.token,
    required this.contractId,
  });

  final String accountNumber;
  final String token;
  final String contractId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['accountNumber'] = accountNumber;
    map['token'] = token;
    map['contractId'] = contractId;
    return map;
  }
}
