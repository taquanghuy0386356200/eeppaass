part of 'transaction_ticket_purchase_bloc.dart';

@immutable
abstract class TransactionTicketPurchaseEvent extends Equatable {
  const TransactionTicketPurchaseEvent();
}

class TransactionTicketPurchaseFetched extends TransactionTicketPurchaseEvent {
  const TransactionTicketPurchaseFetched();

  @override
  List<Object?> get props => [];
}

class TransactionTicketPurchaseRefreshed
    extends TransactionTicketPurchaseEvent {
  const TransactionTicketPurchaseRefreshed();

  @override
  List<Object?> get props => [];
}

class TransactionTicketPurchaseLoadMore extends TransactionTicketPurchaseEvent {
  const TransactionTicketPurchaseLoadMore();

  @override
  List<Object?> get props => [];
}

class TransactionTicketPurchaseSearched extends TransactionTicketPurchaseEvent {
  final String? plateNumber;
  final int efficiencyId;
  final DateTime? saleTransDateFrom;
  final DateTime? saleTransDateTo;

  const TransactionTicketPurchaseSearched({
    this.plateNumber,
    required this.efficiencyId,
    this.saleTransDateFrom,
    this.saleTransDateTo,
  });

  @override
  List<Object?> get props => [
        plateNumber,
        efficiencyId,
        saleTransDateFrom,
        saleTransDateTo,
      ];
}

class TransactionTicketPurchaseDateRangeChanged
    extends TransactionTicketPurchaseEvent {
  final DateTimeRange saleTransDate;

  const TransactionTicketPurchaseDateRangeChanged(this.saleTransDate);

  @override
  List<Object> get props => [saleTransDate];
}

class TransactionTicketPurchasePlateNumberChanged
    extends TransactionTicketPurchaseEvent {
  final String plateNumber;

  const TransactionTicketPurchasePlateNumberChanged(this.plateNumber);

  @override
  List<Object> get props => [plateNumber];
}
