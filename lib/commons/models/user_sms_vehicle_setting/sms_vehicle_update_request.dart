/// status : 1
/// autoRenew : "1"

class SmsVehicleUpdateRequest {
  SmsVehicleUpdateRequest({
    required this.status,
    required this.autoRenew,
  });

  final int status;
  final String autoRenew;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['autoRenew'] = autoRenew;
    return map;
  }
}
