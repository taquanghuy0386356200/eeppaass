import 'dart:typed_data';

import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:epass/commons/services/local_file_service/local_file_service.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/floating_primary_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/full_screen_bottom_sheet.dart';
import 'package:epass/commons/widgets/modal/pdf_view_modal.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/invoice/bloc/invoice_bloc.dart';
import 'package:epass/pages/invoice/bloc/invoice_details_bloc.dart';
import 'package:epass/pages/invoice/widget/invoice_card.dart';
import 'package:epass/pages/invoice/widget/invoice_instruction.dart';
import 'package:epass/pages/invoice/widget/invoice_search_form.dart';
import 'package:epass/pages/invoice/widget/invoice_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:share_plus/share_plus.dart';

class InvoicePage extends StatefulWidget {
  const InvoicePage({Key? key}) : super(key: key);

  @override
  State<InvoicePage> createState() => _InvoicePageState();
}

class _InvoicePageState extends State<InvoicePage> {
  bool _firstRun = true;

  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BasePage(
      title: 'Hoá đơn',
      child: Stack(
        children: [
          FadeAnimation(
            delay: 0.5,
            playAnimation: _firstRun,
            child: const GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              playAnimation: _firstRun,
              direction: FadeDirection.up,
              child: RoundedTopContainer(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => InvoiceBloc(
                        invoiceRepository: getIt<IInvoiceRepository>(),
                        appBloc: getIt<AppBloc>(),
                      )..add(const InvoiceFetched()),
                    ),
                    BlocProvider(
                      create: (context) => InvoiceDetailsBloc(
                        invoiceRepository: getIt<IInvoiceRepository>(),
                        localFileService: getIt<ILocalFileService>(),
                      ),
                    ),
                  ],
                  child: BlocListener<InvoiceDetailsBloc, InvoiceDetailsState>(
                    listener: (context, state) {
                      if (state is InvoiceDownloadedLoading) {
                        context.loaderOverlay.show();
                      } else if (state is InvoiceDiscardLoading) {
                        context.loaderOverlay.hide();
                        showErrorSnackBBar(
                            context: context,
                            message:
                                "Không có dữ liệu thỏa mãn, vui lòng liên hệ 19009080.",
                            duration: const Duration(seconds: 3));
                      } else if (state is InvoiceViewedSuccess) {
                        context.loaderOverlay.hide();
                        showFullScreenBottomSheet(
                          context: context,
                          builder: (context) {
                            return PDFViewModal(
                              title: 'Chi tiết hoá đơn',
                              child: PDFView(
                                pdfData: Uint8List.fromList(state.invoiceData),
                                fitEachPage: true,
                              ),
                            );
                          },
                        );
                      } else if (state is InvoiceDownloadedSuccess) {
                        context.loaderOverlay.hide();
                        showSuccessSnackBBar(
                            context: context,
                            actionLabel: 'Lưu và chia sẻ',
                            message:
                                'Hoá đơn tải về thành công tại đường dẫn: ${state.file.path}',
                            onActionTap: (() => {
                                  Share.shareXFiles([XFile(state.file.path)])
                                }));
                      } else if (state is InvoiceDownloadedFailure) {
                        context.loaderOverlay.hide();
                        showErrorSnackBBar(
                          context: context,
                          message: state.message,
                        );
                      }
                    },
                    child: BlocConsumer<InvoiceBloc, InvoiceState>(
                      listener: (context, state) {
                        final error = state.error;

                        if (error == null) {
                          _refreshController.refreshCompleted();
                          _refreshController.loadComplete();
                        } else {
                          _refreshController.refreshFailed();
                          _refreshController.loadFailed();
                        }
                      },
                      builder: (context, state) {
                        final error = state.error;

                        if (state.isLoading ||
                            state.isRefreshing && state.listData.isEmpty) {
                          return const InvoiceShimmerLoading();
                        } else if (error != null && state.listData.isEmpty) {
                          return Center(
                            child: Text(error),
                          );
                        }
                        final listData = state.listData;
                        return Scaffold(
                          backgroundColor: Colors.transparent,
                          floatingActionButton: GestureDetector(
                            onTap: () => _onActionButtonTapped(
                              context,
                              plateNumber: state.plateNumber,
                              invoiceNo: state.invoiceNo,
                              adjustmentType: state.adjustmentType,
                              transactionType: state.transactionType,
                              stationInId: state.stationInId,
                              stationOutId: state.stationOutId,
                              from: state.from,
                              to: state.to,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "Tìm kiếm",
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                      color: Colors.orange,
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  width: 6.w,
                                ),
                                const FloatingPrimaryButton(),
                              ],
                            ),
                          ),
                          body: state.listData.isEmpty
                              ? NoDataPage(
                                  onTap: () => context
                                      .read<InvoiceBloc>()
                                      .add(const InvoiceRefreshed()),
                                )
                              : SmartRefresher(
                                  physics: const BouncingScrollPhysics(),
                                  enablePullDown: true,
                                  enablePullUp: !state.isFull,
                                  onRefresh: () => context
                                      .read<InvoiceBloc>()
                                      .add(const InvoiceRefreshed()),
                                  onLoading: () => context
                                      .read<InvoiceBloc>()
                                      .add(const InvoiceLoadMore()),
                                  controller: _refreshController,
                                  child: ListView.separated(
                                    physics: const BouncingScrollPhysics(),
                                    itemCount: listData.isEmpty
                                        ? 1
                                        : listData.length + 1,
                                    itemBuilder: (context, index) {
                                      if (index == 0) {
                                        return Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              8.w, 24.h, 8.w, 0.h),
                                          child: const InvoiceInstruction(),
                                        );
                                      }

                                      final item = listData[index - 1];
                                      return Padding(
                                        padding: EdgeInsets.fromLTRB(
                                          16.w,
                                          0.h,
                                          16.w,
                                          index == listData.length ? 80.h : 0.h,
                                        ),
                                        child: InvoiceCard(item: item),
                                      );
                                    },
                                    separatorBuilder: (context, index) =>
                                        SizedBox(
                                            height: index == 0 ? 12.h : 24.h),
                                  ),
                                ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onActionButtonTapped(
    BuildContext context, {
    String? plateNumber,
    String? invoiceNo,
    int? adjustmentType,
    int? transactionType,
    int? stationInId,
    int? stationOutId,
    DateTime? from,
    DateTime? to,
  }) async {
    await showBarModalBottomSheet(
      context: context,
      expand: false,
      useRootNavigator: true,
      builder: (modalContext) => BlocProvider.value(
        value: context.read<InvoiceBloc>(),
        child: InvoiceSearchForm(
          plateNumber: plateNumber,
          invoiceNo: invoiceNo,
          from: from,
          to: to,
        ),
      ),
    );
  }
}
