part of 'viet_map_product_bloc.dart';

@immutable
class VietMapProductState extends Equatable {
  final List<VietMapProduct> listData;
  final String? error;
  final bool isLoading;
  final bool? isPaymentSuccess;

  const VietMapProductState({
    required this.listData,
    this.error,
    this.isLoading = false,
    this.isPaymentSuccess = false
  });

  @override
  List<Object?> get props => [
    listData,
    error,
    isLoading,
    isPaymentSuccess
  ];

  VietMapProductState copyWith({
    List<VietMapProduct>? listData,
    required String? error,
    bool? isLoading,
    bool? isPaymentSuccess
  }) {
    return VietMapProductState(
      listData: listData ?? this.listData,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isPaymentSuccess: isPaymentSuccess
    );
  }
}
