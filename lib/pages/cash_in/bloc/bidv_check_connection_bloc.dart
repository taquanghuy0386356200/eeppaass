import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/services/bidv/bidv_check_connection.dart';
import 'package:epass/commons/services/bidv/bidv_check_connection_request.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment_request.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:equatable/equatable.dart';

part 'bidv_check_connection_event.dart';

part 'bidv_check_connection_state.dart';

class BidvCheckingConnectionBloc
    extends Bloc<BidvCheckingConnectionEvent, BidvCheckingConnectionState> {
  final BidvCheckingConnectionService _bidvCheckingConnectionService;

  BidvCheckingConnectionBloc(
      {required BidvCheckingConnectionService bidvCheckingConnectionService})
      : _bidvCheckingConnectionService = bidvCheckingConnectionService,
        super(BidvCheckingConnectionInitial()) {
    on<BidvCheckingConnectionProceeded>(_checkingConnectionBidv);
  }

  FutureOr<void> _checkingConnectionBidv(
    BidvCheckingConnectionProceeded event,
    Emitter<BidvCheckingConnectionState> emit,
  ) async {
    final bidvCheckingConnectionRequest = BidvCheckingConnectionRequest(
      payerId: event.payerId,
      payerName: event.payerName,
      payerMobile: event.payerMobile,
      payerIdentity: event.payerIdentity,
    );

    final result = await _bidvCheckingConnectionService
        .checkingConnectionBidv(bidvCheckingConnectionRequest);

    result.when(
      success: (data) => emit(BidvCheckingConnectionSuccess(
        serviceId: data.serviceId,
        merchantId: data.merchantId,
        tranDate: data.tranDate,
        transId: data.transId,
        responseCode: data.responseCode,
        moreInfo: data.moreInfo,
        secureCode: data.secureCode,
      )),
      failure: (failure) => emit(
        BidvCheckingConnectionFailure(failure.message ?? ''),
      ),
    );
  }
}
