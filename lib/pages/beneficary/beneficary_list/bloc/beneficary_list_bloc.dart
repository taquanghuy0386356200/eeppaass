import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:epass/pages/bloc/bloc_transforms.dart';
import 'package:equatable/equatable.dart';
import 'package:simple_result/simple_result.dart';

part 'beneficary_list_event.dart';
part 'beneficary_list_state.dart';

class BeneficiaryListBloc
    extends Bloc<BeneficiaryListEvent, BeneficiaryListState> {
  final IBeneficiaryRepository _beneficiaryRepository;

  final int _pageSize;
  int _startRecord = 0;

  BeneficiaryListBloc({
    required IBeneficiaryRepository beneficiaryRepository,
    int pageSize = 20,
  })  : _pageSize = pageSize,
        _beneficiaryRepository = beneficiaryRepository,
        super(const BeneficiaryListState(listData: [])) {
    on<BeneficiaryListFetched>(_onBeneficiaryListFetched);
    on<BeneficiaryListRefreshed>(_onBeneficiaryListRefreshed);
    on<BeneficiaryListLoadMore>(_onBeneficiaryListLoadMore);
    on<BeneficiaryListSearched>(
      _onBeneficiaryListSearched,
      transformer: throttleDroppable(const Duration(milliseconds: 100)),
    );
  }

  FutureOr<void> _onBeneficiaryListFetched(
    BeneficiaryListFetched event,
    Emitter<BeneficiaryListState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      reminiscentName: null,
      error: null,
    ));

    final result = await _getBeneficiaryList();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            total: data.count,
            isFull: data.listData.length == data.count,
            reminiscentName: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        reminiscentName: null,
      )),
    );
  }

  FutureOr<void> _onBeneficiaryListRefreshed(
    BeneficiaryListRefreshed event,
    Emitter<BeneficiaryListState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      reminiscentName: state.reminiscentName,
      error: null,
    ));

    final result = await _getBeneficiaryList();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            total: data.count,
            isFull: data.listData.length == data.count,
            listData: data.listData,
            reminiscentName: state.reminiscentName,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        reminiscentName: state.reminiscentName,
        error: failure.message,
      )),
    );
  }

  FutureOr<void> _onBeneficiaryListLoadMore(
    BeneficiaryListLoadMore event,
    Emitter<BeneficiaryListState> emit,
  ) async {
    final listData = state.listData;

    emit(state.copyWith(
      isLoadingMore: true,
      reminiscentName: state.reminiscentName,
      error: null,
    ));

    final result = await _getBeneficiaryList();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(state.copyWith(
          isLoadingMore: false,
          listData: listData,
          total: data.count,
          isFull: listData.length == data.count,
          reminiscentName: state.reminiscentName,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        reminiscentName: state.reminiscentName,
        error: failure.message,
      )),
    );
  }

  FutureOr<void> _onBeneficiaryListSearched(
    BeneficiaryListSearched event,
    Emitter<BeneficiaryListState> emit,
  ) async {
    final reminiscentName = event.reminiscentName?.trim();
    if (reminiscentName != null) {
      emit(state.copyWith(
        // isLoading: true,
        reminiscentName: reminiscentName.isEmpty ? null : reminiscentName,
        error: null,
      ));

      _startRecord = 0;

      final result = await _getBeneficiaryList();

      result.when(
        success: (data) {
          _startRecord += _pageSize;

          emit(
            state.copyWith(
              isLoading: false,
              listData: data.listData,
              total: data.count,
              isFull: data.listData.length == data.count,
              reminiscentName: state.reminiscentName,
              error: null,
            ),
          );
        },
        failure: (failure) => emit(state.copyWith(
          isLoading: false,
          reminiscentName: state.reminiscentName,
          error: failure.message,
        )),
      );
    }
  }
  
  Future<Result<BeneficiaryListDataNotNull, Failure>>
      _getBeneficiaryList() async {
    final result = await _beneficiaryRepository.getUserBeneficiarys(
      pageSize: _pageSize,
      startRecord: _startRecord,
      reminiscentName: state.reminiscentName,
    );
    return result;
  }
}
