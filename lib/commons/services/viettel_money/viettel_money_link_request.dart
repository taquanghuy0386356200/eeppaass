import 'package:epass/commons/models/vehicle/vehicle.dart';

class ViettelMoneyLinkRequest {
  // Mã giao dịch của đối tác (yyyyMMddHHmmssSSS[000000-999999]) example: "20200707121212789000000"
  final String orderId;
  final String contractId;
  final String contractNo;
  final String msisdn;
  final String contractIdNo;
  final String accountNo;
  final String contractFullName;
  final String contractObjetType;
  final String contractIdType;
  final List<Vehicle> vehicles;

  ViettelMoneyLinkRequest({
    required this.orderId,
    required this.contractId,
    required this.contractNo,
    this.msisdn = 'n/a',
    this.contractIdNo = 'n/a',
    this.accountNo = 'n/a',
    required this.contractFullName,
    this.contractObjetType = 'CN',
    this.contractIdType = 'IC',
    required this.vehicles,
  });

  Map<String, dynamic> toJson() {
    return {
      'orderId': orderId,
      'contractId': contractId,
      'contractNo': contractNo,
      'msisdn': msisdn,
      'contractIdNo': contractIdNo,
      'accountNo': accountNo,
      'contractFullName': contractFullName,
      'contractObjetType': contractObjetType,
      'contractIdType': contractIdType,
      'vehicles': vehicles.map((e) => e.toJson()).toList(),
    };
  }
}
