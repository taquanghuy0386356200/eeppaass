import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';

part 'create_feedback_otp_event.dart';

part 'create_feedback_otp_state.dart';

class CreateFeedbackOtpBloc extends Bloc<CreateFeedbackOtpEvent, CreateFeedbackOtpState> {
  final IFeedbackRepository _feedbackRepository;

  CreateFeedbackOtpBloc({
    required IFeedbackRepository feedbackRepository,
  })  : _feedbackRepository = feedbackRepository,
        super(CreateFeedbackOtpInitial()) {
    on<CreateFeedbackOtpRequested>((event, emit) async {
      // 17 - Góp ý
      // 18 - Báo lỗi dịch vụ
      if (state is CreateFeedbackOtpRequestedSuccess) {
        final submittedTime = (state as CreateFeedbackOtpRequestedSuccess).submittedTime;

        final count = DateTime.now().difference(submittedTime).inSeconds;
        if (count < 500) {
          emit(CreateFeedbackOtpRequestedSuccess(submittedTime));
          return;
        }
      }
      if (state is CreateFeedbackOtpPaused) {
        final submittedTime = (state as CreateFeedbackOtpPaused).submittedTime;
        emit(CreateFeedbackOtpRequestedSuccess(submittedTime));
        return;
      }

      final result = await _feedbackRepository.feedbackRequestOtp(confirmType: 17);

      result.when(
        success: (success) {
          emit(CreateFeedbackOtpRequestedSuccess(DateTime.now()));
        },
        failure: (failure) {
          emit(CreateFeedbackOtpRequestedFailure(message: failure.message ?? 'Có lỗi xảy ra'));
        },
      );
    });

    on<FeedbackOtpPaused>((event, emit) async {
      if (state is CreateFeedbackOtpRequestedSuccess) {
        final submittedTime = (state as CreateFeedbackOtpRequestedSuccess).submittedTime;
        emit(CreateFeedbackOtpPaused(submittedTime: submittedTime));
      }
    });
  }
}
