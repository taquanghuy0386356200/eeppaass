part of 'transaction_vehicle_bloc.dart';

@immutable
class TransactionVehicleState extends Equatable {
  final List<TransactionVehicle> listData;
  final bool isFull;
  final String? plateNumber;
  final DateTime timestampInFrom;
  final DateTime timestampInTo;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const TransactionVehicleState({
    required this.listData,
    this.isFull = false,
    this.plateNumber,
    required this.timestampInFrom,
    required this.timestampInTo,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        plateNumber,
        timestampInFrom,
        timestampInTo,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  TransactionVehicleState copyWith({
    List<TransactionVehicle>? listData,
    bool? isFull,
    required String? plateNumber,
    DateTime? timestampInFrom,
    DateTime? timestampInTo,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return TransactionVehicleState(
      listData: listData ?? this.listData,
      isFull: isFull ?? this.isFull,
      plateNumber: plateNumber,
      timestampInFrom: timestampInFrom ?? this.timestampInFrom,
      timestampInTo: timestampInTo ?? this.timestampInTo,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
