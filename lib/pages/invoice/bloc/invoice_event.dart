part of 'invoice_bloc.dart';

@immutable
abstract class InvoiceEvent extends Equatable {
  const InvoiceEvent();
}

class InvoiceFetched extends InvoiceEvent {
  const InvoiceFetched();

  @override
  List<Object?> get props => [];
}

class InvoiceRefreshed extends InvoiceEvent {
  const InvoiceRefreshed();

  @override
  List<Object?> get props => [];
}

class InvoiceLoadMore extends InvoiceEvent {
  const InvoiceLoadMore();

  @override
  List<Object?> get props => [];
}

/*
    @Query('plateNumber') String? plateNumber,
    @Query('invoiceNo') String? invoiceNo,
    @Query('adjustmentType') int? adjustmentType,
    @Query('transactionType') int? transactionType,
    @Query('stationInId') int? stationInId,
    @Query('stationOutId') int? stationOutId,
 */
class InvoiceSearched extends InvoiceEvent {
  final String? plateNumber;
  final String? invoiceNo;
  final int? adjustmentType;
  final int? transactionType;
  final int? stationInId;
  final int? stationOutId;
  final DateTime? from;
  final DateTime? to;

  const InvoiceSearched({
    this.plateNumber,
    this.invoiceNo,
    this.adjustmentType,
    this.transactionType,
    this.stationInId,
    this.stationOutId,
    this.from,
    this.to,
  });

  @override
  List<Object?> get props => [
        plateNumber,
        invoiceNo,
        adjustmentType,
        transactionType,
        stationInId,
        stationOutId,
        from,
        to,
      ];
}

class InvoiceDateRangeChanged extends InvoiceEvent {
  final DateTimeRange saleTransDate;

  const InvoiceDateRangeChanged(this.saleTransDate);

  @override
  List<Object> get props => [saleTransDate];
}

class InvoicePlateNumberChanged extends InvoiceEvent {
  final String plateNumber;

  const InvoicePlateNumberChanged(this.plateNumber);

  @override
  List<Object> get props => [plateNumber];
}
