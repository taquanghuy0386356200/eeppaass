part of 'bidv_check_connection_bloc.dart';

abstract class BidvCheckingConnectionEvent extends Equatable {
  const BidvCheckingConnectionEvent();
}

class BidvCheckingConnectionProceeded extends BidvCheckingConnectionEvent {
  final String payerId;
  final String payerName;
  final String payerMobile;
  final String payerIdentity;

  const BidvCheckingConnectionProceeded({
    required this.payerId,
    required this.payerName,
    required this.payerMobile,
    required this.payerIdentity,
  });

  @override
  List<Object> get props => [payerId, payerName, payerMobile, payerIdentity];
}
