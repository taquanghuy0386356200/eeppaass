part of 'ticket_vehicle_bloc.dart';

abstract class TicketVehicleEvent extends Equatable {
  const TicketVehicleEvent();
}

class TicketVehicleUserChanged extends TicketVehicleEvent {
  final List<Vehicle> vehicles;

  const TicketVehicleUserChanged(this.vehicles);

  @override
  List<Object> get props => [vehicles];
}

class TicketVehicleOtherChanged extends TicketVehicleEvent {
  final List<Vehicle> vehicles;

  const TicketVehicleOtherChanged(this.vehicles);

  @override
  List<Object> get props => [vehicles];
}
