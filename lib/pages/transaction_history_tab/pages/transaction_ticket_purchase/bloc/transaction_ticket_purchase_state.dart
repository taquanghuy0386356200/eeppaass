part of 'transaction_ticket_purchase_bloc.dart';

class TransactionTicketPurchaseState extends Equatable {
  final List<TransactionTicketPurchase> listData;
  final bool isFull;
  final int efficiencyId;
  final String? plateNumber;
  final DateTime? saleTransDateFrom;
  final DateTime? saleTransDateTo;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const TransactionTicketPurchaseState({
    required this.listData,
    this.isFull = false,
    this.efficiencyId = 1,
    this.plateNumber,
    this.saleTransDateFrom,
    this.saleTransDateTo,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        efficiencyId,
        plateNumber,
        saleTransDateFrom,
        saleTransDateTo,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  TransactionTicketPurchaseState copyWith({
    List<TransactionTicketPurchase>? listData,
    bool? isFull,
    int? efficiencyId,
    required String? plateNumber,
    required DateTime? saleTransDateFrom,
    required DateTime? saleTransDateTo,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return TransactionTicketPurchaseState(
      listData: listData ?? this.listData,
      isFull: isFull ?? this.isFull,
      efficiencyId: efficiencyId ?? this.efficiencyId,
      plateNumber: plateNumber,
      saleTransDateFrom: saleTransDateFrom,
      saleTransDateTo: saleTransDateTo,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
