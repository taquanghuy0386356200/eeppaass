import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/models/customer_type/customer_type_response.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:equatable/equatable.dart';

part 'register_customer_event.dart';

part 'register_customer_state.dart';

class RegisterCustomerBloc extends Bloc<RegisterCustomerEvent, RegisterCustomerState> {
  RegisterCustomerBloc() : super(const RegisterCustomerState()) {
    on<RegisterCustomerUpdated>((event, emit) {
      emit(state.copyWith(
        customerType: event.customerType,
        idImageFrontPath: event.idImageFrontPath,
        idImageBackPath: event.idImageBackPath,
        name: event.name,
        dob: event.dob,
        gender: event.gender,
        documentNumber: event.documentNumber,
        placeOfIssue: event.placeOfIssue,
        dateOfIssue: event.dateOfIssue,
        email: event.email,
        phoneNumber: event.phoneNumber,
        address: event.address,
        province: event.province ?? state.province,
        district: event.district ?? state.district,
        ward: event.ward ?? state.ward,
      ));
    });

    on<RegisterCustomerProvinceUpdated>((event, emit) {
      emit(state.copyWith(
        province: event.province,
        district: null,
        ward: null,
      ));
    });

    on<RegisterCustomerDistrictUpdated>((event, emit) {
      emit(state.copyWith(
        province: state.province,
        district: event.district,
        ward: null,
      ));
    });
  }
}
