import 'package:epass/commons/models/viettel_pay_gate/viettel_pay_gate.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/cash_in/bloc/viettel_pay_gate_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:auto_route/auto_route.dart';

class CashInViettelPayGatePage extends StatefulWidget {
  final String title;
  final ViettelPayGateRequest request;

  const CashInViettelPayGatePage({
    Key? key,
    required this.title,
    required this.request,
  }) : super(key: key);

  @override
  State<CashInViettelPayGatePage> createState() =>
      _CashInViettelPayGatePageState();
}

class _CashInViettelPayGatePageState extends State<CashInViettelPayGatePage> {
  int _progress = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: _progress == 100
            ? BackButton(onPressed: context.popRoute)
            : const SizedBox.shrink(),
      ),
      body: BlocListener<ViettelPayGateBloc, ViettelPayGateState>(
        listener: (context, state) {
          if (state is ViettelPayGatePaymentCompletedSuccess) {
            context.read<BalanceBloc>().add(BalanceFetched());
            context.replaceRoute(CashInResultRoute(isSuccess: true));
          } else if (state is ViettelPayGatePaymentCompletedFailure) {
            context.replaceRoute(CashInResultRoute(
              isSuccess: false,
              message: state.message,
            ));
          }
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: ScreenUtil().statusBarHeight),
            _progress != 100
                ? LinearProgressIndicator(
                    value: _progress.toDouble() / 100.0,
                    backgroundColor: ColorName.blue.withOpacity(0.3),
                    valueColor: const AlwaysStoppedAnimation(ColorName.blue),
                    minHeight: 2.h,
                  )
                : SizedBox(height: 2.h),
            Flexible(
              fit: FlexFit.tight,
              child: WebView(
                initialUrl: widget.request.payGateUrl,
                javascriptMode: JavascriptMode.unrestricted,
                navigationDelegate: (navigation) {
                  final uri = Uri.parse(navigation.url);

                  final orderId = uri.queryParameters['order_id'];
                  final paymentStatus = uri.queryParameters['payment_status'];
                  final errorCode = uri.queryParameters['error_code'];
                  final transAmount = uri.queryParameters['trans_amount'];

                  if (paymentStatus != null &&
                      orderId != null &&
                      transAmount != null) {
                    final paymentStatusInt = int.tryParse(paymentStatus);
                    final transAmountInt = int.tryParse(transAmount);
                    if (paymentStatusInt != null && transAmountInt != null) {
                      context
                          .read<ViettelPayGateBloc>()
                          .add(ViettelPayPaymentCompleted(
                            paymentStatus: paymentStatusInt,
                            orderId: orderId,
                            transAmount: transAmountInt,
                            errorCode: errorCode,
                          ));

                      return NavigationDecision.prevent;
                    }
                  }
                  return NavigationDecision.navigate;
                },
                onProgress: (progress) {
                  setState(() {
                    _progress = progress;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
