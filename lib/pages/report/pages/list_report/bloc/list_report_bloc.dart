import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:simple_result/simple_result.dart';

part 'list_report_event.dart';

part 'list_report_state.dart';

class ListReportBloc extends ReportBloc {
  ListReportBloc({
    ReportBloc? reportBloc,
    required IFeedbackRepository feedbackRepository,
    required FirebaseMessaging firebaseMessaging,
  }) : super(reportBloc: reportBloc, feedbackRepository: feedbackRepository);
}

class ReportBloc extends Bloc<ReportEvent, ReportState> {
  final IFeedbackRepository _feedbackRepository;

  ReportBloc({
    ReportBloc? reportBloc,
    required IFeedbackRepository feedbackRepository,
  })  : _feedbackRepository = feedbackRepository,
        super(const ReportState(listReport: [])) {
    on<ReportFetched>(_onReportFetched);
    on<ReportRefreshed>(_onReportRefreshed);
  }

  FutureOr<void> _onReportFetched(
      ReportFetched event,
      Emitter<ReportState> emit,
      ) async {
    emit(state.copyWith(
      isLoading: true,
      listReport: const [],
      error: null,
    ));

    final result = await _getReport();

    result.when(
      success: (data) {
        print('data: $data');
        emit(
          state.copyWith(
            isLoading: false,
            listReport: data,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
      )),
    );
  }

  Future<void> _onReportRefreshed(ReportRefreshed event, Emitter<ReportState> emit) async {
    emit(state.copyWith(
      isRefreshing: true,
      error: null,
    ));

    final result = await _getReport();

    result.when(
      success: (data) {
        emit(
          state.copyWith(
            isRefreshing: false,
            listReport: data,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
      )),
    );
  }

  Future<Result<List<FeedbackModel>, Failure>> _getReport() async {
    final result = await _feedbackRepository.getFeedback(
      feedbackChannel: state.reportChannel,
    );
    return result;
  }
}
