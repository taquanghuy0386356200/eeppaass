import 'package:draggable_widget/draggable_widget.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MascotWidget extends StatefulWidget {
  final VoidCallback? onTap;
  final VoidCallback? onClose;

  const MascotWidget({
    Key? key,
    this.onTap,
    this.onClose,
  }) : super(key: key);

  @override
  State<MascotWidget> createState() => _MascotWidgetState();
}

class _MascotWidgetState extends State<MascotWidget> {
  final _dragController = DragController();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.delayed(const Duration(milliseconds: 500)),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return DraggableWidget(
            bottomMargin: 120.h,
            topMargin: 40.h,
            intialVisibility: true,
            horizontalSpace: 20.w,
            normalShadow: const BoxShadow(color: Colors.transparent),
            draggingShadow: const BoxShadow(color: Colors.transparent),
            shadowBorderRadius: 20,
            dragAnimationScale: 1.2,
            initialPosition: AnchoringPosition.bottomRight,
            dragController: _dragController,
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Material(
                  borderRadius: BorderRadius.circular(12.0),
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(12.0),
                    onTap: widget.onTap,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: SizedBox(
                        height: 120.h,
                        width: 80.w,
                        child: Assets.images.mascot.image(fit: BoxFit.fitHeight),
                      ),
                    ),
                  ),
                ),
                // Positioned.fill(
                //   top: -24.h,
                //   right: -12.w,
                //   child: Align(
                //     alignment: Alignment.topRight,
                //     child: IconButton(
                //       onPressed: () {
                //         _dragController.hideWidget();
                //       },
                //       icon: Assets.icons.closeMascot.svg(height: 32.r, width: 32.r),
                //     ),
                //   ),
                // )
              ],
            ),
          );
        }
        return const SizedBox.shrink();
      }
    );
  }
}
