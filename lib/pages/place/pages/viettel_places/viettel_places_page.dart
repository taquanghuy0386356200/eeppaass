import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/place/pages/bot_places/ext/viettel_place_type.dart';
import 'package:epass/pages/place/pages/viettel_places/widget/viettel_place_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViettelPlacesPage extends StatelessWidget {
  // TODO: get user location to pass to viettel place card

  const ViettelPlacesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      itemCount: ViettelPlaceType.values.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.fromLTRB(36.w, index == 0 ? 0.h : 20.h, 36.w, 16.h),
          child: ViettelPlaceCard(placeType: ViettelPlaceType.values[index]),
        );
      },
      separatorBuilder: (context, index) => Divider(
        color: ColorName.disabledBorderColor,
        height: 1,
        thickness: 1,
        indent: 36.w,
        endIndent: 36.w,
      ),
    );
  }
}
