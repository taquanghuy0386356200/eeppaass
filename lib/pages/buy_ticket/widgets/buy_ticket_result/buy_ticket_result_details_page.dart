import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/models/buy_ticket/buy_ticket_response.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/buy_ticket_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/service_plan_type_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/station_stage_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_duration_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';

class BuyTicketResultDetailPage extends StatelessWidget {
  const BuyTicketResultDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final stationStage = context.read<StationStageBloc>().state.stationStage;
    final servicePlanType =
        context.read<ServicePlanTypeBloc>().state.servicePlanType;
    final effectiveDate =
        context.read<TicketDurationBloc>().state.effectiveDate ??
            DateTime.now();
    final expireDate =
        context.read<TicketDurationBloc>().state.expireDate ?? DateTime.now();
    final effDateStr = Jiffy(effectiveDate).format('dd/MM/yyyy');
    final expDateStr = Jiffy(expireDate).format('dd/MM/yyyy');

    return BasePage(
      title: 'Chi tiết giao dịch',
      backgroundColor: Colors.white,
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          SafeArea(
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: FadeAnimation(
                delay: 1,
                direction: FadeDirection.up,
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        stationStage?.name ?? '',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.bold,
                              color: ColorName.primaryColor,
                            ),
                      ),
                      SizedBox(height: 16.h),
                      Row(
                        children: [
                          Assets.icons.ticketSm.svg(),
                          SizedBox(width: 8.w),
                          Text(
                            servicePlanType.label,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ],
                      ),
                      SizedBox(height: 4.h),
                      Row(
                        children: [
                          Assets.icons.clockSm.svg(),
                          SizedBox(width: 8.w),
                          Text(
                            '$effDateStr - $expDateStr',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.h),
                      Text(
                        'Thông tin xe',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.bold,
                              color: ColorName.primaryColor,
                            ),
                      ),
                      BlocSelector<BuyTicketBloc, BuyTicketState,
                          List<BuyTicketResponseItem>>(
                        selector: (state) {
                          if (state is BuyTicketConfirmedSuccess) {
                            final listSuccess = state.listSuccess;
                            final listFailure = state.listFailure;
                            return [...listSuccess, ...listFailure];
                          } else {
                            return [];
                          }
                        },
                        builder: (context, tickets) {
                          return ListView.separated(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              final ticket = tickets[index];
                              return Padding(
                                padding: EdgeInsets.fromLTRB(
                                  0.w,
                                  16.h,
                                  0.w,
                                  12.h,
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Biển xe: ${ticket.plateNumber}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(
                                                fontWeight: FontWeight.bold,
                                              ),
                                        ),
                                        const Spacer(),
                                        Text(
                                          'Xe loại ${ticket.vehicleGroupId}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1,
                                        ),
                                      ],
                                    ),
                                    if (ticket.reasons != null)
                                      SizedBox(height: 12.h),
                                    if (ticket.reasons != null)
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Icon(
                                            CupertinoIcons
                                                .exclamationmark_circle,
                                            color: ColorName.error,
                                          ),
                                          SizedBox(width: 8.w),
                                          Expanded(
                                            child: Text(
                                              ticket.reasons!,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                    color: ColorName.error,
                                                  ),
                                            ),
                                          ),
                                        ],
                                      ),
                                  ],
                                ),
                              );
                            },
                            separatorBuilder: (context, index) =>
                                Divider(color: Colors.grey.shade500),
                            itemCount: tickets.length,
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
