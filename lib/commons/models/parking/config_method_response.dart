

import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'config_method_response.g.dart';

@JsonSerializable()
class ConfigMethodVerifyModel {
  int? configId;
  int? contractId;
  int? serviceId;
  String? serviceName;
  int? verifyType;
  int? status;
  bool? isCreatedPassCode;

  ConfigMethodVerifyModel({
    this.configId,
    this.contractId,
    this.serviceId,
    this.serviceName,
    this.verifyType,
    this.status,
    this.isCreatedPassCode
  });

  factory ConfigMethodVerifyModel.fromJson(Map<String, dynamic> json) => _$ConfigMethodVerifyModelFromJson(json);
  Map<String, dynamic> toJson() => _$ConfigMethodVerifyModelToJson(this);
}

@JsonSerializable()
class ConfigMethodVerifyResponse {
  ConfigMethodVerifyResponse({
    this.data,
    this.mess,
  });

  final ConfigMethodVerifyModel? data;
  final MessageData? mess;

  factory ConfigMethodVerifyResponse.fromJson(Map<String, dynamic> json) => _$ConfigMethodVerifyResponseFromJson(json);
}
