part of 'fcm_bloc.dart';

@immutable
class FcmState extends Equatable {
  final String? token;
  final AuthorizationStatus authorizationStatus;
  final RemoteMessage? message;

  const FcmState({
    this.token,
    this.authorizationStatus = AuthorizationStatus.notDetermined,
    this.message,
  });

  @override
  List<Object?> get props => [token, authorizationStatus, message];

  @override
  bool? get stringify => true;

  FcmState copyWith({
    String? token,
    AuthorizationStatus? authorizationStatus,
    required RemoteMessage? message,
  }) {
    return FcmState(
      token: token ?? this.token,
      authorizationStatus: authorizationStatus ?? this.authorizationStatus,
      message: message,
    );
  }
}
