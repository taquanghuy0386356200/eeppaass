part of 'register_otp_bloc.dart';

abstract class RegisterOtpEvent extends Equatable {
  const RegisterOtpEvent();
}

class RegisterOtpRequested extends RegisterOtpEvent {
  final String phoneNumber;

  const RegisterOtpRequested(this.phoneNumber);

  @override
  List<Object> get props => [phoneNumber];
}
