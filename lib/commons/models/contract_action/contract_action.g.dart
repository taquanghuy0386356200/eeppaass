// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_action.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractAction _$ContractActionFromJson(Map<String, dynamic> json) =>
    ContractAction(
      actionAuditId: json['actionAuditId'] as String?,
      actionUserName: json['actionUserName'] as String?,
      actionType: json['actionType'] as String?,
      actionDate: json['actionDate'] as String?,
      userName: json['userName'] as String?,
      reason: json['reason'] as String?,
    );

Map<String, dynamic> _$ContractActionToJson(ContractAction instance) =>
    <String, dynamic>{
      'actionAuditId': instance.actionAuditId,
      'actionUserName': instance.actionUserName,
      'actionType': instance.actionType,
      'actionDate': instance.actionDate,
      'userName': instance.userName,
      'reason': instance.reason,
    };

ContractActionData _$ContractActionDataFromJson(Map<String, dynamic> json) =>
    ContractActionData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => ContractAction.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$ContractActionDataToJson(ContractActionData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

ContractActionResponse _$ContractActionResponseFromJson(
        Map<String, dynamic> json) =>
    ContractActionResponse(
      data: json['data'] == null
          ? null
          : ContractActionData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContractActionResponseToJson(
        ContractActionResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
