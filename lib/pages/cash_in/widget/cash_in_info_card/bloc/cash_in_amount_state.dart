part of 'cash_in_amount_bloc.dart';

@immutable
abstract class CashInAmountState extends Equatable {
  const CashInAmountState();
}

class CashInAmountChangedSuccess extends CashInAmountState {
  final int? amount;
  final String? topupChannel;

  const CashInAmountChangedSuccess({this.amount, this.topupChannel});

  @override
  List<Object?> get props => [amount, topupChannel];
}

class CashInTargetLastRecharge extends CashInAmountEvent {
  @override
  List<Object> get props => [];
}
