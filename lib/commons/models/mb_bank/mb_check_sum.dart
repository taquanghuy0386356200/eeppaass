import 'package:json_annotation/json_annotation.dart';

part 'mb_check_sum.g.dart';

@JsonSerializable()
class MBCheckSumRequest {
  final String checkSumKey;
  String token;

  MBCheckSumRequest({required this.checkSumKey, required this.token});

  factory MBCheckSumRequest.fromJson(Map<String, dynamic> json) =>
      _$MBCheckSumRequestFromJson(json);
}

@JsonSerializable()
class MBCheckSumResponse {
  final String clientMessageId;
  final String? errorCode;
  @JsonKey(name: 'data')
  final MBCheckSumData data;

  MBCheckSumResponse({
    required this.clientMessageId,
    required this.errorCode,
    required this.data,
  });

  factory MBCheckSumResponse.fromJson(Map<String, dynamic> json) =>
      _$MBCheckSumResponseFromJson(json);
}

@JsonSerializable()
class MBCheckSumData {
  final String transactionID;

  MBCheckSumData({required this.transactionID});

  factory MBCheckSumData.fromJson(Map<String, dynamic> json) =>
      _$MBCheckSumDataFromJson(json);
}
