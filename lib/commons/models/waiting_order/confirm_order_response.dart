import 'dart:ffi';

import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'confirm_order_response.g.dart';

@JsonSerializable()
class ConfirmOrder{
  String ticketId;
  bool success;
  String message;

  ConfirmOrder({
    required this.ticketId,
    required this.success,
    required this.message
  });

  Map<String, dynamic> toJson() => _$ConfirmOrderToJson(this);

  factory ConfirmOrder.fromJson(Map<String, dynamic> json)  => _$ConfirmOrderFromJson(json);
}

@JsonSerializable()
class ConfirmOrderResponse {
  List<ConfirmOrder>? details;
  String? responseTime;
  MessageData? mess;

  ConfirmOrderResponse({this.details, this.responseTime, this.mess});

  factory ConfirmOrderResponse.fromJson(Map<String, dynamic> json) => _$ConfirmOrderResponseFromJson(json);
}