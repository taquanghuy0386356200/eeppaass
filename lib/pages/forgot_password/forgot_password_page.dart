import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_logo_page.dart';
import 'package:epass/pages/forgot_password/widgets/forgot_password_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ForgotPasswordPage extends StatelessWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseLogoPage(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      leading: BackButton(onPressed: context.popRoute),
      child: Stack(
        children: [
          const GradientHeaderContainer(),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  padding: EdgeInsets.fromLTRB(16.w, 56.h, 16.w, 32.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          'Quên mật khẩu',
                          style:
                              Theme.of(context).textTheme.headline6!.copyWith(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                        ),
                      ),
                      SizedBox(height: 24.h),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 6.w),
                        child: Text(
                          'Quý khách vui lòng cung cấp thông tin đã đăng ký dịch'
                          ' vụ để xác thực yêu cầu cấp lại mật khẩu.',
                          style: Theme.of(context).textTheme.bodyText1,
                          // textAlign: TextAlign.center,
                        ),
                      ),

                      SizedBox(height: 20.h),

                      // FORGOT PASSWORD FORM
                      const ForgotPasswordForm(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
