import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/invoice/bloc/invoice_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InvoiceSearchForm extends StatefulWidget {
  final String? plateNumber;
  final String? invoiceNo;
  final DateTime? from;
  final DateTime? to;

  const InvoiceSearchForm({
    Key? key,
    this.plateNumber,
    this.invoiceNo,
    this.from,
    this.to,
  }) : super(key: key);

  @override
  State<InvoiceSearchForm> createState() => _InvoiceSearchFormState();
}

class _InvoiceSearchFormState extends State<InvoiceSearchForm> {
  final _plateNumberController = TextEditingController();
  final _invoiceNoController = TextEditingController();
  final _fromController = TextEditingController();
  final _toController = TextEditingController();
  final FocusNode _plateNumberFocusNode = FocusNode();
  final FocusNode _invoiceNoFocusNode = FocusNode();

  DateTime? _from;
  DateTime? _to;

  @override
  void initState() {
    super.initState();

    _plateNumberController.text = widget.plateNumber ?? '';
    _invoiceNoController.text = widget.invoiceNo ?? '';
    _from = widget.from;
    _to = widget.to;

    if (_from != null) {
      _fromController.text = Jiffy(_from).format('dd/MM/yyyy');
    }

    if (_to != null) {
      _toController.text = Jiffy(_to).format('dd/MM/yyyy');
    }
  }

  @override
  void dispose() {
    _plateNumberController.dispose();
    _invoiceNoController.dispose();
    _fromController.dispose();
    _toController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    'TÌM KIẾM',
                    style: Theme.of(context).textTheme.subtitle1,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 24.h),
                PrimaryTextField(
                  controller: _plateNumberController,
                  focusNode: _plateNumberFocusNode,
                  labelText: 'Biển số xe (00X1234)',
                  hintText: 'Biển số xe (00X1234)',
                  isPlateNumber: true,
                  maxLength: 20,
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(_invoiceNoFocusNode);
                  },
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _invoiceNoController,
                  focusNode: _invoiceNoFocusNode,
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(_plateNumberFocusNode);
                  },
                  labelText: 'Số hoá đơn',
                  hintText: 'Số hoá đơn',
                  maxLength: 100,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _fromController,
                  labelText: 'Thời gian hiệu lực: từ ngày',
                  hintText: 'Từ ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showInvoiceDateRangePicker,
                  onClear: () => _from = null,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _toController,
                  labelText: 'Thời gian hiệu lực: đến ngày',
                  hintText: 'Đến ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showInvoiceDateRangePicker,
                  onClear: () => _to = null,
                ),
                SizedBox(height: 34.h),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: SecondaryButton(
                          title: 'Đặt lại',
                          onTap: () {
                            context
                                .read<InvoiceBloc>()
                                .add(const InvoiceFetched());
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: PrimaryButton(
                          title: 'Tìm kiếm',
                          onTap: () {
                            context.read<InvoiceBloc>().add(
                                  InvoiceSearched(
                                    plateNumber: _plateNumberController.text,
                                    invoiceNo: _invoiceNoController.text,
                                    from: _from,
                                    to: _to,
                                  ),
                                );

                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // SizedBox(height: 16.h),
              ],
            ),
          ),
          Positioned(
            top: 12.h,
            right: 12.w,
            child: SplashIconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: ColorName.borderColor,
              ),
              onTap: () => Navigator.of(context).pop(),
            ),
          ),
        ],
      ),
    );
  }

  void _showInvoiceDateRangePicker() async {
    DateTimeRange? initialDateRange;
    if (_from != null) {
      initialDateRange = DateTimeRange(
        start: _from!,
        end: _to ?? Jiffy().dateTime,
      );
    }else{
      initialDateRange = DateTimeRange(start: DateTime.now(), end: DateTime.now());
    }

    final dateRange = await showDateRangePicker(
          context: context,
          locale: const Locale('vi'),
          initialDateRange: initialDateRange,
          firstDate: Jiffy().subtract(years: 2).startOf(Units.DAY).dateTime,
          lastDate: Jiffy().dateTime,
          initialEntryMode: DatePickerEntryMode.calendarOnly,
        ) ??
        initialDateRange;

    _from = dateRange?.start;
    _to = dateRange?.end;

    if (_from != null) {
      _fromController.text = Jiffy(_from).format('dd/MM/yyyy');
    }

    if (_to != null) {
      _toController.text = Jiffy(_to).format('dd/MM/yyyy');
    }
  }
}
