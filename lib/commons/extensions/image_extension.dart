import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

String baseImage = 'assets/images';

class ImageAssests {
  static String headerChangeInfoClient =
      '$baseImage/header_change_info_client.png';
  static String headerChangeInfoVehicle =
      '$baseImage/header_change_info_vehicle.png';
  static String headerTransferVehicle =
      '$baseImage/header_transfer_vehicle_larger.png';
  static String headerChangeInfoContract =
      '$baseImage/header_change_info_contract.png';
  static String headerSplitContract = '$baseImage/header_split_contract.png';
  static String headerMergeContract = '$baseImage/header_merge_contract.png';
  static String headerRequestForm = '$baseImage/header_request_form.png';
  static String arrowLogoHeader = '$baseImage/arrow_logo_header.png';
  static String smsRegister = '$baseImage/sms_register.png';

  static String test = '$baseImage/insurance.svg';

  static String manageAccount = '$baseImage/manage_account.png';

  static String deleteImage = '$baseImage/delete_image.svg';
  static String arrowTop = '$baseImage/arrow_top.svg';
  static String iconCarVehicle = '$baseImage/icon_car_vehicle.svg';

  static String iconVehicleTransfer = '$baseImage/vehicle_transfer.png';
  static String iconMergeContract = '$baseImage/merge_contract.png';
  static String download = '$baseImage/download.png';
  static String referralsClient = '$baseImage/referrals_client.png';
  static String referralsClientHeader = '$baseImage/referral_client_header.png';
  static String alertIcon = '$baseImage/alert_ic.svg';
  static String warningDebt =
      '$baseImage/warning_debt.png';
  static String successDebt =
      '$baseImage/success_debt.png';
  static String iconReceivedVehicleTransfer =
      '$baseImage/received_vehicle_transfer.png';
  static String referralsLogo = '$baseImage/referrals_logo.png';


  static SvgPicture svgAssets(
    String name, {
    Color? color,
    double? width,
    double? height,
    BoxFit? fit,
    BlendMode? blendMode,
  }) {
    var w = width;
    var h = height;
    return SvgPicture.asset(
      name,
      colorBlendMode: blendMode ?? BlendMode.srcIn,
      color: color,
      width: w,
      height: h,
      fit: fit ?? BoxFit.none,
    );
  }
}
