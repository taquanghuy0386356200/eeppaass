import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';

class ConfirmDialog extends StatelessWidget {
  final String title;
  final String? content;
  final Widget? image;
  final TextAlign contentTextAlign;
  final TextStyle? contentTextStyle;
  final String secondaryButtonTitle;
  final bool hasSecondaryButton;
  final String primaryButtonTitle;
  final VoidCallback? onSecondaryButtonTap;
  final VoidCallback onPrimaryButtonTap;
  final bool? isSpace;

  const ConfirmDialog(
      {Key? key,
      required this.title,
      this.content,
      this.image,
      this.contentTextAlign = TextAlign.start,
      this.contentTextStyle,
      this.hasSecondaryButton = true,
      this.secondaryButtonTitle = 'Đóng',
      this.primaryButtonTitle = 'Xác nhận',
      this.onSecondaryButtonTap,
      required this.onPrimaryButtonTap,
      this.isSpace = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.w,
            vertical: 24.h,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (image != null) image!,
              isSpace == false
                  ? SizedBox(height: 20.h)
                  : const SizedBox.shrink(),
              title.isNotEmpty
                  ? Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.w),
                      child: Text(
                        title,
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    )
                  : const SizedBox.shrink(),
              if (content != null && isSpace == false) SizedBox(height: 16.h),
              if (content != null)
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                  child: Text(
                    content!,
                    textAlign: contentTextAlign,
                    style: contentTextStyle ??
                        Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorName.textGray2,
                            ),
                  ),
                ),
              SizedBox(height: 32.h),
              Row(
                children: [
                  if (hasSecondaryButton)
                    Expanded(
                      child: SizedBox(
                        height: 50.h,
                        // child: TextButton(
                        //   onPressed: onSecondaryButtonTap ??
                        //       () => Navigator.of(context).pop(),
                        //   child: Text(
                        //     secondaryButtonTitle,
                        //     style: Theme.of(context).textTheme.button!.copyWith(
                        //           fontSize: 17.sp,
                        //           color: ColorName.borderColor,
                        //           fontWeight: FontWeight.w600,
                        //         ),
                        //   ),
                        // ),
                        child: SecondaryButton(
                          onTap: onSecondaryButtonTap ??
                              () => Navigator.of(context).pop(),
                          title: secondaryButtonTitle,
                        ),
                      ),
                    ),
                  if (hasSecondaryButton) SizedBox(width: 20.w),
                  Expanded(
                    child: SizedBox(
                      height: hasSecondaryButton ? 50.h : 56.h,
                      child: PrimaryButton(
                        onTap: onPrimaryButtonTap,
                        title: primaryButtonTitle,
                        padding: EdgeInsets.zero,
                      ),
                    ),
                  ),
                ],
              ),
              // SizedBox(height: 8.h),
            ],
          ),
        ),
      ),
    );
  }
}
