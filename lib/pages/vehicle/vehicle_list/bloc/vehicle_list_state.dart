part of 'vehicle_list_bloc.dart';

class VehicleListState extends Equatable {
  final List<Vehicle> listData;
  final int? total;
  final bool isFull;
  final String? plateNumber;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const VehicleListState({
    required this.listData,
    this.total,
    this.isFull = false,
    this.plateNumber,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        plateNumber,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  VehicleListState copyWith({
    List<Vehicle>? listData,
    int? total,
    bool? isFull,
    String? plateNumber,
    String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return VehicleListState(
      listData: listData ?? this.listData,
      total: total ?? this.total,
      isFull: isFull ?? this.isFull,
      plateNumber: plateNumber,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
