import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/commons/widgets/buttons/floating_primary_button.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_ticket_purchase/bloc/transaction_ticket_purchase_bloc.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_ticket_purchase/widget/transaction_ticket_purchase_card.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_ticket_purchase/widget/transaction_ticket_purchase_search_form.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_ticket_purchase/widget/transaction_ticket_purchase_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionTicketPurchasePage extends StatefulWidget {
  final String? plateNumber;

  const TransactionTicketPurchasePage({
    Key? key,
    this.plateNumber,
  }) : super(key: key);

  @override
  State<TransactionTicketPurchasePage> createState() =>
      _TransactionTicketPurchasePageState();
}

class _TransactionTicketPurchasePageState
    extends State<TransactionTicketPurchasePage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TransactionTicketPurchaseBloc(
        transRepository: getIt<ITransactionHistoryRepository>(),
      )..add(widget.plateNumber == null
          ? const TransactionTicketPurchaseFetched()
          : TransactionTicketPurchaseSearched(
              efficiencyId: 1, // Còn hiệu lực
              plateNumber: widget.plateNumber,
            )),
      child: BlocConsumer<TransactionTicketPurchaseBloc,
          TransactionTicketPurchaseState>(
        listener: (context, state) {
          final error = state.error;

          if (error == null) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          } else {
            _refreshController.refreshFailed();
            _refreshController.loadFailed();
          }
        },
        builder: (context, state) {
          final error = state.error;

          if (state.isLoading || state.isRefreshing && state.listData.isEmpty) {
            return const TransactionTicketPurchaseShimmerLoading();
          } else if (error != null && state.listData.isEmpty) {
            return CommonErrorPage(
              message: state.error,
              onTap: () => context
                  .read<TransactionTicketPurchaseBloc>()
                  .add(const TransactionTicketPurchaseFetched()),
            );
          }
          final listData = state.listData;
          return Scaffold(
            backgroundColor: Colors.transparent,
            floatingActionButton: GestureDetector(
              onTap: () => _onActionButtonTapped(
                context,
                plateNumber: state.plateNumber,
                efficiencyId: state.efficiencyId,
                saleTransDateFrom: state.saleTransDateFrom,
                saleTransDateTo: state.saleTransDateTo,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "Tìm kiếm",
                    style: Theme.of(context).textTheme.caption!.copyWith(
                        color: Colors.orange,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  const FloatingPrimaryButton(),
                ],
              ),
            ),
            body: state.listData.isEmpty
                ? NoDataPage(
                    title: context
                            .read<TransactionTicketPurchaseBloc>()
                            .isSearching
                        ? 'Bạn không có giao dịch nào phát sinh trong thời gian tìm kiếm'
                        : 'Bạn chưa có giao dịch nào phát sinh',
                    description:
                       '',
                  )
                : SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    enablePullUp: !state.isFull,
                    onRefresh: () => context
                        .read<TransactionTicketPurchaseBloc>()
                        .add(const TransactionTicketPurchaseRefreshed()),
                    onLoading: () => context
                        .read<TransactionTicketPurchaseBloc>()
                        .add(const TransactionTicketPurchaseLoadMore()),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        final item = listData[index];
                        return Padding(
                          padding: EdgeInsets.fromLTRB(
                            16.w,
                            index == 0 ? 24.h : 0.h,
                            16.w,
                            index == listData.length - 1 ? 80.h : 0.h,
                          ),
                          child: TransactionTicketPurchaseCard(item: item),
                        );
                      },
                      separatorBuilder: (context, index) =>
                          SizedBox(height: 24.h),
                    ),
                  ),
          );
        },
      ),
    );
  }

  void _onActionButtonTapped(
    BuildContext context, {
    String? plateNumber,
    required int efficiencyId,
    DateTime? saleTransDateFrom,
    DateTime? saleTransDateTo,
  }) async {
    await showBarModalBottomSheet(
      context: context,
      expand: false,
      useRootNavigator: true,
      builder: (modalContext) => BlocProvider.value(
        value: context.read<TransactionTicketPurchaseBloc>(),
        child: TransactionTicketPurchaseSearchForm(
          plateNumber: plateNumber,
          efficiencyId: efficiencyId,
          saleTransDateFrom: saleTransDateFrom,
          saleTransDateTo: saleTransDateTo,
        ),
      ),
    );
  }
}
