import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/enum/cash_in_for.dart';
import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/balance_alt_widget.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
// import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/container/footer_container.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
// import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_result_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
// import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
// import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/beneficary/beneficary_list/bloc/beneficary_list_bloc.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/cash_in/bloc/create_beneficiary_bloc.dart';
// import 'package:epass/pages/cash_in/bloc/create_beneficiary_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/bloc/payment_methods_card_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CashInResultPage extends StatefulWidget {
  final bool isSuccess;
  final String? message;

  const CashInResultPage({
    Key? key,
    this.isSuccess = true,
    this.message,
  }) : super(key: key);

  @override
  State<CashInResultPage> createState() => _CashInResultPageState();
}

class _CashInResultPageState extends State<CashInResultPage> {
  final _reminiscentName = TextEditingController();
  final _contractNo = TextEditingController();
  final _customerName = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _reminiscentName.dispose();
    _contractNo.dispose();
    _customerName.dispose();
  }

  @override
  void initState() {
    context.read<BeneficiaryListBloc>().add(BeneficiaryListFetched());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final listBeneficiary = context.select((BeneficiaryListBloc bloc) {
      final state = bloc.state.listData;
      return state;
    });

    final amount = context.select((CashInAmountBloc bloc) {
      final state = bloc.state;
      return state is CashInAmountChangedSuccess ? state.amount : null;
    });

    final userName = context.select(
      (CashInTargetBloc bloc) => bloc.state.userName,
    );

    final cashInFor = context.select(
      (CashInTargetBloc bloc) => bloc.state.cashInFor,
    );

    final contractNo = context.select(
      (CashInTargetBloc bloc) => bloc.state.contractNo,
    );

    final paymentMethodName = context.select((PaymentMethodsCardBloc bloc) {
      final state = bloc.state;
      return state is PaymentMethodsFetchedSuccess
          ? state.selectedPaymentMethod?.name
          : null;
    });

    List listFinal = [];
    if (listBeneficiary.isNotEmpty) {
      listFinal =
          listBeneficiary.where((o) => o.contractNo == contractNo).toList();
    }

    _contractNo.text = contractNo.toString();
    _customerName.text = userName.toString();
    setState(() {
      if (_reminiscentName.text == '') {
        _reminiscentName.text = '${'$userName'.toUpperCase()}/$contractNo';
      }
    });

    return BaseResultPage(
      title: widget.isSuccess ? 'Nạp tiền thành công' : 'Nạp tiền thất bại',
      leading: BackButton(onPressed: context.popRoute),
      heading: widget.isSuccess ? 'Nạp tiền thành công!' : 'Nạp tiền thất bại!',
      image: widget.isSuccess
          ? Assets.images.cashIn.cashInSuccess.image(
              height: 200.r,
              width: 200.r,
            )
          : Assets.images.cashIn.cashInFailure.image(
              height: 200.r,
              width: 200.r,
            ),
      content: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Số tiền',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: ColorName.borderColor,
                      ),
                ),
                const Spacer(),
                Text(
                  (amount ?? 0).formatCurrency(symbol: 'đ'),
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ],
            ),
            SizedBox(height: 8.h),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Vào ví',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: ColorName.borderColor,
                      ),
                ),
                // const Spacer(),
                SizedBox(width: 16.w),
                Expanded(
                  child: Text(
                    userName != null ? 'EPASS $userName'.toUpperCase() : 'N/A',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                          overflow: TextOverflow.ellipsis,
                        ),
                    maxLines: 2,
                    textAlign: TextAlign.end,
                  ),
                ),
              ],
            ),
            SizedBox(height: 8.h),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Phương thức',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: ColorName.borderColor,
                      ),
                ),
                const Spacer(),
                Text(
                  paymentMethodName ?? 'N/A',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ],
            ),
            if (cashInFor == CashInFor.me) SizedBox(height: 8.h),
            if (cashInFor == CashInFor.me)
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Số dư hiện tại',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorName.borderColor,
                        ),
                  ),
                  const Spacer(),
                  BlocProvider(
                    create: (context) => BalanceBloc(
                      appBloc: getIt<AppBloc>(),
                      userRepository: getIt<IUserRepository>(),
                    )..add(BalanceFetched()),
                    child: BalanceAltWidget(
                      mainTextSize: 16.sp,
                    ),
                  ),
                ],
              ),
            SizedBox(height: 8.h),
            if (widget.message != null)
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Lí do',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorName.borderColor,
                        ),
                  ),
                  SizedBox(width: 20.w),
                  Flexible(
                      child: Text(
                    widget.message!,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorName.error,
                        ),
                  )),
                ],
              ),
            // SizedBox(height: 24.h),
            SizedBox(height: 10.h),
            if (cashInFor == CashInFor.other &&
                listFinal.isEmpty &&
                widget.isSuccess)
              Row(
                children: [
                  OutlinedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0))),
                    ),
                    child: Row(
                      children: [
                        const Icon(Icons.person_add),
                        SizedBox(width: 10.w),
                        const Text(
                          "Lưu người thụ hưởng",
                          style: TextStyle(color: Colors.black),
                        ),
                      ],
                    ),
                    onPressed: () {
                      showModalBottomSheet<CreateBeneficiaryBloc>(
                        context: context,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(20.0),
                          ),
                        ),
                        builder: (BuildContext context) {
                          return BlocConsumer<CreateBeneficiaryBloc,
                              CreateBeneficiaryState>(
                            listener: (context, state) {
                              if (state
                                  is CreateBeneficiaryFormSubmittedSuccess) {
                                // show success dialog
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (dialogContext) {
                                    return SuccessDialog(
                                      content:
                                          'Lưu thông tin người thụ hưởng thành công',
                                      contentTextAlign: TextAlign.center,
                                      buttonTitle: 'Đóng',
                                      onButtonTap: () async {
                                        Navigator.of(dialogContext).pop();
                                        Navigator.of(context).pop();
                                      },
                                    );
                                  },
                                );
                              } else if (state
                                  is CreateBeneficiaryFormSubmittedFailure) {
                                showErrorSnackBBar(
                                    context: context, message: state.message);
                              }
                            },
                            builder: (context, state) {
                              return Container(
                                padding:
                                    EdgeInsets.fromLTRB(16.w, 20.h, 16.w, 20.h),
                                child: Scaffold(
                                  resizeToAvoidBottomInset: false,
                                  body: SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        const Center(
                                          child: Text(
                                            'Lưu thông tin người thụ hưởng',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 8.0,
                                        ),
                                        const Divider(
                                          color: Colors.grey,
                                          height: 5.0,
                                        ),
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        PrimaryTextField(
                                          controller: _contractNo,
                                          labelText: 'Số hợp đồng',
                                          maxLength: 13,
                                          inputType: TextInputType.text,
                                          isCurrency: true,
                                          enabled: false,
                                          hasClearButton: false,
                                          suffix: const Icon(
                                            Icons.account_circle_rounded,
                                            size: 26,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        PrimaryTextField(
                                          controller: _customerName,
                                          labelText: 'Tên khách hàng',
                                          inputType: TextInputType.text,
                                          enabled: false,
                                          hasClearButton: false,
                                          suffix: const Icon(
                                            Icons.account_circle_rounded,
                                            size: 26,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        PrimaryTextField(
                                          controller: _reminiscentName,
                                          labelText: 'Tên gợi nhớ',
                                          inputType: TextInputType.text,
                                          isPlateNumber: true,
                                          suffix: const Icon(
                                            Icons.account_circle_rounded,
                                            size: 26,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        LoadingPrimaryButton(
                                            title: 'Lưu',
                                            enabled:
                                                _reminiscentName.text != '',
                                            onTap: () => {
                                                  context
                                                      .read<
                                                          CreateBeneficiaryBloc>()
                                                      .add(
                                                          CreateBeneficiaryFormSubmitted(
                                                        contractNo: contractNo
                                                            .toString(),
                                                        customerName:
                                                            userName.toString(),
                                                        reminiscentName:
                                                            _reminiscentName
                                                                .text,
                                                      ))
                                                }),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      );
                    },
                  ),
                ],
              ),
          ],
        ),
      ),
      footer: FooterContainer(
        height: 100.h,
        hasShadow: false,
        child: SizedBox(
          height: 56.h,
          width: double.infinity,
          child: PrimaryButton(
            title: 'Về trang chủ',
            onTap: () {
              context
                  .read<HomeTabsBloc>()
                  .add(const HomeTabBarRequestHidden(isHidden: false));
              context.read<BalanceBloc>().add(BalanceFetched());
              context.navigateTo(const HomeRoute());
            },
          ),
        ),
      ),
    );
  }
}
