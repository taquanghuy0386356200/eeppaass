part of 'cash_in_bloc.dart';

@immutable
abstract class CashInEvent extends Equatable {
  const CashInEvent();
}

class CashInFetchedTopupFee extends CashInEvent {
  final TopupChannelCode topupChannelCode;
  final int value;

  const CashInFetchedTopupFee({
    required this.topupChannelCode,
    required this.value,
  });

  @override
  List<Object> get props => [topupChannelCode, value];
}

class CashInOrderViettelPayCreated extends CashInEvent {
  final String contractId;
  final String contractNo;
  final String customerId;
  final int amount;
  final String desContractId;
  final int quantity = 1;
  final String paymentMethodName;

  const CashInOrderViettelPayCreated({
    required this.contractId,
    required this.contractNo,
    required this.customerId,
    required this.amount,
    required this.desContractId,
    required this.paymentMethodName,
  });

  @override
  List<Object?> get props => [
        contractId,
        contractNo,
        customerId,
        amount,
        desContractId,
        paymentMethodName
      ];
}

class CashInOrderMomoCreated extends CashInEvent {
  final String contractId;
  final String contractNo;
  final String customerId;
  final int amount;
  final String desContractId;
  final int quantity = 1;

  const CashInOrderMomoCreated({
    required this.contractId,
    required this.contractNo,
    required this.customerId,
    required this.amount,
    required this.desContractId,
  });

  @override
  List<Object?> get props =>
      [contractId, contractNo, customerId, amount, desContractId];
}

class CashInOrderVNPayCreated extends CashInEvent {
  final String contractId;
  final String contractNo;
  final String customerId;
  final int amount;
  final String desContractId;
  final int quantity = 1;

  const CashInOrderVNPayCreated({
    required this.contractId,
    required this.contractNo,
    required this.customerId,
    required this.amount,
    required this.desContractId,
  });

  @override
  List<Object?> get props =>
      [contractId, contractNo, customerId, amount, desContractId];
}

class CashInMomoPayment extends CashInEvent {
  final String orderId;
  final String contractNo;
  final int amount;

  const CashInMomoPayment({
    required this.orderId,
    required this.contractNo,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, contractNo, amount];
}

class ConfirmMomoOrder extends CashInEvent {
  final String partnerCode = F.momoPartnerCode;
  final String orderId;
  final int amount;
  final String phoneNumber;
  final String token;

  ConfirmMomoOrder({
    required this.orderId,
    required this.amount,
    required this.phoneNumber,
    required this.token,
  });

  @override
  List<Object> get props => [partnerCode, orderId, amount, phoneNumber, token];
}

class CashInOrderBIDVConnected extends CashInEvent {
  final String contractId;
  final String contractNo;
  final String customerId;
  final int amount;
  final String desContractId;
  final int quantity = 1;

  const CashInOrderBIDVConnected({
    required this.contractId,
    required this.contractNo,
    required this.customerId,
    required this.amount,
    required this.desContractId,
  });

  @override
  List<Object?> get props =>
      [contractId, contractNo, customerId, amount, desContractId];
}

class CashInContractsPayment extends CashInEvent {
  final String topupChannel;
  final String destContractId;
  final int amount;
  final String requestId;

  const CashInContractsPayment(
      {required this.topupChannel,
      required this.destContractId,
      required this.amount,
      required this.requestId});

  @override
  List<Object> get props => [topupChannel, destContractId, amount, requestId];
}

//dumv
class BidvCreateOrder extends CashInEvent {
  final String payerAdd;
  final String payerId;
  final String payerIdentity;
  final String payerMobile;
  final String payerName;
  final int transAmount;
  const BidvCreateOrder(
      {required this.payerAdd,
      required this.payerId,
      required this.payerIdentity,
      required this.payerMobile,
      required this.payerName,
      required this.transAmount});

  @override
  List<Object> get props =>
      [payerAdd, payerId, payerIdentity, payerMobile, payerName, transAmount];
}

class BidvCheckOTP extends CashInEvent {
  final String payerAdd;
  final String transId;
  final String payerId;
  final String payerIdentity;
  final String payerMobile;
  final String payerName;
  final int transAmount;
  final String otpNumber;
  const BidvCheckOTP({
    required this.payerAdd,
    required this.transId,
    required this.payerId,
    required this.payerIdentity,
    required this.payerMobile,
    required this.payerName,
    required this.transAmount,
    required this.otpNumber,
  });

  @override
  List<Object> get props => [
        payerAdd,
        payerId,
        payerIdentity,
        payerMobile,
        payerName,
        transAmount,
        otpNumber
      ];
}

class CheckCustomerType extends CashInEvent {
  final String contractId;
  final int amount;

  const CheckCustomerType({required this.contractId, required this.amount});

  @override
  List<Object> get props => [contractId, amount];
}

class CheckVehicleInContract extends CashInEvent {
  final String contractId;

  const CheckVehicleInContract({required this.contractId});

  @override
  List<Object> get props => [contractId];
}
