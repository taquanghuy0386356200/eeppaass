fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios certs

```sh
[bundle exec] fastlane ios certs
```

Check iOS certificates

### ios build

```sh
[bundle exec] fastlane ios build
```

Build iOS app

### ios pre_deploy

```sh
[bundle exec] fastlane ios pre_deploy
```

Bump new flutter release version

### ios post_deploy

```sh
[bundle exec] fastlane ios post_deploy
```

Generates release notes and download link for telegram

### ios tag

```sh
[bundle exec] fastlane ios tag
```

Create next tag

### ios stg

```sh
[bundle exec] fastlane ios stg
```

Deploy staging build to Diawi and send a message to Telegram channel

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
