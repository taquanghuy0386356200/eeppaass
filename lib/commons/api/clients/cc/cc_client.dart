import 'package:dio/dio.dart';
import 'package:epass/commons/models/feedback/feedback_request.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/report/report_request.dart';
import 'package:retrofit/retrofit.dart';
import 'package:retrofit/http.dart' as http;

import 'package:epass/commons/api/endpoint.dart' as endpoint;

part 'cc_client.g.dart';

@RestApi()
abstract class CCClient {
  factory CCClient(
    Dio dio, {
    String baseUrl,
  }) = _CCClient;

  /// /api/v1/mobile/tickets
  @GET(endpoint.feedback)
  Future<FeedbackResponse> getFeedback({
    @Query('ticketChannel') int? feedbackChannelValue,
  });

  /// /api/v1/ticket-types
  @GET(endpoint.feedbackType)
  Future<FeedbackTypeResponse> getFeedbackTypes({
    @Query('parentId') int? parentId,
    @Query('type') int? feedbackChannelValue,
  });

  /// /api/v1/mobile/tickets
  @POST(endpoint.feedback)
  Future<NoData> feedback({
    @Body() required FeedbackRequest request,
  });

  /// /api/v1/contracts/request-otp
  @GET(endpoint.feedbackRequestOtp)
  Future<NoData> feedbackRequestOtp({
    @Query('confirmType') required int confirmType,
  });

  /// /api/v1/ticket-attachment/{attachmentId}/download
  @POST(endpoint.downloadAttachment)
  @http.Headers({
    'Accept': 'application/json',
  })
  @DioResponseType(ResponseType.bytes)
  Future<HttpResponse<List<int>>> downloadAttachment({
    @Path('attachmentId') required int attachmentId,
  });
  /// /api/v1/mobile/tickets
  @POST(endpoint.feedback)
  Future<NoData> report({
    @Body() required ReportRequest request,
  });
}
