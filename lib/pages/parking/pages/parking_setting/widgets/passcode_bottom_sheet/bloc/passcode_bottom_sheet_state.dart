part of 'passcode_bottom_sheet_bloc.dart';


class PasscodeBottomSheetState extends Equatable {
  const PasscodeBottomSheetState();

  @override
  List<Object> get props => [];
}

class ChangePasscodeFormSubmitted extends PasscodeBottomSheetState {
  final String newPassword;

  const ChangePasscodeFormSubmitted({
    required this.newPassword,
  });

  @override
  List<Object> get props => [newPassword];
}

class ChangePasscodeFormInitial extends PasscodeBottomSheetState {
  @override
  List<Object> get props => [];
}

class ChangePasscodeFormValidated extends PasscodeBottomSheetState {
  const ChangePasscodeFormValidated();

  @override
  List<Object> get props => [];
}

class ChangePasscodeFormSubmittedInProgress extends PasscodeBottomSheetState {
  const ChangePasscodeFormSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class ChangePasscodeFormSubmittedSuccess extends PasscodeBottomSheetState {
  const ChangePasscodeFormSubmittedSuccess();

  @override
  List<Object> get props => [];
}

class ChangePasscodeFormSubmittedFailure extends PasscodeBottomSheetState {
  final String message;

  const ChangePasscodeFormSubmittedFailure(this.message);

  @override
  List<Object> get props => [];
}