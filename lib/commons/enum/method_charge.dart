import 'package:json_annotation/json_annotation.dart';

@JsonEnum(alwaysCreate: true)
enum MethodChargeInt {
  @JsonValue(1)
  normal,
  @JsonValue(2)
  block,
  unknown,
}

extension MethodChargeIntExt on MethodChargeInt {
  String get label {
    switch (this) {
      case MethodChargeInt.normal:
        return 'Thường';
      case MethodChargeInt.block:
        return 'Block';
      case MethodChargeInt.unknown:
        return 'Không xác định';
    }
  }
}

@JsonEnum(alwaysCreate: true)
enum MethodChargeString {
  @JsonValue('1')
  normal,
  @JsonValue('2')
  block,
  unknown,
}

extension MethodChargeStringExt on MethodChargeString {
  String get label {
    switch (this) {
      case MethodChargeString.normal:
        return 'Thường';
      case MethodChargeString.block:
        return 'Block';
      case MethodChargeString.unknown:
        return 'Không xác định';
    }
  }
}
