// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_sms_vehicle_setting.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserSmsVehicleSetting _$UserSmsVehicleSettingFromJson(
        Map<String, dynamic> json) =>
    UserSmsVehicleSetting(
      saleTransId: json['saleTransId'] as int?,
      saleTransCode: json['saleTransCode'] as String?,
      saleTransDetailId: json['saleTransDetailId'] as int?,
      name: json['name'] as String?,
      description: json['description'] as String?,
      saleTransType: json['saleTransType'] as String?,
      effDate: json['effDate'] as String?,
      expDate: json['expDate'] as String?,
      code: json['code'] as String?,
      autoRenew: json['autoRenew'] as String?,
      smsRemain: json['smsRemain'] as int?,
    );

Map<String, dynamic> _$UserSmsVehicleSettingToJson(
        UserSmsVehicleSetting instance) =>
    <String, dynamic>{
      'saleTransId': instance.saleTransId,
      'saleTransCode': instance.saleTransCode,
      'saleTransDetailId': instance.saleTransDetailId,
      'name': instance.name,
      'description': instance.description,
      'saleTransType': instance.saleTransType,
      'effDate': instance.effDate,
      'expDate': instance.expDate,
      'code': instance.code,
      'autoRenew': instance.autoRenew,
      'smsRemain': instance.smsRemain,
    };

UserSmsVehicleSettingResponse _$UserSmsVehicleSettingResponseFromJson(
        Map<String, dynamic> json) =>
    UserSmsVehicleSettingResponse(
      data: json['data'] == null
          ? null
          : UserSmsVehicleSetting.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserSmsVehicleSettingResponseToJson(
        UserSmsVehicleSettingResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
