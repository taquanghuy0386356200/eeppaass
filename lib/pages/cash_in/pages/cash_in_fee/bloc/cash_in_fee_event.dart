part of 'cash_in_fee_bloc.dart';

@immutable
abstract class CashInFeeEvent extends Equatable {
  const CashInFeeEvent();
}

class CashInFeeFetched extends CashInFeeEvent {
  @override
  List<Object> get props => [];
}

class CashInFeeReset extends CashInFeeEvent {
  @override
  List<Object> get props => [];
}
