import 'dart:convert';
import 'dart:developer';

import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart' as flutterBloc;
import 'package:auto_route/auto_route.dart';
import 'package:dio/dio.dart';
import 'package:epass/commons/models/auth_info/auth_info.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/pages/app.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ErrorInterceptor extends Interceptor {
  ErrorInterceptor();

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    switch (err.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw TimeoutException(err.requestOptions);
      case DioErrorType.response:
        switch (err.response?.statusCode) {
          case 401:
            final String jsonStringError = err.response.toString();
            final Map<String, dynamic> jsonError = jsonDecode(jsonStringError);
            final error = jsonError['error'].toString();
            if(error == 'Unauthorized') {
              try {
                final buildContext = navigatorKey.currentContext!;
                showDialog(
                  useRootNavigator: false,
                  context: buildContext,
                  barrierDismissible: true,
                  builder: (_) {
                    return ConfirmDialog(
                      title: 'Có lỗi xảy ra',
                      contentTextAlign: TextAlign.center,
                      content: "Hết phiên đăng nhập",
                      hasSecondaryButton: false,
                      primaryButtonTitle: 'Đóng',
                      onPrimaryButtonTap: () async {
                        Navigator.of(_).pop();
                        Navigator.of(_).pop();
                        buildContext.read<AppBloc>().add(UserLogout());
                        await buildContext.router.replace(const LoginRoute());
                      },
                    );
                  },
                );
              } on Exception catch (_) {
                throw UnauthorizedException(
                  requestOptions: err.requestOptions,
                  response: err.response,
                );
              }
            } else {
              throw UnauthorizedException(
                requestOptions: err.requestOptions,
                response: err.response,
              );
            }
            break;
          case 403:
            throw UnauthorizedException(
              requestOptions: err.requestOptions,
              response: err.response,
            );
          case 400:
          case 404:
          case 409:
          case 500:
            throw ResponseException(
              requestOptions: err.requestOptions,
              response: err.response,
            );
          case 502:
            throw ServiceUnavailableException(
                requestOptions: err.requestOptions);
        }
        break;
      case DioErrorType.cancel:
        break;
      case DioErrorType.other:
        throw BadNetworkException(err.requestOptions);
    }

    return handler.next(err);
  }
}

class ResponseException extends DioError {
  ResponseException({
    required RequestOptions requestOptions,
    Response? response,
  }) : super(requestOptions: requestOptions, response: response);

  @override
  String toString() {
    final data = response?.data;

    if (data != null) {
      try {
        final res = NoData.fromJson(data);
        final desc = res.mess?.description;
        if (desc != null) return desc;
      } on Exception {
        return 'Có lỗi xảy ra';
      }
    }

    return 'Có lỗi xảy ra';
  }
}

class ServiceUnavailableException extends DioError {
  ServiceUnavailableException({required RequestOptions requestOptions})
      : super(requestOptions: requestOptions);

  @override
  String toString() {
    return 'Dịch vụ hiện đang bị gián đoạn';
  }
}

class UnauthorizedException extends DioError {
  UnauthorizedException({
    required RequestOptions requestOptions,
    Response? response,
  }) : super(requestOptions: requestOptions, response: response);

  @override
  String toString() {
    final data = response?.data;

    if (data != null) {
      final mess = AuthMessage.fromJson(data);
      final errorDesc = mess.errorDescription;
      if (errorDesc != null) return errorDesc;
    }

    return 'Không có quyền truy cập';
  }
}

class NotFoundException extends DioError {
  NotFoundException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'The requested information could not be found';
  }
}

class BadNetworkException extends DioError {
  BadNetworkException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Không có kết nối, vui lòng thử lại';
  }
}

class TimeoutException extends DioError {
  TimeoutException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Kết nối bị gián đoạn, vui lòng thử lại';
  }
}
