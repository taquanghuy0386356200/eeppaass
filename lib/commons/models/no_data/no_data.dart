import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'no_data.g.dart';

@JsonSerializable()
class NoData {
  final MessageData? mess;

  NoData({
    required this.mess,
  });

  factory NoData.fromJson(Map<String, dynamic> json) => _$NoDataFromJson(json);
}
