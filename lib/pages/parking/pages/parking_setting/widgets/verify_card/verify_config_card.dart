import 'package:epass/commons/models/parking/verify_config_response.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/parking/pages/parking_setting/bloc/parking_setting_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VerifyConfigCard extends StatefulWidget {
  final VerifyConfigModel verifyConfig;
  final ParkingSettingBloc parkingBloc;

  const VerifyConfigCard(
      {Key? key, required this.verifyConfig, required this.parkingBloc})
      : super(key: key);

  @override
  State<VerifyConfigCard> createState() => _VerifyConfigCardState();
}

class _VerifyConfigCardState extends State<VerifyConfigCard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isCreatedPasscode = widget.verifyConfig.isCreatedPassCode!;
    return Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        shadowColor: Colors.grey,
        child: ListTile(
            leading: widget.verifyConfig.verifyType == 2
                ? Assets.icons.parkingService.svg(width: 25, height: 25)
                : Assets.icons.payOnApp.svg(width: 25, height: 25),
            title: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  widget.verifyConfig.verifyType == 2
                      ? 'Xác thực tại điểm giao dịch'
                      : 'Xác thực trên ứng dụng',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.w600,
                      ),
                )),
            subtitle: (isCreatedPasscode &&
                    widget.verifyConfig.status == 1 &&
                    widget.verifyConfig.verifyType == 2)
                ? TextButton(
                    onPressed: () {
                      widget.parkingBloc.add(PassCodeChanged(
                          verifyConfigModel: widget.verifyConfig));
                    },
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Đổi mật khẩu',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                        )))
                : null,
            trailing: CupertinoSwitch(
              value: widget.verifyConfig.status == 1,
              activeColor: ColorName.primaryColor,
              onChanged: (isOn) async {
                widget.parkingBloc.add(VerifyConfigAlerted(
                    isOn: isOn, verifyConfigModel: widget.verifyConfig));
              },
            )));
  }
}
