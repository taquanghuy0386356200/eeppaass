import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'feedback_form_event.dart';

part 'feedback_form_state.dart';

class FeedbackFormBloc extends Bloc<FeedbackFormEvent, FeedbackFormState> {
  FeedbackFormBloc() : super(const FeedbackFormState()) {
    on<FeedbackTypeChanged>((event, emit) {
      emit(state.copyWith(feedbackType: event.feedbackType));
    });
    on<ReceiveContentChanged>((event, emit) {
      emit(state.copyWith(receiveContent: event.content));
    });
    on<FeedbackAttachmentFilesChanged>((event, emit) {
      emit(state.copyWith(attachments: event.attachments));
    });
  }
}
