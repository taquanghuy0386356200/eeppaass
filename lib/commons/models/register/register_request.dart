import 'package:epass/commons/models/register/document_file.dart';
import 'package:epass/commons/models/register/register_vehicle_dto.dart';

/// identifier : ""
/// fullName : ""
/// dateOfIssue : ""
/// birthDate : ""
/// placeOfIssue : ""
/// gender : ""
/// email : ""
/// customerType : "CN"
/// areaCode : ""
/// phoneNumber : ""
/// province : ""
/// provinceName : ""
/// district : ""
/// districtName : ""
/// ward : ""
/// wardName : ""
/// street : ""
/// stickType : "1"
/// otp : ""
/// regisLevel : ""
/// noticeName : ""
/// noticeStreet : ""
/// noticeAreaCode : ""
/// noticePhoneNumber : ""
/// noticeEmail : ""
/// billCycle : ""
/// billCycleMergeType : ""
/// emailNotification : ""
/// pushNotification : ""
/// areaCodeEtc : ""
/// contractProfileDTOs : []
/// vehicleList : []

class RegisterRequest {
  RegisterRequest({
    this.identifier,
    this.fullName,
    this.dateOfIssue,
    this.birthDate,
    this.placeOfIssue,
    this.gender,
    this.email,
    this.customerType,
    this.areaCode,
    this.phoneNumber,
    this.province,
    this.provinceName,
    this.district,
    this.districtName,
    this.ward,
    this.wardName,
    this.street,
    this.stickType,
    this.otp,
    this.regisLevel,
    this.noticeName,
    this.noticeStreet,
    this.noticeAreaCode,
    this.noticePhoneNumber,
    this.noticeEmail,
    this.billCycle,
    this.billCycleMergeType,
    this.emailNotification,
    this.pushNotification,
    this.areaCodeEtc,
    this.contractProfileDTOs,
    this.vehicleList,
  });

  final String? identifier;
  final String? fullName;
  final String? dateOfIssue;
  final String? birthDate;
  final String? placeOfIssue;
  final String? gender;
  final String? email;
  final String? customerType;
  final String? areaCode;
  final String? phoneNumber;
  final String? province;
  final String? provinceName;
  final String? district;
  final String? districtName;
  final String? ward;
  final String? wardName;
  final String? street;
  final String? stickType;
  final String? regisLevel;
  final String? noticeName;
  final String? noticeStreet;
  final String? noticeAreaCode;
  final String? noticePhoneNumber;
  final String? noticeEmail;
  final String? billCycle;
  final String? billCycleMergeType;
  final String? emailNotification;
  final String? pushNotification;
  final String? areaCodeEtc;
  final List<DocumentFile>? contractProfileDTOs;
  final List<RegisterVehicleDto>? vehicleList;
  final String? otp;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['identifier'] = identifier;
    map['fullName'] = fullName;
    map['dateOfIssue'] = dateOfIssue;
    map['birthDate'] = birthDate;
    map['placeOfIssue'] = placeOfIssue;
    map['gender'] = gender;
    map['email'] = email;
    map['customerType'] = customerType;
    map['areaCode'] = areaCode;
    map['phoneNumber'] = phoneNumber;
    map['province'] = province;
    map['provinceName'] = provinceName;
    map['district'] = district;
    map['districtName'] = districtName;
    map['ward'] = ward;
    map['wardName'] = wardName;
    map['street'] = street;
    map['stickType'] = stickType;
    map['otp'] = otp;
    map['regisLevel'] = regisLevel;
    map['noticeName'] = noticeName;
    map['noticeStreet'] = noticeStreet;
    map['noticeAreaCode'] = noticeAreaCode;
    map['noticePhoneNumber'] = noticePhoneNumber;
    map['noticeEmail'] = noticeEmail;
    map['billCycle'] = billCycle;
    map['billCycleMergeType'] = billCycleMergeType;
    map['emailNotification'] = emailNotification;
    map['pushNotification'] = pushNotification;
    map['areaCodeEtc'] = areaCodeEtc;
    if (contractProfileDTOs != null) {
      map['contractProfileDTOs'] = contractProfileDTOs?.map((v) => v.toJson()).toList();
    }
    if (vehicleList != null) {
      map['vehicleList'] = vehicleList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
