// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'topup_fee_update.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopUpFeeUpdate _$TopUpFeeUpdateFromJson(Map<String, dynamic> json) =>
    TopUpFeeUpdate(
      topupChannelCode: json['topupChannelCode'] as String?,
      topupChannelName: json['topupChannelName'] as String?,
      valueMin: json['valueMin'] as int?,
      valueMax: json['valueMax'] as int?,
      serviceFeeType: json['serviceFeeType'] as int?,
      serviceFeeValAdd: json['serviceFeeValAdd'] as int?,
      serviceFeeVal: (json['serviceFeeVal'] as num?)?.toDouble(),
      groupName: json['groupName'] as String?,
      groupImage: json['groupImage'] as String?,
      containButton: json['containButton'] as bool? ?? false,
      image: json['image'] as String?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$TopUpFeeUpdateToJson(TopUpFeeUpdate instance) =>
    <String, dynamic>{
      'topupChannelCode': instance.topupChannelCode,
      'topupChannelName': instance.topupChannelName,
      'valueMin': instance.valueMin,
      'valueMax': instance.valueMax,
      'serviceFeeType': instance.serviceFeeType,
      'serviceFeeValAdd': instance.serviceFeeValAdd,
      'serviceFeeVal': instance.serviceFeeVal,
      'groupName': instance.groupName,
      'groupImage': instance.groupImage,
      'containButton': instance.containButton,
      'image': instance.image,
      'url': instance.url,
    };

TopupFeeDataUpdate _$TopupFeeDataUpdateFromJson(Map<String, dynamic> json) =>
    TopupFeeDataUpdate(
      groupName: json['groupName'] as String?,
      groupImage: json['groupImage'] as String?,
      containButton: json['containButton'] as bool? ?? false,
      channels: (json['channels'] as List<dynamic>?)
          ?.map((e) => TopUpFeeUpdate.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TopupFeeDataUpdateToJson(TopupFeeDataUpdate instance) =>
    <String, dynamic>{
      'groupName': instance.groupName,
      'groupImage': instance.groupImage,
      'containButton': instance.containButton,
      'channels': instance.channels,
    };

TopupFeeUpdateResponse _$TopupFeeUpdateResponseFromJson(
        Map<String, dynamic> json) =>
    TopupFeeUpdateResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => TopupFeeDataUpdate.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TopupFeeUpdateResponseToJson(
        TopupFeeUpdateResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
