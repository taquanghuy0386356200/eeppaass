// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car_register_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarRegisterResponse _$CarRegisterResponseFromJson(Map<String, dynamic> json) =>
    CarRegisterResponse(
      data: json['data'] == null
          ? null
          : Vehicle.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : Mess.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CarRegisterResponseToJson(
        CarRegisterResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

Mess _$MessFromJson(Map<String, dynamic> json) => Mess(
      code: json['code'] as int?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$MessToJson(Mess instance) => <String, dynamic>{
      'code': instance.code,
      'description': instance.description,
    };
