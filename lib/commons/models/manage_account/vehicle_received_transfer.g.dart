// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle_received_transfer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleReceivedTransferTotalResponse
    _$VehicleReceivedTransferTotalResponseFromJson(Map<String, dynamic> json) =>
        VehicleReceivedTransferTotalResponse(
          data: (json['data'] as List<dynamic>?)
              ?.map((e) => VehicleReceivedTransferResponse.fromJson(
                  e as Map<String, dynamic>))
              .toList(),
          mess: json['mess'] == null
              ? null
              : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
        );

Map<String, dynamic> _$VehicleReceivedTransferTotalResponseToJson(
        VehicleReceivedTransferTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

VehicleReceivedTransferResponse _$VehicleReceivedTransferResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleReceivedTransferResponse(
      contractNo: json['contractNo'] as String?,
      contractId: json['contractId'] as int?,
      signName: json['signName'] as String?,
      isSelect: json['isSelect'] as bool? ?? false,
    );

Map<String, dynamic> _$VehicleReceivedTransferResponseToJson(
        VehicleReceivedTransferResponse instance) =>
    <String, dynamic>{
      'contractNo': instance.contractNo,
      'contractId': instance.contractId,
      'signName': instance.signName,
      'isSelect': instance.isSelect,
    };
