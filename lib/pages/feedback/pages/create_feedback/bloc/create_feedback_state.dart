part of 'create_feedback_bloc.dart';

abstract class CreateFeedbackState extends Equatable {
  const CreateFeedbackState();
}

class CreateFeedbackInitial extends CreateFeedbackState {
  @override
  List<Object> get props => [];
}

class CreateFeedbackFormSubmittedInProgress extends CreateFeedbackState {
  const CreateFeedbackFormSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class CreateFeedbackFormSubmittedSuccess extends CreateFeedbackState {
  final String? message;

  const CreateFeedbackFormSubmittedSuccess({this.message});

  @override
  List<Object?> get props => [message];
}

class CreateFeedbackFormSubmittedFailure extends CreateFeedbackState {
  final String message;

  const CreateFeedbackFormSubmittedFailure(this.message);

  @override
  List<Object> get props => [message];
}
