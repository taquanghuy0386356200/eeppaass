class EmailValidator {
  final emailMaxLength = 100;

  final _expression = RegExp(r'[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}');

  String? emailValidator(String? email) {
    if (email == null || email.isEmpty) {
      return 'Vui lòng nhập Email';
    } else if (!validateDot(email) || !_expression.hasMatch(email)) {
      return 'Email không hợp lệ';
    }
    return null;
  }

  bool validateDot(String value) {
    bool isValid = true;
    for (int i = 0; i < value.length - 1; i++) {
      if (value[i] == '.' && value[i + 1] == '.') {
        isValid = false;
        break;
      }
    }
    return isValid;
  }
}
