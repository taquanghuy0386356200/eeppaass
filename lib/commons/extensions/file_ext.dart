import 'dart:io';
import 'dart:math';

extension FileExt on File {
  double get size {
    final bytes = lengthSync().toDouble();
    final i = (log(bytes) / log(1024)).floor();
    return bytes / pow(1024, i);
  }
}