import 'dart:typed_data';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/services/local_file_service/local_file_service.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/modal/full_screen_bottom_sheet.dart';
import 'package:epass/commons/widgets/modal/pdf_view_modal.dart';
import 'package:epass/commons/widgets/modal/photo_view_modal.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/attachment_file_bloc/attachment_file_bloc.dart';
import 'package:epass/commons/widgets/support_center_widgets/feedback_shimmer_loading.dart';
import 'package:epass/pages/report/pages/list_report/bloc/list_report_bloc.dart';
import 'package:epass/pages/report/pages/list_report/widgets/report_widget.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:photo_view/photo_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:path/path.dart' as path;

class ListReportPage extends StatefulWidget with AutoRouteWrapper {
  const ListReportPage({Key? key}) : super(key: key);

  @override
  State<ListReportPage> createState() => _ListReportPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ListReportBloc(
            feedbackRepository: getIt<IFeedbackRepository>(),
            firebaseMessaging: getIt<FirebaseMessaging>(),
          ),
        ),
        BlocProvider(
          create: (context) => AttachmentFileBloc(
            feedbackRepository: getIt<IFeedbackRepository>(),
          ),
        ),
      ],
      child: this,
    );
  }
}

class _ListReportPageState extends State<ListReportPage> {
  late RefreshController _refreshController;

  var _reportItems = <FeedbackItem>[];

  @override
  void initState() {
    super.initState();
    context.read<ListReportBloc>().add(const ReportFetched(
      reportChannel: FeedbackChannel.report
    ));
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return BlocListener<AttachmentFileBloc, AttachmentFileState>(
      listener: (_, state) async {
        if (state is AttachmentFileDownloadedInProgress) {
          context.loaderOverlay.show();
        } else if (state is AttachmentFileDownloadedFailure) {
          context.loaderOverlay.hide();
          await showErrorSnackBBar(context: context, message: state.message);
        } else if (state is AttachmentFileDownloadedSuccess) {
          context.loaderOverlay.hide();
          final extension = path.extension(state.fileName);

          switch (extension.toLowerCase()) {
            case '.png':
            case '.jpeg':
            case '.jpg':
              await showFullScreenBottomSheet(
                context: context,
                builder: (context) {
                  return PhotoViewModal(
                    child: PhotoView(
                      imageProvider: MemoryImage(Uint8List.fromList(state.attachmentFileData)),
                    ),
                  );
                },
              );
              break;
            case '.pdf':
              await showFullScreenBottomSheet(
                context: context,
                builder: (context) {
                  return PDFViewModal(
                    title: 'Chi tiết hoá đơn',
                    child: PDFView(
                      pdfData: Uint8List.fromList(state.attachmentFileData),
                      fitEachPage: true,
                    ),
                  );
                },
              );
              break;
            default:
              final saveFileResult = await getIt<ILocalFileService>().saveFile(
                data: state.attachmentFileData,
                fileName: state.fileName,
              );

              await saveFileResult.when(
                success: (file) async {
                  await showSuccessSnackBBar(
                    context: context,
                    message: 'Tệp tin tải về thành công tại đường dẫn: ${file.path}',
                  );
                },
                failure: (failure) async {
                  await showErrorSnackBBar(
                    context: context,
                    message: failure.message ?? 'Có lỗi xảy ra',
                  );
                },
              );

              break;
          }
        }
      },
      child: BlocConsumer<ListReportBloc, ReportState>(
      listener: (context, state) {
        if (state.error == null) {
          _refreshController.refreshCompleted();
          _refreshController.loadComplete();
        } else if (state.error != null) {
          _refreshController.refreshFailed();
          _refreshController.loadFailed();

          if (state.listReport.isNotEmpty) {
            showErrorSnackBBar(
              context: context,
              message: state.error ?? 'Có lỗi xảy ra',
            );
          }
        }
      },
      builder: (context, state) {
        final error = state.error;

        if (state.isLoading || state.isRefreshing && state.listReport.isEmpty) {
          return const FeedbackShimmerLoading();
        } else if (error != null && state.listReport.isEmpty) {
          return CommonErrorPage(
            message: error,
            onTap: () => context.read<ListReportBloc>().add(const ReportFetched()),
          );
        }
        _reportItems = state.listReport.map((e) => FeedbackItem(feedback: e)).toList();

        if (_reportItems.isEmpty) {
          return NoDataPage(
            onTap: () => context.read<ListReportBloc>().add(const ReportRefreshed()),
          );
        }
        return SmartRefresher(
          physics: const BouncingScrollPhysics(),
          enablePullDown: true,
          onRefresh: () => context.read<ListReportBloc>().add(const ReportRefreshed()),
          onLoading: () => context.read<ListReportBloc>().add(const ReportFetched()),
          controller: _refreshController,
          child: ListView.builder(
            itemCount: _reportItems.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.fromLTRB(
                  16.w,
                  index == 0 ? 32.h : 16.h,
                  16.w,
                  index == _reportItems.length - 1 ? 32.h : 0,
                ),
                child: ShadowCard(
                  padding: EdgeInsets.fromLTRB(16.w, 12.h, 16.w, 12.h),
                  shadowOpacity: 0.5,
                  child: ReportWidget(
                    key: UniqueKey(),
                    feedbackItem: _reportItems[index],
                  ),
                ),
              );
            },
          ),
        );
      },
    ),
    );
  }
}
