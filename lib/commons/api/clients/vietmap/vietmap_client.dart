import 'package:dio/dio.dart';
import 'package:epass/commons/models/feedback/feedback_request.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/report/report_request.dart';
import 'package:epass/commons/models/vietmap/vietmap_product.dart';
import 'package:retrofit/retrofit.dart';
import 'package:retrofit/http.dart' as http;

import 'package:epass/commons/api/endpoint.dart' as endpoint;

part 'vietmap_client.g.dart';

@RestApi()
abstract class VietMapClient {
  factory VietMapClient(
    Dio dio, {
    String baseUrl,
  }) = _VietMapClient;


  @GET('https://api.vietmap.live/production/v4/epass/GetVmlProducts')
  Future<VietMapProductResponse> getVietMapProducts();
}
