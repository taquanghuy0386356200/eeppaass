import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';

class NeutralConfirmDialog extends StatelessWidget {
  final String title;
  final String content;
  final TextAlign contentTextAlign;
  final String secondaryButtonTitle;
  final Color? secondaryButtonColor;
  final bool hasSecondaryButton;
  final String primaryButtonTitle;
  final Color? primaryButtonColor;
  final VoidCallback? onSecondaryButtonTap;
  final VoidCallback onPrimaryButtonTap;

  const NeutralConfirmDialog({
    Key? key,
    required this.title,
    required this.content,
    this.contentTextAlign = TextAlign.start,
    this.secondaryButtonTitle = 'Đóng',
    this.secondaryButtonColor,
    this.primaryButtonTitle = 'Xác nhận',
    this.primaryButtonColor,
    this.onSecondaryButtonTap,
    required this.onPrimaryButtonTap,
    this.hasSecondaryButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.w,
            vertical: 16.h,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 16.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
              SizedBox(height: 20.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Text(
                  content,
                  textAlign: contentTextAlign,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorName.textGray2,
                      ),
                ),
              ),
              SizedBox(height: 24.h),
              Row(
                children: [
                  if (hasSecondaryButton)
                    Expanded(
                      child: SizedBox(
                        height: 48.h,
                        child: TextButton(
                          onPressed: onSecondaryButtonTap ??
                              () => Navigator.of(context).pop(),
                          child: Text(
                            secondaryButtonTitle,
                            style: Theme.of(context).textTheme.button!.copyWith(
                                  fontSize: 16.sp,
                                  color: secondaryButtonColor ??
                                      ColorName.primaryColor,
                                  fontWeight: FontWeight.w600,
                                ),
                          ),
                        ),
                      ),
                    ),
                  if (hasSecondaryButton) SizedBox(width: 20.w),
                  Expanded(
                    child: SizedBox(
                      height: 48.h,
                      child: TextButton(
                        onPressed: onPrimaryButtonTap,
                        child: Text(
                          primaryButtonTitle,
                          style: Theme.of(context).textTheme.button!.copyWith(
                                fontSize: 16.sp,
                                color: primaryButtonColor ??
                                    ColorName.primaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
