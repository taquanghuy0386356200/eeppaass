import 'package:dio/dio.dart';
import 'package:epass/commons/api/endpoint.dart' as endpoint;
import 'package:epass/commons/models/confirmation_wait_for/confirmation_wait_for.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/waiting_order/waiting_order.dart';
import 'package:retrofit/retrofit.dart';

part 'gateway_client.g.dart';

@RestApi()
abstract class GatewayClient {
  factory GatewayClient(
    Dio dio, {
    String baseUrl,
  }) = _GatewayClient;

  /// api/v1/mobile/sale-orders/waiting
  @GET(endpoint.getWaitingOrder)
  Future<WaitingOrderResponse> getWaitingOrder({
    @Query('serviceId') int? serviceId,
  });

  /// /api/v1/lane-parking/cancel
  @POST(endpoint.waitForConfirmationCancel)
  Future<NoData> waitForConfirmationCancel({
    @Body() ConfirmationWaitForRequest? saleOrderId,
  });

  /// api/v1/lane-parking/confirm-payment
  @POST(endpoint.waitForConfirmation)
  Future<NoData> waitForConfirmation({
    @Body() ConfirmationWaitForRequest? saleOrderId,
  });
}
