part of 'zalopay_gate_bloc.dart';

abstract class ZaloPayGateState extends CashInState {
  const ZaloPayGateState();
}

class ZaloPayInitial extends ZaloPayGateState {
  @override
  List<Object> get props => [];
}

class ZaloPayPaymentInProgress extends ZaloPayGateState {
  @override
  List<Object> get props => [];
}

class ZaloPayPaymentSuccess extends ZaloPayGateState {
  final String zptranstoken;

  const ZaloPayPaymentSuccess({
    required this.zptranstoken,
  });

  @override
  List<Object> get props => [zptranstoken];
}

class ZaloPayPaymentFailure extends ZaloPayGateState {
  final String message;

  const ZaloPayPaymentFailure(this.message);

  @override
  List<Object> get props => [message];
}
