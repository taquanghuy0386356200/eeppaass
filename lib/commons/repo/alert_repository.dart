import 'package:epass/commons/api/clients/category/category_client.dart';
import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/clients/text_to_speech/text_to_speech_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/station/station_detail.dart';
import 'package:epass/commons/models/station_vehicle_alert/station_vehicle_alert_request.dart';
import 'package:epass/commons/models/station_vehicle_alert/station_vehicle_alert_response.dart';
import 'package:epass/commons/models/text_to_speech/text_to_speech_request.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

abstract class IAlertRepository {
  Future<Result<StationDetailDataNotNull, Failure>> getStationDetails();

  Future<Result<StationVehicleAlert, Failure>> vehicleAlert({
    String? trackDate,
    required int stationId,
    required double lat,
    required double long,
  });

  Future<Result<List<int>, Failure>> getSpeech({
    required String ttsToken,
    required String text,
  });
}

class AlertRepository implements IAlertRepository {
  final CrmClient _crmClient;
  final CategoryClient _categoryClient;
  final TextToSpeechClient _textToSpeechClient;

  const AlertRepository({
    required CrmClient crmClient,
    required CategoryClient categoryClient,
    required TextToSpeechClient textToSpeechClient,
  })  : _crmClient = crmClient,
        _categoryClient = categoryClient,
        _textToSpeechClient = textToSpeechClient;

  @override
  Future<Result<StationDetailDataNotNull, Failure>> getStationDetails() async {
    try {
      final response = await _categoryClient.getStationDetails();

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || count == null || listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(StationDetailDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<StationVehicleAlert, Failure>> vehicleAlert({
    String? trackDate,
    required int stationId,
    required double lat,
    required double long,
  }) async {
    try {
      final request = StationVehicleAlertRequest(
        trackDate: trackDate ?? Jiffy().format('dd/MM/yyyy HH:mm:ss'),
        stationId: stationId,
        warLat1: lat,
        warLong1: long,
      );

      final response = await _crmClient.vehicleAlert(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE || response.data == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(response.data!);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<int>, Failure>> getSpeech({
    required String ttsToken,
    required String text,
  }) async {
    try {
      final response = await _textToSpeechClient.getSpeech(
        token: ttsToken,
        request: TextToSpeechRequest(text: text),
      );

      return Result.success(response.data);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
