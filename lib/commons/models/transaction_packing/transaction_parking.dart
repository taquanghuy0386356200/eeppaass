
import 'package:json_annotation/json_annotation.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
part 'transaction_parking.g.dart';

@JsonSerializable()
class TransactionPacking {
  String? code;
  String? serviceName;
  String? stationName;
  String? lanOut;
  String? typeTicket;
  String? timeRequestIn;
  String? timeRequestOut;
  String? fee;
  String? statusOrder;
  String? plateNumber;
  String? timeEditTransaction;
  int? moneyEditTransaction;
  String? timeCreateTransaction;
  String? statusTransaction;
  String? fuelType;
  double? quantity;
  double? unitPrice;
  int? isDelete;

  TransactionPacking(
      {this.code,
        this.serviceName,
        this.stationName,
        this.lanOut,
        this.typeTicket,
        this.timeRequestIn,
        this.timeRequestOut,
        this.fee,
        this.statusOrder,
        this.plateNumber,
        this.timeEditTransaction,
        this.moneyEditTransaction,
        this.timeCreateTransaction,
        this.statusTransaction,
        this.fuelType,
        this.quantity,
        this.unitPrice,
        this.isDelete
      });

  factory TransactionPacking.fromJson(Map<String, dynamic> json)  => _$TransactionPackingFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionPackingToJson(this);
}

@JsonSerializable()
class TransactionPackingData {
  List<TransactionPacking>? listData;
  int? count;

  TransactionPackingData({this.listData, this.count});

  factory TransactionPackingData.fromJson(Map<String, dynamic> json) => _$TransactionPackingDataFromJson(json);
}

class TransactionParkingDataNotNull {
  final List<TransactionPacking> listData;
  final int count;

  TransactionParkingDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class TransactionPackingResponse {
  TransactionPackingData? data;
  MessageData? mess;

  TransactionPackingResponse({this.data, this.mess});

  factory TransactionPackingResponse.fromJson(Map<String, dynamic> json) => _$TransactionPackingResponseFromJson(json);
}
