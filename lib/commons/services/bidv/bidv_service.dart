import 'package:epass/commons/api/failures/failures.dart';
import 'package:flutter/services.dart';
import 'package:simple_result/simple_result.dart';

class BIDVService {
  Future<Result<String?, Failure>> linkAccount({
    required String contractId,
    required String contractNo,
    required String contractFullName,
    // required List<Vehicle> vehicles,
  }) async {
    try {
      // final orderId = Jiffy().format('yyyyMMddHHmmssSSS') +
      //     Random().nextInt(999999 + 1).toString().padLeft(6, '0');

      // final request = ViettelMoneyLinkRequest(
      //   orderId: orderId,
      //   contractId: contractId,
      //   contractNo: contractNo,
      //   contractFullName: contractFullName,
      //   vehicles: vehicles,
      // );

      // Map<String, dynamic>? result = await _channel.invokeMapMethod(
      //   ViettelMoneyLinkConstant.methodLink,
      //   request.toJson(),
      // );

      //   if (result == null) {
      //     return const Result.failure(UnknownFailure());
      //   }

      //   final response = ViettelMoneyResponse.fromMap(result);

      //   if (response.resultStatus == ViettelMoneyStatus.failure) {
      //     return const Result.failure(UnknownFailure());
      //   }
      return const Result.success('abc');
    } on PlatformException catch (error) {
      return Result.failure(UnknownFailure(message: error.toString()));
    }
  }

  Future<Result<String?, Failure>> unlinkAccount({
    required String accountNumber,
    required String token,
    required String contractId,
  }) async {
    try {
      // final request = ViettelMoneyUnlinkRequest(
      //   accountNumber: accountNumber,
      //   token: token,
      //   contractId: contractId,
      // );

      // Map<String, dynamic>? result = await _channel.invokeMapMethod(
      //   ViettelMoneyLinkConstant.methodUnlink,
      //   request.toJson(),
      // );

      // if (result == null) {
      //   return const Result.failure(UnknownFailure());
      // }

      // final response = ViettelMoneyResponse.fromMap(result);
      // if (response.resultStatus == ViettelMoneyStatus.failure) {
      //   return const Result.failure(UnknownFailure());
      // }
      return const Result.success('def');
    } on PlatformException catch (error) {
      return Result.failure(UnknownFailure(message: error.toString()));
    }
  }
}
