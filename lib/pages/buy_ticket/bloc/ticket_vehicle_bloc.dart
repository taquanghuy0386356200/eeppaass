import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'ticket_vehicle_event.dart';

part 'ticket_vehicle_state.dart';

class TicketVehicleBloc extends Bloc<TicketVehicleEvent, TicketVehicleState> {
  TicketVehicleBloc({
    List<Vehicle>? userVehicles,
  }) : super(TicketVehicleState(userVehicles: userVehicles ?? [])) {
    on<TicketVehicleUserChanged>(_onTicketVehicleUserChanged);
    on<TicketVehicleOtherChanged>(_onTicketVehicleOtherChanged);
  }

  FutureOr<void> _onTicketVehicleUserChanged(
    TicketVehicleUserChanged event,
    Emitter<TicketVehicleState> emit,
  ) async {
    emit(state.copyWith(userVehicles: event.vehicles));
  }

  FutureOr<void> _onTicketVehicleOtherChanged(
    TicketVehicleOtherChanged event,
    Emitter<TicketVehicleState> emit,
  ) async {
    emit(state.copyWith(otherVehicles: event.vehicles));
  }
}
