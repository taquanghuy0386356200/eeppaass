// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_vehicle_types.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckVehicleTypesResponse _$CheckVehicleTypesResponseFromJson(
        Map<String, dynamic> json) =>
    CheckVehicleTypesResponse(
      data: json['data'] == null
          ? null
          : CheckVehicleTypes.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CheckVehicleTypesResponseToJson(
        CheckVehicleTypesResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

CheckVehicleTypes _$CheckVehicleTypesFromJson(Map<String, dynamic> json) =>
    CheckVehicleTypes(
      vehicleGroupId: json['vehicleGroupId'] as int?,
      vehicleGroupCode: json['vehicleGroupCode'] as String?,
      name: json['name'] as String?,
    );

Map<String, dynamic> _$CheckVehicleTypesToJson(CheckVehicleTypes instance) =>
    <String, dynamic>{
      'vehicleGroupId': instance.vehicleGroupId,
      'vehicleGroupCode': instance.vehicleGroupCode,
      'name': instance.name,
    };
