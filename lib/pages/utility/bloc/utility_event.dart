part of 'utility_bloc.dart';

@immutable
abstract class UtilityEvent extends Equatable {
  const UtilityEvent();
}

class UnavailableServiceFetched extends UtilityEvent {
  const UnavailableServiceFetched();

  @override
  List<Object?> get props => [];
}