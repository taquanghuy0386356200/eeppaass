import 'package:auto_route/auto_route.dart';
import 'package:car_doctor_epass_services/car_doctor_epass_services.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home/bloc/home_bloc.dart';
import 'package:epass/pages/home/bloc/viettel_money_link_bloc.dart';
import 'package:epass/pages/home/widgets/feature_button.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../utility/track/firebase_track.dart';

class FeaturesGrid extends StatelessWidget {
  const FeaturesGrid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appBloc = context.read<AppBloc>();
    final tabsBloc = context.read<HomeTabsBloc>();
    final features = [
      FeatureButton(
        title: 'Mua vé\n',
        icon: Assets.icons.ticket.svg(
          height: 32.r,
          width: 32.r,
        ),
        onTap: () async {
          await context.pushRoute(BuyTicketRouter());
          tabsBloc.add(const HomeTabBarRequestHidden(isHidden: false));
        },
      ),
      FeatureButton(
        title: 'Lịch sử\n giao dịch',
        icon: Assets.icons.orderHistory.svg(
          height: 32.r,
          width: 32.r,
        ),
        onTap: () => context.pushRoute(const TransactionHistoryRouter()),
      ),
      FeatureButton(
        title: 'Hoá đơn\n',
        icon: Assets.icons.invoice.svg(
          height: 32.r,
          width: 32.r,
        ),
        onTap: () => context.pushRoute(const InvoiceRoute()),
      ),
      // FeatureButton(
      //   title: 'Danh sách \nxe',
      //   icon: Assets.icons.vehicle.svg(
      //     height: 32.r,
      //     width: 32.r,
      //   ),
      //   onTap: () async {
      //     // TODO: wait for better approaching
      //     await context.pushRoute(const VehicleRouter());
      //     tabsBloc.add(const HomeTabBarRequestHidden(isHidden: false));
      //   },
      // ),
      FeatureButton(
        title: 'Dịch vụ\n Ô tô',
        icon: Assets.icons.carService.svg(
          height: 32.r,
          width: 32.r,
        ),
        onTap: () async{
          var appBloc = context.read<AppBloc>();
          var epassToken = '${appBloc.state.authInfo?.accessToken}';
          var contractId = '${appBloc.state.user?.contractId}';
          var name = '${appBloc.state.user?.userName}';
          var phone = '${appBloc.state.user?.phone}';
          var identifier = '${appBloc.state.user?.identifier}';
          FirebaseTrack.trackClick('Dịch vụ Ô tô');

          final sdk = SdkCarServices.instance;
          var flag = SdkFlavor.PRODUCTION;
          // if (F.appFlavor?.name == Flavor.prod.name) {
          //   flag = SdkFlavor.PRODUCTION;
          // }
          sdk.init(flag, epassToken, contractId, name, phone, identifier);
          await sdk.open(context);

          // context.pushRoute(const CarServiceRoute());
          },
      ),
      FeatureButton(
        title: 'Liên kết Viettel Money',
        icon: Assets.icons.viettelMoneyCompact.svg(
          height: 32.r,
          width: 32.r,
        ),
        onTap: () => _onViettelMoneyLinkTapped(context),
      ),
      FeatureButton(
        title: 'Mua\n bảo hiểm',
        icon: Assets.icons.insurance.svg(
          height: 32.r,
          width: 32.r,
        ),
        onTap: () => context.pushRoute(const InsuranceRouter()),
      ),
    ];

    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: features.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 16.w,
        mainAxisSpacing: 4.h,
        childAspectRatio: 0.8,
      ),
      itemBuilder: (context, index) {
        return features[index];
      },
    );
  }

  Future<void> _onViettelMoneyLinkTapped(BuildContext context) async {
    final homeState = context
        .read<HomeBloc>()
        .state;
    if (homeState is HomeLoaded) {
      final contractPayment = homeState.contractPayment;

      if (contractPayment == null) {
        // link viettel money
        final user = context
            .read<AppBloc>()
            .state
            .user;
        final contractId = user?.contractId;
        final contractNo = user?.contractNo;
        final userName = user?.userName;

        if (contractId != null && contractNo != null && userName != null) {
          context.read<ViettelMoneyLinkBloc>().add(ViettelMoneyLinkAccount(
            contractId: contractId,
            contractNo: contractNo,
            contractFullName: userName,
          ));
        }
      } else {
        // show dialog
        final isViettelPay = contractPayment.methodRechargeCode == 'MB';

        await showDialog(
          context: context,
          builder: (dialogContext) {
            return ConfirmDialog(
              title: 'Tài khoản đã liên kết với\n${isViettelPay
                  ? 'Viettel Pay'
                  : 'Tiền di động'}',
              content: 'Vui lòng duy trì số dư trong tài khoản ${isViettelPay
                  ? 'Viettel Pay'
                  : 'Tiền di động'}'
                  ' để qua trạm tại làn thu phí tự động.',
              contentTextAlign: TextAlign.center,
              image: isViettelPay
                  ? Assets.icons.viettelpay.svg(width: 54.r, height: 54.r)
                  : Assets.icons.mobileMoney.svg(width: 54.r, height: 54.r),
              secondaryButtonTitle: 'Huỷ liên kết',
              primaryButtonTitle: 'Đóng',
              onPrimaryButtonTap: () {
                Navigator.of(dialogContext).pop();
              },
              onSecondaryButtonTap: () async {
                Navigator.of(dialogContext).pop();
                final accountNumber = contractPayment.accountNumber;
                final token = contractPayment.token;
                final contractId = contractPayment.contractId;

                if (accountNumber != null && token != null &&
                    contractId != null) {
                  context.read<ViettelMoneyLinkBloc>().add(
                      ViettelMoneyUnlinkAccount(
                        accountNumber: accountNumber,
                        token: token,
                        contractId: contractId.toString(),
                      ));
                }
              },
            );
          },
        );
      }
    }
  }
}
