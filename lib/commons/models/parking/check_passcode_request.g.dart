// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_passcode_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckPassCodeRequest _$CheckPassCodeRequestFromJson(
        Map<String, dynamic> json) =>
    CheckPassCodeRequest(
      contractId: json['contractId'] as int,
      serviceId: json['serviceId'] as int,
      passCode: json['passCode'] as int,
    );

Map<String, dynamic> _$CheckPassCodeRequestToJson(
        CheckPassCodeRequest instance) =>
    <String, dynamic>{
      'contractId': instance.contractId,
      'serviceId': instance.serviceId,
      'passCode': instance.passCode,
    };
