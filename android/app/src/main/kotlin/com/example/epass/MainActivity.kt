﻿package com.example.epass

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import androidx.annotation.NonNull
import com.bplus.vtpay.fragment.etc.model.response.ContractInfoResponse
import com.bplus.vtpay.fragment.etc.model.response.ErrorCode
import io.flutter.Log
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.plugins.MomoUtils
import io.flutter.plugins.VnPayUtils
import io.flutter.plugins.VtMoneyUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import vn.momo.momo_partner.AppMoMoLib
import vn.viettelpay.sdk.ViettelPaySDK
import vn.viettelpay.sdk.VtpBuildVariant.PRODUCT
import vn.zalopay.sdk.Environment
import vn.zalopay.sdk.ZaloPayError
import vn.zalopay.sdk.ZaloPaySDK
import vn.zalopay.sdk.listeners.PayOrderListener

class MainActivity : FlutterFragmentActivity() {
    private val MOMO_CHANNEL = "com.viettel.etc.epass/momo_payment"
    private val VNPAY_CHANNEL = "com.viettel.etc.epass/vnpay_payment"
    private val VTMONEY_CHANNEL = "com.viettel.etc.epass/viettel_money"
    private lateinit var momoChannel: MethodChannel
    private lateinit var vnpayChannel: MethodChannel
    private lateinit var vtmoneyChannel: MethodChannel
    private lateinit var myResult: MethodChannel.Result
    private lateinit var myResultMBB: MethodChannel.Result

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        super.configureFlutterEngine(flutterEngine)
        momoChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, MOMO_CHANNEL)
        momoChannel.setMethodCallHandler { call, result ->
            myResult = result
            if (call.method == "requestPayment") {
                val arguments = call.arguments<HashMap<String, Any>>()
                println(arguments)
                MomoUtils().rechargeByMomo(this, arguments)
            } else {
                result.notImplemented()
            }
        }
        vnpayChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, VNPAY_CHANNEL)
        vnpayChannel.setMethodCallHandler { call, result ->
            myResult = result
            val arguments = call.arguments<HashMap<String, Any>>()
            println(arguments)
            VnPayUtils().openSdk(this, myResult, arguments)
        }
        vtmoneyChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, VTMONEY_CHANNEL)
        vtmoneyChannel.setMethodCallHandler { call, result ->
            myResult = result
            val arguments = call.arguments<HashMap<String, Any>>()
            println(arguments)
            println(call.method)
            ViettelPaySDK.getInstance().setupSDk(this.application, PRODUCT)
            when (call.method) {
                "link" -> VtMoneyUtils().linkEtc(this, arguments)
                "unlink" -> VtMoneyUtils().unLinkETC(this, arguments)
                else -> myResult.notImplemented()
            }
        }

        // zalo pay
        val channelPayOrder = "flutter.native/channelPayOrder"
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channelPayOrder)
                .setMethodCallHandler { call, result ->
                    if (call.method == "payOrder") {
                        val tagSuccess = "[OnPaymentSucceeded]"
                        val tagError = "[onPaymentError]"
                        val tagCanel = "[onPaymentCancel]"
                        val token = call.argument<String>("zptoken")
                        ZaloPaySDK.getInstance()
                                .payOrder(
                                        this@MainActivity,
                                        token!!,
                                        "epass://zalopays",
                                        object : PayOrderListener {
                                            override fun onPaymentCanceled(
                                                    zpTransToken: String?,
                                                    appTransID: String?
                                            ) {
                                                Log.d(
                                                        tagCanel,
                                                        String.format(
                                                                "[TransactionId]: %s, [appTransID]: %s",
                                                                zpTransToken,
                                                                appTransID
                                                        )
                                                )
                                                result.success("User Canceled")
                                            }

                                            override fun onPaymentError(
                                                    zaloPayErrorCode: ZaloPayError?,
                                                    zpTransToken: String?,
                                                    appTransID: String?
                                            ) {
                                                Log.d(
                                                        tagError,
                                                        String.format(
                                                                "[zaloPayErrorCode]: %s, [zpTransToken]: %s, [appTransID]: %s",
                                                                zaloPayErrorCode.toString(),
                                                                zpTransToken,
                                                                appTransID
                                                        )
                                                )
                                                result.success("Payment failed")
                                            }

                                            override fun onPaymentSucceeded(
                                                    transactionId: String,
                                                    transToken: String,
                                                    appTransID: String?
                                            ) {
                                                Log.d(
                                                        tagSuccess,
                                                        String.format(
                                                                "[TransactionId]: %s, [TransToken]: %s, [appTransID]: %s",
                                                                transactionId,
                                                                transToken,
                                                                appTransID
                                                        )
                                                )
                                                result.success("Payment Success" + appTransID)
                                            }
                                        }
                                )
                    } else {
                        Log.d("[METHOD CALLER] ", "Method Not Implemented")
                        result.success("Payment failed")
                    }
                }

        // dumv tessT
        // mb
        val channelMBBCreateOrder = "flutter.native/channelMBBCreateOrder"
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channelMBBCreateOrder)
                .setMethodCallHandler { call, result ->
                    if (call.method == "payOrderMBB") {
                        myResultMBB = result
                        try {
                            val url = call.argument<String>("urlMBB")
                            url?.let {
                                val intent = getDeepLinkIntent(url)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                this.startActivity(intent)
                                // if (!resolveAndFire(it, this)) {
                                //    myResultMBB.success("Không tìm thấy App MBBank")
                                // }
                            }
                        } catch (e: Exception) {
                            myResultMBB.success("Không tìm thấy App MBBank")
                        }
                    } else {
                        Log.d("channelMBBCreateOrder", "Method Not Implemented")
                        result.success("Payment failed")
                    }
                }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO && resultCode == -1) {
            if (data != null) {
                var status: Int = data.getIntExtra("status", -1)
                if (status == 0) {
                    val resultResponse: HashMap<String, Any?> = HashMap<String, Any?>()
                    resultResponse.put("momoappversion", data.getStringExtra("momoappversion"))
                    resultResponse.put("token", data.getStringExtra("data"))
                    resultResponse.put("message", data.getStringExtra("message"))
                    resultResponse.put("phoneNumber", data.getStringExtra("phonenumber"))
                    resultResponse.put("fromapp", data.getStringExtra("fromapp"))
                    resultResponse.put("appSource", data.getStringExtra("appSource"))
                    resultResponse.put("extra", data.getStringExtra("extra"))
                    resultResponse.put("orderId", data.getStringExtra("orderId"))
                    resultResponse.put("status", status)
                    resultResponse.put("isSuccess", status == 0)
                    myResult.success(resultResponse)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public fun onMessageEvent(response: ContractInfoResponse) {
        Log.d("status", response.status.code)
        Log.d("response.cancelRequest", response.cancelRequest)
        var res: Map<String, Any> = HashMap()
        if ("CANCEL_REQUEST".equals(response.cancelRequest, true)) {
            res = res.plus(Pair("status", 1))
            res = res.plus(Pair("message", ""))
        } else {
            res =
                    res.plus(
                            Pair(
                                    "status",
                                    ErrorCode.SUCCESS_ETC.equals(response.status.code, true).toInt()
                            )
                    )
            res = res.plus(Pair("message", response.status.message))
        }
        println(res)
        myResult.success(res)
    }

    fun Boolean.toInt() = if (this) 1 else 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // ZaloPaySDK.init(2554, Environment.SANDBOX); // Merchant AppID
        ZaloPaySDK.init(2414, Environment.PRODUCTION) // Merchant AppID
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val data = intent.data
        if (data != null) {
            if (::myResultMBB.isInitialized) {
                if (data.toString().indexOf("mbbankepass") >= 0) {
                    myResultMBB.success(data.toString())
                }
            }
        }
        ZaloPaySDK.getInstance().onResult(intent)
    }
    /// mb
    fun resolveAndFire(deepLinkUri: String, context: Context): Boolean {
        val intent = getDeepLinkIntent(deepLinkUri)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val resolveInfo = getResolveInfo(context, intent)
        if (resolveInfo != null) {
            context.startActivity(intent)
            return true
        } else {
            return false
        }
    }

    private fun getResolveInfo(context: Context, intent: Intent): ResolveInfo? {
        val pm = context.packageManager
        return pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
    }

    private fun getDeepLinkIntent(deepLinkUri: String): Intent {
        val uri = Uri.parse(deepLinkUri)
        val intent = Intent()
        intent.data = uri
        intent.action = Intent.ACTION_VIEW
        return intent
    }
}
