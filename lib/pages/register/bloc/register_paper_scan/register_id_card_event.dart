part of 'register_id_card_bloc.dart';

abstract class RegisterIdCardEvent extends Equatable {
  const RegisterIdCardEvent();
}

class RegisterIdCardUpdated extends RegisterIdCardEvent {
  final IdCardOcr idCard;
  final String? front;
  final String? back;

  const RegisterIdCardUpdated({
    required this.idCard,
    this.front,
    this.back,
  });

  @override
  List<Object?> get props => [idCard, front, back];
}
