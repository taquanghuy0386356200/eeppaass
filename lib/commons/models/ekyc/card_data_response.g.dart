// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_data_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CardDataResponse _$CardDataResponseFromJson(Map<String, dynamic> json) =>
    CardDataResponse(
      code: json['code'] as int?,
      message: json['message'] as String?,
      notice: json['notice'] as String?,
      frontImageId: json['front_image_id'] as String?,
      requestId: json['request_id'] as String?,
      requestTime: json['request_time'] as String?,
      responseTime: json['response_time'] as String?,
      ocrResult: json['ocr_result'] == null
          ? null
          : OCRResult.fromJson(json['ocr_result'] as Map<String, dynamic>),
      backImageId: json['back_image_id'] as String?,
    );

Map<String, dynamic> _$CardDataResponseToJson(CardDataResponse instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'notice': instance.notice,
      'request_id': instance.requestId,
      'front_image_id': instance.frontImageId,
      'back_image_id': instance.backImageId,
      'request_time': instance.requestTime,
      'response_time': instance.responseTime,
      'ocr_result': instance.ocrResult,
    };

OCRResult _$OCRResultFromJson(Map<String, dynamic> json) => OCRResult(
      message: json['message'] as String?,
      code: json['code'] as int?,
      information: json['information'] == null
          ? null
          : InformationModel.fromJson(
              json['information'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OCRResultToJson(OCRResult instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'information': instance.information,
    };

InformationModel _$InformationModelFromJson(Map<String, dynamic> json) =>
    InformationModel(
      expiry: json['expiry'] as String?,
      address: json['address'] as String?,
      birthday: json['birthday'] as String?,
      name: json['name'] as String?,
      ward: json['ward'] as String?,
      district: json['district'] as String?,
      province: json['province'] as String?,
      id: json['id'] as String?,
      provinceCode: json['province_code'] as String?,
      districtCode: json['district_code'] as String?,
      issueDate: json['issue_date'] as String?,
      birthplace: json['birthplace'] as String?,
      ethnicity: json['ethnicity'] as String?,
      feature: json['feature'] as String?,
      issueBy: json['issue_by'] as String?,
      nationality: json['nationality'] as String?,
      religion: json['religion'] as String?,
      sex: json['sex'] as String?,
      street: json['street'] as String?,
      wardCode: json['ward_code'] as String?,
      cmndId: json['cmnd_id'] as String?,
    );

Map<String, dynamic> _$InformationModelToJson(InformationModel instance) =>
    <String, dynamic>{
      'address': instance.address,
      'birthday': instance.birthday,
      'birthplace': instance.birthplace,
      'district': instance.district,
      'district_code': instance.districtCode,
      'ethnicity': instance.ethnicity,
      'expiry': instance.expiry,
      'feature': instance.feature,
      'id': instance.id,
      'cmnd_id': instance.cmndId,
      'issue_by': instance.issueBy,
      'issue_date': instance.issueDate,
      'name': instance.name,
      'nationality': instance.nationality,
      'province': instance.province,
      'province_code': instance.provinceCode,
      'religion': instance.religion,
      'sex': instance.sex,
      'street': instance.street,
      'ward': instance.ward,
      'ward_code': instance.wardCode,
    };
