import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contract_payment.g.dart';

@JsonSerializable()
class ContractPayment {
  ContractPayment({
    this.contractPaymentId,
    this.custId,
    this.contractId,
    this.methodRechargeId,
    this.accountNumber,
    this.accountOwner,
    this.accountBankId,
    this.createUser,
    this.createDate,
    this.description,
    this.status,
    this.token,
    this.orderId,
    this.methodRechargeCode,
    this.topupAuto,
    this.topupAmount,
    this.documentTypeId,
    this.documentTypeCode,
    this.documentNo,
    this.channel,
    this.linkPhone,
  });

  final int? contractPaymentId;
  final int? custId;
  final int? contractId;
  final int? methodRechargeId;
  final String? accountNumber;
  final String? accountOwner;
  final int? accountBankId;
  final String? createUser;
  final String? createDate;
  final String? description;
  final String? status;
  final String? token;
  final String? orderId;
  final String? methodRechargeCode;
  final int? topupAuto;
  final int? topupAmount;
  final int? documentTypeId;
  final String? documentTypeCode;
  final String? documentNo;
  final int? channel;
  final String? linkPhone;

  factory ContractPayment.fromJson(Map<String, dynamic> json) =>
      _$ContractPaymentFromJson(json);

  Map<String, dynamic> toJson() => _$ContractPaymentToJson(this);
}

@JsonSerializable()
class ContractPaymentResponse {
  @JsonKey(name: 'data')
  final ContractPayment? contractPayment;
  final MessageData? mess;

  ContractPaymentResponse({this.contractPayment, this.mess});

  factory ContractPaymentResponse.fromJson(Map<String, dynamic> json) =>
      _$ContractPaymentResponseFromJson(json);
}
