// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invoice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Invoice _$InvoiceFromJson(Map<String, dynamic> json) => Invoice(
      custInvoiceId: json['custInvoiceId'] as int?,
      custId: json['custId'] as int?,
      contractId: json['contractId'] as int?,
      invoiceNo: json['invoiceNo'] as String?,
      taxRateString: json['taxRateString'] as String?,
      invoiceIssuedDate: json['invoiceIssuedDate'] as String?,
      eventTimeStamp: json['eventTimeStamp'] as String?,
      invoiceSeries: json['invoiceSeries'] as String?,
      transactionTypeName: json['transactionTypeName'] as String?,
      templateCode: json['templateCode'] as String?,
      currencyCode: json['currencyCode'] as String?,
      transType: json['transType'] as int?,
      adjustmentType: json['adjustmentType'] as int?,
      adjustmentTypeName: json['adjustmentTypeName'] as String?,
      invoiceStatus: json['invoiceStatus'] as int?,
      invoiceStatusName: json['invoiceStatusName'] as String?,
      custName: json['custName'] as String?,
      noticeArea: json['noticeArea'] as String?,
      totAmountTax: (json['totAmountTax'] as num?)?.toDouble(),
      taxRate: json['taxRate'] as int?,
      totAmount: (json['totAmount'] as num?)?.toDouble(),
      taxAmount: (json['taxAmount'] as num?)?.toDouble(),
      staDate: json['staDate'] as int?,
      endDate: json['endDate'] as int?,
      contractNo: json['contractNo'] as String?,
      itemName: json['itemName'] as String?,
      transTypeName: json['transTypeName'] as String?,
      strInvoiceIssuedDate: json['strInvoiceIssuedDate'] as String?,
      unitInvoice: json['unitInvoice'] as String?,
      countInvoice: json['countInvoice'] as String?,
      itemTaxAmount: (json['itemTaxAmount'] as num?)?.toDouble(),
      itemTotAmountTax: (json['itemTotAmountTax'] as num?)?.toDouble(),
      itemTotAmount: (json['itemTotAmount'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$InvoiceToJson(Invoice instance) => <String, dynamic>{
      'custInvoiceId': instance.custInvoiceId,
      'custId': instance.custId,
      'contractId': instance.contractId,
      'invoiceNo': instance.invoiceNo,
      'taxRateString': instance.taxRateString,
      'invoiceIssuedDate': instance.invoiceIssuedDate,
      'eventTimeStamp': instance.eventTimeStamp,
      'invoiceSeries': instance.invoiceSeries,
      'transactionTypeName': instance.transactionTypeName,
      'templateCode': instance.templateCode,
      'currencyCode': instance.currencyCode,
      'transType': instance.transType,
      'adjustmentType': instance.adjustmentType,
      'adjustmentTypeName': instance.adjustmentTypeName,
      'invoiceStatus': instance.invoiceStatus,
      'invoiceStatusName': instance.invoiceStatusName,
      'custName': instance.custName,
      'noticeArea': instance.noticeArea,
      'totAmountTax': instance.totAmountTax,
      'taxRate': instance.taxRate,
      'totAmount': instance.totAmount,
      'taxAmount': instance.taxAmount,
      'staDate': instance.staDate,
      'endDate': instance.endDate,
      'contractNo': instance.contractNo,
      'itemName': instance.itemName,
      'transTypeName': instance.transTypeName,
      'strInvoiceIssuedDate': instance.strInvoiceIssuedDate,
      'unitInvoice': instance.unitInvoice,
      'countInvoice': instance.countInvoice,
      'itemTaxAmount': instance.itemTaxAmount,
      'itemTotAmountTax': instance.itemTotAmountTax,
      'itemTotAmount': instance.itemTotAmount,
    };

InvoiceData _$InvoiceDataFromJson(Map<String, dynamic> json) => InvoiceData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => Invoice.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$InvoiceDataToJson(InvoiceData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

InvoiceResponse _$InvoiceResponseFromJson(Map<String, dynamic> json) =>
    InvoiceResponse(
      data: json['data'] == null
          ? null
          : InvoiceData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$InvoiceResponseToJson(InvoiceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
