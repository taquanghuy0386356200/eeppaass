import 'dart:async';

import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/feedback/pages/create_feedback/bloc/create_feedback_otp_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OTPCountdown extends StatefulWidget {
  final DateTime otpSubmittedTime;

  const OTPCountdown({
    Key? key,
    required this.otpSubmittedTime,
  }) : super(key: key);

  @override
  State<OTPCountdown> createState() => _OTPCountdownState();
}

class _OTPCountdownState extends State<OTPCountdown> {
  Timer? timer;
  String countdownTime = '';
  bool countdownFinished = false;

  @override
  void initState() {
    super.initState();
    var count = DateTime.now().difference(widget.otpSubmittedTime).inSeconds;

    timer = Timer.periodic(const Duration(seconds: 1), (_) {
      final diff = 5 * 60 - count;

      count++;

      if (diff > 0) {
        final minutes = (diff / 60).floor();
        final seconds = diff % 60;

        setState(() {
          countdownTime = '${minutes > 0 ? '${minutes}p' : ''}'
              '${seconds.toString().padLeft(2, '0')}s';
          countdownFinished = false;
        });
      } else {
        timer?.cancel();
        setState(() {
          countdownTime = '';
          countdownFinished = true;
        });
      }
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return countdownFinished
        ? Row(
            children: [
              Text(
                'Mã OTP đã hết hạn.',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              SizedBox(width: 4.w),
              TextButton(
                onPressed: () => context.read<CreateFeedbackOtpBloc>().add(const CreateFeedbackOtpRequested()),
                child: Text(
                  'Gửi lại',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: ColorName.linkBlue,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                      ),
                ),
              )
            ],
          )
        : RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Mã OTP sẽ được gửi lại sau ',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                TextSpan(
                  text: countdownTime,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ],
            ),
          );
  }
}
