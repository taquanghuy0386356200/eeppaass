import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:epass/commons/enum/local_authentication_type.dart';
import 'package:epass/commons/models/notification/notification_msg.dart';
import 'package:epass/commons/repo/app_store_repository.dart';
import 'package:epass/commons/repo/campaign_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/services/encryption/encryption.dart';
import 'package:epass/commons/services/local_auth/local_auth_service.dart';
import 'package:epass/commons/services/viettel_money/viettel_money_service.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/first_run_dialog/first_run_dialog_bloc.dart';
import 'package:epass/pages/bloc/profile/avatar_bloc.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/bloc/sms_register/sms_register_bloc.dart';
import 'package:epass/pages/bloc/station_vehicle_alert/station_vehicle_alert_bloc.dart';
import 'package:epass/pages/home/bloc/home_bloc.dart';
import 'package:epass/pages/home/bloc/viettel_money_link_bloc.dart';
import 'package:epass/pages/home/widgets/home_loading_shimmer.dart';
import 'package:epass/pages/home/widgets/home_waiting_order_dialog.dart';
import 'package:epass/pages/home/widgets/home_widgets.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/login/bloc/biometric_login_bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:url_launcher/url_launcher_string.dart';

class HomePage extends StatefulWidget with AutoRouteWrapper {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => HomeBloc(
              userRepository: getIt<IUserRepository>(),
              campaignRepository: getIt<ICampaignRepository>(),
              appBloc: context.read<AppBloc>(),
              iAppStoreRepository: getIt<IAppStoreRepository>()),
        ),
        BlocProvider.value(
          value: context.read<AvatarBloc>(),
        ),
        BlocProvider(
          create: (context) => FirstRunDialogBloc(
            appBloc: getIt<AppBloc>(),
            settingBloc: getIt<SettingBloc>(),
          ),
        ),
        BlocProvider(
          create: (context) => BiometricLoginBloc(
            localAuth: getIt<ILocalAuthService>(),
            secureStorage: getIt<FlutterSecureStorage>(),
            encryption: getIt<IEncryption>(),
          ),
        ),
        BlocProvider(
          create: (context) => ViettelMoneyLinkBloc(
            viettelMoneyService: getIt<ViettelMoneyService>(),
            vehicleRepository: getIt<IVehicleRepository>(),
          ),
        ),
      ],
      child: this,
    );
  }
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    context.read<SmsRegisterBloc>().add(const CheckUserRegister());
    context.read<HomeBloc>().add(HomeFetched());
    context.read<HomeBloc>().add(WaitingOrderFetched());
    context.read<AvatarBloc>().add(const AvatarDownloaded());
    context.read<BiometricLoginBloc>().add(GetBiometricAuthenticationType());
    context.read<StationVehicleAlertBloc>().add(const StationVehicleAlerted());
  }

  @override
  Widget build(BuildContext context) {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      if (message.notification != null) {
        final contentJson = jsonDecode(message.data["data"]);
        if (contentJson["notificationCode"] == "CONFIRM_ROAD_PACKING_PAYMENT") {
          showDialog(
            context: context,
            builder: (dialogContext) {
              return ConfirmDialog(
                title: 'Thông báo xác nhận đỗ xe',
                secondaryButtonTitle: 'Đóng',
                primaryButtonTitle: 'Xem',
                onPrimaryButtonTap: () async {
                  Navigator.of(dialogContext).pop();
                  //navigate to notification
                  AutoTabsRouter.of(context).setActiveIndex(2);
                },
                onSecondaryButtonTap: () async {
                  Navigator.of(dialogContext).pop();
                },
              );
            },
          );
        }
      }
    });
    void checkNoti(Map<String, dynamic> data) {
      AutoTabsRouter.of(context).setActiveIndex(2);
    }

    FirebaseMessaging.onMessageOpenedApp.listen((event) async {
      if (event.data != null) {
        checkNoti(event.data);
      }
    });

    return WillPopScope(
      onWillPop: () async {
        // context.popRoute();
        return false;
      },
      child: BasePage(
        child: Container(
          color: Colors.white,
          child: MultiBlocListener(
            listeners: [
              BlocListener<HomeTabsBloc, HomeTabsState>(
                listener: (homeTabsContext, state) {
                  if (state is HomeTabBarShowed && state.currentTab == 0) {
                    context.read<HomeBloc>().add(WaitingOrderFetched());
                  }
                },
              ),
              BlocListener<BiometricLoginBloc, BiometricLoginState>(
                listener: (context, state) {
                  context.read<FirstRunDialogBloc>().add(FirstRunDialogInit());
                },
              ),
              BlocListener<HomeBloc, HomeState>(
                listener: _popupDialogListener,
              ),
              BlocListener<FirstRunDialogBloc, FirstRunDialogState>(
                listenWhen: (previous, current) {
                  return !(current.biometricLoginDialogShowed ?? false) ||
                      !(current.aliasDialogShowed ?? false);
                },
                listener: (firstRunDialogContext, state) {
                  return _firstRunDialogListener(
                    providerContext: context,
                    firstRunDialogContext: firstRunDialogContext,
                    state: state,
                  );
                },
              ),
            ],
            child: BlocBuilder<HomeBloc, HomeState>(
              buildWhen: (previous, current) {
                return previous.runtimeType != current.runtimeType &&
                    (current is HomeLoading || current is HomeLoaded);
              },
              builder: (context, state) {
                if (state is HomeLoading) {
                  return const HomeLoadingShimmer();
                }
                return const HomeWidgets();
              },
            ),
          ),
        ),
      ),
    );
  }

  void _popupDialogListener(
      BuildContext popupDialogContext, HomeState state) async {
    final bool isRegister = context.read<SmsRegisterBloc>().state.isRegister;

    if (state is HomeLoaded && state.isLoading == true) {
      context.loaderOverlay.show();
    } else {
      context.loaderOverlay.hide();
    }
    if (state is HomeLoaded && state.isConfirmOrderSuccess == true) {
      showSuccessSnackBBar(
        message: 'Xác nhận thành công!',
        context: context,
        duration: const Duration(seconds: 5),
      );
    } else if (state is HomeLoaded && state.isConfirmOrderSuccess == false) {
      showSuccessSnackBBar(
        message: 'Có giao dịch thất bại!',
        context: context,
        duration: const Duration(seconds: 5),
      );
    } else if (state is HomeLoaded &&
        (state.waitingOrders?.isNotEmpty ?? false)) {
      await showDialog(
        context: popupDialogContext,
        barrierDismissible: false,
        builder: (context) {
          return WaitingOrderDialog(
            waitingOrders: state.waitingOrders!,
            onConfirmButtonTap: () async {
              Navigator.of(context).maybePop();
              popupDialogContext
                  .read<HomeBloc>()
                  .add(SendConfirmOrderRequest(state.waitingOrders!));
            },
          );
        },
      );
    } else if (state is HomeLoaded &&
        state.popup?.imagePath != null &&
        state.popup?.isActive == 1 && state.checkShowSpamSMS) {
      final popup = state.popup;
      final imagePath = popup?.imagePath;
      final isUriExternal = state.popup?.isUriExternal == '1';
      final uri = state.popup?.uri;
      if (imagePath != null) {
        await showDialog(
          context: popupDialogContext,
          builder: (context) {
            return Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: Stack(
                  children: [
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: uri != null
                            ? () async {
                                if (isUriExternal) {
                                  await launchUrlString(uri,
                                      mode: LaunchMode.externalApplication);
                                }
                              }
                            : null,
                        child: CachedNetworkImage(
                          width: MediaQuery.of(context).size.width - 32.w,
                          fadeInDuration: const Duration(seconds: 1),
                          placeholder: (context, url) {
                            return const SizedBox.shrink();
                          },
                          imageUrl: imagePath,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      child: Material(
                        color: Colors.transparent,
                        child: IconButton(
                          onPressed: Navigator.of(context).pop,
                          icon: Icon(Icons.cancel_outlined,
                              color: Colors.white, size: 24.r),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      }
    }
    if (state is HomeLoaded &&
        state.popupSMS?.imagePath != null &&
        state.popupSMS?.isActive == 1 &&
        state.checkShowSpamSMS &&
        (!state.checkUserRegisterSMS)) {
      if (state.popupSMS?.imagePath != null) {
        await showDialog(
          context: popupDialogContext,
          builder: (context) {
            return Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: Stack(
                  children: [
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          AutoTabsRouter.of(popupDialogContext)
                              .setActiveIndex(1, notify: true);
                          context.popRoute();
                          context.pushRoute(const SmsRegisterRoute());
                        },
                        child: CachedNetworkImage(
                          width: MediaQuery.of(context).size.width - 32.w,
                          fadeInDuration: const Duration(seconds: 1),
                          placeholder: (context, url) {
                            return const SizedBox.shrink();
                          },
                          imageUrl: state.popupSMS?.imagePath ?? '',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      child: Material(
                        color: Colors.transparent,
                        child: IconButton(
                          onPressed: Navigator.of(context).pop,
                          icon: Icon(Icons.cancel_outlined,
                              color: Colors.white, size: 24.r),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      }
    }
  }

  void _firstRunDialogListener({
    required BuildContext providerContext,
    required BuildContext firstRunDialogContext,
    required FirstRunDialogState state,
  }) async {
    final biometricLoginDialogShowed =
        state.biometricLoginDialogShowed ?? false;
    final aliasDialogShowed = state.aliasDialogShowed ?? false;
    final firstRunDialogBloc = providerContext.read<FirstRunDialogBloc>();

    if (!biometricLoginDialogShowed) {
      LocalAuthenticationType? authenticationType;

      final state = providerContext.read<BiometricLoginBloc>().state;
      if (state is GetBiometricAuthenticationTypeSuccess) {
        authenticationType = state.authenticationType;
      }

      if (authenticationType != null) {
        final isFaceId = authenticationType == LocalAuthenticationType.face;

        await showDialog(
          context: firstRunDialogContext,
          builder: (dialogContext) {
            return ConfirmDialog(
              title: 'Mở khoá bằng'
                  ' ${isFaceId ? 'gương mặt' : 'vân tay'}',
              content: 'Sử dụng ${isFaceId ? 'gương mặt' : 'vân tay'}'
                  ' để mở khoá ứng dụng một cách nhanh chóng'
                  ' và tiện lợi.',
              image: authenticationType?.getIcon(
                height: 54.r,
                width: 54.r,
                color: ColorName.borderColor,
              ),
              secondaryButtonTitle: 'Để sau',
              primaryButtonTitle: 'Bật',
              onPrimaryButtonTap: () async {
                Navigator.of(dialogContext).pop();
                firstRunDialogContext
                    .read<SettingBloc>()
                    .add(const BiometricLoginToggled(true));
              },
              onSecondaryButtonTap: () async {
                Navigator.of(dialogContext).pop();
              },
            );
          },
        );

        firstRunDialogBloc.add(BiometricLoginDialogShowed());
        return;
      }
    }
    if (!aliasDialogShowed) {
      await showDialog(
        context: firstRunDialogContext,
        builder: (dialogContext) {
          return ConfirmDialog(
            title: 'Bí danh đăng nhập',
            content: 'Định nghĩa một tài khoản đăng nhập khác với tài'
                ' khoản được cấp, đăng nhập tiện lợi và dễ'
                ' dàng hơn',
            image: Assets.icons.aliasLine.svg(
              height: 56.r,
              width: 56.r,
              color: ColorName.borderColor,
            ),
            onPrimaryButtonTap: () {
              Navigator.of(dialogContext).pop();
              firstRunDialogContext.pushRoute(const AliasDialogRoute());
            },
          );
        },
      );
      firstRunDialogBloc.add(AliasDialogShowed());
      return;
    }
  }
}
