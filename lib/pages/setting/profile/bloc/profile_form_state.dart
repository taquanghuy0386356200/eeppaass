part of 'profile_form_bloc.dart';

abstract class ProfileFormState extends Equatable {
  const ProfileFormState();
}

class ProfileFormInitial extends ProfileFormState {
  @override
  List<Object> get props => [];
}

class ProfileFormSubmittedInProgress extends ProfileFormState {
  const ProfileFormSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class ProfileFormSubmittedSuccess extends ProfileFormState {
  const ProfileFormSubmittedSuccess();

  @override
  List<Object> get props => [];
}

class ProfileFormSubmittedFailure extends ProfileFormState {
  final String message;

  const ProfileFormSubmittedFailure(this.message);

  @override
  List<Object> get props => [message];
}
