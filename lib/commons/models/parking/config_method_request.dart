
import 'package:json_annotation/json_annotation.dart';

part 'config_method_request.g.dart';

@JsonSerializable()
class ConfigMethodVerifyRequest {
  int serviceId;
  int verifyType;
  int actionType;
  String passCode;
  String otp;

  ConfigMethodVerifyRequest({
    required this.serviceId,
    required this.verifyType,
    required this.actionType,
    required this.passCode,
    required this.otp
  });

  Map<String, dynamic> toJson() => _$ConfigMethodVerifyRequestToJson(this);
}