// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'topup_condition.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopUpCondition _$TopUpConditionFromJson(Map<String, dynamic> json) =>
    TopUpCondition(
      fee: json['fee'] as int?,
      value: json['value'] as int?,
    );

Map<String, dynamic> _$TopUpConditionToJson(TopUpCondition instance) =>
    <String, dynamic>{
      'fee': instance.fee,
      'value': instance.value,
    };

TopUpConditionResponse _$TopUpConditionResponseFromJson(
        Map<String, dynamic> json) =>
    TopUpConditionResponse(
      data: json['data'] == null
          ? null
          : TopUpCondition.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TopUpConditionResponseToJson(
        TopUpConditionResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
