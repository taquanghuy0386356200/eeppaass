import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auto-topup.g.dart';

@JsonSerializable()
class AutoTopupResponse {
  @JsonKey(name: 'data')
  final AutoTopupRequest? data;
  final MessageData? mess;

  AutoTopupResponse({this.data, this.mess});

  factory AutoTopupResponse.fromJson(Map<String, dynamic> json) =>
      _$AutoTopupResponseFromJson(json);
}

@JsonSerializable()
class AutoTopupRequest {
  final int? autoConfigTopupId;
  final int? contractId;
  final int? amount;
  final int? limit;
  final int? topupType;
  final String? topupChannel;
  final int? isActive;
  final String? accountNumber;
  final String? accountName;
  final String? documentNumber;

  AutoTopupRequest({
    this.autoConfigTopupId,
    this.contractId,
    this.amount,
    this.limit,
    this.topupType,
    this.topupChannel,
    this.isActive,
    this.accountNumber,
    this.accountName,
    this.documentNumber,
  });

  factory AutoTopupRequest.fromJson(Map<String, dynamic> json) =>
      _$AutoTopupRequestFromJson(json);

  Map<String, dynamic> toJson() => _$AutoTopupRequestToJson(this);
}
