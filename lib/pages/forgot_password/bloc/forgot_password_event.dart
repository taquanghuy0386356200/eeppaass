part of 'forgot_password_bloc.dart';

abstract class ForgotPasswordEvent extends Equatable {
  const ForgotPasswordEvent();
}

class ForgotPasswordSubmitted extends ForgotPasswordEvent {
  final String plateNumber;
  final String plateTypeCode;
  final String phoneNumber;

  const ForgotPasswordSubmitted({
    required this.plateNumber,
    required this.plateTypeCode,
    required this.phoneNumber,
  });

  @override
  List<Object> get props => [plateNumber, plateTypeCode, phoneNumber];
}
