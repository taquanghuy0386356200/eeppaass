import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'car_doctor_config_response.g.dart';

@JsonSerializable()
class CarDoctorConfig {
  CarDoctorConfig({
    this.name,
    this.code,
    this.id,
    this.description,
  });

  final String? name;
  final String? code;
  final String? id;
  final String? description;

  factory CarDoctorConfig.fromJson(Map<String, dynamic> json) =>
      _$CarDoctorConfigFromJson(json);
}

@JsonSerializable()
class CarDoctorConfigData {
  CarDoctorConfigData({this.listData});

  final List<CarDoctorConfig>? listData;

  factory CarDoctorConfigData.fromJson(Map<String, dynamic> json) =>
      _$CarDoctorConfigDataFromJson(json);
}

@JsonSerializable()
class CarDoctorConfigResponse {
  final CarDoctorConfigData data;
  final MessageData? mess;

  CarDoctorConfigResponse({required this.data, this.mess});

  factory CarDoctorConfigResponse.fromJson(Map<String, dynamic> json) =>
      _$CarDoctorConfigResponseFromJson(json);
}
