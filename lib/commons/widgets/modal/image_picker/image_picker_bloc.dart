import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/extensions/platform_ext.dart';
import 'package:epass/commons/services/permission/permission.dart';
import 'package:equatable/equatable.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart' as p;

part 'image_picker_event.dart';

part 'image_picker_state.dart';

class ImagePickerBloc extends Bloc<ImagePickerEvent, ImagePickerState> {
  final ImagePicker _imagePicker;

  ImagePickerBloc({required ImagePicker imagePicker})
      : _imagePicker = imagePicker,
        super(ImagePickerInitial()) {
    on<ImagePicked>(_onImagePicked);
  }

  FutureOr<void> _onImagePicked(ImagePicked event, emit) async {
    emit(const ImagePickedInProgress());

    Permission permission;
    if (event.source == ImageSource.camera) {
      permission = Permission.camera;
    } else {
      final sdkVersion = await PlatformExt().getSdkVersion();
      if (sdkVersion != null && sdkVersion <= 32.0) {
        permission = Permission.storage;
      } else {
        permission = Permission.photos;
      }
    }

    final permissionStatus = await requestPermission(permission);

    await permissionStatus.when(
      success: (_) async {
        final image = await _imagePicker.pickImage(
          source: event.source,
          maxWidth: 200,
        );

        if (image == null) {
          emit(const ImagePickedCanceled());
          return;
        }

        // Check file extension
        if (event.source == ImageSource.gallery) {
          final fileExtension = p.extension(image.path).replaceAll('.', '');
          if (!event.allowedExtension.contains(fileExtension)) {
            final extStr = event.allowedExtension
                .reduce((value, element) => '$value, $element');
            emit(ImagePickedFailure('Chỉ hỗ trợ định dạng ảnh: $extStr'));
            return;
          }
        }

        emit(ImagePickedSuccess(filePath: image.path));
      },
      failure: (failure) =>
          emit(ImagePickedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
