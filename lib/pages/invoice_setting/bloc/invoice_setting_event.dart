part of 'invoice_setting_bloc.dart';

abstract class InvoiceSettingEvent extends Equatable {
  const InvoiceSettingEvent();
}

class InvoiceSettingCycleUpdated extends InvoiceSettingEvent {
  final int billCycle;
  final int? billCycleMergeType;

  const InvoiceSettingCycleUpdated({
    required this.billCycle,
    this.billCycleMergeType,
  });

  @override
  List<Object?> get props => [billCycle, billCycleMergeType];
}
