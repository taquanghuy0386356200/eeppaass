import 'package:epass/commons/models/feedback/feedback_attachment_file.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AttachmentFilesWidget extends StatelessWidget {
  final List<FeedbackAttachmentFileResponse> attachmentFiles;
  final void Function(String fileName, int fileId)? onAttachmentTap;

  const AttachmentFilesWidget({
    Key? key,
    required this.attachmentFiles,
    this.onAttachmentTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Tệp đính kèm',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.w700,
                ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ConstrainedBox(
            constraints: const BoxConstraints.tightForFinite(),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              for (int i = 0; i < attachmentFiles.length; i++)
                GestureDetector(
                  onTap: () {
                    final fileId = attachmentFiles[i].attachmentId;
                    final fileName = attachmentFiles[i].fileName;
                    if (fileId != null && fileName != null) {
                      onAttachmentTap?.call(fileName, fileId);
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.only(
                      bottom: attachmentFiles.length == 1
                          ? 0
                          : i == attachmentFiles.length - 1
                              ? 0
                              : 8.h,
                    ),
                    child: Text(
                      attachmentFiles[i].fileName ?? '',
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: ColorName.linkBlue,
                          decoration: TextDecoration.underline),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                )
            ]),
          ),
        ),
      ],
    );
  }
}
