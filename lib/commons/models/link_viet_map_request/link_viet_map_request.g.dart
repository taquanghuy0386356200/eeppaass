// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'link_viet_map_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LinkVietMapRequest _$LinkVietMapRequestFromJson(Map<String, dynamic> json) =>
    LinkVietMapRequest(
      phoneNumber: json['phoneNumber'] as String?,
      accountName: json['accountName'] as String?,
      token: json['token'] as String?,
    );

Map<String, dynamic> _$LinkVietMapRequestToJson(LinkVietMapRequest instance) =>
    <String, dynamic>{
      'phoneNumber': instance.phoneNumber,
      'accountName': instance.accountName,
      'token': instance.token,
    };
