import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/commons/services/viettel_money/viettel_money_service.dart';
import 'package:equatable/equatable.dart';

part 'viettel_money_link_event.dart';

part 'viettel_money_link_state.dart';

class ViettelMoneyLinkBloc extends Bloc<ViettelMoneyLinkEvent, ViettelMoneyLinkState> {
  final ViettelMoneyService _viettelMoneyService;
  final IVehicleRepository _vehicleRepository;

  ViettelMoneyLinkBloc({
    required ViettelMoneyService viettelMoneyService,
    required IVehicleRepository vehicleRepository,
  })  : _vehicleRepository = vehicleRepository,
        _viettelMoneyService = viettelMoneyService,
        super(ViettelMoneyLinkInitial()) {
    on<ViettelMoneyLinkAccount>(_onViettelMoneyLinkAccount);
    on<ViettelMoneyUnlinkAccount>(_onViettelMoneyUnlinkAccount);
  }

  FutureOr<void> _onViettelMoneyLinkAccount(
    ViettelMoneyLinkAccount event,
    emit,
  ) async {
    emit(ViettelMoneyLinkAccountInProgress());

    final vehicleResult = await _vehicleRepository.getUserVehicles(
      statuses: const [1],
      activeStatuses: const [1, 4],
    );

    await vehicleResult.when(
      success: (success) async {
        // lấy list data danh sách xe không có debit
        final listVehicelData = success.listData.map((vehicle) {
          return Vehicle(
            vehicleId: vehicle.vehicleId,
            contractId: vehicle.contractId,
            plateNumber: vehicle.plateNumber,
            vehicleTypeId: vehicle.vehicleTypeId,
            vehicleGroupId: vehicle.vehicleGroupId,
            vehicleGroupName: vehicle.vehicleGroupName,
            vehicleGroupDescription: vehicle.vehicleGroupDescription,
            seatNumber: vehicle.seatNumber,
            netWeight: vehicle.netWeight,
            cargoWeight: vehicle.cargoWeight,
            status: vehicle.status,
            activeStatus: vehicle.activeStatus,
            epc: vehicle.epc,
            rfidSerial: vehicle.rfidSerial,
            vehicleTypeCode: vehicle.vehicleTypeCode,
            plateTypeCode: vehicle.plateTypeCode,
          );
        }).toList();

        final linkResult = await _viettelMoneyService.linkAccount(
          contractId: event.contractId,
          contractNo: event.contractNo,
          contractFullName: event.contractFullName,
          vehicles: listVehicelData,
        );

        linkResult.when(
          success: (_) => emit(ViettelMoneyLinkAccountSuccess()),
          failure: (failure) => emit(ViettelMoneyLinkAccountFailure(failure.message ?? 'Có lỗi xảy ra')),
        );
      },
      failure: (failure) => emit(ViettelMoneyLinkAccountFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onViettelMoneyUnlinkAccount(
    ViettelMoneyUnlinkAccount event,
    Emitter<ViettelMoneyLinkState> emit,
  ) async {
    emit(ViettelMoneyUnlinkAccountInProgress());

    final result = await _viettelMoneyService.unlinkAccount(
      token: event.token,
      contractId: event.contractId,
      accountNumber: event.accountNumber,
    );

    result.when(
      success: (success) => emit(ViettelMoneyUnlinkAccountSuccess()),
      failure: (failure) => emit(ViettelMoneyUnlinkAccountFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
