import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/primary_checkbox.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/setting/notification_setting/bloc/sms_vehicle_bloc/sms_vehicle_bloc.dart';
import 'package:epass/pages/setting/notification_setting/bloc/sms_vehicle_bloc/sms_vehicle_category_bloc.dart';
import 'package:epass/pages/setting/notification_setting/widgets/sms_vehicle_dialog_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SMSVehicleDialog extends StatefulWidget {
  final CategoryModel? selectedSMSOption;
  final bool? autoRenew;

  const SMSVehicleDialog({Key? key, this.selectedSMSOption, this.autoRenew}) : super(key: key);

  @override
  State<SMSVehicleDialog> createState() => _SMSVehicleDialogState();
}

class _SMSVehicleDialogState extends State<SMSVehicleDialog> {
  // CategoryModel? selectedSMSOption; không thấy có giá trị truyền sang để sử dụng được sử dụng
  bool autoRenew = true;

  @override
  void initState() {
    super.initState();
    final customerTypeId = context.read<AppBloc>().state.user?.cusTypeId;
    final isPersonal = customerTypeId == 1 || customerTypeId == 7; // 1: domestic, 7: abroad
    context.read<SMSVehicleCategoryBloc>().add(SMSVehicleCategoryFetched(isPersonal: isPersonal));

    // selectedSMSOption = widget.selectedSMSOption;
    autoRenew = widget.autoRenew ?? true;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 16.w),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 32.h),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 12.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Text(
                  'Bạn có chắc chắn muốn đăng ký dịch vụ SMS',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: ColorName.textGray2,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
              SizedBox(height: 24.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.w),
                child: BlocBuilder<SMSVehicleCategoryBloc, SMSVehicleCategoryState>(
                  builder: (context, state) {
                    if (state is SMSVehicleCategoryFetchedInProgress) {
                      return const SMSVehicleDialogShimmerLoading();
                    } else if ((state is SMSVehicleCategoryFetchedSuccess &&
                        (state.smsCategories ?? []).isNotEmpty)) {
                      return Column(
                       children: [
                         RadioGroupButtons<CategoryModel>(
                           defaultValue: state.selectedSMSOption,
                           values: state.smsCategories ?? [],
                           labels: (state.smsCategories ?? [])
                               .map((e) => '${e.name?.trim()} - ${e.description}')
                               .toList(),
                           onChanged: (value) {
                             context
                                 .read<SMSVehicleCategoryBloc>()
                                 .add(SelectedSMSOption(selectedSMSOption: value, smsCategories:state.smsCategories ));
                           },
                         ),
                       ],
                      );
                    } else {
                      return const SizedBox.shrink();
                    }
                  },
                ),
              ),
              SizedBox(height: 4.h),
              Row(
                children: [
                  PrimaryCheckbox(
                    isCheck: autoRenew,
                    onChanged: (isCheck) => setState(() => autoRenew = isCheck ?? true),
                  ),
                  Material(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(8.0),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(8.0),
                      onTap: () => setState(() => autoRenew = !autoRenew),
                      child: Padding(
                        padding: EdgeInsets.all(8.r),
                        child: Text(
                          'Gia hạn tự động',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 24.h),
              Row(
                children: [
                  Expanded(
                    child: SizedBox(
                      height: 50.h,
                      child: SecondaryButton(
                        onTap: () => Navigator.of(context).pop(),
                      ),
                    ),
                  ),
                  SizedBox(width: 20.w),
                  Expanded(
                    child: SizedBox(
                      height: 50.h,
                      child: BlocBuilder<SMSVehicleCategoryBloc, SMSVehicleCategoryState>(
                        builder: (context, state) {
                          if (state is SMSVehicleCategoryFetchedInProgress) {
                            return const SMSVehicleDialogShimmerLoading();
                          } else if ((state is SMSVehicleCategoryFetchedSuccess &&
                              (state.smsCategories ?? []).isNotEmpty)) {
                            return PrimaryButton(
                              enabled: state.selectedSMSOption != null,
                              onTap: state.selectedSMSOption != null
                                  ? () {
                                Navigator.of(context).pop();
                                final code = state.selectedSMSOption?.code;
                                if (code != null) {
                                  context.read<SMSVehicleBloc>().add(
                                    SMSVehicleRegistered(
                                      smsRegisCode: code,
                                      autoRenew: autoRenew,
                                    ),
                                  );
                                }
                              }
                                  : null,
                              title: 'Đồng ý',
                              padding: EdgeInsets.zero,
                            );
                          } else {
                            return const SizedBox.shrink();
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
