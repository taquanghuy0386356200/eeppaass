part of 'insurance_viettelpay_bloc.dart';

@immutable
abstract class InsuranceViettelPayEvent extends Equatable {
  const InsuranceViettelPayEvent();
}

class CountInsuranceFetched extends InsuranceViettelPayEvent {
  const CountInsuranceFetched();

  @override
  List<Object> get props => [];
}
