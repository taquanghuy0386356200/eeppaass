// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_ticket_purchase.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionTicketPurchase _$TransactionTicketPurchaseFromJson(
        Map<String, dynamic> json) =>
    TransactionTicketPurchase(
      stationId: json['stationId'] as int?,
      stageId: json['stageId'] as int?,
      stage: json['stage'] as String?,
      station: json['station'] as String?,
      servicePlanTypeId: $enumDecodeNullable(
          _$ServicePlanTypeEnumMap, json['servicePlanTypeId'],
          unknownValue: ServicePlanType.other),
      servicePlanTypeName: json['servicePlanTypeName'] as String?,
      price: json['price'] as int?,
      status: json['status'] as int?,
      saleTransDate: json['saleTransDate'] as String?,
      dateOfUse: json['dateOfUse'] as String?,
      effDate: json['effDate'] as String?,
      expDate: json['expDate'] as String?,
      autoRenew: json['autoRenew'] as String?,
      efficiency: json['efficiency'] as int?,
      plateNumber: json['plateNumber'] as String?,
      chargeMethodName: json['chargeMethodName'] as String?,
      saleTransCode: json['saleTransCode'] as String?,
      methodCharge: json['methodCharge'] as String?,
      createUser: json['createUser'] as String?,
      vehicleGroupName: json['vehicleGroupName'] as String?,
      vehicleGroupId: json['vehicleGroupId'] as int?,
    );

Map<String, dynamic> _$TransactionTicketPurchaseToJson(
        TransactionTicketPurchase instance) =>
    <String, dynamic>{
      'stationId': instance.stationId,
      'stageId': instance.stageId,
      'stage': instance.stage,
      'station': instance.station,
      'servicePlanTypeId': _$ServicePlanTypeEnumMap[instance.servicePlanTypeId],
      'servicePlanTypeName': instance.servicePlanTypeName,
      'price': instance.price,
      'status': instance.status,
      'saleTransDate': instance.saleTransDate,
      'dateOfUse': instance.dateOfUse,
      'effDate': instance.effDate,
      'expDate': instance.expDate,
      'autoRenew': instance.autoRenew,
      'efficiency': instance.efficiency,
      'plateNumber': instance.plateNumber,
      'chargeMethodName': instance.chargeMethodName,
      'saleTransCode': instance.saleTransCode,
      'methodCharge': instance.methodCharge,
      'createUser': instance.createUser,
      'vehicleGroupName': instance.vehicleGroupName,
      'vehicleGroupId': instance.vehicleGroupId,
    };

const _$ServicePlanTypeEnumMap = {
  ServicePlanType.daily: 1,
  ServicePlanType.monthly: 4,
  ServicePlanType.quarterly: 5,
  ServicePlanType.other: 'other',
};

TransactionTicketPurchaseData _$TransactionTicketPurchaseDataFromJson(
        Map<String, dynamic> json) =>
    TransactionTicketPurchaseData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) =>
              TransactionTicketPurchase.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$TransactionTicketPurchaseDataToJson(
        TransactionTicketPurchaseData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

TransactionTicketPurchaseResponse _$TransactionTicketPurchaseResponseFromJson(
        Map<String, dynamic> json) =>
    TransactionTicketPurchaseResponse(
      data: json['data'] == null
          ? null
          : TransactionTicketPurchaseData.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TransactionTicketPurchaseResponseToJson(
        TransactionTicketPurchaseResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
