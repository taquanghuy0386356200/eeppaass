part of 'biometric_login_bloc.dart';

@immutable
abstract class BiometricLoginState extends Equatable {
  const BiometricLoginState();
}

class BiometricLoginInitial extends BiometricLoginState {
  @override
  List<Object> get props => [];
}

class GetBiometricAuthenticationTypeSuccess extends BiometricLoginState {
  final LocalAuthenticationType authenticationType;

  const GetBiometricAuthenticationTypeSuccess(this.authenticationType);

  @override
  List<Object> get props => [authenticationType];
}

class GetBiometricAuthenticationTypeFailure extends BiometricLoginState {
  final String message;

  const GetBiometricAuthenticationTypeFailure(this.message);

  @override
  List<Object> get props => [message];
}

class BiometricAuthenticatedSuccess extends BiometricLoginState {
  final String username;
  final String password;

  const BiometricAuthenticatedSuccess({
    required this.username,
    required this.password,
  });

  @override
  List<Object> get props => [username, password];
}

class BiometricAuthenticatedFailure extends BiometricLoginState {
  final String message;

  const BiometricAuthenticatedFailure(this.message);

  @override
  List<Object> get props => [message];
}
