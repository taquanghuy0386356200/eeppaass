part of 'login_form_bloc.dart';

@immutable
abstract class LoginFormEvent extends Equatable {
  const LoginFormEvent();
}

class LoginFormSubmittedEvent extends LoginFormEvent {
  final String username;
  final String password;

  const LoginFormSubmittedEvent(this.username, this.password) : super();

  @override
  List<Object?> get props => [username, password];
}

class LoginFormContractNoFetched extends LoginFormEvent {
  const LoginFormContractNoFetched();

  @override
  List<Object> get props => [];
}
