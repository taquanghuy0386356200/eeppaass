import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment_request.dart';
import 'package:equatable/equatable.dart';

part 'vnpay_event.dart';

part 'vnpay_state.dart';

class VNPayBloc extends Bloc<VNPayEvent, VNPayState> {
  final VNPayService _vnpayService;

  VNPayBloc({required VNPayService vnpayService})
      : _vnpayService = vnpayService,
        super(VNPayInitial()) {
    on<VNPayPaymentProceeded>(_onVNPayPayment);
  }

  FutureOr<void> _onVNPayPayment(
    VNPayPaymentProceeded event,
    Emitter<VNPayState> emit,
  ) async {
    final vnpayRequest = VNPayRequest(
      vnpTxnRef: event.orderId,
      vnpAmount: event.amount,
      vnpIpAddr: event.ipAddress,
      vnpOrderInfo: 'Nap tien ung dung ePass cho hop dong ${event.contractNo}',
    );

    final result = await _vnpayService.pushPayment(vnpayRequest);

    result.when(
      success: (data) => emit(VNPayPaymentSuccess(
        orderId: data.vnpTxnRef ?? event.orderId,
        amount: data.vnpAmount ?? event.amount,
      )),
      failure: (failure) => emit(
        VNPayPaymentFailure(failure.message ?? ''),
      ),
    );
  }
}
