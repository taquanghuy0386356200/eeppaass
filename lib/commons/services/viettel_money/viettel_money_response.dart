enum ViettelMoneyStatus {
  success,
  failure,
}

class ViettelMoneyResponse {
  final int? status;
  final String? message;

  ViettelMoneyStatus get resultStatus {
    return status != null && status == 1 ? ViettelMoneyStatus.success : ViettelMoneyStatus.failure;
  }

  ViettelMoneyResponse({this.status, this.message});

  factory ViettelMoneyResponse.fromMap(Map<String, dynamic> map) {
    return ViettelMoneyResponse(
      status: map['status'] != null ? map['status'] as int? : null,
      message: map['message'] != null ? map['message'] as String? : null,
    );
  }
}
