part of 'balance_bloc.dart';

@immutable
abstract class BalanceEvent extends Equatable {
  const BalanceEvent();

  @override
  List<Object> get props => [];
}

class BalanceToggled extends BalanceEvent {}

class BalanceFetched extends BalanceEvent {}

class BalanceViettelMoneyToggled extends BalanceEvent {}
