import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'feedback_type_event.dart';

part 'feedback_type_state.dart';

class FeedbackTypeBloc extends Bloc<FeedbackTypeEvent, FeedbackTypeState> {
  final IFeedbackRepository _feedbackRepository;

  FeedbackTypeBloc({
    required IFeedbackRepository feedbackRepository,
  })  : _feedbackRepository = feedbackRepository,
        super(FeedbackTypeInitial()) {
    on<FeedbackTypeFetched>((event, emit) async {
      emit(const FeedbackTypeFetchedInProgress());

      final result = await _feedbackRepository.getFeedbackTypes(
        feedbackChannel: event.feedbackChannel,
        parentId: event.parentId,
      );

      result.when(
        success: (success) => emit(FeedbackTypeFetchedSuccess(feedbackTypes: success.listData)),
        failure: (failure) => emit(FeedbackTypeFetchedFailure(message: failure.message ?? 'Có lỗi xảy ra')),
      );
    });
  }
}
