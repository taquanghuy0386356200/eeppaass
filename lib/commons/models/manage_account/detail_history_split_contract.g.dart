// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_history_split_contract.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailHistorySplitContractTotal _$DetailHistorySplitContractTotalFromJson(
        Map<String, dynamic> json) =>
    DetailHistorySplitContractTotal(
      data: json['data'] == null
          ? null
          : SplitContractResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DetailHistorySplitContractTotalToJson(
        DetailHistorySplitContractTotal instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
