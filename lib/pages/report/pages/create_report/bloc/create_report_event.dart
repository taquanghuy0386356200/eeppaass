part of 'create_report_bloc.dart';

abstract class CreateReportEvent extends Equatable {
  const CreateReportEvent();
}

class CreateReportFormSubmitted extends CreateReportEvent {
  final String reporterName;
  final String? email;
  final String? plateNumber;
  final String? plateTypeCode;
  final String? location;
  final String? priorityId;
  final String l1TicketTypeId;
  final String l2TicketTypeId;
  final String? l3TicketTypeId;
  final String provinceName;
  final String? districtName;
  final String? communeName;
  final String areaCode;
  final String? stationId;
  final String? stationName;
  final String? stageId;
  final String? stageName;
  final String contentReceive;
  final String? ticketKind;
  // final String requestDate;
  final String? supportInfo;
  final String phoneContact;
  final List<String> filePaths;
  final String otp;

  const CreateReportFormSubmitted({
    required this.reporterName,
    this.email,
    this.plateNumber,
    this.plateTypeCode,
    this.location,
    this.priorityId,
    required this.l1TicketTypeId,
    required this.l2TicketTypeId,
    this.l3TicketTypeId,
    required this.provinceName,
    this.districtName,
    this.communeName,
    required this.areaCode,
    this.stationId,
    this.stationName,
    this.stageId,
    this.stageName,
    required this.contentReceive,
    this.ticketKind,
    this.supportInfo,
    required this.phoneContact,
    this.filePaths = const [],
    required this.otp,
  });

  @override
  List<Object> get props => [
    reporterName,
    email!,
    plateNumber!,
    plateTypeCode!,
    location!,
    priorityId!,
    l1TicketTypeId,
    l2TicketTypeId,
    l3TicketTypeId!,
    provinceName,
    districtName!,
    communeName!,
    areaCode,
    stationId!,
    stationName!,
    stageId!,
    stageName!,
    contentReceive,
    ticketKind!,
    supportInfo!,
    phoneContact,
    filePaths,
    otp
  ];
}
