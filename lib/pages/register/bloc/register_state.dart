part of 'register_bloc.dart';

@immutable
abstract class RegisterState extends Equatable {
  const RegisterState();
}

class RegisterInitial extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterSubmittedInProgress extends RegisterState {
  const RegisterSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class RegisterSubmittedFailure extends RegisterState {
  final String message;

  const RegisterSubmittedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class RegisterSubmittedSuccess extends RegisterState {
  const RegisterSubmittedSuccess();

  @override
  List<Object> get props => [];
}
