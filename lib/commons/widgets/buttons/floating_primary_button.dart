import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';

class FloatingPrimaryButton extends StatelessWidget {
  final VoidCallback? onTap;

  const FloatingPrimaryButton({
    Key? key,
     this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      elevation: 8,
      onPressed: onTap,
      child: Container(
        width: 56,
        height: 56,
        padding: const EdgeInsets.all(12),
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(
            colors: [
              ColorName.primaryGradientStart,
              ColorName.primaryGradientEnd,
            ],
          ),
        ),
        child: Assets.icons.search.svg(),
      ),
    );
  }
}
