import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/cash_in/bloc/cash_in_bloc.dart';
import 'package:epass/pages/cash_in/pages/cash_in_confirm/cash_in_confirm_builder.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_amount_bloc.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/bloc/cash_in_target_bloc.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/bloc/payment_methods_card_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CashInConfirmPage extends StatelessWidget {
  const CashInConfirmPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      backgroundColor: Colors.white,
      title: 'Xác nhận giao dịch',
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: RoundedTopContainer(
                margin: EdgeInsets.only(top: 16.h),
                padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                child: Column(
                  children: [
                    ShadowCard(
                      shadowOpacity: 0.2,
                      padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 20.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          // THONG TIN GIAO DICH
                          Row(
                            children: [
                              Assets.icons.moneyTransfer.svg(
                                height: 24.r,
                                width: 24.r,
                              ),
                              SizedBox(width: 8.w),
                              Text(
                                'Thông tin giao dịch',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .copyWith(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.sp,
                                    ),
                              )
                            ],
                          ),
                          SizedBox(height: 24.h),
                          // SO HOP DONG
                          Row(
                            children: [
                              Text(
                                'Số hợp đồng',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: ColorName.borderColor),
                              ),
                              const Spacer(),
                              BlocSelector<CashInTargetBloc, CashInTargetState,
                                  String?>(
                                selector: (state) => state.contractNo,
                                builder: (context, contractNo) {
                                  return Text(
                                    contractNo ?? '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(fontWeight: FontWeight.w600),
                                  );
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: 10.h),
                          Row(
                            children: [
                              Text(
                                'Phương thức',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: ColorName.borderColor),
                              ),
                              const Spacer(),
                              BlocSelector<PaymentMethodsCardBloc,
                                  PaymentMethodsCardState, String?>(
                                selector: (state) =>
                                    state is PaymentMethodsFetchedSuccess
                                        ? state.selectedPaymentMethod?.name
                                        : null,
                                builder: (context, paymentMethodName) {
                                  return Text(
                                    paymentMethodName ?? '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(fontWeight: FontWeight.w600),
                                  );
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: 10.h),
                          Row(
                            children: [
                              Text(
                                'Số tiền',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: ColorName.borderColor),
                              ),
                              const Spacer(),
                              BlocSelector<CashInAmountBloc, CashInAmountState,
                                  int?>(
                                selector: (state) =>
                                    state is CashInAmountChangedSuccess
                                        ? state.amount
                                        : null,
                                builder: (context, amount) {
                                  return Text(
                                    amount != null
                                        ? amount.formatCurrency(symbol: 'đ')
                                        : '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6!
                                        .copyWith(
                                          fontWeight: FontWeight.bold,
                                        ),
                                  );
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: 10.h),
                          Row(
                            children: [
                              Text(
                                'Phí giao dịch',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: ColorName.borderColor),
                              ),
                              const Spacer(),
                              BlocSelector<CashInBloc, CashInState, int?>(
                                selector: (state) =>
                                    state is CashInFetchedTopupFeeSuccess
                                        ? state.fee.fee
                                        : null,
                                builder: (context, fee) {
                                  return Text(
                                    fee != null
                                        ? fee.formatCurrency(symbol: 'đ')
                                        : '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(fontWeight: FontWeight.w600),
                                  );
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 32.h,
            left: 16.w,
            right: 16.w,
            child: const CashInConfirmBuilder(),
          ),
        ],
      ),
    );
  }
}
