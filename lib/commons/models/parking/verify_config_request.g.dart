// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verify_config_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerifyConfigRequest _$VerifyConfigRequestFromJson(Map<String, dynamic> json) =>
    VerifyConfigRequest(
      listServiceId: (json['listServiceId'] as List<dynamic>?)
          ?.map((e) => e as int)
          .toList(),
    );

Map<String, dynamic> _$VerifyConfigRequestToJson(
        VerifyConfigRequest instance) =>
    <String, dynamic>{
      'listServiceId': instance.listServiceId,
    };
