import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'campaign.g.dart';

@JsonEnum(alwaysCreate: true)
enum CampaignType {
  @JsonValue(1)
  popup,
  @JsonValue(2)
  banner,
  @JsonValue(3)
  popupSMS,
  @JsonValue(99)
  unknown,
}

@JsonSerializable()
class Campaign extends Equatable {
  const Campaign({
    this.campaignId,
    this.name,
    this.imagePath,
    this.slogan,
    this.isUriExternal,
    this.uri,
    this.isActive,
    this.note,
    this.createUser,
    this.createDate,
    this.type,
  });

  final int? campaignId;
  final String? name;
  final String? imagePath;
  final String? slogan;
  final String? isUriExternal;
  final String? uri;
  final int? isActive;
  final String? note;
  final String? createUser;
  final int? createDate;

  @JsonKey(unknownEnumValue: CampaignType.unknown)
  final CampaignType? type;

  factory Campaign.fromJson(Map<String, dynamic> json) =>
      _$CampaignFromJson(json);

  Map<String, dynamic> toJson() => _$CampaignToJson(this);

  @override
  List<Object?> get props => [campaignId];

  @override
  bool? get stringify => true;
}

@JsonSerializable()
class CampaignData {
  final List<Campaign>? listData;
  final int? count;

  CampaignData({
    this.listData,
    this.count,
  });

  factory CampaignData.fromJson(Map<String, dynamic> json) =>
      _$CampaignDataFromJson(json);
}

@JsonSerializable()
class CampaignResponse {
  @JsonKey(name: 'data')
  final CampaignData? campaignData;
  final MessageData? mess;

  CampaignResponse({
    this.campaignData,
    this.mess,
  });

  factory CampaignResponse.fromJson(Map<String, dynamic> json) =>
      _$CampaignResponseFromJson(json);
}
