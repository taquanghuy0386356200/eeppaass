// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'area_address_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AreaAddressClientTotalResponse _$AreaAddressClientTotalResponseFromJson(
        Map<String, dynamic> json) =>
    AreaAddressClientTotalResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) =>
              AreaAddressClientResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AreaAddressClientTotalResponseToJson(
        AreaAddressClientTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

AreaAddressClientResponse _$AreaAddressClientResponseFromJson(
        Map<String, dynamic> json) =>
    AreaAddressClientResponse(
      area_code: json['area_code'] as String?,
      province: json['province'] as String?,
      name: json['name'] as String?,
      district: json['district'] as String?,
      precinct: json['precinct'] as String?,
      provinceName: json['provinceName'] as String?,
      districtName: json['districtName'] as String?,
      precinctName: json['precinctName'] as String?,
      fullName: json['fullName'] as String?,
      districtShop: json['districtShop'] as String?,
      provinceShop: json['provinceShop'] as String?,
      idValueDropdown: json['idValueDropdown'] as String?,
      nameValueDropdown: json['nameValueDropdown'] as String?,
    );

Map<String, dynamic> _$AreaAddressClientResponseToJson(
        AreaAddressClientResponse instance) =>
    <String, dynamic>{
      'area_code': instance.area_code,
      'province': instance.province,
      'name': instance.name,
      'district': instance.district,
      'precinct': instance.precinct,
      'provinceName': instance.provinceName,
      'districtName': instance.districtName,
      'precinctName': instance.precinctName,
      'fullName': instance.fullName,
      'districtShop': instance.districtShop,
      'provinceShop': instance.provinceShop,
      'idValueDropdown': instance.idValueDropdown,
      'nameValueDropdown': instance.nameValueDropdown,
    };
