// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_topup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractTopup _$ContractTopupFromJson(Map<String, dynamic> json) =>
    ContractTopup(
      topupEtcId: json['topupEtcId'] as int?,
      topupDate: json['topupDate'] as String?,
      amount: json['amount'] as int?,
      topupType: $enumDecodeNullable(_$TopupTypeEnumMap, json['topupType'],
          unknownValue: TopupType.other),
      topupCode: json['topupCode'] as String?,
      status: json['status'] as String?,
    );

Map<String, dynamic> _$ContractTopupToJson(ContractTopup instance) =>
    <String, dynamic>{
      'topupEtcId': instance.topupEtcId,
      'topupDate': instance.topupDate,
      'amount': instance.amount,
      'topupType': _$TopupTypeEnumMap[instance.topupType],
      'topupCode': instance.topupCode,
      'status': instance.status,
    };

const _$TopupTypeEnumMap = {
  TopupType.offline: 0,
  TopupType.momo: 1,
  TopupType.viettelMoney: 2,
  TopupType.bank: 3,
  TopupType.adjustment: 4,
  TopupType.zaloPay: 7,
  TopupType.vnptMoney: 8,
  TopupType.mbBank: 9,
  TopupType.bidv: 10,
  TopupType.other: 'other',
};

ContractTopupData _$ContractTopupDataFromJson(Map<String, dynamic> json) =>
    ContractTopupData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => ContractTopup.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$ContractTopupDataToJson(ContractTopupData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

ContractTopupResponse _$ContractTopupResponseFromJson(
        Map<String, dynamic> json) =>
    ContractTopupResponse(
      data: json['data'] == null
          ? null
          : ContractTopupData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContractTopupResponseToJson(
        ContractTopupResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ContractTopupEtc _$ContractTopupEtcFromJson(Map<String, dynamic> json) =>
    ContractTopupEtc(
      topupEtcId: json['topupEtcId'] as int?,
      amount: json['amount'] as int?,
      contractId: json['contractId'] as int?,
      topupChannel: json['topupChannel'] as String?,
    );

Map<String, dynamic> _$ContractTopupEtcToJson(ContractTopupEtc instance) =>
    <String, dynamic>{
      'topupEtcId': instance.topupEtcId,
      'amount': instance.amount,
      'contractId': instance.contractId,
      'topupChannel': instance.topupChannel,
    };

ContractTopupLastChargeResponse _$ContractTopupLastChargeResponseFromJson(
        Map<String, dynamic> json) =>
    ContractTopupLastChargeResponse(
      data: json['data'] == null
          ? null
          : ContractTopupEtc.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContractTopupLastChargeResponseToJson(
        ContractTopupLastChargeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ContractTopupType _$ContractTopupTypeFromJson(Map<String, dynamic> json) =>
    ContractTopupType(
      deposit: json['deposit'] as bool?,
      limit: json['limit'] as int?,
      custType: json['custType'] as String?,
    );

Map<String, dynamic> _$ContractTopupTypeToJson(ContractTopupType instance) =>
    <String, dynamic>{
      'deposit': instance.deposit,
      'limit': instance.limit,
      'custType': instance.custType,
    };

ContractTopupTypeResponse _$ContractTopupTypeResponseFromJson(
        Map<String, dynamic> json) =>
    ContractTopupTypeResponse(
      data: json['data'] == null
          ? null
          : ContractTopupType.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ContractTopupTypeResponseToJson(
        ContractTopupTypeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

VehicleInContractResponse _$VehicleInContractResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleInContractResponse(
      data: json['data'] as bool?,
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleInContractResponseToJson(
        VehicleInContractResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
