import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:equatable/equatable.dart';

part 'change_password_form_event.dart';

part 'change_password_form_state.dart';

class ChangePasswordFormBloc
    extends Bloc<ChangePasswordFormEvent, ChangePasswordFormState> {
  final IUserRepository _userRepository;

  ChangePasswordFormBloc({
    required IUserRepository userRepository,
  })  : _userRepository = userRepository,
        super(ChangePasswordFormInitial()) {
    on<ChangePasswordFormSubmitted>(_onChangePasswordFormSubmitted);
  }

  FutureOr<void> _onChangePasswordFormSubmitted(event, emit) async {
    emit(const ChangePasswordFormSubmittedInProgress());

    final result = await _userRepository.changePassword(
      oldPassword: event.oldPassword,
      newPassword: event.newPassword,
    );

    result.when(
      success: (_) => emit(const ChangePasswordFormSubmittedSuccess()),
      failure: (failure) => emit(
        ChangePasswordFormSubmittedFailure(failure.message ?? 'Có lỗi xảy ra'),
      ),
    );
  }
}
