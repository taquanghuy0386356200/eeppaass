part of 'paynow_pay_gate_bloc.dart';

abstract class PayNowPayGateEvent extends Equatable {
  const PayNowPayGateEvent();
}

class PayNowPayLinkedCompleted extends PayNowPayGateEvent {
  final String orderId;
  final int paymentStatus;
  final String? errorCode;
  final int transAmount;

  const PayNowPayLinkedCompleted({
    required this.orderId,
    required this.paymentStatus,
    this.errorCode,
    required this.transAmount,
  });

  @override
  List<Object?> get props => [orderId, paymentStatus, errorCode, transAmount];
}
