import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/models/auth_info/auth_info.dart';
import 'package:epass/commons/repo/authentication_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/fcm/fcm_bloc.dart';
import 'package:epass/pages/login/validators/login_form_validators.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

part 'login_form_event.dart';

part 'login_form_state.dart';

class LoginFormBloc extends Bloc<LoginFormEvent, LoginFormState>
    with LoginFormValidators {
  final IAuthenticationRepository _authRepository;
  final IUserRepository _userRepository;
  final FcmBloc _fcmBloc;
  final AppBloc _appBloc;
  final FlutterSecureStorage _secureStorage;

  LoginFormBloc({
    required IAuthenticationRepository authenticationRepository,
    required IUserRepository userRepository,
    required FcmBloc fcmBloc,
    required AppBloc appBloc,
    required FlutterSecureStorage secureStorage,
  })  : _authRepository = authenticationRepository,
        _userRepository = userRepository,
        _fcmBloc = fcmBloc,
        _appBloc = appBloc,
        _secureStorage = secureStorage,
        super(LoginFormInitial()) {
    on<LoginFormSubmittedEvent>(_onLoginFormSubmitted);
    on<LoginFormContractNoFetched>(_onLoginFormContractNoFetched);
  }

  int countShowForgotPW = 0;

  FutureOr<void> _onLoginFormSubmitted(event, emit) async {
    emit(LoginFormLoading());

    final authResult = await _authRepository.authenticate(
      username: event.username,
      password: event.password,
    );

    await authResult.when(
      success: (auth) async {
        // Update [AppBloc] authentication info
        _appBloc.add(UpdateAppAuthentication(auth));

        // Get user details (contract)
        final getUserResult = await _userRepository.getUser();

        await getUserResult.when(
          success: (user) async {
            // Write username to secure storage
            final contractNo = user.contractNo;

            if (contractNo != null) {
              await _secureStorage.write(
                key: Constant.encryptedUsernameKey,
                value: user.contractNo,
              );
            }

            // Update [AppBloc] user info
            _appBloc.add(UpdateAppUser(user));

            // Update [AppBloc] encrypted password
            _appBloc.add(UpdateEncryptedPassword(event.password));

            // Fetch FCM token and register with backend
            _fcmBloc.add(const FcmTokenFetched());

            // Emit authentication success state
            emit(AuthenticationSuccess(auth));
          },
          failure: (failure) {
            emit(AuthenticationFailure(failure.message ?? ''));
          },
        );
      },
      failure: (failure) {
        if ((failure.message ?? '').contains(
              'Quý Khách đã nhập sai mật khẩu',
            ) ||
            (failure.message ?? '').contains(
              'Tên đăng nhập hoặc mật khẩu không chính xác',
            )) {
          countShowForgotPW += 1;
        }
        emit(AuthenticationFailure(failure.message ?? ''));
      },
    );
  }

  FutureOr<void> _onLoginFormContractNoFetched(
    LoginFormContractNoFetched event,
    Emitter<LoginFormState> emit,
  ) async {
    final contractNo =
        await _secureStorage.read(key: Constant.encryptedUsernameKey);

    emit(LoginFormContractNoFetchedCompleted(contractNo));
  }
}
