import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/footer_container.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_tab_page.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_list/widgets/vehicle_list_card.dart';
import 'package:epass/pages/vehicle/vehicle_list/widgets/vehicle_list_footer.dart';
import 'package:epass/pages/vehicle/vehicle_list/widgets/vehicle_list_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class VehicleListPage extends StatefulWidget {
  const VehicleListPage({Key? key}) : super(key: key);

  @override
  State<VehicleListPage> createState() => _VehicleListPageState();
}

class _VehicleListPageState extends State<VehicleListPage> {
  late RefreshController _refreshController;
  late TextEditingController _searchController;
  final _scrollController = ScrollController();
  bool _isVisible = true;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
    _searchController = TextEditingController();
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));

    // Fetch api
    context.read<VehicleListBloc>().add(VehicleListFetched());

    // Scroll controller
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (_isVisible) {
          setState(() {
            _isVisible = false;
          });
        }
      }
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if (!_isVisible) {
          setState(() {
            _isVisible = true;
          });
        }
      }
    });
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseTabPage(
      title: 'Danh sách xe',
      scrollController: _scrollController,
      leading: BackButton(onPressed: () => context.popRoute()),
      gradientHeaderContainer: const GradientHeaderContainer(),
      child: FadeAnimation(
        delay: 1,
        direction: FadeDirection.up,
        child: RoundedTopContainer(
          margin: EdgeInsets.only(top: 16.h),
          child: BlocConsumer<VehicleListBloc, VehicleListState>(
            listener: (context, state) {
              if (state.error == null) {
                _refreshController.refreshCompleted();
                _refreshController.loadComplete();
              } else if (state.error != null) {
                _refreshController.refreshFailed();
                _refreshController.loadFailed();

                if (state.listData.isNotEmpty) {
                  showErrorSnackBBar(
                    context: context,
                    message: state.error ?? 'Có lỗi xảy ra',
                  );
                }
              }
            },
            builder: (context, state) {
              return Stack(
                children: [
                  Builder(
                    builder: (context) {
                      if (state.isLoading ||
                          state.isRefreshing && state.listData.isEmpty) {
                        return const VehicleListShimmerLoading();
                      } else if (state.error != null &&
                          state.listData.isEmpty) {
                        return CommonErrorPage(
                          onTap: () => context
                              .read<VehicleListBloc>()
                              .add(VehicleListFetched()),
                          message: state.error,
                        );
                      }
                      final listData = state.listData;
                      if (listData.isEmpty) {
                        return NoDataPage(
                          onTap: () => context
                              .read<VehicleListBloc>()
                              // .add(VehicleListRefreshed()),
                              .add(VehicleListFetched()),
                        );
                      }

                      return Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(16.w, 20.h, 16.w, 4.h),
                            child: PrimaryTextField(
                              controller: _searchController,
                              hintText: 'Nhập biển số xe để tìm kiếm',
                              isPlateNumber: true,
                              hasClearButton: true,
                              onClear: () =>
                                  context.read<VehicleListBloc>().add(
                                        VehicleListSearched(
                                          plateNumber: _searchController.text,
                                        ),
                                      ),
                              onChanged: (value) => context
                                  .read<VehicleListBloc>()
                                  .add(VehicleListSearched(plateNumber: value)),
                              suffix: Assets.icons.search.svg(
                                width: 24.r,
                                height: 24.r,
                                color: ColorName.borderColor,
                              ),
                            ),
                          ),
                          Expanded(
                            child: SmartRefresher(
                              physics: const BouncingScrollPhysics(),
                              enablePullDown: true,
                              enablePullUp: !state.isFull,
                              onRefresh: () => context
                                  .read<VehicleListBloc>()
                                  .add(VehicleListRefreshed()),
                              onLoading: () => context
                                  .read<VehicleListBloc>()
                                  .add(VehicleListLoadMore()),
                              controller: _refreshController,
                              child: ListView.builder(
                                physics: const BouncingScrollPhysics(),
                                itemCount: listData.length,
                                itemBuilder: (context, index) {
                                  final vehicle = listData[index];
                                  return Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 16.w,
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        top: 20.h,
                                        bottom: index == listData.length - 1 &&
                                                state.isFull
                                            ? 108.h
                                            : 0.h,
                                      ),
                                      child: VehicleListCard(vehicle: vehicle),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                  state.listData.isNotEmpty
                      ? Align(
                          alignment: Alignment.bottomCenter,
                          child: FooterContainer(
                            height: _isVisible ? 88.h : 0,
                            child: const VehicleListFooter(),
                          ),
                        )
                      : const SizedBox.shrink(),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
