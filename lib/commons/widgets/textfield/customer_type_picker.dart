import 'package:epass/commons/models/customer_type/customer_type_response.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/widgets/textfield/cupertino_picker_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/category/category_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomerTypePicker extends StatelessWidget {
  final CustomerTypeModel? selectedCustomerType;
  final ValueChanged<PickerItemWithValue<CustomerTypeModel>?>? onItemChanged;

  const CustomerTypePicker({
    Key? key,
    this.selectedCustomerType,
    this.onItemChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CategoryBloc(categoryRepository: getIt<ICategoryRepository>()),
      child: _CustomerTypePicker(
        selectedCustomerType: selectedCustomerType,
        onItemChanged: onItemChanged,
      ),
    );
  }
}

class _CustomerTypePicker extends StatefulWidget {
  final CustomerTypeModel? selectedCustomerType;
  final ValueChanged<PickerItemWithValue<CustomerTypeModel>?>? onItemChanged;

  const _CustomerTypePicker({
    Key? key,
    this.selectedCustomerType,
    this.onItemChanged,
  }) : super(key: key);

  @override
  State<_CustomerTypePicker> createState() => _CustomerTypePickerState();
}

class _CustomerTypePickerState extends State<_CustomerTypePicker> {
  PickerItemWithValue<CustomerTypeModel>? _selectedCustomerType;

  @override
  void initState() {
    context.read<CategoryBloc>().add(const CustomerTypeFetched());

    if (widget.selectedCustomerType != null) {
      _selectedCustomerType = PickerItemWithValue(
        title: widget.selectedCustomerType!.name ?? '',
        value: widget.selectedCustomerType!,
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryState>(
      builder: (context, state) {
        List<PickerItemWithValue<CustomerTypeModel>> items = [];
        if (state is CustomerTypeFetchedSuccess) {
          items = state.customerTypes.map((e) => PickerItemWithValue(title: e.name ?? '', value: e)).toList();
        }
        return CupertinoPickerField(
          items: items,
          defaultItem: _selectedCustomerType,
          isLoading: state is CustomerTypeFetchedInProgress,
          validator: (_) {
            if (_selectedCustomerType == null) {
              return 'Vui lòng chọn Loại khách hàng';
            }
            return null;
          },
          hintText: 'Chọn loại khách hàng',
          enabled: state is! CustomerTypeFetchedInProgress,
          label: 'Loại khách hàng',
          errorText: state is CustomerTypeFetchedFailure ? state.message : null,
          onItemChanged: (PickerItemWithValue<CustomerTypeModel>? selectedItem) {
            setState(() => _selectedCustomerType = selectedItem);
            widget.onItemChanged?.call(selectedItem);
          },
        );
      },
    );
  }
}
