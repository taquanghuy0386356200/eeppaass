import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';
import 'dart:ui' as ui;

class TransactionTopupCard extends StatelessWidget {
  const TransactionTopupCard({
    Key? key,
    required this.contractTopup,
  }) : super(key: key);

  final ContractTopup contractTopup;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Name
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    contractTopup.topupType?.label ?? '',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 12.h),
                  // Date
                  Text(
                    contractTopup.topupDate ?? '',
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          color: ColorName.textGray2,
                        ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            SizedBox(height: 10.w),
            Text(
              '+${(contractTopup.amount ?? 0).formatCurrency(symbol: '₫')}',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: ColorName.textGray2,
                    fontWeight: FontWeight.bold,
                  ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
        SizedBox(height: 20.h),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // ID
            Expanded(
              child: RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Icon(
                        Icons.circle,
                        size: 6.r,
                        color: ColorName.primaryColor,
                      ),
                      alignment: ui.PlaceholderAlignment.middle,
                    ),
                    TextSpan(
                      text: '  ID: ${contractTopup.topupEtcId?.toString()}',
                      style: Theme.of(context).textTheme.subtitle2!,
                    ),
                  ],
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(height: 16.w),
            Text(
              contractTopup.status == '1' ? 'Thành công' : 'Thất bại',
              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    color: contractTopup.status == '1'
                        ? ColorName.success
                        : ColorName.error,
                  ),
            ),
          ],
        ),
      ],
    );
  }
}
