// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'confirmation_wait_for.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfirmationWaitForRequest _$ConfirmationWaitForRequestFromJson(
        Map<String, dynamic> json) =>
    ConfirmationWaitForRequest(
      saleOrderId: json['saleOrderId'] as int?,
    );

Map<String, dynamic> _$ConfirmationWaitForRequestToJson(
        ConfirmationWaitForRequest instance) =>
    <String, dynamic>{
      'saleOrderId': instance.saleOrderId,
    };
