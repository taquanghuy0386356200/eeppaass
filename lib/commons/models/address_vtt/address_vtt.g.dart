// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_vtt.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressVTT _$AddressVTTFromJson(Map<String, dynamic> json) => AddressVTT(
      areaCode: json['areaCode'] as String?,
      areaType: json['areaType'] as String?,
      province: json['province'] as String?,
      district: json['district'] as String?,
      precinct: json['precinct'] as String?,
      center: json['center'] as String?,
      name: json['name'] as String?,
      status: json['status'] as String?,
      vtMapCode: json['vtMapCode'] as String?,
      vnCode: json['vnCode'] as String?,
      vnName: json['vnName'] as String?,
      population: json['population'] as int?,
      square: json['square'] as int?,
      households: json['households'] as int?,
    );

Map<String, dynamic> _$AddressVTTToJson(AddressVTT instance) =>
    <String, dynamic>{
      'areaCode': instance.areaCode,
      'areaType': instance.areaType,
      'province': instance.province,
      'district': instance.district,
      'precinct': instance.precinct,
      'center': instance.center,
      'name': instance.name,
      'status': instance.status,
      'vtMapCode': instance.vtMapCode,
      'vnCode': instance.vnCode,
      'vnName': instance.vnName,
      'population': instance.population,
      'square': instance.square,
      'households': instance.households,
    };

AddressVTTResponse _$AddressVTTResponseFromJson(Map<String, dynamic> json) =>
    AddressVTTResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => AddressVTT.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AddressVTTResponseToJson(AddressVTTResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
