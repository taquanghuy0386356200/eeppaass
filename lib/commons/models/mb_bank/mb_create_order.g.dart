// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mb_create_order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MBCreateOrderRequest _$MBCreateOrderRequestFromJson(
        Map<String, dynamic> json) =>
    MBCreateOrderRequest(
      transAmt: json['transAmt'] as String,
      transactionId: json['transactionId'] as String,
    );

Map<String, dynamic> _$MBCreateOrderRequestToJson(
        MBCreateOrderRequest instance) =>
    <String, dynamic>{
      'transactionId': instance.transactionId,
      'transAmt': instance.transAmt,
    };

MBCreateOrderResponse _$MBCreateOrderResponseFromJson(
        Map<String, dynamic> json) =>
    MBCreateOrderResponse(
      clientMessageId: json['clientMessageId'] as String,
      errorCode: json['errorCode'] as String,
      data: MBOrderData.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MBCreateOrderResponseToJson(
        MBCreateOrderResponse instance) =>
    <String, dynamic>{
      'clientMessageId': instance.clientMessageId,
      'errorCode': instance.errorCode,
      'data': instance.data,
    };

MBOrderData _$MBOrderDataFromJson(Map<String, dynamic> json) => MBOrderData(
      requestId: json['requestId'] as String,
      requestToken: json['requestToken'] as String?,
      oneLink: json['oneLink'] as String?,
      qrString: json['qrString'] as String?,
      feePayment: json['feePayment'] as String?,
      feeNoVAT: json['feeNoVAT'] as String?,
      vat: json['vat'] as String?,
      expriedTime: json['expriedTime'] == null
          ? null
          : DateTime.parse(json['expriedTime'] as String),
    );

Map<String, dynamic> _$MBOrderDataToJson(MBOrderData instance) =>
    <String, dynamic>{
      'requestId': instance.requestId,
      'requestToken': instance.requestToken,
      'oneLink': instance.oneLink,
      'qrString': instance.qrString,
      'feePayment': instance.feePayment,
      'feeNoVAT': instance.feeNoVAT,
      'vat': instance.vat,
      'expriedTime': instance.expriedTime?.toIso8601String(),
    };
