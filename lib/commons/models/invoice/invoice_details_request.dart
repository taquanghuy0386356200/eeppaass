/// fileType : "pdf"

class InvoiceDetailsRequest {
  InvoiceDetailsRequest({
    this.fileType,
  });

  InvoiceDetailsRequest.fromJson(dynamic json) {
    fileType = json['fileType'];
  }

  String? fileType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fileType'] = fileType;
    return map;
  }
}
