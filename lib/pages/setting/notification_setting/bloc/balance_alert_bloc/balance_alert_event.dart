part of 'balance_alert_bloc.dart';

abstract class BalanceAlertEvent extends Equatable {
  const BalanceAlertEvent();
}

class BalanceAlertUpdated extends BalanceAlertEvent {
  final bool isBalanceAlertOn;
  final int? alertAmount;

  const BalanceAlertUpdated({
    required this.isBalanceAlertOn,
    this.alertAmount,
  });

  @override
  List<Object?> get props => [isBalanceAlertOn, alertAmount];
}
