import 'package:dart_exporter_annotation/dart_exporter_annotation.dart';

@Export()
class OrderResultEntity {
  final String orderId;
  final bool sucess;
  OrderResultEntity({
    required this.orderId,
    required this.sucess,
  });

  @override
  String toString() => 'OrderResultEntity(orderId: $orderId, sucess: $sucess)';
}
