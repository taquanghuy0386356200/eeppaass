import 'package:epass/commons/constant.dart';
import 'package:epass/pages/web/web_view_without_header.dart';
import 'package:flutter/material.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({Key? key}) : super(key: key);

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  // late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    // context
    //     .read<NewsNotificationBloc>()
    //     .add(const NotificationFetched(notificationType: 3));
    // _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    // _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebViewWithoutHeader(
      initUrl: Constant.ePassNewsUrl,
    );
    // return BlocConsumer<NewsNotificationBloc, NotificationState>(
    //   listener: (context, state) {
    //     if (state.error == null) {
    //       _refreshController.refreshCompleted();
    //       _refreshController.loadComplete();
    //     } else if (state.error != null) {
    //       _refreshController.refreshFailed();
    //       _refreshController.loadFailed();
    //
    //       if (state.listData.isNotEmpty) {
    //         showErrorSnackBBar(
    //           context: context,
    //           message: state.error ?? 'Có lỗi xảy ra',
    //         );
    //       }
    //     }
    //   },
    //   builder: (context, state) {
    //     return Column(
    //       crossAxisAlignment: CrossAxisAlignment.start,
    //       children: [
    //         SizedBox(height: 24.h),
    //         Padding(
    //           padding: EdgeInsets.symmetric(horizontal: 16.w),
    //           child: PrimaryTextField(
    //             hintText: 'Tìm kiếm',
    //             onChanged: (value) => context
    //                 .read<NewsNotificationBloc>()
    //                 .add(NotificationSearched(value)),
    //             suffix: Assets.icons.search.svg(
    //               width: 24.r,
    //               height: 24.r,
    //               color: ColorName.borderColor,
    //             ),
    //           ),
    //         ),
    //         SizedBox(height: 16.h),
    //         Expanded(
    //           child: Builder(
    //             builder: (_) {
    //               if (state.isLoading ||
    //                   state.isRefreshing && state.listData.isEmpty) {
    //                 return const EventNotificationShimmerLoading();
    //               } else if (state.error != null && state.listData.isEmpty) {
    //                 return Center(
    //                   child: Text(state.error ?? 'Có lỗi xảy ra'),
    //                 );
    //               }
    //               final listData = state.listData;
    //               if (listData.isEmpty) {
    //                 return NoDataPage(
    //                   onTap: () => context
    //                       .read<NewsNotificationBloc>()
    //                       .add(const NotificationRefreshed()),
    //                 );
    //               }
    //
    //               return SmartRefresher(
    //                 physics: const BouncingScrollPhysics(),
    //                 enablePullDown: true,
    //                 enablePullUp: !state.isFull,
    //                 onRefresh: () => context
    //                     .read<NewsNotificationBloc>()
    //                     .add(const NotificationRefreshed()),
    //                 onLoading: () => context
    //                     .read<NewsNotificationBloc>()
    //                     .add(const NotificationLoadMore()),
    //                 controller: _refreshController,
    //                 child: ListView.separated(
    //                   physics: const BouncingScrollPhysics(),
    //                   itemCount: listData.length,
    //                   itemBuilder: (context, index) {
    //                     final notification = listData[index];
    //                     return EventNotificationCard(
    //                       notification: notification,
    //                       onTap: () {
    //                         final notificationId =
    //                             notification.notificationMsgId;
    //                         if (notificationId != null) {
    //                           context
    //                               .read<NewsNotificationBloc>()
    //                               .add(NotificationRead(notificationId));
    //                         }
    //                       },
    //                     );
    //                   },
    //                   separatorBuilder: (context, index) => Divider(
    //                     color: ColorName.disabledBorderColor,
    //                     height: 0.5,
    //                     thickness: 0.5,
    //                     indent: 60.w,
    //                     endIndent: 24.w,
    //                   ),
    //                 ),
    //               );
    //             },
    //           ),
    //         ),
    //       ],
    //     );
    //   },
    // );
  }
}
