part of 'link_viet_map_bloc.dart';

@immutable
abstract class LinkVietMapState extends Equatable {
  const LinkVietMapState();
}

class LinkVietMapInit extends LinkVietMapState {
  @override
  List<Object> get props => [];
}

class LinkedVietMap extends LinkVietMapState {
  final String? name;
  final String? phone;
  final String? token;
  final String? message;

  const LinkedVietMap({this.name, this.phone, this.token, this.message});

  @override
  List<Object?> get props => [name, phone, token, message];
}

class NotLinkedVietMap extends LinkVietMapState {
  final String? message;

  const NotLinkedVietMap({this.message});
  
  @override
  List<Object?> get props => [message];
}
