/// message : {"token":"bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1...","notification":{"title":"Portugal vs. Denmark","body":"great match!"}}

class FirebaseCloudMessage {
  FirebaseCloudMessage({
    this.message,
  });

  FirebaseCloudMessage.fromJson(dynamic json) {
    message = json['message'] != null
        ? NotificationMessage.fromJson(json['message'])
        : null;
  }

  NotificationMessage? message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (message != null) {
      map['message'] = message?.toJson();
    }
    return map;
  }
}

/// token : "bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1..."
/// notification : {"title":"Portugal vs. Denmark","body":"great match!"}

class NotificationMessage {
  NotificationMessage({
    this.token,
    this.notification,
  });

  NotificationMessage.fromJson(dynamic json) {
    token = json['token'];
    notification = json['notification'] != null
        ? NotificationContent.fromJson(json['notification'])
        : null;
  }

  String? token;
  NotificationContent? notification;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    if (notification != null) {
      map['notification'] = notification?.toJson();
    }
    return map;
  }
}

/// title : "Portugal vs. Denmark"
/// body : "great match!"

class NotificationContent {
  NotificationContent({
    this.title,
    this.body,
  });

  NotificationContent.fromJson(dynamic json) {
    title = json['title'];
    body = json['body'];
  }

  String? title;
  String? body;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['body'] = body;
    return map;
  }
}
