import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_logo_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/cash_in/bloc/active_otp_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/otp_countdown.dart';

class CashInConfirmOTPPage extends StatefulWidget {
  const CashInConfirmOTPPage({
    Key? key,
  }) : super(key: key);

  @override
  State<CashInConfirmOTPPage> createState() => _CashInConfirmOTPPageState();
}

class _CashInConfirmOTPPageState extends State<CashInConfirmOTPPage> {
  final _errorController = StreamController<ErrorAnimationType>();

  @override
  void deactivate() {
    context.read<ActiveOtpBloc>().add(const ActiveOTPPaused());
    super.deactivate();
  }

  @override
  void dispose() {
    _errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final phoneNumber = context.read<AppBloc>().state.user?.noticePhoneNumber;

    return Builder(builder: (context) {
      return BlocListener<ActiveOtpBloc, ActiveOtpState>(
        listener: (context, state) {
          if (state is ActiveOtpConfirmedInProgress) {
            context.loaderOverlay.show();
          } else {
            context.loaderOverlay.hide();
          }
        },
        child: BaseLogoPage(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: false,
          child: Stack(
            children: [
              const GradientHeaderContainer(),
              SafeArea(
                child: FadeAnimation(
                  delay: 1,
                  direction: FadeDirection.up,
                  child: RoundedTopContainer(
                    padding: EdgeInsets.fromLTRB(16.w, 48.h, 16.w, 0.h),
                    child: BlocConsumer<ActiveOtpBloc, ActiveOtpState>(
                      listener: (context, state) {
                        if (state is ActiveOtpConfirmedInProgress) {
                          context.loaderOverlay.show();
                        } else if (state is ActiveOtpConfirmedSuccess) {
                          context.loaderOverlay.hide();
                        } else if (state is ActiveOtpConfirmedFailure) {
                          context.loaderOverlay.hide();
                          _errorController.add(ErrorAnimationType.shake);
                        }
                      },
                      builder: (context, state) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: double.infinity,
                              child: Text(
                                'Xác nhận',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .copyWith(
                                      fontSize: 22.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(height: 24.h),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.w),
                              child: RichText(
                                text: TextSpan(
                                  text:
                                      'Nhập mã OTP đã được gửi đến số điện thoại ',
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: '${phoneNumber ?? ''}.',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                  ],
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                textAlign: TextAlign.center,
                                maxLines: 2,
                              ),
                            ),
                            SizedBox(height: 24.h),
                            PinCodeTextField(
                              appContext: context,
                              errorAnimationController: _errorController,
                              length: 6,
                              obscureText: false,
                              animationType: AnimationType.fade,
                              pinTheme: PinTheme(
                                shape: PinCodeFieldShape.box,
                                fieldHeight: 46.r,
                                fieldWidth: 46.r,
                                borderRadius: BorderRadius.circular(8.0),
                                errorBorderColor: ColorName.error,
                                borderWidth: 1.0,
                                activeColor: ColorName.disabledBorderColor,
                                activeFillColor: Colors.white,
                                inactiveColor: ColorName.disabledBorderColor,
                                inactiveFillColor: Colors.white,
                                selectedColor: ColorName.disabledBorderColor,
                                selectedFillColor: Colors.white,
                              ),
                              backgroundColor: Colors.transparent,
                              autoDismissKeyboard: true,
                              autoFocus: true,
                              keyboardType: TextInputType.number,
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.w600,
                                  ),
                              onCompleted: _onOTPCompleted,
                              onChanged: (value) {},
                              beforeTextPaste: (text) {
                                //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                return false;
                              },
                            ),
                            SizedBox(height: 20.h),
                            BlocSelector<ActiveOtpBloc, ActiveOtpState,
                                ActiveOtpConfirmedSuccess?>(
                              selector: (state) =>
                                  state is ActiveOtpConfirmedSuccess
                                      ? state
                                      : null,
                              builder: (context, state) {
                                return state != null
                                    ? OTPCountdown(
                                        otpSubmittedTime: state.submittedTime,
                                      )
                                    : const SizedBox.shrink();
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  void _onOTPCompleted(String value) {
    FocusScope.of(context).unfocus();
    // trungnh xu ly sau khi nhap OTP tai day
    // sau khi check xong otp
    context.popRoute(value);
  }
}
