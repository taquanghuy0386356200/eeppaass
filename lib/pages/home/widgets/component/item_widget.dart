import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/pages/home/widgets/policy_sceen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../commons/widgets/modal/alert_dialog.dart';
import '../../../bloc/app/app_bloc.dart';
import '../../model/apps_model.dart';
import '../../model/list_app.dart';
import '../all_app_store_page.dart';
import '../home_app_store.dart';
import 'section_widget.dart';

class itemWidget extends StatelessWidget {
  final int index;
  final ListApp appModel;
  final SectionType sectionType;
  final isLastItem;
  final bool? isInsertMsisdn;
  final AppsModel appsModel;

  const itemWidget(
      {Key? key,
      required this.index,
      required this.appModel,
      required this.sectionType,
      this.isLastItem,
      this.isInsertMsisdn,
      required this.appsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<AppBloc, AppState, String?>(
      selector: (state) => state.user?.phone,
      builder: (context, phone) {
        return GestureDetector(
          onTap: () async {
            if (appModel.isOpenConfirm == 1) {
              await showConfirmPolicy(context, phone);
            } else {
              directToApp(phone, context, appsModel);
            }
          },
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 4.w,
            ),
            child: Column(
              children: [
                Container(
                    height: 56.r,
                    width: 56.r,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: Image.network(
                              sectionType == SectionType.popular && isLastItem
                                  ? 'https://i.ibb.co/mN85nfm/Xem-th-m.png'
                                  : appModel.appIcon ?? '',
                              width: 32.0.w,
                              height: 32.0.h,
                              fit: BoxFit.fill,
                            ).image))),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top: 6.0.h),
                    child: Text(
                      sectionType == SectionType.popular && isLastItem
                          ? 'Xem thêm'
                          : appModel.appName ?? '',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> showConfirmPolicy(BuildContext context, String? phone) async {
    showAlertDialog(
      context,
      title: 'Thông báo',
      content: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: appModel.contentConfirm,
          style: DefaultTextStyle.of(context).style,
          children: [
            TextSpan(
              text: ' Nội dung điều khoản trước khi sử dụng dịch vụ',
              style: const TextStyle(fontWeight: FontWeight.bold),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Navigator.pop(getContext()!);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PolicyScreen(
                              appsModel: appModel,
                              nextAction: (context) {
                                Navigator.pop(context);
                                directToApp(phone, context, appsModel);
                              },
                            )),
                  );
                },
            ),
          ],
        ),
      ),
      primaryButtonTitle: 'Tiếp tục',
      onPrimaryButtonTap: (context) async {
        Navigator.pop(context);
        directToApp(phone, context, appsModel);
      },
      secondaryButtonTitle: 'Đóng',
    );
  }

  void directToApp(String? phone, BuildContext context, AppsModel appsModel) {
    String linkDirectUrl = isInsertMsisdn == true
        ? appModel.screenRedirect! + "?msisdn=$phone"
        : appModel.screenRedirect ?? '';
    if (sectionType == SectionType.popular && isLastItem) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AppGroupScreen(
                  appsModel: appsModel,
                )),
      );
    } else {
      launchUrl(Uri.parse(linkDirectUrl), mode: LaunchMode.externalApplication);
      // if (appModel.isOpenBrowserApp == 0) {
      //   launchUrl(Uri.parse(linkDirectUrl), mode: LaunchMode.externalApplication);
      // } else {
      //   Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //         builder: (context) => HomeAppStore(
      //               urlWeb: linkDirectUrl,
      //               title: appModel.appName ?? '',
      //             )),
      //   );
      // }
    }
  }
}
