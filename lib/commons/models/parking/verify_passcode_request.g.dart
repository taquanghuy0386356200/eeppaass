// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verify_passcode_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerifyPassCodeRequest _$VerifyPassCodeRequestFromJson(
        Map<String, dynamic> json) =>
    VerifyPassCodeRequest(
      partnerCode: json['partnerCode'] as String?,
      stationCode: json['stationCode'] as String?,
      laneInCode: json['laneInCode'] as String?,
      laneOutCode: json['laneOutCode'] as String?,
      ticketId: json['ticketId'] as int?,
      transId: json['transId'] as int?,
      plateNumber: json['plateNumber'] as int?,
      amount: json['amount'] as int?,
      currency: json['currency'] as String?,
      contractId: json['contractId'] as int?,
      passCode: json['passCode'] as String?,
    );

Map<String, dynamic> _$VerifyPassCodeRequestToJson(
        VerifyPassCodeRequest instance) =>
    <String, dynamic>{
      'partnerCode': instance.partnerCode,
      'stationCode': instance.stationCode,
      'laneInCode': instance.laneInCode,
      'laneOutCode': instance.laneOutCode,
      'ticketId': instance.ticketId,
      'transId': instance.transId,
      'plateNumber': instance.plateNumber,
      'amount': instance.amount,
      'currency': instance.currency,
      'contractId': instance.contractId,
      'passCode': instance.passCode,
    };
