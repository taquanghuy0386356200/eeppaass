import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/modal/neutral_confirm_dialog.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/station_stage_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_duration_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';

class BuyTicketButton extends StatelessWidget {
  const BuyTicketButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final vehicles = context.select((TicketVehicleBloc bloc) {
      final userVehicles = bloc.state.userVehicles;
      final otherVehicles = bloc.state.otherVehicles;
      return [...userVehicles, ...otherVehicles];
    });
    final stationStage = context.select((StationStageBloc bloc) => bloc.state.stationStage);

    final effectiveDate = context.select((TicketDurationBloc bloc) => bloc.state.effectiveDate);

    final expireDate = context.select((TicketDurationBloc bloc) => bloc.state.expireDate);

    final balance = context.select((BalanceBloc bloc) => bloc.state.balance) ?? 0;

    final allValidTicket = context.select((TicketPriceBloc bloc) {
      final state = bloc.state;
      if (state is TicketPriceFetchedSuccess) {
        return state.ticketPrices.every((element) => element.message == null);
      }
      return false;
    });

    final price = context.select((TicketPriceBloc bloc) {
      final state = bloc.state;
      if (state is TicketPriceFetchedSuccess) {
        final price = state.ticketPrices.map((e) => e.fee ?? 0).reduce((value, element) => value += element);
        return price;
      }
      return 0;
    });

    final isBuyTicketEnabled = stationStage != null &&
        effectiveDate != null &&
        expireDate != null &&
        Jiffy().diff(expireDate, Units.DAY).abs() > 5 &&
        vehicles.isNotEmpty &&
        allValidTicket &&
        price > 0;

    return SizedBox(
      height: 56.h,
      child: PrimaryButton(
        title: 'Mua ngay',
        enabled: isBuyTicketEnabled,
        onTap: isBuyTicketEnabled
            ? () async => _onBuyTicketTapped(
                  context: context,
                  balance: balance,
                  price: price,
                  effectiveDate: effectiveDate!,
                  expireDate: expireDate!,
                )
            : null,
      ),
    );
  }

  Future<void> _onBuyTicketTapped({
    required BuildContext context,
    required int balance,
    required int price,
    required DateTime effectiveDate,
    required DateTime expireDate,
  }) async {
    // Balance Check
    if (balance < price) {
      return await showDialog(
        context: context,
        builder: (dialogContext) {
          return NeutralConfirmDialog(
            title: 'Số dư tài khoản không đủ',
            content: 'Số dư tài khoản hiện tại không đủ để thực'
                ' hiện mua vé. Để tiếp tục mua vé, vui lòng nạp'
                ' tiền hoặc chọn phương thức thanh toán khác.',
            contentTextAlign: TextAlign.center,
            secondaryButtonTitle: 'Để sau',
            secondaryButtonColor: ColorName.borderColor,
            primaryButtonTitle: 'Nạp tiền',
            onPrimaryButtonTap: () async {
              Navigator.of(dialogContext).pop();

              final tabsBloc = context.read<HomeTabsBloc>();

              tabsBloc.add(const HomeTabBarRequestHidden(isHidden: false));
              await context.pushRoute(CashInRouter());

              tabsBloc.add(const HomeTabBarRequestHidden(isHidden: true));
            },
          );
        },
      );
    }

    // Ticket Duration Check
    var durationValidated = true;
    final router = context.router;
    if (effectiveDate.isBefore(Jiffy().startOf(Units.DAY).dateTime)) {
      durationValidated = await showDialog(
        context: context,
        builder: (dialogContext) => NeutralConfirmDialog(
          title: 'Xác nhận Thời gian hiệu lực',
          content: 'Vé có Thời gian bắt đầu hiệu lực'
              ' (${Jiffy(effectiveDate).format('dd/MM/yyyy')}) TRƯỚC'
              ' thời gian mua (${Jiffy().startOf(Units.DAY).format('dd/MM/yyyy')}).\n'
              'Bạn có chắc chắn muốn mua vé?',
          contentTextAlign: TextAlign.center,
          secondaryButtonTitle: 'Đóng',
          secondaryButtonColor: ColorName.borderColor,
          onSecondaryButtonTap: () => Navigator.of(dialogContext).pop(false),
          primaryButtonTitle: 'Đồng ý',
          onPrimaryButtonTap: () => Navigator.of(dialogContext).pop(true),
        ),
      );
    }

    if (durationValidated) {
      router.push(const BuyTicketConfirmRoute());
    }
  }
}
