class VnptPaymentResponse {
  final String? responseCode;
  final String? description;
  final String? redirectUrl;
  final String? secureCode;

  VnptPaymentResponse({
    this.responseCode,
    this.description,
    this.redirectUrl,
    this.secureCode,
  });

  factory VnptPaymentResponse.fromMap(Map<String, dynamic> map) {
    return VnptPaymentResponse(
      responseCode: map['RESPONSE_CODE'] as String?,
      description: map['DESCRIPTION'] as String?,
      redirectUrl: map['REDIRECT_URL'] as String?,
      secureCode: map['SECURE_CODE'] as String?,
    );
  }
}
