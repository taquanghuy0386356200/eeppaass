part of 'sms_vehicle_category_bloc.dart';

abstract class SMSVehicleCategoryEvent extends Equatable {
  const SMSVehicleCategoryEvent();
}

class SMSVehicleCategoryFetched extends SMSVehicleCategoryEvent {
  final bool isPersonal;

  const SMSVehicleCategoryFetched({
    required this.isPersonal,
  });

  @override
  List<Object> get props => [];
}

class SelectedSMSOption extends SMSVehicleCategoryEvent {
  final CategoryModel? selectedSMSOption;
  final List<CategoryModel>? smsCategories;

  const SelectedSMSOption({this.selectedSMSOption, this.smsCategories});

  @override
  List<Object?> get props => [];
}
