import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/extensions/platform_ext.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:simple_result/simple_result.dart';

abstract class ILocalFileService {
  Future<Result<File, Failure>> saveFile({
    required List<int> data,
    required String fileName,
    String? fileExtension,
    String? directory,
  });
}

class LocalFileService implements ILocalFileService {
  @override
  Future<Result<File, Failure>> saveFile({
    required List<int> data,
    required String fileName,
    String? fileExtension,
    String? directory,
  }) async {
    bool hasPermission = await _requestStoragePermission();
    if (!hasPermission) {
      return const Result.failure(
          PermissionFailure(message: 'Không có quyền truy cập bộ nhớ máy'));
    }

    try {
      Directory? dir;
      if (Platform.isAndroid) {
        // dir = Directory('/storage/emulated/0/Download');
        dir = await getExternalStorageDirectory();
        // Directory? directory = await getExternalStorageDirectory();
        // String newPath = "";
        // List<String> paths = directory!.path.split("/");
        // for (int x = 1; x < paths.length; x++) {
        //   String folder = paths[x];
        //   if (folder != "Android") {
        //     newPath = '$newPath/$folder';
        //   } else {
        //     break;
        //   }
        // }
        // newPath = '$newPath/Download';
        // dir = Directory(newPath);
      } else {
        dir = await getApplicationDocumentsDirectory();
      }

      if (dir == null) return const Result.failure(UnknownFailure());

      String fullPath = dir.path;
      if (directory != null && directory.isNotEmpty) {
        fullPath += '/$directory';
      }

      final appDir = Directory(fullPath);
      if (!(await appDir.exists())) {
        await appDir.create(recursive: true);
      }

      String filePath = '${appDir.path}/$fileName';
      if (fileExtension != null && fileExtension.isNotEmpty) {
        filePath += '.$fileExtension';
      }
      final file = File(filePath);
      final fileResult = await file.writeAsBytes(data);

      return Result.success(fileResult);
    } on Exception catch (e) {
      print(e);
      return const Result.failure(UnknownFailure());
    }
  }

  Future<bool> _requestStoragePermission() async {
    bool storagePermissionStatus = true;
    bool videoPermissionStatus = true;
    bool photoPermissionStatus = true;
    final sdkRelease = await PlatformExt().getSdkVersion();
    if (sdkRelease != null && sdkRelease >= 33) {
      final photoRequest = await Permission.photos.request();
      photoPermissionStatus = photoRequest.isGranted;
      final videoRequest = await Permission.videos.request();
      videoPermissionStatus = videoRequest.isGranted;
    } else {
      final storageRequest = await Permission.storage.request();
      storagePermissionStatus = storageRequest.isGranted;
    }
    return storagePermissionStatus &&
        videoPermissionStatus &&
        photoPermissionStatus;
  }
}
