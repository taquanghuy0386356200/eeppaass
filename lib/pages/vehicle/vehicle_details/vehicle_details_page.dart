import 'package:auto_route/auto_route.dart';
import 'package:car_doctor_epass_services/gen/assets.gen.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/extensions/image_extension.dart';
import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/cash_in/cash_in_page.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_details/bloc/vehicle_details_bloc.dart';
import 'package:epass/pages/vehicle/vehicle_details/widgets/vehicle_details_card.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:collection/collection.dart';

class VehicleDetailsPage extends StatelessWidget {
  final String vehicleId;
  final String contractId;
  final int? fee;
  // Currently, there's no need to call api to get vehicle details
  // Could've changed later

  const VehicleDetailsPage(
      {Key? key,
      @PathParam('id') required this.vehicleId,
      required this.contractId,
      this.fee})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => VehicleDetailsBloc(
        appBloc: getIt<AppBloc>(),
        vehicleRepository: getIt<IVehicleRepository>(),
      )..add(VehicleCheckDebitFetched(
          vehicleId: vehicleId, contractId: contractId)),
      child: MultiBlocListener(
        listeners: [
          BlocListener<VehicleDetailsBloc, VehicleDetailsState>(
            listener: (context, state) {
              if (state is VehicleRFIDUpdatedInProgress) {
                context.loaderOverlay.show();
              } else if (state is VehicleRFIDUpdatedFailure) {
                context.loaderOverlay.hide();
                showErrorSnackBBar(
                    context: context,
                    message: state.message,
                    duration: const Duration(seconds: 10));
              } else if (state is VehicleRFIDUpdatedSuccess) {
                context.loaderOverlay.hide();
                context.read<VehicleListBloc>().add(VehicleListFetched());
              } else if (state is VehicleCheckDebitFetchSuccess) {
                context.loaderOverlay.hide();
                if (state.vehicleDebitData?.statusDebit ==
                    Constant.isDebitVehicle) {
                  _dialogDebtWarningBuilder(context);
                }
              } else if (state is VehicleDebitTransactionSuccess) {
                context.loaderOverlay.hide();
                context.read<VehicleListBloc>().add(VehicleListFetched());
                _dialogDebtSuccessBuilder(context);
              } else if (state is VehicleDebitTransactionFailure) {
                context.loaderOverlay.hide();

                if (state.code == REQUEST_DEBIT_NO_MONEY) {
                  showErrorSnackBBar(
                      context: context,
                      message: "Số dư tài khoản không đủ",
                      duration: const Duration(seconds: 5));
                } else {
                  showErrorSnackBBar(
                      context: context,
                      message: state.message,
                      duration: const Duration(seconds: 5));
                }
              }
            },
          ),
          BlocListener<VehicleListBloc, VehicleListState>(
            listener: (context, state) async {
              if (state.isLoading) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
              }
              if (state.error == null) {
                showSuccessSnackBBar(
                  context: context,
                  message: 'Cập nhật trạng thái thẻ eTag thành công',
                );
              } else if (state.error != null) {
                if (state.listData.isNotEmpty) {
                  showErrorSnackBBar(
                    context: context,
                    message: state.error ?? 'Có lỗi xảy ra',
                  );
                }
              }
            },
          ),
        ],
        child: BasePage(
          title: 'Thông tin chi tiết',
          backgroundColor: Colors.white,
          child: Stack(
            children: [
              const GradientHeaderContainer(),
              SafeArea(
                child: FadeAnimation(
                  delay: 1,
                  direction: FadeDirection.up,
                  child: SingleChildScrollView(
                    child: RoundedTopContainer(
                      margin: EdgeInsets.only(top: 16.h),
                      padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                      // Your widget go below
                      child: BlocSelector<VehicleListBloc, VehicleListState,
                          Vehicle?>(
                        selector: (state) => state.listData.singleWhereOrNull(
                            (element) =>
                                (element.vehicleId?.toString() ?? '') ==
                                vehicleId),
                        builder: (context, vehicle) {
                          final tabsBloc = context.read<HomeTabsBloc>();

                          return Column(
                            children: [
                              VehicleDetailsCard(vehicle: vehicle),
                              SizedBox(height: 20.h,),
                              if (vehicle?.debit?.statusDebit ==
                                  VehicleDebitStatus.active)
                                PrimaryButton(
                                  onTap: () {
                                    _dialogDebtWarningBuilder(context);
                                  },
                                  title: "Thanh toán",
                                  widthCustom:  MediaQuery.of(context).size.width,
                                ),

                              // SizedBox(height: 24.h),
                              // SizedBox(
                              //   height: 56.h,
                              //   width: double.infinity,
                              //   child: SecondaryButton(
                              //     title: 'Lịch sử mua vé',
                              //     onTap: () async {
                              //       await context.pushRoute(
                              //         TransactionHistoryRouter(
                              //           children: [
                              //             TransactionTicketPurchaseRoute(
                              //               plateNumber: vehicle?.plateNumber,
                              //             ),
                              //           ],
                              //         ),
                              //       );
                              //       tabsBloc.add(const HomeTabBarRequestHidden(isHidden: true));
                              //     },
                              //   ),
                              // ),
                              // SizedBox(height: 16.h),
                              // SizedBox(
                              //   height: 56.h,
                              //   width: double.infinity,
                              //   child: SecondaryButton(
                              //     title: 'Lịch sử xe qua trạm',
                              //     onTap: () async {
                              //       await context.pushRoute(
                              //         TransactionHistoryRouter(
                              //           children: [
                              //             TransactionVehicleRoute(
                              //               plateNumber: vehicle?.plateNumber,
                              //             )
                              //           ],
                              //         ),
                              //       );
                              //       tabsBloc.add(const HomeTabBarRequestHidden(isHidden: true));
                              //     },
                              //   ),
                              // ),
                              // SizedBox(height: 16.h),
                              // SizedBox(
                              //   height: 56.h,
                              //   width: double.infinity,
                              //   child: SecondaryButton(
                              //     title: 'Mua vé tháng/quý',
                              //     onTap: () async {
                              //       if (vehicle != null) {
                              //         await context.pushRoute(BuyTicketRouter(userVehicle: [vehicle]));
                              //         tabsBloc.add(const HomeTabBarRequestHidden(isHidden: true));
                              //       }
                              //     },
                              //   ),
                              // ),
                              // SizedBox(height: 16.h),
                              // SizedBox(
                              //   height: 56.h,
                              //   width: double.infinity,
                              //   child: SecondaryButton(
                              //     title: 'Nạp tiền',
                              //     onTap: () async {
                              //       await context.pushRoute(CashInRouter());
                              //       tabsBloc.add(const HomeTabBarRequestHidden(isHidden: true));
                              //     },
                              //   ),
                              // ),
                              // SizedBox(height: 18.h),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _dialogDebtWarningBuilder(BuildContext dialogContext) {
    final bloc = dialogContext.read<VehicleDetailsBloc>();
    return showDialog<void>(
      context: dialogContext,
      builder: (_) {
        return ConfirmDialog(
          title: '',
          image: Column(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    child: const Icon(Icons.close),
                    onTap: () => Navigator.of(_).pop()),
              ),
              Image.asset(
                ImageAssests.warningDebt,
                width: 90.r,
                height: 90.h,
              )
            ],
          ),
          isSpace: true,
          content:
              'Bạn vui lòng thanh toán phí dán thẻ\n${fee != null ? fee!.formatCurrency() : ''}${fee != null ? ' VNĐ' : ''}',
          contentTextStyle: Theme.of(dialogContext)
              .textTheme
              .subtitle1!
              .copyWith(
                  color: ColorName.textGray2,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w700),
          contentTextAlign: TextAlign.center,
          secondaryButtonTitle: 'Từ chối',
          primaryButtonTitle: 'Thanh toán',
          onPrimaryButtonTap: () {
            Navigator.of(_).pop();
            bloc.add(VehicleDebitTransactionFetched(vehicleId: vehicleId));
          },
        );
      },
    );
  }

  Future<void> _dialogDebtSuccessBuilder(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          titlePadding: EdgeInsets.zero,
          content: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    child: const Icon(Icons.close),
                    onTap: () => Navigator.of(context).pop()),
              ),
              Center(
                child: Image.asset(
                  ImageAssests.successDebt,
                  width: 90.r,
                  height: 90.h,
                ),
              ),
              SizedBox(
                height: 12.h,
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                  child: Text(
                    'Kích hoạt thẻ thành công!',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: ColorName.textGray2,
                          fontSize: 18.sp,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            Row(
              children: [
                const Expanded(
                  flex: 3,
                  child: SizedBox.shrink(),
                ),
                Expanded(
                  flex: 4,
                  child: SizedBox(
                    height: 50.h,
                    child: SecondaryButton(
                      title: 'Quay lại',
                      onTap: () => Navigator.pop(context),
                    ),
                  ),
                ),
                const Expanded(
                  flex: 3,
                  child: SizedBox.shrink(),
                ),
              ],
            )
          ],
        );
      },
    );
  }
}
