import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home/bloc/viettel_money_link_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViettelMoneyCardUnlinked extends StatelessWidget {
  const ViettelMoneyCardUnlinked({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 0.h),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(right: 24.w),
            child: Assets.icons.viettelMoneyFull.svg(),
          ),
          SizedBox(height: 10.h),
          RichText(
            //quý khách có thể thanh toán phí qua trạm trực tiếp qua tài khoản viettel money
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
            text: TextSpan(
              text: 'Quý khách có thể thanh toán phí qua trạm trực tiếp bằng tài khoản\n',
              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                fontSize: 15.sp,
                color: Colors.black,
              ),
              children: <TextSpan>[
                TextSpan(
                  text: 'Viettel Money',
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    fontSize: 15.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                // const TextSpan(
                //   text: ' giúp tài xế chủ động rút nạp, không lo đọng tiền trong\n tài khoản giao thông.',
                // ),
              ],
            ),
          ),
          TextButton.icon(
            onPressed: () {
              final user = context.read<AppBloc>().state.user;
              final contractId = user?.contractId;
              final contractNo = user?.contractNo;
              final userName = user?.userName;

              if (contractId != null && contractNo != null && userName != null) {
                context.read<ViettelMoneyLinkBloc>().add(ViettelMoneyLinkAccount(
                  contractId: contractId,
                  contractNo: contractNo,
                  contractFullName: userName,
                ));
              }
            },
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              padding: EdgeInsets.symmetric(horizontal: 16.w),
            ),
            label: Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Assets.icons.chevronVtmoneyRight.svg(),
            ),
            icon: Text(
              'Liên kết ngay',
              style: Theme.of(context).textTheme.button!.copyWith(
                fontSize: 15.sp,
                fontWeight: FontWeight.bold,
                color: ColorName.textMainColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
