// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_total.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageTotalResponse _$MessageTotalResponseFromJson(
        Map<String, dynamic> json) =>
    MessageTotalResponse(
      json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MessageTotalResponseToJson(
        MessageTotalResponse instance) =>
    <String, dynamic>{
      'mess': instance.mess,
    };
