// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ticket_price_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TicketPriceRequest _$TicketPriceRequestFromJson(Map<String, dynamic> json) =>
    TicketPriceRequest(
      quantity: json['quantity'] as int?,
      seatNumber: json['seatNumber'] as int?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      stationId: json['stationId'] as int?,
      stageId: json['stageId'] as int?,
      autoRenew: json['autoRenew'] as int?,
      stationType: json['stationType'] as int?,
      expDate: json['expDate'] as String?,
      chargeMethodId:
          $enumDecodeNullable(_$MethodChargeIntEnumMap, json['chargeMethodId']),
      epc: json['epc'] as String?,
      booCode: $enumDecodeNullable(_$BooCodeEnumMap, json['booCode']),
      effDate: json['effDate'] as String?,
      vehicleId: json['vehicleId'] as int?,
      vehicleGroupId: json['vehicleGroupId'] as int?,
      servicePlanTypeId: $enumDecodeNullable(
          _$ServicePlanTypeEnumMap, json['servicePlanTypeId']),
      status: json['status'] as int?,
      plateNumber: json['plateNumber'] as String?,
      netWeight: (json['netWeight'] as num?)?.toDouble(),
      cargoWeight: (json['cargoWeight'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$TicketPriceRequestToJson(TicketPriceRequest instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('quantity', instance.quantity);
  writeNotNull('seatNumber', instance.seatNumber);
  writeNotNull('vehicleTypeId', instance.vehicleTypeId);
  writeNotNull('stationId', instance.stationId);
  writeNotNull('stageId', instance.stageId);
  writeNotNull('autoRenew', instance.autoRenew);
  writeNotNull('stationType', instance.stationType);
  writeNotNull('expDate', instance.expDate);
  writeNotNull(
      'chargeMethodId', _$MethodChargeIntEnumMap[instance.chargeMethodId]);
  writeNotNull('epc', instance.epc);
  writeNotNull('booCode', _$BooCodeEnumMap[instance.booCode]);
  writeNotNull('effDate', instance.effDate);
  writeNotNull('vehicleId', instance.vehicleId);
  writeNotNull('vehicleGroupId', instance.vehicleGroupId);
  writeNotNull('servicePlanTypeId',
      _$ServicePlanTypeEnumMap[instance.servicePlanTypeId]);
  writeNotNull('status', instance.status);
  writeNotNull('plateNumber', instance.plateNumber);
  writeNotNull('cargoWeight', instance.cargoWeight);
  writeNotNull('netWeight', instance.netWeight);
  return val;
}

const _$MethodChargeIntEnumMap = {
  MethodChargeInt.normal: 1,
  MethodChargeInt.block: 2,
  MethodChargeInt.unknown: 'unknown',
};

const _$BooCodeEnumMap = {
  BooCode.boo1: 'BOO1',
  BooCode.boo2: 'BOO2',
  BooCode.unknown: 'unknown',
};

const _$ServicePlanTypeEnumMap = {
  ServicePlanType.daily: 1,
  ServicePlanType.monthly: 4,
  ServicePlanType.quarterly: 5,
  ServicePlanType.other: 'other',
};
