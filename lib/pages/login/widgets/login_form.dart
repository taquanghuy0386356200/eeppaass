import 'package:epass/commons/enum/local_authentication_type.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/password_text_field.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/app.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/login/bloc/biometric_login_bloc.dart';
import 'package:epass/pages/login/bloc/login_form_bloc.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:auto_route/auto_route.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _usernameController;

  late TextEditingController _passwordController;

  late LoginFormBloc _loginBloc;
  late SettingBloc _settingBloc;

  final FocusNode _passwordFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _usernameController = TextEditingController(text: '');
    _passwordController = TextEditingController(text: '');
    _loginBloc = context.read<LoginFormBloc>();
    _settingBloc = context.read<SettingBloc>();
    context.read<LoginFormBloc>().add(const LoginFormContractNoFetched());
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginFormBloc, LoginFormState>(
      listener: (context, state) {
        if (state is AuthenticationFailure) {
          if (_loginBloc.countShowForgotPW == 3) {
            showDialog(
              context: context,
              barrierDismissible: true,
              builder: (_) {
                return ConfirmDialog(
                  title: '',
                  isSpace: true,
                  contentTextAlign: TextAlign.center,
                  content:
                      "Bạn có muốn lấy lại thông tin tài khoản đăng nhập ứng dụng?",
                  primaryButtonTitle: 'Lấy lại mật khẩu',
                  secondaryButtonTitle: 'Đóng',
                  onSecondaryButtonTap: () {
                    _loginBloc.countShowForgotPW = 0;
                    Navigator.of(_).pop();
                  },
                  onPrimaryButtonTap: () async {
                    _loginBloc.countShowForgotPW = 0;
                    Navigator.of(_).pop();
                    context.pushRoute(const ForgotPasswordRouter());
                  },
                );
              },
            );
          }
          showErrorSnackBBar(
            context: context,
            message: state.message,
          );
        } else if (state is AuthenticationSuccess) {
          // Remove current snackbar
          ScaffoldMessenger.of(context).removeCurrentSnackBar();

          // fetch notification
          context.read<NotificationBloc>().add(const NotificationFetched());

          // redirect to home
          context.replaceRoute(const HomeTabsRoute());
        } else if (state is LoginFormContractNoFetchedCompleted) {
          _usernameController.text = state.contractNo ?? '';
          // if ((canNotShowPopupUpdateApp == false) &&
          //     _usernameController.text.isEmpty) {
          //   showDialog(
          //     context: context,
          //     barrierDismissible: true,
          //     builder: (_) {
          //       return ConfirmDialog(
          //         title: '',
          //         isSpace: true,
          //         contentTextAlign: TextAlign.center,
          //         content:
          //             "Quý khách có thể lấy lại tài khoản qua số điện thoại",
          //         primaryButtonTitle: 'Lấy lại mật khẩu',
          //         secondaryButtonTitle: 'Đóng',
          //         onSecondaryButtonTap: () {
          //           Navigator.of(_).pop();
          //         },
          //         onPrimaryButtonTap: () async {
          //           Navigator.of(_).pop();
          //           context.pushRoute(const ForgotPasswordRouter());
          //         },
          //       );
          //     },
          //   );
          // }
        }
      },
      child: Form(
        key: _formKey,
        child: BlocBuilder<LoginFormBloc, LoginFormState>(
          builder: (context, state) {
            return Column(
              children: [
                SizedBox(height: 16.h),
                PrimaryTextField(
                  controller: _usernameController,
                  enabled: state is! LoginFormLoading,
                  labelText: 'Tên đăng nhập',
                  hintText: 'Nhập tên đăng nhập',
                  validator: context.read<LoginFormBloc>().usernameValidator,
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(_passwordFocusNode);
                  },
                ),
                SizedBox(height: 20.h),
                PasswordTextField(
                  controller: _passwordController,
                  enabled: state is! LoginFormLoading,
                  labelText: 'Mật khẩu',
                  hintText: 'Nhập mật khẩu',
                  validator: context.read<LoginFormBloc>().passwordValidator,
                  focusNode: _passwordFocusNode,
                  onFieldSubmitted: (value) {
                    FocusScope.of(context).unfocus();
                    if (_formKey.currentState!.validate()) {
                      context.read<LoginFormBloc>().add(
                            LoginFormSubmittedEvent(
                              _usernameController.text,
                              _passwordController.text,
                            ),
                          );
                    }
                  },
                ),
                SizedBox(height: 14.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Spacer(),
                    TextButton(
                      onPressed: state is! LoginFormLoading
                          ? () =>
                              context.pushRoute(const ForgotPasswordRouter())
                          : null,
                      child: Text(
                        'Quên mật khẩu?',
                        style: Theme.of(context).textTheme.subtitle1!.copyWith(
                              color: ColorName.linkBlue,
                              decoration: TextDecoration.underline,
                            ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 32.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: LoadingPrimaryButton(
                        title: 'Đăng nhập',
                        height: 56.h,
                        isLoading: state is LoginFormLoading,
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          if (_formKey.currentState!.validate()) {
                            context.read<LoginFormBloc>().add(
                                  LoginFormSubmittedEvent(
                                    _usernameController.text,
                                    _passwordController.text,
                                  ),
                                );
                          }
                        },
                      ),
                    ),

                    SizedBox(width: 8.w),

                    // BIOMETRIC LOGIN
                    BlocBuilder<SettingBloc, SettingState>(
                      builder: (context, settingState) {
                        return BlocConsumer<BiometricLoginBloc,
                            BiometricLoginState>(
                          listener: (context, state) {
                            if (state is BiometricAuthenticatedFailure) {
                              showErrorSnackBBar(
                                  context: context, message: state.message);
                            } else if (state is BiometricAuthenticatedSuccess) {
                              context
                                  .read<LoginFormBloc>()
                                  .add(LoginFormSubmittedEvent(
                                    state.username,
                                    state.password,
                                  ));
                            }
                          },
                          builder: (context, state) {
                            if (settingState.isBiometricLoginEnabled &&
                                state
                                    is GetBiometricAuthenticationTypeSuccess) {
                              return SplashIconButton(
                                icon: state.authenticationType.getIcon(
                                  color: ColorName.primaryColor,
                                  width: 36.r,
                                  height: 36.r,
                                ),
                                onTap: () => context
                                    .read<BiometricLoginBloc>()
                                    .add(BiometricAuthenticated()),
                              );
                            }

                            return const SizedBox.shrink();
                          },
                        );
                      },
                    ),
                  ],
                ),
                SizedBox(height: 24.h),
                Visibility(
                  visible: false,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Chưa có tài khoản?',
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(width: 2.w),
                      TextButton(
                        onPressed: state is! LoginFormLoading ? () {} : null,
                        child: Text(
                          'Đăng ký',
                          style:
                              Theme.of(context).textTheme.subtitle1!.copyWith(
                                    color: ColorName.linkBlue,
                                    decoration: TextDecoration.underline,
                                  ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
