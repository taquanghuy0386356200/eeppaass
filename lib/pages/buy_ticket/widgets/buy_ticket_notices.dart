import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BuyTicketNotices extends StatelessWidget {
  const BuyTicketNotices({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TicketPriceBloc, TicketPriceState>(
      builder: (context, state) {
        if (state is TicketPriceFetchedSuccess) {
          final messages = state.ticketPrices
              .where((element) => element.message != null)
              .map((e) => Padding(
                    padding: EdgeInsets.only(bottom: 8.h),
                    child: Text(
                      e.message!,
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontSize: 15.sp,
                          ),
                    ),
                  ))
              .toList();

          if (messages.isNotEmpty) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(width: 6.w),
                Icon(
                  CupertinoIcons.exclamationmark_circle_fill,
                  color: ColorName.error,
                  size: 24.r,
                ),
                SizedBox(width: 12.w),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: messages,
                  ),
                ),
                SizedBox(width: 6.w),
              ],
            );
          }
        }
        return const SizedBox.shrink();
      },
    );
  }
}
