import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/models/buy_ticket/cart.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_response.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/container/footer_container.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/buy_ticket_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/service_plan_type_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/station_stage_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_duration_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_amount.dart';
import 'package:epass/pages/buy_ticket/widgets/buy_ticket_confirm/widget/ticket_confirm_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:loader_overlay/loader_overlay.dart';

class BuyTicketConfirmPage extends StatelessWidget {
  const BuyTicketConfirmPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final stationStage = context.read<StationStageBloc>().state.stationStage;
    final servicePlanType =
        context.read<ServicePlanTypeBloc>().state.servicePlanType;
    final effectiveDate =
        context.read<TicketDurationBloc>().state.effectiveDate ??
            DateTime.now();
    final expireDate =
        context.read<TicketDurationBloc>().state.expireDate ?? DateTime.now();
    final effDateStr = Jiffy(effectiveDate).format('dd/MM/yyyy');
    final expDateStr = Jiffy(expireDate).format('dd/MM/yyyy');

    return BlocListener<BuyTicketBloc, BuyTicketState>(
      listener: (context, state) {
        if (state is BuyTicketConfirmedInProgress) {
          context.loaderOverlay.show();
        } else if (state is BuyTicketConfirmedSuccess) {
          context.loaderOverlay.hide();
          context.replaceRoute(const BuyTicketResultRouter());
        } else if (state is BuyTicketConfirmedFailure) {
          context.loaderOverlay.hide();
          showErrorSnackBBar(context: context, message: state.message);
        }
      },
      child: BasePage(
        title: 'Xác nhận Mua vé',
        backgroundColor: Colors.white,
        child: Stack(
          children: [
            const FadeAnimation(
              delay: 0.5,
              child: GradientHeaderContainer(),
            ),
            SafeArea(
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: FadeAnimation(
                  direction: FadeDirection.up,
                  delay: 1,
                  child: RoundedTopContainer(
                    margin: EdgeInsets.only(top: 16.h),
                    padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                    // Your widget go below
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          stationStage?.name ?? '',
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: ColorName.primaryColor,
                                  ),
                        ),
                        SizedBox(height: 16.h),
                        Row(
                          children: [
                            Assets.icons.ticketSm.svg(),
                            SizedBox(width: 8.w),
                            Text(
                              servicePlanType.label,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ],
                        ),
                        SizedBox(height: 4.h),
                        Row(
                          children: [
                            Assets.icons.clockSm.svg(),
                            SizedBox(width: 8.w),
                            Text(
                              '$effDateStr - $expDateStr',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ],
                        ),
                        SizedBox(height: 24.h),
                        Text(
                          'Thông tin xe',
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: ColorName.primaryColor,
                                  ),
                        ),
                        const TicketConfirmList(),
                        SizedBox(height: 100.h),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            // FOOTER
            Align(
              alignment: Alignment.bottomCenter,
              child: FooterContainer(
                height: 100.h,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  // mainAxisAlignment:
                  //     MainAxisAlignment.spaceBetween,
                  children: [
                    const BuyTicketAmount(),
                    const Spacer(),
                    Expanded(
                      flex: 3,
                      child: SizedBox(
                        height: 56.h,
                        child: PrimaryButton(
                          title: 'Thanh toán',
                          onTap: () => _onPaymentTap(context),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onPaymentTap(BuildContext context) {
    final ticketPricesState = context.read<TicketPriceBloc>().state;
    var ticketPrices = <TicketPrice>[];
    if (ticketPricesState is TicketPriceFetchedSuccess) {
      ticketPrices = ticketPricesState.ticketPrices;
    }

    final stationStage = context.read<StationStageBloc>().state.stationStage;
    final userVehicles = context.read<TicketVehicleBloc>().state.userVehicles;
    final otherVehicles = context.read<TicketVehicleBloc>().state.otherVehicles;
    final vehicles = [...userVehicles, ...otherVehicles];

    final servicePlanType =
        context.read<ServicePlanTypeBloc>().state.servicePlanType;

    final effDate = context.read<TicketDurationBloc>().state.effectiveDate;
    final expDate = context.read<TicketDurationBloc>().state.expireDate;

    if (ticketPrices.isNotEmpty &&
        vehicles.isNotEmpty &&
        stationStage != null &&
        effDate != null &&
        expDate != null) {
      final cartItems = ticketPrices.map((e) {
        final vehicle =
            vehicles.firstWhere((element) => e.vehicleId == element.vehicleId);
        return CartItem(
          ticketPrice: e,
          vehicle: vehicle,
          stationStage: stationStage,
          servicePlanType: servicePlanType,
          effDate: effDate,
          expDate: expDate,
        );
      }).toList();

      context.read<BuyTicketBloc>().add(BuyTicketConfirmed(cartItems));
    }
  }
}
