// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_service_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddServiceModel _$AddServiceModelFromJson(Map<String, dynamic> json) =>
    AddServiceModel(
      mapId: json['mapId'] as int,
      contractId: json['contractId'] as int,
      serviceId: json['serviceId'] as int,
      status: json['status'] as int,
    );

Map<String, dynamic> _$AddServiceModelToJson(AddServiceModel instance) =>
    <String, dynamic>{
      'mapId': instance.mapId,
      'contractId': instance.contractId,
      'serviceId': instance.serviceId,
      'status': instance.status,
    };

AddServiceResponse _$AddServiceResponseFromJson(Map<String, dynamic> json) =>
    AddServiceResponse(
      data: (json['data'] as List<dynamic>)
          .map((e) => AddServiceModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AddServiceResponseToJson(AddServiceResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
