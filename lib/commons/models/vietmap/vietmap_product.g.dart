// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vietmap_product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VietMapProductResponse _$VietMapProductResponseFromJson(
        Map<String, dynamic> json) =>
    VietMapProductResponse(
      data: json['data'] == null
          ? null
          : VietMapProductData.fromJson(json['data'] as Map<String, dynamic>),
      error: json['error'] as String?,
    );

Map<String, dynamic> _$VietMapProductResponseToJson(
        VietMapProductResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'error': instance.error,
    };

VietMapProductData _$VietMapProductDataFromJson(Map<String, dynamic> json) =>
    VietMapProductData(
      results: (json['results'] as List<dynamic>?)
          ?.map((e) => VietMapProduct.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$VietMapProductDataToJson(VietMapProductData instance) =>
    <String, dynamic>{
      'results': instance.results,
    };

VietMapProduct _$VietMapProductFromJson(Map<String, dynamic> json) =>
    VietMapProduct(
      id: json['id'] as String?,
      name: json['name'] as String?,
      thumbnail: json['thumbnail'] as String?,
      shortDescription: json['shortDescription'] as String?,
      description: json['description'] as String?,
      price: json['price'] as int?,
      oldPrice: json['oldPrice'] as int?,
      categoryLayoutId: json['categoryLayoutId'] as int?,
      productDetailUrl: json['productDetailUrl'] as String?,
    );

Map<String, dynamic> _$VietMapProductToJson(VietMapProduct instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'thumbnail': instance.thumbnail,
      'shortDescription': instance.shortDescription,
      'description': instance.description,
      'price': instance.price,
      'oldPrice': instance.oldPrice,
      'categoryLayoutId': instance.categoryLayoutId,
      'productDetailUrl': instance.productDetailUrl,
    };
