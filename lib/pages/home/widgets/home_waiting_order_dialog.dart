import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/waiting_order/waiting_order.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WaitingOrderDialog extends StatefulWidget {
  final List<WaitingOrder> waitingOrders;
  final VoidCallback onConfirmButtonTap;

  const WaitingOrderDialog({
    Key? key,
    required this.waitingOrders,
    required this.onConfirmButtonTap,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WaitingOrderDialogState();
}

class _WaitingOrderDialogState extends State<WaitingOrderDialog> {
  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return Colors.red;
  }

  var isRefuseAll = false;
  var isAgreeAll = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12.w),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 12.w,
            vertical: 24.h,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Assets.images.home.waitingOrder.image(
                height: 80.h,
                width: 80.w,
              ),
              SizedBox(height: 16.h),
              Text(
                'Bạn có đơn hàng chờ xác nhận thanh toán. Vui lòng chọn và xác nhận.',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 16.h),
              Row(
                children: [
                  SizedBox(
                    width: 50.w,
                    child: Text(
                      'Từ chối',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        fontSize: 14.sp,
                      ),
                    ),
                  ),
                  const SizedBox(
                      height: 40,
                      child: VerticalDivider(color: ColorName.textGray)),
                  SizedBox(
                    width: 50.w,
                    child: Text(
                      'Đồng ý',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        fontSize: 14.sp,
                        color: ColorName.primaryColor
                      ),
                    ),
                  ),
                  const SizedBox(
                      height: 40,
                      child: VerticalDivider(color: ColorName.textGray)),
                  Expanded(
                      child: Text(
                        'Nội dung đơn hàng',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          fontSize: 14.sp,
                        ),
                      ))
                ],
              ),
              SizedBox(
                height: 1.h,
                child: const Divider(
                  color: ColorName.textGray,
                ),
              ),
              Flexible(child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 50.w,
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                isRefuseAll = !isRefuseAll;
                                if (isRefuseAll){
                                  isAgreeAll = false;
                                  for (var order in widget.waitingOrders) {
                                    order.isRefuse = true;
                                    order.isAgree = false;
                                  }
                                }
                              });
                            },
                            child: isRefuseAll
                                ? Assets.icons.checkboxChecked.svg(
                                height: 20.h,
                                width: 20.w,
                                color: ColorName.logoGray)
                                : Assets.icons.checkboxUnchecked.svg(
                                height: 20.h,
                                width: 20.w,
                                color: ColorName.logoGray),
                          ),
                        ),
                        const SizedBox(
                            height: 40,
                            child: VerticalDivider(color: ColorName.textGray)),
                        SizedBox(
                          width: 50.w,
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                isAgreeAll = !isAgreeAll;
                                if (isAgreeAll){
                                  isRefuseAll = false;
                                  for (var order in widget.waitingOrders) {
                                    order.isAgree = true;
                                    order.isRefuse = false;
                                  }
                                }
                              });
                            },
                            child: isAgreeAll
                                ? Assets.icons.checkboxChecked.svg(
                              height: 20.h,
                              width: 20.w,
                            )
                                : Assets.icons.checkboxUnchecked.svg(
                              height: 20.h,
                              width: 20.w,
                            ),
                          ),
                        ),
                        const SizedBox(
                            height: 40,
                            child: VerticalDivider(color: ColorName.textGray)),
                        Expanded(
                            child: Text(
                              'Tất cả',
                              textAlign: TextAlign.left,
                              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                fontSize: 14.sp,
                              ),
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                      child: const Divider(
                        color: ColorName.textGray,
                      ),
                    ),
                    ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      itemCount: widget.waitingOrders.length,
                      itemBuilder: (context, index) {
                        final item = widget.waitingOrders[index];
                        return Column(
                          children: [
                            IntrinsicHeight(
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 50.w,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          item.isRefuse = !item.isRefuse;
                                          if (item.isRefuse){
                                            item.isAgree = false;
                                            isAgreeAll = false;
                                          }else{
                                            isRefuseAll = false;
                                          }
                                        });
                                      },
                                      child: item.isRefuse
                                          ? Assets.icons.checkboxChecked.svg(
                                          height: 20.h,
                                          width: 20.w,
                                          color: ColorName.logoGray)
                                          : Assets.icons.checkboxUnchecked.svg(
                                          height: 20.h,
                                          width: 20.w,
                                          color: ColorName.logoGray),
                                    ),
                                  ),
                                  const VerticalDivider(color: ColorName.textGray),
                                  SizedBox(
                                    width: 50.w,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          item.isAgree = !item.isAgree;
                                          if (item.isAgree){
                                            item.isRefuse = false;
                                            isRefuseAll = false;
                                          }else{
                                            isAgreeAll = false;
                                          }
                                        });
                                      },
                                      child: item.isAgree
                                          ? Assets.icons.checkboxChecked.svg(
                                        height: 20.h,
                                        width: 20.w,
                                      )
                                          : Assets.icons.checkboxUnchecked.svg(
                                        height: 20.h,
                                        width: 20.w,
                                      ),
                                    ),
                                  ),
                                  const VerticalDivider(color: ColorName.textGray),
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children:[
                                          SizedBox(height: 8.h),
                                          Text(
                                            'Tại: ${item.serviceName ?? ''} ${item.plateNumber ?? ''}',
                                            textAlign: TextAlign.left,
                                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                          Text(
                                            'Tại: ${item.stationName ?? ''} - ${item.laneOutName ?? ''}',
                                            textAlign: TextAlign.left,
                                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                          Text(
                                            'Giá: ${(item.amount ?? 0).formatCurrency(symbol: ' VNĐ')}',
                                            textAlign: TextAlign.left,
                                            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                              fontSize: 14.sp,
                                            ),
                                          ),
                                          SizedBox(height: 8.h),
                                        ],
                                      ))
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 1.h,
                              child: const Divider(
                                color: ColorName.textGray,
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
              )),
              SizedBox(height: 16.h),
              SizedBox(
                height: 50.h,
                width: 150.w,
                child: PrimaryButton(
                  onTap: () {
                    if ( widget.waitingOrders.any((element) => element.isRefuse)
                        || widget.waitingOrders.any((element) => element.isAgree)){
                      widget.onConfirmButtonTap.call();
                    }
                  },
                  title: 'Xác nhận',
                  enabled: widget.waitingOrders.any((element) => element.isRefuse)
                  || widget.waitingOrders.any((element) => element.isAgree) ,
                ),
              ),
              // SizedBox(height: 8.h),
            ],
          ),
        ),
      ),
    );
  }
}
