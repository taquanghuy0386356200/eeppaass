import 'package:encrypt/encrypt.dart';
import 'package:epass/flavors.dart';

abstract class IEncryption {
  String encrypt(String input);
  String decrypt(String input);
}

class AESEncryption extends IEncryption {
  late Encrypter _encrypter;
  final _iv = IV.fromLength(16);

  AESEncryption() {
    final key = Key.fromBase64(F.ePassSecret);
    _encrypter = Encrypter(AES(key));
  }

  @override
  String decrypt(String input) {
    return _encrypter.decrypt64(input, iv: _iv);
  }

  @override
  String encrypt(String input) {
    return _encrypter.encrypt(input, iv: _iv).base64;
  }
}
