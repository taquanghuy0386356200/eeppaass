import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/notification_tab/pages/event/widget/event_notification_card.dart';
import 'package:epass/pages/notification_tab/pages/event/widget/event_notification_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/gen/assets.gen.dart';

class BalanceChangePage extends StatefulWidget {
  const BalanceChangePage({Key? key}) : super(key: key);

  @override
  State<BalanceChangePage> createState() => _BalanceChangePageState();
}

class _BalanceChangePageState extends State<BalanceChangePage> {
  late RefreshController _refreshController;
  final  _searchController  = TextEditingController();

  @override
  void initState() {
    super.initState();
    context.read<BalanceChangeNotificationBloc>().add(const NotificationFetched(notificationType: 2));
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BalanceChangeNotificationBloc, NotificationState>(
      listener: (context, state) {
        if (state.error == null) {
          _refreshController.refreshCompleted();
          _refreshController.loadComplete();
        } else if (state.error != null) {
          _refreshController.refreshFailed();
          _refreshController.loadFailed();

          if (state.listData.isNotEmpty) {
            showErrorSnackBBar(
              context: context,
              message: state.error ?? 'Có lỗi xảy ra',
            );
          }
        }
      },
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 24.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: PrimaryTextField(
                hintText: 'Tìm kiếm',
                controller: _searchController,
                onClear: (){
                  context.read<BalanceChangeNotificationBloc>().add( NotificationSearched(_searchController.text));
                },
                onChanged: (value) => context.read<BalanceChangeNotificationBloc>().add(NotificationSearched(value)),
                suffix: Assets.icons.search.svg(
                  width: 24.r,
                  height: 24.r,
                  color: ColorName.borderColor,
                ),
              ),
            ),
            SizedBox(height: 16.h),
            Expanded(
              child: Builder(
                builder: (_) {
                  if (state.isLoading || state.isRefreshing && state.listData.isEmpty) {
                    return const EventNotificationShimmerLoading();
                  } else if (state.error != null && state.listData.isEmpty) {
                    return Center(
                      child: Text(state.error ?? 'Có lỗi xảy ra'),
                    );
                  }
                  final listData = state.listData;
                  if (listData.isEmpty) {
                    return NoDataPage(
                      onTap: () => context.read<BalanceChangeNotificationBloc>().add(const NotificationRefreshed()),
                      title: '',
                      description: 'Bạn chưa có giao dịch nào phát sinh',
                    );
                  }

                  return SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    enablePullUp: !state.isFull,
                    onRefresh: () => context.read<BalanceChangeNotificationBloc>().add(const NotificationRefreshed()),
                    onLoading: () => context.read<BalanceChangeNotificationBloc>().add(const NotificationLoadMore()),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        final notification = listData[index];
                        return EventNotificationCard(
                          notification: notification,
                          onTap: () {
                            final notificationId = notification.notificationMsgId;
                            if (notificationId != null) {
                              context.read<BalanceChangeNotificationBloc>().add(NotificationRead(notificationId));
                            }
                          },
                        );
                      },
                      separatorBuilder: (context, index) => Divider(
                        color: ColorName.disabledBorderColor,
                        height: 0.5,
                        thickness: 0.5,
                        indent: 60.w,
                        endIndent: 24.w,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
