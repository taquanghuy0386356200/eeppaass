import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:equatable/equatable.dart';

part 'forgot_password_event.dart';

part 'forgot_password_state.dart';

class ForgotPasswordBloc extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  final IUserRepository _userRepository;

  ForgotPasswordBloc({required IUserRepository userRepository})
      : _userRepository = userRepository,
        super(ForgotPasswordInitial()) {
    on<ForgotPasswordSubmitted>(_onForgotPasswordSubmitted);
  }

  FutureOr<void> _onForgotPasswordSubmitted(
    ForgotPasswordSubmitted event,
    Emitter<ForgotPasswordState> emit,
  ) async {
    emit(ForgotPasswordSubmittedInProgress());

    final result = await _userRepository.requestOTPResetPassword(
      phoneNumber: event.phoneNumber,
      plateNumber: event.plateNumber,
      plateTypeCode: event.plateTypeCode,
    );

    result.when(
      success: (success) => emit(ForgotPasswordSubmittedSuccess(
        resubmitEvent: event,
        submittedSuccessTime: DateTime.now(),
      )),
      failure: (failure) => emit(
        ForgotPasswordSubmittedFailure(
          message: failure.message ?? 'Có lỗi xảy ra',
          resubmitEvent: event,
        ),
      ),
    );
  }
}
