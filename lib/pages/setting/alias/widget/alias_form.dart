import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/pages/setting/alias/bloc/alias_form_bloc.dart';
import 'package:epass/pages/setting/alias/validator/alias_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AliasForm extends StatefulWidget {
  final String? currentAlias;
  final VoidCallback? onCancelTap;

  const AliasForm({
    Key? key,
    this.currentAlias,
    this.onCancelTap,
  }) : super(key: key);

  @override
  State<AliasForm> createState() => _AliasFormState();
}

class _AliasFormState extends State<AliasForm> with AliasValidator {
  late TextEditingController _aliasController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _aliasController = TextEditingController(text: widget.currentAlias);
    super.initState();
  }

  @override
  void dispose() {
    _aliasController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AliasFormBloc, AliasFormState>(
      builder: (context, state) {
        return Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (widget.currentAlias != null)
                Text(
                  'Bí danh hiện tại của bạn là: ',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              if (widget.currentAlias != null) SizedBox(height: 20.h),
              PrimaryTextField(
                controller: _aliasController,
                autofocus: true,
                labelText: 'Bí danh',
                hintText: 'Nhập bí danh',
                validator: aliasValidator,
                hasClearButton: true,
                enabled: state is! AliasFormSubmittedInProgress,
              ),
              SizedBox(height: 20.h),
              FadeAnimation(
                delay: 0.2,
                child: Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        width: double.infinity,
                        child: SecondaryButton(
                          onTap: widget.onCancelTap,
                        ),
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        width: double.infinity,
                        child: LoadingPrimaryButton(
                          title: 'Lưu',
                          isLoading: state is AliasFormSubmittedInProgress,
                          onTap: () {
                            if (_formKey.currentState!.validate()) {
                              context.read<AliasFormBloc>().add(
                                  AliasFormSubmitted(_aliasController.text));
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
