// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_parking.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionPacking _$TransactionPackingFromJson(Map<String, dynamic> json) =>
    TransactionPacking(
      code: json['code'] as String?,
      serviceName: json['serviceName'] as String?,
      stationName: json['stationName'] as String?,
      lanOut: json['lanOut'] as String?,
      typeTicket: json['typeTicket'] as String?,
      timeRequestIn: json['timeRequestIn'] as String?,
      timeRequestOut: json['timeRequestOut'] as String?,
      fee: json['fee'] as String?,
      statusOrder: json['statusOrder'] as String?,
      plateNumber: json['plateNumber'] as String?,
      timeEditTransaction: json['timeEditTransaction'] as String?,
      moneyEditTransaction: json['moneyEditTransaction'] as int?,
      timeCreateTransaction: json['timeCreateTransaction'] as String?,
      statusTransaction: json['statusTransaction'] as String?,
      fuelType: json['fuelType'] as String?,
      quantity: (json['quantity'] as num?)?.toDouble(),
      unitPrice: (json['unitPrice'] as num?)?.toDouble(),
      isDelete: json['isDelete'] as int?,
    );

Map<String, dynamic> _$TransactionPackingToJson(TransactionPacking instance) =>
    <String, dynamic>{
      'code': instance.code,
      'serviceName': instance.serviceName,
      'stationName': instance.stationName,
      'lanOut': instance.lanOut,
      'typeTicket': instance.typeTicket,
      'timeRequestIn': instance.timeRequestIn,
      'timeRequestOut': instance.timeRequestOut,
      'fee': instance.fee,
      'statusOrder': instance.statusOrder,
      'plateNumber': instance.plateNumber,
      'timeEditTransaction': instance.timeEditTransaction,
      'moneyEditTransaction': instance.moneyEditTransaction,
      'timeCreateTransaction': instance.timeCreateTransaction,
      'statusTransaction': instance.statusTransaction,
      'fuelType': instance.fuelType,
      'quantity': instance.quantity,
      'unitPrice': instance.unitPrice,
      'isDelete': instance.isDelete,
    };

TransactionPackingData _$TransactionPackingDataFromJson(
        Map<String, dynamic> json) =>
    TransactionPackingData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => TransactionPacking.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$TransactionPackingDataToJson(
        TransactionPackingData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

TransactionPackingResponse _$TransactionPackingResponseFromJson(
        Map<String, dynamic> json) =>
    TransactionPackingResponse(
      data: json['data'] == null
          ? null
          : TransactionPackingData.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TransactionPackingResponseToJson(
        TransactionPackingResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
