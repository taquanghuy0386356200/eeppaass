import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/repo/address_repository.dart';
import 'package:epass/pages/register/pages/register_address/widgets/address_dropdown/address_dropdown.dart';
import 'package:equatable/equatable.dart';
import 'package:simple_result/simple_result.dart';

part 'address_dropdown_event.dart';

part 'address_dropdown_state.dart';

class AddressDropdownBloc extends Bloc<AddressDropdownEvent, AddressDropdownState> {
  final IAddressRepository _addressRepository;

  AddressDropdownBloc({required IAddressRepository addressRepository})
      : _addressRepository = addressRepository,
        super(AddressDropdownInitial()) {
    on<AddressVTTDropdownFetched>(_onAddressVTTDropdownFetched);
  }

  FutureOr<void> _onAddressVTTDropdownFetched(
    AddressVTTDropdownFetched event,
    Emitter<AddressDropdownState> emit,
  ) async {
    Result<List<AddressVTT>, Failure>? result;

    switch (event.dropdownType) {
      case AddressDropdownType.province:
        result = await _addressRepository.getProvinces();
        break;
      case AddressDropdownType.district:
        if (event.provinceCode != null) {
          result = await _addressRepository.getDistricts(provinceCode: event.provinceCode!);
        }
        break;
      case AddressDropdownType.ward:
        if (event.provinceCode != null && event.districtCode != null) {
          result = await _addressRepository.getWards(
            provinceCode: event.provinceCode!,
            districtCode: event.districtCode!,
          );
        }
        break;
    }

    if (result == null) {
      emit(const AddressVTTFetchedFailure('Có lỗi xảy ra'));
      return;
    }

    result.when(
      success: (success) => emit(AddressVTTFetchedSuccess(success)),
      failure: (failure) => emit(AddressVTTFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
