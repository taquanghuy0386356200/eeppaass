part of 'station_vehicle_alert_bloc.dart';

abstract class StationVehicleAlertEvent extends Equatable {
  const StationVehicleAlertEvent();
}

class StationVehicleAlerted extends StationVehicleAlertEvent {
  final bool? isOn;

  const StationVehicleAlerted({this.isOn});

  @override
  List<Object?> get props => [isOn];
}

