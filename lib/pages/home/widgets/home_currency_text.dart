import 'package:flutter/material.dart';

class HomeCurrencyText extends StatelessWidget {
  final String text;
  final String symbol;
  final Color? color;
  final double? fontSize;

  const HomeCurrencyText({
    Key? key,
    required this.text,
    this.color,
    this.symbol = 'đ',
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          text,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.headline5!.copyWith(
                color: color,
                fontWeight: FontWeight.w700,
                fontSize: fontSize,
              ),
        ),
        Text(
          symbol,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: color,
                fontWeight: FontWeight.w600,
              ),
        ),
      ],
    );
  }
}
