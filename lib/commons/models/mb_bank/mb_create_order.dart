import 'package:json_annotation/json_annotation.dart';

part 'mb_create_order.g.dart';

@JsonSerializable()
class MBCreateOrderRequest {
  final String authenType = 'APP_MB';
  final String transactionId;
  final String bankCode = '970422';
  final String billNumber = DateTime.now().microsecondsSinceEpoch.toString();
  final String creditAccount = '';
  final String creditName = '';
  final String deeplinkCallback = 'mbbankepass://';
  final String transAmt;
  final String discountAmt = '';
  final String feeAmt = '';
  final String paymentDetail = 'Nap tien vao tai khoan ePass';
  final String merchantId = 'VDTC';
  final String terminalId = '';
  final String prodName = 'Thu phi khong dung ePass';
  final String transChannel = 'APP';
  final String transMultiple = 'N';
  final String transType = 'INHOUSE';
  final String voucherCode = '';

  MBCreateOrderRequest({
    required this.transAmt,
    required this.transactionId,
  });

  factory MBCreateOrderRequest.fromJson(Map<String, dynamic> json) =>
      _$MBCreateOrderRequestFromJson(json);
}

@JsonSerializable()
class MBCreateOrderResponse {
  final String clientMessageId;
  final String errorCode;
  @JsonKey(name: 'data')
  final MBOrderData data;

  MBCreateOrderResponse({
    required this.clientMessageId,
    required this.errorCode,
    required this.data,
  });

  factory MBCreateOrderResponse.fromJson(Map<String, dynamic> json) =>
      _$MBCreateOrderResponseFromJson(json);
}

@JsonSerializable()
class MBOrderData {
  final String requestId;
  final String? requestToken;
  final String? oneLink;
  final String? qrString;
  final String? feePayment;
  final String? feeNoVAT;
  final String? vat;
  final DateTime? expriedTime;

  MBOrderData(
      {required this.requestId,
      required this.requestToken,
      required this.oneLink,
      required this.qrString,
      required this.feePayment,
      required this.feeNoVAT,
      required this.vat,
      required this.expriedTime});

  factory MBOrderData.fromJson(Map<String, dynamic> json) =>
      _$MBOrderDataFromJson(json);
}
