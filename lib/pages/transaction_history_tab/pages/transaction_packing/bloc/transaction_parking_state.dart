part of 'transaction_parking_bloc.dart';


class TransactionParkingState extends Equatable {
  final List<TransactionPacking> listData;
  final bool isFull;
  final String? plateNumber;
  final DateTime? startDate;
  final DateTime? endDate;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const TransactionParkingState({
    required this.listData,
    this.isFull = false,
    this.plateNumber,
    this.startDate,
    this.endDate,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        plateNumber,
        startDate,
        endDate,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  TransactionParkingState copyWith({
    List<TransactionPacking>? listData,
    bool? isFull,
    required String? plateNumber,
    required DateTime? startDate,
    required DateTime? endDate,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return TransactionParkingState(
      listData: listData ?? this.listData,
      isFull: isFull ?? this.isFull,
      plateNumber: plateNumber,
      startDate: startDate,
      endDate: endDate,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
