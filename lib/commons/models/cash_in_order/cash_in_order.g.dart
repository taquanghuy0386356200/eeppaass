// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cash_in_order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CashInOrder _$CashInOrderFromJson(Map<String, dynamic> json) => CashInOrder(
      billingCode: json['billingCode'] as int?,
    );

Map<String, dynamic> _$CashInOrderToJson(CashInOrder instance) =>
    <String, dynamic>{
      'billingCode': instance.billingCode,
    };

CashInOrderResponse _$CashInOrderResponseFromJson(Map<String, dynamic> json) =>
    CashInOrderResponse(
      order: json['data'] == null
          ? null
          : CashInOrder.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CashInOrderResponseToJson(
        CashInOrderResponse instance) =>
    <String, dynamic>{
      'data': instance.order,
      'mess': instance.mess,
    };

CashInOrderRequest _$CashInOrderRequestFromJson(Map<String, dynamic> json) =>
    CashInOrderRequest(
      contractNo: json['contractNo'] as String,
      amount: json['amount'] as int,
      receiverContractId: json['desContractId'] as String,
      quantity: json['quantity'] as int,
      paymentMethodName: json['paymentMethodName'] as String?,
    );

Map<String, dynamic> _$CashInOrderRequestToJson(CashInOrderRequest instance) =>
    <String, dynamic>{
      'contractNo': instance.contractNo,
      'amount': instance.amount,
      'desContractId': instance.receiverContractId,
      'quantity': instance.quantity,
      'paymentMethodName': instance.paymentMethodName,
    };
