import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/widgets/textfield/cupertino_picker_field.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/category/category_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PlateTypePicker extends StatelessWidget {
  final CategoryModel? selectedPlateType;
  final ValueChanged<PickerItemWithValue<CategoryModel>?>? onItemChanged;
  final bool isMandatory;

  const PlateTypePicker({
    Key? key,
    this.selectedPlateType,
    this.onItemChanged,
    this.isMandatory = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CategoryBloc(categoryRepository: getIt<ICategoryRepository>()),
      child: _PlateTypePicker(
        isMandatory: isMandatory,
        selectedPlateType: selectedPlateType,
        onItemChanged: onItemChanged,
      ),
    );
  }
}

class _PlateTypePicker extends StatefulWidget {
  final CategoryModel? selectedPlateType;
  final ValueChanged<PickerItemWithValue<CategoryModel>?>? onItemChanged;
  final bool isMandatory;

  const _PlateTypePicker({
    Key? key,
    this.selectedPlateType,
    this.onItemChanged,
    this.isMandatory = false,
  }) : super(key: key);

  @override
  State<_PlateTypePicker> createState() => _PlateTypePickerState();
}

class _PlateTypePickerState extends State<_PlateTypePicker> {
  PickerItemWithValue<CategoryModel>? _selectedPlateType;

  @override
  void initState() {
    context.read<CategoryBloc>().add(const PlateTypeFetched());

    if (widget.selectedPlateType != null) {
      _selectedPlateType = PickerItemWithValue(
        title: widget.selectedPlateType!.name ?? '',
        value: widget.selectedPlateType!,
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryState>(
      builder: (context, state) {
        List<PickerItemWithValue<CategoryModel>> items = [];
        if (state is PlateTypeFetchedSuccess) {
          items = state.plateTypes.map((e) => PickerItemWithValue(title: e.name ?? '', value: e)).toList();
        }
        return CupertinoPickerField(
          isMandatory: widget.isMandatory,
          items: items,
          defaultItem: _selectedPlateType,
          isLoading: state is PlateTypeFetchedInProgress,
          validator: (_) {
            if (_selectedPlateType == null) {
              return 'Vui lòng chọn Màu biển';
            }
            return null;
          },
          hintText: 'Chọn màu biển',
          enabled: state is! PlateTypeFetchedInProgress,
          label: 'Màu biển',
          errorText: state is PlateTypeFetchedFailure ? state.message : null,
          onItemChanged: (PickerItemWithValue<CategoryModel>? selectedItem) {
            setState(() => _selectedPlateType = selectedItem);
            widget.onItemChanged?.call(selectedItem);
          },
        );
      },
    );
  }
}
