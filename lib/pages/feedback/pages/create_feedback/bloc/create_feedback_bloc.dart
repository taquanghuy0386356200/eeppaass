import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/extensions/file_ext.dart';
import 'package:epass/commons/models/feedback/feedback_attachment_file.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:path/path.dart' as p;

part 'create_feedback_event.dart';

part 'create_feedback_state.dart';

class CreateFeedbackBloc extends Bloc<CreateFeedbackEvent, CreateFeedbackState> {
  final IFeedbackRepository _feedbackRepository;
  final AppBloc _appBloc;

  CreateFeedbackBloc({
    required IFeedbackRepository feedbackRepository,
    required AppBloc appBloc,
  })  : _feedbackRepository = feedbackRepository,
        _appBloc = appBloc,
        super(CreateFeedbackInitial()) {
    on<CreateFeedbackFormSubmitted>(_onCreateFeedbackFormSubmitted);
  }

  FutureOr<void> _onCreateFeedbackFormSubmitted(
    CreateFeedbackFormSubmitted event,
    Emitter<CreateFeedbackState> emit,
  ) async {
    emit(const CreateFeedbackFormSubmittedInProgress());

    final user = _appBloc.state.user;
    final customerId = user?.customerId;
    final contractNo = user?.contractNo;
    final contractId = user?.contractId;
    final customerTypeId = user?.cusTypeId;
    final customerName = user?.userName;
    final phoneNumber = user?.noticePhoneNumber;

    if (customerId == null ||
        contractNo == null ||
        contractId == null ||
        customerTypeId == null ||
        customerName == null ||
        phoneNumber == null) {
      emit(const CreateFeedbackFormSubmittedFailure('Có lỗi xảy ra'));
      return;
    }

    final attachmentFiles = event.filePaths.map(
      (e) {
        final file = File(e);
        final fileName = p.basename(e);
        final bytes = file.readAsBytesSync();
        final fileSize = file.size;
        bytes.length / 1024;
        final base64 = base64Encode(bytes);
        return FeedbackAttachmentFileRequest(fileName: fileName, base64Data: base64, fileSize: fileSize);
      },
    ).toList();

    final result = await _feedbackRepository.createFeedback(
      custId: customerId,
      contractNo: contractNo,
      contractId: contractId,
      custTypeId: customerTypeId.toString(),
      custName: customerName,
      phoneNumber: phoneNumber,
      phoneContact: phoneNumber,
      l1TicketTypeId: event.feedbackType.ticketTypeId.toString(),
      contentReceive: event.receiveContent,
      attachmentFiles: attachmentFiles,
      otp: event.otp,
    );

    result.when(
      success: (success) => emit(const CreateFeedbackFormSubmittedSuccess()),
      failure: (failure) => emit(CreateFeedbackFormSubmittedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
