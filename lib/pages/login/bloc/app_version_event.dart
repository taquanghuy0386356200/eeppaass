part of 'app_version_bloc.dart';

abstract class AppVersionEvent extends Equatable {
  const AppVersionEvent();
}

class AppVersionFetched extends AppVersionEvent {
  const AppVersionFetched();

  @override
  List<Object> get props => [];
}

class RegistFlagFetched extends AppVersionEvent {
  const RegistFlagFetched();

  @override
  List<Object> get props => [];
}