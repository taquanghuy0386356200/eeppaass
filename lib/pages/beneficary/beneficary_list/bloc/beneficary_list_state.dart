part of 'beneficary_list_bloc.dart';

class BeneficiaryListState extends Equatable {
  final List<Beneficiary> listData;
  final int? total;
  final bool isFull;
  final String? reminiscentName;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const BeneficiaryListState({
    required this.listData,
    this.total,
    this.isFull = false,
    this.reminiscentName,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        reminiscentName,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  BeneficiaryListState copyWith({
    List<Beneficiary>? listData,
    int? total,
    bool? isFull,
    String? reminiscentName,
    String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return BeneficiaryListState(
      listData: listData ?? this.listData,
      total: total ?? this.total,
      isFull: isFull ?? this.isFull,
      reminiscentName: reminiscentName,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
