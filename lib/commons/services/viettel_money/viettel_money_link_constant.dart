class ViettelMoneyLinkConstant {
  static const String channelName = 'com.viettel.etc.epass/viettel_money';
  static const String methodLink = 'link';
  static const String methodUnlink = 'unlink';
}