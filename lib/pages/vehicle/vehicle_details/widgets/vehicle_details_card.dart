import 'package:epass/commons/constant.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/modal/neutral_confirm_dialog.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_vehicle/widget/gradient_text.dart';
import 'package:epass/pages/vehicle/ext/vehicle_ext.dart';
import 'package:epass/pages/vehicle/vehicle_details/bloc/vehicle_details_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VehicleDetailsCard extends StatelessWidget {
  final Vehicle? vehicle;

  const VehicleDetailsCard({Key? key, this.vehicle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isRFIDSwitchEnabled = vehicle?.status == VehicleStatus.active &&
        vehicle?.activeStatus != VehicleActiveStatus.canceled &&
        vehicle?.activeStatus != VehicleActiveStatus.transfered;
    final isRFIDOn = vehicle?.status == VehicleStatus.active &&
        (vehicle?.activeStatus == VehicleActiveStatus.active ||
            vehicle?.activeStatus == VehicleActiveStatus.open);
    bool isOpenTmp = isRFIDOn; // thêm để mở rộng vùng chọn Mở thẻ eTag
    return ShadowCard(
      shadowOpacity: 0.2,
      borderRadius: 20.0,
      child: Container(
        padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 20.h),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                GradientText(
                  vehicle?.plateNumber ?? '',
                  gradient: const LinearGradient(colors: [
                    ColorName.primaryGradientStart,
                    ColorName.primaryGradientEnd,
                  ]),
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                const Spacer(),
                vehicle?.debit?.statusDebit == VehicleDebitStatus.active
                    ? const SizedBox.shrink()
                    : InkWell(
                        onTap: () async {
                          if (isRFIDSwitchEnabled) {
                            await _onRFIDSwitchChanged(context, !isOpenTmp);
                          }
                        },
                        // thêm để mở rộng vùng chọn Mở thẻ eTag
                        child: Row(
                          children: [
                            Text(
                              'Mở thẻ eTag',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    color: isRFIDSwitchEnabled
                                        ? ColorName.textGray1
                                        : ColorName.disabledBorderColor,
                                  ),
                            ),
                            SizedBox(width: 12.w),
                            CupertinoSwitch(
                              value: isRFIDOn,
                              activeColor: ColorName.primaryColor,
                              onChanged: isRFIDSwitchEnabled
                                  ? (isOpening) async =>
                                      await _onRFIDSwitchChanged(
                                          context, isOpening)
                                  : null,
                            ),
                          ],
                        ),
                      ),
              ],
            ),
            SizedBox(height: 24.h),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Loại xe',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const Spacer(),
                Text(
                  vehicle?.vehicleGroupName ?? '',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                        color: vehicle?.vehicleGroupColor,
                      ),
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
            if (vehicle?.netWeight != null) SizedBox(height: 12.h),
            if (vehicle?.seatNumber != null)
              Row(
                children: [
                  Text(
                    'Số chỗ',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  const Spacer(),
                  Text(
                    '${vehicle?.seatNumber ?? 0} chỗ',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(fontWeight: FontWeight.bold),
                    maxLines: 1,
                  ),
                ],
              ),
            if (vehicle?.seatNumber != null) SizedBox(height: 12.h),
            if (vehicle?.rfidSerial != null)
              Row(
                children: [
                  Text(
                    'Số thẻ eTag',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  const Spacer(),
                  Text(
                    vehicle?.rfidSerial ?? '',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(fontWeight: FontWeight.bold),
                    maxLines: 2,
                  ),
                ],
              ),
            if (vehicle?.rfidSerial != null) SizedBox(height: 12.h),
            if (vehicle?.activeStatus != null && vehicle?.status != null)
              Row(
                children: [
                  Text(
                    'Trạng thái',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  const Spacer(),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 6.h, horizontal: 8.w),
                    decoration: BoxDecoration(
                      color: vehicle?.backgroundStatusColor,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Text(
                      vehicle?.activeStatus?.title ?? '',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                            color: vehicle?.textStatusColor,
                          ),
                    ),
                  ),
                ],
              ),
            SizedBox(
              height: 6.h,
            ),
            if (vehicle?.debit?.statusDebit == VehicleDebitStatus.active)
              StatusDebitWidget(vehicle: vehicle),

          ],
        ),
      ),
    );
  }

  Future<void> _onRFIDSwitchChanged(
    BuildContext context,
    bool isOpening,
  ) async {
    final title = isOpening ? 'Mở thẻ RFID (eTag)' : 'Đóng thẻ RFID (eTag)';
    final message = isOpening
        ? 'Bạn có muốn mở thẻ RFID (eTag)?'
        : 'Đóng thẻ RFID (eTag) sẽ khiến dịch '
            'vụ qua làn thu phí không dừng '
            'của phương tiện tạm ngừng hoạt '
            'động. Bạn có muốn tiếp tục '
            'thực hiện?';

    await showDialog(
      context: context,
      builder: (dialogContext) {
        return BlocProvider.value(
          value: context.read<VehicleDetailsBloc>(),
          child: isOpening
              ? ConfirmDialog(
                  title: title,
                  content: message,
                  contentTextAlign: TextAlign.center,
                  secondaryButtonTitle: 'Huỷ',
                  primaryButtonTitle: 'Mở thẻ',
                  onPrimaryButtonTap: () {
                    context.read<VehicleDetailsBloc>().add(VehicleRFIDUpdated(
                          vehicleId: vehicle!.vehicleId.toString(),
                          isOpen: true,
                        ));
                    Navigator.of(dialogContext).pop();
                  },
                )
              : NeutralConfirmDialog(
                  title: title,
                  content: message,
                  contentTextAlign: TextAlign.center,
                  secondaryButtonTitle: 'Huỷ',
                  primaryButtonTitle: 'Đóng thẻ',
                  primaryButtonColor: ColorName.error,
                  onPrimaryButtonTap: () {
                    context.read<VehicleDetailsBloc>().add(VehicleRFIDUpdated(
                          vehicleId: vehicle!.vehicleId.toString(),
                          isOpen: false,
                        ));
                    Navigator.of(dialogContext).pop();
                  },
                ),
        );
      },
    );
  }
}

class StatusDebitWidget extends StatelessWidget {
  const StatusDebitWidget({
    Key? key,
    required this.vehicle,
  }) : super(key: key);

  final Vehicle? vehicle;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'Trạng thái nợ phí',
          style: Theme.of(context).textTheme.bodyText1,
        ),
        const Spacer(),
        Container(
          padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 8.w),
          decoration: BoxDecoration(
            color: const Color(0xFFFFE5E5),
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Text(
            vehicle?.debit?.statusDebit?.statusDebit ?? '',
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                  color: const Color(0xFFEE0033),
                ),
          ),
        ),
      ],
    );
  }
}
