import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/app_version/app_version.dart';
import 'package:simple_result/simple_result.dart';

abstract class IAppVersionRepository {
  Future<Result<AppVersion, Failure>> getAppVersion(
      {required String deviceType});
}

class AppVersionRepository implements IAppVersionRepository {
  final CrmClient _crmClient;

  AppVersionRepository({required CrmClient crmClient}) : _crmClient = crmClient;

  @override
  Future<Result<AppVersion, Failure>> getAppVersion(
      {required String deviceType}) async {
    try {
      final response = await _crmClient.getAppVersion(
        deviceType: deviceType,
      );
      final appVersion = response.data;
      if (response.mess?.code != REQUEST_SUCCESS_CODE || appVersion == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(appVersion);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
