part of 'image_picker_bloc.dart';

abstract class ImagePickerState extends Equatable {
  const ImagePickerState();
}

class ImagePickerInitial extends ImagePickerState {
  @override
  List<Object> get props => [];
}

class ImagePickedInProgress extends ImagePickerState {
  const ImagePickedInProgress();

  @override
  List<Object> get props => [];
}

class ImagePickedSuccess extends ImagePickerState {
  final String filePath;

  const ImagePickedSuccess({required this.filePath});

  @override
  List<Object> get props => [];
}

class ImagePickedFailure extends ImagePickerState {
  final String message;

  const ImagePickedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class ImagePickedCanceled extends ImagePickerState {
  const ImagePickedCanceled();

  @override
  List<Object> get props => [];
}
