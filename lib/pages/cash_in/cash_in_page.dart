import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/showcase/custom_showcase.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/cash_in/cash_in_builder.dart';
import 'package:epass/pages/cash_in/widget/cash_in_balance_card.dart';
import 'package:epass/pages/cash_in/widget/cash_in_info_card/cash_in_info_card.dart';
import 'package:epass/pages/cash_in/widget/payment_methods_card/payment_methods_card.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:showcaseview/showcaseview.dart';

class CashInPage extends StatefulWidget {
  const CashInPage({
    Key? key,
  }) : super(key: key);

  @override
  State<CashInPage> createState() => _CashInPageState();
}

class _CashInPageState extends State<CashInPage> {
  bool _firstRun = true;

  final _targetShowCaseKey = GlobalKey();
  final _amountShowCaseKey = GlobalKey();
  final _feeShowCaseKey = GlobalKey();
  // final _autoTopupShowCaseKey = GlobalKey();
  final _methodShowCaseKey = GlobalKey();
  final _groupBenefiShowCaseKey = GlobalKey();
  final _questionShowCaseKey = GlobalKey();

  late List<GlobalKey> _showCaseList;

  late BuildContext _showCaseContext;

  @override
  void initState() {
    super.initState();
    _showCaseList = [
      _targetShowCaseKey,
      _amountShowCaseKey,
      // _autoTopupShowCaseKey,
      _feeShowCaseKey,
      _methodShowCaseKey,
      _groupBenefiShowCaseKey,
      _questionShowCaseKey,
    ];
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _firstRun = false;
      final isCashInFirstRun = getIt<SettingBloc>().state.isCashInFirstRun;
      if (isCashInFirstRun) {
        Future.delayed(
          const Duration(milliseconds: 1000),
          () {
            context
                .read<HomeTabsBloc>()
                .add(const HomeTabBarRequestHidden(isHidden: true));
            ShowCaseWidget.of(_showCaseContext).startShowCase(_showCaseList);
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: ShowCaseWidget(
        onFinish: () {
          context.read<SettingBloc>().add(CashInFirstRun());
          context
              .read<HomeTabsBloc>()
              .add(const HomeTabBarRequestHidden(isHidden: false));
        },
        builder: Builder(
          builder: (context) {
            _showCaseContext = context;
            return BasePage(
              floatingActionButton: Container(
                color: Colors.transparent,
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.h),
                height: 56.h,
                width: double.infinity,
                child: const CashInBuilder(),
              ),
              title: 'Nạp tiền',
              leading: BackButton(onPressed: () {
                context.popRoute();
                context
                    .read<HomeTabsBloc>()
                    .add(const HomeTabBarRequestHidden(isHidden: false));
              }),
              backgroundColor: Colors.white,
              trailing: [
                CustomShowcase(
                    showCaseKey: _groupBenefiShowCaseKey,
                    overlayPadding: EdgeInsets.zero,
                    description: 'Xem danh sách người thụ hưởng tại đây',
                    child: IconButton(
                      icon: Icon(
                        Icons.groups,
                        color: Colors.white,
                        size: 28.r,
                      ),
                      onPressed: () =>
                          context.pushRoute(const BenficaryRouter()),
                    )),
                CustomShowcase(
                  showCaseKey: _questionShowCaseKey,
                  overlayPadding: EdgeInsets.zero,
                  description:
                      'Xem lại hướng dẫn bất kỳ lúc nào bạn muốn tại đây',
                  child: IconButton(
                    icon: Icon(
                      CupertinoIcons.question_circle,
                      color: Colors.white,
                      size: 28.r,
                    ),
                    onPressed: () {
                      context
                          .read<HomeTabsBloc>()
                          .add(const HomeTabBarRequestHidden(isHidden: true));
                      ShowCaseWidget.of(_showCaseContext)
                          .startShowCase(_showCaseList);
                    },
                  ),
                ),
              ],
              child: Stack(
                children: [
                  FadeAnimation(
                    delay: 0.5,
                    playAnimation: _firstRun,
                    child: GradientHeaderContainer(height: 210.h),
                  ),
                  SafeArea(
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          physics: const BouncingScrollPhysics(),
                          child: FadeAnimation(
                            delay: 1.5,
                            playAnimation: _firstRun,
                            direction: FadeDirection.up,
                            child: RoundedTopContainer(
                              margin: EdgeInsets.only(top: 50.h),
                              padding:
                                  EdgeInsets.fromLTRB(16.w, 60.h, 16.w, 32.h),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Contract No and Cash In Value
                                  CashInInfoCard(
                                    targetShowCaseKey: _targetShowCaseKey,
                                    amountShowCaseKey: _amountShowCaseKey,
                                    feeShowCaseKey: _feeShowCaseKey,
                                    //  autoTopupShowCaseKey: _autoTopupShowCaseKey,
                                  ),
                                  SizedBox(height: 20.h),

                                  // Payment methods
                                  CustomShowcase(
                                    showCaseKey: _methodShowCaseKey,
                                    overlayPadding:
                                        EdgeInsets.fromLTRB(0.w, 0.h, 0.w, 0.h),
                                    description: 'Chọn phương thức thanh toán'
                                        ' phù hợp. Sau đó ấn “nạp tiền” và làm'
                                        ' theo hướng dẫn',
                                    child: const PaymentMethodsCard(),
                                  ),
                                  SizedBox(height: 20.h),

                                  // Submit Button
                                  // SizedBox(
                                  //   height: 56.h,
                                  //   width: double.infinity,
                                  //   child: const CashInBuilder(),
                                  // ),
                                  // SizedBox(height: 100.h),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // For better UI when scroll SingleChildScrollView up
                        FutureBuilder(
                          future: Future.delayed(const Duration(seconds: 1)),
                          builder: (context, snapshot) =>
                              snapshot.connectionState == ConnectionState.done
                                  ? GradientHeaderContainer(
                                      height: 40.h,
                                      radius: 0,
                                    )
                                  : const SizedBox.shrink(),
                        ),
                        const FadeAnimation(
                          delay: 1,
                          child: CashInBalanceCard(),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
