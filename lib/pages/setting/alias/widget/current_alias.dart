import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CurrentAlias extends StatelessWidget {
  final String alias;
  final VoidCallback? onTap;

  const CurrentAlias({
    Key? key,
    required this.alias,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Bí danh hiện tại của bạn là: ',
          style: Theme.of(context).textTheme.bodyText1,
        ),
        SizedBox(height: 4.h),
        Material(
          borderRadius: BorderRadius.circular(16.0),
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(8.0),
            onTap: onTap,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 6.w,
                vertical: 4.h,
              ),
              child: RichText(
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: alias,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: ColorName.linkBlue,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                          ),
                    ),
                    WidgetSpan(child: SizedBox(width: 4.w)),
                    WidgetSpan(
                      child: Assets.icons.pencil.svg(
                        height: 20.h,
                        color: ColorName.linkBlue,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
