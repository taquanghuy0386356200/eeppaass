import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/transaction_other/transaction_other.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';

class TransactionOtherCard extends StatelessWidget {
  const TransactionOtherCard({
    Key? key,
    required this.transactionOther,
  }) : super(key: key);

  final TransactionOther transactionOther;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 4.w,
                constraints: BoxConstraints(
                  minHeight: 40.h,
                ),
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      ColorName.primaryGradientStart,
                      ColorName.primaryGradientEnd,
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
              ),
              SizedBox(width: 12.w),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 6.h),
                  child: Text(
                    '${transactionOther.actTypeName}',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
              ),
              SizedBox(width: 24.w),
              Padding(
                padding: EdgeInsets.only(bottom: 6.h),
                child: Text(
                  (transactionOther.amount ?? 0).formatCurrency(symbol: '₫'),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.end,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20.h),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(width: 16.w),
            Text(
              'Lý do',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: ColorName.textGray1,
                  ),
            ),
            SizedBox(width: 12.w),
            Expanded(
              child: Text(
                '${transactionOther.actReasonName}',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.end,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ],
        ),
        SizedBox(height: 12.h),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(width: 4.w),
            SizedBox(width: 12.w),
            Text(
              'Ngày thực hiện',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: ColorName.textGray1,
                  ),
            ),
            SizedBox(width: 12.w),
            Expanded(
              child: Text(
                '${transactionOther.saleTransDate}',
                maxLines: 2,
                textAlign: TextAlign.end,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
