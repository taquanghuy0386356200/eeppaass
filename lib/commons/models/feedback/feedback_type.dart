import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

/// ticketTypeId : 2
/// name : "1. Hỗ trợ ePass CPT"
/// code : "2"
/// status : 1
/// createUser : "sys"
/// createDate : "14/07/2021 15:20:22"

part 'feedback_type.g.dart';

@JsonSerializable()
class FeedbackType extends DropdownItem with EquatableMixin {
  FeedbackType({
    this.ticketTypeId,
    this.name,
    this.code,
    this.status,
    this.createUser,
    this.createDate,
    this.ticketTemplate,
  }) : super(title: name, value: code);

  final int? ticketTypeId;
  final String? name;
  final String? code;
  final int? status;
  final String? createUser;
  final String? createDate;
  final String? ticketTemplate;

  @override
  List<Object?> get props => [ticketTypeId, name, code, status, createUser, createDate, ticketTemplate];

  @override
  String toString() {
    return name ?? '';
  }

  factory FeedbackType.fromJson(Map<String, dynamic> json) => _$FeedbackTypeFromJson(json);
}

@JsonSerializable()
class FeedbackTypeData {
  final List<FeedbackType>? listData;
  final int? count;

  FeedbackTypeData({this.listData, this.count});

  factory FeedbackTypeData.fromJson(Map<String, dynamic> json) => _$FeedbackTypeDataFromJson(json);
}

class FeedbackTypeDataNotNull {
  final List<FeedbackType> listData;
  final int count;

  FeedbackTypeDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
class FeedbackTypeResponse {
  final FeedbackTypeData? data;
  final MessageData? mess;

  FeedbackTypeResponse({this.data, this.mess});

  factory FeedbackTypeResponse.fromJson(Map<String, dynamic> json) => _$FeedbackTypeResponseFromJson(json);
}
