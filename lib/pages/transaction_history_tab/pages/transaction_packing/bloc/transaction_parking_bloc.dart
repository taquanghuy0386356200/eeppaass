import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/transaction_packing/transaction_parking.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_packing/validator/transaction_parking_validator.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'transaction_parking_event.dart';

part 'transaction_parking_state.dart';

class TransactionParkingBloc
    extends Bloc<TransactionParkingEvent, TransactionParkingState>
    with TransactionParkingValidator {
  final ITransactionHistoryRepository _transRepository;
  final AppBloc _appBloc;

  final int _pageSize;
  int _startRecord = 0;
  bool isSearching = false;

  TransactionParkingBloc({
    required ITransactionHistoryRepository transRepository,
    required AppBloc appBloc,
    int pageSize = 10,
  })  : _transRepository = transRepository,
        _appBloc = appBloc,
        _pageSize = pageSize,
        super(const TransactionParkingState(listData: [])) {
    on<TransactionParkingFetched>(_onTransactionParkingFeeFetched);
    on<TransactionParkingRefreshed>(_onTransactionParkingFeeRefreshed);
    on<TransactionParkingLoadMore>(_onTransactionParkingFeeLoadMore);
    on<TransactionParkingSearched>(_onTransactionParkingFeeSearched);
  }

  FutureOr<void> _onTransactionParkingFeeFetched(
    TransactionParkingFetched event,
    Emitter<TransactionParkingState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      plateNumber: null,
      startDate: null,
      endDate: null,
      error: null,
    ));

    final result = await _getTransactionParkingFee();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: null,
            startDate: null,
            endDate: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: null,
        startDate: null,
        endDate: null,
      )),
    );
  }

  FutureOr<void> _onTransactionParkingFeeRefreshed(
      TransactionParkingRefreshed event,
      Emitter<TransactionParkingState> emit) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      plateNumber: state.plateNumber,
      startDate: state.startDate,
      endDate: state.endDate,
      error: null,
    ));

    final result = await _getTransactionParkingFee();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            startDate: state.startDate,
            endDate: state.endDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        startDate: state.startDate,
        endDate: state.endDate,
      )),
    );
  }

  FutureOr<void> _onTransactionParkingFeeLoadMore(
    TransactionParkingLoadMore event,
    Emitter<TransactionParkingState> emit,
  ) async {
    final listData = state.listData;

    emit(state.copyWith(
      isLoadingMore: true,
      plateNumber: state.plateNumber,
      startDate: state.startDate,
      endDate: state.endDate,
      error: null,
    ));

    final result = await _getTransactionParkingFee();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(state.copyWith(
          isLoadingMore: false,
          listData: listData,
          isFull: listData.length == data.count,
          plateNumber: state.plateNumber,
          startDate: state.startDate,
          endDate: state.endDate,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        startDate: state.startDate,
        endDate: state.endDate,
      )),
    );
  }

  FutureOr<void> _onTransactionParkingFeeSearched(
    TransactionParkingSearched event,
    Emitter<TransactionParkingState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
      plateNumber: event.plateNumber,
      startDate: event.startDate,
      endDate: event.endDate,
      error: null,
    ));

    _startRecord = 0;

    final result = await _getTransactionParkingFee();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: event.plateNumber,
            startDate: event.startDate,
            endDate: event.endDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: event.plateNumber,
        startDate: event.startDate,
        endDate: event.endDate,
      )),
    );
  }

  Future<Result<TransactionParkingDataNotNull, Failure>>
      _getTransactionParkingFee() async {
    final result = await _transRepository.getTransactionParking(
      pageSize: _pageSize,
      startRecord: _startRecord,
      plateNumber: state.plateNumber,
      startDate: state.startDate != null
          ? Jiffy(state.startDate).format('dd/MM/yyyy')
          : null,
      endDate: state.endDate != null
          ? Jiffy(state.endDate).format('dd/MM/yyyy')
          : null,
    );
    return result;
  }
}
