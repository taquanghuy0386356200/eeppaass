import 'package:epass/commons/models/feedback/feedback_attachment_file.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AttachmentFilesWidget extends StatelessWidget {
  final List<FeedbackAttachmentFileResponse> attachmentFiles;
  final void Function(String fileName, int fileId)? onAttachmentTap;

  const AttachmentFilesWidget({
    Key? key,
    required this.attachmentFiles,
    this.onAttachmentTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          'Tệp đính kèm',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontWeight: FontWeight.w700,
              ),
        ),
        SizedBox(height: 8.h),
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: attachmentFiles.map((file) {
            return GestureDetector(
              onTap: () {
                final fileId = file.attachmentId;
                final fileName = file.fileName;
                if (fileId != null && fileName != null) {
                  onAttachmentTap?.call(fileName, fileId);
                }
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.h),
                child: Text(
                  file.fileName ?? '',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: ColorName.linkBlue,
                      ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }
}
