part of 'attachment_file_bloc.dart';

abstract class AttachmentFileState extends Equatable {
  const AttachmentFileState();
}

class AttachmentFileInitial extends AttachmentFileState {
  @override
  List<Object> get props => [];
}

class AttachmentFileDownloadedInProgress extends AttachmentFileState {
  const AttachmentFileDownloadedInProgress();

  @override
  List<Object> get props => [];
}

class AttachmentFileDownloadedSuccess extends AttachmentFileState {
  final String fileName;
  final List<int> attachmentFileData;

  const AttachmentFileDownloadedSuccess({
    required this.fileName,
    required this.attachmentFileData,
  });

  @override
  List<Object> get props => [fileName, attachmentFileData];
}

class AttachmentFileDownloadedFailure extends AttachmentFileState {
  final String message;

  const AttachmentFileDownloadedFailure({required this.message});

  @override
  List<Object> get props => [message];
}
