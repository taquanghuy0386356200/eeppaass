part of 'invoice_bloc.dart';

/*
    @Query('plateNumber') String? plateNumber,
    @Query('invoiceNo') String? invoiceNo,
    @Query('adjustmentType') int? adjustmentType,
    @Query('transactionType') int? transactionType,
    @Query('stationInId') int? stationInId,
    @Query('stationOutId') int? stationOutId,
 */

class InvoiceState extends Equatable {
  final List<Invoice> listData;
  final bool isFull;
  final String? plateNumber;
  final String? invoiceNo;
  final int? adjustmentType;
  final int? transactionType;
  final int? stationInId;
  final int? stationOutId;
  final DateTime? from;
  final DateTime? to;
  final String? error;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const InvoiceState({
    required this.listData,
    this.isFull = false,
    this.plateNumber,
    this.invoiceNo,
    this.adjustmentType,
    this.transactionType,
    this.stationInId,
    this.stationOutId,
    this.from,
    this.to,
    this.error,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        listData,
        isFull,
        adjustmentType,
        plateNumber,
        stationInId,
        stationOutId,
        from,
        to,
        error,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];

  InvoiceState copyWith({
    List<Invoice>? listData,
    bool? isFull,
    required String? plateNumber,
    required String? invoiceNo,
    required int? adjustmentType,
    required int? transactionType,
    required int? stationInId,
    required int? stationOutId,
    required DateTime? from,
    required DateTime? to,
    required String? error,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return InvoiceState(
      listData: listData ?? this.listData,
      isFull: isFull ?? this.isFull,
      plateNumber: plateNumber,
      invoiceNo: invoiceNo,
      adjustmentType: adjustmentType,
      transactionType: transactionType,
      stationInId: stationInId,
      stationOutId: stationOutId,
      from: from,
      to: to,
      error: error,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
