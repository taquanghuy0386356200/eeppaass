import 'package:epass/commons/validators/plate_number_validator.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/pages/register_confirm/widgets/register_confirm_vehicle_info/vehicle_input_form_alt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ConfirmVehicleInfo extends StatelessWidget with PlateNumberValidator {
  ConfirmVehicleInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Thông tin xe',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: ColorName.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 18.sp,
              ),
        ),
        SizedBox(height: 20.h),

        // VEHICLES
        BlocBuilder<RegisterVehiclesBloc, RegisterVehiclesState>(
          builder: (context, state) => ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: state.listRegisterVehicle.length,
            itemBuilder: (context, index) {
              return VehicleInputFormAlt(vehicleIndex: index);
            },
            separatorBuilder: (context, index) => SizedBox(height: 44.h),
          ),
        ),
      ],
    );
  }
}
