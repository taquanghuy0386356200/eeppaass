part of 'forgot_password_form_bloc.dart';

abstract class ForgotPasswordFormEvent extends Equatable {
  const ForgotPasswordFormEvent();
}

class ForgotPasswordFormCompleted extends ForgotPasswordFormEvent {
  final String plateNumber;
  final String plateTypeCode;
  final String phoneNumber;

  const ForgotPasswordFormCompleted({
    required this.plateNumber,
    required this.plateTypeCode,
    required this.phoneNumber,
  });

  @override
  List<Object> get props => [plateNumber, plateTypeCode, phoneNumber];
}
