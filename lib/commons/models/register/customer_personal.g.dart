// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_personal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerPersonal _$CustomerPersonalFromJson(Map<String, dynamic> json) =>
    CustomerPersonal(
      custId: json['custId'] as int?,
      custTypeId: json['custTypeId'] as int?,
      documentNumber: json['documentNumber'] as String?,
      documentTypeId: json['documentTypeId'] as int?,
      taxCode: json['taxCode'] as String?,
      custName: json['custName'] as String?,
      birthDate: json['birthDate'] as String?,
      gender: json['gender'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$CustomerPersonalToJson(CustomerPersonal instance) =>
    <String, dynamic>{
      'custId': instance.custId,
      'custTypeId': instance.custTypeId,
      'documentNumber': instance.documentNumber,
      'documentTypeId': instance.documentTypeId,
      'taxCode': instance.taxCode,
      'custName': instance.custName,
      'birthDate': instance.birthDate,
      'gender': instance.gender,
      'description': instance.description,
    };

CustomerPersonalResponse _$CustomerPersonalResponseFromJson(
        Map<String, dynamic> json) =>
    CustomerPersonalResponse(
      data: json['data'] == null
          ? null
          : CustomerPersonal.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CustomerPersonalResponseToJson(
        CustomerPersonalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
