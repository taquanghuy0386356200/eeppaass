import 'package:json_annotation/json_annotation.dart';

part 'report_attachment_file.g.dart';

/// fileName : "CMND3.jpg"
/// attachmentId : 908

@JsonSerializable()
class ReportAttachmentFileResponse {
  ReportAttachmentFileResponse({
    this.fileName,
    this.attachmentId,
  });

  final String? fileName;
  final int? attachmentId;

  factory ReportAttachmentFileResponse.fromJson(Map<String, dynamic> json) =>
      _$ReportAttachmentFileResponseFromJson(json);
}

@JsonSerializable()
class ReportAttachmentFileRequest {
  final String fileName;
  final String base64Data;
  final double fileSize;

  ReportAttachmentFileRequest({
    required this.fileName,
    required this.base64Data,
    required this.fileSize,
  });

  Map<String, dynamic> toJson() => _$ReportAttachmentFileRequestToJson(this);

  factory ReportAttachmentFileRequest.fromJson(Map<String, dynamic> json) =>
      _$ReportAttachmentFileRequestFromJson(json);
}
