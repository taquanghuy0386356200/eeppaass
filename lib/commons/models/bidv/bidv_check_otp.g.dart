// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bidv_check_otp.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BIDVCheckOTPRequest _$BIDVCheckOTPRequestFromJson(Map<String, dynamic> json) =>
    BIDVCheckOTPRequest(
      data: json['data'] as String,
    );

Map<String, dynamic> _$BIDVCheckOTPRequestToJson(
        BIDVCheckOTPRequest instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

BIDVCheckOTPResponse _$BIDVCheckOTPResponseFromJson(
        Map<String, dynamic> json) =>
    BIDVCheckOTPResponse(
      data: BIDVCheckOTPData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BIDVCheckOTPResponseToJson(
        BIDVCheckOTPResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

BIDVCheckOTPData _$BIDVCheckOTPDataFromJson(Map<String, dynamic> json) =>
    BIDVCheckOTPData(
      transId: json['transId'] as String,
      responseCode: json['responseCode'] as String,
    );

Map<String, dynamic> _$BIDVCheckOTPDataToJson(BIDVCheckOTPData instance) =>
    <String, dynamic>{
      'transId': instance.transId,
      'responseCode': instance.responseCode,
    };
