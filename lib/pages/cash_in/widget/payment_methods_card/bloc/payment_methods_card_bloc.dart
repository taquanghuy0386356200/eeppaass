import 'dart:async';
import 'dart:io' show Platform;
import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/app_version/app_version.dart';
import 'package:epass/commons/models/topup_channel/topup_channel.dart';
import 'package:epass/commons/repo/app_version_repository.dart';
import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:epass/commons/utils/number_extension/number_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:package_info_plus/package_info_plus.dart';

part 'payment_methods_card_event.dart';

part 'payment_methods_card_state.dart';

class PaymentMethodsCardBloc
    extends Bloc<PaymentMethodsCardEvent, PaymentMethodsCardState> {
  final ICashInRepository _cashInRepository;

  PaymentMethodsCardBloc({
    required ICashInRepository cashInRepository,
  })  : _cashInRepository = cashInRepository,
        super(PaymentMethodsCardInitial()) {
    on<PaymentMethodsReset>((event, emit) => emit(PaymentMethodsCardInitial()));

    on<PaymentMethodsFetched>((event, emit) async {
      emit(PaymentMethodsCardLoading());

      final result = await _cashInRepository.getTopupChannel();

      result.when(
        success: (topupChannels) {
          emit(
            PaymentMethodsFetchedSuccess(
              paymentMethods: topupChannels,
            ),
          );
        },
        failure: (failure) => emit(
          PaymentMethodsFetchedFailure(failure.message ?? ''),
        ),
      );
    });

    on<PaymentMethodChanged>(
      (event, emit) async {
        List<TopupChannel> topupChannels = [];
        if (state is PaymentMethodsFetchedSuccess) {
          topupChannels =
              (state as PaymentMethodsFetchedSuccess).paymentMethods;
          emit(
            (state as PaymentMethodsFetchedSuccess).copyWith(
              selectedPaymentMethod: event.selectedPaymentMethod,
            ),
          );
        }
        // if (event.selectedPaymentMethod.name == 'MBBank') {
        //   final appInfo = await PackageInfo.fromPlatform();
        //   if (needUpdateNewestApp(appInfo)) {
        //     emit(
        //       PaymentMethodMBNotSupportNeedUpdate(
        //         platform: getPlatform(),
        //         paymentMethods: topupChannels,
        //         selectedPaymentMethod: event.selectedPaymentMethod,
        //       ),
        //     );
        //   }
        // }
      },
    );

    on<PaymentMethodsLastChange>(_onMethodTargetLastRecharge);
  }

  Future<List<TopupChannel>> listTopupChannelsCustomize(
      {required List<TopupChannel> defaultTopupChannels}) async {
    final tmp = defaultTopupChannels;
    final appInfo = await PackageInfo.fromPlatform();
    //current version >= android app : 1.4.28, ios: 2.4.2
    if (canShowIconApp(appInfo)) {
      for (int i = 0; i < tmp.length; i++) {
        if (tmp[i].code == TopupChannelCode.MB ||
            tmp[i].code == TopupChannelCode.ZaloPay) {
          tmp[i].status = TopupChannelStatus.active;
        }
      }
    }
    return tmp;
  }

  String getPlatform() {
    if (Platform.isAndroid) {
      return 'ANDROID';
    } else if (Platform.isIOS) {
      return 'IOS';
    } else {
      return 'IOS';
    }
  }

  bool canShowIconApp(PackageInfo appInfo) {
    if (Platform.isAndroid) {
      final canShowIconAppAndroid = appInfo.version.getExtendedVersionNumber() >
          ('1.4.29'.getExtendedVersionNumber());
      if (canShowIconAppAndroid) {
        return true;
      } else {
        return false;
      }
    } else {
      final canShowIconAppIOS = appInfo.version.getExtendedVersionNumber() >
          ('2.4.1'.getExtendedVersionNumber());
      if (canShowIconAppIOS) {
        return true;
      } else {
        return false;
      }
    }
  }

  FutureOr<void> _onMethodTargetLastRecharge(event, emit) async {
    final result = await _cashInRepository.getLastCharge();
    result.when(
      success: (contractsInfo) {
        emit(PaymentMethodsSuccess(
            selectedPaymentMethod: contractsInfo.data?.topupChannel));
      },
      failure: (failure) {
        emit(const PaymentMethodsSuccess(selectedPaymentMethod: null));
      },
    );
  }
}
