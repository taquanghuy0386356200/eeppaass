import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/clients/vietmap/vietmap_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/vehicle/update_vehicle_etag.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/models/vietmap/vietmap_product.dart';
import 'package:simple_result/simple_result.dart';

abstract class IVietMapRepository {
  Future<Result<VietMapProductData, Failure>> getVietMapProducts();
}

class VietMapRepository implements IVietMapRepository {
  final VietMapClient _vietMapClient;

  VietMapRepository({required VietMapClient vietMapClient}) : _vietMapClient = vietMapClient;

  @override
  Future<Result<VietMapProductData, Failure>> getVietMapProducts() async {
    try {
      final response = await _vietMapClient.getVietMapProducts();
      final listData = response.data?.results;

      if (listData == null) {
        return Result.failure(
          ResponseFailure(message: response.error ?? ''),
        );
      }
      return Result.success(response.data!);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
