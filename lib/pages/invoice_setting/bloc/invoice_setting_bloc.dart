import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'invoice_setting_event.dart';

part 'invoice_setting_state.dart';

class InvoiceSettingBloc extends Bloc<InvoiceSettingEvent, InvoiceSettingState> {
  final IInvoiceRepository _invoiceRepository;
  final IUserRepository _userRepository;
  final AppBloc _appBloc;

  InvoiceSettingBloc({
    required IInvoiceRepository invoiceRepository,
    required IUserRepository userRepository,
    required AppBloc appBloc,
  })  : _invoiceRepository = invoiceRepository,
        _userRepository = userRepository,
        _appBloc = appBloc,
        super(InvoiceSettingInitial()) {
    on<InvoiceSettingCycleUpdated>(_onInvoiceSettingCycleUpdated);
  }

  FutureOr<void> _onInvoiceSettingCycleUpdated(
    InvoiceSettingCycleUpdated event,
    emit,
  ) async {
    emit(InvoiceSettingCycleUpdatedInProgress());

    final result = await _invoiceRepository.updateInvoiceSetting(
      billCycle: event.billCycle,
      billCycleMergeType: event.billCycleMergeType,
    );

    await result.when(
      success: (success) async {
        final getUserResult = await _userRepository.getUser();

        getUserResult.when(
          success: (user) {
            // Update [AppBloc] user info
            _appBloc.add(UpdateAppUser(user));

            // Emit authentication success state
            emit(InvoiceSettingCycleUpdatedSuccess(success ?? 'Thành công'));
          },
          failure: (failure) => emit(InvoiceSettingCycleUpdatedFailure(failure.message ?? 'Có lỗi xảy ra')),
        );
      },
      failure: (failure) => emit(InvoiceSettingCycleUpdatedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
