// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feedback_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackType _$FeedbackTypeFromJson(Map<String, dynamic> json) => FeedbackType(
      ticketTypeId: json['ticketTypeId'] as int?,
      name: json['name'] as String?,
      code: json['code'] as String?,
      status: json['status'] as int?,
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
      ticketTemplate: json['ticketTemplate'] as String?,
    );

Map<String, dynamic> _$FeedbackTypeToJson(FeedbackType instance) =>
    <String, dynamic>{
      'ticketTypeId': instance.ticketTypeId,
      'name': instance.name,
      'code': instance.code,
      'status': instance.status,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'ticketTemplate': instance.ticketTemplate,
    };

FeedbackTypeData _$FeedbackTypeDataFromJson(Map<String, dynamic> json) =>
    FeedbackTypeData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => FeedbackType.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$FeedbackTypeDataToJson(FeedbackTypeData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

FeedbackTypeResponse _$FeedbackTypeResponseFromJson(
        Map<String, dynamic> json) =>
    FeedbackTypeResponse(
      data: json['data'] == null
          ? null
          : FeedbackTypeData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FeedbackTypeResponseToJson(
        FeedbackTypeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
