part of 'transaction_other_bloc.dart';

@immutable
abstract class TransactionOtherEvent extends Equatable {
  const TransactionOtherEvent();
}

class TransactionOtherFetched extends TransactionOtherEvent {
  const TransactionOtherFetched();

  @override
  List<Object?> get props => [];
}

class TransactionOtherRefreshed extends TransactionOtherEvent {
  const TransactionOtherRefreshed();

  @override
  List<Object?> get props => [];
}

class TransactionOtherLoadMore extends TransactionOtherEvent {
  const TransactionOtherLoadMore();

  @override
  List<Object?> get props => [];
}

class TransactionOtherSearched extends TransactionOtherEvent {
  final String? plateNumber;
  final DateTime? fromDate;
  final DateTime? toDate;

  const TransactionOtherSearched({
    this.plateNumber,
    this.fromDate,
    this.toDate,
  });

  @override
  List<Object?> get props => [plateNumber, fromDate, toDate];
}

class TransactionOtherFromDateChanged extends TransactionOtherEvent {
  final String? fromDate;

  const TransactionOtherFromDateChanged(this.fromDate);

  @override
  List<Object?> get props => [fromDate];
}

class TransactionOtherToDateChanged extends TransactionOtherEvent {
  final String? toDate;

  const TransactionOtherToDateChanged(this.toDate);

  @override
  List<Object?> get props => [toDate];
}
