import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/extensions/file_ext.dart';
import 'package:epass/commons/models/report/report_attachment_file.dart';
import 'package:epass/commons/repo/report_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:path/path.dart' as p;

part 'create_report_event.dart';

part 'create_report_state.dart';

class CreateReportBloc extends Bloc<CreateReportEvent, CreateReportState> {
  final IReportRepository _reportRepository;
  final AppBloc _appBloc;

  CreateReportBloc({
    required IReportRepository reportRepository,
    required AppBloc appBloc,
  })  : _reportRepository = reportRepository,
        _appBloc = appBloc,
        super(CreateReportInitial()) {
    on<CreateReportFormSubmitted>(_onCreateReportFormSubmitted);
  }

  FutureOr<void> _onCreateReportFormSubmitted(
      CreateReportFormSubmitted event,
      Emitter<CreateReportState> emit,
      ) async {
    emit(const CreateReportFormSubmittedInProgress());

    final user = _appBloc.state.user;
    final custId = user?.customerId;
    final contractNo = user?.contractNo;
    final contractId = user?.contractId;
    final custTypeId = user?.cusTypeId;
    final phoneNumber = user?.noticePhoneNumber;
    final location = user?.address;

    if (custId == null ||
        contractNo == null ||
        contractId == null ||
        custTypeId == null ||
        phoneNumber == null
    ) {
      emit(const CreateReportFormSubmittedFailure('Có lỗi xảy ra'));
      return;
    }

    final requestDate = '${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year}';

    final attachmentFiles = event.filePaths.map(
          (e) {
        final file = File(e);
        final fileName = p.basename(e);
        final bytes = file.readAsBytesSync();
        final fileSize = file.size;
        bytes.length / 1024;
        final base64 = base64Encode(bytes);
        return ReportAttachmentFileRequest(fileName: fileName, base64Data: base64, fileSize: fileSize);
      },
    ).toList();

    final result = await _reportRepository.createReport(
      custId: custId,
      contractNo: contractNo,
      contractId: contractId,
      custTypeId: custTypeId.toString(),
      custName: event.reporterName,
      email: event.email,
      plateNumber: event.plateNumber,
      plateTypeCode: event.plateTypeCode,
      phoneNumber: phoneNumber,
      location: location,
      priorityId: event.priorityId.toString(),
      l1TicketTypeId: event.l1TicketTypeId,
      l2TicketTypeId: event.l2TicketTypeId,
      l3TicketTypeId: event.l3TicketTypeId,
      provinceName: event.provinceName,
      districtName: event.districtName,
      communeName: event.communeName,
      areaCode: event.areaCode,
      stationId: event.stationId,
      stationName: event.stationName,
      stageId: event.stageId,
      stageName: event.stageName,
      contentReceive: event.contentReceive,
      ticketKind: event.ticketKind,
      requestDate: requestDate,
      supportInfo: event.supportInfo,
      phoneContact: event.phoneContact,
      attachmentFiles: attachmentFiles,
      otp: event.otp,
    );

    result.when(
      success: (success) => emit(const CreateReportFormSubmittedSuccess()),
      failure: (failure) => emit(CreateReportFormSubmittedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
