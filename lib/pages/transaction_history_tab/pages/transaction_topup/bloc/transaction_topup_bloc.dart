import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'transaction_topup_event.dart';

part 'transaction_topup_state.dart';

class TransactionTopupBloc
    extends Bloc<TransactionTopupEvent, TransactionTopupState> {
  final ITransactionHistoryRepository _transRepository;

  final int _pageSize;
  int _startRecord = 0;
  bool isSearching = false;

  TransactionTopupBloc({
    required ITransactionHistoryRepository transactionHistoryRepository,
    int pageSize = 10,
  })  : _pageSize = pageSize,
        _transRepository = transactionHistoryRepository,
        super(const TransactionTopupState(listData: [])) {
    on<TransactionTopupFetched>(_onTransactionTopupFetched);
    on<TransactionTopupRefreshed>(_onTransactionTopupRefreshed);
    on<TransactionTopupLoadMore>(_onTransactionTopupLoadMore);
    on<TransactionTopupSearched>(_onTransactionTopupSearched);
  }

  FutureOr<void> _onTransactionTopupFetched(
    TransactionTopupFetched event,
    Emitter<TransactionTopupState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      topupDateFrom: null,
      topupDateTo: null,
      error: null,
    ));

    final result = await _getContractTopup();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            topupDateFrom: null,
            topupDateTo: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        topupDateFrom: null,
        topupDateTo: null,
      )),
    );
  }

  FutureOr<void> _onTransactionTopupRefreshed(
    TransactionTopupRefreshed event,
    Emitter<TransactionTopupState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      topupDateFrom: state.topupDateFrom,
      topupDateTo: state.topupDateTo,
      error: null,
    ));

    final result = await _getContractTopup();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            topupDateFrom: state.topupDateFrom,
            topupDateTo: state.topupDateTo,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
        topupDateFrom: state.topupDateFrom,
        topupDateTo: state.topupDateTo,
      )),
    );
  }

  FutureOr<void> _onTransactionTopupLoadMore(
    TransactionTopupLoadMore event,
    Emitter<TransactionTopupState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      topupDateFrom: state.topupDateFrom,
      topupDateTo: state.topupDateTo,
      error: null,
    ));

    final result = await _getContractTopup();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(
          state.copyWith(
            isLoadingMore: false,
            listData: listData,
            isFull: listData.length == data.count,
            topupDateFrom: state.topupDateFrom,
            topupDateTo: state.topupDateTo,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        error: failure.message,
        topupDateFrom: state.topupDateFrom,
        topupDateTo: state.topupDateTo,
      )),
    );
  }

  FutureOr<void> _onTransactionTopupSearched(
    TransactionTopupSearched event,
    Emitter<TransactionTopupState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      topupDateFrom: event.topupDateFrom,
      topupDateTo: event.topupDateTo,
      error: null,
    ));

    final result = await _getContractTopup();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            topupDateFrom: state.topupDateFrom,
            topupDateTo: state.topupDateTo,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        topupDateFrom: state.topupDateFrom,
        topupDateTo: state.topupDateTo,
      )),
    );
  }

  Future<Result<ContractTopupDataNotNull, Failure>> _getContractTopup() async {
    final result = await _transRepository.getContractTopup(
      pageSize: _pageSize,
      startRecord: _startRecord,
      topupDateFrom: state.topupDateFrom != null
          ? Jiffy(state.topupDateFrom).format('dd/MM/yyyy')
          : null,
      topupDateTo: state.topupDateTo != null
          ? Jiffy(state.topupDateTo).format('dd/MM/yyyy')
          : null,
    );
    return result;
  }
}
