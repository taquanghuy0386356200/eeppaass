import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/widgets/animations/shimmer_widget.dart';
import 'package:epass/commons/widgets/balance_alt_widget.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:epass/pages/home/widgets/home_currency_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BuyTicketAmount extends StatelessWidget {
  final Color? titleColor;

  const BuyTicketAmount({
    Key? key,
    this.titleColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Số dư ví  ',
              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    color: ColorName.borderColor,
                  ),
            ),
            SizedBox(width: 10.w),
            BalanceAltWidget(
              mainTextSize: 16.sp,
              mainTextColor: ColorName.textGray2,
            ),
          ],
        ),
        SizedBox(height: 6.h),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Tổng tiền',
              style: Theme.of(context).textTheme.bodyText2!.copyWith(
                    color: ColorName.borderColor,
                  ),
            ),
            SizedBox(width: 10.w),
            BlocConsumer<TicketPriceBloc, TicketPriceState>(
              listener: (context, state) {
                if (state is TicketPriceFetchedFailure) {
                  showErrorSnackBBar(
                    context: context,
                    message: state.message,
                  );
                }
              },
              builder: (context, state) {
                if (state is TicketPriceFetchedInProgress) {
                  return ShimmerColor(
                    child: ShimmerWidget(
                      height: 20.h,
                      width: 80.w,
                    ),
                  );
                } else if (state is TicketPriceFetchedSuccess) {
                  final totalAmount = state.ticketPrices
                      .map((e) => e.fee ?? 0)
                      .reduce((value, element) => value += element);

                  return HomeCurrencyText(
                    text: totalAmount.formatCurrency(),
                    fontSize: 16.sp,
                    color: ColorName.waiting,
                  );
                } else {
                  return HomeCurrencyText(
                    text: '0',
                    fontSize: 16.sp,
                    color: ColorName.waiting,
                  );
                }
              },
            ),
          ],
        ),
      ],
    );
  }
}
