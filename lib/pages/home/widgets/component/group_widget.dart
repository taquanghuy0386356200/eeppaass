import 'package:epass/pages/home/model/apps_model.dart';
import 'package:epass/pages/home/model/list_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'item_widget.dart';
import 'section_widget.dart';

class GroupWidget extends StatelessWidget {
  const GroupWidget(
      {Key? key,
      required this.appModel,
      required this.sectionType,
      required this.appsModel})
      : super(key: key);
  final List<ListApp> appModel;
  final sectionType;
  final AppsModel appsModel;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.zero,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisSpacing: sectionType == SectionType.popular ? 2 : 8.0.h,
          crossAxisCount: sectionType == SectionType.popular ? 3 : 4,
          childAspectRatio: 0.8,
        ),
        itemCount: sectionType == SectionType.popular
            ? appModel.length > 7
                ? 7
                : appModel.length + 1
            : appModel.length,
        itemBuilder: (BuildContext context, int index) {
          return itemWidget(
            index: index,
            appModel: index > appModel.length - 1 ? ListApp() : appModel[index],
            sectionType: sectionType,
            isLastItem: index > appModel.length - 1,
            isInsertMsisdn: index > appModel.length - 1
                ? false
                : appModel[index].isInsertMsisdn == 1,
            appsModel: appsModel,
          );
        });
  }
}
