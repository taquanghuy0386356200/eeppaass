// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_other.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionOther _$TransactionOtherFromJson(Map<String, dynamic> json) =>
    TransactionOther(
      serviceFeeName: json['serviceFeeName'] as String?,
      createUser: json['createUser'] as String?,
      saleTransDate: json['saleTransDate'] as String?,
      amount: json['amount'] as int?,
      saleTransCode: json['saleTransCode'] as String?,
      actReasonName: json['actReasonName'] as String?,
      actTypeName: json['actTypeName'] as String?,
    );

Map<String, dynamic> _$TransactionOtherToJson(TransactionOther instance) =>
    <String, dynamic>{
      'serviceFeeName': instance.serviceFeeName,
      'createUser': instance.createUser,
      'saleTransDate': instance.saleTransDate,
      'amount': instance.amount,
      'saleTransCode': instance.saleTransCode,
      'actReasonName': instance.actReasonName,
      'actTypeName': instance.actTypeName,
    };

TransactionOtherData _$TransactionOtherDataFromJson(
        Map<String, dynamic> json) =>
    TransactionOtherData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => TransactionOther.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$TransactionOtherDataToJson(
        TransactionOtherData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

TransactionOtherResponse _$TransactionOtherResponseFromJson(
        Map<String, dynamic> json) =>
    TransactionOtherResponse(
      data: json['data'] == null
          ? null
          : TransactionOtherData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TransactionOtherResponseToJson(
        TransactionOtherResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
