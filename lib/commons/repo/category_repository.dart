import 'package:epass/commons/api/clients/category/category_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:epass/commons/models/customer_type/customer_type_response.dart';
import 'package:simple_result/simple_result.dart';

abstract class ICategoryRepository {
  Future<Result<CategoryModelDataNotNull, Failure>> getPlateTypes();

  Future<Result<CategoryModelDataNotNull, Failure>> getCategories(
      {String? tableName, String? code});

  Future<Result<CategoryModelDataNotNull, Failure>> getVehicleTypes();

  Future<Result<List<CustomerTypeModel>, Failure>> getCustomerTypes();

  Future<Result<CategoryModelDataNotNull, Failure>> getCategoriesConfig();
}

class CategoryRepository implements ICategoryRepository {
  final CategoryClient _categoryClient;

  CategoryRepository({required CategoryClient categoryClient})
      : _categoryClient = categoryClient;

  @override
  Future<Result<CategoryModelDataNotNull, Failure>> getPlateTypes() async {
    try {
      final response = await _categoryClient.getPlateTypes();

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(CategoryModelDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<CategoryModelDataNotNull, Failure>> getCategories(
      {String? tableName, String? code}) async {
    try {
      final response =
          await _categoryClient.getCategories(tableName: tableName, code: code);

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(CategoryModelDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<CategoryModelDataNotNull, Failure>> getVehicleTypes() async {
    try {
      final response = await _categoryClient.getVehicleTypes();

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(CategoryModelDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<CustomerTypeModel>, Failure>> getCustomerTypes() async {
    try {
      final response = await _categoryClient.getCustomerType();

      final listData = response.data;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(listData);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<CategoryModelDataNotNull, Failure>>
      getCategoriesConfig() async {
    try {
      final response = await _categoryClient.getCategoriesConfig();

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(CategoryModelDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
