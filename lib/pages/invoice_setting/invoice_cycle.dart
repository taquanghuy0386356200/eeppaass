enum InvoiceCycle { timely, monthly }

extension InvoiceCycleExt on InvoiceCycle {
  String get label {
    switch (this) {
      case InvoiceCycle.timely:
        return 'Theo lượt';
      case InvoiceCycle.monthly:
        return 'Theo tháng';
    }
  }

  int get value {
    switch (this) {
      case InvoiceCycle.timely:
        return 1;
      case InvoiceCycle.monthly:
        return 3;
    }
  }
}

enum InvoiceCycleGroupType { byContract, byVehicle }

extension InvoiceCycleGroupTypeExt on InvoiceCycleGroupType {
  String get label {
    switch (this) {
      case InvoiceCycleGroupType.byContract:
        return 'Theo hợp đồng';
      case InvoiceCycleGroupType.byVehicle:
        return 'Theo phương tiện';
    }
  }

  int get value {
    switch (this) {
      case InvoiceCycleGroupType.byContract:
        return 0;
      case InvoiceCycleGroupType.byVehicle:
        return 1;
    }
  }
}
