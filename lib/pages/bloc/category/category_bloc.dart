import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/models/customer_type/customer_type_response.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:equatable/equatable.dart';

part 'category_event.dart';

part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  final ICategoryRepository _categoryRepository;

  CategoryBloc({required ICategoryRepository categoryRepository})
      : _categoryRepository = categoryRepository,
        super(CategoryInitial()) {
    on<PlateTypeFetched>(_onPlateTypeFetched);
    on<VehicleTypeFetched>(_onVehicleTypeFetched);
    on<CustomerTypeFetched>(_onCustomerTypeFetched);
  }

  FutureOr<void> _onPlateTypeFetched(PlateTypeFetched event, Emitter<CategoryState> emit) async {
    if (state is PlateTypeFetchedSuccess) {
      emit(PlateTypeFetchedSuccess((state as PlateTypeFetchedSuccess).plateTypes));
      return;
    }

    emit(PlateTypeFetchedInProgress());

    final result = await _categoryRepository.getPlateTypes();

    result.when(
      success: (success) {
        final activeData = success.listData.where((element) => element.isActive == '1').toList();
        emit(PlateTypeFetchedSuccess(activeData));
      },
      failure: (failure) => emit(PlateTypeFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onVehicleTypeFetched(
    VehicleTypeFetched event,
    Emitter<CategoryState> emit,
  ) async {
    emit(const VehicleTypeFetchedInProgress());

    final result = await _categoryRepository.getVehicleTypes();

    result.when(
      success: (success) {
        final filteredData = success.listData.where((element) => element.isActive == '1').toList();
        emit(VehicleTypeFetchedSuccess(vehicleTypes: filteredData));
      },
      failure: (failure) => emit(VehicleTypeFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onCustomerTypeFetched(
    CustomerTypeFetched event,
    Emitter<CategoryState> emit,
  ) async {
    final result = await _categoryRepository.getCustomerTypes();

    result.when(
      success: (success) {
        final filteredData = success.where((element) => element.type == '1').toList();
        emit(CustomerTypeFetchedSuccess(filteredData));
      },
      failure: (failure) => emit(CustomerTypeFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
