import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/parking/config_service_response.dart';
import 'package:epass/commons/models/parking/verify_config_response.dart';
import 'package:epass/commons/repo/parking_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'parking_setting_event.dart';
part 'parking_setting_state.dart';

class ParkingSettingBloc
    extends Bloc<ParkingSettingEvent, ParkingSettingState> {
  final IParkingRepository _parkingRepository;
  final AppBloc _appBloc;

  ParkingSettingBloc({
    required IParkingRepository parkingRepository,
    required AppBloc appBloc,
  })  : _parkingRepository = parkingRepository,
        _appBloc = appBloc,
        super(const ParkingSettingState(
            services: [], serviceVerifyConfig: [], verifyConfigs: [])) {
    on<ServiceFetched>(_onServiceFetched);
    on<ServiceRoadSideFetched>(_onServiceRoadSideFetched);
    on<ServiceEnabled>(_onServiceEnabled);
    on<ParkingSettingAlerted>(_onParkingSettingAlerted);
    on<ParkingRoadSideSettingAlerted>(_onParkingRoadSideSettingAlerted);

    on<VerifyConfigAlerted>(_onVerifyConfigAlerted);
    on<OtpRequested>(_onOtpRequested);
    on<OtpSubmitted>(_onOtpSubmitted);
    on<PassCodeChanged>(_onPassCodeChanged);
    on<PassCodeSubmitted>(_onPassCodeSubmitted);
    on<DialogCancelled>(_onDialogCancelled);
    on<BottomSheetCancelled>(_onBottomSheetCancelled);
    on<SnackBarCancelled>(_onSnackBarCancelled);
  }

  FutureOr<void> _onServiceFetched(
    ServiceFetched event,
    Emitter<ParkingSettingState> emit,
  ) async {
    emit(state.copyWith(isLoading: true, serviceVerifyConfig: [], error: null));

    final result = await _parkingRepository.getConfigServices();

    result.when(
      success: (data) {
        emit(
          state.copyWith(services: data?.services, error: null),
        );
      },
      failure: (failure) => emit(state.copyWith(
          isOn: false,
          isOnRoadSide: false,
          isLoading: false,
          services: [],
          serviceVerifyConfig: [],
          error: failure.message)),
    );

    getCurrentVerifyConfig();
  }

  FutureOr<void> _onServiceRoadSideFetched(
    ServiceRoadSideFetched event,
    Emitter<ParkingSettingState> emit,
  ) async {
    emit(state.copyWith(isLoading: true, serviceVerifyConfig: [], error: null));

    final result = await _parkingRepository.getConfigServices();

    result.when(
      success: (data) {
        emit(
          state.copyWith(services: data?.services, error: null),
        );
      },
      failure: (failure) => emit(state.copyWith(
          isOnRoadSide: false,
          isLoading: false,
          services: [],
          serviceVerifyConfig: [],
          error: failure.message)),
    );

    getCurrentVerifyRoadSideConfig();
  }

  FutureOr<void> getCurrentVerifyRoadSideConfig() async {
    final user = _appBloc.state.user;
    final contractId = user?.contractId;

    var serviceId = 1;
    for (var element in state.services) {
      if (element.code == 'ROAD_PARKING_SERVICE') {
        serviceId = element.serviceId;
      }
    }

    final serviceConfigResult = await _parkingRepository
        .getCurrentVerifyConfig(listServiceId: [serviceId]);

    serviceConfigResult.when(
        success: (configData) async {
          List<VerifyConfigModel> verifyConfigs = [];
          for (var s in state.services) {
            if (s.code == 'ROAD_PARKING_SERVICE') {
              s.verifyConfig?.forEach((e) {
                List<VerifyConfigModel>? filteredConfig =
                    configData?.where((element) {
                  return element.verifyType != null &&
                      element.verifyType == e.verifyType;
                }).toList();
                VerifyConfigModel vcm = VerifyConfigModel(
                    configId: e.verifyConfigId,
                    contractId: int.parse(contractId!),
                    serviceId: serviceId,
                    verifyType: e.verifyType,
                    status: filteredConfig!.isNotEmpty
                        ? filteredConfig.first.status
                        : 0,
                    isCreatedPassCode: filteredConfig.isNotEmpty
                        ? filteredConfig.first.isCreatedPassCode
                        : false);
                verifyConfigs.add(vcm);
              });
            }
          }
          emit(
            state.copyWith(
                isOnRoadSide: configData?.isNotEmpty,
                verifyConfigs: verifyConfigs,
                // isOn: false, // to test case not allowed and policy
                isLoading: false,
                serviceVerifyConfig: configData,
                error: null),
          );
        },
        failure: (failure) => emit(state.copyWith(
            isOnRoadSide: false,
            isLoading: false,
            serviceVerifyConfig: [],
            error: failure.message)));
  }

  FutureOr<void> getCurrentVerifyConfig() async {
    final user = _appBloc.state.user;
    final contractId = user?.contractId;

    var serviceId = 1;
    for (var element in state.services) {
      if (element.code == 'PARKING_SERVICE') {
        serviceId = element.serviceId;
      }
    }

    final serviceConfigResult = await _parkingRepository
        .getCurrentVerifyConfig(listServiceId: [serviceId]);

    serviceConfigResult.when(
        success: (configData) async {
          List<VerifyConfigModel> verifyConfigs = [];
          for (var s in state.services) {
            if (s.code == 'PARKING_SERVICE') {
              s.verifyConfig?.forEach((e) {
                List<VerifyConfigModel>? filteredConfig =
                    configData?.where((element) {
                  return element.verifyType != null &&
                      element.verifyType == e.verifyType;
                }).toList();
                VerifyConfigModel vcm = VerifyConfigModel(
                    configId: e.verifyConfigId,
                    contractId: int.parse(contractId!),
                    serviceId: serviceId,
                    verifyType: e.verifyType,
                    status: filteredConfig!.isNotEmpty
                        ? filteredConfig.first.status
                        : 0,
                    isCreatedPassCode: filteredConfig.isNotEmpty
                        ? filteredConfig.first.isCreatedPassCode
                        : false);
                verifyConfigs.add(vcm);
              });
            }
          }
          emit(
            state.copyWith(
                isOn: configData?.isNotEmpty,
                // isOnRoadSide: configData?.isNotEmpty,
                verifyConfigs: verifyConfigs,
                // isOn: false, // to test case not allowed and policy
                isLoading: false,
                serviceVerifyConfig: configData,
                error: null),
          );
        },
        failure: (failure) => emit(state.copyWith(
            isOn: false,
            // isOnRoadSide: false,
            isLoading: false,
            serviceVerifyConfig: [],
            error: failure.message)));
  }

  FutureOr<void> _onServiceEnabled(
    ServiceEnabled event,
    Emitter<ParkingSettingState> emit,
  ) async {
    emit(state.copyWith(isDoing: true, isPolicyShowing: false, error: null));

    final serviceId = event.serviceId;
    final result = await _parkingRepository.addService(
        serviceId: serviceId!, actionType: 1);

    result.when(
        success: (data) async {
          emit(
            state.copyWith(
                isPolicyShowing: false,
                isDoing: false,
                isOn: true,
                isOnRoadSide: true,
                isLoading: false,
                error: null),
          );
        },
        failure: (failure) => emit(state.copyWith(
            isPolicyShowing: false,
            isDoing: false,
            isOn: false,
            isOnRoadSide: false,
            isLoading: false,
            serviceVerifyConfig: [],
            error: failure.message)));

    if (result.isSuccess) {
      getCurrentVerifyConfig();
    }
  }

  FutureOr<void> _onParkingSettingAlerted(
    ParkingSettingAlerted event,
    emit,
  ) async {
    emit(state.copyWith(
      isDoing: true,
      error: null,
    ));

    if (event.isOn!) {
      // Check condition use service
      final result = await _parkingRepository.checkConditionUseService(
          isRegisterService: true, serviceId: event.serviceId!);

      result.when(success: (res) async {
        if (res?.mess?.code != 1) {
          emit(state.copyWith(
              isDoing: false,
              isPolicyShowing: false,
              isNotAllowedToUseService: true,
              error: null));
        } else if (res?.data?.isFirstTime == true) {
          emit(state.copyWith(
              isDoing: false,
              isPolicyShowing: true,
              isNotAllowedToUseService: false,
              error: null));
        } else {
          emit(state.copyWith(
              isPolicyShowing: false,
              isNotAllowedToUseService: false,
              error: null));
        }
      }, failure: (failure) async {
        emit(state.copyWith(
            isDoing: false,
            isOn: false,
            isLoading: false,
            serviceVerifyConfig: [],
            error: failure.message));
      });
    }

    if (!state.isPolicyShowing && !state.isNotAllowedToUseService) {
      // Turn on/off parking service
      final serviceId = event.serviceId;
      final result = await _parkingRepository.addService(
          serviceId: serviceId!, actionType: event.isOn! ? 1 : 0);

      result.when(success: (data) async {
        emit(
          state.copyWith(
              isDoing: false, isOn: event.isOn, isLoading: false, error: null),
        );
      }, failure: (failure) {
        emit(state.copyWith(
            isDoing: false,
            isOn: false,
            isLoading: false,
            serviceVerifyConfig: [],
            error: failure.message));
      });
    }
  }

  FutureOr<void> _onParkingRoadSideSettingAlerted(
    ParkingRoadSideSettingAlerted event,
    emit,
  ) async {
    emit(state.copyWith(
      isDoing: true,
      error: null,
    ));

    if (event.isOnRoadSide!) {
      // Check condition use service
      final result = await _parkingRepository.checkConditionUseService(
          isRegisterService: true, serviceId: event.serviceId!);

      result.when(success: (res) async {
        if (res?.mess?.code != 1) {
          emit(state.copyWith(
              isDoing: false,
              isPolicyShowing: false,
              isNotAllowedToUseService: true,
              error: null));
        } else if (res?.data?.isFirstTime == true) {
          emit(state.copyWith(
              isDoing: false,
              isPolicyShowing: true,
              isNotAllowedToUseService: false,
              error: null));
        } else {
          emit(state.copyWith(
              isPolicyShowing: false,
              isNotAllowedToUseService: false,
              error: null));
        }
      }, failure: (failure) async {
        emit(state.copyWith(
            isDoing: false,
            isOnRoadSide: false,
            isLoading: false,
            serviceVerifyConfig: [],
            error: failure.message));
      });
    }

    if (!state.isPolicyShowing && !state.isNotAllowedToUseService) {
      // Turn on/off parking service
      final result = await _parkingRepository.addService(
          serviceId: 256, actionType: event.isOnRoadSide! ? 1 : 0);

      result.when(success: (data) async {
        emit(
          state.copyWith(
              isDoing: false,
              isOnRoadSide: event.isOnRoadSide,
              isLoading: false,
              error: null),
        );
      }, failure: (failure) {
        emit(state.copyWith(
            isDoing: false,
            isOnRoadSide: false,
            isLoading: false,
            serviceVerifyConfig: [],
            error: failure.message));
      });
    }
  }

  FutureOr<void> _onVerifyConfigAlerted(
    VerifyConfigAlerted event,
    emit,
  ) async {
    bool isPassCodeSetting =
        event.verifyConfigModel!.verifyType == 2 && event.isOn!;
    bool isOtpConfirming =
        event.verifyConfigModel!.verifyType == 1 || !isPassCodeSetting;
    emit(state.copyWith(
        isPassCodeSetting: isPassCodeSetting,
        isOtpConfirming: isOtpConfirming,
        confirmType: 19,
        activeVerifyConfig: event.verifyConfigModel,
        isActiveVerifyConfigOn: event.isOn,
        error: null,
        success: null));
    if (isOtpConfirming) {
      // Request OTP
      final user = _appBloc.state.user;
      final phoneNumber = user?.phone;
      final result = await _parkingRepository.requestOtp(
          phone: phoneNumber!, confirmType: state.confirmType);

      result.when(
        success: (success) {
          emit(state.copyWith(
              isPassCodeSetting: false,
              isOtpConfirming: false,
              confirmType: 19,
              activeVerifyConfig: event.verifyConfigModel,
              isActiveVerifyConfigOn: event.isOn,
              error: null,
              success: 'Mã xác thực đã được gửi đến số điện thoại của bạn'));
        },
        failure: (failure) {
          emit(state.copyWith(
              isDoing: false,
              isLoading: false,
              isOtpConfirming: false,
              activeVerifyConfig: null,
              isActiveVerifyConfigOn: event.isOn,
              error: failure.message,
              success: null));
        },
      );
    }
  }

  FutureOr<void> _onOtpSubmitted(
    OtpSubmitted event,
    emit,
  ) async {
    final serviceId = state.activeVerifyConfig?.serviceId;
    final verifyType = state.activeVerifyConfig?.verifyType;
    if (state.confirmType == 19) {
      final result = await _parkingRepository.configMethodVerify(
          serviceId: serviceId!,
          verifyType: verifyType!,
          actionType: state.isActiveVerifyConfigOn ? 1 : 0,
          otp: event.otp,
          passCode: state.newPassCode);

      result.when(
          success: (res) async {
            if (res?.mess?.code == 1) {
              List<VerifyConfigModel> verifyConfigs = [];
              for (var p in state.verifyConfigs) {
                if (p.verifyType != state.activeVerifyConfig?.verifyType) {
                  verifyConfigs.add(p);
                } else {
                  VerifyConfigModel vcm = VerifyConfigModel(
                    configId: p.configId,
                    contractId: p.contractId,
                    serviceId: p.serviceId,
                    verifyType: p.verifyType,
                    serviceName: p.serviceName,
                    status: res?.data?.status,
                    isCreatedPassCode: res?.data?.isCreatedPassCode,
                  );
                  verifyConfigs.add(vcm);
                }
              }
              emit(state.copyWith(
                  success: state.isActiveVerifyConfigOn
                      ? "Bật phương thức xác thực thành công"
                      : "Tắt phương thức xác thực thành công",
                  isOtpConfirming: false,
                  isPassCodeSetting: false,
                  verifyConfigs: verifyConfigs,
                  isDoing: false,
                  newPassCode: '',
                  isOtpSuccess: true,
                  error: null));
            } else {
              emit(state.copyWith(
                  isOtpConfirming: false,
                  isPassCodeSetting: false,
                  isDoing: false,
                  newPassCode: '',
                  error: res?.mess?.description ??
                      (state.isActiveVerifyConfigOn
                          ? "Bật phương thức xác thực thất bại!"
                          : "Tắt phương thức xác thực thất bại!")));
            }
          },
          failure: (failure) => emit(state.copyWith(
              isOtpConfirming: false,
              isPassCodeSetting: false,
              isDoing: false,
              isLoading: false,
              error: failure.message
              // error: state.isActiveVerifyConfigOn ?
              // "Bật phương thức xác thực thất bại!" : "Tắt phương thức xác thực thất bại!"

              )));
    } else {
      final result = await _parkingRepository.changePassCode(
          serviceId: serviceId!, passCode: state.newPassCode, otp: event.otp);

      result.when(
          success: (res) async {
            if (res?.mess?.code == 1) {
              List<VerifyConfigModel> verifyConfigs = [];
              for (var p in state.verifyConfigs) {
                if (p.verifyType != state.activeVerifyConfig?.verifyType) {
                  verifyConfigs.add(p);
                } else {
                  VerifyConfigModel vcm = VerifyConfigModel(
                      configId: p.configId,
                      contractId: p.contractId,
                      serviceId: p.serviceId,
                      verifyType: p.verifyType,
                      serviceName: p.serviceName,
                      status: res?.data?.status,
                      isCreatedPassCode: res?.data?.isCreatedPassCode);
                  verifyConfigs.add(vcm);
                }
              }
              emit(state.copyWith(
                  success: "Đổi mật khẩu thành công!",
                  isOtpConfirming: false,
                  isPassCodeSetting: false,
                  verifyConfigs: verifyConfigs,
                  isDoing: false,
                  isOtpSuccess: true,
                  error: null));
            } else {
              emit(state.copyWith(
                  isOtpConfirming: false,
                  isPassCodeSetting: false,
                  isDoing: false,
                  error: res?.mess?.description ?? "Đổi mật khẩu thất bại!"));
            }
          },
          failure: (failure) => emit(state.copyWith(
              isDoing: false,
              isLoading: false,
              isOtpConfirming: false,
              isPassCodeSetting: false,
              error: failure.message ?? "Đổi mật khẩu thất bại!")));
    }
  }

  FutureOr<void> _onOtpRequested(
    OtpRequested event,
    emit,
  ) async {
    // Request OTP
    final user = _appBloc.state.user;
    final phoneNumber = user?.phone;
    final result = await _parkingRepository.requestOtp(
        phone: phoneNumber!, confirmType: state.confirmType);
  }

  FutureOr<void> _onPassCodeChanged(
    PassCodeChanged event,
    emit,
  ) async {
    emit(state.copyWith(
        isOtpConfirming: false,
        isPassCodeSetting: true,
        activeVerifyConfig: event.verifyConfigModel,
        isDoing: false,
        confirmType: 18,
        error: null));
  }

  FutureOr<void> _onPassCodeSubmitted(
    PassCodeSubmitted event,
    emit,
  ) async {
    final user = _appBloc.state.user;
    final phoneNumber = user?.phone;
    final result = await _parkingRepository.requestOtp(
        phone: phoneNumber!, confirmType: state.confirmType);

    result.when(
      success: (success) {
        emit(state.copyWith(
            isOtpConfirming: true,
            isPassCodeSetting: false,
            isDoing: false,
            confirmType: event.confirmType,
            newPassCode: event.passCode,
            error: null,
            success: 'Mã xác thực đã được gửi đến số điện thoại của bạn'));
      },
      failure: (failure) {
        emit(state.copyWith(
            isDoing: false,
            isLoading: false,
            isOtpConfirming: true,
            activeVerifyConfig: null,
            error: failure.message));
      },
    );
  }

  FutureOr<void> _onBottomSheetCancelled(
    BottomSheetCancelled event,
    emit,
  ) async {
    emit(state.copyWith(
      isPassCodeSetting: false,
      isOtpConfirming: false,
      isPolicyShowing: false,
      error: null,
    ));
  }

  FutureOr<void> _onSnackBarCancelled(
    SnackBarCancelled event,
    emit,
  ) async {
    emit(state.copyWith(
      isPassCodeSetting: false,
      isOtpConfirming: false,
      isPolicyShowing: false,
      success: null,
      error: null,
    ));
  }

  FutureOr<void> _onDialogCancelled(
    DialogCancelled event,
    emit,
  ) async {
    emit(state.copyWith(
      isPassCodeSetting: false,
      isOtpConfirming: false,
      isPolicyShowing: false,
      isNotAllowedToUseService: false,
      success: null,
      error: null,
    ));
  }
}
