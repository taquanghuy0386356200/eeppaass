// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history_request_manage_account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HistoryRequestsManageAccountTotalResponse
    _$HistoryRequestsManageAccountTotalResponseFromJson(
            Map<String, dynamic> json) =>
        HistoryRequestsManageAccountTotalResponse(
          data: json['data'] == null
              ? null
              : HistoryRequestsManageAccount.fromJson(
                  json['data'] as Map<String, dynamic>),
          mess: json['mess'] == null
              ? null
              : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
        );

Map<String, dynamic> _$HistoryRequestsManageAccountTotalResponseToJson(
        HistoryRequestsManageAccountTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

HistoryRequestsManageAccount _$HistoryRequestsManageAccountFromJson(
        Map<String, dynamic> json) =>
    HistoryRequestsManageAccount(
      listData: (json['listData'] as List<dynamic>?)
          ?.map(
              (e) => HistoryRequestCategory.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$HistoryRequestsManageAccountToJson(
        HistoryRequestsManageAccount instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

HistoryRequestCategory _$HistoryRequestCategoryFromJson(
        Map<String, dynamic> json) =>
    HistoryRequestCategory(
      ticketId: json['ticketId'] as String?,
      l1TicketTypeId: json['l1TicketTypeId'] as String?,
      l2TicketTypeId: json['l2TicketTypeId'] as String?,
      l3TicketTypeId: json['l3TicketTypeId'] as String?,
      ticketStatus: json['ticketStatus'] as String?,
      rejectReasonDetail: json['rejectReasonDetail'] as String?,
      cancelReason: json['cancelReason'] as String?,
      createDate: json['createDate'] as String?,
      ticketProcessId: json['ticketProcessId'] as int?,
      approveStatus: json['approveStatus'] as int?,
      processTime: json['processTime'] as String?,
      approveStatusName: json['approveStatusName'] as String?,
      reasonLevel1: json['reasonLevel1'] as String?,
      rejectReason: json['rejectReason'] as String?,
      reasonLevel1Name: json['reasonLevel1Name'] as String?,
    );

Map<String, dynamic> _$HistoryRequestCategoryToJson(
        HistoryRequestCategory instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'l1TicketTypeId': instance.l1TicketTypeId,
      'l2TicketTypeId': instance.l2TicketTypeId,
      'l3TicketTypeId': instance.l3TicketTypeId,
      'ticketStatus': instance.ticketStatus,
      'createDate': instance.createDate,
      'ticketProcessId': instance.ticketProcessId,
      'approveStatus': instance.approveStatus,
      'processTime': instance.processTime,
      'approveStatusName': instance.approveStatusName,
      'reasonLevel1': instance.reasonLevel1,
      'rejectReasonDetail': instance.rejectReasonDetail,
      'rejectReason': instance.rejectReason,
      'reasonLevel1Name': instance.reasonLevel1Name,
      'cancelReason': instance.cancelReason,
    };
