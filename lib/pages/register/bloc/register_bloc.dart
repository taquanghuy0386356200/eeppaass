import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/register/register_request.dart';
import 'package:epass/commons/repo/register_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'register_event.dart';

part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final IRegisterRepository _registerRepository;

  RegisterBloc({required IRegisterRepository registerRepository})
      : _registerRepository = registerRepository,
        super(RegisterInitial()) {
    on<RegisterSubmitted>((event, emit) async {
      emit(const RegisterSubmittedInProgress());

      final result = await _registerRepository.register(request: event.request);

      result.when(
        success: (success) => emit(const RegisterSubmittedSuccess()),
        failure: (failure) => emit(RegisterSubmittedFailure(failure.message ?? 'Có lỗi xảy ra')),
      );
    });
  }
}
