part of 'forgot_password_otp_bloc.dart';

abstract class ForgotPasswordOtpEvent extends Equatable {
  const ForgotPasswordOtpEvent();
}

class ForgotPasswordOTPConfirmed extends ForgotPasswordOtpEvent {
  final String plateNumber;
  final String plateTypeCode;
  final String phoneNumber;
  final String otp;
  final String newPassword;

  const ForgotPasswordOTPConfirmed({
    required this.plateNumber,
    required this.plateTypeCode,
    required this.phoneNumber,
    required this.otp,
    required this.newPassword,
  });

  @override
  List<Object> get props => [
        plateNumber,
        plateTypeCode,
        phoneNumber,
        otp,
        newPassword,
      ];
}
