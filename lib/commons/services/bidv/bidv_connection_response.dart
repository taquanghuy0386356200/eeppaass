import 'package:xml/xml.dart' as xml;

class BidvConnectionResponse {
  final String serviceId;
  final String merchantId;
  final String tranDate;
  final String transId;
  final String responseCode;
  final String moreInfo;
  final String redirectUrl;
  final String secureCode;

  BidvConnectionResponse({
    required this.serviceId,
    required this.merchantId,
    required this.tranDate,
    required this.transId,
    required this.responseCode,
    required this.moreInfo,
    required this.secureCode,
    required this.redirectUrl,
  });

  factory BidvConnectionResponse.fromMap(xml.XmlDocument document) {
    return BidvConnectionResponse(
      serviceId: document.findAllElements('ns0:Service_Id').first.text,
      merchantId: document.findAllElements('ns0:Merchant_Id').first.text,
      tranDate: document.findAllElements('ns0:Trandate').first.text,
      transId: document.findAllElements('ns0:Trans_Id').first.text,
      responseCode: document.findAllElements('ns0:Response_Code').first.text,
      moreInfo: document.findAllElements('ns0:More_Info').first.text,
      secureCode: document.findAllElements('ns0:Secure_Code').first.text,
      redirectUrl: document.findAllElements('ns0:Redirect_Url').first.text,
    );
  }
}
