import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/clients/ocr/ocr_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/ocr_id_card/id_card_ocr.dart';
import 'package:epass/commons/models/ocr_id_card/id_card_ocr_request.dart';
import 'package:epass/commons/models/register/register_request.dart';
import 'package:epass/commons/models/register/request_otp_register_request.dart';
import 'package:loggy/loggy.dart';
import 'package:simple_result/simple_result.dart';

abstract class IRegisterRepository {
  Future<Result<IdCardOcr, Failure>> scanIdCard({required String imageBase64});

  Future<Result<String?, Failure>> requestOTPRegister({required String phoneNumber});

  Future<Result<String?, Failure>> register({required RegisterRequest request});
}

class RegisterRepository implements IRegisterRepository {
  final OCRClient _ocrClient;
  final CrmClient _crmClient;

  RegisterRepository({
    required OCRClient ocrClient,
    required CrmClient crmClient,
  })  : _crmClient = crmClient,
        _ocrClient = ocrClient;

  @override
  Future<Result<IdCardOcr, Failure>> scanIdCard({
    required String imageBase64,
  }) async {
    try {
      final response = await _ocrClient.scanIdCard(
        idCardOcrRequest: IdCardOcrRequest(image: imageBase64),
      );
      if (response.resultCode != 200) {
        logDebug('message: ${response.message}');
        return const Result.failure(UnknownFailure());
      }
      return Result.success(response);
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> requestOTPRegister({required String phoneNumber}) async {
    try {
      final request = RequestOtpRegisterRequest(
        phone: phoneNumber,
      );

      final response = await _crmClient.requestOTPRegister(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE && response.mess?.code != 1030 // Mã lỗi mã giao dịch đã dc gửi
          ) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }

  @override
  Future<Result<String?, Failure>> register({required RegisterRequest request}) async {
    try {
      final response = await _crmClient.register(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }
}
