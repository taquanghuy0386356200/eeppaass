import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:epass/commons/models/campaign/campaign.dart';
import 'package:epass/pages/home/bloc/home_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:collection/collection.dart';
import 'package:url_launcher/url_launcher_string.dart';

class BannerCarousel extends StatelessWidget {
  const BannerCarousel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<HomeBloc, HomeState, List<Campaign>>(
      selector: (state) {
        if (state is HomeLoaded) return state.banners ?? [];
        return [];
      },
      builder: (context, banners) {
        final bannerImagePaths = banners.map((banner) => banner.imagePath).whereNotNull().toList();

        return bannerImagePaths.isNotEmpty
            ? CarouselSlider.builder(
                itemCount: bannerImagePaths.length,
                itemBuilder: (context, index, realIndex) {
                  final item = banners[index];
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: InkWell(
                      onTap: () async {
                        final isUrlExternal = item.isUriExternal;

                        if (isUrlExternal == '1') {
                          final uri = item.uri;
                          if (uri != null && uri.isNotEmpty) {
                            await launchUrlString(
                              uri,
                              mode: LaunchMode.externalApplication
                            );
                          }
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          image: DecorationImage(
                            image: CachedNetworkImageProvider(
                              bannerImagePaths[index],
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  );
                },
                options: CarouselOptions(
                  autoPlay: true,
                  viewportFraction: 0.9,
                  autoPlayInterval: const Duration(seconds: 3),
                  aspectRatio: 2.25,
                ),
              )
            : const SizedBox.shrink();
      },
    );
  }
}
