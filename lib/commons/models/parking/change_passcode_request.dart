import 'package:json_annotation/json_annotation.dart';

part 'change_passcode_request.g.dart';

@JsonSerializable()
class ChangePassCodeRequest {
  int serviceId;
  String passCode;
  String otp;

  ChangePassCodeRequest({
    required this.serviceId,
    required this.passCode,
    required this.otp
  });

  Map<String, dynamic> toJson() => _$ChangePassCodeRequestToJson(this);
}