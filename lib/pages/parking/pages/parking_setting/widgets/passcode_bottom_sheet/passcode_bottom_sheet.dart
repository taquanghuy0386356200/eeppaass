import 'package:epass/pages/parking/pages/parking_setting/bloc/parking_setting_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/passcode_bottom_sheet/widgets/change_passcode_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'bloc/passcode_bottom_sheet_bloc.dart';

class PassCodeBottomSheet extends StatefulWidget {
  final ParkingSettingBloc parkingBloc;
  final bool isCreatePassCode;

  const PassCodeBottomSheet({Key? key, required this.parkingBloc, required this.isCreatePassCode}) : super(key: key);

  @override
  State<PassCodeBottomSheet> createState() => _PassCodeBottomSheetState();
}

class _PassCodeBottomSheetState extends State<PassCodeBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PasscodeBottomSheetBloc, PasscodeBottomSheetState>(
        listener: (context, state) {

        },
        builder: (context, state) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 10.h,
                width: 60.w,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.5),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(24.0),
                  ),
                ),
              ),
              SizedBox(height: 10.h),
              Text( '${widget.isCreatePassCode? 'Tạo mật khẩu' : 'Đổi mật khẩu'} xác thực thanh toán tại điểm giao dịch',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      fontSize: 18.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.bold)),
              SizedBox(height: 6.h),
              Material(
                  color: Colors.white,
                  child: ChangePasscodeForm(parkingBloc: widget.parkingBloc, isCreatePassCode: widget.isCreatePassCode,)
              )
            ],
          );
        },
    );
  }
}
