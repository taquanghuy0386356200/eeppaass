import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FeedbackHorizontalWidget extends StatelessWidget {
  const FeedbackHorizontalWidget({
    Key? key,
    this.title = 'Thời gian xử lý',
    this.content = '22/02/2022 19:00:00',
    this.contentColor = ColorName.textGray2,
    this.contentFontWeight
  }) : super(key: key);

  final String title;
  final String content;
  final Color contentColor;
  final FontWeight? contentFontWeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 16.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
              fontWeight: FontWeight.w700,
            ),
          ),
          Text(
            content,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: contentColor,
              fontWeight:  contentFontWeight,
            ),
          )
        ],
      ),
    );
  }
}
