import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/balance/balance_bloc.dart';
import 'package:epass/pages/home/widgets/home_currency_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BalanceAltWidget extends StatelessWidget {
  final double? mainTextSize;
  final Color? mainTextColor;

  const BalanceAltWidget({
    Key? key,
    this.mainTextSize,
    this.mainTextColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BalanceBloc, BalanceState>(
      listener: (context, state) {
        if (state.error != null) {
          showErrorSnackBBar(
            context: context,
            message: state.error!,
          );
        }
      },
      builder: (context, state) => state.isLoading
          ? Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '— — — —',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        fontWeight: FontWeight.w700,
                        fontSize: mainTextSize,
                      ),
                ),
                Text(
                  ' đ',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ],
            )
          : HomeCurrencyText(
              text: (state.balance ?? 0).formatCurrency(),
              color: mainTextColor ?? ColorName.textGray1,
              fontSize: mainTextSize,
            ),
    );
  }
}
