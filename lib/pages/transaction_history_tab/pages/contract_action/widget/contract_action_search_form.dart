import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/contract_action/bloc/contract_action_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContractActionSearchForm extends StatefulWidget {
  final DateTime? startDate;
  final DateTime? endDate;

  const ContractActionSearchForm({
    Key? key,
    this.startDate,
    this.endDate,
  }) : super(key: key);

  @override
  State<ContractActionSearchForm> createState() =>
      _ContractActionSearchFormState();
}

class _ContractActionSearchFormState extends State<ContractActionSearchForm> {
  final _startDateController = TextEditingController();
  final _endDateController = TextEditingController();

  DateTime? _startDate;
  DateTime? _endDate;

  @override
  void initState() {
    super.initState();
    context.read<ContractActionBloc>().isSearching = false;
    _startDate = widget.startDate;
    _endDate = widget.endDate;

    if (_startDate != null) {
      _startDateController.text = Jiffy(_startDate).format('dd/MM/yyyy');
    }

    if (_endDate != null) {
      _endDateController.text = Jiffy(_endDate).format('dd/MM/yyyy');
    }
  }

  @override
  void dispose() {
    _startDateController.dispose();
    _endDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Positioned(
            top: 12.h,
            right: 12.w,
            child: SplashIconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: ColorName.borderColor,
              ),
              onTap: () => Navigator.of(context).pop(),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'TÌM KIẾM',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(height: 24.h),
                PrimaryTextField(
                  controller: _startDateController,
                  labelText: 'Ngày giao dịch: từ ngày',
                  hintText: 'Từ ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showContractActionDateRangePicker,
                  onClear: () => _startDate = null,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _endDateController,
                  labelText: 'Ngày giao dịch: đến ngày',
                  hintText: 'Đến ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showContractActionDateRangePicker,
                  onClear: () => _endDate = null,
                ),
                SizedBox(height: 34.h),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: SecondaryButton(
                          onTap: () {
                            context
                                .read<ContractActionBloc>()
                                .add(const ContractActionFetched());
                            context.read<ContractActionBloc>().isSearching =
                                false;
                            Navigator.of(context).pop();
                          },
                          title: 'Đặt lại',
                        ),
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: PrimaryButton(
                          title: 'Tìm kiếm',
                          onTap: () {
                            context.read<ContractActionBloc>().add(
                                  ContractActionSearched(
                                    startDate: _startDate,
                                    endDate: _endDate,
                                  ),
                                );
                            context.read<ContractActionBloc>().isSearching =
                                true;
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // SizedBox(height: 16.h),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _showContractActionDateRangePicker() async {
    DateTimeRange? initialDateRange;
    if (_startDate != null) {
      initialDateRange = DateTimeRange(
        start: _startDate!,
        end: _endDate ?? Jiffy().dateTime,
      );
    }

    final dateRange = await showDateRangePicker(
          context: context,
          locale: const Locale('vi'),
          initialDateRange: initialDateRange,
          firstDate: Jiffy().subtract(years: 2).startOf(Units.DAY).dateTime,
          lastDate: Jiffy().dateTime,
          initialEntryMode: DatePickerEntryMode.calendarOnly,
        ) ??
        initialDateRange;

    _startDate = dateRange?.start;
    _endDate = dateRange?.end;

    if (_startDate != null) {
      _startDateController.text = Jiffy(_startDate).format('dd/MM/yyyy');
    }

    if (_endDate != null) {
      _endDateController.text = Jiffy(_endDate).format('dd/MM/yyyy');
    }
  }
}
