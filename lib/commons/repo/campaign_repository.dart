import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/campaign/campaign.dart';
import 'package:simple_result/simple_result.dart';

abstract class ICampaignRepository {
  Future<Result<CampaignData, Failure>> getCampaigns();
}

class CampaignRepository extends ICampaignRepository {
  final CrmClient _crmClient;

  CampaignRepository({required CrmClient crmClient}) : _crmClient = crmClient;

  @override
  Future<Result<CampaignData, Failure>> getCampaigns() async {
    try {
      final response = await _crmClient.getCampaigns();

      final campaignData = response.campaignData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || campaignData == null) {
        return Result.failure(
          ResponseFailure(
            message: response.mess?.description ?? '',
          ),
        );
      }

      return Result.success(campaignData);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception catch (err) {
      return Result.failure(UnknownFailure(message: err.toString()));
    }
  }
}
