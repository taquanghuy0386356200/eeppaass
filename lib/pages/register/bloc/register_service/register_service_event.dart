part of 'register_service_bloc.dart';

@immutable
abstract class RegisterServiceEvent extends Equatable {
  const RegisterServiceEvent();
}

class RegisterServiceUpdated extends RegisterServiceEvent {
  final InvoiceCycle? invoiceCycle;
  final InvoiceCycleGroupType? invoiceCycleGroupType;

  const RegisterServiceUpdated({
    this.invoiceCycle,
    this.invoiceCycleGroupType,
  });

  @override
  List<Object?> get props => [invoiceCycle, invoiceCycleGroupType];
}
