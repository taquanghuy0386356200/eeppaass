import 'package:epass/commons/models/station/station_stage.dart';
import 'package:epass/commons/repo/station_repository.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/text_field_circular_progress_indicator.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/buy_ticket/bloc/station_stage_bloc.dart';
import 'package:epass/pages/buy_ticket/widgets/station_stage_dropdown/bloc/station_stage_dropdown_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StationStageDropdown extends StatelessWidget {
  const StationStageDropdown({Key? key, this.haveChoose}) : super(key: key);
  final Function(bool value)? haveChoose;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => StationStageDropdownBloc(
        stationRepository: getIt<IStationRepository>(),
      ),
      child: _StationStageDropdown(haveChoose: haveChoose,),
    );
  }
}

class _StationStageDropdown extends StatefulWidget {
  const _StationStageDropdown({Key? key, this.haveChoose}) : super(key: key);
  final Function(bool value)? haveChoose;

  @override
  State<_StationStageDropdown> createState() => _StationStageDropdownState();
}

class _StationStageDropdownState extends State<_StationStageDropdown> {
  @override
  void initState() {
    context
        .read<StationStageDropdownBloc>()
        .add(const StationStageDropdownFetched());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<StationStageDropdownBloc, StationStageDropdownState>(
      listener: (context, state) {
        if (state is StationStageDropdownFetchedFailure) {
          showErrorSnackBBar(context: context, message: state.message);
        }
      },
      builder: (context, state) {
        return PrimaryDropdown<StationStage>(
          key: UniqueKey(),
          isMandatory: true,
          label: 'Trạm/Đoạn',
          dropdownHeight: 205.h,
          items: state is StationStageDropdownFetchedSuccess ? state.data : [],
          onChanged: (stationStage) {
            if(widget.haveChoose != null) {
              if(stationStage != null) {
                widget.haveChoose!(true);
              } else {
                widget.haveChoose!(false);
              }
            }
            context
                .read<StationStageBloc>()
                .add(StationStageChanged(stationStage: stationStage));
          },
          suffix: state is StationStageDropdownFetchedInProgress
              ? const TextFieldCircularProgressIndicator()
              : Assets.icons.chevronDown.svg(),
        );
      },
    );
  }
}
