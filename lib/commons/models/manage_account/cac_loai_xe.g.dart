// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cac_loai_xe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CacLoaiXeTotalReponse _$CacLoaiXeTotalReponseFromJson(
        Map<String, dynamic> json) =>
    CacLoaiXeTotalReponse(
      data: json['data'] == null
          ? null
          : CacLoaiXeReponse.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CacLoaiXeTotalReponseToJson(
        CacLoaiXeTotalReponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

CacLoaiXeReponse _$CacLoaiXeReponseFromJson(Map<String, dynamic> json) =>
    CacLoaiXeReponse(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => LoaiXeResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$CacLoaiXeReponseToJson(CacLoaiXeReponse instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

LoaiXeResponse _$LoaiXeResponseFromJson(Map<String, dynamic> json) =>
    LoaiXeResponse(
      id: json['id'] as String?,
      tableName: json['tableName'] as String?,
      name: json['name'] as String?,
      code: json['code'] as String?,
      is_default: json['is_default'] as String?,
      description: json['description'] as String?,
      is_active: json['is_active'] as String?,
    );

Map<String, dynamic> _$LoaiXeResponseToJson(LoaiXeResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'tableName': instance.tableName,
      'name': instance.name,
      'code': instance.code,
      'is_default': instance.is_default,
      'description': instance.description,
      'is_active': instance.is_active,
    };
