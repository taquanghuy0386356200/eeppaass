part of 'create_beneficiary_bloc.dart';

abstract class CreateBeneficiaryEvent extends Equatable {
  const CreateBeneficiaryEvent();
}

class CreateBeneficiaryFormSubmitted extends CreateBeneficiaryEvent {
  final String contractNo;
  final String customerName;
  final String reminiscentName;

  const CreateBeneficiaryFormSubmitted({
    required this.contractNo,
    required this.customerName,
    required this.reminiscentName,
  });

  @override
  List<Object> get props => [contractNo, customerName, reminiscentName];
}
