import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VerticalLabel extends StatelessWidget {
  final String label;
  final String value;

  const VerticalLabel({
    Key? key,
    required this.label,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          label,
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                color: ColorName.borderColor,
              ),
        ),
        SizedBox(height: 6.h),
        Text(
          value,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 15.sp,
                fontWeight: FontWeight.w600,
              ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        )
      ],
    );
  }
}
