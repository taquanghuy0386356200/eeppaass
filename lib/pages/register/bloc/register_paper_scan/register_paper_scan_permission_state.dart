part of 'register_paper_scan_permission_bloc.dart';

abstract class RegisterPaperScanPermissionState extends Equatable {
  const RegisterPaperScanPermissionState();
}

class RegisterPaperScanPermissionInitial extends RegisterPaperScanPermissionState {
  @override
  List<Object> get props => [];
}

class RegisterPaperScanPermissionRequestedInProgress extends RegisterPaperScanPermissionState {
  @override
  List<Object> get props => [];
}

class RegisterPaperScanPermissionRequestedSuccess extends RegisterPaperScanPermissionState {
  final bool isFront;

  const RegisterPaperScanPermissionRequestedSuccess(this.isFront);

  @override
  List<Object> get props => [isFront];
}

class RegisterPaperScanPermissionRequestedFailure extends RegisterPaperScanPermissionState {
  final String message;

  const RegisterPaperScanPermissionRequestedFailure(this.message);

  @override
  List<Object> get props => [message];
}