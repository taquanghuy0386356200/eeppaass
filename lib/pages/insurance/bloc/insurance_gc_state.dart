part of 'insurance_gc_bloc.dart';

abstract class InsuranceGcState extends Equatable {
  const InsuranceGcState();
}

class InsuranceGcInitial extends InsuranceGcState {
  @override
  List<Object> get props => [];
}

class ContractEncryptedFetchedInProgress extends InsuranceGcState {
  const ContractEncryptedFetchedInProgress();

  @override
  List<Object> get props => [];
}

class ContractEncryptedFetchedSuccess extends InsuranceGcState {
  final String contractNoEncrypted;

  const ContractEncryptedFetchedSuccess(this.contractNoEncrypted);

  @override
  List<Object> get props => [contractNoEncrypted];
}

class ContractEncryptedFetchedFailure extends InsuranceGcState {
  final String message;

  const ContractEncryptedFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}
