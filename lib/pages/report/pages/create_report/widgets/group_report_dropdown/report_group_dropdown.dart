import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:epass/commons/widgets/textfield/text_field_circular_progress_indicator.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/report/pages/create_report/widgets/group_report_dropdown/bloc/report_group_dropdown_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum ReportGroupDropdownType { group, kind, type }

extension ReportGroupDropdownTypeExt on ReportGroupDropdownType {
  String get label {
    switch (this) {
      case ReportGroupDropdownType.group:
        return 'Nhóm phản ánh';
      case ReportGroupDropdownType.kind:
        return 'Thể loại';
      case ReportGroupDropdownType.type:
        return 'Loại phản ánh';
    }
  }
}

class ReportGroupDropdown extends StatelessWidget {
  const ReportGroupDropdown({
    Key? key,
    required this.reportGroupDropdownType,
    this.parentId,
    this.selectedItem,
    this.isMandatory = false,
    this.onChanged,
  }) : super(key: key);

  final ReportGroupDropdownType reportGroupDropdownType;
  final int? parentId;
  final bool isMandatory;
  final FeedbackType? selectedItem;
  final Function(FeedbackType?)? onChanged;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ReportGroupDropdownBloc(feedbackRepository: getIt<IFeedbackRepository>())
        ..add(ReportGroupDropdownFetched(
          parentId: parentId
        )),
      child: Builder(
        builder: (context) {
          return BlocBuilder<ReportGroupDropdownBloc, ReportGroupDropdownState>(
            builder: (context, state) {
              return PrimaryDropdown<FeedbackType>(
                isMandatory: isMandatory,
                label: reportGroupDropdownType == ReportGroupDropdownType.type ? reportGroupDropdownType.label : '${reportGroupDropdownType.label}',
                enabled: state is ReportGroupDropdownFetchedSuccess && state.data.isNotEmpty,
                dropdownHeight: 205.h,
                items: state is ReportGroupDropdownFetchedSuccess ? state.data : [],
                selectedItem: selectedItem,
                validator: (value) {
                  if (value == null) {
                    return 'Vui lòng chọn ${reportGroupDropdownType.label}';
                  }
                  return null;
                },
                onChanged: (feedbackType) {
                  onChanged?.call(feedbackType);
                },
                suffix: state is ReportGroupDropdownFetchedInProgress
                    ? const TextFieldCircularProgressIndicator()
                    : Assets.icons.chevronDown.svg(),
              );
            },
          );
        },
      ),
    );
  }
}
