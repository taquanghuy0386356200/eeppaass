part of 'report_group_dropdown_bloc.dart';

abstract class ReportGroupDropdownEvent extends Equatable {
  const ReportGroupDropdownEvent();
}

class ReportGroupDropdownFetched extends ReportGroupDropdownEvent {
  final int? parentId;

  const ReportGroupDropdownFetched({
    this.parentId
  });

  @override
  List<Object?> get props => [parentId];
}

