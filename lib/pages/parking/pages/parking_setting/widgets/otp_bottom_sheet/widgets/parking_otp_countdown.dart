import 'dart:async';

import 'package:epass/commons/repo/parking_repository.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/bloc/parking_setting_bloc.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class ParkingOTPCountdown extends StatefulWidget {
  final ParkingSettingBloc parkingBloc;
  final DateTime otpSubmittedTime;
  final VoidCallback onResend;

  const ParkingOTPCountdown({
    Key? key,
    required this.parkingBloc,
    required this.otpSubmittedTime,
    required this.onResend
  }) : super(key: key);

  @override
  State<ParkingOTPCountdown> createState() => _ParkingOTPCountdownState();
}

class _ParkingOTPCountdownState extends State<ParkingOTPCountdown> {
  Timer? timer;
  String countdownTime = '';
  bool countdownFinished = false;
  DateTime? otpResubmittedTime;
  int count = 0;
  @override
  void initState() {
    super.initState();
    setState(() {
      count = DateTime
          .now()
          .difference(otpResubmittedTime ?? widget.otpSubmittedTime )
          .inSeconds;
    });

    initTimer();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.left,
      text: TextSpan(
        children: [
          TextSpan(
            text: countdownFinished
                ? 'Không nhận được mã? '
                : 'Mã OTP sẽ được gửi lại sau: ',
            style: Theme
                .of(context)
                .textTheme
                .bodyText1,
          ),
          TextSpan(
              text: countdownFinished
                  ? 'Gửi lại'
                  : countdownTime,
              style: countdownFinished ? Theme
                  .of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(
                color: ColorName.linkBlue,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
              ) : Theme
                  .of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(
                fontWeight: FontWeight.bold,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () async {
                  if (countdownFinished){
                    final user = getIt<AppBloc>().state.user;
                    final phoneNumber = user?.phone;
                    final result = await getIt<IParkingRepository>().requestOtp(phone: phoneNumber!,
                        confirmType: widget.parkingBloc.state.confirmType);

                    result.when(
                      success: (data) async {
                        showSuccessSnackBBar(
                            context: context,
                            message: 'Mã xác thực đã được gửi đến số điện thoại của bạn',
                            duration: const Duration(seconds: 3)
                        );
                        setState(() {
                          otpResubmittedTime = DateTime.now();
                          count = DateTime
                              .now()
                              .difference(otpResubmittedTime ?? widget.otpSubmittedTime )
                              .inSeconds;
                          countdownFinished = false;
                        });
                        initTimer();
                        widget.onResend.call();
                      },
                      failure: (failure) async {
                        await showErrorSnackBBar(context: context, message: failure.message!);
                      },
                    );
                  }
                }
          ),
        ],
      ),
    );
  }

  void initTimer(){
    timer = Timer.periodic(const Duration(seconds: 1), (_) {
      final diff = 3 * 60 - count;

      count++;

      if (diff > 0) {
        final minutes = (diff / 60).floor();
        final seconds = diff % 60;

        setState(() {
          countdownTime = '${minutes > 0 ? '${minutes}p' : ''}'
              '${seconds.toString().padLeft(2, '0')}s';
          countdownFinished = false;
        });
      } else {
        timer?.cancel();
        setState(() {
          countdownTime = '';
          countdownFinished = true;
        });
      }
    });
  }
}
