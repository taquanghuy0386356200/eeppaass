// import 'package:epass/commons/models/data_fake/data_payment.dart';
// import 'package:epass/commons/models/topup_fee/topup_fee.dart';
// import 'package:epass/gen/colors.gen.dart';
// import 'package:epass/pages/cash_in/pages/cash_in_fee/widget/item_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
//
// class CashInFeeCardBottomUpdate extends StatelessWidget {
//   final TopupFee? topupFee;
//   final String? otherFee;
//   final DataPayment? dataPayment;
//   const CashInFeeCardBottomUpdate(
//       {Key? key, this.topupFee, this.otherFee, this.dataPayment})
//       : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: [
//           Container(
//             decoration: const BoxDecoration(
//                 color: Colors.orange, //new Color.fromRGBO(255, 0, 0, 0.0),
//                 borderRadius: BorderRadius.all(Radius.circular(20.0)),
//                 gradient: LinearGradient(
//                   colors: [
//                     ColorName.primaryGradientStart,
//                     ColorName.primaryGradientEnd,
//                   ],
//                   begin: Alignment.centerLeft,
//                   end: Alignment.centerRight,
//                 )),
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   CashInFeeGroupIcon(
//                     title: dataPayment?.title,
//                     height: 28.r,
//                     width: 28.r,
//                   ),
//                   SizedBox(
//                     width: 4.w,
//                   ),
//                   Text(
//                     dataPayment?.title?.name ?? '',
//                     style: const TextStyle(fontWeight: FontWeight.w600),
//                   )
//                 ],
//               ),
//             ),
//           ),
//           Padding(
//             padding: EdgeInsets.fromLTRB(16.w, 24.h, 0.w, 0.h),
//             child: ListView.builder(
//                 physics: const NeverScrollableScrollPhysics(),
//                 shrinkWrap: true,
//                 itemCount: 1,
//                 itemBuilder: (BuildContext ctx, index) {
//                   return ItemWidget(
//                       content: dataPayment?.dataBank?[index].content,
//                       icon: dataPayment?.dataBank?[index].icon);
//                 }),
//           ),
//         ],
//       ),
//     );
//   }
// }
