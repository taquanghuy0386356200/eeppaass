part of 'invoice_setting_bloc.dart';

abstract class InvoiceSettingState extends Equatable {
  const InvoiceSettingState();
}

class InvoiceSettingInitial extends InvoiceSettingState {
  @override
  List<Object> get props => [];
}

class InvoiceSettingCycleUpdatedInProgress extends InvoiceSettingState {
  @override
  List<Object> get props => [];
}

class InvoiceSettingCycleUpdatedSuccess extends InvoiceSettingState {
  final String? message;

  const InvoiceSettingCycleUpdatedSuccess(this.message);

  @override
  List<Object?> get props => [message];
}

class InvoiceSettingCycleUpdatedFailure extends InvoiceSettingState {
  final String? message;

  const InvoiceSettingCycleUpdatedFailure(this.message);

  @override
  List<Object?> get props => [message];
}
