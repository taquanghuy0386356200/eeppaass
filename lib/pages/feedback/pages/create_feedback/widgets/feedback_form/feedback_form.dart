import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/widgets/support_center_widgets/attachment_file_item.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_modal.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/bloc/create_feedback_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/bloc/create_feedback_otp_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/create_feedback_otp_page.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_form/bloc/feedback_form_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_type_picker/feedback_type_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class FeedbackForm extends StatefulWidget {
  const FeedbackForm({Key? key}) : super(key: key);

  @override
  State<FeedbackForm> createState() => _FeedbackFormState();
}

class _FeedbackFormState extends State<FeedbackForm> {
  final _formKey = GlobalKey<FormState>();

  final _contentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: MultiBlocListener(
        listeners: [
          BlocListener<FilePickerBloc, FilePickerState>(
            listener: (context, state) async {
              if (state.isLoading) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
              }

              context.read<FeedbackFormBloc>().add(FeedbackAttachmentFilesChanged(state.filePaths));

              if (state.error != null) {
                await showErrorSnackBBar(context: context, message: state.error!);
              }
            },
          ),
          BlocListener<CreateFeedbackOtpBloc, CreateFeedbackOtpState>(
            listener: (context, state) async {
              if (state is CreateFeedbackOtpRequestedFailure) {
                await showErrorSnackBBar(context: context, message: state.message);
              } else if (state is CreateFeedbackOtpRequestedSuccess) {
                await _showOTPDialog(context);
              }
            },
          ),
        ],
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.w),
          child: SingleChildScrollView(
            primary: false,
            physics: const NeverScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24.h),
                BlocSelector<FeedbackFormBloc, FeedbackFormState, FeedbackType?>(
                  selector: (state) => state.feedbackType,
                  builder: (context, feedbackType) {
                    return FeedbackTypePicker(
                      selectedFeedbackType: feedbackType,
                      onItemChanged: (item) => context.read<FeedbackFormBloc>().add(FeedbackTypeChanged(item?.value)),
                    );
                  },
                ),
                SizedBox(height: 16.h),
                BlocSelector<FeedbackFormBloc, FeedbackFormState, FeedbackType?>(
                  selector: (state) => state.feedbackType,
                  builder: (context, feedbackType) {
                    _contentController.text = feedbackType?.ticketTemplate ?? "";
                    context.read<FeedbackFormBloc>().add(ReceiveContentChanged(feedbackType?.ticketTemplate ?? ""));
                    return PrimaryTextField(
                      controller: _contentController,
                      labelText: 'Ý kiến đóng góp',
                      isMandatory: true,
                      hintText: 'Nhập ý kiến đóng góp',
                      maxLines: 5,
                      maxLength: 320,
                      onChanged: (value) => context.read<FeedbackFormBloc>().add(ReceiveContentChanged(value)),
                      validator: (value) {
                        if (value?.isEmpty ?? true) {
                          return 'Vui lòng nhập Ý kiến đóng góp';
                        }
                        return null;
                      },
                    );
                  },
                ),
                SizedBox(height: 16.h),
                BlocSelector<FeedbackFormBloc, FeedbackFormState, List<String>>(
                  selector: (state) => state.attachments,
                  builder: (context, filePaths) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (filePaths.isNotEmpty)
                          SizedBox(
                            height: 90.r,
                            child: ListView.separated(
                              primary: false,
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: filePaths.length,
                              itemBuilder: (context, index) {
                                final path = filePaths[index];
                                return AttachmentFileItem(path: path);
                              },
                              separatorBuilder: (context, index) => SizedBox(width: 8.w),
                            ),
                          ),
                        if (filePaths.isNotEmpty) SizedBox(height: 20.h),
                        Padding(
                          padding: EdgeInsets.only(left: 4.w),
                          child: Text(
                            'Đính kèm tệp (${filePaths.length}/4)',
                            style: Theme.of(context).textTheme.bodyText1!.copyWith(fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 12.h),
                        Row(
                          children: [
                            SizedBox(
                              height: 48.h,
                              child: SecondaryButton(
                                enabled: filePaths.length < 4,
                                title: 'Chọn tệp đính kèm',
                                icon: Assets.icons.upload.svg(),
                                onTap: filePaths.length < 4
                                    ? () async {
                                        await showFilePickerModal(context);
                                      }
                                    : null,
                              ),
                            ),
                            const Spacer(),
                          ],
                        ),
                      ],
                    );
                  },
                ),
                SizedBox(height: 20.h),
                SizedBox(
                  height: 56.h,
                  width: double.infinity,
                  child: BlocBuilder<CreateFeedbackOtpBloc, CreateFeedbackOtpState>(
                    builder: (context, state) {
                      return LoadingPrimaryButton(
                        title: 'Gửi góp ý',
                        enabled: state is! CreateFeedbackOtpRequestedInProgress,
                        isLoading: state is CreateFeedbackOtpRequestedInProgress,
                        onTap: () async {
                          FocusScope.of(context).unfocus();
                          if (_formKey.currentState!.validate()) {
                            context.read<CreateFeedbackOtpBloc>().add(const CreateFeedbackOtpRequested());
                          }
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _showOTPDialog(BuildContext context) async {
    await showGeneralDialog(
      context: context,
      pageBuilder: (dialogContext, animation, secondaryAnimation) {
        return MultiBlocProvider(
          providers: [
            BlocProvider.value(value: context.read<CreateFeedbackOtpBloc>()),
            BlocProvider.value(value: context.read<FeedbackFormBloc>()),
            BlocProvider.value(value: context.read<FilePickerBloc>()),
            BlocProvider(
              create: (context) => CreateFeedbackBloc(
                feedbackRepository: getIt<IFeedbackRepository>(),
                appBloc: getIt<AppBloc>(),
              ),
            ),
          ],
          child: const CreateFeedbackOTPPage(),
        );
      },
    );
  }
}
