part of 'policy_bloc.dart';

abstract class PolicyEvent extends Equatable {
  const PolicyEvent();
}

class PolicyChecked extends PolicyEvent {
  final bool? isPolicyChecked;

  const PolicyChecked({required this.isPolicyChecked});

  @override
  List<Object?> get props => [isPolicyChecked];
}