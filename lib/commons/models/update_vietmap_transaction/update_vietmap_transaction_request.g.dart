// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_vietmap_transaction_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateVietMapTransactionRequest _$UpdateVietMapTransactionRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateVietMapTransactionRequest(
      saleOrderCode: json['saleOrderCode'] as String?,
    );

Map<String, dynamic> _$UpdateVietMapTransactionRequestToJson(
        UpdateVietMapTransactionRequest instance) =>
    <String, dynamic>{
      'saleOrderCode': instance.saleOrderCode,
    };
