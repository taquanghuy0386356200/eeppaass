import 'dart:io';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewWithoutHeader extends StatefulWidget {
  const WebViewWithoutHeader({Key? key, required this.initUrl})
      : super(key: key);
  final String initUrl;

  @override
  // ignore: no_logic_in_create_state
  State<WebViewWithoutHeader> createState() =>
      // ignore: no_logic_in_create_state
      _WebViewWithoutHeaderState(initUrl);
}

class _WebViewWithoutHeaderState extends State<WebViewWithoutHeader> {
  int _progress = 0;
  WebViewController? _controller;
  late String initUrl = '';
  _WebViewWithoutHeaderState(this.initUrl);

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = AndroidWebView();
    } else {
      WebView.platform = CupertinoWebView();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BasePage(
      child: Stack(
        children: [
          const FadeAnimation(
            delay: 0.5,
            child: GradientHeaderContainer(),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _progress != 100
                  ? LinearProgressIndicator(
                      value: _progress.toDouble() / 100.0,
                      backgroundColor: ColorName.blue.withOpacity(0.3),
                      valueColor: const AlwaysStoppedAnimation(ColorName.blue),
                      minHeight: 2.h,
                    )
                  : SizedBox(height: 2.h),
              Flexible(
                fit: FlexFit.tight,
                child: WebView(
                  // ignore: prefer_collection_literals
                  gestureRecognizers: Set()
                    ..add(
                      Factory<VerticalDragGestureRecognizer>(
                            () => VerticalDragGestureRecognizer(),
                      ), // or null
                    ),
                  initialUrl: initUrl,
                  javascriptMode: JavascriptMode.unrestricted,
                  gestureNavigationEnabled: true,
                  onWebViewCreated: (WebViewController webViewController) {
                    _controller = webViewController;
                  },
                  onProgress: (progress) {
                    setState(() {
                      _progress = progress;
                    });
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
