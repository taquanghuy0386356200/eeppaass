import 'dart:async';

import 'package:epass/commons/constant.dart';
import 'package:epass/commons/services/local_auth/local_auth_service.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:car_doctor_sdk_tracking/car_doctor_sdk_tracking.dart';

part 'setting_event.dart';

part 'setting_state.dart';

class SettingBloc extends HydratedBloc<SettingEvent, SettingState> {
  final ILocalAuthService _localAuthService;
  final FlutterSecureStorage _secureStorage;
  final AppBloc _appBloc;

  SettingBloc({
    required ILocalAuthService localAuthService,
    required FlutterSecureStorage secureStorage,
    required AppBloc appBloc,
  })  : _appBloc = appBloc,
        _secureStorage = secureStorage,
        _localAuthService = localAuthService,
        super(const SettingState()) {
    on<FirstRun>(_onFirstRun);
    on<BiometricLoginToggled>(_onBiometricLoginToggled);
    on<CashInFirstRun>(_onCashInFirstRun);
    on<CashInBankTransferFirstRun>(_onCashInBankTransferFirstRun);
    on<BeneficiaryFirstRun>(_onBeneficiaryFirstRun);
  }

  @override
  SettingState? fromJson(Map<String, dynamic> json) {
    return SettingState.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(SettingState state) {
    return state.toJson();
  }

  FutureOr<void> _onFirstRun(
    FirstRun event,
    Emitter<SettingState> emit,
  ) {
    emit(state.copyWith(isFirstRun: false));
  }

  FutureOr<void> _onBiometricLoginToggled(
      BiometricLoginToggled event, Emitter<SettingState> emit) async {
    final contractNo = _appBloc.state.user?.contractNo;

    if (contractNo == null || contractNo.isEmpty) {
      emit(BiometricSettingAuthenticateFailure(
        currentState: state,
        message: 'Thông tin đăng nhập không hợp lệ (contractNo - client)',
      ));
      return;
    }

    final encryptedPasswordStorageKey =
        Constant.encryptedPasswordKey(contractNo);

    if (!event.value) {
      // Remove encrypted password from secure storage
      await _secureStorage.delete(key: Constant.encryptedUsernameKey);
      await _secureStorage.delete(key: encryptedPasswordStorageKey);

      emit(state.copyWith(isBiometricLoginEnabled: event.value));
    } else {
      final result = await _localAuthService.authenticate();

      await result.when(
        success: (_) async {
          final encryptedPassword = _appBloc.state.encryptedPassword;
          AppTracking.instance.trackLogin(contractNo);
          await _secureStorage.write(
            key: Constant.encryptedUsernameKey,
            value: contractNo,
          );
          await _secureStorage.write(
            key: encryptedPasswordStorageKey,
            value: encryptedPassword,
          );

          emit(BiometricSettingAuthenticateSuccess(currentState: state));
        },
        failure: (failure) {
          emit(BiometricSettingAuthenticateFailure(
            currentState: state,
            message: failure.message ?? 'Có lỗi xảy ra',
          ));
        },
      );
    }
  }

  FutureOr<void> _onCashInFirstRun(
    CashInFirstRun event,
    Emitter<SettingState> emit,
  ) {
    emit(state.copyWith(isCashInFirstRun: false));
  }

  FutureOr<void> _onCashInBankTransferFirstRun(
      CashInBankTransferFirstRun event, Emitter<SettingState> emit) {
    emit(state.copyWith(isCashInBankTransferFirstRun: false));
  }

  FutureOr<void> _onBeneficiaryFirstRun(
    BeneficiaryFirstRun event,
    Emitter<SettingState> emit,
  ) {
    emit(state.copyWith(isBeneficiaryFirstRun: false));
  }
}
