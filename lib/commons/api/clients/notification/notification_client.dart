import 'package:dio/dio.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/commons/models/firebase_cloud_messaging/register_firebase_token_request.dart';
import 'package:epass/commons/models/no_data/no_data.dart';
import 'package:epass/commons/models/notification/notification_msg.dart';
import 'package:retrofit/http.dart';

import 'package:epass/commons/api/endpoint.dart' as endpoint;

part 'notification_client.g.dart';

@RestApi()
abstract class NotificationClient {
  factory NotificationClient(
    Dio dio, {
    String baseUrl,
  }) = _NotificationClient;

  /// /api/v1/notification-msg
  @GET(endpoint.notifications)
  Future<NotificationMsgResponse> getNotifications({
    @Query('pagesize') int? pageSize,
    @Query('startrecord') int? startRecord,
    @Query('message') String? searchInput,
    @Query('firebaseToken') String? firebaseToken,
    @Query('notificationType') int? notificationType,
  });

  /// /api/v1/notification-msg/{notificationId}
  @PUT(endpoint.updateNotification)
  Future<NoData> updateNotificationViewStatus({
    @Path('notificationId') required String notificationId,
    @Body(nullToAbsent: true) required UpdateNotificationViewStatusRequest requestBody,
  });

  /// /api/v1/token-firebases
  @POST(endpoint.tokenFirebase)
  Future<NoData> tokenFirebase({
    @Body() required RegisterFirebaseTokenRequest requestBody,
  });
}
