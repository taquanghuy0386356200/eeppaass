import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';

class NotEligibleDialog extends StatelessWidget {
  final VoidCallback onLinkViettelPayTap;

  const NotEligibleDialog({
    Key? key,
    required this.onLinkViettelPayTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16.w,
            vertical: 24.h,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Assets.icons.notEnoughMoney.image(
                width: 80.w,
                height: 80.h
              ),
              SizedBox(height: 20.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: [
                    TextSpan(
                      text:
                          'Quý khách không đủ điều kiện để đăng ký dịch vụ trừ '
                          'phí đỗ xe tự động do chưa liên kết nguồn tiền thanh toán '
                          '\n\n Để thực hiện vui lòng vào mục :\n '
                          'Trang chủ/Liên kết Viettel Money.\n',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          fontSize: 16.sp,
                          color: ColorName.textGray2,
                          height: 1.5),
                    ),
                    TextSpan(
                      text: 'Liên kết ngay',
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          fontWeight: FontWeight.w700,
                          fontSize: 16.sp,
                          color: ColorName.blue,
                          height: 1.5),
                      recognizer: TapGestureRecognizer()
                        ..onTap = onLinkViettelPayTap,
                    ),
                  ]),
                ),
              ),
              SizedBox(height: 16.h),
              SizedBox(
                height: 50.h,
                width: 150.w,
                child: SecondaryButton(
                  onTap: () => Navigator.of(context).pop(),
                  title: 'Đóng',
                ),
              ),
              // SizedBox(height: 8.h),
            ],
          ),
        ),
      ),
    );
  }
}
