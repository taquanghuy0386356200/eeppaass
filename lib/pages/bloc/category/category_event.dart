part of 'category_bloc.dart';

abstract class CategoryEvent extends Equatable {
  const CategoryEvent();
}

class PlateTypeFetched extends CategoryEvent {
  const PlateTypeFetched();

  @override
  List<Object> get props => [];
}

class VehicleTypeFetched extends CategoryEvent {
  const VehicleTypeFetched();

  @override
  List<Object> get props => [];
}

class CustomerTypeFetched extends CategoryEvent {
  const CustomerTypeFetched();

  @override
  List<Object> get props => [];
}