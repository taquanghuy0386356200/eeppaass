// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle_groups.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleGroupTotalResponse _$VehicleGroupTotalResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleGroupTotalResponse(
      data: json['data'] == null
          ? null
          : ListVehicleGroupResponse.fromJson(
              json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleGroupTotalResponseToJson(
        VehicleGroupTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ListVehicleGroupResponse _$ListVehicleGroupResponseFromJson(
        Map<String, dynamic> json) =>
    ListVehicleGroupResponse(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => VehicleGroupResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$ListVehicleGroupResponseToJson(
        ListVehicleGroupResponse instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

VehicleGroupResponse _$VehicleGroupResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleGroupResponse(
      id: json['id'] as int?,
      code: json['code'] as String?,
      name: json['name'] as String?,
      description: json['description'] as String?,
      is_active: json['is_active'] as String?,
    );

Map<String, dynamic> _$VehicleGroupResponseToJson(
        VehicleGroupResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'name': instance.name,
      'description': instance.description,
      'is_active': instance.is_active,
    };
