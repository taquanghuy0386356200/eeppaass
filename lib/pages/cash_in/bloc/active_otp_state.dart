part of 'active_otp_bloc.dart';

abstract class ActiveOtpState extends Equatable {
  const ActiveOtpState();
}

class ActiveOtpInitial extends ActiveOtpState {
  @override
  List<Object> get props => [];
}

class ActiveOtpConfirmedInProgress extends ActiveOtpState {
  @override
  List<Object> get props => [];
}

class ActiveOtpConfirmedSuccess extends ActiveOtpState {
  final DateTime submittedTime;

  const ActiveOtpConfirmedSuccess(
    this.submittedTime,
  );

  @override
  List<Object> get props => [submittedTime];
}

class ActiveOtpConfirmedFailure extends ActiveOtpState {
  final String message;

  const ActiveOtpConfirmedFailure({required this.message});

  @override
  List<Object> get props => [message];
}

class ActiveOtpPaused extends ActiveOtpState {
  final DateTime submittedTime;

  const ActiveOtpPaused(this.submittedTime);

  @override
  List<Object> get props => [submittedTime];
}
