import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/insurance_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/insurance/bloc/insurance_gc_bloc.dart';
import 'package:epass/pages/insurance/bloc/insurance_viettelpay_bloc.dart';
import 'package:epass/pages/utility/widgets/utility_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class InsurancePage extends StatefulWidget implements AutoRouteWrapper {
  const InsurancePage({Key? key}) : super(key: key);

  @override
  State<InsurancePage> createState() => _InsurancePageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => InsuranceViettelPayBloc(
            insuranceRepository: getIt<IInsuranceRepository>(),
          ),
        ),
        BlocProvider(
          create: (context) => InsuranceGcBloc(
            insuranceRepository: getIt<IInsuranceRepository>(),
          ),
        )
      ],
      child: this,
    );
  }
}

class _InsurancePageState extends State<InsurancePage>
    with AutomaticKeepAliveClientMixin {
  bool _firstRun = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final utilityButtons = [
      // UtilityButton(
      //   icon: Assets.icons.insuranceVds.svg(height: 36.r),
      //   title: 'Bảo hiểm VDS',
      //   onTap: () {
      //     context.read<InsuranceViettelPayBloc>().add(const CountInsuranceFetched());
      //   },
      // ),
      UtilityButton(
        icon: Assets.icons.insuranceAba.svg(height: 36.r),
        title: 'Bảo hiểm\n An Bình',
        onTap: () {
          context.pushRoute(const InsuranceAnBinhRoute());
        },
      ),
      UtilityButton(
        icon: Assets.icons.insuranceGc.svg(height: 36.r),
        title: 'Bảo hiểm\n GlobalCare',
        onTap: () {
          context.read<InsuranceGcBloc>().add(const ContractEncryptedFetched());
        },
      ),
    ];

    return MultiBlocListener(
      listeners: [
        BlocListener<InsuranceViettelPayBloc, InsuranceViettelPayState>(
          listener: _insuranceVDSBlocListener,
        ),
        BlocListener<InsuranceGcBloc, InsuranceGcState>(
          listener: _insuranceGcBlocListener,
        ),
      ],
      child: BasePage(
        title: 'Mua bảo hiểm',
        leading: BackButton(color: Colors.white, onPressed: context.popRoute),
        backgroundColor: ColorName.backgroundAvatar,
        child: Stack(
          children: [
            FadeAnimation(
              delay: 0.5,
              playAnimation: _firstRun,
              child: const GradientHeaderContainer(),
            ),
            SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 10.w,
                  vertical: 16.h,
                ),
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: utilityButtons.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 0.w,
                    mainAxisSpacing: 0.h,
                    childAspectRatio: 0.85,
                  ),
                  itemBuilder: (context, index) {
                    return FadeAnimation(
                      delay: 1 + (index / (utilityButtons.length * 2)),
                      playAnimation: _firstRun,
                      child: utilityButtons[index],
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _insuranceVDSBlocListener(
      BuildContext context, InsuranceViettelPayState state) async {
    final tabsBloc = context.read<HomeTabsBloc>();

    if (state is CountInsuranceFetchedInProgress) {
      context.loaderOverlay.show();
    } else if (state is CountInsuranceFetchedFailure) {
      context.loaderOverlay.hide();
      showErrorSnackBBar(context: context, message: state.message);
    } else if (state is CountInsuranceFetchedSuccess) {
      context.loaderOverlay.hide();
      if (state is InsuranceServiceAccepted) {
        await context.pushRoute(const InsuranceViettelPayRoute());
        tabsBloc.add(const HomeTabBarRequestHidden(isHidden: false));
      } else if (state is InsuranceServiceAccessRequested) {
        showDialog(
          context: context,
          builder: (dialogContext) {
            return ConfirmDialog(
              title: 'Xác nhận cung cấp thông tin',
              content: 'Để thực hiện tìm kiếm và mua bảo hiểm, quý'
                  ' khách vui lòng cho phép hệ thống của Tổng Công'
                  ' ty Dịch vụ số Viettel truy vấn thông tin số hợp'
                  ' đồng và số điện thoại khi chuyển hướng tới'
                  ' baohiem.viettelpay.vn',
              onPrimaryButtonTap: () async {
                Navigator.of(dialogContext).pop();
                await context.pushRoute(const InsuranceRoute());
                tabsBloc.add(const HomeTabBarRequestHidden(isHidden: false));
              },
            );
          },
        );
      }
    }
  }

  Future<void> _insuranceGcBlocListener(
      BuildContext context, InsuranceGcState state) async {
    final tabsBloc = context.read<HomeTabsBloc>();

    if (state is ContractEncryptedFetchedInProgress) {
      context.loaderOverlay.show();
    } else {
      context.loaderOverlay.hide();
      if (state is ContractEncryptedFetchedSuccess) {
        final contractNoEncrypted = state.contractNoEncrypted;
        tabsBloc.add(const HomeTabBarRequestHidden(isHidden: true));
        await context.pushRoute(
            InsuranceGCRoute(contractNoEncrypted: contractNoEncrypted));
        tabsBloc.add(const HomeTabBarRequestHidden(isHidden: false));
      } else if (state is ContractEncryptedFetchedFailure) {
        showErrorSnackBBar(context: context, message: state.message);
      }
    }
  }
}
