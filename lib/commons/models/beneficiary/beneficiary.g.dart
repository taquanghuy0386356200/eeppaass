// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beneficiary.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Beneficiary _$BeneficiaryFromJson(Map<String, dynamic> json) => Beneficiary(
      beneficiaryId: json['beneficiaryId'] as int?,
      contractNo: json['contractNo'] as String?,
      customerName: json['customerName'] as String?,
      contractId: json['contractId'] as int?,
      reminiscentName: json['reminiscentName'] as String?,
      status: $enumDecodeNullable(_$BeneficiaryStatusEnumMap, json['status'],
          unknownValue: BeneficiaryStatus.unknown),
    );

Map<String, dynamic> _$BeneficiaryToJson(Beneficiary instance) =>
    <String, dynamic>{
      'beneficiaryId': instance.beneficiaryId,
      'contractNo': instance.contractNo,
      'customerName': instance.customerName,
      'reminiscentName': instance.reminiscentName,
      'contractId': instance.contractId,
      'status': _$BeneficiaryStatusEnumMap[instance.status],
    };

const _$BeneficiaryStatusEnumMap = {
  BeneficiaryStatus.inactive: '0',
  BeneficiaryStatus.active: '1',
  BeneficiaryStatus.unknown: 'unknown',
};

BeneficiaryListData _$BeneficiaryListDataFromJson(Map<String, dynamic> json) =>
    BeneficiaryListData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => Beneficiary.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$BeneficiaryListDataToJson(
        BeneficiaryListData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

BeneficiaryListDataResponse _$BeneficiaryListDataResponseFromJson(
        Map<String, dynamic> json) =>
    BeneficiaryListDataResponse(
      data: json['data'] == null
          ? null
          : BeneficiaryListData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BeneficiaryListDataResponseToJson(
        BeneficiaryListDataResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
