import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/models/app_version/app_version.dart';
import 'package:epass/commons/repo/app_version_repository.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/utils/number_extension/number_extension.dart';
import 'package:equatable/equatable.dart';

import 'dart:developer' as developer;
import 'dart:io' show Platform;

import 'package:package_info_plus/package_info_plus.dart';

part 'app_version_event.dart';

part 'app_version_state.dart';

class AppVersionBloc extends Bloc<AppVersionEvent, AppVersionState> {
  final IAppVersionRepository _appVersionRepository;
  final ICategoryRepository _categoryRepository;

  AppVersionBloc(
      {required IAppVersionRepository appVersionRepository,
      required ICategoryRepository categoryRepository})
      : _appVersionRepository = appVersionRepository,
        _categoryRepository = categoryRepository,
        super(AppVersionInitial()) {
    on<AppVersionFetched>(_onAppVersionFetched);
    on<RegistFlagFetched>(_onRegistFlagFetched);
  }

  FutureOr<void> _onRegistFlagFetched(
    RegistFlagFetched event,
    Emitter<AppVersionState> emit,
  ) async {
    final result = await _categoryRepository.getCategoriesConfig();
    var listConfig = result.success?.listData ?? [];
    var isOnRegist = "";
    var registerFee = "";

    for (var element in listConfig) {
      print(element.name);
      switch (element.code) {
        case "CUST_REGIS_LOCK_UNLOCK":
          isOnRegist = element.name ?? "";
          break;
        case "REGISTER_FEE":
          registerFee = element.name ?? "";
          break;
        default:
          break;
      }
    }
    result.when(
      success: (success) => emit(RegistFlagFetchedSuccess(
          isOnRegistration: isOnRegist, registerFee: registerFee)),
      failure: (failure) =>
          emit(AppVersionFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  bool isAppHaveNewVersion = false;
  String versionApp = '';

  FutureOr<void> _onAppVersionFetched(
    AppVersionFetched event,
    Emitter<AppVersionState> emit,
  ) async {
    emit(const AppVersionFetchedInProgress());

    String resultPlatform =
        Platform.isAndroid ? Constant.ANDROID : Constant.IOS;

    final currentVersion = (await PackageInfo.fromPlatform()).version;

    final result =
        await _appVersionRepository.getAppVersion(deviceType: resultPlatform);

    result.when(
      success: (success) {
        versionApp = currentVersion;

        if ((success.versionName ?? '').isNotEmpty) {
          if (currentVersion.getExtendedVersionNumber() <
              (success.versionName ?? '').getExtendedVersionNumber()) {
            isAppHaveNewVersion = true;
          } else {
            isAppHaveNewVersion = false;
          }
        } else {
          isAppHaveNewVersion = false;
        }

        emit(
          AppVersionFetchedSuccess(
            success,
          ),
        );
      },
      failure: (failure) =>
          emit(AppVersionFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
