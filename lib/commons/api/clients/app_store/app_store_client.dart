import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:epass/commons/api/endpoint.dart' as endpoint;

import '../../../../pages/home/model/apps_model.dart';

part 'app_store_client.g.dart';

@RestApi()
abstract class AppStoreClient{
  factory AppStoreClient(
      Dio dio, {
        String baseUrl,
      }) = _AppStoreClient;

  @GET(endpoint.appStore)
  Future<AppsModel> getAppsStore();
}