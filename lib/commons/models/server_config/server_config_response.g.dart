// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server_config_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServerConfig _$ServerConfigFromJson(Map<String, dynamic> json) => ServerConfig(
      name: json['name'] as String?,
      table_name: json['table_name'] as String?,
      code: json['code'] as String?,
      id: json['id'] as String?,
      description: json['description'] as String?,
      is_default: json['is_default'] as String?,
      is_active: json['is_active'] as String?,
    );

Map<String, dynamic> _$ServerConfigToJson(ServerConfig instance) =>
    <String, dynamic>{
      'name': instance.name,
      'table_name': instance.table_name,
      'code': instance.code,
      'id': instance.id,
      'description': instance.description,
      'is_default': instance.is_default,
      'is_active': instance.is_active,
    };

ServerConfigData _$ServerConfigDataFromJson(Map<String, dynamic> json) =>
    ServerConfigData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => ServerConfig.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ServerConfigDataToJson(ServerConfigData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
    };

ServerConfigResponse _$ServerConfigResponseFromJson(
        Map<String, dynamic> json) =>
    ServerConfigResponse(
      data: json['data'] == null
          ? null
          : ServerConfigData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ServerConfigResponseToJson(
        ServerConfigResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
