import 'package:json_annotation/json_annotation.dart';

part 'confirm_order_request.g.dart';

@JsonSerializable()
class ConfirmOrderItem {
  String? ticketId;
  String? verifyAction;

  ConfirmOrderItem({
    required this.ticketId,
    required this.verifyAction
  });

  Map<String, dynamic> toJson() => _$ConfirmOrderItemToJson(this);

  factory ConfirmOrderItem.fromJson(Map<String, dynamic> json)  => _$ConfirmOrderItemFromJson(json);
}

@JsonSerializable()
class ConfirmOrderRequest{
  List<ConfirmOrderItem>? lstVerify;

  ConfirmOrderRequest({
    required this.lstVerify
  });

  Map<String, dynamic> toJson() => _$ConfirmOrderRequestToJson(this);

  factory ConfirmOrderRequest.fromJson(Map<String, dynamic> json)  => _$ConfirmOrderRequestFromJson(json);
}