import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/pages/register_confirm/widgets/register_confirm_customer_info/confirm_customer_info.dart';
import 'package:epass/pages/register/pages/register_confirm/widgets/register_confirm_vehicle_info/confirm_vehicle_info.dart';
import 'package:epass/pages/register/widgets/base_register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterConfirmPage extends StatefulWidget {
  const RegisterConfirmPage({Key? key}) : super(key: key);

  @override
  State<RegisterConfirmPage> createState() => _RegisterConfirmPageState();
}

class _RegisterConfirmPageState extends State<RegisterConfirmPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BaseRegisterPage(
      title: 'Kiểm tra thông tin',
      step: 4,
      resizeToAvoidBottomInset: true,
      child: BlocBuilder<RegisterVehiclesBloc, RegisterVehiclesState>(
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Bạn vui lòng kiểm tra lại tất cả thông tin',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                SizedBox(height: 8.h),
                const ConfirmCustomerInfo(),

                SizedBox(height: 8.h),

                ConfirmVehicleInfo(),

                SizedBox(height: 32.h),
                SizedBox(
                  height: 56.h,
                  width: double.infinity,
                  child: PrimaryButton(
                    title: 'Tiếp theo',
                    onTap: () {
                      if (_formKey.currentState!.validate()) {
                        context.pushRoute(const RegisterServiceRoute());
                      }
                    },
                  ),
                ),
                SizedBox(height: 16.h),
              ],
            ),
          );
        },
      ),
    );
  }
}
