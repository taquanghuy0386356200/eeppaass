import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:epass/gen/colors.gen.dart';

class BasePaginationPage extends StatefulWidget {
  const BasePaginationPage({Key? key}) : super(key: key);

  @override
  State<BasePaginationPage> createState() => _BasePaginationPageState();
}

class _BasePaginationPageState extends State<BasePaginationPage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      physics: const BouncingScrollPhysics(),
      enablePullDown: true,
      enablePullUp: true,
      onRefresh: () {},
      onLoading: () {},
      controller: _refreshController,
      child: ListView.separated(
        physics: const BouncingScrollPhysics(),
        // Example value
        itemCount: 0,
        itemBuilder: (context, index) {
          return Text(index.toString());
        },
        separatorBuilder: (context, index) => Column(
          children: [
            SizedBox(height: 16.h),
            Divider(
              color: ColorName.disabledBorderColor,
              height: 0.5,
              thickness: 0.5,
              indent: 32.w,
              endIndent: 16.w,
            ),
            SizedBox(height: 16.h),
          ],
        ),
      ),
    );
  }
}
