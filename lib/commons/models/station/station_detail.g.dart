// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'station_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StationDetail _$StationDetailFromJson(Map<String, dynamic> json) =>
    StationDetail(
      code: json['code'] as String?,
      name: json['name'] as String?,
      id: json['id'] as int?,
      nameShort: json['nameShort'] as String?,
      stationType: json['stationType'] as String?,
      isActive: json['isActive'] as String?,
      addressDetail: json['addressDetail'] as String?,
      methodChargeId: json['methodChargeId'] as String?,
      longitude: (json['longitude'] as num?)?.toDouble(),
      latitude: (json['latitude'] as num?)?.toDouble(),
      booCode: json['booCode'] as String?,
      isEtc: json['isEtc'] as int?,
      priceImagePath: json['priceImagePath'] as String?,
      priceLinkWeb: json['priceLinkWeb'] as String?,
      warLeftLat1: (json['warLeftLat1'] as num?)?.toDouble(),
      warLeftLong1: (json['warLeftLong1'] as num?)?.toDouble(),
      warLeftLat2: (json['warLeftLat2'] as num?)?.toDouble(),
      warLeftLong2: (json['warLeftLong2'] as num?)?.toDouble(),
      warRightLat1: (json['warRightLat1'] as num?)?.toDouble(),
      warRightLong1: (json['warRightLong1'] as num?)?.toDouble(),
      warRightLat2: (json['warRightLat2'] as num?)?.toDouble(),
      warRightLong2: (json['warRightLong2'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$StationDetailToJson(StationDetail instance) =>
    <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'id': instance.id,
      'nameShort': instance.nameShort,
      'stationType': instance.stationType,
      'isActive': instance.isActive,
      'addressDetail': instance.addressDetail,
      'methodChargeId': instance.methodChargeId,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'booCode': instance.booCode,
      'isEtc': instance.isEtc,
      'priceImagePath': instance.priceImagePath,
      'priceLinkWeb': instance.priceLinkWeb,
      'warLeftLat1': instance.warLeftLat1,
      'warLeftLong1': instance.warLeftLong1,
      'warLeftLat2': instance.warLeftLat2,
      'warLeftLong2': instance.warLeftLong2,
      'warRightLat1': instance.warRightLat1,
      'warRightLong1': instance.warRightLong1,
      'warRightLat2': instance.warRightLat2,
      'warRightLong2': instance.warRightLong2,
    };

StationDetailData _$StationDetailDataFromJson(Map<String, dynamic> json) =>
    StationDetailData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => StationDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$StationDetailDataToJson(StationDetailData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

StationDetailResponse _$StationDetailResponseFromJson(
        Map<String, dynamic> json) =>
    StationDetailResponse(
      data: json['data'] == null
          ? null
          : StationDetailData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$StationDetailResponseToJson(
        StationDetailResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
