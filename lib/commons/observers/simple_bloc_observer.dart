import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loggy/loggy.dart';

class SimpleBlocObserver extends BlocObserver {
  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    String changeStr = change.toString();
    if (change.toString().length > 500) {
      changeStr = '${change.toString().substring(0, 500)}...';
    }
    // logInfo('${bloc.runtimeType} $changeStr');
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    // logInfo('${bloc.runtimeType} $error $stackTrace');
    super.onError(bloc, error, stackTrace);
  }
}
