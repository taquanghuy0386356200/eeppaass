import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

/// fee : 2200
/// value : 12200

part 'topup_channel_fee.g.dart';

@JsonSerializable()
class TopupChannelFee extends Equatable {
  const TopupChannelFee({
    this.fee,
    this.value,
  });

  final int? fee;
  final int? value;

  @override
  List<Object?> get props => [fee, value];

  factory TopupChannelFee.fromJson(Map<String, dynamic> json) =>
      _$TopupChannelFeeFromJson(json);
}

@JsonSerializable()
class TopupChannelFeeResponse {
  final TopupChannelFee? data;
  final MessageData? mess;

  TopupChannelFeeResponse({
    this.data,
    this.mess,
  });

  factory TopupChannelFeeResponse.fromJson(Map<String, dynamic> json) =>
      _$TopupChannelFeeResponseFromJson(json);
}
