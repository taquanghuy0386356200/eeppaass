

import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'condition_service_response.g.dart';

@JsonSerializable()
class ConditionServiceModel {
  int? vehicleId;
  int contractId;
  int custId;
  String customerName;
  String contactNumber;
  String contractNo;
  int? plateTypeId;
  String? epc;
  bool isTraffic;
  String? moneyCode;
  bool? isFirstTime;
  String? description;
  int? etagged;

  ConditionServiceModel({
    this.vehicleId,
    required this.contractId,
    required this.custId,
    required this.customerName,
    required this.contactNumber,
    required this.contractNo,
    this.plateTypeId,
    this.epc,
    required this.isTraffic,
    this.moneyCode,
    this.isFirstTime,
    this.description,
    this.etagged,
  });

  factory ConditionServiceModel.fromJson(Map<String, dynamic> json) => _$ConditionServiceModelFromJson(json);
  Map<String, dynamic> toJson() => _$ConditionServiceModelToJson(this);

}

@JsonSerializable()
class ConditionServiceResponse {
  ConditionServiceResponse({
    this.data,
    this.mess,
  });

  final ConditionServiceModel? data;
  final MessageData? mess;

  factory ConditionServiceResponse.fromJson(Map<String, dynamic> json) => _$ConditionServiceResponseFromJson(json);
}