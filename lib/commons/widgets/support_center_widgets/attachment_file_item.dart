import 'dart:io';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:flutter/material.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path/path.dart' as p;

class AttachmentFileItem extends StatelessWidget {
  const AttachmentFileItem({
    Key? key,
    required this.path,
  }) : super(key: key);

  final String path;

  @override
  Widget build(BuildContext context) {
    var fileName = p.basenameWithoutExtension(path);
    final extension = p.extension(path);
    if (fileName.length > 10) {
      fileName = '${fileName.substring(0, 20)}....$extension';
    }

    bool isImage;

    switch (extension) {
      case '.jpg':
      case '.jpeg':
      case '.png':
      case '.heic':
        isImage = true;
        break;
      default:
        isImage = false;
        break;
    }
    
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 10.r, 10.r, 0),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          isImage
              ? Container(
            width: 80.r,
            height: 80.r,
            decoration: BoxDecoration(
              border: Border.all(
                color: ColorName.disabledBorderColor,
              ),
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Image.file(
                File(path),
                fit: BoxFit.cover,
              ),
            ),
          )
        : Container(
            width: 80.r,
            height: 80.r,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              gradient: const LinearGradient(
                colors: [
                  Color(0xFFFFE0E0),
                  Color(0xFFFFF0E1),
                ],
                begin: AlignmentDirectional.topCenter,
                end: AlignmentDirectional.bottomCenter,
              ),
            ),
            child: Center(
              child: Text(
                extension.substring(1, extension.length).toUpperCase(),
                style: Theme.of(context).textTheme.headline6!.copyWith(
                  color: ColorName.textGray2,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Positioned.fill(
            top: -10.r,
            right: -10.r,
            child: Align(
              alignment: Alignment.topRight,
              child: SplashIconButton(
                padding: EdgeInsets.zero,
                icon: const Icon(
                  Icons.cancel_rounded,
                  color: ColorName.borderColor,
                ),
                onTap: () {
                  context.read<FilePickerBloc>().add(FileRemoved(path));
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
