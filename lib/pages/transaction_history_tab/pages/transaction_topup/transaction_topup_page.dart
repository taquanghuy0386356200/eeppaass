import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/commons/widgets/buttons/floating_primary_button.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_topup/bloc/transaction_topup_bloc.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_topup/widget/transaction_topup_card.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_topup/widget/transaction_topup_search_form.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_topup/widget/transaction_topup_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionTopupPage extends StatefulWidget {
  const TransactionTopupPage({Key? key}) : super(key: key);

  @override
  State<TransactionTopupPage> createState() => _TransactionTopupPageState();
}

class _TransactionTopupPageState extends State<TransactionTopupPage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TransactionTopupBloc(
        transactionHistoryRepository: getIt<ITransactionHistoryRepository>(),
      )..add(const TransactionTopupFetched()),
      child: BlocConsumer<TransactionTopupBloc, TransactionTopupState>(
        listener: (context, state) {
          final error = state.error;

          if (error == null) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          } else {
            _refreshController.refreshFailed();
            _refreshController.loadFailed();
          }
        },
        builder: (context, state) {
          final error = state.error;

          if (state.isLoading || state.isRefreshing && state.listData.isEmpty) {
            return const TransactionTopupShimmerLoading();
          } else if (error != null && state.listData.isEmpty) {
            return CommonErrorPage(
              message: error,
              onTap: () => context
                  .read<TransactionTopupBloc>()
                  .add(const TransactionTopupFetched()),
            );
          }
          final listData = state.listData;
          return Scaffold(
            backgroundColor: Colors.transparent,
            floatingActionButton: GestureDetector(
              onTap: () => _onActionButtonTapped(
                context,
                topupDateFrom: state.topupDateFrom,
                topupDateTo: state.topupDateTo,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "Tìm kiếm",
                    style: Theme.of(context).textTheme.caption!.copyWith(
                        color: Colors.orange,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  const FloatingPrimaryButton(),
                ],
              ),
            ),
            body: state.listData.isEmpty
                ? NoDataPage(
                    title: context.read<TransactionTopupBloc>().isSearching
                        ? 'Bạn không có giao dịch nào phát sinh trong thời gian tìm kiếm'
                        : 'Bạn chưa có giao dịch nào phát sinh',
                    description: '',
                  )
                : SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    enablePullUp: !state.isFull,
                    onRefresh: () => context.read<TransactionTopupBloc>().add(
                          const TransactionTopupRefreshed(),
                        ),
                    onLoading: () => context.read<TransactionTopupBloc>().add(
                          const TransactionTopupLoadMore(),
                        ),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        final item = listData[index];
                        return Padding(
                          padding: EdgeInsets.fromLTRB(
                            24.w,
                            index == 0 ? 24.h : 8.h,
                            24.w,
                            index == listData.length - 1 ? 24.h : 8.h,
                          ),
                          child: TransactionTopupCard(contractTopup: item),
                        );
                      },
                      separatorBuilder: (context, index) => Column(
                        children: [
                          SizedBox(height: 8.h),
                          Divider(
                            color: ColorName.borderColor,
                            height: 0.5,
                            thickness: 0.5,
                            indent: 16.w,
                            endIndent: 16.w,
                          ),
                          SizedBox(height: 16.h),
                        ],
                      ),
                    ),
                  ),
          );
        },
      ),
    );
  }

  void _onActionButtonTapped(
    BuildContext context, {
    DateTime? topupDateFrom,
    DateTime? topupDateTo,
  }) async {
    await showBarModalBottomSheet(
      context: context,
      expand: false,
      useRootNavigator: true,
      builder: (modalContext) {
        return BlocProvider.value(
          value: context.read<TransactionTopupBloc>(),
          child: TransactionTopupSearchForm(
            topupDateFrom: topupDateFrom,
            topupDateTo: topupDateTo,
          ),
        );
      },
    );
  }
}
