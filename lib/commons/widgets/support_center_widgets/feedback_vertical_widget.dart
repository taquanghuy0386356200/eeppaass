import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FeedbackVerticalWidget extends StatelessWidget {
  const FeedbackVerticalWidget({
    Key? key,
    this.title = 'Chủ đề góp ý',
    this.content = '1. Hỗ trợ CTT',
  }) : super(key: key);

  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontWeight: FontWeight.w700,
              ),
        ),
        SizedBox(height: 8.h),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.75,
          child: Text(
            content,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: ColorName.textGray2,
                ),
          ),
        ),
      ],
    );
  }
}
