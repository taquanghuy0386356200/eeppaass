// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_passcode_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChangePassCodeResponse _$ChangePassCodeResponseFromJson(
        Map<String, dynamic> json) =>
    ChangePassCodeResponse(
      data: json['data'] == null
          ? null
          : VerifyConfigModel.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChangePassCodeResponseToJson(
        ChangePassCodeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
