import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/validators/phone_number_validator.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/textfield/plate_type_picker.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_form_bloc.dart';
import 'package:epass/pages/forgot_password/validators/forgot_password_form_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ForgotPasswordForm extends StatefulWidget {
  const ForgotPasswordForm({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordForm> createState() => _ForgotPasswordFormState();
}

class _ForgotPasswordFormState extends State<ForgotPasswordForm>
    with PhoneNumberValidator, ForgotPasswordFormValidator {
  final _formKey = GlobalKey<FormState>();
  final _plateNumberController = TextEditingController();
  final _plateTypeController = TextEditingController();
  final _phoneNumberController = TextEditingController();
  CategoryModel? selectedPlateType;

  @override
  void dispose() {
    _plateNumberController.dispose();
    _plateTypeController.dispose();
    _phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: BlocListener<ForgotPasswordFormBloc, ForgotPasswordFormState>(
        listener: (context, state) {
          context.pushRoute(const ForgotPasswordNewPasswordRoute());
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PrimaryTextField(
              isMandatory: true,
              controller: _plateNumberController,
              labelText: 'Biển số xe',
              hintText: 'Biển số xe (00X12345)',
              isPlateNumber: true,
              maxLength: 20,
              validator: plateNumberValidator,
            ),
            SizedBox(height: 20.h),
            PlateTypePicker(
              isMandatory: true,
              onItemChanged: (plateType) => selectedPlateType = plateType?.value,
            ),
            SizedBox(height: 20.h),
            PrimaryTextField(
              isMandatory: true,
              controller: _phoneNumberController,
              labelText: 'Số điện thoại đăng ký',
              hintText: 'Nhập số điện thoại',
              inputType: TextInputType.number,
              validator: phoneNumberValidator,
              maxLength: 10,
            ),
            SizedBox(height: 40.h),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: SizedBox(
                    height: 56.h,
                    child: SecondaryButton(
                      title: 'Huỷ',
                      onTap: context.popRoute,
                    ),
                  ),
                ),
                SizedBox(width: 20.w),
                Expanded(
                  child: LoadingPrimaryButton(
                    height: 56.h,
                    title: 'Tiếp tục',
                    onTap: () {
                      FocusScope.of(context).unfocus();

                      final plateTypeCode = selectedPlateType?.code;
                      final isValidated = _formKey.currentState!.validate() &&
                          plateTypeCode != null;

                      if (isValidated) {
                        context
                            .read<ForgotPasswordFormBloc>()
                            .add(ForgotPasswordFormCompleted(
                              plateNumber: _plateNumberController.text,
                              plateTypeCode: plateTypeCode!,
                              phoneNumber: _phoneNumberController.text,
                            ));
                      }
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
