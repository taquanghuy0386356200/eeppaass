import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/repo/vietmap_repository.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/utility/vietmap/link_vietmap/link_viet_map_bloc.dart';
import 'package:epass/pages/utility/vietmap/link_vietmap/link_viet_map_page.dart';
import 'package:epass/pages/utility/vietmap/vietmap_products/viet_map_product_bloc.dart';
import 'package:epass/pages/utility/vietmap/vietmap_products/viet_map_product_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_segmented_control/material_segmented_control.dart';

class VietMapPage extends StatefulWidget {
  const VietMapPage({Key? key}) : super(key: key);

  @override
  State<VietMapPage> createState() => _VietMapPageState();
}

class _VietMapPageState extends State<VietMapPage> {
  final Map<int, Widget> _children = {
    0: const Text('Liên kết tài khoản'),
    1: const Text('Sản phẩm Vietmap'),
  };
  int _currentSelection = 0;
  final _linkVietMapPage = const LinkVietMapPage();
  final _vietMapProductPage = const VietMapProductPage();

  @override
  void initState() {
    super.initState();
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));
  }

  @override
  Widget build(BuildContext context) {
    return BasePage(
      backgroundColor: Colors.white,
      leading: BackButton(onPressed: () {
        context
            .read<HomeTabsBloc>()
            .add(const HomeTabBarRequestHidden(isHidden: false));
        context.popRoute();
        context.popRoute();
      }),
      title: 'Kết nối Vietmap',
      resizeToAvoidBottomInset: true,
      child: Stack(
        children: [
          FadeAnimation(
              delay: 0.5,
              child: GradientHeaderContainer(
                height: 120.h,
              )),
          SafeArea(
            child: FadeAnimation(
              delay: 1,
              direction: FadeDirection.up,
              child: Column(
                children: [
                  SizedBox(
                    height: 40.h,
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(16.w, 20.w, 16.w, 4.w),
                    width: double.infinity,
                    child: MaterialSegmentedControl(
                      children: _children,
                      selectionIndex: _currentSelection,
                      borderColor: ColorName.primaryColor,
                      selectedColor: ColorName.primaryColor,
                      unselectedColor: Colors.white,
                      borderRadius: 32.0,
                      horizontalPadding: EdgeInsets.zero,
                      onSegmentChosen: (index) {
                        setState(() {
                          _currentSelection = index as int;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  MultiBlocProvider(
                      providers: [
                        BlocProvider<LinkVietMapBloc>(
                            create: (context) => LinkVietMapBloc(
                                  userRepository: getIt<IUserRepository>(),
                                  appBloc: context.read<AppBloc>(),
                                )),
                        BlocProvider<VietMapProductBloc>(
                            create: (context) =>
                                VietMapProductBloc(
                                  vietMapRepository: getIt<IVietMapRepository>(),
                                  userRepository: getIt<IUserRepository>()
                                )
                        )
                      ],
                      child: _currentSelection == 0
                          ? _linkVietMapPage
                          : _vietMapProductPage)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


}
