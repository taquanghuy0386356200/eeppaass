import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/pages/login/bloc/app_version_bloc.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/pages/register_vehicles/widgets/vehicle_input_form.dart';
import 'package:epass/pages/register/widgets/base_register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterVehiclesPage extends StatefulWidget {
  const RegisterVehiclesPage({Key? key}) : super(key: key);

  @override
  State<RegisterVehiclesPage> createState() => _RegisterVehiclesPageState();
}

class _RegisterVehiclesPageState extends State<RegisterVehiclesPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    context.read<AppVersionBloc>().add(const RegistFlagFetched());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppVersionBloc, AppVersionState>(
      listener: (context, state) {
        if (state is RegistFlagFetchedFailure) {
          showErrorSnackBBar(
            context: context,
            message: state.message,
          );
        }
      },
      builder: (context, state) {
        final registerFee = state is RegistFlagFetchedSuccess ? state.registerFee : '';
        return Form(
          key: _formKey,
          child: BaseRegisterPage(
            title: 'Đăng ký dán thẻ',
            step: 1,
            child: BlocBuilder<RegisterVehiclesBloc, RegisterVehiclesState>(
              builder: (context, state) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.listRegisterVehicle.length,
                      itemBuilder: (context, index) {
                        final vehicleInfo = state.listRegisterVehicle[index].vehicleInfo;
                        return VehicleInputForm(
                          vehicleIndex: index,
                          plateNumber: vehicleInfo.plateNumber,
                          plateType: vehicleInfo.plateType,
                          registerFee: registerFee.toString(),
                        );
                      },
                      separatorBuilder: (context, index) => SizedBox(height: 20.h),
                    ),
                    // SizedBox(height: 20.h),
                    // SizedBox(
                    //   height: 56.h,
                    //   width: double.infinity,
                    //   child: SecondaryButton(
                    //     icon: Assets.icons.add.svg(color: ColorName.textGray1),
                    //     title: 'Thêm xe',
                    //     onTap: () => context.read<RegisterVehiclesBloc>().add(VehicleInfoAdded()),
                    //   ),
                    // ),
                    SizedBox(height: 32.h),
                    SizedBox(
                      height: 56.h,
                      width: double.infinity,
                      child: PrimaryButton(
                        title: 'Tiếp theo',
                        onTap: () {
                          if (_formKey.currentState!.validate()) {
                            FocusScope.of(context).unfocus();
                            context.pushRoute(const RegisterPaperTypeRoute());
                          }
                        },
                      ),
                    ),
                    SizedBox(height: 16.h),
                  ],
                );
              },
            ),
          ),
        );
      }
    );
  }
}
