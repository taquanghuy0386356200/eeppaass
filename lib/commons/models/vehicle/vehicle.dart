import 'package:epass/commons/extensions/string_ext.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

/// vehicleId : 1000976
/// contractId : 1001022
/// plateNumber : "02G11111"
/// vehicleTypeId : 11
/// vehicleGroupId : 2
/// vehicleGroupName : "Loại 2"
/// vehicleGroupDescription : "Xe dưới 12 ghế đến 30 ghế ngồi; xe tải có tải trọng từ 2 tấn đến 4 tấn"
/// seatNumber : 5
/// netWeight : 1.0
/// status : "1"
/// activeStatus : "1"
/// epc : "3416214B9400F274G0001000"
/// rfidSerial : "G0001000"
/// vehicleTypeCode : "11"
/// plateTypeCode : "1"

part 'vehicle.g.dart';

@JsonEnum(alwaysCreate: true)
enum VehicleStatus {
  @JsonValue('0')
  inactive,
  @JsonValue('1')
  active,
  @JsonValue('unknown')
  unknown,
}

@JsonEnum(alwaysCreate: true)
enum VehicleActiveStatus {
  @JsonValue('0')
  inactive,
  @JsonValue('1')
  active,
  @JsonValue('2')
  canceled,
  @JsonValue('3')
  closed,
  @JsonValue('4')
  open,
  @JsonValue('5')
  transfered,
  @JsonValue('unknown')
  unknown,
}

@JsonEnum(alwaysCreate: true)
enum PlateTypeCode {
  @JsonValue('1')
  white,
  @JsonValue('2')
  blue,
  @JsonValue('3')
  red,
  @JsonValue('4')
  ng,
  @JsonValue('5')
  inter,
  @JsonValue('6')
  yellow,
  @JsonValue('unknown')
  unknown,
}

@JsonEnum(alwaysCreate: true)
enum VehicleDebitStatus {
  @JsonValue(0)
  inactive,
  @JsonValue(1)
  active,
  @JsonValue(2)
  complete,
}


@JsonSerializable()
@immutable
class Vehicle {
  const Vehicle({
    this.vehicleId,
    this.contractId,
    this.plateNumber,
    this.vehicleTypeId,
    this.vehicleGroupId,
    this.vehicleGroupName,
    this.vehicleGroupDescription,
    this.seatNumber,
    this.netWeight,
    this.cargoWeight,
    this.status,
    this.activeStatus,
    this.epc,
    this.rfidSerial,
    this.vehicleTypeCode,
    this.plateTypeCode,
    this.debit
  });

  final int? vehicleId;
  final int? contractId;
  final String? plateNumber;
  final int? vehicleTypeId;
  final int? vehicleGroupId;
  final String? vehicleGroupName;
  final String? vehicleGroupDescription;
  final int? seatNumber;
  final double? netWeight;
  final double? cargoWeight;

  @JsonKey(unknownEnumValue: VehicleStatus.unknown)
  final VehicleStatus? status;

  @JsonKey(unknownEnumValue: VehicleActiveStatus.unknown)
  final VehicleActiveStatus? activeStatus;

  final String? epc;
  final String? rfidSerial;
  final String? vehicleTypeCode;

  @JsonKey(unknownEnumValue: PlateTypeCode.unknown)
  final PlateTypeCode? plateTypeCode;
  final VehicleDebit? debit;

  factory Vehicle.fromJson(Map<String, dynamic> json) =>
      _$VehicleFromJson(json);

  Map<String, dynamic> toJson() => _$VehicleToJson(this);

  @override
  String toString() {
    return 'Vehicle{vehicleId: $vehicleId}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Vehicle &&
          runtimeType == other.runtimeType &&
          vehicleId == other.vehicleId;

  @override
  int get hashCode => vehicleId.hashCode;
}

extension VehicleExt on Vehicle {
  String? get plateNumberNoSuffix {
    if (plateNumber != null && plateNumber!.trim().isNotEmpty) {
      final lastCharacter = plateNumber!.last();
      if (lastCharacter.isAz()) {
        return plateNumber!.first(n: plateNumber!.length - 1);
      }
    }

    return plateNumber;
  }
}

@JsonSerializable()
@immutable
class VehicleListData {
  final List<Vehicle>? listData;
  final int? count;

  const VehicleListData({
    this.listData,
    this.count,
  });

  factory VehicleListData.fromJson(Map<String, dynamic> json) =>
      _$VehicleListDataFromJson(json);
}

@immutable
class VehicleListDataNotNull {
  final List<Vehicle> listData;
  final int count;

  const VehicleListDataNotNull({
    required this.listData,
    required this.count,
  });
}

@JsonSerializable()
@immutable
class VehicleListDataResponse {
  final VehicleListData? data;
  final MessageData? mess;

  const VehicleListDataResponse({
    this.data,
    this.mess,
  });

  factory VehicleListDataResponse.fromJson(Map<String, dynamic> json) =>
      _$VehicleListDataResponseFromJson(json);
}

@JsonSerializable()
@immutable
class VehicleDebit {
  final VehicleDebitStatus? statusDebit;
  final int? vehicleId;
  final int? fee;
  final String? debitStatusName;

  const VehicleDebit({
    this.statusDebit,
    this.vehicleId,
    this.fee,
    this.debitStatusName
  });

  factory VehicleDebit.fromJson(Map<String, dynamic> json) =>
      _$VehicleDebitFromJson(json);
}
