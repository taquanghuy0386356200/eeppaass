import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contracts_info.g.dart';

/// contractId : 1001022
/// contractNo : "V001001022"
/// plateNumber : "02G11111"
/// custName : "Giang0204"

@JsonSerializable()
class ContractsInfo {
  const ContractsInfo({
    this.contractId,
    this.contractNo,
    this.plateNumber,
    this.custName,
  });

  final int? contractId;
  final String? contractNo;
  final String? plateNumber;
  final String? custName;

  factory ContractsInfo.fromJson(Map<String, dynamic> json) =>
      _$ContractsInfoFromJson(json);
}

@JsonSerializable()
class ContractsInfoData {
  final List<ContractsInfo>? listData;
  final int? count;

  ContractsInfoData({
    this.listData,
    this.count,
  });

  factory ContractsInfoData.fromJson(Map<String, dynamic> json) =>
      _$ContractsInfoDataFromJson(json);
}

@JsonSerializable()
class ContractsInfoResponse {
  final ContractsInfoData? data;
  final MessageData? mess;

  ContractsInfoResponse({
    this.data,
    this.mess,
  });

  factory ContractsInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$ContractsInfoResponseFromJson(json);
}
