// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'confirm_contracts_payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfirmContractsPaymentRequest _$ConfirmContractsPaymentRequestFromJson(
        Map<String, dynamic> json) =>
    ConfirmContractsPaymentRequest(
      amount: json['amount'] as int,
      destContractId: json['destContractId'] as String,
      requestId: json['requestId'] as String,
      topupChannel: json['topupChannel'] as String,
    );

Map<String, dynamic> _$ConfirmContractsPaymentRequestToJson(
        ConfirmContractsPaymentRequest instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'destContractId': instance.destContractId,
      'requestId': instance.requestId,
      'topupChannel': instance.topupChannel,
    };

ConfirmContractsPaymentResponse _$ConfirmContractsPaymentResponseFromJson(
        Map<String, dynamic> json) =>
    ConfirmContractsPaymentResponse(
      order: json['data'] == null
          ? null
          : CashInOrder.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfirmContractsPaymentResponseToJson(
        ConfirmContractsPaymentResponse instance) =>
    <String, dynamic>{
      'data': instance.order,
      'mess': instance.mess,
    };
