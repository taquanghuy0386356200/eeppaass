// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'station_vehicle_alert_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StationVehicleAlert _$StationVehicleAlertFromJson(Map<String, dynamic> json) =>
    StationVehicleAlert(
      vehicleTrackId: json['vehicleTrackId'] as int?,
      contractId: json['contractId'] as int?,
      contractNo: json['contractNo'] as String?,
      trackDate: json['trackDate'] as String?,
      stationId: json['stationId'] as String?,
      status: json['status'] as int?,
      warLat1: (json['warLat1'] as num?)?.toDouble(),
      warLong1: (json['warLong1'] as num?)?.toDouble(),
      createUser: json['createUser'] as String?,
      createDate: json['createDate'] as String?,
      updateUser: json['updateUser'] as String?,
      updateDate: json['updateDate'] as String?,
      message: json['message'] as String?,
    );

Map<String, dynamic> _$StationVehicleAlertToJson(
        StationVehicleAlert instance) =>
    <String, dynamic>{
      'vehicleTrackId': instance.vehicleTrackId,
      'contractId': instance.contractId,
      'contractNo': instance.contractNo,
      'trackDate': instance.trackDate,
      'stationId': instance.stationId,
      'status': instance.status,
      'warLat1': instance.warLat1,
      'warLong1': instance.warLong1,
      'createUser': instance.createUser,
      'createDate': instance.createDate,
      'updateUser': instance.updateUser,
      'updateDate': instance.updateDate,
      'message': instance.message,
    };

StationVehicleAlertResponse _$StationVehicleAlertResponseFromJson(
        Map<String, dynamic> json) =>
    StationVehicleAlertResponse(
      data: json['data'] == null
          ? null
          : StationVehicleAlert.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$StationVehicleAlertResponseToJson(
        StationVehicleAlertResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
