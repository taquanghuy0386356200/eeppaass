// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_vehicle_profile_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateVehicleProfileRequest _$UpdateVehicleProfileRequestFromJson(
        Map<String, dynamic> json) =>
    UpdateVehicleProfileRequest(
      contractId: json['contractId'] as String?,
      custId: json['custId'] as String?,
      actTypeId: json['actTypeId'] as int?,
      isUpdate: json['isUpdate'] as bool?,
      vehicleProfiles: (json['vehicleProfiles'] as List<dynamic>?)
          ?.map((e) => VehicleDocument.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UpdateVehicleProfileRequestToJson(
        UpdateVehicleProfileRequest instance) =>
    <String, dynamic>{
      'contractId': instance.contractId,
      'custId': instance.custId,
      'actTypeId': instance.actTypeId,
      'isUpdate': instance.isUpdate,
      'vehicleProfiles': instance.vehicleProfiles,
    };

VehicleDocument _$VehicleDocumentFromJson(Map<String, dynamic> json) =>
    VehicleDocument(
      vehicleProfileId: json['vehicleProfileId'] as int?,
      documentTypeId: json['documentTypeId'] as int?,
      fileName: json['fileName'] as String?,
      fileBase64: json['fileBase64'] as String?,
    );

Map<String, dynamic> _$VehicleDocumentToJson(VehicleDocument instance) =>
    <String, dynamic>{
      'vehicleProfileId': instance.vehicleProfileId,
      'documentTypeId': instance.documentTypeId,
      'fileName': instance.fileName,
      'fileBase64': instance.fileBase64,
    };
