// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_msg.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationMsg _$NotificationMsgFromJson(Map<String, dynamic> json) =>
    NotificationMsg(
      notificationMsgId: json['notificationMsgId'] as int?,
      notificationCode: json['notificationCode'] as String?,
      notificationName: json['notificationName'] as String?,
      toContractId: json['toContractId'] as int?,
      toContractNo: json['toContractNo'] as String?,
      toPlateTypeCode: json['toPlateTypeCode'] as String?,
      toPlateNumber: json['toPlateNumber'] as String?,
      toUserId: json['toUserId'] as String?,
      message: json['message'] as String?,
      orgPushApp: json['orgPushApp'] as String?,
      pushDate: json['pushDate'] as String?,
      viewStatus: $enumDecodeNullable(_$ViewStatusEnumMap, json['viewStatus'],
          unknownValue: ViewStatus.unknown),
      uri: json['uri'] as String?,
    );

Map<String, dynamic> _$NotificationMsgToJson(NotificationMsg instance) =>
    <String, dynamic>{
      'notificationMsgId': instance.notificationMsgId,
      'notificationCode': instance.notificationCode,
      'notificationName': instance.notificationName,
      'toContractId': instance.toContractId,
      'toContractNo': instance.toContractNo,
      'toPlateTypeCode': instance.toPlateTypeCode,
      'toPlateNumber': instance.toPlateNumber,
      'toUserId': instance.toUserId,
      'message': instance.message,
      'orgPushApp': instance.orgPushApp,
      'pushDate': instance.pushDate,
      'uri': instance.uri,
      'viewStatus': _$ViewStatusEnumMap[instance.viewStatus],
    };

const _$ViewStatusEnumMap = {
  ViewStatus.read: 1,
  ViewStatus.unread: 0,
  ViewStatus.unknown: 'unknown',
};

NotificationMsgData _$NotificationMsgDataFromJson(Map<String, dynamic> json) =>
    NotificationMsgData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => NotificationMsg.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$NotificationMsgDataToJson(
        NotificationMsgData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

NotificationMsgResponse _$NotificationMsgResponseFromJson(
        Map<String, dynamic> json) =>
    NotificationMsgResponse(
      data: json['data'] == null
          ? null
          : NotificationMsgData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
      totalUnread: json['totalUnread'] as int?,
    );

Map<String, dynamic> _$NotificationMsgResponseToJson(
        NotificationMsgResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
      'totalUnread': instance.totalUnread,
    };

UpdateNotificationViewStatusRequest
    _$UpdateNotificationViewStatusRequestFromJson(Map<String, dynamic> json) =>
        UpdateNotificationViewStatusRequest(
          viewStatus: json['viewStatus'] as int? ?? 1,
        );

Map<String, dynamic> _$UpdateNotificationViewStatusRequestToJson(
        UpdateNotificationViewStatusRequest instance) =>
    <String, dynamic>{
      'viewStatus': instance.viewStatus,
    };
