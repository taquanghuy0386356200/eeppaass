// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'station.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Station _$StationFromJson(Map<String, dynamic> json) => Station(
      code: json['code'] as String?,
      name: json['name'] as String?,
      id: json['id'] as int?,
      nameShort: json['name_short'] as String?,
      stationType: json['station_type'] as String?,
      isActive: json['is_active'] as String?,
      addressDetail: json['address_detail'] as String?,
      methodChargeId: $enumDecodeNullable(
          _$MethodChargeStringEnumMap, json['method_charge_id'],
          unknownValue: MethodChargeString.unknown),
      province: json['province'] as String?,
      longitude: (json['longitude'] as num?)?.toDouble(),
      latitude: (json['latitude'] as num?)?.toDouble(),
      booCode: $enumDecodeNullable(_$BooCodeEnumMap, json['boo_code'],
          unknownValue: BooCode.unknown),
      isEtc: json['is_etc'] as int?,
      priceImagePath: json['price_image_path'] as String?,
      priceLinkWeb: json['price_link_web'] as String?,
    );

Map<String, dynamic> _$StationToJson(Station instance) => <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'id': instance.id,
      'name_short': instance.nameShort,
      'station_type': instance.stationType,
      'is_active': instance.isActive,
      'address_detail': instance.addressDetail,
      'method_charge_id': _$MethodChargeStringEnumMap[instance.methodChargeId],
      'province': instance.province,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'boo_code': _$BooCodeEnumMap[instance.booCode],
      'is_etc': instance.isEtc,
      'price_image_path': instance.priceImagePath,
      'price_link_web': instance.priceLinkWeb,
    };

const _$MethodChargeStringEnumMap = {
  MethodChargeString.normal: '1',
  MethodChargeString.block: '2',
  MethodChargeString.unknown: 'unknown',
};

const _$BooCodeEnumMap = {
  BooCode.boo1: 'BOO1',
  BooCode.boo2: 'BOO2',
  BooCode.unknown: 'unknown',
};

StationData _$StationDataFromJson(Map<String, dynamic> json) => StationData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => Station.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$StationDataToJson(StationData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

StationResponse _$StationResponseFromJson(Map<String, dynamic> json) =>
    StationResponse(
      data: json['data'] == null
          ? null
          : StationData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$StationResponseToJson(StationResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
