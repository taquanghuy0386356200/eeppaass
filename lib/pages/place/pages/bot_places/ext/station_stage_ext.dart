import 'package:epass/commons/models/station/station_stage.dart';

extension StationStageExt on StationStage {
  Map<String, String>? get _googleMapURLParams {
    if (stationId != null && latitude != null && longitude != null) {
      return {
        'q': 'Trạm $name',
        'center': '$latitude, $longitude',
        'zoom': '14',
      };
    } else if (stageId != null && addressIn != null && addressOut != null) {
      return {
        'saddr': addressIn!,
        'daddr': addressOut!,
        'zoom': '14',
        'directionsmode': 'driving'
      };
    } else {
      return null;
    }
  }

  String? get _googleMapURLEncodedParams {
    return _googleMapURLParams?.entries
        .map((e) =>
            '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }

  String? getGoogleMapUrl({required bool openApp}) {
    if (_googleMapURLParams != null) {
      if (openApp) {
        return Uri(
          scheme: 'comgooglemapsurl',
          host: 'maps.google.com',
          query: _googleMapURLEncodedParams,
        ).toString();
      } else {
        return Uri(
          scheme: 'https',
          host: 'maps.google.com',
          query: _googleMapURLEncodedParams,
        ).toString();
      }
    }
    return null;
  }
}
