part of 'home_tabs_bloc.dart';

@immutable
abstract class HomeTabsEvent extends Equatable {
  const HomeTabsEvent();
}

class HomeTabBarRequestHidden extends HomeTabsEvent {
  final bool isHidden;

  const HomeTabBarRequestHidden({required this.isHidden});

  @override
  List<Object> get props => [isHidden];
}

class HomeTabChanged extends HomeTabsEvent {
  final int currentTabIndex;

  const HomeTabChanged({required this.currentTabIndex});

  @override
  List<Object> get props => [currentTabIndex];
}
