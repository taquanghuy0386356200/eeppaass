// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle_types.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleTotalResponse _$VehicleTotalResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleTotalResponse(
      data: json['data'] == null
          ? null
          : ListVehicleResponse.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleTotalResponseToJson(
        VehicleTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

ListVehicleResponse _$ListVehicleResponseFromJson(Map<String, dynamic> json) =>
    ListVehicleResponse(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => VehicleResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$ListVehicleResponseToJson(
        ListVehicleResponse instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

VehicleResponse _$VehicleResponseFromJson(Map<String, dynamic> json) =>
    VehicleResponse(
      id: json['id'] as int?,
      code: json['code'] as String?,
      name: json['name'] as String?,
      description: json['description'] as String?,
      is_active: json['is_active'] as String?,
      mappingType: json['mappingType'] as String?,
    );

Map<String, dynamic> _$VehicleResponseToJson(VehicleResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'name': instance.name,
      'description': instance.description,
      'is_active': instance.is_active,
      'mappingType': instance.mappingType,
    };
