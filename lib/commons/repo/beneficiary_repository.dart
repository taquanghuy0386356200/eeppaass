import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/models/beneficiary/beneficiary_request.dart';
import 'package:simple_result/simple_result.dart';

abstract class IBeneficiaryRepository {
  Future<Result<BeneficiaryListDataNotNull, Failure>> getUserBeneficiarys({
    int pageSize = 20,
    int startRecord = 0,
    List<int>? statuses,
    String? reminiscentName,
    bool? inContract,
  });

  Future<Result<String?, Failure>> updateBeneficiary({
    required int beneficiaryInformationId,
    required String reminiscentName,
  });

  Future<Result<String?, Failure>> deleteBeneficiary({
    required String beneficiaryId,
  });

  Future<Result<String?, Failure>> createBeneficiary({
    required String contractNo,
    required String customerName,
    required String reminiscentName,
  });
}

class BeneficiaryRepository implements IBeneficiaryRepository {
  final CrmClient _crmClient;

  BeneficiaryRepository({required CrmClient crmClient})
      : _crmClient = crmClient;

  @override
  Future<Result<BeneficiaryListDataNotNull, Failure>> getUserBeneficiarys({
    int pageSize = 20,
    int startRecord = 0,
    List<int>? statuses,
    String? reminiscentName,
    bool? inContract,
  }) async {
    try {
      final response = await _crmClient.getUserBeneficiarys(
        pageSize: pageSize,
        startRecord: startRecord,
        reminiscentName: reminiscentName,
        statuses: statuses,
        inContract: inContract,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          count == null ||
          listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(BeneficiaryListDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> updateBeneficiary({
    required int beneficiaryInformationId,
    required String reminiscentName,
  }) async {
    try {
      final request = BeneficiaryRequestUpdate(
        reminiscentName: reminiscentName,
        beneficiaryInformationId: beneficiaryInformationId,
      );
      final response = await _crmClient.updateBeneficiary(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> deleteBeneficiary({
    required String beneficiaryId,
  }) async {
    try {
      final response =
          await _crmClient.deleteBeneficiary(beneficiaryId: beneficiaryId);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(ResponseFailure(
          message: response.mess?.description ?? 'Có lỗi xảy ra',
        ));
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> createBeneficiary({
    required String contractNo,
    required String customerName,
    required String reminiscentName,
  }) async {
    try {
      final request = BeneficiaryRequest(
        contractNo: contractNo,
        customerName: customerName,
        reminiscentName: reminiscentName,
      );

      final response = await _crmClient.saveBeneficiary(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
