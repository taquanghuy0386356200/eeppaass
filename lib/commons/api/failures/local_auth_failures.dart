import 'package:epass/commons/api/failures/failures.dart';

class UndefinedMethodFailure extends Failure {
  const UndefinedMethodFailure()
      : super(
            message:
                'Không xác định được phương thức xác thực sinh trắc học phù hợp');
}

class PasscodeNotSetFailure extends Failure {
  const PasscodeNotSetFailure() : super(message: 'Chưa cài đặt Passcode');
}

class NotEnrolledFailure extends Failure {
  const NotEnrolledFailure()
      : super(message: 'Thiết bị chưa được cài đặt xác thực sinh trắc học');
}

class BiometricNotAvailableFailure extends Failure {
  const BiometricNotAvailableFailure()
      : super(message: 'Xác thực sinh trắc học không khả dụng');
}

class BiometricLockedOutFailure extends Failure {
  const BiometricLockedOutFailure()
      : super(message: 'Xác thực sinh trắc học đã bị khoá');
}

class BiometricPermanentlyLockedOutFailure extends Failure {
  const BiometricPermanentlyLockedOutFailure()
      : super(message: 'Xác thực sinh trắc học đã bị khoá');
}
