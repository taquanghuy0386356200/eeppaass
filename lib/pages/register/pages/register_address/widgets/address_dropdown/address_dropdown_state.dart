part of 'address_dropdown_bloc.dart';

abstract class AddressDropdownState extends Equatable {
  const AddressDropdownState();
}

class AddressDropdownInitial extends AddressDropdownState {
  @override
  List<Object> get props => [];
}

class AddressVTTFetchedInProgress extends AddressDropdownState {
  @override
  List<Object> get props => [];
}

class AddressVTTFetchedSuccess extends AddressDropdownState {
  final List<AddressVTT> data;

  const AddressVTTFetchedSuccess(this.data);

  @override
  List<Object> get props => [data];
}

class AddressVTTFetchedFailure extends AddressDropdownState {
  final String message;

  const AddressVTTFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}
