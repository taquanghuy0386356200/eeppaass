// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'viet_map_status_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VietMapStatus _$VietMapStatusFromJson(Map<String, dynamic> json) =>
    VietMapStatus(
      linked: json['linked'] as bool?,
      accountName: json['accountName'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      token: json['token'] as String?,
    );

Map<String, dynamic> _$VietMapStatusToJson(VietMapStatus instance) =>
    <String, dynamic>{
      'linked': instance.linked,
      'accountName': instance.accountName,
      'phoneNumber': instance.phoneNumber,
      'token': instance.token,
    };

VietMapStatusResponse _$VietMapStatusResponseFromJson(
        Map<String, dynamic> json) =>
    VietMapStatusResponse(
      data: VietMapStatus.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VietMapStatusResponseToJson(
        VietMapStatusResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
