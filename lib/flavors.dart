enum Flavor {
  dev,
  stg,
  prod,
}

extension FlavorName on Flavor {
  String get name => toString().split('.').last;
}

class F {
  static Flavor? appFlavor;

  static String get name => appFlavor?.name ?? '';

  static String get title {
    switch (appFlavor) {
      case Flavor.dev:
        return 'ePass 2.0 (development)';
      case Flavor.stg:
        return 'ePass 2.0 (stg)';
      case Flavor.prod:
        return 'ePass 2.0';
      default:
        return 'title';
    }
  }

  static String get bundleId {
    switch (appFlavor) {
      case Flavor.dev:
        return 'com.viettel.etc.epass.dev';
      case Flavor.stg:
        return 'com.viettel.etc.epass.test';
      case Flavor.prod:
        return 'com.viettel.etc.epass.prod';
      default:
        return 'bundleId';
    }
  }

  // Server URLs
  static String get categoriesURL {
    switch (appFlavor) {
      case Flavor.dev:
        return "http://10.248.158.7:9752";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/dmdc2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/dmdc2";
      default:
        return "categoriesURL";
    }
  }

  static String get appStoreHomeModuleUrl {
    switch (appFlavor) {
      case Flavor.dev:
        return  'https://etctest.epass-vdtc.com.vn/wp-content/uploads/2023/03';
      case Flavor.stg:
        return 'https://etctest.epass-vdtc.com.vn/wp-content/uploads/2023/03';
      case Flavor.prod:
        return 'https://epass-vdtc.com.vn/wp-content/uploads/2023/02';
      default:
        return 'appStoreHomeModuleUrl';
    }
  }
  static String get crmURL {
    switch (appFlavor) {
      case Flavor.dev:
        return "http://10.248.158.7:9789";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/crm2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/crm2";
      default:
        return "crmURL";
    }
  }

  static String get reconcilURL {
    switch (appFlavor) {
      case Flavor.dev:
        return "http://10.248.158.7:9761";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/doisoat2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/doisoat2";
      default:
        return "reconcilURL";
    }
  }

  static String get gatewayURL {
    switch (appFlavor) {
      case Flavor.dev:
        return "http://125.235.9.98:9753";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/gateway2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/gateway2";
      default:
        return "gatewayURL";
    }
  }

  static String get billingURL {
    switch (appFlavor) {
      case Flavor.dev:
        return "http://10.248.158.7:9972";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/billing2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/billing2";
      default:
        return "billingURL";
    }
  }

  static String get shopURL {
    switch (appFlavor) {
      case Flavor.dev:
        return "http://10.248.158.7:9762";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/im2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/im2";
      default:
        return "shopURL";
    }
  }

  static String get notificationURL {
    switch (appFlavor) {
      case Flavor.dev:
        return " http://10.254.247.7:9793";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/notification2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/notification2";
      default:
        return "notificationURL";
    }
  }

  static String get ccURL {
    switch (appFlavor) {
      case Flavor.dev:
        return "http://10.248.158.7:9790";
      case Flavor.stg:
        return "https://backendtest.epass-vdtc.com.vn/cc2";
      case Flavor.prod:
        return "https://backend.epass-vdtc.com.vn/cc2";
      default:
        return "ccURL";
    }
  }

  static String get ocrURL {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
      case Flavor.prod:
        return "http://27.71.110.53:8084";
      default:
        return "ocrURL";
    }
  }

  static String get textToSpeechURL {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
      case Flavor.prod:
        return "https://viettelgroup.ai";
      default:
        return "textToSpeechURL";
    }
  }

  // KEYCLOAK
  static String get keycloakClientId {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
      case Flavor.prod:
        return "mobile-app-chupt";
      default:
        return "keycloakClientId";
    }
  }

  static String get keycloakGrantType {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
      case Flavor.prod:
        return "password";
      default:
        return "keycloakGrantType";
    }
  }

  // SECRETS
  static String get momoMerchantName {
    return 'VDTC';
  }

  static String get momoPartnerCode {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'MOMOGN7I20201104';
      case Flavor.prod:
        return 'MOMONTIF20201125';
      default:
        return 'momoPartnerCode';
    }
  }

  static String get momoPartnerSchemeId {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'momogn7i20201104';
      case Flavor.prod:
        return 'momontif20201125';
      default:
        return 'momoPartnerSchemeId';
    }
  }

  static String get viettelPayGateUrl {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'https://chain.bankplus.vn/PaymentGateway/payment';
      case Flavor.prod:
        return 'https://pay3.viettel.vn/PaymentGateway/payment';
      default:
        return 'viettelPayGateUrl';
    }
  }

  static String get viettelPayGateAccessCode {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'd41d8cd98f00b204e9800998ecf8427e188fb2c51942db8928d4231bfbc3dea7';
      case Flavor.prod:
        return 'd41d8cd98f00b204e9800998ecf8427ef88e6de69b4b8637cd2144426e66a2ce';
      default:
        return 'viettelPayGateAccessCode';
    }
  }

  static String get viettelPayGateHashCode {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'd41d8cd98f00b204e9800998ecf8427e9524c097eefca369fbce6fb3041c8755';
      case Flavor.prod:
        return 'd41d8cd98f00b204e9800998ecf8427e95058a08d13fd6abffe9a6a2beb55e7d';
      default:
        return 'viettelPayGateHashCode';
    }
  }

  static String get viettelPayGateMerchantCode {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'EPASSUAT';
      case Flavor.prod:
        return 'VDTC1';
      default:
        return 'viettelPayGateMerchantCode';
    }
  }

  //BIDV
  static String get bidvPayGateMerchantId {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return '287002';
      case Flavor.prod:
        return '287002';
      default:
        return 'bidvPayGateMerchantCode';
    }
  }

  static String get bidvPayGateUrl {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'https://www.bidv.net/BIDVGateway/WS';
      case Flavor.prod:
        return 'https://www.bidv.net/BIDVGateway/WS';
      default:
        return 'bidvPayGateUrl';
    }
  }

  // PayNow
  static String get payNowGateUrl {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'https://sandbox.viettel.vn/PaymentGateway/auto-payment/register';
      case Flavor.prod:
        return 'https://pay3.viettel.vn/PaymentGateway/auto-payment/register';
      default:
        return 'payNowGateUrl';
    }
  }

  // MB// MB
  static String get mbUsername {
    switch (appFlavor) {
      case Flavor.dev:
        return '5waPA98NTd5L9fAYWb8CL8QwGSdD3C0B';
      case Flavor.stg:
        return '5waPA98NTd5L9fAYWb8CL8QwGSdD3C0B';
      case Flavor.prod:
        return 'paaraeb5nVZSFRNDSyuCL3rukwkH2HVN';
      default:
        return 'mbUsername';
    }
  }

  static String get mbPassword {
    switch (appFlavor) {
      case Flavor.dev:
        return 'cx96qbBOsH5juyeN';
      case Flavor.stg:
        return 'cx96qbBOsH5juyeN';
      case Flavor.prod:
        return '9mIEMZJZh3IQRJ1C';
      default:
        return 'mbPassword';
    }
  }

  static String get mbGetTokenUrl {
    switch (appFlavor) {
      case Flavor.dev:
        return 'https://api-sandbox.mbbank.com.vn/oauth2/v1/token';
      case Flavor.stg:
        return 'https://api-sandbox.mbbank.com.vn/oauth2/v1/token';
      case Flavor.prod:
        return 'https://api-public.mbbank.com.vn/oauth2/v1/token';
      default:
        return 'mbGetTokenUrl';
    }
  }

  static String get mbPaymentUrl {
    switch (appFlavor) {
      case Flavor.dev:
        return 'https://api-sandbox.mbbank.com.vn/ms/payment/payment/app-mb/v1.0/request';
      case Flavor.stg:
        return 'https://api-sandbox.mbbank.com.vn/ms/payment/payment/app-mb/v1.0/request';
      case Flavor.prod:
        return 'https://api-public.mbbank.com.vn/ms/payment/payment/app-mb/v1.0/request';
      default:
        return 'mbPaymentUrl';
    }
  }

  static String get mbCheckSumUrl {
    switch (appFlavor) {
      case Flavor.dev:
        return 'https://api-sandbox.mbbank.com.vn/ms/payment/payment/app-mb/get-trans/v1.0/check';
      case Flavor.stg:
        return 'https://api-sandbox.mbbank.com.vn/ms/payment/payment/app-mb/get-trans/v1.0/check';
      case Flavor.prod:
        return 'https://api-public.mbbank.com.vn/ms/payment/payment/app-mb/get-trans/v1.0/check';
      default:
        return 'mbCheckSumUrl';
    }
  }

  static String get mbCheckPaymentResultUrl {
    switch (appFlavor) {
      case Flavor.dev:
        return 'https://api-sandbox.mbbank.com.vn/ms/payment/payment/app-mb/v1.0/transaction/get';
      case Flavor.stg:
        return 'https://api-sandbox.mbbank.com.vn/ms/payment/payment/app-mb/v1.0/transaction/get';
      case Flavor.prod:
        return 'https://api-public.mbbank.com.vn/ms/payment/payment/app-mb/v1.0/transaction/get';
      default:
        return 'mbCheckPaymentResultUrl';
    }
  }

  static String get vnPayTmnCode {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'EPASSAPP';
      case Flavor.prod:
        return 'APPEPASS';
      default:
        return 'vnPayTmnCode';
    }
  }

  static String get vnPayHashSecret {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'WNLSFJTFUQKMFDVOSNTOYQXYEPGETDCO';
      case Flavor.prod:
        return 'MLLROUJYHNQFMTXYDHGHEHVUCADFIMUF';
      default:
        return 'vnPayHashSecret';
    }
  }

  static String get vnPayPartnerSchemeId {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'vnpEpassApp';
      case Flavor.prod:
        return 'vnpEpassApp';
      default:
        return 'vnpPartnerSchemeId';
    }
  }

  static String get vnPayUrl {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html';
      case Flavor.prod:
        return 'https://pay.vnpay.vn/vpcpay.html';
      default:
        return 'vnpPartnerSchemeId';
    }
  }

  static String get ePassSecret {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'CFjLUi3NbWmbgJH8lUtmyTklq2BMPzGZkwP4Eamr3J8=';
      case Flavor.prod:
        return 'E+j/OXKwmULnm+V3pkDw9kId2hYuhC5ujxYjazr7zlo=';
      default:
        return 'ePassSecret';
    }
  }

  static String get crmAccessKey {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'b4a724fc47e43d5b16bb31e4bad4d2435f6faa9db0f97286f5973c836102c7cf';
      case Flavor.prod:
        return 'b4a724fc47e43d5b16bb31e4bad4d2435f6faa9db0f97286f5973c836102c7cf';
      default:
        return 'crmAccessKey';
    }
  }

  static String get crmHashKey {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'eed8fcb19a19fe0c8fc40a713967d560d3ffbabd7a5b7e715a5ba820b8844add';
      case Flavor.prod:
        return 'eed8fcb19a19fe0c8fc40a713967d560d3ffbabd7a5b7e715a5ba820b8844add';
      default:
        return 'crmHashKey';
    }
  }

  /// INSURANCE
  static String get insuranceVDSURL {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'https://apibaohiem.viettelpay.vn/Account/LoginByEpass';
      case Flavor.prod:
        return 'https://baohiem.viettelmoney.vn/Account/LoginByEpass';
      default:
        return 'insuranceURL';
    }
  }

  static String get insuranceVDSMerchantCode {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'EPASS_BAOHIEM';
      case Flavor.prod:
        return 'EPASS_BAOHIEM';
      default:
        return 'insuranceMerchantCode';
    }
  }

  static String get insuranceVDSAccessCode {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'e2a1059e971d7f64091e09a38e6049274fecc561ff1c56af7090afcfb6b022fe';
      case Flavor.prod:
        return 'b8c53766bd86b5249442017bb59d0cba8f66e880fd5e675fcc881b690935ab05';
      default:
        return 'insuranceAccessCode';
    }
  }

  static String get insuranceVDSSecretKey {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return '9073fe8f3474d22f4592826c0a1293b6fcff40493139c3ce506fe2dc1c444524';
      case Flavor.prod:
        return '0208d18b5107b867beb88910ca09ef468d3b77590c7aae3eda243cc037b5423e';
      default:
        return 'insuranceSecretKey';
    }
  }

  static String get insuranceGCURL {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'https://product-dev.globalcare.vn/all-products-epass';
      case Flavor.prod:
        return 'https://product.globalcare.vn/all-products-epass';
      default:
        return 'insuranceGCURL';
    }
  }

  static String get insuranceGCToken {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
        return 'J8cHVeCUVreFDIU3zu0p62';
      case Flavor.prod:
        return 'buw6pAwn7uwc7Tf3';
      default:
        return 'insuranceGCToken';
    }
  }

  static String get ocrApiToken {
    switch (appFlavor) {
      case Flavor.dev:
      case Flavor.stg:
      case Flavor.prod:
      default:
        return '7c8ba773-64cd-4ba5-a9bd-f035f06d0149';
    }
  }
}
