
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'verify_config_response.g.dart';

@JsonSerializable()
class VerifyConfigModel {
  int? configId;
  int? contractId;
  int? serviceId;
  String? serviceName;
  int? verifyType;
  int? status;
  bool? isCreatedPassCode;

  VerifyConfigModel({
    this.configId,
    required this.contractId,
    required this.serviceId,
    this.serviceName,
    this.verifyType,
    this.status,
    this.isCreatedPassCode,
  });

  factory VerifyConfigModel.fromJson(Map<String, dynamic> json) => _$VerifyConfigModelFromJson(json);
  Map<String, dynamic> toJson() => _$VerifyConfigModelToJson(this);
}

@JsonSerializable()
class VerifyConfigResponse {
  VerifyConfigResponse({
    this.data,
    this.mess,
  });

  final List<VerifyConfigModel>? data;
  final MessageData? mess;

  factory VerifyConfigResponse.fromJson(Map<String, dynamic> json) => _$VerifyConfigResponseFromJson(json);
}