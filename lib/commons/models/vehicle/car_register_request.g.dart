// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car_register_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarRegisterRequest _$CarRegisterRequestFromJson(Map<String, dynamic> json) =>
    CarRegisterRequest(
      plateNumber: json['plateNumber'] as String?,
      plateType: json['plateType'] as int?,
      isRegistry: json['isRegistry'] as bool?,
      cargoWeight: (json['cargoWeight'] as num?)?.toDouble(),
      grossWeight: (json['grossWeight'] as num?)?.toDouble(),
      seatNumber: json['seatNumber'] as int?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      vehicleGroupId: json['vehicleGroupId'] as int?,
      owner: json['owner'] as String?,
      vehicleProfileDTOs: (json['vehicleProfileDTOs'] as List<dynamic>?)
          ?.map((e) => VehicleProfile.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CarRegisterRequestToJson(CarRegisterRequest instance) =>
    <String, dynamic>{
      'plateNumber': instance.plateNumber,
      'plateType': instance.plateType,
      'isRegistry': instance.isRegistry,
      'cargoWeight': instance.cargoWeight,
      'grossWeight': instance.grossWeight,
      'seatNumber': instance.seatNumber,
      'vehicleTypeId': instance.vehicleTypeId,
      'vehicleGroupId': instance.vehicleGroupId,
      'owner': instance.owner,
      'vehicleProfileDTOs': instance.vehicleProfileDTOs,
    };
