/// isAlertMoney : 1
/// actTypeId : 14
/// reasonId : 26

class UpdateBalanceAlertRequest {
  final int isAlertMoney;
  final int? alertMoney;
  final int actTypeId;
  final int reasonId;

  UpdateBalanceAlertRequest({
    required this.isAlertMoney,
    this.alertMoney,
    this.actTypeId = 14,
    this.reasonId = 26,
  });

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isAlertMoney'] = isAlertMoney;
    map['alertMoney'] = alertMoney;
    map['actTypeId'] = actTypeId;
    map['reasonId'] = reasonId;
    return map;
  }
}
