import 'package:device_info_plus/device_info_plus.dart';
import 'package:dio/dio.dart';
import 'package:epass/commons/api/clients/app_store/app_store_client.dart';
import 'package:epass/commons/api/clients/billing/billing_client.dart';
import 'package:epass/commons/api/clients/cardoctor/cardoctor_client.dart';
import 'package:epass/commons/api/clients/category/category_client.dart';
import 'package:epass/commons/api/clients/cc/cc_client.dart';
import 'package:epass/commons/api/clients/crm/crm_client.dart';
import 'package:epass/commons/api/clients/gateway/gateway_client.dart';
import 'package:epass/commons/api/clients/notification/notification_client.dart';
import 'package:epass/commons/api/clients/ocr/ocr_client.dart';
import 'package:epass/commons/api/clients/reconcil/reconcil_client.dart';
import 'package:epass/commons/api/clients/text_to_speech/text_to_speech_client.dart';
import 'package:epass/commons/api/clients/vietmap/vietmap_client.dart';
import 'package:epass/commons/api/dio_di.dart';
import 'package:epass/commons/repo/address_repository.dart';
import 'package:epass/commons/repo/alert_repository.dart';
import 'package:epass/commons/repo/app_store_repository.dart';
import 'package:epass/commons/repo/app_version_repository.dart';
import 'package:epass/commons/repo/auto_cash_in_repository.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:epass/commons/repo/auto_cash_in_repository.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:epass/commons/repo/buy_ticket_repository.dart';
import 'package:epass/commons/repo/campaign_repository.dart';
import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:epass/commons/repo/insurance_repository.dart';
import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:epass/commons/repo/notification_repository.dart';
import 'package:epass/commons/repo/parking_repository.dart';
import 'package:epass/commons/repo/register_repository.dart';
import 'package:epass/commons/repo/report_repository.dart';
import 'package:epass/commons/repo/station_repository.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/commons/repo/vietmap_repository.dart';
import 'package:epass/commons/services/encryption/encryption.dart';
import 'package:epass/commons/services/local_auth/local_auth_service.dart';
import 'package:epass/commons/services/local_file_service/local_file_service.dart';
import 'package:epass/commons/services/momo/momo_payment.dart';
import 'package:epass/commons/services/viettel_money/viettel_money_service.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment.dart';
import 'package:epass/flavors.dart';
import 'package:epass/commons/repo/authentication_repository.dart';
import 'package:epass/pages/app.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/category/category_bloc.dart';
import 'package:epass/pages/bloc/fcm/fcm_bloc.dart';
import 'package:epass/pages/bloc/notification/notification_bloc.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';
import 'package:epass/pages/bloc/sms_register/sms_register_bloc.dart';
import 'package:epass/pages/bloc/station_vehicle_alert/station_vehicle_alert_bloc.dart';
import 'package:epass/pages/bloc/wait_for_confirmation/wait_for_confirmation_bloc.dart';
import 'package:epass/pages/login/bloc/app_version_bloc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';
import 'package:local_auth/local_auth.dart';

final getIt = GetIt.instance;

void configureDependencies() {
  // Third-party
  getIt.registerLazySingleton<Dio>(() => _DioDi().dio);
  getIt.registerLazySingleton<MomoPayment>(() => MomoPayment());
  getIt.registerLazySingleton<VNPayService>(() => VNPayService());
  getIt.registerLazySingleton<ViettelMoneyService>(() => ViettelMoneyService());
  getIt.registerLazySingleton<LocalAuthentication>(() => LocalAuthentication());
  getIt.registerLazySingleton<IEncryption>(() => AESEncryption());
  getIt.registerLazySingleton<FlutterSecureStorage>(
      () => const FlutterSecureStorage());
  getIt.registerSingletonAsync<FirebaseMessaging>(() async {
    final firebaseMessaging = FirebaseMessaging.instance;
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );
    return firebaseMessaging;
  });
  getIt.registerLazySingleton<ImagePicker>(() => ImagePicker());
  getIt.registerLazySingleton<FilePicker>(() => FilePicker.platform);
  getIt.registerLazySingleton<DeviceInfoPlugin>(() => DeviceInfoPlugin());

  // Service
  getIt.registerLazySingleton<ILocalAuthService>(
    () => LocalAuthService(localAuth: getIt<LocalAuthentication>()),
  );
  getIt.registerLazySingleton<ILocalFileService>(() => LocalFileService());

  // API Client
  getIt.registerLazySingleton<CrmClient>(() => CrmClient(
        getIt<Dio>(),
        baseUrl: F.crmURL,
      ));
  getIt.registerLazySingleton<ReconcilClient>(() => ReconcilClient(
        getIt<Dio>(),
        baseUrl: F.reconcilURL,
      ));
  getIt.registerLazySingleton<NotificationClient>(() => NotificationClient(
        getIt<Dio>(),
        baseUrl: F.notificationURL,
      ));
  getIt.registerLazySingleton<GatewayClient>(() => GatewayClient(
        getIt<Dio>(),
        baseUrl: F.gatewayURL,
      ));
  getIt.registerLazySingleton<CCClient>(() => CCClient(
        getIt<Dio>(),
        baseUrl: F.ccURL,
      ));
  getIt.registerLazySingleton<BillingClient>(() => BillingClient(
        getIt<Dio>(),
        baseUrl: F.billingURL,
      ));
  getIt.registerLazySingleton<CategoryClient>(() => CategoryClient(
        getIt<Dio>(),
        baseUrl: F.categoriesURL,
      ));
  getIt.registerLazySingleton<OCRClient>(() => OCRClient(
        getIt<Dio>(),
        baseUrl: F.ocrURL,
      ));
  getIt.registerLazySingleton<TextToSpeechClient>(() => TextToSpeechClient(
        getIt<Dio>(),
        baseUrl: F.textToSpeechURL,
      ));

  getIt.registerLazySingleton<SmsRegisterBloc>(() => SmsRegisterBloc(
        userRepository: getIt<IUserRepository>(),
        categoryRepository: getIt<ICategoryRepository>(),
        appBloc: getIt<AppBloc>(),
      ));

  getIt.registerLazySingleton<CarDoctorClient>(() => CarDoctorClient(
        getIt<Dio>(),
        baseUrl: dotenv.env['SSOURL']!,
      ));
  getIt.registerLazySingleton<VietMapClient>(() => VietMapClient(
        getIt<Dio>(),
      ));
  // App
  // Repository
  getIt.registerLazySingleton<IAuthenticationRepository>(
    () => AuthenticationRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<IUserRepository>(
    () => UserRepository(
        gatewayClient: getIt<GatewayClient>(),
        crmClient: getIt<CrmClient>(),
        categoryClient: getIt<CategoryClient>(),
        carDoctorClient: getIt<CarDoctorClient>()),
  );
  getIt.registerLazySingleton<ICampaignRepository>(
    () => CampaignRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<ICashInRepository>(
    () => CashInRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<ITransactionHistoryRepository>(
    () => TransactionHistoryRepository(
      crmClient: getIt<CrmClient>(),
      reconcilClient: getIt<ReconcilClient>(),
    ),
  );
  getIt.registerLazySingleton<INotificationRepository>(
    () => NotificationRepository(
      notificationClient: getIt<NotificationClient>(),
    ),
  );
  getIt.registerLazySingleton<IInvoiceRepository>(
    () => InvoiceRepository(
      billingClient: getIt<BillingClient>(),
      crmClient: getIt<CrmClient>(),
    ),
  );
  getIt.registerLazySingleton<ICategoryRepository>(
    () => CategoryRepository(
      categoryClient: getIt<CategoryClient>(),
    ),
  );
  getIt.registerLazySingleton<IVehicleRepository>(
    () => VehicleRepository(
      crmClient: getIt<CrmClient>(),
    ),
  );
  getIt.registerLazySingleton<IStationRepository>(
    () => StationRepository(
      categoryClient: getIt<CategoryClient>(),
    ),
  );
  getIt.registerLazySingleton<IBuyTicketRepository>(
    () => BuyTicketRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<IInsuranceRepository>(
    () => InsuranceRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<IAppVersionRepository>(
    () => AppVersionRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<IRegisterRepository>(
    () => RegisterRepository(
      ocrClient: getIt<OCRClient>(),
      crmClient: getIt<CrmClient>(),
    ),
  );
  getIt.registerLazySingleton<IAddressRepository>(
    () => AddressRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<IAlertRepository>(
    () => AlertRepository(
      crmClient: getIt<CrmClient>(),
      categoryClient: getIt<CategoryClient>(),
      textToSpeechClient: getIt<TextToSpeechClient>(),
    ),
  );
  getIt.registerLazySingleton<IFeedbackRepository>(
    () => FeedbackRepository(
      ccClient: getIt<CCClient>(),
    ),
  );
  getIt.registerLazySingleton<IReportRepository>(
    () => ReportRepository(
      ccClient: getIt<CCClient>(),
    ),
  );

  getIt.registerLazySingleton<IAutoCashInRepository>(
    () => AutoCashInRepository(crmClient: getIt<CrmClient>()),
  );

  getIt.registerLazySingleton<IVietMapRepository>(
    () => VietMapRepository(
      vietMapClient: getIt<VietMapClient>(),
    ),
  );

  // Bloc
  getIt.registerLazySingleton<AppBloc>(
      () => AppBloc(encryption: getIt<IEncryption>()));
  getIt.registerLazySingleton<SettingBloc>(() => SettingBloc(
        localAuthService: getIt<ILocalAuthService>(),
        secureStorage: getIt<FlutterSecureStorage>(),
        appBloc: getIt<AppBloc>(),
      ));

  // NotificationBloc
  _registerNotificationBlocs();

  getIt.registerLazySingleton<CategoryBloc>(() => CategoryBloc(
        categoryRepository: getIt<ICategoryRepository>(),
      ));
  getIt.registerLazySingleton<StationVehicleAlertBloc>(
      () => StationVehicleAlertBloc(
            alertRepository: getIt<IAlertRepository>(),
            categoryRepository: getIt<ICategoryRepository>(),
          ));
  getIt.registerLazySingleton<AppVersionBloc>(() => AppVersionBloc(
        appVersionRepository: getIt<IAppVersionRepository>(),
        categoryRepository: getIt<ICategoryRepository>(),
      ));
  getIt.registerLazySingleton<WaitForConfirmationBloc>(
      () => WaitForConfirmationBloc(
            userRepository: getIt<IUserRepository>(),
          ));
}

void _registerNotificationBlocs() {
  getIt.registerSingletonWithDependencies<NotificationBloc>(
    () => NotificationBloc(
      notificationRepository: getIt<INotificationRepository>(),
      firebaseMessaging: getIt<FirebaseMessaging>(),
    ),
    dependsOn: [FirebaseMessaging],
  );
  getIt.registerSingletonWithDependencies<EventNotificationBloc>(
    () => EventNotificationBloc(
      notificationBloc: getIt<NotificationBloc>(),
      notificationRepository: getIt<INotificationRepository>(),
      firebaseMessaging: getIt<FirebaseMessaging>(),
    ),
    dependsOn: [FirebaseMessaging],
  );
  getIt.registerSingletonWithDependencies<BalanceChangeNotificationBloc>(
    () => BalanceChangeNotificationBloc(
      notificationBloc: getIt<NotificationBloc>(),
      notificationRepository: getIt<INotificationRepository>(),
      firebaseMessaging: getIt<FirebaseMessaging>(),
    ),
    dependsOn: [FirebaseMessaging],
  );
  getIt.registerSingletonWithDependencies<NewsNotificationBloc>(
    () => NewsNotificationBloc(
      notificationBloc: getIt<NotificationBloc>(),
      notificationRepository: getIt<INotificationRepository>(),
      firebaseMessaging: getIt<FirebaseMessaging>(),
    ),
    dependsOn: [FirebaseMessaging],
  );
  getIt.registerSingletonWithDependencies<DiscountNotificationBloc>(
    () => DiscountNotificationBloc(
      notificationBloc: getIt<NotificationBloc>(),
      notificationRepository: getIt<INotificationRepository>(),
      firebaseMessaging: getIt<FirebaseMessaging>(),
    ),
    dependsOn: [FirebaseMessaging],
  );
  getIt.registerSingletonWithDependencies<FcmBloc>(
    () => FcmBloc(
      notificationBloc: getIt<NotificationBloc>(),
      eventNotificationBloc: getIt<EventNotificationBloc>(),
      balanceChangeNotificationBloc: getIt<BalanceChangeNotificationBloc>(),
      discountNotificationBloc: getIt<DiscountNotificationBloc>(),
      newsNotificationBloc: getIt<NewsNotificationBloc>(),
      firebaseMessaging: getIt<FirebaseMessaging>(),
      deviceInfo: getIt<DeviceInfoPlugin>(),
      notificationRepository: getIt<INotificationRepository>(),
      appBloc: getIt<AppBloc>(),
    ),
    dependsOn: [FirebaseMessaging],
  );

  //beneficiary
  getIt.registerLazySingleton<IBeneficiaryRepository>(
    () => BeneficiaryRepository(crmClient: getIt<CrmClient>()),
  );
  getIt.registerLazySingleton<AppStoreClient>(() => AppStoreClient(
        getIt<Dio>(),
        baseUrl: F.appStoreHomeModuleUrl,
      ));
  getIt.registerLazySingleton<IAppStoreRepository>(
    () => AppStoreRepository(appStoreClient: getIt<AppStoreClient>()),
  );
  getIt.registerLazySingleton<IParkingRepository>(
    () => ParkingRepository(
      crmClient: getIt<CrmClient>(),
    ),
  );
}

class _DioDi extends DioDi {}
