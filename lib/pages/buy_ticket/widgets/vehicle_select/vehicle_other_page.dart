import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/bloc/vehicle_other_bloc.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/validator/vehicle_other_page_validator.dart';
import 'package:epass/pages/buy_ticket/widgets/vehicle_select/widgets/vehicle_user_list_card.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class VehicleOtherPage extends StatefulWidget implements AutoRouteWrapper {
  const VehicleOtherPage({Key? key}) : super(key: key);

  @override
  State<VehicleOtherPage> createState() => _VehicleOtherPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (context) => VehicleOtherBloc(
        vehicleRepository: getIt<IVehicleRepository>(),
      ),
      child: this,
    );
  }
}

class _VehicleOtherPageState extends State<VehicleOtherPage>
    with VehicleOtherPageValidator {
  late RefreshController _refreshController;

  var _initialVehicles = <Vehicle>[];
  var _selectedVehicles = <Vehicle>[];

  @override
  void initState() {
    _refreshController = RefreshController(initialRefresh: false);
    _initialVehicles =
        List.from(context.read<TicketVehicleBloc>().state.otherVehicles);
    _selectedVehicles = context.read<TicketVehicleBloc>().state.otherVehicles;
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: BlocBuilder<VehicleOtherBloc, VehicleOtherState>(
        builder: (context, state) {
          return Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 24.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: PrimaryTextField(
                      hintText: 'Nhập chính xác biển số xe',
                      isPlateNumber: true,
                      onChanged: _onSearchChanged,
                      suffix: Assets.icons.search.svg(
                        width: 24.r,
                        height: 24.r,
                        color: ColorName.borderColor,
                      ),
                      isLoading: state is VehicleOtherSearchedInProgress,
                      validator: plateNumberValidator,
                      errorText: state is VehicleOtherSearchedFailure
                          ? state.message
                          : null,
                    ),
                  ),
                  Expanded(
                    child: Builder(
                      builder: (context) {
                        if (_initialVehicles.isEmpty &&
                            state is VehicleOtherSearchedSuccess &&
                            state.vehicles.isEmpty) {
                          return const NoDataPage();
                        }

                        if (state is VehicleOtherSearchedSuccess) {
                          _initialVehicles.addAll(state.vehicles
                              .where((element) =>
                                  !_initialVehicles.contains(element))
                              .toList());
                        }

                        return ListView.builder(
                          physics: const BouncingScrollPhysics(),
                          itemCount: _initialVehicles.length,
                          itemBuilder: (context, index) {
                            final vehicle = _initialVehicles[index];
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.w),
                              child: Padding(
                                padding: EdgeInsets.only(
                                  top: index == 0 ? 8.h : 20.h,
                                  bottom: index == _initialVehicles.length - 1
                                      ? 120.h
                                      : 0,
                                ),
                                child: VehicleUserListCard(
                                  vehicle: vehicle,
                                  isSelected:
                                      _selectedVehicles.contains(vehicle),
                                  onTap: () => _onVehicleTapped(vehicle),
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
              _initialVehicles.isNotEmpty
                  ? Align(
                      alignment: Alignment.bottomCenter,
                      child: AnimatedContainer(
                        duration: const Duration(milliseconds: 500),
                        height: 100.h,
                        color: Colors.white,
                        child: Wrap(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 16.w,
                                vertical: 20.h,
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 2,
                                    blurRadius: 10,
                                    offset: const Offset(
                                        0, -6), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: SizedBox(
                                height: 56.h,
                                width: double.infinity,
                                child: PrimaryButton(
                                  title:
                                      'Chọn xong (${_selectedVehicles.length})',
                                  onTap: _onDoneTap,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : const SizedBox.shrink(),
            ],
          );
        },
      ),
    );
  }

  void _onVehicleTapped(Vehicle vehicle) {
    if (_selectedVehicles.contains(vehicle)) {
      setState(() =>
          _selectedVehicles = List.from(_selectedVehicles)..remove(vehicle));
    } else {
      setState(
          () => _selectedVehicles = List.from(_selectedVehicles)..add(vehicle));
    }
  }

  void _onSearchChanged(String? value) {
    if (plateNumberValidator(value) == null) {
      context.read<VehicleOtherBloc>().add(VehicleOtherSearched(value!));
    }
  }

  void _onDoneTap() {
    context
        .read<TicketVehicleBloc>()
        .add(TicketVehicleOtherChanged(_selectedVehicles));
    context.popRoute();
  }
}
