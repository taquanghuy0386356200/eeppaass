part of 'category_bloc.dart';

abstract class CategoryState extends Equatable {
  const CategoryState();
}

class CategoryInitial extends CategoryState {
  @override
  List<Object> get props => [];
}

class PlateTypeFetchedInProgress extends CategoryState {
  @override
  List<Object> get props => [];
}

class PlateTypeFetchedSuccess extends CategoryState {
  final List<CategoryModel> plateTypes;

  const PlateTypeFetchedSuccess(this.plateTypes);

  @override
  List<Object> get props => [plateTypes];
}

class PlateTypeFetchedFailure extends CategoryState {
  final String message;

  const PlateTypeFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class VehicleTypeFetchedInProgress extends CategoryState {
  const VehicleTypeFetchedInProgress();

  @override
  List<Object> get props => [];
}

class VehicleTypeFetchedSuccess extends CategoryState {
  final List<CategoryModel> vehicleTypes;

  const VehicleTypeFetchedSuccess({required this.vehicleTypes});

  @override
  List<Object> get props => [vehicleTypes];
}

class VehicleTypeFetchedFailure extends CategoryState {
  final String message;

  const VehicleTypeFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CustomerTypeFetchedInProgress extends CategoryState {
  @override
  List<Object> get props => [];
}

class CustomerTypeFetchedSuccess extends CategoryState {
  final List<CustomerTypeModel> customerTypes;

  const CustomerTypeFetchedSuccess(this.customerTypes);

  @override
  List<Object?> get props => [customerTypes];
}

class CustomerTypeFetchedFailure extends CategoryState {
  final String message;

  const CustomerTypeFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}
