part of 'create_report_otp_bloc.dart';

abstract class CreateReportOtpEvent extends Equatable {
  const CreateReportOtpEvent();
}

class CreateReportOtpRequested extends CreateReportOtpEvent {
  const CreateReportOtpRequested();

  @override
  List<Object> get props => [];
}

class ReportOtpPaused extends CreateReportOtpEvent {
  const ReportOtpPaused();

  @override
  List<Object> get props => [];
}
