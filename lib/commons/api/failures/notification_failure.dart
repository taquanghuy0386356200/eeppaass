import 'package:epass/commons/api/failures/failures.dart';

class RequestNotificationDenied extends Failure {
  const RequestNotificationDenied()
      : super(message: 'Notification permission not granted');
}

class RequestNotificationNotDetermined extends Failure {
  const RequestNotificationNotDetermined()
      : super(message: 'Notification permission not determined');
}
