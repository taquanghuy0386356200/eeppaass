import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/station/station_stage.dart';
import 'package:epass/commons/repo/station_repository.dart';
import 'package:equatable/equatable.dart';

part 'station_stage_dropdown_event.dart';

part 'station_stage_dropdown_state.dart';

class StationStageDropdownBloc
    extends Bloc<StationStageDropdownEvent, StationStageDropdownState> {
  final IStationRepository _stationRepository;

  StationStageDropdownBloc({
    required IStationRepository stationRepository,
  })  : _stationRepository = stationRepository,
        super(StationStageDropdownInitial()) {
    on<StationStageDropdownFetched>(_onStationStageFetched);
  }

  FutureOr<void> _onStationStageFetched(event, emit) async {
    emit(const StationStageDropdownFetchedInProgress());

    final result = await _stationRepository.getStationStages();

    result.when(
      success: (success) =>
          emit(StationStageDropdownFetchedSuccess(data: success.listData)),
      failure: (failure) =>
          emit(StationStageDropdownFetchedFailure(failure.message ?? '')),
    );
  }
}
