class Constant {
  static String hotline = '19009080';
  static String encryptedUsernameKey = 'EPASS_USERNAME';

  static String encryptedPasswordKey(String input) => 'EPASS_${input}_ENCRYPTED_PASSWORD';
  static String csMail = 'cskh@vdtc.com.vn';

  static int blockMonthDays = 29;
  static int blockQuarterDays = 89;

  /// TABLE_NAME
  static String smsRegisPersonal = 'KHCN_SMS_REGIS';
  static String smsRegisEnterprise = 'KHDN_SMS_REGIS';

  /// PENALTY URL
  static String coolPenaltyUrl = 'https://www.csgt.vn/m/tra-cuu-phuong-tien-vi-pham.html';

  ///NEWS_MODULE_URL
  static String ePassNewsUrl = 'https://epass-vdtc.com.vn/tin-tuc-su-kien/';
  /// STATION VEHICLE ALERT
  static int distanceRadius = 500;

  /// FACEBOOK FANPAGE
  static String facebookPageName = 'epass.vdtc';
  static String facebookPageId = '102818478380265';

  /// OTP EXPIRE TIME (in second)
  static int otpExpireTime = 300;

  static String ANDROID = 'ANDROID';
  static String IOS = 'IOS';

  static List<String> allowedExtensions = [
    'jpg',
    'png',
    'tiff',
    'bmp',
    'pdf',
    'jpeg',
    'webp',
    'rar',
    'zip',
    'xlsx',
    'xls',
    'txt',
    'doc',
    'docx',
  ];
  static int isDebitVehicle = 1;
  static int openETAG =21;


}
