import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:epass/commons/enum/cash_in_for.dart';
import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/bloc_transforms.dart';
import 'package:epass/pages/cash_in/validators/cash_in_page_validator.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'cash_in_target_event.dart';
part 'cash_in_target_state.dart';

class CashInTargetBloc extends Bloc<CashInTargetEvent, CashInTargetState>
    with CashInPageValidators {
  final AppBloc _appBloc;
  final ICashInRepository _cashInRepository;

  final contractNoStream = StreamController<String>();
  CashInTargetBloc({
    required AppBloc appBloc,
    required ICashInRepository cashInRepository,
    Beneficiary? beneficary,
  })  : _appBloc = appBloc,
        _cashInRepository = cashInRepository,
        super(CashInTargetState(
          cashInFor: beneficary != null ? CashInFor.other : CashInFor.me,
          contractNo: beneficary != null
              ? beneficary.contractNo
              : appBloc.state.user?.contractNo,
          contractId: beneficary != null
              ? beneficary.contractId.toString()
              : appBloc.state.user?.contractId,
          target: (beneficary != null
                  ? beneficary.contractNo
                  : appBloc.state.user?.contractNo) ??
              "",
          userName: beneficary != null
              ? beneficary.customerName
              : appBloc.state.user?.userName,
        )) {
    
    on<CashInForChanged>(_onCashInForChanged);

    on<CashInTargetValidated>(
      _onCashInTargetValidate,
      transformer: throttleDroppable(const Duration(milliseconds: 100)),
    );
  }

  FutureOr<void> _onCashInTargetValidate(event, emit) async {
    if (state.cashInFor == CashInFor.other) {
      // Validate input: event.target
      final error = contractNoOrPlateNoValidator(event.target);
      if (error != null) return;

      // Emit loading
      emit(state.copyWith(isLoading: true));

      // Fetch user contract no.
      final result = await _cashInRepository.getContractsInfo(
        inputSearch: event.target.trim(),
      );

      // Emit success or failure
      result.when(
        success: (contractsInfo) {
          if (contractsInfo.isEmpty) {
            emit(state.copyWith(
              isLoading: false,
              target: event.target,
              contractNo: null,
              userName: null,
              error: 'Số hợp đồng/Biển số xe không hợp lệ',
            ));
          } else {
            emit(state.copyWith(
              isLoading: false,
              target: event.target,
              contractNo: contractsInfo.first.contractNo,
              contractId: contractsInfo.first.contractId?.toString() ?? '',
              userName: contractsInfo.first.custName,
              error: null,
            ));
          }
        },
        failure: (failure) {
          emit(state.copyWith(
            isLoading: false,
            target: event.target,
            contractNo: null,
            userName: null,
            error: failure.message ?? '',
          ));
        },
      );
    }
  }

  FutureOr<void> _onCashInForChanged(event, emit) {
    if (event.cashInFor == CashInFor.me) {
      emit(CashInTargetState(
        cashInFor: CashInFor.me,
        contractNo: _appBloc.state.user?.contractNo,
        contractId: _appBloc.state.user?.contractId,
        target: _appBloc.state.user?.contractNo ?? '',
        userName: _appBloc.state.user?.userName,
      ));
    } else {
      emit(const CashInTargetState(cashInFor: CashInFor.other, target: ''));
    }
  }
}
