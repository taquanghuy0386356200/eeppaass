import 'dart:collection';
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:epass/commons/services/vnpt/vnpt_create_order.dart';
import 'package:epass/flavors.dart';
import 'package:jiffy/jiffy.dart';

class VNPTRequest {
  final String action = "INIT";

  //contractId
  final String contractId;

  final String version = '1.0.5';

  // Mã dịch vụ của merchant
  final int merchant_service_id = VnptConfig.MERCHANT_SERVICE_ID;

  // Mã đơn hàng
  final int merchant_order_id = DateTime.now().millisecondsSinceEpoch;

  // Số tiền thanh toán
  final int? amount;

  // Số tiền thanh toán
  final int amount_detail = 0;

  // Mã dịch vụ - Mặc định = 1
  final int service_id = 1;

  // Mã loại hình dịch vụ
  //  1: Dịch vụ thông thường
  //  2: Dịch vụ công
  //  3: Dịch vụ tiện ích
  final int service_category = 1;

  // Mã thiết bị, mặc định = 1
  final int device = 1;

  // Ngôn ngữ: vi-VN/en-US; mặc định: vi-VN
  final String locale = "vi-VN";

  // Mã tiền tệ: VND hoặc USD
  final String currency_code = "VND";

  // Phương thức thanh toán, mặc định: VNPTPAY
  final String payment_methods = "VNPTPAY";

  // Phương thức thanh toán, mặc định: VNPTPAY
  final String description = "Thanh toan don hang";

  // Thời gian giao dịch, định dạng yyyyMMddHHmmss
  final String ceateDate = Jiffy().format('yyyyMMddHHmmss');

  // IP của khách hàng
  final String client_ip;

  String get secureCode {
    // ignore: prefer_interpolation_to_compose_strings
    String combinedParams = "" +
        action +
        "|" +
        version +
        "|" +
        merchant_service_id.toString() +
        "|" +
        merchant_order_id.toString() +
        "|" +
        amount.toString() +
        "|" +
        amount.toString() +
        "|" +
        service_id.toString() +
        "|" +
        service_category.toString() +
        "|" +
        device.toString() +
        "|" +
        locale +
        "|" +
        currency_code +
        "|" +
        payment_methods +
        "|" +
        description +
        "|" +
        ceateDate +
        "|" +
        client_ip +
        "|" +
        VnptConfig.SECRET_KEY;
    final hash = sha256.convert(utf8.encode(combinedParams));
    return hash.toString();
  }

  VNPTRequest({
    required this.contractId,
    required this.amount,
    required this.client_ip,
  });
}
