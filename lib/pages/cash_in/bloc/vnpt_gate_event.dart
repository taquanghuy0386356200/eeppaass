part of 'vnpt_gate_bloc.dart';

abstract class VnptGateEvent extends Equatable {
  const VnptGateEvent();
}

class VnptPaymentProceeded extends VnptGateEvent {
  final int amount;
  final String client_ip;
  const VnptPaymentProceeded({required this.amount, required this.client_ip});

  @override
  List<Object> get props => [];
}

class VnptPaymentsSuccess extends VnptGateEvent {
  final String? responseCode;
  final String transactionId;
  final String? secureCode;
  const VnptPaymentsSuccess(
      {required this.responseCode,
      required this.transactionId,
      required this.secureCode});

  @override
  List<Object> get props => [];
}

class VnptPaymentsFailure extends VnptGateState {
  final String message;

  const VnptPaymentsFailure(this.message);

  @override
  List<Object> get props => [message];
}
