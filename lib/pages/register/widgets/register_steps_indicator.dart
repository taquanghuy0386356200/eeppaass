import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:steps_indicator/steps_indicator.dart';

class RegisterStepsIndicator extends StatelessWidget {
  final int step;
  final double? lineLength;

  const RegisterStepsIndicator({
    Key? key,
    required this.step,
    required this.lineLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StepsIndicator(
      selectedStep: step - 1,
      nbSteps: 5,
      lineLength: lineLength ?? 40,
      doneLineColor: ColorName.primaryColor,
      doneLineThickness: 2.0,
      doneStepColor: ColorName.primaryColor,
      doneStepSize: 12.r,
      undoneLineColor: ColorName.textGray6,
      undoneLineThickness: 2.0,
      unselectedStepWidget: Container(
        height: 12.r,
        width: 12.r,
        decoration: const BoxDecoration(color: ColorName.textGray6, shape: BoxShape.circle),
      ),
      selectedStepWidget: Container(
        width: 24.r,
        height: 24.r,
        decoration: const BoxDecoration(
          color: ColorName.disabledBorderColor,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Text(
            '$step',
            style: Theme.of(context).textTheme.caption!.copyWith(
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}