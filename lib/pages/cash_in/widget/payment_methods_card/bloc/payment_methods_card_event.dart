part of 'payment_methods_card_bloc.dart';

@immutable
abstract class PaymentMethodsCardEvent extends Equatable {
  const PaymentMethodsCardEvent();

  @override
  List<Object?> get props => [];
}

class PaymentMethodMBCheckNewest extends PaymentMethodsCardEvent {}

class PaymentMethodsReset extends PaymentMethodsCardEvent {}

class PaymentMethodsFetched extends PaymentMethodsCardEvent {}

class PaymentMethodChanged extends PaymentMethodsCardEvent {
  final TopupChannel selectedPaymentMethod;

  const PaymentMethodChanged(this.selectedPaymentMethod);
}

class PaymentMethodsLastChange extends PaymentMethodsCardEvent {}