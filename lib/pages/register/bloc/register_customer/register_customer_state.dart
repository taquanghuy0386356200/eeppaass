part of 'register_customer_bloc.dart';

class RegisterCustomerState extends Equatable {
  final CustomerTypeModel? customerType;
  final String? idImageFrontPath;
  final String? idImageBackPath;
  final String? name;
  final DateTime? dob;
  final Gender? gender;
  final String? documentNumber;
  final String? placeOfIssue;
  final DateTime? dateOfIssue;
  final String? email;
  final String? phoneNumber;
  final String? address;
  final AddressVTT? province;
  final AddressVTT? district;
  final AddressVTT? ward;

  const RegisterCustomerState({
    this.customerType,
    this.idImageFrontPath,
    this.idImageBackPath,
    this.name,
    this.dob,
    this.gender,
    this.documentNumber,
    this.placeOfIssue,
    this.dateOfIssue,
    this.email,
    this.phoneNumber,
    this.address,
    this.province,
    this.district,
    this.ward,
  });

  @override
  List<Object?> get props => [
        customerType,
        idImageFrontPath,
        idImageBackPath,
        name,
        dob,
        gender,
        documentNumber,
        placeOfIssue,
        dateOfIssue,
        email,
        phoneNumber,
        address,
        province,
        district,
        ward,
      ];

  RegisterCustomerState copyWith({
    CustomerTypeModel? customerType,
    String? idImageFrontPath,
    String? idImageBackPath,
    String? name,
    DateTime? dob,
    Gender? gender,
    String? documentNumber,
    String? placeOfIssue,
    DateTime? dateOfIssue,
    String? email,
    String? phoneNumber,
    String? address,
    required AddressVTT? province,
    required AddressVTT? district,
    required AddressVTT? ward,
  }) {
    return RegisterCustomerState(
      customerType: customerType ?? this.customerType,
      idImageFrontPath: idImageFrontPath ?? this.idImageFrontPath,
      idImageBackPath: idImageBackPath ?? this.idImageBackPath,
      name: name ?? this.name,
      dob: dob ?? this.dob,
      gender: gender ?? this.gender,
      documentNumber: documentNumber ?? this.documentNumber,
      placeOfIssue: placeOfIssue ?? this.placeOfIssue,
      dateOfIssue: dateOfIssue ?? this.dateOfIssue,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      address: address ?? this.address,
      province: province,
      district: district,
      ward: ward,
    );
  }
}
