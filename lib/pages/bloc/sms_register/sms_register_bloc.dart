import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/user_sms_vehicle_setting.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'sms_register_event.dart';

part 'sms_register_state.dart';

class SmsRegisterBloc extends Bloc<SmsRegisterEvent, SmsRegisterTotalState> {
  final IUserRepository _userRepository;
  final ICategoryRepository _categoryRepository;
  final AppBloc _appBloc;

  SmsRegisterBloc({
    required IUserRepository userRepository,
    required ICategoryRepository categoryRepository,
    required AppBloc appBloc,
  })  : _userRepository = userRepository,
        _categoryRepository = categoryRepository,
        _appBloc = appBloc,
        super(const SmsRegisterTotalState()) {
    on<CheckUserRegister>(_checkUserRegister);
    on<CheckUserNotRegisterYet>(_checkUserNotRegisterYet);
    on<SMSVehicleRegistered>(_smsRegistered);
    on<SMSVehicleRegisterUpdated>(_onSMSVehicleUpdated);
    on<SmsRegisterDispose>(_dispose);
    on<SelectedSMSOption>(_onSelectedSMSOption);
  }

  FutureOr<void> _dispose(event, emit) {
    emit(state.copyWith(
      message: '',
      messagePopup: '',
      isLoading: false,
      successRegister: false,
    ));
  }

  FutureOr<void> _smsRegistered(
    SMSVehicleRegistered event,
    emit,
  ) async {
    emit(state.copyWith(isLoading: true));

    final result = await _userRepository.registerSMSVehicle(
      code: event.smsRegisCode,
      autoRenew: event.autoRenew,
    );

    result.when(
      success: (success) => emit(
        state.copyWith(
          isLoading: false,
          successRegister: true,
          message: '',
          isRegister: true,
          messagePopup: '',
        ),
      ),
      failure: (failure) => emit(
        state.copyWith(
          isLoading: false,
          successRegister: false,
          message: '',
          isRegister: false,
          messagePopup: failure.message,
        ),
      ),
    );
  }

  //Check when isRegister = false && message = ''
  FutureOr<void> _checkUserNotRegisterYet(
    CheckUserNotRegisterYet event,
    emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
    ));

    final customerTypeId = _appBloc.state.user?.cusTypeId;
    final bool isPersonal =
        customerTypeId == 1 || customerTypeId == 7; // 1: domestic, 7: abroad

    final result = await _categoryRepository.getCategories(
      tableName:
          isPersonal ? Constant.smsRegisPersonal : Constant.smsRegisEnterprise,
    );
    result.when(
      success: (response) {
        final CategoryModel selectedSMSOption = response.listData.firstWhere(
            (data) => data.isDefault == '1',
            orElse: () => const CategoryModel());

        emit(
          state.copyWith(
              smsCategories: response.listData,
              isLoading: false,
              message: '',
              userSMSVehicleSetting: null,
              isRegister: false,
              selectedSMSOption: selectedSMSOption),
        );
      },
      failure: (failure) {
        emit(
          state.copyWith(
            smsCategories: [],
            selectedSMSOption: null,
            isLoading: false,
            message: failure.message,
          ),
        );
      },
    );
  }

  FutureOr<void> _checkUserRegister(event, emit) async {
    emit(
      state.copyWith(isLoading: true),
    );
    final result = await _userRepository.getUserSMSVehicle();
    result.when(success: (success) {
      emit(
        state.copyWith(
            userSMSVehicleSetting: success,
            isRegister: true,
            isLoading: false,
            isExtendTime: success.autoRenew == '1'),
      );
    }, failure: (failure) {
      if ((failure.message ?? '') == 'Không có dữ liệu thỏa mãn') {
        add(const CheckUserNotRegisterYet());
      } else {
        emit(
          state.copyWith(
            userSMSVehicleSetting: null,
            isRegister: false,
            isLoading: false,
            message: failure.message ?? 'Có lỗi xảy ra',
          ),
        );
      }
    });
  }

  FutureOr<void> _onSMSVehicleUpdated(
    SMSVehicleRegisterUpdated event,
    emit,
  ) async {
    emit(
      state.copyWith(
        isLoading: true,
      ),
    );

    final result = await _userRepository.updateSMSVehicle(
      status: event.status,
      autoRenew: event.autoRenew,
    );

    result.when(
      success: (success) => emit(state.copyWith(
        messagePopup: '',
        message: '',
        isExtendTime: event.autoRenew ? true : false,
        isLoading: false,
      )),
      failure: (failure) => emit(
        state.copyWith(
          isLoading: false,
          message: failure.message,
        ),
      ),
    );
  }

  FutureOr<void> _onSelectedSMSOption(
    SelectedSMSOption event,
    emit,
  ) async {
    emit(
      state.copyWith(
          selectedSMSOption: event.selectedSMSOption),
    );
  }
}

///
/// https://backend.epass-vdtc.com.vn/dmdc2/api/v1/categories?table_name=KHCN_SMS_REGIS
/// api lấy thông tin khi khác hàng chưa đăng ký
///
