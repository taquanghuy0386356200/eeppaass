import 'package:epass/commons/enum/boo_code.dart';
import 'package:epass/commons/enum/method_charge.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:json_annotation/json_annotation.dart';

/// price : 100000
/// plateNumber : ""
/// quantity : 1
/// vehicleId : 123
/// vehiclesGroupId : 123
/// effDate : ""
/// expDate : ""
/// autoRenew : 0
/// chargeMethodId : 0
/// booCode : ""
/// stationType : 0
/// laneOut : ""
/// servicePlanTypeId : 0
/// offerId : ""
/// offerLevel : 2
/// seatNumber : 1
/// vehicleTypeId : 1
/// cargoWeight : 1
/// netWeight : 1
/// epc : ""
/// stageId : 1
/// stageName : ""
/// stationId : 1
/// stationName : ""

part 'buy_ticket_request.g.dart';

@JsonSerializable(includeIfNull: false)
class BuyTicketRequestItem {
  BuyTicketRequestItem({
    this.price,
    // REMOVE T,X,V BEFORE SET VALUE
    this.plateNumber,
    this.quantity = 1,
    this.vehicleId,
    this.vehiclesGroupId,
    this.effDate,
    this.expDate,
    this.autoRenew = 0,
    this.chargeMethodId,
    this.booCode,
    this.stationType,
    this.servicePlanTypeId,
    this.seatNumber,
    this.vehicleTypeId,
    this.cargoWeight,
    this.netWeight,
    this.epc,
    this.stageId,
    this.stageName,
    this.stationId,
    this.stationName,
  });

  final int? price;
  final String? plateNumber;
  final int? quantity;
  final int? vehicleId;
  final int? vehiclesGroupId;
  final String? effDate;
  final String? expDate;
  final int? autoRenew;
  final MethodChargeInt? chargeMethodId;
  final BooCode? booCode;
  final int? stationType;
  final ServicePlanType? servicePlanTypeId;
  final int? seatNumber;
  final int? vehicleTypeId;
  final int? cargoWeight;
  final int? netWeight;
  final String? epc;
  final int? stageId;
  final String? stageName;
  final int? stationId;
  final String? stationName;

  Map<String, dynamic> toJson() => _$BuyTicketRequestItemToJson(this);
}

class BuyTicketRequest {
  final String acountETC;
  final int amount;
  final int actTypeId;
  final int quantity;
  final List<BuyTicketRequestItem> list;

  const BuyTicketRequest({
    this.acountETC = 'true',
    required this.amount,
    this.actTypeId = 24,
    required this.quantity,
    required this.list,
  });

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['acountETC'] = acountETC;
    map['amount'] = amount;
    map['actTypeId'] = actTypeId;
    map['quantity'] = quantity;
    map['list'] = list.map((v) => v.toJson()).toList();
    return map;
  }
}
