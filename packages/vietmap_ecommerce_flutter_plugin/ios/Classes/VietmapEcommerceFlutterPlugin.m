#import "VietmapEcommerceFlutterPlugin.h"
#if __has_include(<vietmap_ecommerce_flutter_plugin/vietmap_ecommerce_flutter_plugin-Swift.h>)
#import <vietmap_ecommerce_flutter_plugin/vietmap_ecommerce_flutter_plugin-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "vietmap_ecommerce_flutter_plugin-Swift.h"
#endif

@implementation VietmapEcommerceFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftVietmapEcommerceFlutterPlugin registerWithRegistrar:registrar];
}
@end
