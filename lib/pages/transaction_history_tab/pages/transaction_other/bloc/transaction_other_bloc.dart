import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/transaction_other/transaction_other.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'transaction_other_event.dart';
part 'transaction_other_state.dart';

class TransactionOtherBloc
    extends Bloc<TransactionOtherEvent, TransactionOtherState> {
  final ITransactionHistoryRepository _transRepository;

  final int _pageSize;
  int _startRecord = 0;
  bool isSearching = false;

  TransactionOtherBloc({
    required ITransactionHistoryRepository transactionHistoryRepository,
    int pageSize = 10,
  })  : _pageSize = pageSize,
        _transRepository = transactionHistoryRepository,
        super(const TransactionOtherState(listData: [])) {
    on<TransactionOtherFetched>(_onTransactionOtherFetched);
    on<TransactionOtherRefreshed>(_onTransactionOtherRefreshed);
    on<TransactionOtherLoadMore>(_onTransactionOtherLoadMore);
    on<TransactionOtherFromDateChanged>(_onTransactionOtherFromDateChanged);
    on<TransactionOtherToDateChanged>(_onTransactionOtherToDateChanged);
    on<TransactionOtherSearched>(_onTransactionOtherSearched);
  }

  FutureOr<void> _onTransactionOtherFetched(
    TransactionOtherFetched event,
    Emitter<TransactionOtherState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      plateNumber: null,
      fromDate: null,
      toDate: null,
      error: null,
    ));

    final result = await _getTransactionOther();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: null,
            fromDate: null,
            toDate: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: null,
        fromDate: null,
        toDate: null,
      )),
    );
  }

  FutureOr<void> _onTransactionOtherRefreshed(
    TransactionOtherRefreshed event,
    Emitter<TransactionOtherState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      plateNumber: state.plateNumber,
      fromDate: state.fromDate,
      toDate: state.toDate,
      error: null,
    ));

    final result = await _getTransactionOther();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            fromDate: state.fromDate,
            toDate: state.toDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        fromDate: state.fromDate,
        toDate: state.toDate,
      )),
    );
  }

  FutureOr<void> _onTransactionOtherLoadMore(
    TransactionOtherLoadMore event,
    Emitter<TransactionOtherState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      plateNumber: state.plateNumber,
      fromDate: state.fromDate,
      toDate: state.toDate,
      error: null,
    ));

    final result = await _getTransactionOther();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(
          state.copyWith(
            isLoadingMore: false,
            listData: listData,
            isFull: listData.length == data.count,
            plateNumber: state.plateNumber,
            fromDate: state.fromDate,
            toDate: state.toDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        fromDate: state.fromDate,
        toDate: state.toDate,
      )),
    );
  }

  FutureOr<void> _onTransactionOtherSearched(
    TransactionOtherSearched event,
    Emitter<TransactionOtherState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      plateNumber: event.plateNumber,
      fromDate: event.fromDate,
      toDate: event.toDate,
      error: null,
    ));

    final result = await _getTransactionOther();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            fromDate: state.fromDate,
            toDate: state.toDate,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        fromDate: state.fromDate,
        toDate: state.toDate,
      )),
    );
  }

  Future<Result<TransactionOtherDataNotNull, Failure>>
      _getTransactionOther() async {
    final result = await _transRepository.getTransactionOther(
      pageSize: _pageSize,
      startRecord: _startRecord,
      plateNumber: state.plateNumber,
      fromDate: state.fromDate != null
          ? Jiffy(state.fromDate).format('dd/MM/yyyy')
          : null,
      toDate: state.toDate != null
          ? Jiffy(state.toDate).format('dd/MM/yyyy')
          : null,
    );
    return result;
  }

  FutureOr<void> _onTransactionOtherFromDateChanged(
    TransactionOtherFromDateChanged event,
    Emitter<TransactionOtherState> emit,
  ) {
    final fromDateStr = event.fromDate;
    DateTime? fromDate;
    if (fromDateStr?.isNotEmpty ?? false) {
      try {
        fromDate = Jiffy(fromDateStr, 'dd/MM/yyyy').dateTime;
      } on Exception {
        fromDate = null;
      }
    }
    emit(state.copyWith(
      plateNumber: state.plateNumber,
      fromDate: fromDate,
      toDate: state.toDate,
      error: state.error,
    ));
  }

  FutureOr<void> _onTransactionOtherToDateChanged(
    TransactionOtherToDateChanged event,
    Emitter<TransactionOtherState> emit,
  ) {
    final toDateStr = event.toDate;
    DateTime? toDate;
    if (toDateStr?.isNotEmpty ?? false) {
      try {
        toDate = Jiffy(toDateStr, 'dd/MM/yyyy').dateTime;
      } on Exception {
        toDate = null;
      }
    }
    emit(state.copyWith(
      plateNumber: state.plateNumber,
      fromDate: state.fromDate,
      toDate: toDate,
      error: state.error,
    ));
  }
}
