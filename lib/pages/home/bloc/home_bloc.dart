import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/locals/prefs_service.dart';
import 'package:epass/commons/models/campaign/campaign.dart';
import 'package:epass/commons/models/contract_payment/contract_payment.dart';
import 'package:epass/commons/models/waiting_order/waiting_order.dart';
import 'package:epass/commons/repo/campaign_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home/model/apps_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:simple_result/simple_result.dart';
import 'package:collection/collection.dart';

import '../../../commons/repo/app_store_repository.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final IUserRepository _userRepository;
  final ICampaignRepository _campaignRepository;
  final AppBloc _appBloc;
  final IAppStoreRepository _appStoreRepository;

  HomeBloc({
    required IUserRepository userRepository,
    required ICampaignRepository campaignRepository,
    required AppBloc appBloc,
    required IAppStoreRepository iAppStoreRepository,
  })  : _userRepository = userRepository,
        _campaignRepository = campaignRepository,
        _appBloc = appBloc,
        _appStoreRepository = iAppStoreRepository,
        super(HomeInitial()) {
    // HomeFetched
    on<HomeFetched>(_onHomeFetched);
    on<ContractPaymentFetched>(_onContractPaymentFetched);
    on<WaitingOrderFetched>(_onWaitingOrderFetched);
    on<SendConfirmOrderRequest>(_onSendConfirmOrderRequest);
  }

  Future<bool> checkUserRegister() async {
    bool isRegister = false;
    final result = await _userRepository.getUserSMSVehicle();
    result.when(success: (success) {
      isRegister = true;
    }, failure: (failure) {
      if ((failure.message ?? '') == 'Không có dữ liệu thỏa mãn') {
        isRegister = false;
      } else {
        isRegister = false;
      }
    });
    return isRegister;
  }

  Future<bool> checkShowPopupSMS() async {
    int lastShowDay = await PrefsService.getDateSavePopupSMS();
    int today = DateTime.now().day;
    if (lastShowDay != today) {
      await PrefsService.saveLastShowSMS(today);
      return true;
    } else {
      return false;
    }
  }

  FutureOr<void> _onHomeFetched(
      HomeFetched event, Emitter<HomeState> emit) async {
    final contractId = _appBloc.state.user?.contractId;

    emit(HomeLoading());
    bool checkShowSpam = await checkShowPopupSMS();
    List<Future<Result<dynamic, Failure>>> futures = [];

    // get contract payment for viettel pay
    if (contractId != null) {
      futures.add(_userRepository.getContractPayment(contractId));
    }

    // get banners and popup
    futures.add(_campaignRepository.getCampaigns());

    //get appsStore
    futures.add(_appStoreRepository.getAppsStore());

    bool isRegister = await checkUserRegister();

    final responses = await Future.wait(futures);

    Campaign? popup;
    Campaign? popupSMS;
    List<Campaign>? banners;
    ContractPayment? contractPayment;
    List<String> errors = [];
    AppsModel? appsStore;

    for (var response in responses) {
      response.when(success: (success) {
        switch (success.runtimeType) {
          case ContractPayment:
            contractPayment = success as ContractPayment;
            break;
          case CampaignData:
            final listData = (success as CampaignData).listData;
            if (listData != null && listData.isNotEmpty) {
              popup = listData.firstWhereOrNull(
                (element) => element.type == CampaignType.popup,
              );

              popupSMS = listData.firstWhereOrNull(
                  (element) => element.type == CampaignType.popupSMS);

              banners = listData
                  .where((element) => element.type == CampaignType.banner)
                  .toList();
            }
            break;
          case AppsModel:
            appsStore = success;
            break;
        }
      }, failure: (failure) {
        errors.add(failure.message ?? '');
      });
    }

    emit(
      HomeLoaded(
        checkUserRegisterSMS: isRegister,
        contractPayment: contractPayment,
        banners: banners,
        popup: popup,
        popupSMS: popupSMS,
        errors: errors,
        checkShowSpamSMS: checkShowSpam,
        appsStore: appsStore,
      ),
    );

    add(WaitingOrderFetched());
  }

  FutureOr<void> _onContractPaymentFetched(
      ContractPaymentFetched event, Emitter<HomeState> emit) async {
    if (state is HomeLoaded) {
      final contractId = _appBloc.state.user?.contractId;

      final currentState = state as HomeLoaded;

      emit(HomeLoading());

      if (contractId != null) {
        final result = await _userRepository.getContractPayment(contractId);

        result.when(
          success: (success) {
            emit(currentState.copyWith(contractPayment: success));
          },
          failure: (failure) => emit(currentState),
        );
      }
    }
  }

  FutureOr<void> _onWaitingOrderFetched(
      WaitingOrderFetched event, Emitter<HomeState> emit) async {
    if (state is HomeLoaded) {
      final currentState = state as HomeLoaded;
      final result = await _userRepository.getWaitingOrder(serviceId: 253);
      result.when(
        success: (success) {
          if (success != null && success.isNotEmpty) {
            emit(currentState.copyWith(waitingOrders: success));
          }
        },
        failure: (failure) {
          print(failure.message);
        },
      );
    }
  }

  FutureOr<void> _onSendConfirmOrderRequest(
      SendConfirmOrderRequest event, Emitter<HomeState> emit) async {
    if (state is HomeLoaded) {
      final currentState = state as HomeLoaded;
      emit(currentState.copyWith(isLoading: true));
      final result = await _userRepository.confirmWaitingRequest(
          waitingOrders: event.waitingOrders);
      result.when(
        success: (success) {
          emit(currentState.copyWith(isConfirmOrderSuccess: true));
        },
        failure: (failure) =>
            emit(currentState.copyWith(isConfirmOrderSuccess: false)),
      );
      add(WaitingOrderFetched());
    }
  }
}
