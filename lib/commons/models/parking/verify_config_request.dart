
import 'package:json_annotation/json_annotation.dart';

part 'verify_config_request.g.dart';

@JsonSerializable(explicitToJson: true)
class VerifyConfigRequest {
  final List<int>? listServiceId;

  VerifyConfigRequest({
    this.listServiceId
  });

  Map<String, dynamic> toJson() => _$VerifyConfigRequestToJson(this);
}