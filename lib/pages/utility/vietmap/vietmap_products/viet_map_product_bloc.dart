import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/contract_topup/contract_topup.dart';
import 'package:epass/commons/models/create_vietmap_transaction/create_vietmap_transaction_request.dart';
import 'package:epass/commons/models/update_vietmap_transaction/update_vietmap_transaction_request.dart';
import 'package:epass/commons/models/vietmap/vietmap_product.dart';
import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/repo/vietmap_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'viet_map_product_event.dart';
part 'viet_map_product_state.dart';

class VietMapProductBloc
    extends Bloc<VietMapProductEvent, VietMapProductState> {
  final IVietMapRepository _vietMapRepository;
  final IUserRepository _userRepository;

  VietMapProductBloc({
    required IVietMapRepository vietMapRepository,
    required IUserRepository userRepository,
  }):_vietMapRepository = vietMapRepository,
        _userRepository = userRepository,
        super(const VietMapProductState(listData: [])) {
    on<VietMapProductFetched>(_onVietMapProductFetched);
    on<CreateVietMapTransaction>(onCreateVietMapTransaction);
    on<UpdateVietMapTransaction>(_onUpdateVietMapTransaction);
  }

  FutureOr<void> _onVietMapProductFetched(
  VietMapProductFetched event,
    Emitter<VietMapProductState> emit,
  ) async {

    emit(state.copyWith(
      isLoading: true,
      error: null,
    ));

    final result = await _vietMapRepository.getVietMapProducts();

    result.when(
      success: (data) {
        emit(
          state.copyWith(
            isLoading: false,
            listData: data.results,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message
      )),
    );
  }

  FutureOr<void> onCreateVietMapTransaction(CreateVietMapTransaction event, Emitter<VietMapProductState> emit) async{
    final request = CreateVietMapTransactionRequest(
        refTransactionId: event.transactionId,
        expireDate: event.expiredDate,
        price: event.price);
    createVietMapTransaction(request);
  }

  FutureOr<void> createVietMapTransaction(CreateVietMapTransactionRequest request) async{
    final response = await _userRepository.createVietMapTransaction(request: request);
    response.when(
      success: (response) {

      },
      failure: (failure) {

      },
    );
  }

  FutureOr<void> _onUpdateVietMapTransaction(UpdateVietMapTransaction event, Emitter<VietMapProductState> emit) async{
    final request = UpdateVietMapTransactionRequest(
        saleOrderCode: event.transactionId);
    emit(state.copyWith(
      isLoading: true,
      error: null,
    ));
    final response = await _userRepository.updateVietMapTransaction(request: request);
    response.when(
      success: (response) {
        emit(
          state.copyWith(
            isLoading: false,
            error: null,
            isPaymentSuccess: true
          ),
        );
      },
      failure: (failure) {
        emit(
          state.copyWith(
              isLoading: false,
              error: null,
              isPaymentSuccess: false
          ),
        );
      },
    );
  }
}
