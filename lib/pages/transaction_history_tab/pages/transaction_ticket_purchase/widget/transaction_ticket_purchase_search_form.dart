import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_ticket_purchase/bloc/transaction_ticket_purchase_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionTicketPurchaseSearchForm extends StatefulWidget {
  final String? plateNumber;
  final int efficiencyId;
  final DateTime? saleTransDateFrom;
  final DateTime? saleTransDateTo;

  const TransactionTicketPurchaseSearchForm({
    Key? key,
    this.plateNumber,
    required this.efficiencyId,
    this.saleTransDateFrom,
    this.saleTransDateTo,
  }) : super(key: key);

  @override
  State<TransactionTicketPurchaseSearchForm> createState() =>
      _TransactionTicketPurchaseSearchFormState();
}

class _TransactionTicketPurchaseSearchFormState
    extends State<TransactionTicketPurchaseSearchForm> {
  final _plateNumberController = TextEditingController();
  final _saleTransDateFromController = TextEditingController();
  final _saleTransDateToController = TextEditingController();

  int _efficiencyId = 1;

  DateTime? _saleTransDateFrom;
  DateTime? _saleTransDateTo;

  @override
  void initState() {
    super.initState();
    context.read<TransactionTicketPurchaseBloc>().isSearching = false;
    _plateNumberController.text = widget.plateNumber ?? '';
    _efficiencyId = widget.efficiencyId;
    _saleTransDateFrom = widget.saleTransDateFrom;
    _saleTransDateTo = widget.saleTransDateTo;

    if (_saleTransDateFrom != null) {
      _saleTransDateFromController.text =
          Jiffy(_saleTransDateFrom).format('dd/MM/yyyy');
    }

    if (_saleTransDateTo != null) {
      _saleTransDateToController.text =
          Jiffy(_saleTransDateTo).format('dd/MM/yyyy');
    }
  }

  @override
  void dispose() {
    _plateNumberController.dispose();
    _saleTransDateFromController.dispose();
    _saleTransDateToController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    'TÌM KIẾM',
                    style: Theme.of(context).textTheme.subtitle1,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 24.h),
                PrimaryTextField(
                  controller: _plateNumberController,
                  labelText: 'Biển số xe',
                  hintText: 'Biển số xe (00X1234)',
                  isPlateNumber: true,
                  maxLength: 20,
                ),
                SizedBox(height: 28.h),
                Text(
                  'Trạng thái vé',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2!
                      .copyWith(fontSize: 18.sp),
                ),
                SizedBox(height: 12.h),
                RadioGroupButtons<int>(
                  defaultValue: _efficiencyId,
                  values: const [1, 2],
                  labels: const ['Còn hiệu lực', 'Hết hiệu lực'],
                  onChanged: (value) =>
                      setState(() => _efficiencyId = value ?? 1),
                  scrollDirection: Axis.horizontal,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _saleTransDateFromController,
                  labelText: 'Ngày giao dịch: từ ngày',
                  hintText: 'Từ ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showTransactionTicketPurchaseDateRangePicker,
                  onClear: () => _saleTransDateFrom = null,
                ),
                SizedBox(height: 28.h),
                PrimaryTextField(
                  controller: _saleTransDateToController,
                  labelText: 'Ngày giao dịch: đến ngày',
                  hintText: 'Đến ngày',
                  maxLength: 20,
                  suffix: Assets.icons.calendar.svg(),
                  readonly: true,
                  onTap: _showTransactionTicketPurchaseDateRangePicker,
                  onClear: () => _saleTransDateTo = null,
                ),
                SizedBox(height: 34.h),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: SecondaryButton(
                          onTap: () {
                            context
                                .read<TransactionTicketPurchaseBloc>()
                                .add(const TransactionTicketPurchaseFetched());
                            context
                                .read<TransactionTicketPurchaseBloc>()
                                .isSearching = false;
                            Navigator.of(context).pop();
                          },
                          title: 'Đặt lại',
                        ),
                      ),
                    ),
                    SizedBox(width: 16.w),
                    Expanded(
                      child: SizedBox(
                        height: 56.h,
                        child: PrimaryButton(
                          title: 'Tìm kiếm',
                          onTap: () {
                            context.read<TransactionTicketPurchaseBloc>().add(
                                  TransactionTicketPurchaseSearched(
                                    plateNumber: _plateNumberController.text,
                                    efficiencyId: _efficiencyId,
                                    saleTransDateFrom: _saleTransDateFrom,
                                    saleTransDateTo: _saleTransDateTo,
                                  ),
                                );
                            context
                                .read<TransactionTicketPurchaseBloc>()
                                .isSearching = true;
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // SizedBox(height: 16.h),
              ],
            ),
          ),
          Positioned(
            top: 12.h,
            right: 12.w,
            child: SplashIconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: ColorName.borderColor,
              ),
              onTap: () => Navigator.of(context).pop(),
            ),
          ),
        ],
      ),
    );
  }

  void _showTransactionTicketPurchaseDateRangePicker() async {
    DateTimeRange? initialDateRange;
    if (_saleTransDateFrom != null) {
      initialDateRange = DateTimeRange(
        start: _saleTransDateFrom!,
        end: _saleTransDateTo ?? Jiffy().dateTime,
      );
    }

    final dateRange = await showDateRangePicker(
          context: context,
          locale: const Locale('vi'),
          initialDateRange: initialDateRange,
          firstDate: Jiffy().subtract(years: 2).startOf(Units.DAY).dateTime,
          lastDate: Jiffy().dateTime,
          initialEntryMode: DatePickerEntryMode.calendarOnly,
        ) ??
        initialDateRange;

    _saleTransDateFrom = dateRange?.start;
    _saleTransDateTo = dateRange?.end;

    if (_saleTransDateFrom != null) {
      _saleTransDateFromController.text =
          Jiffy(_saleTransDateFrom).format('dd/MM/yyyy');
    }

    if (_saleTransDateTo != null) {
      _saleTransDateToController.text =
          Jiffy(_saleTransDateTo).format('dd/MM/yyyy');
    }
  }
}
