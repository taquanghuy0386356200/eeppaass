// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'confirm_order_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfirmOrder _$ConfirmOrderFromJson(Map<String, dynamic> json) => ConfirmOrder(
      ticketId: json['ticketId'] as String,
      success: json['success'] as bool,
      message: json['message'] as String,
    );

Map<String, dynamic> _$ConfirmOrderToJson(ConfirmOrder instance) =>
    <String, dynamic>{
      'ticketId': instance.ticketId,
      'success': instance.success,
      'message': instance.message,
    };

ConfirmOrderResponse _$ConfirmOrderResponseFromJson(
        Map<String, dynamic> json) =>
    ConfirmOrderResponse(
      details: (json['details'] as List<dynamic>?)
          ?.map((e) => ConfirmOrder.fromJson(e as Map<String, dynamic>))
          .toList(),
      responseTime: json['responseTime'] as String?,
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfirmOrderResponseToJson(
        ConfirmOrderResponse instance) =>
    <String, dynamic>{
      'details': instance.details,
      'responseTime': instance.responseTime,
      'mess': instance.mess,
    };
