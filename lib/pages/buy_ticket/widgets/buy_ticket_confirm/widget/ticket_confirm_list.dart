import 'package:epass/commons/extensions/number_ext.dart';
import 'package:epass/commons/models/ticket_price/ticket_price_response.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_price_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TicketConfirmList extends StatelessWidget {
  const TicketConfirmList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<TicketPriceBloc, TicketPriceState, List<TicketPrice>>(
      selector: (state) {
        if (state is TicketPriceFetchedSuccess) {
          return state.ticketPrices;
        }
        return [];
      },
      builder: (context, ticketPrices) {
        return ListView.separated(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            final ticketPrice = ticketPrices[index];
            return Padding(
              padding: EdgeInsets.fromLTRB(
                0.w,
                16.h,
                0.w,
                12.h,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Biển xe: ${ticketPrice.plateNumber}',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      SizedBox(height: 8.h),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Loại xe:  ',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    color: ColorName.disabledBorderColor,
                                  ),
                            ),
                            TextSpan(
                              text: 'Xe loại ${ticketPrice.vehicleGroupId}',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  Text(
                    (ticketPrice.fee ?? 0).formatCurrency(symbol: '₫'),
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ],
              ),
            );
          },
          separatorBuilder: (context, index) =>
              Divider(color: Colors.grey.shade500),
          itemCount: ticketPrices.length,
        );
      },
    );
  }
}
