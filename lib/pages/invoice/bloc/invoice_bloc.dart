import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/invoice/invoice.dart';
import 'package:epass/commons/repo/invoice_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:simple_result/simple_result.dart';

part 'invoice_event.dart';

part 'invoice_state.dart';

class InvoiceBloc extends Bloc<InvoiceEvent, InvoiceState> {
  final IInvoiceRepository _invoiceRepository;
  final AppBloc _appBloc;

  final int _pageSize;
  int _startRecord = 0;

  InvoiceBloc({
    required IInvoiceRepository invoiceRepository,
    required AppBloc appBloc,
    int pageSize = 10,
  })  : _invoiceRepository = invoiceRepository,
        _appBloc = appBloc,
        _pageSize = pageSize,
        super(const InvoiceState(listData: [])) {
    on<InvoiceFetched>(_onInvoiceFetched);
    on<InvoiceRefreshed>(_onInvoiceRefreshed);
    on<InvoiceLoadMore>(_onInvoiceLoadMore);
    on<InvoiceSearched>(_onInvoiceSearched);
  }

  FutureOr<void> _onInvoiceFetched(
    InvoiceFetched event,
    Emitter<InvoiceState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      plateNumber: null,
      invoiceNo: null,
      adjustmentType: null,
      transactionType: null,
      stationInId: null,
      stationOutId: null,
      from: null,
      to: null,
      error: null,
    ));

    final result = await _getInvoice();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: null,
            invoiceNo: null,
            adjustmentType: null,
            transactionType: null,
            stationInId: null,
            stationOutId: null,
            from: null,
            to: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: null,
        invoiceNo: null,
        adjustmentType: null,
        transactionType: null,
        stationInId: null,
        stationOutId: null,
        from: null,
        to: null,
      )),
    );
  }

  FutureOr<void> _onInvoiceRefreshed(
      InvoiceRefreshed event, Emitter<InvoiceState> emit) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      plateNumber: state.plateNumber,
      invoiceNo: state.invoiceNo,
      adjustmentType: state.adjustmentType,
      transactionType: state.transactionType,
      stationInId: state.stationInId,
      stationOutId: state.stationOutId,
      from: state.from,
      to: state.to,
      error: null,
    ));

    final result = await _getInvoice();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            invoiceNo: state.invoiceNo,
            adjustmentType: state.adjustmentType,
            transactionType: state.transactionType,
            stationInId: state.stationInId,
            stationOutId: state.stationOutId,
            from: state.from,
            to: state.to,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        invoiceNo: state.invoiceNo,
        adjustmentType: state.adjustmentType,
        transactionType: state.transactionType,
        stationInId: state.stationInId,
        stationOutId: state.stationOutId,
        from: state.from,
        to: state.to,
      )),
    );
  }

  FutureOr<void> _onInvoiceLoadMore(
    InvoiceLoadMore event,
    Emitter<InvoiceState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      plateNumber: state.plateNumber,
      invoiceNo: state.invoiceNo,
      adjustmentType: state.adjustmentType,
      transactionType: state.transactionType,
      stationInId: state.stationInId,
      stationOutId: state.stationOutId,
      from: state.from,
      to: state.to,
      error: null,
    ));

    final result = await _getInvoice();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(state.copyWith(
          isLoadingMore: false,
          listData: listData,
          isFull: listData.length == data.count,
          plateNumber: state.plateNumber,
          invoiceNo: state.invoiceNo,
          adjustmentType: state.adjustmentType,
          transactionType: state.transactionType,
          stationInId: state.stationInId,
          stationOutId: state.stationOutId,
          from: state.from,
          to: state.to,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        invoiceNo: state.invoiceNo,
        adjustmentType: state.adjustmentType,
        transactionType: state.transactionType,
        stationInId: state.stationInId,
        stationOutId: state.stationOutId,
        from: state.from,
        to: state.to,
      )),
    );
  }

  FutureOr<void> _onInvoiceSearched(
    InvoiceSearched event,
    Emitter<InvoiceState> emit,
  ) async {
    emit(state.copyWith(
      isLoading: true,
      plateNumber:
          event.plateNumber?.isEmpty ?? true ? null : event.plateNumber,
      invoiceNo: event.invoiceNo?.isEmpty ?? true ? null : event.invoiceNo,
      adjustmentType: event.adjustmentType,
      transactionType: event.transactionType,
      stationInId: event.stationInId,
      stationOutId: event.stationOutId,
      from: event.from,
      to: event.to,
      error: null,
    ));

    _startRecord = 0;

    final result = await _getInvoice();

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            isFull: data.listData.length == data.count,
            plateNumber: state.plateNumber,
            invoiceNo: state.invoiceNo,
            adjustmentType: state.adjustmentType,
            transactionType: state.transactionType,
            stationInId: state.stationInId,
            stationOutId: state.stationOutId,
            from: state.from,
            to: state.to,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: state.plateNumber,
        invoiceNo: state.invoiceNo,
        adjustmentType: state.adjustmentType,
        transactionType: state.transactionType,
        stationInId: state.stationInId,
        stationOutId: state.stationOutId,
        from: state.from,
        to: state.to,
      )),
    );
  }

  Future<Result<InvoiceDataNotNull, Failure>> _getInvoice() async {
    final result = await _invoiceRepository.getCustInvoices(
      pageSize: _pageSize,
      startRecord: _startRecord,
      contractId: _appBloc.state.user?.contractId ?? '',
      plateNumber: state.plateNumber,
      invoiceNo: state.invoiceNo,
      adjustmentType: state.adjustmentType,
      transactionType: state.transactionType,
      stationInId: state.stationInId,
      stationOutId: state.stationOutId,
      from: state.from != null ? Jiffy(state.from).format('MM/dd/yyyy') : null,
      to: state.to != null ? Jiffy(state.to).format('MM/dd/yyyy') : null,
    );
    return result;
  }
}
