import 'package:epass/commons/enum/boo_code.dart';
import 'package:epass/commons/enum/method_charge.dart';
import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:json_annotation/json_annotation.dart';

/// name : "Bạc Liêu"
/// code : "419"
/// stationId : 419
/// methodChargeId : 2
/// longitude : 105.712573
/// latitude : 9.374213
/// address : "Km2171+200 QL1, Bạc Liêu, Tỉnh Bạc Liêu"
/// booCode : "BOO1"
/// isEtc : 1
/// priceImagePath : "station/419.png"
/// priceLinkWeb : "https://epass-vdtc.com.vn/bieu-muc-thu-phi-tai-tram-bot/bieu-muc-thu-phi-su-dung-duong-bo-tai-tram-thu-phi-do-vetc-van-hanh/#tram-thu-phi-bac-lieu"
/// stationType : 1

part 'station_stage.g.dart';

@JsonSerializable()
class StationStage extends DropdownItem {
  StationStage({
    this.name,
    this.code,
    this.stationId,
    this.stageId,
    this.methodChargeId,
    this.longitude,
    this.latitude,
    this.address,
    this.addressIn,
    this.addressOut,
    this.booCode,
    this.isEtc,
    this.priceImagePath,
    this.priceLinkWeb,
    this.stationType,
  }) : super(title: name, value: code);

  final String? name;
  final String? code;
  final int? stationId;
  final int? stageId;

  @JsonKey(unknownEnumValue: MethodChargeInt.unknown)
  final MethodChargeInt? methodChargeId;

  final double? longitude;
  final double? latitude;
  final String? address;
  final String? addressIn;
  final String? addressOut;

  @JsonKey(unknownEnumValue: BooCode.unknown)
  final BooCode? booCode;

  final int? isEtc;
  final String? priceImagePath;
  final String? priceLinkWeb;
  final int? stationType;

  factory StationStage.fromJson(Map<String, dynamic> json) =>
      _$StationStageFromJson(json);
}

@JsonSerializable()
class StationStageData {
  final List<StationStage>? listData;
  final int? count;

  StationStageData({this.listData, this.count});

  factory StationStageData.fromJson(Map<String, dynamic> json) =>
      _$StationStageDataFromJson(json);
}

class StationStageDataNotNull {
  final List<StationStage> listData;
  final int count;

  StationStageDataNotNull({required this.listData, required this.count});
}

@JsonSerializable()
class StationStageResponse {
  final StationStageData? data;
  final MessageData? mess;

  StationStageResponse({this.data, this.mess});

  factory StationStageResponse.fromJson(Map<String, dynamic> json) =>
      _$StationStageResponseFromJson(json);
}
