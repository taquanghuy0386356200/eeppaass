enum RegisterLevel { hot, normal }

extension RegisterLevelExt on RegisterLevel {
  String get title {
    switch (this) {
      case RegisterLevel.hot:
        return 'HOT (Dán thẻ trong vòng 24h - Phụ thu 20.000đ)';
      case RegisterLevel.normal:
        return 'Thường (Dán thẻ trong vòng 5 ngày - Không phí phụ thu)';
    }
  }

  int get value {
    switch (this) {
      case RegisterLevel.hot:
        return 2;
      case RegisterLevel.normal:
        return 3;
    }
  }
}
