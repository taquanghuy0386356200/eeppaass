import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/authentication_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'setting_page_event.dart';

part 'setting_page_state.dart';

class SettingPageBloc extends Bloc<SettingPageEvent, SettingPageState> {
  final IAuthenticationRepository _authenticationRepository;
  final AppBloc _appBloc;

  SettingPageBloc({
    required IAuthenticationRepository authenticationRepository,
    required AppBloc appBloc,
  })  : _authenticationRepository = authenticationRepository,
        _appBloc = appBloc,
        super(SettingPageInitial()) {
    on<SettingPageLogout>(_onLogout);
  }

  FutureOr<void> _onLogout(
    SettingPageLogout event,
    Emitter<SettingPageState> emit,
  ) async {
    final refreshToken = _appBloc.state.authInfo?.refreshToken;
    if (refreshToken == null) {
      emit(const SettingPageLogoutFailure('Không tìm thấy token - client'));
      return;
    }

    final result =
        await _authenticationRepository.logout(refreshToken: refreshToken);

    result.when(
      success: (_) {
        emit(SettingPageLogoutSuccess());
      },
      failure: (failure) =>
          emit(SettingPageLogoutFailure(failure.message ?? '')),
    );
  }
}
