import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/models/ocr_id_card/id_card_ocr.dart';
import 'package:epass/commons/models/user/user.dart';
import 'package:epass/pages/register/bloc/register_customer/register_customer_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:jiffy/jiffy.dart';

part 'register_id_card_event.dart';

part 'register_id_card_state.dart';

class RegisterIdCardBloc extends Bloc<RegisterIdCardEvent, RegisterIdCardState> {
  final RegisterCustomerBloc _registerCustomerBloc;

  RegisterIdCardBloc({
    required RegisterCustomerBloc registerCustomerBloc,
  })  : _registerCustomerBloc = registerCustomerBloc,
        super(const RegisterIdCardState()) {
    on<RegisterIdCardUpdated>((event, emit) {
      final idCard = state.idCard != null ? state.idCard?.copyWith(idCard: event.idCard) : event.idCard;

      final genderRaw = idCard?.sex;
      Gender? gender;

      if (genderRaw != null) {
        if (genderRaw == 'NAM') {
          gender = Gender.male;
        } else if (genderRaw == 'NỮ') {
          gender = Gender.female;
        }
      }

      final dobRaw = idCard?.birthday;
      DateTime? dob;
      if (dobRaw != null && dobRaw.isNotEmpty) {
        dob = Jiffy(dobRaw, 'dd-MM-yyyy').dateTime;
      }

      final dateOfIssueRaw = idCard?.issueDate;
      DateTime? dateOfIssue;
      if (dateOfIssueRaw != null && dateOfIssueRaw.isNotEmpty) {
        dateOfIssue = Jiffy(dateOfIssueRaw, 'dd-MM-yyyy').dateTime;
      }

      final detailAddress = idCard?.streetName;

      final province = idCard?.province;
      final provinceName = idCard?.provinceName;
      final district = idCard?.district;
      final districtName = idCard?.districtName;
      final ward = idCard?.precinct;
      final wardName = idCard?.precinctName;

      AddressVTT? provinceVtt;
      AddressVTT? districtVtt;
      AddressVTT? wardVtt;

      if (province != null &&
          provinceName != null &&
          district != null &&
          districtName != null &&
          ward != null &&
          wardName != null) {
        provinceVtt = AddressVTT(
          province: province,
          district: '$province$district',
          precinct: '$province$district',
        );

        districtVtt = AddressVTT(
          province: province,
          district: '$province$district',
          precinct: '$province$district',
          name: districtName,
        );

        wardVtt = AddressVTT(
          province: province,
          district: '$province$district',
          precinct: '$province$district$ward',
          name: wardName,
        );
      }

      _registerCustomerBloc.add(RegisterCustomerUpdated(
        name: idCard?.name,
        dob: dob,
        dateOfIssue: dateOfIssue,
        documentNumber: idCard?.id,
        placeOfIssue: idCard?.issueBy,
        gender: gender,
        idImageBackPath: event.back,
        idImageFrontPath: event.front,
        address: detailAddress,
        province: provinceVtt,
        district: districtVtt,
        ward: wardVtt,
      ));

      emit(state.copyWith(
        front: event.front,
        back: event.back,
        idCard: idCard,
      ));
    });
  }
}
