import 'package:json_annotation/json_annotation.dart';

part 'mb_check_payment_result.g.dart';

@JsonSerializable()
class MBCheckPaymentResultRequest {
  final String checkSum = '';
  final String debitAccount = '';
  final String debitName = '';
  final String paymentResource = '';
  final String requestToken = '';
  final String transactionId;
  String token;

  MBCheckPaymentResultRequest(
      {required this.transactionId, required this.token});

  factory MBCheckPaymentResultRequest.fromJson(Map<String, dynamic> json) =>
      _$MBCheckPaymentResultRequestFromJson(json);
}

@JsonSerializable()
class MBCheckPaymentResultResponse {
  final String clientMessageId;
  final String? errorCode;
  @JsonKey(name: 'data')
  final MBPaymentResultData data;

  MBCheckPaymentResultResponse({
    required this.clientMessageId,
    required this.errorCode,
    required this.data,
  });

  factory MBCheckPaymentResultResponse.fromJson(Map<String, dynamic> json) =>
      _$MBCheckPaymentResultResponseFromJson(json);
}

@JsonSerializable()
class MBPaymentResultData {
  final String status;
  // final String? t24ReferenceNumber;
  final String data;
  // final String way4ReferenceNumber;
  // final String t24UpdateDateTime;

  MBPaymentResultData({
    required this.status,
    // required this.t24ReferenceNumber,
    required this.data,
    // required this.way4ReferenceNumber,
    // required this.t24UpdateDateTime
  });

  factory MBPaymentResultData.fromJson(Map<String, dynamic> json) =>
      _$MBPaymentResultDataFromJson(json);

  Map<String, dynamic> toJson() => _$MBPaymentResultDataToJson(this);
}
