part of 'wait_for_confirmation_bloc.dart';

abstract class WaitForConfirmationEvent extends Equatable {
  const WaitForConfirmationEvent();
}

class ListWaitingOrderFetched extends WaitForConfirmationEvent {
  @override
  List<Object> get props => [];
}

class WaitingOrderCancel extends WaitForConfirmationEvent {
  final int? saleOrderId;
  const WaitingOrderCancel({this.saleOrderId});

  @override
  List<Object?> get props => [saleOrderId];
}

class WaitingOrderConfirm extends WaitForConfirmationEvent {
  final int? saleOrderId;
  const WaitingOrderConfirm({this.saleOrderId});

  @override
  List<Object?> get props => [saleOrderId];
}

class ListConfirmOrderRequest extends WaitForConfirmationEvent {
  final List<WaitingOrder> waitingOrders;

  const ListConfirmOrderRequest(this.waitingOrders);

  @override
  List<Object> get props => [waitingOrders];
}

class ConfirmationNotificationRefreshed extends WaitForConfirmationEvent {
  const ConfirmationNotificationRefreshed();

  @override
  List<Object?> get props => [];
}

class ConfirmationNotificationLoadMore extends WaitForConfirmationEvent {
  const ConfirmationNotificationLoadMore();

  @override
  List<Object?> get props => [];
}

class ConfirmationNotificationSearched extends WaitForConfirmationEvent {
  final String? input;

  const ConfirmationNotificationSearched(this.input);

  @override
  List<Object?> get props => [input];
}

class ConfirmationNotificationRead extends WaitForConfirmationEvent {
  final int notificationId;

  const ConfirmationNotificationRead(this.notificationId);

  @override
  List<Object> get props => [notificationId];
}
