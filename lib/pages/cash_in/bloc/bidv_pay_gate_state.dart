part of 'bidv_pay_gate_bloc.dart';

abstract class BIDVPayGateState extends Equatable {
  const BIDVPayGateState();
}

class BIDVPayGateInitial extends BIDVPayGateState {
  @override
  List<Object> get props => [];
}

class BIDVPayGateLinkedCompletedSuccess extends BIDVPayGateState {
  final String orderId;
  final int amount;

  const BIDVPayGateLinkedCompletedSuccess({
    required this.orderId,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, amount];
}

class BIDVPayGateLinkedCompletedFailure extends BIDVPayGateState {
  final String message;

  const BIDVPayGateLinkedCompletedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CheckLinkBIDVConnnectedSuccess extends BIDVPayGateState {
  final String status;

  const CheckLinkBIDVConnnectedSuccess({
    required this.status,
  });

  @override
  List<Object> get props => [status];
}

class CheckLinkBIDVConnnectedFailure extends BIDVPayGateState {
  final String message;

  const CheckLinkBIDVConnnectedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class CashInOrderBIDVConnnectedSuccess extends BIDVPayGateState {
  final String contractNo;
  final int amount;

  const CashInOrderBIDVConnnectedSuccess({
    required this.contractNo,
    required this.amount,
  });

  @override
  List<Object> get props => [contractNo, amount];
}

class CashInOrderBIDVConnnectedFailure extends BIDVPayGateState {
  final String message;

  const CashInOrderBIDVConnnectedFailure(this.message);

  @override
  List<Object> get props => [message];
}
