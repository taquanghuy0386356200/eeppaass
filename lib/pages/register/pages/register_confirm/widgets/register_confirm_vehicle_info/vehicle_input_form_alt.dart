import 'package:epass/commons/enum/register_level.dart';
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/validators/plate_number_validator.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/textfield/plate_type_picker.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/commons/widgets/textfield/vehicle_type_picker.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/pages/register/pages/register_address/widgets/address_dropdown/address_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VehicleInputFormAlt extends StatefulWidget {
  final int vehicleIndex;

  const VehicleInputFormAlt({
    Key? key,
    required this.vehicleIndex,
  }) : super(key: key);

  @override
  State<VehicleInputFormAlt> createState() => _VehicleInputFormAltState();
}

class _VehicleInputFormAltState extends State<VehicleInputFormAlt> with PlateNumberValidator {
  final _ownerController = TextEditingController();
  final _plateNumberController = TextEditingController();
  final _addressDetailsController = TextEditingController();

  @override
  void initState() {
    final vehicleItem = context.read<RegisterVehiclesBloc>().state.listRegisterVehicle[widget.vehicleIndex];

    _ownerController.text = vehicleItem.vehicleInfo.owner ?? '';
    _plateNumberController.text = vehicleItem.vehicleInfo.plateNumber ?? '';
    _addressDetailsController.text = vehicleItem.addressInfo.addressDetails ?? '';

    super.initState();
  }

  @override
  void dispose() {
    _ownerController.dispose();
    _plateNumberController.dispose();
    _addressDetailsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // VEHICLE INFO
        PrimaryTextField(
          controller: _ownerController,
          inputAction: TextInputAction.next,
          labelText: 'Tên chủ phương tiện',
          hintText: 'Nhập tên chủ phương tiện',
          validator: (value) {
            if (value?.isEmpty ?? true) {
              return 'Vui lòng nhập Tên chủ phương tiện';
            }
            return null;
          },
          onChanged: (owner) {
            context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                  vehicleIndex: widget.vehicleIndex,
                  owner: owner,
                ));
          },
        ),
        SizedBox(height: 20.h),
        PrimaryTextField(
          controller: _plateNumberController,
          inputAction: TextInputAction.next,
          labelText: 'Biển số xe',
          hintText: 'Nhập biển số xe',
          isPlateNumber: true,
          validator: (value) {
            final error = plateNumberValidator(value);
            if (error != null) {
              return error;
            }

            final duplicateItems = context
                .read<RegisterVehiclesBloc>()
                .state
                .listRegisterVehicle
                .where((element) => element.vehicleInfo.plateNumber == value);
            return duplicateItems.length > 1 ? 'Biển số xe đã tồn tại' : null;
          },
          onChanged: (plateNumber) {
            context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                  vehicleIndex: widget.vehicleIndex,
                  plateNumber: plateNumber,
                ));
          },
        ),
        SizedBox(height: 20.h),
        BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, CategoryModel?>(
          selector: (state) => state.listRegisterVehicle[widget.vehicleIndex].vehicleInfo.plateType,
          builder: (context, state) {
            return PlateTypePicker(
              selectedPlateType: state,
              onItemChanged: (plateType) => context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                    vehicleIndex: widget.vehicleIndex,
                    plateType: plateType?.value,
                  )),
            );
          },
        ),
        SizedBox(height: 20.h),
        BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, CategoryModel?>(
          selector: (state) => state.listRegisterVehicle[widget.vehicleIndex].vehicleInfo.vehicleType,
          builder: (context, state) {
            return VehicleTypePicker(
              selectedVehicleType: state,
              onItemChanged: (vehicleType) => context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                    vehicleIndex: widget.vehicleIndex,
                    vehicleType: vehicleType?.value,
                  )),
            );
          },
        ),

        SizedBox(height: 20.h),

        // ADDRESS INFO
        PrimaryTextField(
          controller: _addressDetailsController,
          maxLines: 2,
          inputAction: TextInputAction.next,
          hintText: 'Nhập Địa chỉ dán thẻ',
          labelText: 'Địa chỉ dán thẻ',
          onChanged: (address) {
            context.read<RegisterVehiclesBloc>().add(AddressInfoAddressDetailsUpdated(
                  index: widget.vehicleIndex,
                  addressDetails: address,
                ));
          },
        ),
        SizedBox(height: 20.h),
        BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, AddressInfo>(
          selector: (state) => state.listRegisterVehicle[widget.vehicleIndex].addressInfo,
          builder: (context, addressInfo) {
            return AddressDropdown(
              dropdownType: AddressDropdownType.province,
              selectedItem: addressInfo.province,
              onChanged: (province) => context.read<RegisterVehiclesBloc>().add(AddressInfoProvinceUpdated(
                    index: widget.vehicleIndex,
                    province: province,
                  )),
            );
          },
        ),
        SizedBox(height: 20.h),
        BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, AddressInfo>(
          selector: (state) => state.listRegisterVehicle[widget.vehicleIndex].addressInfo,
          builder: (context, addressInfo) {
            return AddressDropdown(
              key: UniqueKey(),
              dropdownType: AddressDropdownType.district,
              provinceCode: addressInfo.province?.province,
              selectedItem: addressInfo.district,
              onChanged: (district) => context.read<RegisterVehiclesBloc>().add(AddressInfoDistrictUpdated(
                    index: widget.vehicleIndex,
                    district: district,
                  )),
            );
          },
        ),
        SizedBox(height: 20.h),
        BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, AddressInfo>(
          selector: (state) => state.listRegisterVehicle[widget.vehicleIndex].addressInfo,
          builder: (context, addressInfo) {
            return AddressDropdown(
              key: UniqueKey(),
              dropdownType: AddressDropdownType.ward,
              provinceCode: addressInfo.province?.province,
              districtCode: addressInfo.district?.district,
              selectedItem: addressInfo.ward,
              onChanged: (ward) => context.read<RegisterVehiclesBloc>().add(AddressInfoWardUpdated(
                    index: widget.vehicleIndex,
                    ward: ward,
                  )),
            );
          },
        ),

        SizedBox(height: 20.h),
        Text(
          'Thời gian dán thẻ mong muốn *',
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 16.h),
        BlocSelector<RegisterVehiclesBloc, RegisterVehiclesState, RegisterLevel?>(
          selector: (state) => state.listRegisterVehicle[widget.vehicleIndex].addressInfo.registerLevel,
          builder: (context, registerLevel) {
            return RadioGroupButtons<RegisterLevel>(
              labels: RegisterLevel.values.map((e) => e.title).toList(),
              values: RegisterLevel.values,
              defaultValue: registerLevel,
              onChanged: (value) {
                FocusScope.of(context).unfocus();
                context.read<RegisterVehiclesBloc>().add(AddressInfoRegisterLevelUpdated(
                  index: widget.vehicleIndex,
                  timeFrame: value,
                ));
              },
            );
          },
        ),
      ],
    );
  }
}
