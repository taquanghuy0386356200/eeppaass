import 'package:flutter/material.dart';

class CashInCurrencyText extends StatelessWidget {
  final String text;
  final String symbol;
  final Color? color;

  const CashInCurrencyText({
    Key? key,
    required this.text,
    this.color,
    this.symbol = 'đ',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          text,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                fontWeight: FontWeight.w600,
                color: color,
              ),
        ),
        const SizedBox(width: 2.0),
        Text(
          symbol,
          style: Theme.of(context).textTheme.overline!.copyWith(
                color: color,
              ),
        ),
      ],
    );
  }
}
