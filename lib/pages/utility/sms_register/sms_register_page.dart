import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/models/user_sms_vehicle_setting/user_sms_vehicle_setting.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/modal/neutral_confirm_dialog.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/primary_checkbox.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/sms_register/sms_register_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class SmsRegisterPage extends StatefulWidget {
  const SmsRegisterPage({Key? key}) : super(key: key);

  @override
  State<SmsRegisterPage> createState() => _SmsRegisterPageState();
}

class _SmsRegisterPageState extends State<SmsRegisterPage> {
  bool _firstRun = true;
  late SmsRegisterBloc _smsRegisterBloc;
  bool autoRenew = true;

  @override
  void initState() {
    super.initState();
    _smsRegisterBloc = context.read<SmsRegisterBloc>();

    _smsRegisterBloc.add(const CheckUserRegister());

    ///End check
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _firstRun = false;
    });
  }

  @override
  void dispose() {
    _smsRegisterBloc.add(SmsRegisterDispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final phoneNumber =
        context.select((AppBloc bloc) => bloc.state.user?.phone);
    return BasePage(
      resizeToAvoidBottomInset: true,
      title: 'Thông báo SMS',
      backgroundColor: Colors.white,
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.w),
        margin: EdgeInsets.only(bottom: 30.h),
        child: BlocBuilder<SmsRegisterBloc, SmsRegisterTotalState>(
          builder: (context, state) {
            if (!state.isRegister && state.message.isEmpty) {
              return Row(
                children: [
                  Expanded(
                    child: SecondaryButton(
                      title: 'Huỷ',
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  SizedBox(
                    width: 13.w,
                  ),
                  Expanded(
                    child: PrimaryButton(
                      title: 'Tiếp tục',
                      enabled: state.selectedSMSOption?.code != null,
                      onTap: () {
                        final code = state.selectedSMSOption?.code;
                        if (code != null) {
                          _smsRegisterBloc.add(
                            SMSVehicleRegistered(
                              smsRegisCode: code,
                              autoRenew: autoRenew,
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ],
              );
            } else {
              return const SizedBox.shrink();
            }
          },
        ),
      ),
      child: Stack(
        children: [
          const GradientHeaderContainer(),
          SafeArea(
            child: FadeAnimation(
              delay: 0.5,
              playAnimation: _firstRun,
              direction: FadeDirection.up,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: RoundedTopContainer(
                  margin: EdgeInsets.only(top: 16.h),
                  // padding: EdgeInsets.only(top: 24.h),
                  child: BlocConsumer<SmsRegisterBloc, SmsRegisterTotalState>(
                    listener: (context, state) {
                      if (state.isLoading) {
                        context.loaderOverlay.show();
                      } else {
                        context.loaderOverlay.hide();
                        if (state.messagePopup.isNotEmpty) {
                          showErrorSnackBBar(
                            context: context,
                            message: state.messagePopup,
                          );
                        }
                        if (state.successRegister) {
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (dialogContext) {
                              return SuccessDialog(
                                content: 'Bạn đã đăng ký thành công',
                                contentTextAlign: TextAlign.center,
                                buttonTitle: 'Đóng',
                                onButtonTap: () async {
                                  Navigator.of(dialogContext).pop();
                                },
                              );
                            },
                          );
                          Navigator.pop(context);
                        }
                      }
                    },
                    builder: (context, state) {
                      if (state.message.isNotEmpty) {
                        ///This will handle error case
                        return CommonErrorPage(
                          message: 'Có lỗi xảy ra',
                          onTap: () =>
                              _smsRegisterBloc.add(const CheckUserRegister()),
                        );

                        ///End
                      } else if (!state.isRegister && state.message.isEmpty) {
                        ///This case for new user
                        return Container(
                          margin: const EdgeInsets.all(16),
                          child: ShadowCard(
                            shadowOpacity: 0.2,
                            padding: EdgeInsets.fromLTRB(
                              16.w,
                              4.h,
                              16.w,
                              18.h,
                            ),
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    'Bạn có chắc chắn muốn đăng ký dịch vụ SMS',
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle1!
                                        .copyWith(
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.w600,
                                        ),
                                  ),
                                ),
                                SizedBox(height: 24.h),
                                RadioGroupButtons<CategoryModel>(
                                  defaultValue: state.selectedSMSOption,
                                  values: state.smsCategories ?? [],
                                  labels: (state.smsCategories ?? [])
                                      .map((e) =>
                                          '${e.name?.trim()}: ${e.description}')
                                      .toList(),
                                  onChanged: (value) {
                                    _smsRegisterBloc.add( SelectedSMSOption(selectedSMSOption: value));
                                  },
                                ),
                                SizedBox(height: 4.h),
                                Row(
                                  children: [
                                    PrimaryCheckbox(
                                        isCheck: autoRenew,
                                        onChanged: (isCheck) {
                                          setState(() =>
                                              autoRenew = isCheck ?? true);
                                        }),
                                    Material(
                                      color: Colors.transparent,
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: InkWell(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        onTap: () => setState(
                                          () => autoRenew = !autoRenew,
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.all(8.r),
                                          child: Text(
                                            'Gia hạn tự động',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        );

                        ///End
                      } else {
                        ///Handle case user registered
                        UserSmsVehicleSetting? userSMSVehicle;
                        userSMSVehicle = state.userSMSVehicleSetting;
                        final name = userSMSVehicle?.name;
                        final effDate = userSMSVehicle?.effDate;
                        final remain = userSMSVehicle?.smsRemain;
                        return Container(
                          margin: const EdgeInsets.all(16),
                          child: ShadowCard(
                            shadowOpacity: 0.2,
                            padding: EdgeInsets.fromLTRB(
                              16.w,
                              4.h,
                              16.w,
                              18.h,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Thông báo trừ tiền xe qua trạm bằng SMS',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                              fontWeight: FontWeight.bold,
                                            ),
                                        maxLines: 2,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.h),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // SMS NAME
                                    if (name != null)
                                      Row(
                                        children: [
                                          Icon(Icons.circle,
                                              size: 6.r,
                                              color: ColorName.primaryColor),
                                          SizedBox(width: 10.w),
                                          Text(
                                            name,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1!
                                                .copyWith(
                                                  fontWeight: FontWeight.bold,
                                                ),
                                          ),
                                        ],
                                      ),

                                    if (name != null) SizedBox(height: 16.h),
                                    if (name != null)
                                      Container(
                                        padding: EdgeInsets.fromLTRB(
                                            16.w, 12.h, 16.w, 6.h),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          border: Border.all(
                                              color: const Color(0xffc4c4c4),
                                              width: 1.0),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            // SMS PHONE NUMBER
                                            if (phoneNumber != null)
                                              RichText(
                                                text: TextSpan(
                                                  children: [
                                                    TextSpan(
                                                      text:
                                                          'Số điện thoại nhận:  ',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1!
                                                          .copyWith(
                                                            color: ColorName
                                                                .borderColor,
                                                          ),
                                                    ),
                                                    TextSpan(
                                                      text: phoneNumber,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1!
                                                          .copyWith(
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            // SMS EXP
                                            if (effDate != null)
                                              SizedBox(height: 10.h),
                                            if (effDate != null)
                                              RichText(
                                                text: TextSpan(
                                                  children: [
                                                    TextSpan(
                                                      text: 'Ngày hiệu lực:  ',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1!
                                                          .copyWith(
                                                            color: ColorName
                                                                .borderColor,
                                                          ),
                                                    ),
                                                    TextSpan(
                                                      text: effDate,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1!
                                                          .copyWith(
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            // SMS REMAIN
                                            if (remain != null)
                                              SizedBox(height: 10.h),
                                            if (remain != null)
                                              RichText(
                                                text: TextSpan(
                                                  children: [
                                                    TextSpan(
                                                      text: 'Số SMS còn lại:  ',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1!
                                                          .copyWith(
                                                            color: ColorName
                                                                .borderColor,
                                                          ),
                                                    ),
                                                    TextSpan(
                                                      text: remain.toString(),
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1!
                                                          .copyWith(
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            // AUTO RENEW
                                            Row(
                                              children: [
                                                Text(
                                                  'Tự động gia hạn:',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText1!
                                                      .copyWith(
                                                        color: ColorName
                                                            .borderColor,
                                                      ),
                                                ),
                                                SizedBox(width: 10.w),
                                                Transform.scale(
                                                  scale: 0.8,
                                                  child: CupertinoSwitch(
                                                    value: state.isExtendTime,
                                                    activeColor:
                                                        ColorName.primaryColor,
                                                    onChanged: (isOn) async {
                                                      if (isOn) {
                                                        await _showSMSAutoRenewDialog(
                                                            context);
                                                      } else {
                                                        await _showCancelSMSAutoRenewDialog(
                                                            context);
                                                      }
                                                    },
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        );

                        ///End
                      }
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showCancelSMSAutoRenewDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (dialogContext) {
        return BlocProvider.value(
          value: context.read<SmsRegisterBloc>(),
          child: NeutralConfirmDialog(
            title: 'Huỷ gia hạn tự động',
            content:
                'Sau khi hủy gia hạn gói tin nhắn sẽ không tự động gia hạn vào đầu tháng sau.'
                ' Bạn có muốn hủy gia hạn không?',
            contentTextAlign: TextAlign.start,
            secondaryButtonTitle: 'Đóng',
            primaryButtonTitle: 'Huỷ gia hạn',
            onPrimaryButtonTap: () async {
              Navigator.of(dialogContext).pop();
              context.read<SmsRegisterBloc>().add(
                    const SMSVehicleRegisterUpdated(
                      status: 1,
                      autoRenew: false,
                    ),
                  );
            },
          ),
        );
      },
    );
  }

  Future<void> _showSMSAutoRenewDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (dialogContext) {
        return BlocProvider.value(
          value: context.read<SmsRegisterBloc>(),
          child: ConfirmDialog(
            title: 'Tự động gia hạn gói cước',
            content:
                'Gói cước sẽ tự động gia hạn vào đầu tháng sau. Bạn có chắc chắn muốn gia hạn không?',
            contentTextAlign: TextAlign.start,
            secondaryButtonTitle: 'Đóng',
            primaryButtonTitle: 'Gia hạn',
            onPrimaryButtonTap: () async {
              Navigator.of(dialogContext).pop();
              context.read<SmsRegisterBloc>().add(
                    const SMSVehicleRegisterUpdated(
                      status: 1,
                      autoRenew: true,
                    ),
                  );
            },
          ),
        );
      },
    );
  }
}
