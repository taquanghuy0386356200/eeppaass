#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlBeta(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://kpp.bankplus.vn/vtpaymsbeta/MobileAppService2/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlStaging(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://wallet.viettelpay.vn/stagingms/MobileAppService2/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlUat(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://merchant.viettelpay.vn:8451/MobileAppService2/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlProduct(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://bankplus.vn/MobileAppService2/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getETCBaseURLProduction(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://api.viettelpay.vn/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlCdcnBeta(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://kpp.bankplus.vn/betamm/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlCdcnStaging(
        JNIEnv *env,
        jclass type) {
    std::string url = "http://125.235.38.229:8080/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlCdcnUat(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://kpp.bankplus.vn/uatmm/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseUrlCdcnProduct(
        JNIEnv *env,
        jclass type) {
    std::string url = "https://api.viettelpay.vn/";
    return env->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getBaseNotificationUrl(
        JNIEnv *env,
        jclass type) {
    std::string url = "http://bankplus.com.vn/cms/api/";
    return env->NewStringUTF(url.c_str());
}

//ETC
extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getClientIdStaging(
        JNIEnv *env,
        jclass type) {
    std::string clientId = "vtpay-sdk";
    return env->NewStringUTF(clientId.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getClientSecretStaging(
        JNIEnv *env,
        jclass type) {
    std::string clientSecret = "123456@a";
    return env->NewStringUTF(clientSecret.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getClientIdUAT(
        JNIEnv *env,
        jclass type) {
    std::string clientId = "vtpay-sdk";
    return env->NewStringUTF(clientId.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getClientSecretUAT(
        JNIEnv *env,
        jclass type) {
    std::string clientSecret = "01EQJN4GJD88M5RB1WGQRNDYTW";
    return env->NewStringUTF(clientSecret.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getETCBaseURLStaging(
        JNIEnv * env,
        jclass type) {
    std::string baseURLStaging = "http://125.235.38.229:8080/";
    return env->NewStringUTF(baseURLStaging.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getETCBaseURLUAT(
        JNIEnv * env,
        jclass type) {
    std::string baseURLStaging = "https://kpp.bankplus.vn/uatmm/";
    return env->NewStringUTF(baseURLStaging.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getCHPlayLink(
        JNIEnv * env,
        jclass type) {
    std::string CHPlayLink = "https://km.vtmoney.vn/314y/epass";
    return env->NewStringUTF(CHPlayLink.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_viettel_etc_epass_utils_SecurityCrypt_getKey(
        JNIEnv *env,
        jobject /* this */) {
    std::string k = "1Etc_220account_";
    return env->NewStringUTF(k.c_str());
}

extern "C" jstring
Java_com_viettel_etc_epass_utils_SecurityCrypt_IVFromJNI(
        JNIEnv *env,
        jclass clazz) {
    std::string k = "1234567812345678";
    return env->NewStringUTF(k.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_vn_viettelpay_sdk_NativeCall_getAppsFlyerKey(
        JNIEnv * env,
        jclass ){
    std::string  AppsFlyerKey = "u8x/A?D(G+KaPdSgVkYp3s6v9y$B&E)H";
    return env ->NewStringUTF(AppsFlyerKey.c_str());
}
