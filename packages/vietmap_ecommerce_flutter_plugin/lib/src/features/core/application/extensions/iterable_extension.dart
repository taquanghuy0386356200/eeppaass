extension SafeAccess<T> on Iterable<T> {
  T? getOrNull(int index) {
    if (isEmpty) {
      return null;
    }
    if (index >= length) {
      return null;
    }
    return elementAt(index);
  }
}
