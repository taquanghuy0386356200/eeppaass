import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:epass/flavors.dart';

/// phone : ""
/// user : ""
/// plateTypeCode : ""
/// userType : ""
/// confirmType : ""
/// requestTime : ""
/// checkSum : ""

class RequestOtpResetPasswordRequest {
  const RequestOtpResetPasswordRequest({
    required this.phone,
    required this.user,
    this.plateTypeCode,
    required this.userType,
    required this.confirmType,
    required this.requestTime,
  });

  final String phone;

  /// Could be ContractNo or PlateNumber
  final String user;
  final String? plateTypeCode;

  /// ContractNo: 0, PlateNumber: 1
  final String userType;

  /// Register: 0, ResetPassword: 1
  final String confirmType;
  final String requestTime;

  /// Checksum is hmac base64 of: access code + phone + user + plateTypeCode + userType + request time + confirm type
  String get checkSum {
    final checksum = '${F.crmAccessKey}'
        '$phone'
        '$user'
        '${plateTypeCode ?? ''}'
        '$userType'
        '$requestTime'
        '$confirmType';
    final key = utf8.encode(F.crmHashKey);
    final data = utf8.encode(checksum);
    final hmacSha1 = Hmac(sha1, key);
    final digest = hmacSha1.convert(data);
    final checkSumBase64 = base64.encode(digest.bytes);

    return checkSumBase64;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['phone'] = phone;
    map['user'] = user;
    map['plateTypeCode'] = plateTypeCode;
    map['userType'] = userType;
    map['confirmType'] = confirmType;
    map['requestTime'] = requestTime;
    map['checkSum'] = checkSum;
    return map;
  }
}
