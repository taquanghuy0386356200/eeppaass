import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// saleTransId : 2343954
/// saleTransCode : "DVSMS8187.20220425110829"
/// saleTransDetailId : 2347852
/// name : "Gói cho khách hàng cá nhân"
/// description : "8.600 vnđ/ 20 tin nhắn /tháng"
/// saleTransType : "3"
/// effDate : "25/04/2022"
/// expDate : "01/01/2100"
/// code : "3003"
/// autoRenew : "1"
/// smsRemain : 10

part 'user_sms_vehicle_setting.g.dart';

@JsonSerializable()
class UserSmsVehicleSetting {
  UserSmsVehicleSetting({
    this.saleTransId,
    this.saleTransCode,
    this.saleTransDetailId,
    this.name,
    this.description,
    this.saleTransType,
    this.effDate,
    this.expDate,
    this.code,
    this.autoRenew,
    this.smsRemain,
  });

  final int? saleTransId;
  final String? saleTransCode;
  final int? saleTransDetailId;
  final String? name;
  final String? description;
  final String? saleTransType;
  final String? effDate;
  final String? expDate;
  final String? code;
  final String? autoRenew;
  final int? smsRemain;

  factory UserSmsVehicleSetting.fromJson(Map<String, dynamic> json) => _$UserSmsVehicleSettingFromJson(json);

  @override
  String toString() {
    return 'UserSmsVehicleSetting{saleTransId: $saleTransId,'
        ' saleTransCode: $saleTransCode,'
        ' saleTransDetailId: $saleTransDetailId,'
        ' name: $name,'
        ' description: $description,'
        ' saleTransType: $saleTransType,'
        ' effDate: $effDate,'
        ' expDate: $expDate,'
        ' code: $code,'
        ' autoRenew: $autoRenew,'
        ' smsRemain: $smsRemain}';
  }
}

@JsonSerializable()
class UserSmsVehicleSettingResponse {
  final UserSmsVehicleSetting? data;
  final MessageData? mess;

  UserSmsVehicleSettingResponse({this.data, this.mess});

  factory UserSmsVehicleSettingResponse.fromJson(Map<String, dynamic> json) =>
      _$UserSmsVehicleSettingResponseFromJson(json);
}
