import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:camera/camera.dart';
import 'package:epass/commons/services/permission/permission.dart';
import 'package:equatable/equatable.dart';
import 'package:permission_handler/permission_handler.dart';

part 'register_paper_scan_permission_event.dart';

part 'register_paper_scan_permission_state.dart';

class RegisterPaperScanPermissionBloc extends Bloc<RegisterPaperScanPermissionEvent, RegisterPaperScanPermissionState> {
  RegisterPaperScanPermissionBloc() : super(RegisterPaperScanPermissionInitial()) {
    on<RegisterPaperScanPermissionRequested>(_onRegisterPaperScanPermissionRequested);
  }

  FutureOr<void> _onRegisterPaperScanPermissionRequested(
    RegisterPaperScanPermissionRequested event,
    emit,
  ) async {
    emit(RegisterPaperScanPermissionRequestedInProgress());
    final permissionStatus = await requestPermission(Permission.camera);
    await permissionStatus.when(
      success: (_) async {
        final cameras = await availableCameras();
        if (cameras.isNotEmpty) {
          emit(RegisterPaperScanPermissionRequestedSuccess(event.isFront));
        } else {
          emit(const RegisterPaperScanPermissionRequestedFailure('Camera không khả dụng'));
        }
      },
      failure: (failure) => emit(RegisterPaperScanPermissionRequestedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
