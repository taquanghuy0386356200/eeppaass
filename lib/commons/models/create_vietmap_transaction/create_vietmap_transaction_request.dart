import 'package:json_annotation/json_annotation.dart';

part 'create_vietmap_transaction_request.g.dart';

@JsonSerializable()
class CreateVietMapTransactionRequest {
  CreateVietMapTransactionRequest({
    this.refTransactionId,
    this.expireDate,
    this.price,
  });

  final String? refTransactionId;
  final String? expireDate;
  final int? price;

  factory CreateVietMapTransactionRequest.fromJson(Map<String, dynamic> json) => _$CreateVietMapTransactionRequestFromJson(json);
  Map<String, dynamic> toJson() => _$CreateVietMapTransactionRequestToJson(this);
}
