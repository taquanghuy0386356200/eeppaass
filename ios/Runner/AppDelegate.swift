import UIKit
import Flutter
import ViettelPayLiteSDK

// Begin Zalo
import zpdk

// chanel Init to handle Channel Flutter
enum ChannelName {
  static let channelPayOrder = "flutter.native/channelPayOrder"
  static let eventPayOrder = "flutter.native/eventPayOrder"
}

// methods define to handle in channel
enum MethodNames {
    static let methodPayOrder = "payOrder"
}

// End Zalo

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, UINavigationControllerDelegate {
    var navigationController: UINavigationController!
    
    private let momoPaymentChannelName = "com.viettel.etc.epass/momo_payment"
    private var momoFlutterResult: FlutterResult? = nil
    
    private let vnpayPaymentChannelName = "com.viettel.etc.epass/vnpay_payment"
    private var vnpayFlutterResult: FlutterResult? = nil
    
    private let viettelMoneyChannelName = "com.viettel.etc.epass/viettel_money"
    private var viettelMoneyFlutterResult: FlutterResult? = nil
    // Begin Zalo
    let PAYMENTCOMPLETE = 1
    let PAYMENTERROR = -1
    let PAYMENTCANCELED = 4
    private var eventSink: FlutterEventSink?
    // End Zalo
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
        
        // Viettel Money
        let env = Bundle.main.infoDictionary!["ENVIRONMENT"] as? String
        
        if let env = env, env == "prod" {
            VTPaySDKManager.sharedInstance().env = .Product
        } else {
            VTPaySDKManager.sharedInstance().env = .Staging
        }
        
        
        // MoMo Payment
        let momoPaymentChannel = FlutterMethodChannel(name: momoPaymentChannelName, binaryMessenger: controller.binaryMessenger)
        
        momoPaymentChannel.setMethodCallHandler { (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            switch call.method {
            case "requestPayment":
                self.onMoMoRequestPayment(call: call, result: result)
                break
            default:
                print("no method")
                result(FlutterMethodNotImplemented)
            }
        }
        
        let vnpayPaymentChannel = FlutterMethodChannel(name: vnpayPaymentChannelName, binaryMessenger: controller.binaryMessenger)
        
        vnpayPaymentChannel.setMethodCallHandler { (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            switch call.method {
            case "pushPayment":
                self.onVNPayPushPayment(call: call, result: result)
                break
            default:
                print("no method")
                result(FlutterMethodNotImplemented)
            }
        }
        
        let viettelMoneyPaymentChannel = FlutterMethodChannel(name: viettelMoneyChannelName, binaryMessenger: controller.binaryMessenger)
        
        viettelMoneyPaymentChannel.setMethodCallHandler { (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            switch call.method {
            case "link":
                self.onViettelMoneyLink(call: call, result: result)
                break
            case "unlink":
                self.onViettelMoneyUnlink(call: call, result: result)
                break
            default:
                print("no method")
                result(FlutterMethodNotImplemented)
            }
        }
        // Begin Zalo
        //ZaloPay
        // ZaloPaySDK.sharedInstance()?.initWithAppId(2414, uriScheme: "demozpdk://app", environment: .sandbox)
        ZaloPaySDK.sharedInstance()?.initWithAppId(2414, uriScheme: "demozpdk://app", environment: .production)
        // handle channel in native iOS
        let nativeChannel = FlutterMethodChannel(name: ChannelName.channelPayOrder, binaryMessenger: controller.binaryMessenger)

        nativeChannel.setMethodCallHandler({
        [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            guard call.method == MethodNames.methodPayOrder else {
              result(FlutterMethodNotImplemented)
              return
            }

            let args = call.arguments as? [String: Any]
            let  _zptoken = args?["zptoken"] as? String

            ZaloPaySDK.sharedInstance()?.paymentDelegate = self
            ZaloPaySDK.sharedInstance()?.payOrder(_zptoken)
            result("Processing...")
        })

        let eventPayOrderChannel = FlutterEventChannel(name: ChannelName.eventPayOrder,
                                                  binaryMessenger: controller.binaryMessenger)

        eventPayOrderChannel.setStreamHandler(self)
        // End Zalo
        
        GeneratedPluginRegistrant.register(with: self)
        
        // create and then add a new UINavigationController
        navigationController = UINavigationController(rootViewController: controller)
        navigationController.delegate = self
        window.rootViewController = navigationController
        navigationController.setNavigationBarHidden(true, animated: false)
        window.makeKeyAndVisible()
        
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    override func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        if let momoPartnerSchemeId = Bundle.main.object(forInfoDictionaryKey: "MOMO_PARTNER_SCHEME_ID") as? String,
           url.scheme == momoPartnerSchemeId {
            MoMoPayment.handleOpenUrl(url: url, sourceApp: "")
        }
        
        if let vnpayPartnerSchemeId = Bundle.main.object(forInfoDictionaryKey: "VNPAY_PARTNER_SCHEME_ID") as? String,
           url.scheme == vnpayPartnerSchemeId {
            self.vnpayFlutterResult?([
                "vnpTransactionStatus": 0
            ])
        }
        
        if (url.scheme == "mbbankepass") {
                   self.paymentMBBSucceede(url: url.absoluteString)
           }
            
        return ZaloPaySDK.sharedInstance().application(app, open: url, sourceApplication: "vn.com.vng.zalopay", annotation: nil) 
    }
    
    // MOMO
    private func onMoMoRequestPayment(call: FlutterMethodCall, result: @escaping FlutterResult) {
        momoFlutterResult = result
        NotificationCenter.default.addObserver(self, selector: #selector(NoficationCenterTokenStartRequest), name: NSNotification.Name(rawValue: "NoficationCenterTokenStartRequest"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NoficationCenterTokenReceived), name: NSNotification.Name(rawValue: "NoficationCenterTokenReceived"), object: nil)
        let options = call.arguments as! NSMutableDictionary
        MoMoPayment.createPaymentInformation(info: options)
        MoMoPayment.requestToken()
    }
    
    @objc func NoficationCenterTokenStartRequest(notif: NSNotification) {
    }
    
    @objc func NoficationCenterTokenReceived(notif: NSNotification) {
        print("::MoMoPay Log::Received Token Replied::\(notif.object!)")
        let response: NSMutableDictionary = notif.object! as! NSMutableDictionary
        let _statusStr = "\(response["status"] as! String)"
        if (_statusStr == "0") {
            self.momoFlutterResult?([
                "isSuccess": true,
                "status": 0,
                "token": response["data"],
                "phoneNumber": response["phonenumber"],
                "extra": response["extra"]
            ])
        } else {
            self.momoFlutterResult?([
                "isSuccess": false,
                "status": response["status"],
                "phoneNumber": response["phonenumber"],
                "extra": response["extra"]
            ])
        }
    }
    
    // VNPAY
    private func onVNPayPushPayment(call: FlutterMethodCall, result: @escaping FlutterResult) {
        vnpayFlutterResult = result
        
        let topController = viewController(with: nil)
        var windowToUse: UIWindow? = nil
        if windowToUse == nil {
            for window in UIApplication.shared.windows {
                if window.isKeyWindow {
                    windowToUse = window
                    break
                }
            }
        }

        if (topController is FlutterViewController) == false {
            topController?.dismiss(animated: true)
        }

        let options = call.arguments as! NSMutableDictionary
        CallAppInterface.setHomeViewController(viewController(with: nil)!)
        
        if let vnpayPartnerSchemeId = Bundle.main.object(forInfoDictionaryKey: "VNPAY_PARTNER_SCHEME_ID") as? String {
            CallAppInterface.setSchemes(vnpayPartnerSchemeId)
        }
        if let isSandbox = options["isSandbox"] as? Bool,
           let paymentUrl = options["paymentUrl"] as? String,
           let title = options["title"] as? String,
           let beginColor = options["beginColor"] as? String,
           let endColor = options["endColor"] as? String,
           let titleColor = options["titleColor"] as? String,
           let tmnCode = options["tmnCode"] as? String
        {
            CallAppInterface.setIsSandbox(isSandbox)
            CallAppInterface.showPushPaymentwithPaymentURL(
                paymentUrl,
                withTitle: title,
                iconBackName: "ic_back",
                beginColor: beginColor,
                endColor: endColor,
                titleColor: titleColor,
                tmn_code: tmnCode
            )
        }
    }
    
    private func onViettelMoneyLink(call: FlutterMethodCall, result: @escaping FlutterResult) {
        viettelMoneyFlutterResult = result
        
        let options = call.arguments as! NSMutableDictionary
        
        if let orderId = options["orderId"] as? String,
           let contractId = options["contractId"] as? String,
           let contractNo = options["contractNo"] as? String,
           let msisdn = options["msisdn"] as? String,
           let contractIdNo = options["contractIdNo"] as? String,
           let accountNo = options["accountNo"] as? String,
           let contractFullName = options["contractFullName"] as? String,
           let contractObjetType = options["contractObjetType"] as? String,
           let contractIdType = options["contractIdType"] as? String,
           let vehicles = options["vehicles"] as? Array<Dictionary<String, Any>>
        {
            let vehiclesDic = vehicles.map { vehicle in
                return (vehicle as NSDictionary).mutableCopy() as! NSMutableDictionary
            }
            
            let data = (
                [
                    "orderId": orderId,
                    "contractId": contractId,
                    "contractNo": contractNo,
                    "msisdn": msisdn,
                    "contractIdNo": contractIdNo,
                    "accountNo": accountNo,
                    "contractFullName": contractFullName,
                    "contractObjetType": contractObjetType,
                    "contractIdType": contractIdType,
                    "vehicles": vehiclesDic
                ] as NSDictionary).mutableCopy() as! NSMutableDictionary
            
            VTPaySDKManager.sharedInstance().initVTPSDK(
                withFunctionETC: .LienKet,
                phoneNumber: "",
                withToken: "",
                dicTicket: data,
                navigationController: navigationController) { (error) in
                    debugPrint(error)
                }
        } else {
            viettelMoneyFlutterResult?(["status": 0, "message": "Invalid parameters (vtm_001)"])
        }
    }
    
    private func onViettelMoneyUnlink(call: FlutterMethodCall, result: @escaping FlutterResult) {
        viettelMoneyFlutterResult = result
        
        let options = call.arguments as! NSMutableDictionary
        
        if let accountNumber = options["accountNumber"] as? String,
           let token = options["token"] as? String,
           let contractId = options["contractId"] as? String {
            let data = [
                "contractId": contractId,
                "msisdn": accountNumber,
                "token": token
            ]
            
            VTPaySDKManager.sharedInstance().initVTPSDK(
                withFunctionETC: .HuyLienKet,
                phoneNumber: accountNumber,
                withToken: token,
                dicTicket: NSMutableDictionary(dictionary: data),
                navigationController: navigationController) { (error) in
                    debugPrint(error)
                }
        } else {
            viettelMoneyFlutterResult?(["status": 0, "message": "Invalid parameters (vtm_001)"])
        }
    }
    
    func viewController(with window: UIWindow?) -> UIViewController? {
        var windowToUse = window
        if windowToUse == nil {
            for window in UIApplication.shared.windows {
                if window.isKeyWindow {
                    windowToUse = window
                    break
                }
            }
        }
        
        var topController = windowToUse?.rootViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        
        return topController
    }
    
    // MARK: - UINavigationControllerDelegate
    var linkViettelSuccess = false
    var backFromViettelSDK = false
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        // The order of code matters
        if viewController is FlutterViewController {
            if linkViettelSuccess {
                backFromViettelSDK = false
                linkViettelSuccess = false
                viettelMoneyFlutterResult?(["status": 1, "message": "Success"])
                return
            }
            
            if backFromViettelSDK {
                backFromViettelSDK = false
                linkViettelSuccess = false
                viettelMoneyFlutterResult?(["status": 2, "message": "Canceled"])
            }
        }
        
        // Back from "PaymentSuccessViewController" means processing successfully
        linkViettelSuccess = navigationController
            .viewControllers
            .contains(where: { String(describing: type(of: $0)) == "PaymentSuccessViewController" })
        
        backFromViettelSDK = navigationController
            .viewControllers
            .contains(where: { viewController in
                let vcName = String(describing: type(of: viewController))
                return vcName == "LoginETCPhoneViewController"
                        || vcName == "LoginETCLinkViewController"
                        || vcName == "SumitLinkViewController"
            })
    }
}
extension AppDelegate: ZPPaymentDelegate,FlutterStreamHandler {
    // MARK: - ZALO
    //dumv
    func paymentMBBSucceede(url: String!) {
        //Handle Success
        guard let eventSink = eventSink else {
          return
        }
        eventSink(["poupcode": "mbbankepass","url": url])
    }
    
    //ZaloPay
    func paymentDidSucceeded(_ transactionId: String!, zpTranstoken: String!, appTransId: String!) {
        //Handle Success
        guard let eventSink = eventSink else {
          return
        }
        eventSink(["poupcode": "zalopay","errorCode": PAYMENTCOMPLETE, "zpTranstoken": zpTranstoken ?? "", "transactionId": transactionId ?? "", "appTransId": appTransId ?? ""])
    }
    func paymentDidCanceled(_ zpTranstoken: String!, appTransId: String!) {
        //Handle Canceled
        guard let eventSink = eventSink else {
          return
        }
        eventSink(["poupcode": "zalopay","errorCode": PAYMENTCANCELED, "zpTranstoken": zpTranstoken ?? "", "appTransId": appTransId ?? ""])
    }
    func paymentDidError(_ errorCode: ZPPaymentErrorCode, zpTranstoken: String!, appTransId: String!) {
        guard let eventSink = eventSink else {
          return
        }
        eventSink(["poupcode": "zalopay","errorCode": PAYMENTERROR, "zpTranstoken": zpTranstoken ?? "", "appTransId": appTransId ?? ""])
        if errorCode == ZPPaymentErrorCode.appNotInstall {
            ZaloPaySDK.sharedInstance()?.navigateToZaloStore()
            return
        }
    }
    
    func installSandbox(){
        let alert = UIAlertController(title: "Info", message: "Please install ZaloPay", preferredStyle: UIAlertController.Style.alert)
        let installLink = "https://sandbox.zalopay.com.vn/static/ps_res_2019/ios/enterprise/sandboxmer/install.html"
        let controller = window.rootViewController as? FlutterViewController
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in

        }
        let installAction = UIAlertAction(title: "Install App", style: .default) { (action) in
            guard let url = URL(string: installLink) else {
                return //be safe
            }
            //


            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }

        alert.addAction(cancelAction)
        alert.addAction(installAction)
        controller?.present(alert, animated: true, completion: nil)
    }

    func installProduction(){
        let alert = UIAlertController(title: "Info", message: "Please install ZaloPay", preferredStyle: UIAlertController.Style.alert)
        let controller = window.rootViewController as? FlutterViewController
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in

        }
        let installAction = UIAlertAction(title: "Install App", style: .default) { (action) in
            ZaloPaySDK.sharedInstance()?.navigateToZaloStore()
        }
        alert.addAction(cancelAction)
        alert.addAction(installAction)
        controller?.present(alert, animated: true, completion: nil)
    }

    // func implement with FlutterStreamHandler
    func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink
        return nil
     }

    // func implement with FlutterStreamHandler
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil
        return nil
    }
}
