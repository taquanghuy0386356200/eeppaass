part of 'invoice_details_bloc.dart';

abstract class InvoiceDetailsEvent extends Equatable {
  const InvoiceDetailsEvent();
}

class InvoiceDownloaded extends InvoiceDetailsEvent {
  final String invoiceId;

  const InvoiceDownloaded(this.invoiceId);

  @override
  List<Object> get props => [invoiceId];
}

class InvoiceViewed extends InvoiceDetailsEvent {
  final String invoiceId;

  const InvoiceViewed(this.invoiceId);

  @override
  List<Object> get props => [invoiceId];
}
