import 'package:epass/commons/enum/boo_code.dart';
import 'package:epass/commons/enum/method_charge.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ticket_price_request.g.dart';

/// servicePlanDTOList : [{"quantity":1,"seatNumber":3,"vehicleTypeId":21,"stationId":5033,"autoRenew":0,"stationType":1,"expDate":"09/04/2022","chargeMethodId":2,"epc":"","booCode":"BOO2","effDate":"11/03/2022","vehicleId":827582,"vehicleGroupId":5,"servicePlanTypeId":4,"status":2,"plateNumber":"18L20239"}]

class TicketPriceListRequest {
  const TicketPriceListRequest({
    this.ticketPriceList,
  });

  final List<TicketPriceRequest>? ticketPriceList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (ticketPriceList != null) {
      map['servicePlanDTOList'] =
          ticketPriceList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// quantity : 1
/// seatNumber : 3
/// vehicleTypeId : 21
/// stationId : 5033
/// autoRenew : 0
/// stationType : 1
/// expDate : "09/04/2022"
/// chargeMethodId : 2
/// epc : ""
/// booCode : "BOO2"
/// effDate : "11/03/2022"
/// vehicleId : 827582
/// vehicleGroupId : 5
/// servicePlanTypeId : 4
/// status : 2
/// plateNumber : "18L20239"

@JsonSerializable(includeIfNull: false)
class TicketPriceRequest {
  const TicketPriceRequest({
    this.quantity,
    this.seatNumber,
    this.vehicleTypeId,
    this.stationId,
    this.stageId,
    this.autoRenew,
    this.stationType,
    this.expDate,
    this.chargeMethodId,
    this.epc,
    this.booCode,
    this.effDate,
    this.vehicleId,
    this.vehicleGroupId,
    this.servicePlanTypeId,
    this.status,
    this.plateNumber,
    this.netWeight,
    this.cargoWeight,
  });

  final int? quantity;
  final int? seatNumber;
  final int? vehicleTypeId;
  final int? stationId;
  final int? stageId;
  final int? autoRenew;
  final int? stationType;
  final String? expDate;
  final MethodChargeInt? chargeMethodId;
  final String? epc;
  final BooCode? booCode;
  final String? effDate;
  final int? vehicleId;
  final int? vehicleGroupId;
  final ServicePlanType? servicePlanTypeId;
  final int? status;
  final String? plateNumber;
  final double? cargoWeight;
  final double? netWeight;

  Map<String, dynamic> toJson() => _$TicketPriceRequestToJson(this);
}
