import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/services/permission/permission.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_source.dart';
import 'package:equatable/equatable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

part 'file_picker_event.dart';

part 'file_picker_state.dart';

class FilePickerBloc extends Bloc<FilePickerEvent, FilePickerState> {
  final ImagePicker _imagePicker;
  final FilePicker _filePicker;

  FilePickerBloc({
    required ImagePicker imagePicker,
    required FilePicker filePicker,
  })  : _imagePicker = imagePicker,
        _filePicker = filePicker,
        super(const FilePickerState()) {
    on<FilePicked>(_onFilePicked);
    on<FileRemoved>(_onFileRemoved);
  }

  FutureOr<void> _onFilePicked(
    FilePicked event,
    emit,
  ) async {
    emit(state.copyWith(isLoading: true, error: null));

    late Permission permission;

    switch (event.source) {
      case FileSource.camera:
        permission = Permission.camera;
        break;
      case FileSource.gallery:
        permission = Permission.photos;
        break;
      case FileSource.file:
        permission = Permission.storage;
        break;
    }

    final permissionStatus = await requestPermission(permission);

    await permissionStatus.when(
      success: (_) async {
        final filePaths = List<String>.from(state.filePaths);

        switch (event.source) {
          case FileSource.camera:
            final image = await _imagePicker.pickImage(
              source: ImageSource.camera,
              maxWidth: 800,
            );

            if (image == null) {
              emit(state.copyWith(isLoading: false, error: null));
              return;
            }

            filePaths.add(image.path);
            break;
          case FileSource.gallery:
            final images = await _imagePicker.pickMultiImage(maxWidth: 800);

            if (images == null) {
              emit(state.copyWith(isLoading: false, error: null));
              return;
            }

            filePaths.addAll(images.map((e) => e.path));

            if (filePaths.length > 4) {
              emit(state.copyWith(
                filePaths: filePaths.getRange(0, 4).toList(),
                isLoading: false,
                error: 'Lựa chọn tối đa 4 tệp tin',
              ));
              return;
            }

            break;
          case FileSource.file:
            final files = await _filePicker.pickFiles(
              type: FileType.custom,
              allowMultiple: true,
              allowedExtensions: Constant.allowedExtensions,
            );

            if (files == null) {
              emit(state.copyWith(isLoading: false, error: null));
              return;
            }

            filePaths.addAll(files.paths.whereType<String>());

            if (filePaths.length > 4) {
              emit(state.copyWith(
                filePaths: filePaths.getRange(0, 4).toList(),
                isLoading: false,
                error: 'Lựa chọn tối đa 4 tệp tin',
              ));
              return;
            }

            break;
        }

        emit(state.copyWith(
          filePaths: filePaths.toSet().toList(),
          isLoading: false,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message ?? 'Có lỗi xảy ra',
      )),
    );
  }

  FutureOr<void> _onFileRemoved(
    FileRemoved event,
    Emitter<FilePickerState> emit,
  ) {
    final filePaths = List<String>.from(state.filePaths);
    filePaths.remove(event.path);

    emit(state.copyWith(
      isLoading: false,
      error: null,
      filePaths: filePaths,
    ));
  }
}
