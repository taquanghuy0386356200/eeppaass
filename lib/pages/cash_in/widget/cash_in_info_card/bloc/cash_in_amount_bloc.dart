import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/cash_in_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'cash_in_amount_event.dart';

part 'cash_in_amount_state.dart';

class CashInAmountBloc extends Bloc<CashInAmountEvent, CashInAmountState> {
  final ICashInRepository _cashInRepository;

  CashInAmountBloc({
    required ICashInRepository cashInRepository,
  })  : _cashInRepository = cashInRepository,
        super(const CashInAmountChangedSuccess()) {
    on<CashInAmountChanged>((event, emit) =>
        emit(CashInAmountChangedSuccess(amount: event.cashInAmount)));
    on<CashInTargetLastRecharge>(_onCashInTargetLastRecharge);
  }

  FutureOr<void> _onCashInTargetLastRecharge(event, emit) async {
    final result = await _cashInRepository.getLastCharge();
    result.when(
      success: (contractsInfo) {
        emit(CashInAmountChangedSuccess(
            amount: contractsInfo.data?.amount,
            topupChannel: contractsInfo.data?.topupChannel));
      },
      failure: (failure) {
        emit(const CashInAmountChangedSuccess(amount: null));
      },
    );
  }
}
