part of 'buy_ticket_bloc.dart';

abstract class BuyTicketEvent extends Equatable {
  const BuyTicketEvent();
}

class BuyTicketConfirmed extends BuyTicketEvent {
  final List<CartItem> cartItems;

  const BuyTicketConfirmed(this.cartItems);

  @override
  List<Object> get props => [cartItems];
}
