import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VehicleBlocProviderPage extends StatelessWidget {
  const VehicleBlocProviderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<VehicleListBloc>(
      create: (context) => VehicleListBloc(
        vehicleRepository: getIt<IVehicleRepository>(),
        pageSize: 100,
      ),
      child: const AutoRouter(),
    );
  }
}
