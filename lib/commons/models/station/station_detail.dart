import 'package:epass/commons/models/message_data/message_data.dart';
import 'package:json_annotation/json_annotation.dart';

/// code : "780"
/// name : "An Sương An Lạc"
/// id : 780
/// name_short : "780"
/// station_type : "1"
/// is_active : "1"
/// address_detail : "QL 1, Q.Bình Tân, Thành Phố Hồ Chí Minh"
/// method_charge_id : "2"
/// longitude : 106.595981
/// latitude : 10.797814
/// booCode : "BOO1"
/// isEtc : 1
/// priceImagePath : "stations/920.png"
/// priceLinkWeb : "https://epass-vdtc.com.vn/bieu-muc-thu-phi-tai-tram-bot/bieu-muc-thu-phi-su-dung-duong-bo-tai-tram-thu-phi-do-vetc-van-hanh/#tram-thu-phi-an-suong-an-lac"

part 'station_detail.g.dart';

@JsonSerializable()
class StationDetail {
  final String? code;
  final String? name;
  final int? id;
  final String? nameShort;
  final String? stationType;
  final String? isActive;
  final String? addressDetail;
  final String? methodChargeId;
  final double? longitude;
  final double? latitude;
  final String? booCode;
  final int? isEtc;
  final String? priceImagePath;
  final String? priceLinkWeb;
  final double? warLeftLat1;
  final double? warLeftLong1;
  final double? warLeftLat2;
  final double? warLeftLong2;
  final double? warRightLat1;
  final double? warRightLong1;
  final double? warRightLat2;
  final double? warRightLong2;

  StationDetail({
    this.code,
    this.name,
    this.id,
    this.nameShort,
    this.stationType,
    this.isActive,
    this.addressDetail,
    this.methodChargeId,
    this.longitude,
    this.latitude,
    this.booCode,
    this.isEtc,
    this.priceImagePath,
    this.priceLinkWeb,
    this.warLeftLat1,
    this.warLeftLong1,
    this.warLeftLat2,
    this.warLeftLong2,
    this.warRightLat1,
    this.warRightLong1,
    this.warRightLat2,
    this.warRightLong2,
  });

  factory StationDetail.fromJson(Map<String, dynamic> json) => _$StationDetailFromJson(json);

  Map<String, dynamic> toJson() => _$StationDetailToJson(this);
}

@JsonSerializable()
class StationDetailData {
  final List<StationDetail>? listData;
  final int? count;

  StationDetailData({this.listData, this.count});

  factory StationDetailData.fromJson(Map<String, dynamic> json) => _$StationDetailDataFromJson(json);
}

class StationDetailDataNotNull {
  final List<StationDetail> listData;
  final int count;

  StationDetailDataNotNull({required this.listData, required this.count});
}

@JsonSerializable()
class StationDetailResponse {
  final StationDetailData? data;
  final MessageData? mess;

  StationDetailResponse({this.data, this.mess});

  factory StationDetailResponse.fromJson(Map<String, dynamic> json) => _$StationDetailResponseFromJson(json);
}
