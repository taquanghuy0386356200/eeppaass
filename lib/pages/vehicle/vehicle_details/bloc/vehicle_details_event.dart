part of 'vehicle_details_bloc.dart';

abstract class VehicleDetailsEvent extends Equatable {
  const VehicleDetailsEvent();
}

class VehicleRFIDUpdated extends VehicleDetailsEvent {
  final bool isOpen;
  final String vehicleId;

  const VehicleRFIDUpdated({
    required this.vehicleId,
    required this.isOpen,
  });

  @override
  List<Object> get props => [isOpen];
}

class VehicleCheckDebitFetched extends VehicleDetailsEvent {
  final String contractId;
  final String vehicleId;
  const VehicleCheckDebitFetched(
      {required this.vehicleId, required this.contractId});
  @override
  List<Object> get props => [vehicleId, contractId];
}
class VehicleDebitTransactionFetched extends VehicleDetailsEvent {
  final String vehicleId;
  const VehicleDebitTransactionFetched(
      { required this.vehicleId});
  @override
  List<Object> get props => [vehicleId];
}
