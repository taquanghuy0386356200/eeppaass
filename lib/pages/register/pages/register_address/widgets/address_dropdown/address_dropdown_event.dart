part of 'address_dropdown_bloc.dart';

abstract class AddressDropdownEvent extends Equatable {
  const AddressDropdownEvent();
}

class AddressVTTDropdownFetched extends AddressDropdownEvent {
  final AddressDropdownType dropdownType;
  final String? provinceCode;
  final String? districtCode;

  const AddressVTTDropdownFetched({
    required this.dropdownType,
    this.provinceCode,
    this.districtCode,
  });

  @override
  List<Object?> get props => [dropdownType, provinceCode, districtCode];
}

