part of 'verify_card_bloc.dart';

class VerifyCardState extends Equatable {
  final bool isOn;

  const VerifyCardState({
    this.isOn = false
  });

  @override
  List<Object> get props => [isOn];

  VerifyCardState copyWith({
    bool? isOn
  }) {
    return VerifyCardState(
        isOn: isOn ?? this.isOn
    );
  }
}
