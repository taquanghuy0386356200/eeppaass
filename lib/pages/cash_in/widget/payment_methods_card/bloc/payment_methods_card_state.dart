part of 'payment_methods_card_bloc.dart';

@immutable
abstract class PaymentMethodsCardState extends Equatable {
  const PaymentMethodsCardState();
}

class PaymentMethodsCardInitial extends PaymentMethodsCardState {
  @override
  List<Object> get props => [];
}

class PaymentMethodsCardLoading extends PaymentMethodsCardState {
  @override
  List<Object> get props => [];
}

class PaymentMethodsFetchedSuccess extends PaymentMethodsCardState {
  final List<TopupChannel> paymentMethods;
  final TopupChannel? selectedPaymentMethod;

  const PaymentMethodsFetchedSuccess({
    required this.paymentMethods,
    this.selectedPaymentMethod,
  });

  @override
  List<Object?> get props => [paymentMethods, selectedPaymentMethod];

  PaymentMethodsFetchedSuccess copyWith({
    List<TopupChannel>? paymentMethods,
    TopupChannel? selectedPaymentMethod,
  }) {
    return PaymentMethodsFetchedSuccess(
      paymentMethods: paymentMethods ?? this.paymentMethods,
      selectedPaymentMethod:
          selectedPaymentMethod ?? this.selectedPaymentMethod,
    );
  }
}

class PaymentMethodsFetchedFailure extends PaymentMethodsCardState {
  final String message;

  const PaymentMethodsFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class PaymentMethodChangedSuccess extends PaymentMethodsCardState {
  final TopupChannel selectedPaymentMethod;

  const PaymentMethodChangedSuccess(this.selectedPaymentMethod);

  @override
  List<Object> get props => [selectedPaymentMethod];
}

class PaymentMethodsSuccess extends PaymentMethodsCardState {
  final String? selectedPaymentMethod;

  const PaymentMethodsSuccess({
    this.selectedPaymentMethod,
  });

  @override
  List<Object?> get props => [selectedPaymentMethod];

  PaymentMethodsSuccess copyWith({
    String? selectedPaymentMethod,
  }) {
    return PaymentMethodsSuccess(
      selectedPaymentMethod:
          selectedPaymentMethod ?? this.selectedPaymentMethod,
    );
  }
}
