// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registry_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegistrationInfo _$RegistrationInfoFromJson(Map<String, dynamic> json) =>
    RegistrationInfo(
      code: json['code'] as int?,
      plateNumber: json['plateNumber'] as String?,
      vehicleTypeId: json['vehicleTypeId'] as String?,
      chassicNumber: json['chassicNumber'] as String?,
      seatNumber: json['seatNumber'] as String?,
      cargoWeight: json['cargoWeight'] as String?,
      netWeight: json['netWeight'] as String?,
      pullingWeight: json['pullingWeight'] as String?,
      grossWeight: json['grossWeight'] as String?,
      owner: json['owner'] as String?,
      address: json['address'] as String?,
    );

Map<String, dynamic> _$RegistrationInfoToJson(RegistrationInfo instance) =>
    <String, dynamic>{
      'code': instance.code,
      'plateNumber': instance.plateNumber,
      'vehicleTypeId': instance.vehicleTypeId,
      'chassicNumber': instance.chassicNumber,
      'seatNumber': instance.seatNumber,
      'cargoWeight': instance.cargoWeight,
      'netWeight': instance.netWeight,
      'pullingWeight': instance.pullingWeight,
      'grossWeight': instance.grossWeight,
      'owner': instance.owner,
      'address': instance.address,
    };

RegistrationResponse _$RegistrationResponseFromJson(
        Map<String, dynamic> json) =>
    RegistrationResponse(
      data: json['data'] == null
          ? null
          : RegistrationInfo.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RegistrationResponseToJson(
        RegistrationResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
