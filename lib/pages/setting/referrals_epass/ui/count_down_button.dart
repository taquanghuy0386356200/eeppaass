import 'dart:async';

import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rxdart/rxdart.dart';

class CountDownButton extends StatefulWidget {
  const CountDownButton({
    Key? key,
    this.onTap,
    required this.title,
    required this.countDown,
    this.padding,
    this.borderRadius = 12.0,
    this.widthCustom,
    this.heightCustom = 56,
    this.enabled = true,
  }) : super(key: key);

  final void Function()? onTap;
  final String title;
  final EdgeInsets? padding;
  final double borderRadius;
  final bool enabled;
  final int countDown;
  final double? widthCustom;
  final double? heightCustom;

  @override
  State<CountDownButton> createState() => _CountDownButtonState();
}

class _CountDownButtonState extends State<CountDownButton> {
  Timer? _timer;
  late int _countDown;
  bool enabled = true;

  @override
  void initState() {
    super.initState();
    _countDown = widget.countDown;
    enabled = widget.enabled;
    if (!widget.enabled) {
      _startCountDown();
    }
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  void _startCountDown() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_countDown > 0) {
        setState(() {
          _countDown--;
          if (_countDown == 0) {
            enabled = true;
          }
        });
      } else {
        _timer?.cancel();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.widthCustom,
      height: widget.heightCustom,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(widget.borderRadius),
        color: enabled ? null : Colors.grey.shade300,
        gradient: enabled
            ? const LinearGradient(
                colors: <Color>[
                  ColorName.primaryGradientStart,
                  ColorName.primaryGradientEnd,
                ],
              )
            : null,
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: widget.padding ??
              EdgeInsets.symmetric(
                horizontal: 16.w,
                vertical: 12.h,
              ),
          visualDensity: VisualDensity.standard,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius),
          ),
          primary: enabled ? Colors.white : Colors.grey.shade600,
        ),
        onPressed: enabled ? widget.onTap : null,
        child: Text(
          !enabled ? 'Chờ $_countDown (s)' : widget.title,
          style: Theme.of(context).textTheme.button!.copyWith(
                fontSize: 16.sp,
                fontWeight: FontWeight.w700,
                color: enabled ? Colors.white : Colors.grey.shade600,
              ),
        ),
      ),
    );
  }
}
