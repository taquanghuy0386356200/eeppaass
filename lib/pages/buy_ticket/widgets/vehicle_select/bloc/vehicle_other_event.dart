part of 'vehicle_other_bloc.dart';

abstract class VehicleOtherEvent extends Equatable {
  const VehicleOtherEvent();
}

class VehicleOtherSearched extends VehicleOtherEvent {
  final String plateNumber;

  @override
  List<Object> get props => [plateNumber];

  const VehicleOtherSearched(this.plateNumber);
}
