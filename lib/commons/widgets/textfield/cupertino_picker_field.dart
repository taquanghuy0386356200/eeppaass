import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/commons/widgets/textfield/text_field_circular_progress_indicator.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PickerItem {
  final String title;

  PickerItem({
    required this.title,
  });
}

class PickerItemWithValue<T> extends PickerItem with EquatableMixin {
  final T value;

  PickerItemWithValue({
    required String title,
    required this.value,
  }) : super(title: title);

  @override
  List<Object> get props => [title];
}

class CupertinoPickerField<T extends PickerItem> extends StatefulWidget {
  final String? label;
  final String? hintText;
  final bool enabled;
  final bool isLoading;
  final bool isMandatory;
  final T? defaultItem;
  final List<T> items;
  final String? Function(String? value)? validator;
  final void Function(T? item)? onItemChanged;
  final String? errorText;

  const CupertinoPickerField({
    Key? key,
    this.label,
    this.hintText,
    this.enabled = true,
    this.isLoading = false,
    this.isMandatory = false,
    this.defaultItem,
    required this.items,
    this.validator,
    this.onItemChanged,
    this.errorText,
  }) : super(key: key);

  @override
  State<CupertinoPickerField<T>> createState() =>
      _CupertinoPickerFieldState<T>();
}

class _CupertinoPickerFieldState<T extends PickerItem>
    extends State<CupertinoPickerField<T>> {
  late TextEditingController _textController;
  var _scrollController = FixedExtentScrollController();
  int? _selectedIndex;

  @override
  void initState() {
    _textController = TextEditingController(text: widget.defaultItem?.title);
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PrimaryTextField(
      controller: _textController,
      labelText: widget.label,
      hintText: widget.hintText,
      maxLength: 20,
      isMandatory: widget.isMandatory,
      hasClearButton: false,
      suffix: widget.isLoading
          ? const TextFieldCircularProgressIndicator()
          : Assets.icons.chevronDown.svg(),
      readonly: true,
      validator: widget.validator,
      enabled: widget.enabled,
      onTap: widget.enabled ? () => _showPicker(context, widget.items) : null,
      errorText: widget.errorText,
    );
  }

  void _showPicker(BuildContext context, List<T> items) {
    showCupertinoModalPopup(
      context: context,
      builder: (context) {
        _scrollController =
            FixedExtentScrollController(initialItem: _selectedIndex ?? 0);

        return CupertinoActionSheet(
          actions: [
            SizedBox(
              height: 200.h,
              child: CupertinoPicker(
                scrollController: _scrollController,
                itemExtent: 44.h,
                onSelectedItemChanged: (index) {
                  setState(() => _selectedIndex = index);
                  _textController.text = items[index].title;
                  widget.onItemChanged?.call(items[index]);
                },
                children: items
                    .map((item) => Center(
                          child: Text(item.title),
                        ))
                    .toList(),
              ),
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: Text(
              'Đóng',
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: ColorName.textGray1,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.w600,
                  ),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        );
      },
    );
  }
}
