part of 'balance_alert_bloc.dart';

abstract class BalanceAlertState extends Equatable {
  const BalanceAlertState();
}

class BalanceAlertInitial extends BalanceAlertState {
  @override
  List<Object> get props => [];
}

class BalanceAlertUpdatedInProgress extends BalanceAlertState {
  @override
  List<Object> get props => [];
}

class BalanceAlertUpdatedSuccess extends BalanceAlertState {
  @override
  List<Object> get props => [];
}

class BalanceAlertUpdatedFailure extends BalanceAlertState {
  final String message;

  const BalanceAlertUpdatedFailure(this.message);

  @override
  List<Object> get props => [message];
}