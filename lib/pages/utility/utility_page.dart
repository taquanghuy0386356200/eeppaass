import 'package:auto_route/auto_route.dart';
import 'package:car_doctor_epass_sos/car_doctor_epass_sos.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/extensions/image_extension.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/bloc/sms_register/sms_register_bloc.dart';
import 'package:epass/pages/home/bloc/home_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/parking_roadSide_page.dart';
import 'package:epass/pages/utility/car_doctor/car_doctor_bloc.dart';
import 'package:epass/pages/utility/car_doctor/car_doctor_fnc.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/utility/bloc/utility_bloc.dart';
import 'package:epass/pages/utility/widgets/sms_text_high_light.dart';
import 'package:epass/pages/utility/widgets/utility_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:url_launcher/url_launcher_string.dart';

class UtilityPage extends StatefulWidget {
  const UtilityPage({Key? key}) : super(key: key);

  @override
  State<UtilityPage> createState() => _UtilityPageState();
}

class _UtilityPageState extends State<UtilityPage>
    with AutomaticKeepAliveClientMixin {
  bool _firstRun = true;
  late BuildContext providerContext;
  late String action = 'SOS';

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _firstRun = false);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final utilityButtons = [
      UtilityButton(
        icon: Assets.icons.invoicePeriod.svg(),
        title: 'Chu kỳ \nhoá đơn',
        onTap: () => context.pushRoute(const InvoiceSettingRoute()),
      ),
      UtilityButton(
        icon: Assets.icons.station.svg(),
        title: 'Điểm \ndịch vụ',
        onTap: () => context.pushRoute(const PlaceTabRoute()),
      ),
      UtilityButton(
        icon: Assets.icons.coolPenalty.svg(),
        title: 'Tra cứu \nphạt nguội',
        onTap: () async {
          if (await canLaunchUrlString(Constant.coolPenaltyUrl)) {
            context.pushRoute(const CoolPenaltyRoute());
          }
        },
      ),
      UtilityButton(
        icon: Assets.images.home.sosSdkCardoctor.svg(width: 40.0, height: 40.0),
        title: 'Cứu hộ\n24/7',
        onTap: () async {
          providerContext.loaderOverlay.show();
          action = 'SOS';
          providerContext.read<CarDoctorBloc>().add(GetCarDoctorConfig());
        },
      ),
      // UtilityButton(
      //   icon: Assets.images.carPark.image(width: 40.0, height: 40.0),
      //   title: 'Đỗ xe lòng đường',
      //   onTap: () =>
      //       context.pushRoute(const ParkingBlocProviderRoadSideRoute()),
      // ),
      UtilityButton(
        icon: Assets.icons.vietmap.svg(width: 40, height: 40),
        title: 'Kết nối\nVietmap',
        onTap: () async {
          context.pushRoute(const VietMapRoute());
        },
      ),

      Stack(
        fit: StackFit.expand,
        children: [
          UtilityButton(
            icon: Image.asset(
              ImageAssests.smsRegister,
              height: 40.h,
              width: 40.w,
            ),
            title: 'Đăng ký SMS',
            onTap: () {
              context.pushRoute(const SmsRegisterRoute());
            },
          ),
          BlocBuilder<SmsRegisterBloc, SmsRegisterTotalState>(
            builder: (context, state) {
              if (state.isRegister) {
                return const SizedBox.shrink();
              } else {
                return Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.r),
                      color: ColorName.primaryGradientStart,
                    ),
                    padding:
                        EdgeInsets.symmetric(vertical: 2.h, horizontal: 3.w),
                    child: const BlinkingText(
                      text: 'Đăng ký SMS',
                      fontSize: 12,
                    ),
                  ),
                );
              }
            },
          ),
        ],
      ),
      UtilityButton(
        icon: Image.asset(
          ImageAssests.referralsClient,
          width: 40.w,
          height: 40.h,
        ),
        title: 'Giới thiệu ePass',
        onTap: () async {
          context.pushRoute(const ReferralsClientRoute());
        },
      ),
    ];
    return WillPopScope(
      onWillPop: () async => false,
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) {
            providerContext = context;
            return CarDoctorBloc(
                userRepository: getIt<IUserRepository>(),
                appBloc: context.read<AppBloc>());
          }),
          BlocProvider(
              create: (context) =>
                  UtilityBloc(userRepository: getIt<IUserRepository>())
                    ..add(const UnavailableServiceFetched())),
        ],
        child: MultiBlocListener(
          listeners: [
            BlocListener<CarDoctorBloc, CarDoctorState>(
              listener: (context, state) {
                providerContext = context;
                if (state is GetAccessTokenSuccess) {
                  providerContext.loaderOverlay.hide();
                  switch (action) {
                    case 'Accessories':
                      // context.pushRoute(WebViewRoute(accessToken: state.accessToken));
                      break;
                    case 'SOS':
                      final option = CarDoctorOption(
                        bearerToken: state.accessToken,
                        sosUrl: '',
                        apiUrl: dotenv.env['APIURL']!,
                        imageUrl: dotenv.env['IMAGEURL']!,
                        mapKey: dotenv.env['MAPKEY']!,
                      );
                      final sdkSos = CarDoctorSdkSos.instance;
                      sdkSos.init(option);
                      sdkSos.open(context);
                      break;
                    default:
                      break;
                  }
                } else {
                  providerContext.loaderOverlay.hide();
                }
                if (state is GetConfigSuccess) {
                  CarDoctorFnc.requestLocationPermission(
                      providerContext, state.config.name ?? '', 'SOS');
                }
                if (state is GetConfigFail) {
                  providerContext.loaderOverlay.hide();
                  showErrorSnackBBar(context: context, message: state.message);
                }
                if (state is GetAccessTokenFail) {
                  providerContext.loaderOverlay.hide();
                  showErrorSnackBBar(context: context, message: state.message);
                }
              },
            ),
            BlocListener<UtilityBloc, UtilityState>(
              listener: (context, state) {
                if (state is GetServiceSuccess) {
                  if (!state.unavailableServices
                      .contains('ROAD_PARKING_SERVICE')) {
                    utilityButtons.add(UtilityButton(
                      icon: Assets.images.carPark
                          .image(width: 40.0, height: 40.0),
                      title: 'Đỗ xe lòng đường',
                      onTap: () => context
                          .pushRoute(const ParkingBlocProviderRoadSideRoute()),
                    ));
                  }
                }
                if (state is GetServiceFail) {
                  utilityButtons.add(UtilityButton(
                    icon:
                        Assets.images.carPark.image(width: 40.0, height: 40.0),
                    title: 'Đỗ xe lòng đường',
                    onTap: () => context
                        .pushRoute(const ParkingBlocProviderRoadSideRoute()),
                  ));
                }
              },
            ),
            BlocListener<UtilityBloc, UtilityState>(
              listener: (context, state) {
                if (state is GetServiceSuccess) {
                  if (!state.unavailableServices.contains('PARKING_SERVICE')) {
                    utilityButtons.add(UtilityButton(
                      icon: Assets.icons.parkingService.svg(),
                      title: 'Dịch vụ\ngửi xe',
                      onTap: () =>
                          context.pushRoute(const ParkingBlocProviderRoute()),
                    ));
                  }
                }
                if (state is GetServiceFail) {
                  utilityButtons.add(UtilityButton(
                    icon: Assets.icons.parkingService.svg(),
                    title: 'Dịch vụ\ngửi xe',
                    onTap: () =>
                        context.pushRoute(const ParkingBlocProviderRoute()),
                  ));
                }
              },
            ),
          ],
          child: BlocBuilder<CarDoctorBloc, CarDoctorState>(
            builder: (context, state) {
              providerContext = context;
              return BasePage(
                title: 'Tiện ích',
                backgroundColor: ColorName.backgroundAvatar,
                child: Stack(
                  children: [
                    FadeAnimation(
                      delay: 0.5,
                      playAnimation: _firstRun,
                      child: const GradientHeaderContainer(),
                    ),
                    SafeArea(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 10.w,
                          vertical: 16.h,
                        ),
                        child: BlocBuilder<UtilityBloc, UtilityState>(
                          builder: (context, state) {
                            return GridView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: utilityButtons.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 0.w,
                                mainAxisSpacing: 0.h,
                                childAspectRatio: 0.85,
                              ),
                              itemBuilder: (context, index) {
                                return FadeAnimation(
                                  delay:
                                      1 + (index / (utilityButtons.length * 2)),
                                  playAnimation: _firstRun,
                                  child: utilityButtons[index],
                                );
                              },
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
