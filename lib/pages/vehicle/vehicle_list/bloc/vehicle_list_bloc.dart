import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/models/vehicle/vehicle.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/pages/bloc/bloc_transforms.dart';
import 'package:equatable/equatable.dart';
import 'package:simple_result/simple_result.dart';

part 'vehicle_list_event.dart';

part 'vehicle_list_state.dart';

class VehicleListBloc extends Bloc<VehicleListEvent, VehicleListState> {
  final IVehicleRepository _vehicleRepository;

  final int _pageSize;
  int _startRecord = 0;

  final List<int> _statuses;
  final List<int> _activeStatuses;

  VehicleListBloc({
    required IVehicleRepository vehicleRepository,
    List<int> statuses = const [0, 1],
    List<int> activeStatuses = const [0, 1, 2, 3, 4, 5],
    int pageSize = 20,
  })  : _pageSize = pageSize,
        _vehicleRepository = vehicleRepository,
        _statuses = statuses,
        _activeStatuses = activeStatuses,
        super(const VehicleListState(listData: [])) {
    on<VehicleListFetched>(_onVehicleListFetched);
    on<VehicleListRefreshed>(_onVehicleListRefreshed);
    on<VehicleListLoadMore>(_onVehicleListLoadMore);
    on<VehicleListSearched>(
      _onVehicleListSearched,
      transformer: throttleDroppable(const Duration(milliseconds: 100)),
    );
  }

  FutureOr<void> _onVehicleListFetched(
    VehicleListFetched event,
    Emitter<VehicleListState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isLoading: true,
      listData: const [],
      plateNumber: null,
      error: null,
    ));

    final result = await _getVehicleList(
      statuses: _statuses,
      activeStatuses: _activeStatuses,
    );

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isLoading: false,
            listData: data.listData,
            total: data.count,
            isFull: data.listData.length == data.count,
            plateNumber: null,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isLoading: false,
        error: failure.message,
        plateNumber: null,
      )),
    );
  }

  FutureOr<void> _onVehicleListRefreshed(
    VehicleListRefreshed event,
    Emitter<VehicleListState> emit,
  ) async {
    _startRecord = 0;

    emit(state.copyWith(
      isRefreshing: true,
      plateNumber: state.plateNumber,
      error: null,
    ));

    final result = await _getVehicleList(
      statuses: _statuses,
      activeStatuses: _activeStatuses,
    );

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        emit(
          state.copyWith(
            isRefreshing: false,
            total: data.count,
            isFull: data.listData.length == data.count,
            listData: data.listData,
            plateNumber: state.plateNumber,
            error: null,
          ),
        );
      },
      failure: (failure) => emit(state.copyWith(
        isRefreshing: false,
        plateNumber: state.plateNumber,
        error: failure.message,
      )),
    );
  }

  FutureOr<void> _onVehicleListLoadMore(
    VehicleListLoadMore event,
    Emitter<VehicleListState> emit,
  ) async {
    final listData = state.listData;

    if (state.isLoadingMore ||
        state.isFull ||
        (state.listData.length % _pageSize) > 0) {
      return;
    }

    emit(state.copyWith(
      isLoadingMore: true,
      plateNumber: state.plateNumber,
      error: null,
    ));

    final result = await _getVehicleList(
      statuses: _statuses,
      activeStatuses: _activeStatuses,
    );

    result.when(
      success: (data) {
        _startRecord += _pageSize;

        listData.addAll(data.listData);

        emit(state.copyWith(
          isLoadingMore: false,
          listData: listData,
          total: data.count,
          isFull: listData.length == data.count,
          plateNumber: state.plateNumber,
          error: null,
        ));
      },
      failure: (failure) => emit(state.copyWith(
        isLoadingMore: false,
        plateNumber: state.plateNumber,
        error: failure.message,
      )),
    );
  }

  FutureOr<void> _onVehicleListSearched(
    VehicleListSearched event,
    Emitter<VehicleListState> emit,
  ) async {
    final plateNumber = event.plateNumber?.trim();
    if (plateNumber != null) {
      emit(state.copyWith(
        // isLoading: true,
        plateNumber: plateNumber.isEmpty ? null : plateNumber,
        error: null,
      ));

      _startRecord = 0;

      final result = await _getVehicleList(
        statuses: _statuses,
        activeStatuses: _activeStatuses,
      );

      result.when(
        success: (data) {
          _startRecord += _pageSize;

          emit(
            state.copyWith(
              isLoading: false,
              listData: data.listData,
              total: data.count,
              isFull: data.listData.length == data.count,
              plateNumber: state.plateNumber,
              error: null,
            ),
          );
        },
        failure: (failure) => emit(state.copyWith(
          isLoading: false,
          plateNumber: state.plateNumber,
          error: failure.message,
        )),
      );
    }
  }

  Future<Result<VehicleListDataNotNull, Failure>> _getVehicleList({
    List<int>? statuses,
    List<int>? activeStatuses,
  }) async {
    final result = await _vehicleRepository.getUserVehicles(
      pageSize: _pageSize,
      startRecord: _startRecord,
      statuses: statuses,
      activeStatuses: activeStatuses,
      plateNumber: state.plateNumber,
    );
    return result;
  }
}
