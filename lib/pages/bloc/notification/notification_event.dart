part of 'notification_bloc.dart';

@immutable
abstract class NotificationEvent extends Equatable {
  const NotificationEvent();
}

class NotificationFetched extends NotificationEvent {
  final int? notificationType;

  const NotificationFetched({this.notificationType});

  @override
  List<Object?> get props => [notificationType];
}

class NotificationRefreshed extends NotificationEvent {
  const NotificationRefreshed();

  @override
  List<Object?> get props => [];
}

class NotificationLoadMore extends NotificationEvent {
  const NotificationLoadMore();

  @override
  List<Object?> get props => [];
}

class NotificationSearched extends NotificationEvent {
  final String? input;

  const NotificationSearched(this.input);

  @override
  List<Object?> get props => [input];
}

class NotificationRead extends NotificationEvent {
  final int notificationId;

  const NotificationRead(this.notificationId);

  @override
  List<Object> get props => [notificationId];
}
