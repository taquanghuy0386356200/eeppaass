import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/services/vnpay/vnpay_constant.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment_request.dart';
import 'package:epass/commons/services/vnpay/vnpay_payment_response.dart';
import 'package:epass/flavors.dart';
import 'package:flutter/services.dart';
import 'package:simple_result/simple_result.dart';

class VNPayService {
  static const MethodChannel _channel =
      MethodChannel(VNPayConstant.channelName);

  Future<Result<VNPayPaymentResponse, Failure>> pushPayment(
    VNPayRequest request,
  ) async {
    try {
      final vnpayPushPaymentRequest = VNPayPushPaymentRequest(
        paymentUrl: request.paymentUrl,
        title: 'VNPAY',
        beginColor: '#FFFFFF',
        endColor: '#FFFFFF',
        titleColor: '#000000',
        tmnCode: request.vnpTmnCode,
        isSandbox: F.appFlavor != Flavor.prod,
      );

      Map<String, dynamic>? result = await _channel.invokeMapMethod(
        VNPayConstant.methodPushPayment,
        vnpayPushPaymentRequest.toMap(),
      );
      if (result == null) {
        return const Result.failure(UnknownFailure());
      }
      final response = VNPayPaymentResponse.fromMap(result);
      if (response.paymentStatus == VNPayPaymentStatus.failure) {
        return const Result.failure(UnknownFailure());
      }
      return Result.success(response);
    } on PlatformException catch (error) {
      return Result.failure(UnknownFailure(message: error.toString()));
    }
  }
}
