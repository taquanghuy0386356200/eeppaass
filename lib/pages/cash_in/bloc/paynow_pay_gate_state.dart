part of 'paynow_pay_gate_bloc.dart';

abstract class PayNowPayGateState extends Equatable {
  const PayNowPayGateState();
}

class PayNowPayGateInitial extends PayNowPayGateState {
  @override
  List<Object> get props => [];
}

class PayNowPayGateLinkedCompletedSuccess extends PayNowPayGateState {
  final String orderId;
  final int amount;

  const PayNowPayGateLinkedCompletedSuccess({
    required this.orderId,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, amount];
}

class PayNowPayGateLinkedCompletedFailure extends PayNowPayGateState {
  final String message;

  const PayNowPayGateLinkedCompletedFailure(this.message);

  @override
  List<Object> get props => [message];
}
