part of 'avatar_bloc.dart';

abstract class AvatarEvent extends Equatable {
  const AvatarEvent();
}

class AvatarDownloaded extends AvatarEvent {
  const AvatarDownloaded();

  @override
  List<Object> get props => [];
}

class AvatarUploaded extends AvatarEvent {
  final String filePath;

  const AvatarUploaded({required this.filePath});

  @override
  List<Object> get props => [filePath];
}
