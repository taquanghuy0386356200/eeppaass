part of 'car_doctor_bloc.dart';

@immutable
abstract class CarDoctorEvent extends Equatable {
  const CarDoctorEvent();
}

class GetAccessToken extends CarDoctorEvent {
  @override
  List<Object?> get props => [];
}

class GetAnonymousAccessToken extends CarDoctorEvent {
  @override
  List<Object?> get props => [];
}

class GetCarDoctorConfig extends CarDoctorEvent {
  @override
  List<Object?> get props => [];
}
