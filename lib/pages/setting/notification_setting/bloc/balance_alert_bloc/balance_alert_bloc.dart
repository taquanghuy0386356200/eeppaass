import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/user_repository.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:equatable/equatable.dart';

part 'balance_alert_event.dart';

part 'balance_alert_state.dart';

class BalanceAlertBloc extends Bloc<BalanceAlertEvent, BalanceAlertState> {
  final IUserRepository _userRepository;
  final AppBloc _appBloc;

  BalanceAlertBloc({required IUserRepository userRepository, required AppBloc appBloc})
      : _userRepository = userRepository,
        _appBloc = appBloc,
        super(BalanceAlertInitial()) {
    on<BalanceAlertUpdated>(_onBalanceAlertUpdated);
  }

  FutureOr<void> _onBalanceAlertUpdated(
    BalanceAlertUpdated event,
    emit,
  ) async {
    emit(BalanceAlertUpdatedInProgress());

    final result = await _userRepository.updateBalanceAlert(
      isAlertMoney: event.isBalanceAlertOn,
      alertMoney: event.alertAmount,
    );

    await result.when(
      success: (_) async {
        // Get user details (contract)
        final getUserResult = await _userRepository.getUser();

        getUserResult.when(
          success: (user) {
            // Update [AppBloc] user info
            _appBloc.add(UpdateAppUser(user));

            // Emit authentication success state
            emit(BalanceAlertUpdatedSuccess());
          },
          failure: (failure) => emit(BalanceAlertUpdatedFailure(failure.message ?? 'Có lỗi xảy ra')),
        );
      },
      failure: (failure) => emit(BalanceAlertUpdatedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }
}
