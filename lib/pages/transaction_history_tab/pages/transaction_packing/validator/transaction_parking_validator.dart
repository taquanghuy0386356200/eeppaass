class TransactionParkingValidator {
  String? startDateValidator(String? startDate) {
    if (startDate == null || startDate.isEmpty) return 'Vui lòng nhập ngày lấy xe';
    return null;
  }

  String? endDateValidator(String? endDate) {
    if (endDate == null || endDate.isEmpty) return 'Vui lòng nhập ngày lấy xe';
    return null;
  }
}
