part of 'biometric_login_bloc.dart';

@immutable
abstract class BiometricLoginEvent extends Equatable {
  const BiometricLoginEvent();
}

class GetBiometricAuthenticationType extends BiometricLoginEvent {
  @override
  List<Object> get props => [];
}

class BiometricAuthenticated extends BiometricLoginEvent {
  @override
  List<Object> get props => [];
}
