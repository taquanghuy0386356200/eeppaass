// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mb_get_token.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MBGetTokenRequest _$MBGetTokenRequestFromJson(Map<String, dynamic> json) =>
    MBGetTokenRequest(
      grant_type: json['grant_type'] as String,
    );

Map<String, dynamic> _$MBGetTokenRequestToJson(MBGetTokenRequest instance) =>
    <String, dynamic>{
      'grant_type': instance.grant_type,
    };

MBGetTokenResponse _$MBGetTokenResponseFromJson(Map<String, dynamic> json) =>
    MBGetTokenResponse(
      access_token: json['access_token'] as String,
      expires_in: json['expires_in'] as int,
      scope: json['scope'] as String,
      issued_at: json['issued_at'] as String,
      token_type: json['token_type'] as String,
    );

Map<String, dynamic> _$MBGetTokenResponseToJson(MBGetTokenResponse instance) =>
    <String, dynamic>{
      'access_token': instance.access_token,
      'expires_in': instance.expires_in,
      'scope': instance.scope,
      'issued_at': instance.issued_at,
      'token_type': instance.token_type,
    };
