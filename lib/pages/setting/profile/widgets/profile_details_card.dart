import 'package:epass/commons/models/user/user.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfileDetailsCard extends StatelessWidget {
  const ProfileDetailsCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShadowCard(
      borderRadius: 16.0,
      shadowOpacity: 0.2,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(
          vertical: 16.h,
          horizontal: 16.w,
        ),
        child: BlocSelector<AppBloc, AppState, User?>(
          selector: (state) => state.user,
          builder: (context, user) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _TextRow(title: 'Họ và tên', value: user?.userName ?? ''),
                SizedBox(height: 10.h),
                _TextRow(title: 'Giới tính', value: user?.gender?.title ?? ''),
                SizedBox(height: 10.h),
                _TextRow(title: 'CMND/CCCD', value: user?.identifier ?? ''),
                SizedBox(height: 10.h),
                _TextRow(title: 'Cấp ngày', value: user?.dateOfIssue ?? ''),
                SizedBox(height: 10.h),
                _TextRow(title: 'Địa chỉ', value: user?.address ?? ''),
              ],
            );
          },
        ),
      ),
    );
  }
}

class _TextRow extends StatelessWidget {
  final String title;
  final String value;

  const _TextRow({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: ColorName.disabledBorderColor,
              ),
        ),
        SizedBox(width: 16.w),
        Expanded(
          child: Text(
            value,
            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                  fontWeight: FontWeight.w600,
                ),
            textAlign: TextAlign.right,
          ),
        ),
      ],
    );
  }
}
