class VehicleOtherPageValidator {
  String? plateNumberValidator(String? plateNumber) {
    if (plateNumber == null || plateNumber.isEmpty) {
      return 'Vui lòng nhập Biển số xe';
    } else if (plateNumber.trim().length < 4) {
      return 'Biển số xe không hợp lệ';
    }
    return null;
  }
}
