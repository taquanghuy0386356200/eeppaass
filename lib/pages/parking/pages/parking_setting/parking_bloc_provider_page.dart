import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/repo/parking_repository.dart';
import 'package:epass/commons/repo/vehicle_repository.dart';
import 'package:epass/commons/services/viettel_money/viettel_money_service.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home/bloc/viettel_money_link_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/passcode_bottom_sheet/bloc/passcode_bottom_sheet_bloc.dart';
import 'package:epass/pages/parking/pages/parking_setting/widgets/policy_bottom_sheet/bloc/policy_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/parking_setting_bloc.dart';

class ParkingBlocProviderPage extends StatelessWidget {
  const ParkingBlocProviderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ParkingSettingBloc(
            appBloc: getIt<AppBloc>(),
            parkingRepository: getIt<IParkingRepository>(),
          ),
        ),
        BlocProvider(
            create: (context) => PolicyBloc()
        ),
        BlocProvider(
            create: (context) => PasscodeBottomSheetBloc(
              appBloc: getIt<AppBloc>(),
              parkingRepository: getIt<IParkingRepository>(),
            ),
        ),
        BlocProvider(
          create: (context) => ViettelMoneyLinkBloc(
            viettelMoneyService: getIt<ViettelMoneyService>(),
            vehicleRepository: getIt<IVehicleRepository>(),
          ),
        ),
      ],
      child: const AutoRouter(),
    );
  }
}
