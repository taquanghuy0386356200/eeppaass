part of 'invoice_details_bloc.dart';

abstract class InvoiceDetailsState extends Equatable {
  const InvoiceDetailsState();
}

class InvoiceDetailsInitial extends InvoiceDetailsState {
  @override
  List<Object> get props => [];
}

class InvoiceDownloadedLoading extends InvoiceDetailsState {
  @override
  List<Object> get props => [];
}
class InvoiceDiscardLoading extends InvoiceDetailsState {
  @override
  List<Object> get props => [];
}

class InvoiceDownloadedSuccess extends InvoiceDetailsState {
  final File file;

  const InvoiceDownloadedSuccess(this.file);

  @override
  List<Object> get props => [file];
}

class InvoiceDownloadedFailure extends InvoiceDetailsState {
  final String message;

  const InvoiceDownloadedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class InvoiceViewedSuccess extends InvoiceDetailsState {
  final String invoiceId;
  final List<int> invoiceData;

  const InvoiceViewedSuccess({
    required this.invoiceId,
    required this.invoiceData,
  });

  @override
  List<Object> get props => [invoiceData];
}
