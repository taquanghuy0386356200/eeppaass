import 'package:epass/commons/services/permission/permission.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/utility/car_doctor/car_doctor_bloc.dart';
import 'package:epass/pages/utility/widgets/car_doctor_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:permission_handler/permission_handler.dart';

class CarDoctorFnc {
  static void requestLocationPermission(
      BuildContext providerContext, String content, String action) async {
    providerContext.loaderOverlay.hide();
    var status = await Permission.location.status;
    if (status.isGranted || action == 'Accessories') {
      // showDialogCarDoctorRequestUserInfo(content);
      providerContext.loaderOverlay.show();
      providerContext.read<CarDoctorBloc>().add(GetAccessToken());
    } else {
      final permissionStatus = await requestPermission(Permission.location);
      permissionStatus.when(success: (_) {
        // showDialogCarDoctorRequestUserInfo(content);
        providerContext.loaderOverlay.show();
        providerContext.read<CarDoctorBloc>().add(GetAccessToken());
      }, failure: (_) {
        // showDialogCarDoctorRequestUserInfo(content);
        providerContext.loaderOverlay.show();
        providerContext.read<CarDoctorBloc>().add(GetAccessToken());
      });
    }
  }

  void showDialogCarDoctorRequestUserInfo(
      BuildContext context, String content) {
    final isSyncData = context.read<AppBloc>().state.isSyncCarDoctor;
    if (isSyncData != null && isSyncData) {
      context.loaderOverlay.show();
      context.read<CarDoctorBloc>().add(GetAccessToken());
    } else {
      showDialog(
        context: context,
        builder: (dialogContext) {
          return CarDoctorDialog(
            content: content,
            onRejectButtonClicked: () {
              context.loaderOverlay.show();
              Navigator.of(dialogContext).pop();
              context.read<CarDoctorBloc>().add(GetAnonymousAccessToken());
            },
            onAgreeButtonClick: () {
              context.loaderOverlay.show();
              context
                  .read<AppBloc>()
                  .add(const UpdateSyncCarDoctorStatus(true));
              Navigator.of(dialogContext).pop();
              context.read<CarDoctorBloc>().add(GetAccessToken());
            },
          );
        },
      );
    }
  }
}
