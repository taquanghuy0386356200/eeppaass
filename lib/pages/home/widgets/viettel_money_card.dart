import 'package:epass/commons/models/contract_payment/contract_payment.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/pages/home/bloc/home_bloc.dart';
import 'package:epass/pages/home/bloc/viettel_money_link_bloc.dart';
import 'package:epass/pages/home/widgets/viettel_money_card_linked.dart';
import 'package:epass/pages/home/widgets/viettel_money_card_unlinked.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ViettelMoneyCard extends StatelessWidget {
  final double shadowOpacity;

  const ViettelMoneyCard({
    Key? key,
    required this.shadowOpacity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShadowCard(
      shadowOpacity: shadowOpacity,
      child: BlocSelector<HomeBloc, HomeState, ContractPayment?>(
        selector: (state) => state is HomeLoaded ? state.contractPayment : null,
        builder: (context, contractPayment) {
          return BlocListener<ViettelMoneyLinkBloc, ViettelMoneyLinkState>(
            listener: (context, state) {
              if (state is ViettelMoneyLinkAccountInProgress) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
                if (state is ViettelMoneyLinkAccountSuccess || state is ViettelMoneyUnlinkAccountSuccess) {
                  context.read<HomeBloc>().add(ContractPaymentFetched());
                }
              }
            },
            child: contractPayment != null
                ? ViettelMoneyCardLinked(contractPayment: contractPayment)
                : const ViettelMoneyCardUnlinked(),
          );
        },
      ),
    );
  }
}
