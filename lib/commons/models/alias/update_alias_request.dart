class UpdateAliasRequest {
  final String aliasName;

  const UpdateAliasRequest({
    required this.aliasName,
  });

  @override
  String toString() {
    return 'UpdateAliasRequest{' ' aliasName: $aliasName,' '}';
  }

  Map<String, dynamic> toJson() {
    return {
      'aliasName': aliasName,
    };
  }
}
