import 'package:epass/commons/models/feedback/feedback_attachment_file.dart';
import 'package:json_annotation/json_annotation.dart';

/// ticketChannel : "2"
/// custId : ""
/// contractNo : ""
/// contractId : ""
/// custTypeId : ""
/// custName : ""
/// priorityId : "1"
/// l1TicketTypeId : ""
/// feedBack : ""
/// contentReceive : ""
/// ticketKind : "1"
/// phoneNumber : ""
/// phoneContact : ""
/// attachmentFiles : ""
/// otp : ""

/*
 ticketChannel  bắt buộc    body    Number      Fix giá trị = 2
 custId         bắt buộc    body    Number      Truyền giá trị custId từ api ../crm2/api/v1/../ocsInfo    Mã khách hàng
 contractNo     bắt buộc    body    String      Truyền giá trị contractNo từ api ../crm2/api/v1/../ocsInfo    Số hợp đồng
 contractId     bắt buộc    body    Number      ID hợp đồng
 custTypeId                 body    Number      Loại khách hàng
 custName       bắt buộc    body    String      Người phản ánh
 priorityId     bắt buộc    body    Number      Fix giá trị = 1
 l1TicketTypeId             body    Number      Truyền ID nhóm PA    Chủ đề góp ý (xem mô tả API lấy DS nhóm PA )
 feedBack                   body    String      Text góp ý khi chọn chủ đề góp ý là Khác
 contentReceive bắt buộc    body    String      Ý kiến đóng góp
 ticketKind                 body    Number      Fix giá trị = 1
 phoneNumber                body    String      Truyền giá trị noticePhoneNumber từ api ../crm2/api/v1/../ocsInfo
 phoneContact               body    String      Truyền giá trị noticePhoneNumber từ api ../crm2/api/v1/../ocsInfo
 attachmentFiles            body    List Object File dẫn chứng
 fileName                           String      Đuôi file chỉ accept: ".JPG", ".PNG", ".TIFF", ".BMP", ".PDF", ".JPEG", ".WEBP", "RAR", "ZIP", "XLSX", "XLS", "TXT", "DOC", "DOCX"    Tên file dẫn chứng
 base64Data                         String      Dung lượng k quá 5MB    Chuỗi base64 data file dẫn chứng
 */

part 'feedback_request.g.dart';

@JsonSerializable(explicitToJson: true)
class FeedbackRequest {
  FeedbackRequest({
    this.ticketChannel = '2',
    required this.custId,
    required this.contractNo,
    required this.contractId,
    required this.custTypeId,
    required this.custName,
    this.priorityId = '1',
    required this.l1TicketTypeId,
    this.feedBack,
    required this.contentReceive,
    this.ticketKind = '1',
    required this.phoneNumber,
    required this.phoneContact,
    this.attachmentFiles,
    required this.otp,
  });

  final String ticketChannel;
  final String custId;
  final String contractNo;
  final String contractId;
  final String custTypeId;
  final String custName;
  final String priorityId;
  final String l1TicketTypeId;
  final String? feedBack;
  final String contentReceive;
  final String ticketKind;
  final String phoneNumber;
  final String phoneContact;
  final List<FeedbackAttachmentFileRequest>? attachmentFiles;
  final String otp;

  Map<String, dynamic> toJson() => _$FeedbackRequestToJson(this);
}
