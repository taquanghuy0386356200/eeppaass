import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/pages/home/widgets/banner_carousel.dart';
import 'package:epass/pages/home/widgets/features_grid.dart';
import 'package:epass/pages/home/widgets/home_header.dart';
import 'package:epass/pages/home/widgets/info_and_viettel_pay_card.dart';
import 'package:epass/pages/home/widgets/mascot_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'component/app_store_widget.dart';

class HomeWidgets extends StatefulWidget {
  const HomeWidgets({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeWidgets> createState() => _HomeWidgetsState();
}

class _HomeWidgetsState extends State<HomeWidgets> with AutomaticKeepAliveClientMixin {
  bool _firstRun = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _firstRun = false;
    });
  }
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final homeWidgets = [
      SizedBox(height: 8.h),

      // LOGO AND SEARCH ICON
      const HomeHeader(),

      // INFO CARD
      SizedBox(height: 8.h),
      const InfoAndViettelPayCards(),

      // FEATURE GRID
      SizedBox(height: 16.h),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.w),
        child: const FeaturesGrid(),
      ),

      // APP STORE
      const AppStore(),
      // BANNER CAROUSEL
      SizedBox(height: 32.h),
      const BannerCarousel(),
      SizedBox(height: 32.h),
    ];

    return Stack(
      children: [
        SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: Stack(
            children: [
              FadeAnimation(
                delay: 0.5,
                playAnimation: _firstRun,
                child: const GradientHeaderContainer(),
              ),
              SafeArea(
                child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: homeWidgets.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return FadeAnimation(
                      delay: index / (homeWidgets.length * 2),
                      playAnimation: _firstRun,
                      child: homeWidgets[index],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        MascotWidget(
          onTap: () => context.pushRoute(const SupportRouter()),
        ),
      ],
    );
  }
}
