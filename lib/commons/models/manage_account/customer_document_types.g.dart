// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_document_types.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerDocumentTypeTotalResponse _$CustomerDocumentTypeTotalResponseFromJson(
        Map<String, dynamic> json) =>
    CustomerDocumentTypeTotalResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) =>
              CustomerDocumentTypeResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CustomerDocumentTypeTotalResponseToJson(
        CustomerDocumentTypeTotalResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

CustomerDocumentTypeResponse _$CustomerDocumentTypeResponseFromJson(
        Map<String, dynamic> json) =>
    CustomerDocumentTypeResponse(
      id: json['id'] as int?,
      code: json['code'] as String?,
      val: json['val'] as String?,
    );

Map<String, dynamic> _$CustomerDocumentTypeResponseToJson(
        CustomerDocumentTypeResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'val': instance.val,
    };
