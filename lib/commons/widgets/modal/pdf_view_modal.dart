import 'package:flutter/material.dart';

class PDFViewModal extends StatelessWidget {
  final String? title;
  final Widget child;

  const PDFViewModal({
    Key? key,
    this.title,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.grey[800]),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(title ?? ''),
        centerTitle: true,
      ),
      body: child,
    );
  }
}
