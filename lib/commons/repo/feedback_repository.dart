import 'package:epass/commons/api/clients/cc/cc_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/commons/api/network_constants.dart';
import 'package:epass/commons/models/feedback/feedback_attachment_file.dart';
import 'package:epass/commons/models/feedback/feedback_request.dart';
import 'package:epass/commons/models/feedback/feedback_response.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:simple_result/simple_result.dart';

abstract class IFeedbackRepository {
  Future<Result<List<FeedbackModel>, Failure>> getFeedback({required FeedbackChannel feedbackChannel});

  Future<Result<String?, Failure>> createFeedback({
    required String custId,
    required String contractNo,
    required String contractId,
    required String custTypeId,
    required String custName,
    required String l1TicketTypeId,
    String? feedBack,
    required String contentReceive,
    required String phoneNumber,
    required String phoneContact,
    List<FeedbackAttachmentFileRequest> attachmentFiles,
    required String otp,
  });

  Future<Result<FeedbackTypeDataNotNull, Failure>> getFeedbackTypes({
    int? parentId,
    required FeedbackChannel feedbackChannel,
  });

  Future<Result<FeedbackTypeDataNotNull, Failure>> getFeedbackTypesById({
    required int parentId,
    required FeedbackChannel feedbackChannel,
  });

  Future<Result<String?, Failure>> feedbackRequestOtp({
    required int confirmType,
  });

  Future<Result<List<int>, Failure>> downloadAttachmentFile({
    required int attachmentId,
  });
}

class FeedbackRepository implements IFeedbackRepository {
  final CCClient _ccClient;

  FeedbackRepository({
    required CCClient ccClient,
  }) : _ccClient = ccClient;

  @override
  Future<Result<List<FeedbackModel>, Failure>> getFeedback({required FeedbackChannel feedbackChannel}) async {
    try {
      final response = await _ccClient.getFeedback(feedbackChannelValue: feedbackChannel.value);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.data ?? []);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<FeedbackTypeDataNotNull, Failure>> getFeedbackTypes({
    int? parentId,
    required FeedbackChannel feedbackChannel,
  }) async {
    try {
      final response = await _ccClient.getFeedbackTypes(
        parentId: parentId,
        feedbackChannelValue: feedbackChannel.value,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || count == null || listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(FeedbackTypeDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<FeedbackTypeDataNotNull, Failure>> getFeedbackTypesById({
    required int parentId,
    required FeedbackChannel feedbackChannel,
  }) async {
    try {
      final response = await _ccClient.getFeedbackTypes(
        parentId: parentId,
        feedbackChannelValue: feedbackChannel.value,
      );

      final count = response.data?.count;
      final listData = response.data?.listData;

      if (response.mess?.code != REQUEST_SUCCESS_CODE || count == null || listData == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }

      return Result.success(FeedbackTypeDataNotNull(
        listData: listData,
        count: count,
      ));
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> createFeedback({
    required String custId,
    required String contractNo,
    required String contractId,
    required String custTypeId,
    required String custName,
    required String l1TicketTypeId,
    String? feedBack,
    required String contentReceive,
    required String phoneNumber,
    required String phoneContact,
    List<FeedbackAttachmentFileRequest>? attachmentFiles,
    required String otp,
  }) async {
    try {
      final request = FeedbackRequest(
        custId: custId,
        contractNo: contractNo,
        contractId: contractId,
        custTypeId: custTypeId,
        custName: custName,
        l1TicketTypeId: l1TicketTypeId,
        contentReceive: contentReceive,
        phoneNumber: phoneNumber,
        phoneContact: phoneContact,
        attachmentFiles: attachmentFiles,
        otp: otp,
      );

      final response = await _ccClient.feedback(request: request);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<String?, Failure>> feedbackRequestOtp({required int confirmType}) async {
    try {
      final response = await _ccClient.feedbackRequestOtp(confirmType: confirmType);

      if (response.mess?.code != REQUEST_SUCCESS_CODE) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      }
      return Result.success(response.mess?.description);
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }

  @override
  Future<Result<List<int>, Failure>> downloadAttachmentFile({required int attachmentId}) async {
    try {
      final response = await _ccClient.downloadAttachment(attachmentId: attachmentId);

      if (response.response.statusCode != 200) {
        return const Result.failure(UnknownFailure());
      }

      final data = response.data;

      if (data.isNotEmpty) {
        return Result.success(data);
      }

      return const Result.failure(UnknownFailure());
    } on UnauthorizedException catch (err) {
      return Result.failure(UnauthorizedFailure(message: err.toString()));
    } on ResponseException catch (err) {
      return Result.failure(ResponseFailure(message: err.toString()));
    } on Exception {
      return const Result.failure(UnknownFailure());
    }
  }
}
