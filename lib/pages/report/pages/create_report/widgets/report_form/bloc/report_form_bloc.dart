import 'dart:async';
import 'dart:ffi';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/models/address_vtt/address_vtt.dart';
import 'package:epass/commons/models/feedback/feedback_type.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/register/pages/register_address/widgets/address_dropdown/address_dropdown.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

part 'report_form_event.dart';

part 'report_form_state.dart';

class ReportFormBloc extends Bloc<ReportFormEvent, ReportFormState> {
  ReportFormBloc({
    required AppBloc appBloc,
  }) : super(ReportFormState(
    groupReportState: GroupReportState(),
    locationInfo: LocationInfo(
      province: AddressVTT(
        name: getAreaFromAdress(appBloc.state.user?.noticeAreaName, AddressDropdownType.province),
        areaCode: getAreaCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.province),
        province: getCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.province),
      ),
      district: AddressVTT(
        name: getAreaFromAdress(appBloc.state.user?.noticeAreaName, AddressDropdownType.district),
        areaCode: getAreaCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.district),
        province: getCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.province),
        district: getCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.district),
      ),
      ward: AddressVTT(
        name: getAreaFromAdress(appBloc.state.user?.noticeAreaName, AddressDropdownType.ward),
        areaCode: getAreaCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.ward),
        province: getCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.province),
        district: getCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.district),
        precinct: getCode(appBloc.state.user?.noticeAreaCode, AddressDropdownType.ward),
      ),
    ),
    reporterName: appBloc.state.user?.userName ?? '',
    email: appBloc.state.user?.email,
    location: appBloc.state.user?.address,
    phoneContact: appBloc.state.user?.noticePhoneNumber ?? ''
  )
  ) {
    on<ReporterNameChanged>(_onReporterNameChanged);
    on<VehiclesChanged>(_onVehiclesChanged);
    on<EmailChanged>(_onEmailChanged);
    on<LocationChanged>(_onLocationChanged);
    on<ProvinceChanged>(_onProvinceChanged);
    on<DistrictChanged>(_onDistrictChanged);
    on<CommuneChanged>(_onCommuneChanged);
    on<L1TicketTypeChanged>(_onL1TicketTypeChanged);
    on<L2TicketTypeChanged>(_onL2TicketTypeChanged);
    on<L3TicketTypeChanged>(_onL3TicketTypeChanged);
    on<StationIdChanged>(_onStationIdChanged);
    on<StageIdChanged>(_onStageIdChanged);
    on<StationStageNameChanged>(_onStationStageNameChanged);
    on<ReceiveContentChanged>(_onReceiveContentChanged);
    on<TicketKindChanged>(_onTicketKindChanged);
    on<SupportInfoChanged>(_onSupportInfoChanged);
    on<PhoneContactChanged>(_onPhoneContactChanged);
    on<ReportAttachmentFilesChanged>(_onReportAttachmentFilesChanged);
  }

  FutureOr<void> _onReporterNameChanged(event, emit) {
    emit(state.copyWith(reporterName: event.reporterName));
  }

  FutureOr<void> _onVehiclesChanged(event, emit) {
    emit(state.copyWith(
      plateNumber: event.plateNumber,
      plateTypeCode: event.plateTypeCode,
    ));
  }

  FutureOr<void> _onEmailChanged(event, emit) {
    emit(state.copyWith(email: event.email));
  }

  FutureOr<void> _onLocationChanged(event, emit) {
    emit(state.copyWith(location: event.location));
  }

  FutureOr<void> _onProvinceChanged(
    ProvinceChanged event,
    Emitter<ReportFormState> emit
  ) {
    final locationInfo = state.locationInfo;

    final isOldProvince = locationInfo.province?.province == event.province?.province;

    final newLocationInfo = locationInfo.copyWith(
      province: event.province,
      district: isOldProvince ? locationInfo.district : null,
      ward: isOldProvince ? locationInfo.ward : null,
    );

    emit(state.copyWith(locationInfo: newLocationInfo));
  }

  FutureOr<void> _onDistrictChanged(
      DistrictChanged event,
      Emitter<ReportFormState> emit
      ) {
    final locationInfo = state.locationInfo;

    final isOldDistrict = locationInfo.district?.district == event.district?.district;

    final newLocationInfo = locationInfo.copyWith(
      province: locationInfo.province,
      district: event.district,
      ward: isOldDistrict ? locationInfo.ward : null,
    );

    emit(state.copyWith(locationInfo: newLocationInfo));
  }

  FutureOr<void> _onCommuneChanged(
      CommuneChanged event,
      Emitter<ReportFormState> emit
      ) {
    final locationInfo = state.locationInfo;

    final newLocationInfo = locationInfo.copyWith(
      province: locationInfo.province,
      district: locationInfo.district,
      ward: event.ward,
    );

    emit(state.copyWith(locationInfo: newLocationInfo));
  }

  FutureOr<void> _onL1TicketTypeChanged(event, emit) {
    final groupReportState = state.groupReportState;

    final isOldTicketType = groupReportState.l1TicketType == event.l1TicketType;

    final newGroupReportState = groupReportState.copyWith(
      l1TicketType: event.l1TicketType,
      l2TicketType: isOldTicketType ? groupReportState.l2TicketType : null,
      l3TicketType: isOldTicketType ? groupReportState.l3TicketType : null,
      currentLevel: 1,
    );

    emit(state.copyWith(groupReportState: newGroupReportState));
  }

  FutureOr<void> _onL2TicketTypeChanged(event, emit) {
    final groupReportState = state.groupReportState;

    final isOldTicketType = groupReportState.l2TicketType == event.l2TicketType;

    final newGroupReportState = groupReportState.copyWith(
      l1TicketType: groupReportState.l1TicketType,
      l2TicketType: event.l2TicketType,
      l3TicketType: isOldTicketType ? groupReportState.l3TicketType : null,
      currentLevel: 2,
    );

    emit(state.copyWith(groupReportState: newGroupReportState));
  }

  FutureOr<void> _onL3TicketTypeChanged(event, emit) {
    final groupReportState = state.groupReportState;

    final newGroupReportState = groupReportState.copyWith(
      l1TicketType: groupReportState.l1TicketType,
      l2TicketType: groupReportState.l2TicketType,
      l3TicketType: event.l3TicketType,
      currentLevel: 3,
    );

    emit(state.copyWith(groupReportState: newGroupReportState));
  }

  FutureOr<void> _onStationIdChanged(event, emit) {
    emit(state.copyWith(stationId: event.stationId));
  }

  FutureOr<void> _onStageIdChanged(event, emit) {
    emit(state.copyWith(stageId: event.stageId));
  }

  FutureOr<void> _onStationStageNameChanged(event, emit) {
    emit(state.copyWith(stationStageName: event.stationStageName));
  }

  FutureOr<void> _onReceiveContentChanged(event, emit) {
    emit(state.copyWith(contentReceive: event.content));
  }

  FutureOr<void> _onTicketKindChanged(event, emit) {
    emit(state.copyWith(ticketKind: event.ticketKind));
  }

  FutureOr<void> _onSupportInfoChanged(event, emit) {
    emit(state.copyWith(supportInfo: event.supportInfo));
  }

  FutureOr<void> _onPhoneContactChanged(event, emit) {
    emit(state.copyWith(phoneContact: event.phoneContact));
  }

  FutureOr<void> _onReportAttachmentFilesChanged(event, emit) {
    emit(state.copyWith(attachments: event.attachments));
  }
}
