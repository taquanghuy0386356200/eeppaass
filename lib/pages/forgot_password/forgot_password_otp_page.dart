import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_logo_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:epass/pages/forgot_password/bloc/forgot_password_form_bloc.dart';
import 'package:epass/pages/forgot_password/pages/forgot_password_new_password/bloc/forgot_password_new_password_form_bloc.dart';
import 'package:epass/pages/forgot_password/pages/forgot_password_otp/bloc/forgot_password_otp_bloc.dart';
import 'package:epass/pages/forgot_password/pages/forgot_password_otp/widgets/otp_countdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class ForgotPasswordOTPPage extends StatefulWidget {
  const ForgotPasswordOTPPage({
    Key? key,
  }) : super(key: key);

  @override
  State<ForgotPasswordOTPPage> createState() => _ForgotPasswordOTPPageState();
}

class _ForgotPasswordOTPPageState extends State<ForgotPasswordOTPPage> {
  final _errorController = StreamController<ErrorAnimationType>();

  @override
  void dispose() {
    _errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final phoneNumber = context.read<ForgotPasswordFormBloc>().state.phoneNumber;

    return BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
      listener: (context, state) {
        if (state is ForgotPasswordSubmittedInProgress) {
          context.loaderOverlay.show();
        } else {
          context.loaderOverlay.hide();
        }
      },
      child: BaseLogoPage(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        child: Stack(
          children: [
            const GradientHeaderContainer(),
            SafeArea(
              child: FadeAnimation(
                delay: 1,
                direction: FadeDirection.up,
                child: RoundedTopContainer(
                  padding: EdgeInsets.fromLTRB(16.w, 48.h, 16.w, 0.h),
                  child: BlocConsumer<ForgotPasswordOtpBloc, ForgotPasswordOtpState>(
                    listener: (context, state) {
                      if (state is ForgotPasswordOTPConfirmedInProgress) {
                        context.loaderOverlay.show();
                      } else if (state is ForgotPasswordOTPConfirmedSuccess) {
                        context.loaderOverlay.hide();
                        showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (dialogContext) {
                            return SuccessDialog(
                              content: 'Đặt lại mật khẩu thành công, vui lòng đăng nhập lại bằng mật khẩu mới.',
                              buttonTitle: 'Đăng nhập lại',
                              onButtonTap: () {
                                Navigator.of(dialogContext).pop();
                                context.router.replaceAll([const LoginRoute()]);
                              },
                            );
                          },
                        );
                      } else if (state is ForgotPasswordOTPConfirmedFailure) {
                        context.loaderOverlay.hide();
                        _errorController.add(ErrorAnimationType.shake);
                      }
                    },
                    builder: (context, state) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: double.infinity,
                            child: Text(
                              'Xác nhận',
                              style: Theme.of(context).textTheme.headline6!.copyWith(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(height: 24.h),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 2.w),
                            child: RichText(
                              text: TextSpan(
                                text: 'Nhập mã OTP đã được gửi đến số điện thoại ',
                                children: <TextSpan>[
                                  TextSpan(
                                    text: '${phoneNumber ?? ''}.',
                                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                          fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                ],
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            ),
                          ),
                          SizedBox(height: 24.h),
                          PinCodeTextField(
                            appContext: context,
                            errorAnimationController: _errorController,
                            length: 6,
                            obscureText: false,
                            animationType: AnimationType.fade,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.box,
                              fieldHeight: 46.r,
                              fieldWidth: 46.r,
                              borderRadius: BorderRadius.circular(8.0),
                              errorBorderColor: ColorName.error,
                              borderWidth: 1.0,
                              activeColor: ColorName.disabledBorderColor,
                              activeFillColor: Colors.white,
                              inactiveColor: ColorName.disabledBorderColor,
                              inactiveFillColor: Colors.white,
                              selectedColor: ColorName.disabledBorderColor,
                              selectedFillColor: Colors.white,
                            ),
                            backgroundColor: Colors.transparent,
                            autoDismissKeyboard: true,
                            autoFocus: true,
                            keyboardType: TextInputType.number,
                            textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.w600,
                                ),
                            onCompleted: _onOTPCompleted,
                            onChanged: (value) {},
                            beforeTextPaste: (text) {
                              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                              //but you can show anything you want here, like your pop up saying wrong paste format or etc
                              return false;
                            },
                          ),
                          if (state is ForgotPasswordOTPConfirmedFailure) SizedBox(height: 8.h),
                          if (state is ForgotPasswordOTPConfirmedFailure)
                            Text(
                              state.message,
                              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                    color: ColorName.error,
                                  ),
                            ),
                          SizedBox(height: 20.h),
                          BlocSelector<ForgotPasswordBloc, ForgotPasswordState, ForgotPasswordSubmittedSuccess?>(
                            selector: (state) => state is ForgotPasswordSubmittedSuccess ? state : null,
                            builder: (context, state) {
                              return state != null
                                  ? OTPCountdown(
                                      otpSubmittedTime: state.submittedSuccessTime,
                                      resubmitEvent: state.resubmitEvent,
                                    )
                                  : const SizedBox.shrink();
                            },
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onOTPCompleted(String otp) {
    FocusScope.of(context).unfocus();

    final plateNumber = context.read<ForgotPasswordFormBloc>().state.plateNumber;
    final plateTypeCode = context.read<ForgotPasswordFormBloc>().state.plateTypeCode;
    final phoneNumber = context.read<ForgotPasswordFormBloc>().state.phoneNumber;
    final newPassword = context.read<ForgotPasswordNewPasswordFormBloc>().state.newPassword;

    if (plateNumber != null && plateTypeCode != null && phoneNumber != null && newPassword != null) {
      context.read<ForgotPasswordOtpBloc>().add(ForgotPasswordOTPConfirmed(
            plateNumber: plateNumber,
            plateTypeCode: plateTypeCode,
            phoneNumber: phoneNumber,
            otp: otp,
            newPassword: newPassword,
          ));
    }
  }
}
