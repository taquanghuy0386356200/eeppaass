part of 'viettel_pay_gate_bloc.dart';

abstract class ViettelPayGateState extends Equatable {
  const ViettelPayGateState();
}

class ViettelPayGateInitial extends ViettelPayGateState {
  @override
  List<Object> get props => [];
}

class ViettelPayGatePaymentCompletedSuccess extends ViettelPayGateState {
  final String orderId;
  final int amount;

  const ViettelPayGatePaymentCompletedSuccess({
    required this.orderId,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, amount];
}

class ViettelPayGatePaymentCompletedFailure extends ViettelPayGateState {
  final String message;

  const ViettelPayGatePaymentCompletedFailure(this.message);

  @override
  List<Object> get props => [message];
}
