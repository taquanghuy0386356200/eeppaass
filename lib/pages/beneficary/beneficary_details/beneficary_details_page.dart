import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/repo/beneficiary_repository.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/buttons/primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/confirm_dialog.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/beneficary/beneficary_details/bloc/beneficary_details_bloc.dart';
import 'package:epass/pages/beneficary/beneficary_details/widgets/beneficiary_details_card.dart';
import 'package:epass/pages/beneficary/beneficary_list/bloc/beneficary_list_bloc.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class BeneficiaryDetailsPage extends StatelessWidget {
  final String beneficiaryId;

  // Currently, there's no need to call api to get vehicle details
  // Could've changed later

  const BeneficiaryDetailsPage({
    Key? key,
    @PathParam('id') required this.beneficiaryId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BeneficiaryDetailsBloc(
        beneficiaryRepository: getIt<IBeneficiaryRepository>(),
      ),
      child: MultiBlocListener(
        listeners: [
          BlocListener<BeneficiaryDetailsBloc, BeneficiaryDetailsState>(
            listener: (context, state) {
              if (state is BeneficiaryUpdatedInProgress) {
                context.loaderOverlay.show();
              } else if (state is BeneficiaryUpdatedFailure) {
                context.loaderOverlay.hide();
                showErrorSnackBBar(context: context, message: state.message);
              } else if (state is BeneficiaryUpdatedSuccess) {
                context.loaderOverlay.hide();
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (dialogContext) {
                    return SuccessDialog(
                      content: 'Cập nhật thông tin người thụ hưởng thành công',
                      contentTextAlign: TextAlign.center,
                      buttonTitle: 'Đóng',
                      onButtonTap: () async {
                        Navigator.of(dialogContext).pop();
                      },
                    );
                  },
                );
                context
                    .read<BeneficiaryListBloc>()
                    .add(BeneficiaryListFetched());
              }
            },
          ),
          BlocListener<BeneficiaryDetailsBloc, BeneficiaryDetailsState>(
            listener: (context, state) {
              if (state is BeneficiaryDeleteInProgress) {
                context.loaderOverlay.show();
              } else if (state is BeneficiaryDeleteFailure) {
                context.loaderOverlay.hide();
                showErrorSnackBBar(context: context, message: state.message);
              } else if (state is BeneficiaryDeleteSuccess) {
                context.loaderOverlay.hide();
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (dialogContext) {
                    return SuccessDialog(
                      content: 'Xóa thông tin người thụ hưởng thành công',
                      contentTextAlign: TextAlign.center,
                      buttonTitle: 'Đóng',
                      onButtonTap: () async {
                        Navigator.of(dialogContext).pop();
                        Navigator.of(context).pop();
                      },
                    );
                  },
                );
                context.pushRoute(const BenficaryRouter());
              }
            },
          ),
        ],
        child: BasePage(
          title: 'Thông tin chi tiết',
          backgroundColor: Colors.white,
          child: Stack(
            children: [
              const GradientHeaderContainer(),
              SafeArea(
                child: FadeAnimation(
                  delay: 1,
                  direction: FadeDirection.up,
                  child: SingleChildScrollView(
                    child: RoundedTopContainer(
                      margin: EdgeInsets.only(top: 16.h),
                      padding: EdgeInsets.fromLTRB(16.w, 24.h, 16.w, 32.h),
                      // Your widget go below
                      child: BlocSelector<BeneficiaryListBloc,
                          BeneficiaryListState, Beneficiary?>(
                        selector: (state) => state.listData.singleWhereOrNull(
                            (element) =>
                                (element.beneficiaryId?.toString() ?? '') ==
                                beneficiaryId),
                        builder: (context, beneficiary) {
                          final tabsBloc = context.read<HomeTabsBloc>();
                          final reminiscentName =
                              context.select((BeneficiaryDetailsBloc bloc) {
                            final state = bloc.state;
                            return state is BeneficiaryChangedSuccess
                                ? state.reminiscentName
                                : null;
                          });
                          return Column(
                            children: [
                              BeneficiaryDetailsCard(beneficiary: beneficiary),
                              SizedBox(height: 16.h),
                              SizedBox(
                                height: 56.h,
                                width: double.infinity,
                                child: PrimaryButton(
                                  title: 'Xác nhận',
                                  onTap: () async {
                                    if (beneficiary != null) {
                                      context
                                          .read<BeneficiaryDetailsBloc>()
                                          .add(BeneficiaryUpdated(
                                              beneficiaryInformationId:
                                                  int.parse(beneficiaryId),
                                              reminiscentName:
                                                  reminiscentName.toString()));
                                    }
                                    tabsBloc.add(const HomeTabBarRequestHidden(
                                        isHidden: true));
                                  },
                                ),
                              ),
                              SizedBox(height: 16.h),
                              SizedBox(
                                height: 56.h,
                                width: double.infinity,
                                child: SecondaryButton(
                                  title: 'Xóa thông tin',
                                  onTap: () async {
                                    showDialog(
                                      context: context,
                                      builder: (dialogContext) {
                                        return ConfirmDialog(
                                          title:
                                              'Bạn có chắc chắn muốn xóa thông tin người thụ hưởng?',
                                          image: Assets.icons.scanFailure.svg(),
                                          primaryButtonTitle: 'Xác nhận',
                                          onPrimaryButtonTap: () {
                                            Navigator.of(dialogContext).pop();
                                            if (beneficiary != null) {
                                              context
                                                  .read<
                                                      BeneficiaryDetailsBloc>()
                                                  .add(BeneficiaryDelete(
                                                      beneficiaryId:
                                                          beneficiaryId));
                                            }
                                          },
                                          secondaryButtonTitle: 'Đóng',
                                          onSecondaryButtonTap: () {
                                            Navigator.of(dialogContext).pop();
                                          },
                                        );
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
