import 'package:epass/commons/api/clients/app_store/app_store_client.dart';
import 'package:epass/commons/api/failures/failures.dart';
import 'package:epass/commons/api/interceptors/error_interceptor.dart';
import 'package:epass/pages/home/model/apps_model.dart';
import 'package:simple_result/simple_result.dart';

abstract class IAppStoreRepository {
  Future<Result<AppsModel?, Failure>> getAppsStore();
}

class AppStoreRepository extends IAppStoreRepository {
  final AppStoreClient _appStoreClient;

  AppStoreRepository({required AppStoreClient appStoreClient})
      : _appStoreClient = appStoreClient;

  @override
  Future<Result<AppsModel?, Failure>> getAppsStore() async {
    try {
      final response = await _appStoreClient.getAppsStore();

      final appsStore = response;

      if (appsStore == null) {
        return const Result.failure(
          ResponseFailure(
            message: 'Có lỗi xảy ra khi lấy dữ liệu chợ ứng dụng',
          ),
        );
      }
      return Result.success(appsStore);
    } on ServiceUnavailableException catch (err) {
      return Result.success(null);
    } on UnauthorizedException catch (err) {
      return Result.success(null);
    } on ResponseException catch (err) {
      return Result.success(null);
    } on Exception catch (err) {
      return Result.success(null);
    }
  }
}
