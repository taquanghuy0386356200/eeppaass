import 'package:epass/pages/home/model/list_app.dart';
import 'package:json_annotation/json_annotation.dart';

import 'list_app_category.dart';

@JsonSerializable()
class AppsModel {
  AppsModel({
    this.labelMain,
    this.limitDisplay,
    this.labelSeeMore,
    this.imageSeeMode,
    this.listAppPopular,
    this.listAppCategory,
  });

  AppsModel.fromJson(dynamic json) {
    try {
      labelMain = json['labelMain'];
      limitDisplay = json['limitDisplay'];
      labelSeeMore = json['labelSeeMore'];
      imageSeeMode = json['imageSeeMode'];
      if (json['listAppHome'] != null) {
        listAppPopular = [];
        json['listAppHome'].forEach((v) {
          listAppPopular?.add(ListApp.fromJson(v));
        });
      }
      if (json['listAppCategory'] != null) {
        listAppCategory = [];
        json['listAppCategory'].forEach((v) {
          listAppCategory?.add(ListAppCategory.fromJson(v));
        });
      }
    } catch (e) {
      labelMain = null;
      limitDisplay = null;
      labelSeeMore = null;
      imageSeeMode = null;
      listAppPopular = null;
      listAppCategory = null;
    }
  }

  String? labelMain;
  int? limitDisplay;
  String? labelSeeMore;
  String? imageSeeMode;
  List<ListApp>? listAppPopular;
  List<ListAppCategory>? listAppCategory;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['labelMain'] = labelMain;
    map['limitDisplay'] = limitDisplay;
    map['labelSeeMore'] = labelSeeMore;
    map['imageSeeMode'] = imageSeeMode;
    if (listAppPopular != null) {
      map['listAppHome'] = listAppPopular?.map((v) => v.toJson()).toList();
    }
    if (listAppCategory != null) {
      map['listAppCategory'] = listAppCategory?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
