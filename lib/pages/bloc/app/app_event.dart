part of 'app_bloc.dart';

@immutable
abstract class AppEvent extends Equatable {
  const AppEvent();
}

class UpdateAppAuthentication extends AppEvent {
  final AuthInfo authInfo;

  const UpdateAppAuthentication(this.authInfo);

  @override
  List<Object> get props => [authInfo];
}

class UpdateAppUser extends AppEvent {
  final User user;
  const UpdateAppUser(this.user);

  @override
  List<Object> get props => [user];
}

class UpdateEncryptedPassword extends AppEvent {
  final String password;

  const UpdateEncryptedPassword(this.password);

  @override
  List<Object> get props => [password];
}

class UserLogout extends AppEvent {
  @override
  List<Object> get props => [];
}

class UpdateSyncCarDoctorStatus extends AppEvent {
  final bool isSyncCarDoctor;

  const UpdateSyncCarDoctorStatus(this.isSyncCarDoctor);

  @override
  List<Object> get props => [isSyncCarDoctor];
}
