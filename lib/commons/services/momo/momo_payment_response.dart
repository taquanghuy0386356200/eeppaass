enum MomoPaymentStatus {
  success,
  failure,
}

class MomoPaymentResponse {
  bool? isSuccess;
  MomoPaymentStatus status;
  String? token;
  String? phoneNumber;
  String? data;
  String? message;
  String? extra;

  MomoPaymentResponse({
    this.isSuccess,
    required this.status,
    this.token,
    this.phoneNumber,
    this.message,
    this.data,
    this.extra,
  });

  static MomoPaymentResponse fromMap(Map<dynamic, dynamic> map) {
    bool? isSuccess = map['isSuccess'];
    int statusInt = int.parse(map['status'].toString());
    String? token = map['token'];
    String? phoneNumber = map['phoneNumber'];
    String? data = map['data'];
    String? message = map['message'];
    String? extra = map['extra'];
    return MomoPaymentResponse(
      isSuccess: isSuccess,
      status: statusInt == 0
          ? MomoPaymentStatus.success
          : MomoPaymentStatus.failure,
      token: token,
      phoneNumber: phoneNumber,
      data: data,
      message: message,
      extra: extra,
    );
  }
}
