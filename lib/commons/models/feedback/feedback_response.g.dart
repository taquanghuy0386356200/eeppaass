// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feedback_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackModel _$FeedbackModelFromJson(Map<String, dynamic> json) =>
    FeedbackModel(
      l1TicketTypeName: json['l1TicketTypeName'] as String?,
      l2TicketTypeName: json['l2TicketTypeName'] as String?,
      l3TicketTypeName: json['l3TicketTypeName'] as String?,
      contentReceive: json['contentReceive'] as String?,
      statusName: json['statusName'] as String?,
      createDate: json['createDate'] as String?,
      processTime: json['processTime'] as String?,
      processContent: json['processContent'] as String?,
      attachmentFiles: (json['attachmentFiles'] as List<dynamic>?)
          ?.map((e) => FeedbackAttachmentFileResponse.fromJson(
              e as Map<String, dynamic>))
          .toList(),
      staffName: json['staffName'] as String?,
      fileName: json['fileName'] as String?,
      attachmentId: json['attachmentId'] as String?,
    );

Map<String, dynamic> _$FeedbackModelToJson(FeedbackModel instance) =>
    <String, dynamic>{
      'l1TicketTypeName': instance.l1TicketTypeName,
      'l2TicketTypeName': instance.l2TicketTypeName,
      'l3TicketTypeName': instance.l3TicketTypeName,
      'contentReceive': instance.contentReceive,
      'statusName': instance.statusName,
      'createDate': instance.createDate,
      'processTime': instance.processTime,
      'processContent': instance.processContent,
      'attachmentFiles': instance.attachmentFiles,
      'staffName': instance.staffName,
      'fileName': instance.fileName,
      'attachmentId': instance.attachmentId,
    };

FeedbackResponse _$FeedbackResponseFromJson(Map<String, dynamic> json) =>
    FeedbackResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => FeedbackModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FeedbackResponseToJson(FeedbackResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

const _$FeedbackChannelEnumMap = {
  FeedbackChannel.report: 'report',
  FeedbackChannel.suggestion: 'suggestion',
};
