part of 'viettel_pay_gate_bloc.dart';

abstract class ViettelPayGateEvent extends Equatable {
  const ViettelPayGateEvent();
}

class ViettelPayPaymentCompleted extends ViettelPayGateEvent {
  final String orderId;
  final int paymentStatus;
  final String? errorCode;
  final int transAmount;

  const ViettelPayPaymentCompleted({
    required this.orderId,
    required this.paymentStatus,
    this.errorCode,
    required this.transAmount,
  });

  @override
  List<Object?> get props => [orderId, paymentStatus, errorCode, transAmount];
}
