import 'package:epass/commons/repo/transaction_history_repository.dart';
import 'package:epass/commons/widgets/buttons/floating_primary_button.dart';
import 'package:epass/commons/widgets/pages/common_error_page.dart';
import 'package:epass/commons/widgets/pages/no_data_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_other/bloc/transaction_other_bloc.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_other/widget/transaction_other_card.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_other/widget/transaction_other_search_form.dart';
import 'package:epass/pages/transaction_history_tab/pages/transaction_other/widget/transaction_other_shimmer_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TransactionOtherPage extends StatefulWidget {
  const TransactionOtherPage({Key? key}) : super(key: key);

  @override
  State<TransactionOtherPage> createState() => _TransactionOtherPageState();
}

class _TransactionOtherPageState extends State<TransactionOtherPage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TransactionOtherBloc(
        transactionHistoryRepository: getIt<ITransactionHistoryRepository>(),
      )..add(const TransactionOtherFetched()),
      child: BlocConsumer<TransactionOtherBloc, TransactionOtherState>(
        listener: (context, state) {
          final error = state.error;

          if (error == null) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          } else {
            _refreshController.refreshFailed();
            _refreshController.loadFailed();
          }
        },
        builder: (context, state) {
          final error = state.error;

          if (state.isLoading || state.isRefreshing && state.listData.isEmpty) {
            return const TransactionOtherShimmerLoading();
          } else if (error != null && state.listData.isEmpty) {
            return CommonErrorPage(
              message: error,
              onTap: () => context
                  .read<TransactionOtherBloc>()
                  .add(const TransactionOtherFetched()),
            );
          }
          final listData = state.listData;
          return Scaffold(
            backgroundColor: Colors.transparent,
            floatingActionButton: GestureDetector(
              onTap: () => _onActionButtonTapped(
                context,
                plateNumber: state.plateNumber,
                fromDate: state.fromDate,
                toDate: state.toDate,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "Tìm kiếm",
                    style: Theme.of(context).textTheme.caption!.copyWith(
                        color: Colors.orange,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  const FloatingPrimaryButton(),
                ],
              ),
            ),
            body: state.listData.isEmpty
                ? NoDataPage(
                    title: context.read<TransactionOtherBloc>().isSearching
                        ? 'Bạn không có giao dịch nào phát sinh trong thời gian tìm kiếm'
                        : 'Bạn chưa có giao dịch nào phát sinh',
                    description: '',
                  )
                : SmartRefresher(
                    physics: const BouncingScrollPhysics(),
                    enablePullDown: true,
                    enablePullUp: !state.isFull,
                    onRefresh: () => context
                        .read<TransactionOtherBloc>()
                        .add(const TransactionOtherRefreshed()),
                    onLoading: () => context
                        .read<TransactionOtherBloc>()
                        .add(const TransactionOtherLoadMore()),
                    controller: _refreshController,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        final item = listData[index];
                        return Padding(
                          padding: EdgeInsets.fromLTRB(
                            16.w,
                            index == 0 ? 24.h : 8.h,
                            16.w,
                            index == listData.length - 1 ? 80.h : 8.h,
                          ),
                          child: TransactionOtherCard(transactionOther: item),
                        );
                      },
                      separatorBuilder: (context, index) => Column(
                        children: [
                          SizedBox(height: 16.h),
                          Divider(
                            color: ColorName.disabledBorderColor,
                            height: 0.5,
                            thickness: 0.5,
                            indent: 32.w,
                            endIndent: 16.w,
                          ),
                          SizedBox(height: 16.h),
                        ],
                      ),
                    ),
                  ),
          );
        },
      ),
    );
  }

  void _onActionButtonTapped(
    BuildContext context, {
    String? plateNumber,
    DateTime? fromDate,
    DateTime? toDate,
  }) async {
    await showBarModalBottomSheet(
      context: context,
      expand: false,
      useRootNavigator: true,
      builder: (modalContext) => BlocProvider.value(
        value: context.read<TransactionOtherBloc>(),
        child: TransactionOtherSearchForm(
          plateNumber: plateNumber,
          fromDate: fromDate,
          toDate: toDate,
        ),
      ),
    );
  }
}
