import 'package:epass/commons/models/invoice/invoice.dart';
import 'package:epass/pages/invoice/widget/invoice_card.dart';
import 'package:epass/pages/invoice/widget/invoice_instruction.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InvoiceList extends StatelessWidget {
  const InvoiceList({
    Key? key,
    required this.listData,
  }) : super(key: key);

  final List<Invoice> listData;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      itemCount: listData.isEmpty ? 1 : listData.length + 1,
      itemBuilder: (context, index) {
        if (index == 0) {
          return Padding(
            padding: EdgeInsets.fromLTRB(
              8.w,
              24.h,
              8.w,
              0.h,
            ),
            child: const InvoiceInstruction(),
          );
        }

        final item = listData[index - 1];
        return Padding(
          padding: EdgeInsets.fromLTRB(
            16.w,
            0.h,
            16.w,
            index == listData.length ? 80.h : 0.h,
          ),
          child: InvoiceCard(item: item),
        );
      },
      separatorBuilder: (context, index) =>
          SizedBox(height: index == 0 ? 12.h : 24.h),
    );
  }
}
