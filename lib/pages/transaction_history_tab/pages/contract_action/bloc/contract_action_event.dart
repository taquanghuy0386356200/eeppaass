part of 'contract_action_bloc.dart';

@immutable
abstract class ContractActionEvent extends Equatable {
  const ContractActionEvent();
}

class ContractActionFetched extends ContractActionEvent {
  const ContractActionFetched();

  @override
  List<Object?> get props => [];
}

class ContractActionRefreshed extends ContractActionEvent {
  const ContractActionRefreshed();

  @override
  List<Object?> get props => [];
}

class ContractActionLoadMore extends ContractActionEvent {
  const ContractActionLoadMore();

  @override
  List<Object?> get props => [];
}

class ContractActionSearched extends ContractActionEvent {
  final DateTime? startDate;
  final DateTime? endDate;

  const ContractActionSearched({this.startDate, this.endDate});

  @override
  List<Object?> get props => [startDate, endDate];
}

class ContractActionStartDateChanged extends ContractActionEvent {
  final String? startDate;

  const ContractActionStartDateChanged(this.startDate);

  @override
  List<Object?> get props => [startDate];
}

class ContractActionEndDateChanged extends ContractActionEvent {
  final String? endDate;

  const ContractActionEndDateChanged(this.endDate);

  @override
  List<Object?> get props => [endDate];
}
