import 'package:flutter/material.dart';

class BottomIndicatorNavigationBarItem {
  final Widget icon;
  final Widget activeIcon;
  final Color backgroundColor;
  final String? label;

  BottomIndicatorNavigationBarItem({
    required this.icon,
    required this.activeIcon,
    this.label,
    this.backgroundColor = Colors.white,
  });
}
