part of 'setting_bloc.dart';

class SettingState extends Equatable {
  final bool isFirstRun;
  final bool isCashInFirstRun;
  final bool isCashInBankTransferFirstRun;
  final bool isBiometricLoginEnabled;
  final bool isBeneficiaryFirstRun;

  const SettingState({
    this.isFirstRun = true,
    this.isCashInFirstRun = true,
    this.isCashInBankTransferFirstRun = true,
    this.isBiometricLoginEnabled = false,
    this.isBeneficiaryFirstRun = true,
  });

  @override
  List<Object> get props => [
        isFirstRun,
        isCashInFirstRun,
        isCashInBankTransferFirstRun,
        isBiometricLoginEnabled,
        isBeneficiaryFirstRun,
      ];

  SettingState copyWith({
    bool? isFirstRun,
    bool? isCashInFirstRun,
    bool? isCashInBankTransferFirstRun,
    bool? isBiometricLoginEnabled,
    bool? isBeneficiaryFirstRun,
  }) {
    return SettingState(
      isFirstRun: isFirstRun ?? this.isFirstRun,
      isCashInFirstRun: isCashInFirstRun ?? this.isCashInFirstRun,
      isCashInBankTransferFirstRun: isCashInBankTransferFirstRun ?? this.isCashInBankTransferFirstRun,
      isBiometricLoginEnabled: isBiometricLoginEnabled ?? this.isBiometricLoginEnabled,
      isBeneficiaryFirstRun: isBeneficiaryFirstRun ?? this.isBeneficiaryFirstRun,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isFirstRun': isFirstRun,
      'isCashInFirstRun': isCashInFirstRun,
      'isCashInBankTransferFirstRun': isCashInBankTransferFirstRun,
      'isBiometricLoginEnabled': isBiometricLoginEnabled,
      'isBeneficiaryFirstRun' : isBeneficiaryFirstRun,
    };
  }

  factory SettingState.fromJson(Map<String, dynamic> json) {
    return SettingState(
      isFirstRun: json['isFirstRun'] != null ? json['isFirstRun'] as bool : true,
      isCashInFirstRun: json['isCashInFirstRun'] != null ? json['isCashInFirstRun'] as bool : true,
      isCashInBankTransferFirstRun:
          json['isCashInBankTransferFirstRun'] != null ? json['isCashInBankTransferFirstRun'] as bool : true,
      isBiometricLoginEnabled: json['isBiometricLoginEnabled'] != null ? json['isBiometricLoginEnabled'] as bool : true,
      isBeneficiaryFirstRun: json['isBeneficiaryFirstRun'] != null ? json['isBeneficiaryFirstRun'] as bool : true,
    );
  }
}

class BiometricSettingAuthenticateSuccess extends SettingState {
  BiometricSettingAuthenticateSuccess({
    required SettingState currentState,
  }) : super(
          isBiometricLoginEnabled: true,
          isFirstRun: currentState.isFirstRun,
        );
}

class BiometricSettingAuthenticateFailure extends SettingState {
  final String message;

  BiometricSettingAuthenticateFailure({
    required SettingState currentState,
    required this.message,
  }) : super(
          isBiometricLoginEnabled: false,
          isFirstRun: currentState.isFirstRun,
        );
}
