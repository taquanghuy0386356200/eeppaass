import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'home_tabs_event.dart';

part 'home_tabs_state.dart';

class HomeTabsBloc extends Bloc<HomeTabsEvent, HomeTabsState> {
  HomeTabsBloc() : super(const HomeTabBarShowed()) {
    on<HomeTabBarRequestHidden>((event, emit) {
      if (event.isHidden) {
        emit(const HomeTabBarHidden());
      } else {
        emit(const HomeTabBarShowed());
      }
    });

    on<HomeTabChanged>((event, emit) {
      emit(HomeTabBarShowed(currentTab: event.currentTabIndex));
    });
  }
}
