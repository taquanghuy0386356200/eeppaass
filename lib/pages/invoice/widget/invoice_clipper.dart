import 'package:flutter/material.dart';

class InvoiceClipper1 extends CustomClipper<Path> {
  final bigRadius = 8.0;
  final factor = 4.0;

  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, 0);
    path.addOval(Rect.fromCircle(
      center: Offset(0, size.height + bigRadius / factor),
      radius: bigRadius,
    ));
    path.moveTo(size.width, size.height);
    path.addOval(Rect.fromCircle(
      center: Offset(size.width, size.height + bigRadius / factor),
      radius: bigRadius,
    ));

    path.moveTo(0, 0);
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);

    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}

class InvoiceClipper2 extends CustomClipper<Path> {
  final bigRadius = 8.0;
  final radius = 6.0;
  final numberOfJag = 20;
  final distance = 4.0;
  final factor = 4.0;

  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, 0);
    path.addOval(Rect.fromCircle(
      center: Offset(0, bigRadius / factor),
      radius: bigRadius,
    ));
    path.moveTo(size.width, 0);
    path.addOval(Rect.fromCircle(
      center: Offset(size.width, bigRadius / factor),
      radius: bigRadius,
    ));

    path.moveTo(0, 0);
    path.lineTo(0, size.height);
    var curXPos = 0.0;
    var curYPos = size.height;
    var increment = (size.width - distance * (numberOfJag + 1)) / numberOfJag;
    while (curXPos <= size.width - distance * 2) {
      curXPos += distance;
      path.lineTo(curXPos, curYPos);
      curXPos += increment;
      path.arcToPoint(
        Offset(curXPos, curYPos),
        radius: Radius.circular(radius),
      );
    }
    path.lineTo(size.width, curYPos);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
