part of 'vnpay_bloc.dart';

abstract class VNPayState extends Equatable {
  const VNPayState();
}

class VNPayInitial extends VNPayState {
  @override
  List<Object> get props => [];
}

class VNPayPaymentInProgress extends VNPayState {
  @override
  List<Object> get props => [];
}

class VNPayPaymentSuccess extends VNPayState {
  final String orderId;
  final int amount;

  const VNPayPaymentSuccess({
    required this.orderId,
    required this.amount,
  });

  @override
  List<Object> get props => [orderId, amount];
}

class VNPayPaymentFailure extends VNPayState {
  final String message;

  const VNPayPaymentFailure(this.message);

  @override
  List<Object> get props => [message];
}
