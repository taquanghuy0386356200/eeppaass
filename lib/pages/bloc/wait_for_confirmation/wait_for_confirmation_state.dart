part of 'wait_for_confirmation_bloc.dart';

@immutable
class WaitForConfirmationState extends Equatable {
  final List<WaitingOrder>? waitingOrders;
  final String? searchInput;
  final int? notificationType;
  final String? message;
  final bool isLoading;
  final bool isRefreshing;
  final bool isLoadingMore;

  const WaitForConfirmationState({
    this.waitingOrders,
    this.searchInput,
    this.notificationType,
    this.message,
    this.isLoading = false,
    this.isRefreshing = false,
    this.isLoadingMore = false,
  });

  @override
  List<Object?> get props => [
        waitingOrders,
        searchInput,
        notificationType,
        message,
        isLoading,
        isRefreshing,
        isLoadingMore,
      ];
  WaitForConfirmationState copyWith({
    List<WaitingOrder>? waitingOrders,
    String? searchInput,
    int? notificationType,
    String? message,
    bool? isLoading,
    bool? isRefreshing,
    bool? isLoadingMore,
  }) {
    return WaitForConfirmationState(
      waitingOrders: waitingOrders,
      searchInput: searchInput ?? this.searchInput,
      notificationType: notificationType ?? this.notificationType,
      message: message,
      isLoading: isLoading ?? this.isLoading,
      isRefreshing: isRefreshing ?? this.isRefreshing,
      isLoadingMore: isLoadingMore ?? this.isLoadingMore,
    );
  }
}
