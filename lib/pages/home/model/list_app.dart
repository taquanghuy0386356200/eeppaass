import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ListApp {
  ListApp({
    this.appId,
    this.appName,
    this.appIcon,
    this.appType,
    this.screenRedirect,
    this.priority,
    this.priorityMain,
    this.isInsertMsisdn,
    this.description,
  });

  ListApp.fromJson(dynamic json) {
    appId = json['appId'];
    appName = json['appName'];
    appIcon = json['appIcon'];
    appType = json['appType'];
    screenRedirect = json['screenRedirect'];
    priority = json['priority'];
    priorityMain = json['priorityMain'];
    isInsertMsisdn = json['isInsertMsisdn'];
    description = json['description'];
   isOpenBrowserApp= json["isOpenBrowserApp"];
   isOpenConfirm = json['isOpenConfirm'];
   contentCondition = json['contentCondition'];
   contentConfirm = json['contentConfirm'];
  }

  int? appId;
  String? appName;
  String? appIcon;
  int? appType;
  String? screenRedirect;
  int? priority;
  int? priorityMain;
  int? isInsertMsisdn;
  String? description;
  int? isOpenBrowserApp;
  int? isOpenConfirm;
  String? contentCondition;
  String? contentConfirm;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['appId'] = appId;
    map['appName'] = appName;
    map['appIcon'] = appIcon;
    map['appType'] = appType;
    map['screenRedirect'] = screenRedirect;
    map['priority'] = priority;
    map['priorityMain'] = priorityMain;
    map['isInsertMsisdn'] = isInsertMsisdn;
    map['description'] = description;
    return map;
  }
}
