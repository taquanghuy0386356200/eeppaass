// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feedback_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackRequest _$FeedbackRequestFromJson(Map<String, dynamic> json) =>
    FeedbackRequest(
      ticketChannel: json['ticketChannel'] as String? ?? '2',
      custId: json['custId'] as String,
      contractNo: json['contractNo'] as String,
      contractId: json['contractId'] as String,
      custTypeId: json['custTypeId'] as String,
      custName: json['custName'] as String,
      priorityId: json['priorityId'] as String? ?? '1',
      l1TicketTypeId: json['l1TicketTypeId'] as String,
      feedBack: json['feedBack'] as String?,
      contentReceive: json['contentReceive'] as String,
      ticketKind: json['ticketKind'] as String? ?? '1',
      phoneNumber: json['phoneNumber'] as String,
      phoneContact: json['phoneContact'] as String,
      attachmentFiles: (json['attachmentFiles'] as List<dynamic>?)
          ?.map((e) =>
              FeedbackAttachmentFileRequest.fromJson(e as Map<String, dynamic>))
          .toList(),
      otp: json['otp'] as String,
    );

Map<String, dynamic> _$FeedbackRequestToJson(FeedbackRequest instance) =>
    <String, dynamic>{
      'ticketChannel': instance.ticketChannel,
      'custId': instance.custId,
      'contractNo': instance.contractNo,
      'contractId': instance.contractId,
      'custTypeId': instance.custTypeId,
      'custName': instance.custName,
      'priorityId': instance.priorityId,
      'l1TicketTypeId': instance.l1TicketTypeId,
      'feedBack': instance.feedBack,
      'contentReceive': instance.contentReceive,
      'ticketKind': instance.ticketKind,
      'phoneNumber': instance.phoneNumber,
      'phoneContact': instance.phoneContact,
      'attachmentFiles':
          instance.attachmentFiles?.map((e) => e.toJson()).toList(),
      'otp': instance.otp,
    };
