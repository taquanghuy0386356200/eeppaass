part of 'sms_vehicle_bloc.dart';

abstract class SMSVehicleState extends Equatable {
  const SMSVehicleState();
}

class SMSVehicleInitial extends SMSVehicleState {
  @override
  List<Object> get props => [];
}

class UserSMSVehicleFetchedInProgress extends SMSVehicleState {
  @override
  List<Object> get props => [];
}

class UserSMSVehicleFetchedSuccess extends SMSVehicleState {
  final UserSmsVehicleSetting userSMSVehicleSetting;

  const UserSMSVehicleFetchedSuccess(this.userSMSVehicleSetting);

  @override
  List<Object> get props => [userSMSVehicleSetting];
}

class UserSMSVehicleFetchedFailure extends SMSVehicleState {
  final String message;

  const UserSMSVehicleFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class SMSVehicleRegisteredInProgress extends SMSVehicleState {
  @override
  List<Object> get props => [];
}

class SMSVehicleRegisteredSuccess extends SMSVehicleState {
  final String? message;

  const SMSVehicleRegisteredSuccess({this.message});

  @override
  List<Object?> get props => [message];
}

class SMSVehicleRegisteredFailure extends SMSVehicleState {
  final String message;

  const SMSVehicleRegisteredFailure(this.message);

  @override
  List<Object> get props => [message];
}

class SMSVehicleUpdatedInProgress extends SMSVehicleState {
  @override
  List<Object> get props => [];
}

class SMSVehicleUpdatedSuccess extends SMSVehicleState {
  final String? message;

  const SMSVehicleUpdatedSuccess({this.message});

  @override
  List<Object?> get props => [message];
}

class SMSVehicleUpdatedFailure extends SMSVehicleState {
  final String message;

  const SMSVehicleUpdatedFailure(this.message);

  @override
  List<Object> get props => [message];
}
