// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Vehicle _$VehicleFromJson(Map<String, dynamic> json) => Vehicle(
      vehicleId: json['vehicleId'] as int?,
      contractId: json['contractId'] as int?,
      plateNumber: json['plateNumber'] as String?,
      vehicleTypeId: json['vehicleTypeId'] as int?,
      vehicleGroupId: json['vehicleGroupId'] as int?,
      vehicleGroupName: json['vehicleGroupName'] as String?,
      vehicleGroupDescription: json['vehicleGroupDescription'] as String?,
      seatNumber: json['seatNumber'] as int?,
      netWeight: (json['netWeight'] as num?)?.toDouble(),
      cargoWeight: (json['cargoWeight'] as num?)?.toDouble(),
      status: $enumDecodeNullable(_$VehicleStatusEnumMap, json['status'],
          unknownValue: VehicleStatus.unknown),
      activeStatus: $enumDecodeNullable(
          _$VehicleActiveStatusEnumMap, json['activeStatus'],
          unknownValue: VehicleActiveStatus.unknown),
      epc: json['epc'] as String?,
      rfidSerial: json['rfidSerial'] as String?,
      vehicleTypeCode: json['vehicleTypeCode'] as String?,
      plateTypeCode: $enumDecodeNullable(
          _$PlateTypeCodeEnumMap, json['plateTypeCode'],
          unknownValue: PlateTypeCode.unknown),
      debit: json['debit'] == null
          ? null
          : VehicleDebit.fromJson(json['debit'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleToJson(Vehicle instance) => <String, dynamic>{
      'vehicleId': instance.vehicleId,
      'contractId': instance.contractId,
      'plateNumber': instance.plateNumber,
      'vehicleTypeId': instance.vehicleTypeId,
      'vehicleGroupId': instance.vehicleGroupId,
      'vehicleGroupName': instance.vehicleGroupName,
      'vehicleGroupDescription': instance.vehicleGroupDescription,
      'seatNumber': instance.seatNumber,
      'netWeight': instance.netWeight,
      'cargoWeight': instance.cargoWeight,
      'status': _$VehicleStatusEnumMap[instance.status],
      'activeStatus': _$VehicleActiveStatusEnumMap[instance.activeStatus],
      'epc': instance.epc,
      'rfidSerial': instance.rfidSerial,
      'vehicleTypeCode': instance.vehicleTypeCode,
      'plateTypeCode': _$PlateTypeCodeEnumMap[instance.plateTypeCode],
      'debit': instance.debit,
    };

const _$VehicleStatusEnumMap = {
  VehicleStatus.inactive: '0',
  VehicleStatus.active: '1',
  VehicleStatus.unknown: 'unknown',
};

const _$VehicleActiveStatusEnumMap = {
  VehicleActiveStatus.inactive: '0',
  VehicleActiveStatus.active: '1',
  VehicleActiveStatus.canceled: '2',
  VehicleActiveStatus.closed: '3',
  VehicleActiveStatus.open: '4',
  VehicleActiveStatus.transfered: '5',
  VehicleActiveStatus.unknown: 'unknown',
};

const _$PlateTypeCodeEnumMap = {
  PlateTypeCode.white: '1',
  PlateTypeCode.blue: '2',
  PlateTypeCode.red: '3',
  PlateTypeCode.ng: '4',
  PlateTypeCode.inter: '5',
  PlateTypeCode.yellow: '6',
  PlateTypeCode.unknown: 'unknown',
};

VehicleListData _$VehicleListDataFromJson(Map<String, dynamic> json) =>
    VehicleListData(
      listData: (json['listData'] as List<dynamic>?)
          ?.map((e) => Vehicle.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$VehicleListDataToJson(VehicleListData instance) =>
    <String, dynamic>{
      'listData': instance.listData,
      'count': instance.count,
    };

VehicleListDataResponse _$VehicleListDataResponseFromJson(
        Map<String, dynamic> json) =>
    VehicleListDataResponse(
      data: json['data'] == null
          ? null
          : VehicleListData.fromJson(json['data'] as Map<String, dynamic>),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VehicleListDataResponseToJson(
        VehicleListDataResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };

VehicleDebit _$VehicleDebitFromJson(Map<String, dynamic> json) => VehicleDebit(
      statusDebit:
          $enumDecodeNullable(_$VehicleDebitStatusEnumMap, json['statusDebit']),
      vehicleId: json['vehicleId'] as int?,
      fee: json['fee'] as int?,
      debitStatusName: json['debitStatusName'] as String?,
    );

Map<String, dynamic> _$VehicleDebitToJson(VehicleDebit instance) =>
    <String, dynamic>{
      'statusDebit': _$VehicleDebitStatusEnumMap[instance.statusDebit],
      'vehicleId': instance.vehicleId,
      'fee': instance.fee,
      'debitStatusName': instance.debitStatusName,
    };

const _$VehicleDebitStatusEnumMap = {
  VehicleDebitStatus.inactive: 0,
  VehicleDebitStatus.active: 1,
  VehicleDebitStatus.complete: 2,
};
