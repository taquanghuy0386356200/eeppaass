part of 'create_beneficiary_bloc.dart';

abstract class CreateBeneficiaryState extends Equatable {
  const CreateBeneficiaryState();
}

class CreateBeneficiaryInitial extends CreateBeneficiaryState {
  @override
  List<Object> get props => [];
}

class CreateBeneficiaryFormSubmittedInProgress extends CreateBeneficiaryState {
  const CreateBeneficiaryFormSubmittedInProgress();

  @override
  List<Object> get props => [];
}

class CreateBeneficiaryFormSubmittedSuccess extends CreateBeneficiaryState {
  final String? message;

  const CreateBeneficiaryFormSubmittedSuccess({this.message});

  @override
  List<Object?> get props => [message];
}

class CreateBeneficiaryFormSubmittedFailure extends CreateBeneficiaryState {
  final String message;

  const CreateBeneficiaryFormSubmittedFailure(this.message);

  @override
  List<Object> get props => [message];
}
