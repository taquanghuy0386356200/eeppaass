part of 'vehicle_other_bloc.dart';

@immutable
abstract class VehicleOtherState extends Equatable {
  const VehicleOtherState();
}

class VehicleOtherInitial extends VehicleOtherState {
  @override
  List<Object> get props => [];
}

class VehicleOtherSearchedInProgress extends VehicleOtherState {
  const VehicleOtherSearchedInProgress();

  @override
  List<Object> get props => [];
}

class VehicleOtherSearchedFailure extends VehicleOtherState {
  final String message;

  const VehicleOtherSearchedFailure(this.message);

  @override
  List<Object> get props => [message];
}

class VehicleOtherSearchedSuccess extends VehicleOtherState {
  final List<Vehicle> vehicles;

  const VehicleOtherSearchedSuccess(this.vehicles);

  @override
  List<Object> get props => [vehicles];
}
