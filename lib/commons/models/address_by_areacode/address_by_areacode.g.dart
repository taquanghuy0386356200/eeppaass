// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_by_areacode.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressByAreaCode _$AddressByAreaCodeFromJson(Map<String, dynamic> json) =>
    AddressByAreaCode(
      areaCode: json['areaCode'] as String?,
      province: json['province'] as String?,
      name: json['name'] as String?,
      district: json['district'] as String?,
      precinct: json['precinct'] as String?,
      provinceName: json['provinceName'] as String?,
      districtName: json['districtName'] as String?,
      precinctName: json['precinctName'] as String?,
      fullName: json['fullName'] as String?,
      districtShop: json['districtShop'] as String?,
      provinceShop: json['provinceShop'] as String?,
    );

Map<String, dynamic> _$AddressByAreaCodeToJson(AddressByAreaCode instance) =>
    <String, dynamic>{
      'areaCode': instance.areaCode,
      'province': instance.province,
      'name': instance.name,
      'district': instance.district,
      'precinct': instance.precinct,
      'provinceName': instance.provinceName,
      'districtName': instance.districtName,
      'precinctName': instance.precinctName,
      'fullName': instance.fullName,
      'districtShop': instance.districtShop,
      'provinceShop': instance.provinceShop,
    };

AddressByAreaCodeResponse _$AddressByAreaCodeResponseFromJson(
        Map<String, dynamic> json) =>
    AddressByAreaCodeResponse(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => AddressByAreaCode.fromJson(e as Map<String, dynamic>))
          .toList(),
      mess: json['mess'] == null
          ? null
          : MessageData.fromJson(json['mess'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AddressByAreaCodeResponseToJson(
        AddressByAreaCodeResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'mess': instance.mess,
    };
