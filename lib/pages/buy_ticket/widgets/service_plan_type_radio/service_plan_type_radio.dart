import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:epass/commons/widgets/buttons/radio_group_buttons.dart';
import 'package:epass/pages/buy_ticket/bloc/service_plan_type_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ServicePlanTypeRadio extends StatelessWidget {
  const ServicePlanTypeRadio({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ServicePlanTypeBloc, ServicePlanTypeState>(
      builder: (context, state) {
        return RadioGroupButtons<ServicePlanType>(
          scrollDirection: Axis.horizontal,
          labels: ServicePlanType.values
              .where(
                (element) =>
                    element == ServicePlanType.monthly ||
                    element == ServicePlanType.quarterly,
              )
              .map((e) => e.label)
              .toList(),
          values: ServicePlanType.values
              .where(
                (element) =>
                    element == ServicePlanType.monthly ||
                    element == ServicePlanType.quarterly,
              )
              .toList(),
          defaultValue: state.servicePlanType,
          onChanged: (value) {
            if (value != null) {
              context
                  .read<ServicePlanTypeBloc>()
                  .add(ServicePlanTypeChanged(value));
            }
          },
        );
      },
    );
  }
}
