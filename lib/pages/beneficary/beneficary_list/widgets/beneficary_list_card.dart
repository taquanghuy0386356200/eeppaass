import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/models/beneficiary/beneficiary.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_shadow/simple_shadow.dart';

class BeneficaryListCard extends StatefulWidget {
  final Beneficiary beneficiary;

  const BeneficaryListCard({
    Key? key,
    required this.beneficiary,
  }) : super(key: key);

  @override
  State<BeneficaryListCard> createState() => _BeneficaryListCardState();
}

class _BeneficaryListCardState extends State<BeneficaryListCard> {
  @override
  Widget build(BuildContext context) {
    return SimpleShadow(
      offset: const Offset(0, 1),
      opacity: 0.1,
      sigma: 10,
      child: Material(
        borderRadius: BorderRadius.circular(16.0),
        color: Colors.white,
        child: InkWell(
          borderRadius: BorderRadius.circular(16.0),
          onTap: () => context.pushRoute(CashInRouter(beneficary: widget.beneficiary)),
          child: Container(
            padding: EdgeInsets.fromLTRB(8.w, 16.h, 25.w, 16.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Row(
              children: [
                SizedBox(width: 70.w, child: Assets.images.logo.image()),
                SizedBox(width: 8.w),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Tên gợi nhớ: ${widget.beneficiary.reminiscentName}',
                      ),
                      SizedBox(height: 6.h),
                      Text(
                        'Số tài khoản: ${widget.beneficiary.contractNo}',
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  child: IconButton(
                    icon: Icon(
                      Icons.info_rounded,
                      color: ColorName.primaryColor,
                      size: 26.r,
                    ),
                    onPressed: () => context.pushRoute(BeneficiaryDetailsRoute(
                      beneficiaryId:
                          widget.beneficiary.beneficiaryId.toString(),
                    )),
                  ),
                ),
                // Assets.icons.chevronRight.svg(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
