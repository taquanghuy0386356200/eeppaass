import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:epass/commons/constant.dart';
import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/repo/category_repository.dart';
import 'package:equatable/equatable.dart';

part 'sms_vehicle_category_event.dart';

part 'sms_vehicle_category_state.dart';

class SMSVehicleCategoryBloc extends Bloc<SMSVehicleCategoryEvent, SMSVehicleCategoryState> {
  final ICategoryRepository _categoryRepository;

  SMSVehicleCategoryBloc({required ICategoryRepository categoryRepository})
      : _categoryRepository = categoryRepository,
        super(SMSVehicleCategoryInitial()) {
    on<SMSVehicleCategoryFetched>(_onSMSVehicleCategoryFetched);
    on<SelectedSMSOption>(_onSelectedSMSOption);
  }

  FutureOr<void> _onSMSVehicleCategoryFetched(
    SMSVehicleCategoryFetched event,
    Emitter<SMSVehicleCategoryState> emit,
  ) async {
    emit(SMSVehicleCategoryFetchedInProgress());

    final result = await _categoryRepository.getCategories(
      tableName: event.isPersonal ? Constant.smsRegisPersonal : Constant.smsRegisEnterprise,
    );
    result.when(
      success: (response) {
        final CategoryModel selectedSMSOption = response.listData.firstWhere(
            (data) => data.isDefault == '1',
            orElse: () => const CategoryModel());
        emit(SMSVehicleCategoryFetchedSuccess(smsCategories: response.listData, selectedSMSOption: selectedSMSOption));

      },
      failure: (failure) => emit(
          SMSVehicleCategoryFetchedFailure(failure.message ?? 'Có lỗi xảy ra')),
    );
  }

  FutureOr<void> _onSelectedSMSOption(
    SelectedSMSOption event,
    emit,
  ) async {
    emit(SMSVehicleCategoryFetchedSuccess(
     smsCategories: event.smsCategories ,selectedSMSOption: event.selectedSMSOption ));
  }
}
