import 'package:epass/commons/models/categories/category_response.dart';
import 'package:epass/commons/validators/plate_number_validator.dart';
import 'package:epass/commons/widgets/buttons/splash_icon_button.dart';
import 'package:epass/commons/widgets/container/shadow_card.dart';
import 'package:epass/commons/widgets/textfield/plate_type_picker.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/register/bloc/register_vehicles/register_vehicles_bloc.dart';
import 'package:epass/commons/widgets/textfield/vehicle_type_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VehicleInputForm extends StatefulWidget {
  final int vehicleIndex;
  final String? owner;
  final String? plateNumber;
  final CategoryModel? plateType;
  final CategoryModel? vehicleType;
  final String registerFee;

  const VehicleInputForm({
    Key? key,
    required this.vehicleIndex,
    this.owner,
    this.plateNumber,
    this.plateType,
    this.vehicleType,
    this.registerFee = ''
  }) : super(key: key);

  @override
  State<VehicleInputForm> createState() => _VehicleInputFormState();
}

class _VehicleInputFormState extends State<VehicleInputForm> with PlateNumberValidator {
  final _controller = TextEditingController();
  late TextEditingController _usernameController;
  final FocusNode _usernameFocusNode = FocusNode();
  final FocusNode _plateNumberFocusNode = FocusNode();

  @override
  void initState() {
    _controller.text = widget.plateNumber ?? '';
    _usernameController = TextEditingController(text: '');
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _usernameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final String titleStep1 = (widget.vehicleIndex == 0) ? 'Nhập thông tin đăng ký' : 'Xe ${(widget.vehicleIndex + 1).toString()}';

    return ShadowCard(
      padding: EdgeInsets.fromLTRB(16.w, widget.vehicleIndex != 0 ? 10.h : 20.h, 16.w, 24.h),
      shadowOpacity: 0.2,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SizedBox(width: 20.w),
              Text(
                titleStep1,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: ColorName.primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              const Spacer(),
              widget.vehicleIndex != 0
                  ? SplashIconButton(
                      icon: Assets.icons.trash.svg(),
                      onTap: () => context.read<RegisterVehiclesBloc>().add(VehicleInfoRemoved(widget.vehicleIndex)),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
          SizedBox(height: 28.h),
          PrimaryTextField(
            controller: _usernameController,
            focusNode: _usernameFocusNode,
            isMandatory: true,
            labelText: 'Tên chủ phương tiện',
            hintText: 'Nhập tên chủ phương tiện',
            validator: (value) {
              if (value?.isEmpty ?? true) {
                return 'Vui lòng nhập Tên chủ phương tiện';
              }
              return null;
            },
            onChanged: (owner) {
              context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                    vehicleIndex: widget.vehicleIndex,
                    owner: owner,
                  ));
            },
            onClear: ()=> context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
              vehicleIndex: widget.vehicleIndex,
              owner: _usernameController.text,
            )),
            onEditingComplete: () {
              FocusScope.of(context).requestFocus(_plateNumberFocusNode);
            },
          ),
          SizedBox(height: 20.h),
          PrimaryTextField(
            controller: _controller,
            focusNode: _plateNumberFocusNode,
            isMandatory: true,
            labelText: 'Biển số xe',
            hintText: '30H12345',
            isPlateNumber: true,
            maxLength: plateNumberMaxLength,
            validator: (value) {
              final error = plateNumberValidator(value);
              if (error != null) {
                return error;
              }

              final duplicateItems = context
                  .read<RegisterVehiclesBloc>()
                  .state
                  .listRegisterVehicle
                  .where((element) => element.vehicleInfo.plateNumber == value);
              return duplicateItems.length > 1 ? 'Biển số xe đã tồn tại' : null;
            },
            onChanged: (plateNumber) {
              context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                    vehicleIndex: widget.vehicleIndex,
                    plateNumber: plateNumber,
                  ));
            },
            onClear: () =>
                context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                      vehicleIndex: widget.vehicleIndex,
                      plateNumber: _controller.text,
                    )),
            onEditingComplete: () {
              FocusScope.of(context).requestFocus(_usernameFocusNode);
            },
          ),
          SizedBox(height: 20.h),
          PlateTypePicker(
            isMandatory: true,
            selectedPlateType: widget.plateType,
            onItemChanged: (selectedItem) {
              context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                  vehicleIndex: widget.vehicleIndex,
                  plateType: selectedItem?.value,
                ));
            },
          ),
          SizedBox(height: 20.h),
          VehicleTypePicker(
            isMandatory: true,
            selectedVehicleType: widget.vehicleType,
            onItemChanged: (selectedItem) {
              context.read<RegisterVehiclesBloc>().add(VehicleInfoUpdated(
                  vehicleIndex: widget.vehicleIndex,
                  vehicleType: selectedItem?.value,
                ));
            },
          ),
          if (widget.registerFee != '')
            SizedBox(height: 20.h),
          if (widget.registerFee != '')
            Align(
              alignment: Alignment.center,
              child: Text(
                widget.registerFee.trim(),
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontSize: 18.sp,
                  fontWeight: FontWeight.w700,
                ),
              ),
            )
        ],
      ),
    );
  }
}
