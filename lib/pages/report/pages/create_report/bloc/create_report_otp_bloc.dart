import 'package:bloc/bloc.dart';
import 'package:epass/commons/repo/feedback_repository.dart';
import 'package:equatable/equatable.dart';

part 'create_report_otp_event.dart';

part 'create_report_otp_state.dart';

class CreateReportOtpBloc extends Bloc<CreateReportOtpEvent, CreateReportOtpState> {
  final IFeedbackRepository _feedbackRepository;

  CreateReportOtpBloc({
    required IFeedbackRepository feedbackRepository,
  })  : _feedbackRepository = feedbackRepository,
        super(CreateReportOtpInitial()) {
    on<CreateReportOtpRequested>((event, emit) async {
      // 17 - Góp ý
      // 18 - Báo lỗi dịch vụ
      if (state is CreateReportOtpRequestedSuccess) {
        final submittedTime = (state as CreateReportOtpRequestedSuccess).submittedTime;

        final count = DateTime.now().difference(submittedTime).inSeconds;
        if (count < 500) {
          emit(CreateReportOtpRequestedSuccess(submittedTime));
          return;
        }
      }
      if (state is CreateReportOtpPaused) {
        final submittedTime = (state as CreateReportOtpPaused).submittedTime;
        emit(CreateReportOtpRequestedSuccess(submittedTime));
        return;
      }

      final result = await _feedbackRepository.feedbackRequestOtp(confirmType: 18);

      result.when(
        success: (success) {
          final current = DateTime.now();
          emit(CreateReportOtpRequestedSuccess(current));
        },
        failure: (failure) {
          emit(CreateReportOtpRequestedFailure(message: failure.message ?? 'Có lỗi xảy ra'));
        },
      );
    });

    on<ReportOtpPaused>((event, emit) {
      if (state is CreateReportOtpRequestedSuccess) {
        final submittedTime = (state as CreateReportOtpRequestedSuccess).submittedTime;
        emit(CreateReportOtpPaused(submittedTime: submittedTime));
      }
    });
  }
}
