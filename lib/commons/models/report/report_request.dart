import 'package:epass/commons/models/report/report_attachment_file.dart';
import 'package:json_annotation/json_annotation.dart';

/// ticketChannel : "1"
/// custId : ""
/// contractNo : ""
/// contractId : ""
/// custTypeId : ""
/// custName : ""
/// email: ""
/// plateNumber: ""
/// plateTypeCode: ""
/// phoneNumber: ""
/// location: ""
/// priorityId: ""
/// l1TicketTypeId: ""
/// l2TicketTypeId: ""
/// l3TicketTypeId: ""
/// communeName: ""
/// provinceName: ""
/// districtName: ""
/// areaCode: ""
/// stationId: ""
/// stationName: ""
/// stageId: ""
/// stageName: ""
/// contentReceive : ""
/// ticketKind : "1"
/// requestDate: ""
/// supportInfo: ""
/// phoneContact : ""
/// attachmentFiles : ""
/// fileName: ""

/*
 ticketChannel        bắt buộc    body    Number    Fix giá trị = 1
 custId        bắt buộc    body    Number        Mã khách hàng
 contractNo        bắt buộc    body    String        Số hợp đồng
 contractId        bắt buộc    body    Number        ID hợp đồng
 custTypeId            body    Number        Loại khách hàng
 custName        bắt buộc    body    String        Người phản ánh
 email            body    String        Email
 plateNumber            body    String        Biển số xe liên quan
 plateTypeCode            body    String        Màu biển: 1,2,3,4,5,6 tương ứng ( Trắng, Xanh, Đỏ, .. , Vàng)
 phoneNumber            body    String        Số điện thoại liên hệ
 location            body    String        Địa chỉ
 priorityId        bắt buộc    body    Number    1: Bt, 2: Hot, 3: Vip    Mức độ ưu tiên
 l1TicketTypeId            body    Number    Truyền ID nhóm PA    Nhóm phản ánh (xem mô tả API lấy DS nhóm PA )
 l2TicketTypeId        bắt buộc    body    Number    Truyền ID thể loại    Thể loại ( xem mô tả API lấy DS thể loại)
 l3TicketTypeId            body    Number    Truyền ID DS loại PA    Loại phản ánh ( xem mô tả API lấy DS loại PA )
 communeName            body    String    Tên xã phường    Xã/phường
 provinceName        bắt buộc    body    String    Tên của thành phô    Tỉnh thành phố
 districtName            body    String    Tên của quận huyện    Quận huyện
 areaCode        bắt buộc    body    String    "Nếu chọn đến thành phố thì lấy mã thành phố
 Nếu chọn đến quận thì lấy mã quận
 Nếu chọn đến huyện thì lấy mã của huyện"    Mã của tính huyện thành phố
 stationId            body    Number        Trạm / Đoạn ( ID của trạm )
 stationName            body    String        Trạm / Đoạn ( Tên của trạm )
 stageId            body    Number        Trạm / Đoạn ( ID của đoạn )
 stageName            body    String        Trạm / Đoạn ( Tên của đoạn )
 contentReceive        bắt buộc    body    String        Nội dung phản ánh
 ticketKind            body    Number    1: Phát sinh, 2 Đơn lẻ    Loại lỗi
 requestDate        bắt buộc    body    String    Định dạng dd/MM/yyyy    Ngày muốn được xử lý xong
 supportInfo            body    String        Thông tin bổ sung
 phoneContact        bắt buộc    body    String        Số ĐT liên hệ khi xử lý PA
 attachmentFiles            body    List Object        File dẫn chứng
  fileName            String    Đuôi file chỉ accept: ".JPG", ".PNG", ".TIFF", ".BMP", ".PDF", ".JPEG", ".WEBP", "RAR", "ZIP", "XLSX", "XLS", "TXT", "DOC", "DOCX"    Tên file dẫn chứng
  base64Data            String    Dung lượng k quá 5MB    Chuỗi base64 data file dẫn chứng
 */

part 'report_request.g.dart';

@JsonSerializable(explicitToJson: true)
class ReportRequest {
  ReportRequest({
    this.ticketChannel = '1',
    required this.custId,
    required this.contractNo,
    required this.contractId,
    required this.custTypeId,
    required this.custName,
    this.email,
    this.plateNumber,
    this.plateTypeCode,
    this.phoneNumber,
    this.location,
    this.priorityId = '1',
    this.l1TicketTypeId,
    required this.l2TicketTypeId,
    this.l3TicketTypeId,
    required this.provinceName,
    this.districtName,
    this.communeName,
    required this.areaCode,
    this.stationId,
    this.stationName,
    this.stageId,
    this.stageName,
    required this.contentReceive,
    this.ticketKind = '1',
    required this.requestDate,
    this.supportInfo,
    required this.phoneContact,
    this.attachmentFiles,
    this.otp,
  });

  final String ticketChannel;
  final String custId;
  final String contractNo;
  final String contractId;
  final String custTypeId;
  final String custName;
  final String? email;
  final String? plateNumber;
  final String? plateTypeCode;
  final String? phoneNumber;
  final String? location;
  final String priorityId;
  final String? l1TicketTypeId;
  final String l2TicketTypeId;
  final String? l3TicketTypeId;
  final String provinceName;
  final String? districtName;
  final String? communeName;
  final String areaCode;
  final String? stationId;
  final String? stationName;
  final String? stageId;
  final String? stageName;
  final String contentReceive;
  final String? ticketKind;
  final String requestDate;
  final String? supportInfo;
  final String phoneContact;
  final List<ReportAttachmentFileRequest>? attachmentFiles;
  final String? otp;

  Map<String, dynamic> toJson() => _$ReportRequestToJson(this);
}
