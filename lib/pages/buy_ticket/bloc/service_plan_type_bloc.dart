import 'package:bloc/bloc.dart';
import 'package:epass/commons/enum/service_plan_type.dart';
import 'package:equatable/equatable.dart';

part 'service_plan_type_event.dart';

part 'service_plan_type_state.dart';

class ServicePlanTypeBloc
    extends Bloc<ServicePlanTypeEvent, ServicePlanTypeState> {
  ServicePlanTypeBloc() : super(const ServicePlanTypeState()) {
    on<ServicePlanTypeChanged>((event, emit) {
      emit(ServicePlanTypeState(servicePlanType: event.servicePlanType));
    });
  }
}
