part of 'create_feedback_otp_bloc.dart';

abstract class CreateFeedbackOtpState extends Equatable {
  const CreateFeedbackOtpState();
}

class CreateFeedbackOtpInitial extends CreateFeedbackOtpState {
  @override
  List<Object> get props => [];
}

class CreateFeedbackOtpRequestedInProgress extends CreateFeedbackOtpState {
  const CreateFeedbackOtpRequestedInProgress();

  @override
  List<Object> get props => [];
}

class CreateFeedbackOtpPaused extends CreateFeedbackOtpState {
  final DateTime submittedTime;

  const CreateFeedbackOtpPaused({required this.submittedTime});

  @override
  List<Object> get props => [submittedTime];
}

class CreateFeedbackOtpRequestedSuccess extends CreateFeedbackOtpState {
  final DateTime submittedTime;

  const CreateFeedbackOtpRequestedSuccess(this.submittedTime);

  @override
  List<Object> get props => [submittedTime];
}

class CreateFeedbackOtpRequestedFailure extends CreateFeedbackOtpState {
  final String message;

  const CreateFeedbackOtpRequestedFailure({
    required this.message,
  });

  @override
  List<Object> get props => [message];
}
