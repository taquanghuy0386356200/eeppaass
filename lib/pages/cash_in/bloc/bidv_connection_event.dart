part of 'bidv_connection_bloc.dart';

abstract class BidvConnectionEvent extends Equatable {
  const BidvConnectionEvent();
}

class BidvConnectionProceeded extends BidvConnectionEvent {
  final String payerId;
  final String payerName;
  final String payerAdd;
  final String payerMobile;
  final String payerIdentity;

  const BidvConnectionProceeded({
    required this.payerId,
    required this.payerName,
    required this.payerAdd,
    required this.payerMobile,
    required this.payerIdentity,
  });

  @override
  List<Object> get props =>
      [payerId, payerName, payerMobile, payerIdentity, payerAdd];
}
