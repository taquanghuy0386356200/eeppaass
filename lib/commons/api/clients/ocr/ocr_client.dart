import 'package:dio/dio.dart' hide Headers;
import 'package:epass/commons/models/ocr_id_card/id_card_ocr.dart';
import 'package:epass/commons/models/ocr_id_card/id_card_ocr_request.dart';
import 'package:retrofit/retrofit.dart';
import 'package:epass/commons/api/endpoint.dart' as endpoint;

part 'ocr_client.g.dart';

@RestApi()
abstract class OCRClient {
  factory OCRClient(
    Dio dio, {
    String baseUrl,
  }) = _OCRClient;

  /// /api/v2/ocr/idcard
  @POST(endpoint.scanIdCard)
  @DioResponseType(ResponseType.json)
  @Headers(<String, dynamic>{
    'Content-type': 'application/json',
    'api-key': '7c8ba773-64cd-4ba5-a9bd-f035f06d0149',
  })
  Future<IdCardOcr> scanIdCard({
    @Body() required IdCardOcrRequest idCardOcrRequest,
  });
}
