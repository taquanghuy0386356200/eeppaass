/**
 * Automatically generated file. DO NOT MODIFY
 */
package dev.jerson.fast_rsa;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "dev.jerson.fast_rsa";
  public static final String BUILD_TYPE = "release";
}
