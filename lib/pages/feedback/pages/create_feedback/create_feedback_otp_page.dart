import 'dart:async';
import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/commons/widgets/animations/fade_animation.dart';
import 'package:epass/commons/widgets/container/gradient_header_container.dart';
import 'package:epass/commons/widgets/container/rounded_top_container.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/success_dialog.dart';
import 'package:epass/commons/widgets/pages/base_page.dart';
import 'package:epass/gen/colors.gen.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/bloc/create_feedback_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/bloc/create_feedback_otp_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_form/bloc/feedback_form_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/otp_countdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class CreateFeedbackOTPPage extends StatefulWidget {
  const CreateFeedbackOTPPage({
    Key? key,
  }) : super(key: key);

  @override
  State<CreateFeedbackOTPPage> createState() => _CreateFeedbackOTPPageState();
}

class _CreateFeedbackOTPPageState extends State<CreateFeedbackOTPPage> {
  final _errorController = StreamController<ErrorAnimationType>();

  @override
  void deactivate() {
    context.read<CreateFeedbackOtpBloc>().add(const FeedbackOtpPaused());
    super.deactivate();
  }

  @override
  void dispose() {
    _errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final phoneNumber = context.read<AppBloc>().state.user?.noticePhoneNumber;

    return Builder(builder: (context) {
      return BlocListener<CreateFeedbackBloc, CreateFeedbackState>(
        listener: (context, state) {
          if (state is CreateFeedbackFormSubmittedInProgress) {
            context.loaderOverlay.show();
          } else {
            context.loaderOverlay.hide();
          }
        },
        child: BasePage(
          backgroundColor: Colors.white,
          title: 'Gửi góp ý',
          resizeToAvoidBottomInset: false,
          child: Stack(
            children: [
              const GradientHeaderContainer(),
              SafeArea(
                child: FadeAnimation(
                  delay: 1,
                  direction: FadeDirection.up,
                  child: RoundedTopContainer(
                    padding: EdgeInsets.fromLTRB(16.w, 48.h, 16.w, 0.h),
                    child:
                        BlocConsumer<CreateFeedbackBloc, CreateFeedbackState>(
                      listener: (context, state) {
                        log("patpat $state");
                        if (state is CreateFeedbackFormSubmittedInProgress) {
                          context.loaderOverlay.show();
                        } else if (state
                            is CreateFeedbackFormSubmittedSuccess) {
                          context.loaderOverlay.hide();
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (dialogContext) {
                              return SuccessDialog(
                                content:
                                    "Cảm ơn bạn đã đóng góp ý kiến cho ePass, chúng tôi xin ghi nhận để nghiên cứu, cải tiến để nâng cao chất lượng dịch vụ",
                                buttonTitle: 'Quay lại',
                                contentTextAlign: TextAlign.center,
                                onButtonTap: () async {
                                  Navigator.of(dialogContext).pop();
                                  Navigator.of(context).pop();
                                  context.navigateTo(const SupportRoute());
                                },
                              );
                            },
                          );
                        } else if (state
                            is CreateFeedbackFormSubmittedFailure) {
                          context.loaderOverlay.hide();
                          _errorController.add(ErrorAnimationType.shake);
                        }
                      },
                      builder: (context, state) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: double.infinity,
                              child: Text(
                                'Xác nhận',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .copyWith(
                                      fontSize: 22.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(height: 24.h),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.w),
                              child: RichText(
                                text: TextSpan(
                                  text:
                                      'Nhập mã OTP đã được gửi đến số điện thoại ',
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: '${phoneNumber ?? ''}.',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                  ],
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                textAlign: TextAlign.center,
                                maxLines: 2,
                              ),
                            ),
                            SizedBox(height: 24.h),
                            PinCodeTextField(
                              appContext: context,
                              errorAnimationController: _errorController,
                              length: 6,
                              obscureText: false,
                              animationType: AnimationType.fade,
                              pinTheme: PinTheme(
                                shape: PinCodeFieldShape.box,
                                fieldHeight: 46.r,
                                fieldWidth: 46.r,
                                borderRadius: BorderRadius.circular(8.0),
                                errorBorderColor: ColorName.error,
                                borderWidth: 1.0,
                                activeColor: ColorName.disabledBorderColor,
                                activeFillColor: Colors.white,
                                inactiveColor: ColorName.disabledBorderColor,
                                inactiveFillColor: Colors.white,
                                selectedColor: ColorName.disabledBorderColor,
                                selectedFillColor: Colors.white,
                              ),
                              backgroundColor: Colors.transparent,
                              autoDismissKeyboard: true,
                              autoFocus: true,
                              keyboardType: TextInputType.number,
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.w600,
                                  ),
                              onCompleted: _onOTPCompleted,
                              onChanged: (value) {},
                              beforeTextPaste: (text) {
                                //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                return false;
                              },
                            ),
                            if (state is CreateFeedbackFormSubmittedFailure)
                              SizedBox(height: 8.h),
                            if (state is CreateFeedbackFormSubmittedFailure)
                              Text(
                                state.message,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                      color: ColorName.error,
                                    ),
                              ),
                            SizedBox(height: 20.h),
                            BlocSelector<
                                CreateFeedbackOtpBloc,
                                CreateFeedbackOtpState,
                                CreateFeedbackOtpRequestedSuccess?>(
                              selector: (state) =>
                                  state is CreateFeedbackOtpRequestedSuccess
                                      ? state
                                      : null,
                              builder: (context, state) {
                                return state != null
                                    ? OTPCountdown(
                                        otpSubmittedTime: state.submittedTime,
                                      )
                                    : const SizedBox.shrink();
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  void _onOTPCompleted(String otp) {
    FocusScope.of(context).unfocus();

    final feedbackFormState = context.read<FeedbackFormBloc>().state;
    final filePickerState = context.read<FilePickerBloc>().state;

    final feedbackType = feedbackFormState.feedbackType;
    final receiveContent = feedbackFormState.receiveContent;
    log("patpat step 1 ${feedbackType} -- ${receiveContent}");
    if (feedbackType != null && receiveContent != null) {
      log("patpat step 2");
      context.read<CreateFeedbackBloc>().add(CreateFeedbackFormSubmitted(
            feedbackType: feedbackType,
            receiveContent: receiveContent,
            filePaths: filePickerState.filePaths,
            otp: otp,
          ));
    }
  }
}
