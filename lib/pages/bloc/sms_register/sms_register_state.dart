part of 'sms_register_bloc.dart';

class SmsRegisterTotalState  {
  final bool successRegister;
  final String message;
  final String messagePopup;
  final bool isLoading;
  final bool isRegister;
  final UserSmsVehicleSetting? userSMSVehicleSetting;
  final List<CategoryModel>? smsCategories;
  final bool isExtendTime;
  final CategoryModel? selectedSMSOption;

  const SmsRegisterTotalState({
    this.successRegister = false,
    this.message = '',
    this.messagePopup = '',
    this.isLoading = false,
    this.isRegister = false,
    this.isExtendTime = false,
    this.userSMSVehicleSetting,
    this.smsCategories,
    this.selectedSMSOption
  });

  SmsRegisterTotalState copyWith({
    bool? successRegister,
    String? message,
    bool? isLoading,
    bool? isRegister,
    bool? isExtendTime,
    String? messagePopup,
    UserSmsVehicleSetting? userSMSVehicleSetting,
    List<CategoryModel>? smsCategories,
    CategoryModel? selectedSMSOption,
  }) {
    return SmsRegisterTotalState(
      successRegister: successRegister ?? this.successRegister,
      message: message ?? this.message,
      isLoading: isLoading ?? this.isLoading,
      isExtendTime: isExtendTime ?? this.isExtendTime,
      messagePopup: messagePopup ?? this.messagePopup,
      smsCategories: smsCategories ?? this.smsCategories,
      isRegister: isRegister ?? this.isRegister,
      userSMSVehicleSetting:
          userSMSVehicleSetting ?? this.userSMSVehicleSetting,
        selectedSMSOption:selectedSMSOption?? this.selectedSMSOption
    );
  }

}
