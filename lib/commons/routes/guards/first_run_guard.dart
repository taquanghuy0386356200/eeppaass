import 'package:auto_route/auto_route.dart';
import 'package:epass/commons/routes/router.gr.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/app.dart';
import 'package:epass/pages/bloc/setting/setting_bloc.dart';

class FirstRunGuard extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    final settingBloc = getIt<SettingBloc>();
    if (settingBloc.state.isFirstRun) {
      // canNotShowPopupUpdateApp = true;
      router.replace(const OnboardingRoute());
    } else {
      // canNotShowPopupUpdateApp = false;
      resolver.next();
    }
  }
}
