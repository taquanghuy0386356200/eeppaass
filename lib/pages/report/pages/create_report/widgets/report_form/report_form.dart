import 'dart:developer';

import 'package:epass/commons/repo/report_repository.dart';
import 'package:epass/commons/validators/email_validator.dart';
import 'package:epass/commons/validators/phone_number_validator.dart';
import 'package:epass/commons/widgets/support_center_widgets/attachment_file_item.dart';
import 'package:epass/commons/widgets/buttons/loading_primary_button.dart';
import 'package:epass/commons/widgets/buttons/secondary_button.dart';
import 'package:epass/commons/widgets/dropdown/primary_dropdown.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_bloc.dart';
import 'package:epass/commons/widgets/modal/file_picker/file_picker_modal.dart';
import 'package:epass/commons/widgets/snackbar/snackbbar.dart';
import 'package:epass/commons/widgets/textfield/primary_text_field.dart';
import 'package:epass/commons/widgets/textfield/text_field_circular_progress_indicator.dart';
import 'package:epass/gen/assets.gen.dart';
import 'package:epass/injections.dart';
import 'package:epass/pages/bloc/app/app_bloc.dart';
import 'package:epass/pages/buy_ticket/bloc/ticket_vehicle_bloc.dart';
import 'package:epass/pages/feedback/pages/create_feedback/widgets/feedback_type_picker/feedback_type_bloc.dart';
import 'package:epass/pages/home_tab/bloc/home_tabs_bloc.dart';
import 'package:epass/pages/register/pages/register_address/widgets/address_dropdown/address_dropdown.dart';
import 'package:epass/pages/report/pages/create_report/bloc/create_report_bloc.dart';
import 'package:epass/pages/report/pages/create_report/bloc/create_report_otp_bloc.dart';
import 'package:epass/pages/report/pages/create_report/create_report_otp_page.dart';
import 'package:epass/pages/report/pages/create_report/widgets/group_report_dropdown/report_group_dropdown.dart';
import 'package:epass/pages/report/pages/create_report/widgets/report_form/bloc/report_form_bloc.dart';
import 'package:epass/pages/report/pages/create_report/widgets/report_station_dropdown.dart';
import 'package:epass/pages/vehicle/vehicle_list/bloc/vehicle_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ReportForm extends StatefulWidget {
  const ReportForm({Key? key}) : super(key: key);

  @override
  State<ReportForm> createState() => _ReportFormState();
}

class _ReportFormState extends State<ReportForm> with PhoneNumberValidator,EmailValidator {
  final _formKey = GlobalKey<FormState>();

  final _contentController = TextEditingController();
  late TextEditingController _nguoiPhanAnh;
  late TextEditingController _email;
  late TextEditingController _diaChi;
  late TextEditingController _thongTinBoSung;
  late TextEditingController _soDienThoaiLienHe;

  @override
  void initState() {
    super.initState();
    context.read<VehicleListBloc>().add(VehicleListFetched());
    _nguoiPhanAnh = TextEditingController(
        text: context.read<ReportFormBloc>().state.reporterName);
    _email =
        TextEditingController(text: context.read<ReportFormBloc>().state.email);
    _diaChi = TextEditingController(
        text: context.read<ReportFormBloc>().state.location);
    _thongTinBoSung = TextEditingController();
    _soDienThoaiLienHe = TextEditingController(text: context.read<ReportFormBloc>().state.phoneContact);
    print(
        'sfafaf ${context.read<AppBloc>().state.user?.noticeAreaName} and ${context.read<AppBloc>().state.user?.address}');
    context
        .read<HomeTabsBloc>()
        .add(const HomeTabBarRequestHidden(isHidden: true));
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: MultiBlocListener(
        listeners: [
          BlocListener<FilePickerBloc, FilePickerState>(
            listener: (context, state) {
              if (state.isLoading) {
                context.loaderOverlay.show();
              } else {
                context.loaderOverlay.hide();
              }
              context
                  .read<ReportFormBloc>()
                  .add(ReportAttachmentFilesChanged(state.filePaths));
            },
          ),
          BlocListener<VehicleListBloc, VehicleListState>(
              listener: (context, state) async {
            if (state.isLoading) {
              context.loaderOverlay.show();
            } else {
              context.loaderOverlay.hide();
            }
          }),
          BlocListener<TicketVehicleBloc, TicketVehicleState>(
            listener: (context, state) {},
          ),
          BlocListener<FeedbackTypeBloc, FeedbackTypeState>(
              listener: (context, state) {}),
          BlocListener<ReportFormBloc, ReportFormState>(
              listener: (context, state) {}),
          BlocListener<CreateReportOtpBloc, CreateReportOtpState>(
            listener: (context, state) async {
              if (state is CreateReportOtpRequestedFailure) {
                await showErrorSnackBBar(
                    context: context, message: state.message);
              } else if (state is CreateReportOtpRequestedSuccess) {
                await _showOTPDialog(context);
              }
            },
          ),
        ],
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.w),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 24.h,
                ),
                Text(
                  'Thông tin người phản ánh',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 16.h,
                ),
                PrimaryTextField(
                  labelText: 'Người phản ánh',
                  hasClearButton: true,
                  isMandatory: true,
                  controller: _nguoiPhanAnh,
                  hintText: 'Nhập tên người phản ánh',
                  // initialValue:
                  //     context.read<ReportFormBloc>().state.reporterName,
                  onClear: () => context
                      .read<ReportFormBloc>()
                      .add(ReporterNameChanged(_nguoiPhanAnh.text)),
                  onChanged: (value) => context
                      .read<ReportFormBloc>()
                      .add(ReporterNameChanged(value)),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Vui lòng nhập tên người phản ánh';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.h),
                SizedBox(height: 16.h),
                BlocBuilder<VehicleListBloc, VehicleListState>(
                    builder: (context, state) {
                  return PrimaryDropdown<DropdownItem>(
                    label: 'Biển số xe liên quan',
                    dropdownHeight: 205.h,
                    items: state.listData
                        .map((v) => DropdownItem(
                            title: '${v.plateNumber} (${v.vehicleGroupName})',
                            value: '${v.plateTypeCode?.index}'))
                        .toList(),
                    onChanged: (item) =>
                        context.read<ReportFormBloc>().add(VehiclesChanged(
                              plateNumber: item?.title
                                  ?.substring(0, item.title?.indexOf(' (')),
                              plateTypeCode: item?.value,
                            )),
                    suffix: state.isFull
                        ? Assets.icons.chevronDown.svg()
                        : const TextFieldCircularProgressIndicator(),
                  );
                }),
                SizedBox(height: 16.h),
                PrimaryTextField(
                  labelText: 'Email',
                  hintText: 'Nhập email',
                  controller: _email,
                  onClear: () => context
                      .read<ReportFormBloc>()
                      .add(EmailChanged(_email.text)),
                  // initialValue: context.read<ReportFormBloc>().state.email,
                  onChanged: (value) =>
                      context.read<ReportFormBloc>().add(EmailChanged(value)),
                  validator: emailValidator,
                ),
                SizedBox(height: 16.h),
                PrimaryTextField(
                  controller: _diaChi,
                  labelText: 'Địa chỉ',
                  hintText: 'Nhập địa chỉ',
                  // initialValue: context.read<ReportFormBloc>().state.location,
                  onChanged: (value) => context
                      .read<ReportFormBloc>()
                      .add(LocationChanged(value)),
                  onClear: () => context
                      .read<ReportFormBloc>()
                      .add(LocationChanged(_diaChi.text)),
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Vui lòng nhập địa chỉ';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 24.h,
                ),
                Text(
                  'Thông tin phản ánh',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontWeight: FontWeight.w700),
                ),
                SizedBox(height: 16.h),
                BlocSelector<ReportFormBloc, ReportFormState,
                        GroupReportState?>(
                    selector: (state) => state.groupReportState,
                    builder: (context, groupReportState) {
                      return ReportGroupDropdown(
                        isMandatory: true,
                          reportGroupDropdownType:
                              ReportGroupDropdownType.group,
                          selectedItem: groupReportState?.l1TicketType,
                          onChanged: (feedbackType) {
                            context
                                .read<ReportFormBloc>()
                                .add(L1TicketTypeChanged(feedbackType));
                          });
                    }),
                SizedBox(height: 16.h),
                BlocSelector<ReportFormBloc, ReportFormState,
                        GroupReportState?>(
                    selector: (state) => state.groupReportState,
                    builder: (context, groupReportState) {
                      return ReportGroupDropdown(
                          isMandatory: true,
                          key: UniqueKey(),
                          reportGroupDropdownType: ReportGroupDropdownType.kind,
                          selectedItem: groupReportState?.l2TicketType,
                          parentId: int.parse(
                              groupReportState?.l1TicketType?.code ?? '0'),
                          onChanged: (feedbackType) {
                            context
                                .read<ReportFormBloc>()
                                .add(L2TicketTypeChanged(feedbackType));
                          });
                    }),
                SizedBox(height: 16.h),
                BlocSelector<ReportFormBloc, ReportFormState,
                        GroupReportState?>(
                    selector: (state) => state.groupReportState,
                    builder: (context, groupReportState) {
                      return ReportGroupDropdown(
                          isMandatory: true,
                          key: UniqueKey(),
                          reportGroupDropdownType: ReportGroupDropdownType.type,
                          parentId: int.parse(
                              groupReportState?.l2TicketType?.code ?? '0'),
                          selectedItem: groupReportState?.l3TicketType,
                          onChanged: (feedbackType) => context
                              .read<ReportFormBloc>()
                              .add(L3TicketTypeChanged(feedbackType)));
                    }),
                SizedBox(height: 16.h),
                BlocSelector<ReportFormBloc, ReportFormState, LocationInfo?>(
                    selector: (state) => state.locationInfo,
                    builder: (context, locationInfo) {
                      return AddressDropdown(
                          dropdownType: AddressDropdownType.province,
                          selectedItem: locationInfo?.province,
                          onChanged: (province) {
                            print('province: $province');
                            context
                                .read<ReportFormBloc>()
                                .add(ProvinceChanged(province));
                          });
                    }),
                SizedBox(
                  height: 16.h,
                ),
                BlocSelector<ReportFormBloc, ReportFormState, LocationInfo?>(
                    selector: (state) => state.locationInfo,
                    builder: (context, locationInfo) {
                      return AddressDropdown(
                          key: UniqueKey(),
                          dropdownType: AddressDropdownType.district,
                          provinceCode: locationInfo?.province?.province,
                          selectedItem: locationInfo?.district,
                          isValidate: false,
                          onChanged: (district) {
                            print('district: $district');
                            context
                                .read<ReportFormBloc>()
                                .add(DistrictChanged(district));
                          });
                    }),
                SizedBox(
                  height: 16.h,
                ),
                BlocSelector<ReportFormBloc, ReportFormState, LocationInfo?>(
                    selector: (state) => state.locationInfo,
                    builder: (context, locationInfo) {
                      return AddressDropdown(
                          key: UniqueKey(),
                          dropdownType: AddressDropdownType.ward,
                          provinceCode: locationInfo?.province?.province,
                          districtCode: locationInfo?.district?.district,
                          selectedItem: locationInfo?.ward,
                          isValidate: false,
                          onChanged: (ward) {
                            context
                                .read<ReportFormBloc>()
                                .add(CommuneChanged(ward));
                          });
                    }),
                SizedBox(height: 16.h),
                ReportStationDropdown(
                  onChanged: (value) {
                    context.read<ReportFormBloc>().add(value?.stationId != null
                        ? StationIdChanged(value?.stationId.toString())
                        : StageIdChanged(value?.stageId.toString()));
                    context
                        .read<ReportFormBloc>()
                        .add(StationStageNameChanged(value?.name));
                  },
                ),
                SizedBox(
                  height: 16.h,
                ),
                BlocSelector<ReportFormBloc, ReportFormState,
                        GroupReportState?>(
                    selector: (state) => state.groupReportState,
                    builder: (context, groupReportState) {
                      final level = groupReportState?.currentLevel ?? 1;
                      log("patpatpat $level");
                      switch (level) {
                        case 1:
                          _contentController.text =
                              groupReportState?.l1TicketType?.ticketTemplate ??
                                  "";
                          context.read<ReportFormBloc>().add(
                              ReceiveContentChanged(groupReportState
                                      ?.l1TicketType?.ticketTemplate ??
                                  ""));
                          break;
                        case 2:
                          _contentController.text =
                              groupReportState?.l2TicketType?.ticketTemplate ??
                                  "";
                          context.read<ReportFormBloc>().add(
                              ReceiveContentChanged(groupReportState
                                      ?.l2TicketType?.ticketTemplate ??
                                  ""));
                          break;
                        case 3:
                          _contentController.text =
                              groupReportState?.l3TicketType?.ticketTemplate ??
                                  "";
                          context.read<ReportFormBloc>().add(
                              ReceiveContentChanged(groupReportState
                                      ?.l3TicketType?.ticketTemplate ??
                                  ""));
                          break;
                      }
                      return PrimaryTextField(
                        controller: _contentController,
                        labelText: 'Nội dung phản ánh',
                        isMandatory: true,
                        hintText: '',
                        maxLines: 5,
                        onClear: () => context
                            .read<ReportFormBloc>()
                            .add(ReceiveContentChanged(
                              _contentController.text,
                            )),
                        maxLength: 320,
                        onChanged: (value) => context
                            .read<ReportFormBloc>()
                            .add(ReceiveContentChanged(value?.trim())),
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'Vui lòng nhập nội dung phản ánh';
                          }
                          return null;
                        },
                      );
                    }),
                SizedBox(height: 16.h),
                PrimaryDropdown<DropdownItem>(
                  label: 'Loại lỗi',
                  dropdownHeight: 145.h,
                  items: [
                    DropdownItem(title: 'Phát sinh', value: '1'),
                    DropdownItem(title: 'Đơn lẻ', value: '2'),
                  ],
                  onChanged: (item) => context
                      .read<ReportFormBloc>()
                      .add(TicketKindChanged(item?.value)),
                  suffix: Assets.icons.chevronDown.svg(),
                ),
                SizedBox(height: 16.h),
                PrimaryTextField(
                  controller: _thongTinBoSung,
                  labelText: 'Thông tin bổ sung',
                  hintText: 'Nhập thông tin bổ sung',
                  maxLength: 320,
                  onClear: () => context
                      .read<ReportFormBloc>()
                      .add(SupportInfoChanged(_thongTinBoSung.text)),
                  onChanged: (supportInfo) => context
                      .read<ReportFormBloc>()
                      .add(SupportInfoChanged(supportInfo?.trim())),
                ),
                SizedBox(height: 16.h),
                PrimaryTextField(
                  controller: _soDienThoaiLienHe,
                  isMandatory: true,
                  labelText: 'SĐT liên hệ',
                  hintText: 'Nhập số điện thoại liên hệ',
                  // initialValue:
                  //     context.read<ReportFormBloc>().state.phoneContact,
                  inputType: TextInputType.number,
                  maxLength: 20,
                  validator: phoneNumberValidator,
                  onClear: () => context
                      .read<ReportFormBloc>()
                      .add(PhoneContactChanged(_soDienThoaiLienHe.text)),
                  onChanged: (phoneContact) => context
                      .read<ReportFormBloc>()
                      .add(PhoneContactChanged(phoneContact)),
                ),
                SizedBox(
                  height: 16.h,
                ),
                BlocSelector<ReportFormBloc, ReportFormState, List<String>>(
                  selector: (state) => state.attachments,
                  builder: (context, filePaths) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (filePaths.isNotEmpty)
                          SizedBox(
                            height: 90.r,
                            child: ListView.separated(
                              primary: false,
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: filePaths.length,
                              itemBuilder: (context, index) {
                                final path = filePaths[index];
                                return AttachmentFileItem(path: path);
                              },
                              separatorBuilder: (context, index) =>
                                  SizedBox(width: 8.w),
                            ),
                          ),
                        if (filePaths.isNotEmpty) SizedBox(height: 20.h),
                        Padding(
                          padding: EdgeInsets.only(left: 4.w),
                          child: Text(
                            'Đính kèm tệp (${filePaths.length}/4)',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 12.h),
                        Row(
                          children: [
                            SizedBox(
                              height: 54.h,
                              child: SecondaryButton(
                                enabled: filePaths.length < 4,
                                title: 'Chọn tệp đính kèm',
                                icon: Assets.icons.upload.svg(),
                                onTap: filePaths.length < 4
                                    ? () async {
                                        await showFilePickerModal(context);
                                      }
                                    : null,
                              ),
                            ),
                            const Spacer(),
                          ],
                        ),
                      ],
                    );
                  },
                ),
                SizedBox(height: 32.h),
                SizedBox(
                  height: 56.h,
                  width: double.infinity,
                  child: BlocBuilder<CreateReportOtpBloc, CreateReportOtpState>(
                      builder: (context, state) => LoadingPrimaryButton(
                            title: 'Báo lỗi dịch vụ',
                            enabled:
                                state is! CreateReportOtpRequestedInProgress,
                            isLoading:
                                state is CreateReportOtpRequestedInProgress,
                            onTap: () async {
                              FocusScope.of(context).unfocus();
                              if (_formKey.currentState!.validate()) {
                                context
                                    .read<CreateReportOtpBloc>()
                                    .add(const CreateReportOtpRequested());
                              }
                            },
                          )),
                ),
                SizedBox(height: 32.h),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _showOTPDialog(BuildContext context) async {
    await showGeneralDialog(
      context: context,
      pageBuilder: (dialogContext, animation, secondaryAnimation) {
        return MultiBlocProvider(
          providers: [
            BlocProvider.value(
              value: context.read<CreateReportOtpBloc>(),
            ),
            BlocProvider.value(value: context.read<ReportFormBloc>()),
            BlocProvider.value(value: context.read<FilePickerBloc>()),
            BlocProvider(
              create: (context) => CreateReportBloc(
                reportRepository: getIt<IReportRepository>(),
                appBloc: getIt<AppBloc>(),
              ),
            ),
          ],
          child: const CreateReportOTPPage(),
        );
      },
    );
  }
}
