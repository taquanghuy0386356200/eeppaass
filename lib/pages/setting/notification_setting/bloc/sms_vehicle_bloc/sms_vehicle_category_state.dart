part of 'sms_vehicle_category_bloc.dart';

abstract class SMSVehicleCategoryState extends Equatable {
  const SMSVehicleCategoryState();
}

class SMSVehicleCategoryInitial extends SMSVehicleCategoryState {
  @override
  List<Object> get props => [];
}

class SMSVehicleCategoryFetchedInProgress extends SMSVehicleCategoryState {
  @override
  List<Object> get props => [];
}

class SMSVehicleCategoryFetchedSuccess extends SMSVehicleCategoryState {
  final List<CategoryModel>? smsCategories;
  final CategoryModel? selectedSMSOption;
   const SMSVehicleCategoryFetchedSuccess({ this.smsCategories, this.selectedSMSOption});

  @override
  List<Object?> get props => [smsCategories,selectedSMSOption];
}

class SMSVehicleCategoryFetchedFailure extends SMSVehicleCategoryState {
  final String message;

  const SMSVehicleCategoryFetchedFailure(this.message);

  @override
  List<Object> get props => [message];
}
